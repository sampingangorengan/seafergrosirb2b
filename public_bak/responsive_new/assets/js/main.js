var addItemToCart = function(id, sku, name, qty, ppi, price, image, metric) 
{

    $.ajax({
        type:'post',
        url: 'ajax/new-add-item.php',
		 dataType: 'JSON',
        data: {
            id:id,
            sku:sku,
            name:name,
            image:image,
            metric:metric,
            price_per_item: ppi,
            qty:qty,
            price:price
        }
    }).done(function(result){
        if (result.code == '400') {
            alert(result.message);
        }

        
        drawCart();
        countCartItem();
        $('#item_add_to_cart_wrap').html(drawItemButton(result.identifier));
        $('.control-cart-'+id).append(drawItemButton(result.identifier));
        $('.control-cart-'+id+' .list_add_to_cart').remove();
    })
}

var drawItemButton = function(data)
{
    var button = '<div class="qty-change" style="display: block;"> <div class="counter independent"><button class="btn minus decrease_cart_item_qty_out_pop">-</button> <input class="item_identifier" value="'+data+'" type="hidden"> <input class="totalx item_qty" name="" value="1" type="text"> <button class="btn plus increase_cart_item_qty_out_pop">+</button> </div> </div>';
    return button;
}


var drawCart = function() 
{
    $('.cart_item_container').html('');
    $('.cart_item_container').load('ajax/my-cart.php');
    
}

var emptyCart = function() 
{
	var button = '<div class="if-empty"><img src="assets/images/img-empty.png?59d219a0b81d3"><span>Your cart is still empty</span></div>';
    return button;
	
}

var countCartItem = function(){
    $.ajax({
        type:'get',
        url: 'ajax/cart-total-item.php'
    }).done(function(result){
        if(result == 0){
            $('.cart-notif').hide();
            $('#popupChart').attr('style','left:-25px;');
			$("#openChart").addClass('open');
			$("#openChart .dropdown-toggle").attr("aria-expanded","true");
        } else {
            $('.cart-notif').text(result);
            $('.cart-notif').show();
            $('#popupChart').attr('style','left:-65px;');
			$("#openChart").addClass('open');
			$("#openChart .dropdown-toggle").attr("aria-expanded","true");
        }
    });
}
var count_total_price = function()
{
    $.ajax({
        url : 'ajax/cart-total.php',
        type : 'POST',
        success : function(response){
            $('#cart_popup_total').text(response);
        },
        error : function(response){
            console.log('ajax error');
        }
    })
}
$('.procat').on('click', '.increase_cart_item_qty_out_pop', function(){
        var container = $(this).parent();
        var quantity = parseInt(container.find('.item_qty').val());
        var identifier = container.find('.item_identifier').val();

        var idf = container.find('.item_identifier').val();
        var item_id = $('#'+idf+' .item_id').val();
        //var quantity = $('#'+idf+' li .total').val();
        var input_qty = $('#'+idf+' li .total');
        var item_total = $('#'+idf+' .item_total');
        $.ajax({
            type:'post',
            url: 'ajax/increase-qty.php',
			dataType: 'JSON',
            data: {
                identifier: identifier,
				quantity : quantity,
            }
        }).done(function(result){
            $('.control-cart-'+item_id).find('.qty-change .item_qty').val(parseInt(quantity) + 1);
            input_qty.val(result.qty);
            item_total.text(result.total_price_item);
            count_total_price();
            countCartItem();
            container.find('.item_qty').val(result.qty);
        })

    });

    $('.procat').on('click', '.decrease_cart_item_qty_out_pop', function(){
        var container = $(this).parent();
        var quantity = parseInt(container.find('.item_qty').val());

        var idf = container.find('.item_identifier').val();
        var row = $('#'+idf);
        var item_id = $('#'+idf+' .item_id').val();
        //var quantity = $('#'+idf+' li .total').val();
        var input_qty = $('#'+idf+' li .total');
        var item_total = $('#'+idf+' .item_total');
        
        container.find('.item_qty').val(quantity-1);
        $.ajax({
            type:'post',
            url: 'ajax/decrease-qty.php',
			dataType: 'JSON',
            data: {
                identifier: container.find('.item_identifier').val(),
				quantity : quantity,
            }
        }).done(function(result){
            $('.control-cart-'+item_id).find('.qty-change .item_qty').val(parseInt(quantity) - 1);
            input_qty.val(result.qty);
            item_total.text(result.total_price_item);
            count_total_price();
            if(result.qty == 0){
                row.remove();
                if(result.total_item == 0){
                    $('.cart_item_container').html(emptyCart);
					$('#popupChart').attr('style','left:-25px;');
                }
            }
            if (result.qty == 0) {
                console.log(container.parent().parent());
                container.parent().parent().append('<button class="btn add-to-cart-btn list_add_to_cart" >ADD TO CART</button>');
                $('.qty-change').remove();
            }
            container.find('.item_qty').val(result.qty);

            countCartItem();
        })
    });
	$('#popupChart').on('click', '.decrease_cart_item_qty', function(){

        var row = $(this).parent().parent().parent().parent();
        var idf = $(this).parent().parent().parent().parent().attr('id');
        var item_id = $(this).parent().parent().parent().parent().find('.item_id').val();
        var quantity = $(this).parent().parent().find('li .total').val();
        var input_qty = $(this).parent().parent().find('li .total');
        var item_total = $(this).parent().parent().parent().parent().find('.item_total');

        $.ajax({
            type:'post',
             url: 'ajax/decrease-qty.php',
			 dataType: 'JSON',
            data: {
                identifier: idf,
				quantity : quantity,
            }
        }).done(function(response){
            $('.control-cart-'+item_id).find('.qty-change .item_qty').val(parseInt(quantity) - 1);
            input_qty.val(response.qty);
            item_total.text(response.total_price_item);
            count_total_price();
            if(response.qty == 0){
                row.remove();
                if(response.total_item == 0){
                    $('.cart_item_container').html(emptyCart);
					$('#popupChart').attr('style','left:-25px;');
                }
            }
            // drawCart();
            countCartItem();
        })

    });

    $('#popupChart').on('click', '.increase_cart_item_qty', function(){

        var idf = $(this).parent().parent().parent().parent().attr('id');
        var item_id = $(this).parent().parent().parent().parent().find('.item_id').val();
        var quantity = $(this).parent().parent().find('li .total').val();
        var input_qty = $(this).parent().parent().find('li .total');
        var item_total = $(this).parent().parent().parent().parent().find('.item_total');
        $.ajax({
            type:'post',
            url: 'ajax/increase-qty.php',
			dataType: 'JSON',
            data: {
                identifier: idf,
				quantity : quantity,
            }
        }).done(function(response){
            $('.control-cart-'+item_id).find('.qty-change .item_qty').val(parseInt(quantity) + 1);
            input_qty.val(response.qty);
            item_total.text(response.total_price_item);
            count_total_price();
            // drawCart();
            countCartItem();
        })

    });
	
	 $('#popupChart').on('click', '.list_del_cart_item_on_pop', function() {

        var item_container = $(this).parent().parent();
        var item_id = $(this).parent().parent().attr('id');
        var row = $(this).parent().parent();
        $.ajax({
            type: 'get',
             url: 'ajax/decrease-qty.php',
            data: {
                id: item_id
            }
        }).done(function(response){
            row.remove();
            count_total_price();
            if(response.total_item == 0){
                $('.cart_item_container').html(emptyCart);
				$('#popupChart').attr('style','left:-25px;');
            }
            countCartItem();
        })
    })

    $('.counter').on('click', '.decrease_cart_item_qty', function(){

        var idf = $(this).parent().parent().parent().attr('id');
		var quantity = $(this).parent().find('.total').val();
		var input_qty = $(this).parent().find('.total');
		var item_total = $(this).parent().parent().parent().parent().find('.item_total');

        $.ajax({
            type:'post',
             url: 'ajax/decrease-qty.php',
			 dataType: 'JSON',
            data: {
                identifier: idf,
				quantity : quantity
            }
        }).done(function(result){
			input_qty.val(result.qty);
			item_total.text(result.total_item);
            //location.reload();
        })

    });

    $('.counter').on('click', '.increase_cart_item_qty', function(){

        var idf = $(this).parent().parent().parent().attr('id');
		var quantity = $(this).parent().find('.total').val();
		var input_qty = $(this).parent().find('.total');
		var item_total = $(this).parent().parent().parent().parent().find('.item_total');
		
        $.ajax({
            type:'post',
            url: 'ajax/increase-qty.php',
			dataType: 'JSON',
            data: {
                identifier: idf,
				quantity : quantity
            }
        }).done(function(result){
			input_qty.val(result.qty);
			item_total.text(result.total_item);
            //location.reload();
        })

    });

    $('.cpage').on('click', '.list_del_cart_item', function(){

        var idf = $(this).parent().parent().attr('id');

        $.ajax({
            type: 'get',
            url: 'ajax/increase-qty.php',
            data: {
                id: idf
            }
        }).done(function(result){
            console.log(result)
            // Reload the page

            location.reload();

        })

    });
	$('#item_add_to_cart_wrap').on('click', '.increase_cart_item_qty_out_pop', function(){
        var container = $(this).parent();
        var quantity = parseInt(container.find('.item_qty').val());
        var identifier = container.find('.item_identifier').val();

        var idf = container.find('.item_identifier').val();
        var item_id = $('#'+idf+' .item_id').val();
        //var quantity = $('#'+idf+' li .total').val();
        var input_qty = $('#'+idf+' li .total');
        var item_total = $('#'+idf+' .item_total');
        $.ajax({
            type:'post',
            url: 'ajax/increase-qty.php',
			dataType: 'JSON',
            data: {
                identifier: identifier,
				quantity : quantity,
            }
        }).done(function(result){
            $('.control-cart-'+item_id).find('.qty-change .item_qty').val(parseInt(quantity) + 1);
            input_qty.val(result.qty);
            item_total.text(result.total_price_item);
            count_total_price();
            countCartItem();
            container.find('.item_qty').val(result.qty);
        })

    });

    $('#item_add_to_cart_wrap').on('click', '.decrease_cart_item_qty_out_pop', function(){
        var container = $(this).parent();
        var quantity = parseInt(container.find('.item_qty').val());

        var idf = container.find('.item_identifier').val();
        var row = $('#'+idf);
        var item_id = $('#'+idf+' .item_id').val();
        //var quantity = $('#'+idf+' li .total').val();
        var input_qty = $('#'+idf+' li .total');
        var item_total = $('#'+idf+' .item_total');
        
        container.find('.item_qty').val(quantity-1);
        $.ajax({
            type:'post',
            url: 'ajax/decrease-qty.php',
			dataType: 'JSON',
            data: {
                identifier: container.find('.item_identifier').val(),
				quantity : quantity,
            }
        }).done(function(result){
            $('.control-cart-'+item_id).find('.qty-change .item_qty').val(parseInt(quantity) - 1);
            input_qty.val(result.qty);
            item_total.text(result.total_price_item);
            count_total_price();
            if(result.qty == 0){
                row.remove();
                if(result.total_item == 0){
                    $('.cart_item_container').html(emptyCart);
					$('#popupChart').attr('style','left:-25px;');
                }
            }
            if (result.qty == 0) {
                console.log(container.parent().parent());
                container.parent().parent().append('<button class="btn btn-aneh" id="item_add_to_cart">ADD TO CART</button>');
                $('.qty-change').remove();
            }
            container.find('.item_qty').val(result.qty);

            countCartItem();
        })
    });
$(document).ready(function() {

	$('.procat').on('click', '.list_add_to_cart', function() {
        var closest = $(this).closest(':has(div input)');

        var item_id = closest.find('.atc-initiate').find('.item_id').val();
        var item_name = closest.find('.atc-initiate').find('.item_name').val();
        var item_sku= closest.find('.atc-initiate').find('.item_sku').val();
        var item_image = closest.find('.atc-initiate').find('.item_image').val();
        var item_metric = closest.find('.atc-initiate').find('.item_metric').val();
        var item_price = closest.find('.atc-initiate').find('.item_price').val();
        var item_qty = 1;


        addItemToCart(item_id, item_sku, item_name, item_qty, item_price, item_price, item_image, item_metric);
    });
	
	 $('#item_add_to_cart').on('click', function() {
        var item_qty = $('#item_qty').val();
        var item_id = $('#item_id').val();
        var item_name = $('#item_name').val();
        var item_sku = $('#item_sku').val();
        var item_image = $('.item_image').val();
        var item_metric = $('.item_metric').val();
        var item_price = $('.item_price').val();

        addItemToCart(item_id, item_sku, item_name, item_qty, item_price, item_price, item_image, item_metric);
    });

});

$(document).ready(function(){
	$('.addReview').click(function(){
	var container = $(this).parent();
	container.css('margin-top','5px');
	container.find('p').before(
        "<input placeholder='(Reviews Headline) e.g The Best Carrot Ever' name='title' type='text'><textarea rows='5' placeholder='(write your review here)' name='body'></textarea><button type='button' class='btn btn-aneh doSubmit btn-long'>SUBMIT</button>"
	);
	$(this).hide();
	container.find('p.shr').hide();

});
	
	$('.check-input').on('blur', function(){
		tmpval = $(this).val();
		if(tmpval == '') {
			$(this).addClass('empty');
			$(this).removeClass('not-empty');
		} else {
			$(this).addClass('not-empty');
			$(this).removeClass('empty');
		}
	});	
	$('.small-thumbnail').on('click', function(){
/*                alert($(this).attr('src'));*/

                $('.item.xlarge img').attr('src', $(this).attr('src'));
            });
	$('.close').click(function() {
	  $(this).parents('.dropdown').find('.dropdown-toggle').dropdown('toggle')
	});
	$("#show-search").click(function(){
		$(".mask-modal").show();
        $(".search-box").show(0).addClass("open");;
        
    });
	$(".hamburger-button").click(function(){
		$("body").addClass("fixed");
		$(".mask-modal").show();
        $("#side-nav").show(0).addClass("open");
        
    });
	
	$(".mask-modal").click(function(){
		$("body").removeClass("fixed");
        $(".search-box").removeClass("open").hide(0);
		$("#side-nav").removeClass("open").hide(0);
        $(".mask-modal").delay(300).hide(0); 
    });
	
	$('#grocer').click(function(e){
	e.preventDefault();
	if($(this).hasClass('active')){
		$(this).removeClass('active');
		$('#secondGrocer').hide();
	}else{
		$(this).addClass('active');
		$('#secondGrocer').show();
	}
});
$('#thirdGrocerBtn').click(function(e){
	e.preventDefault();
	if ($('#thirdGrocer').hasClass('active')) {
		$('#thirdGrocer').removeClass('active').hide();
	}else{
		$('#thirdGrocer').addClass('active').show();
		$('#thirdGrocer1').hide();
		$('#thirdGrocer2').hide();
		$('#thirdGrocer3').hide();
		$('#thirdGrocer4').hide();
		$('#thirdGrocer5').hide();
		$('#thirdGrocer6').hide();
		$('#thirdGrocer7').hide();
		$('#thirdGrocer8').hide();
		$('#thirdGrocer9').hide();
		$('#thirdGrocer10').hide();
		$('#thirdGrocer11').hide();
		$('#thirdGrocer12').hide();
	}
})

$('#thirdGrocerBtn1').click(function(e){
	e.preventDefault();
	if ($('#thirdGrocer1').hasClass('active')) {
		$('#thirdGrocer1').removeClass('active').hide();
        $('.second-level-category').hide();
        $('.second-lv').css('width','220px');

	}else{
        $('.second-level-category').show();
        $('.second-lv').css('width','420px');
		$('#thirdGrocer1').addClass('active').show();
		$('#thirdGrocer').hide();
		$('#thirdGrocer2').hide();
		$('#thirdGrocer3').hide();
		$('#thirdGrocer4').hide();
		$('#thirdGrocer5').hide();
		$('#thirdGrocer6').hide();
		$('#thirdGrocer7').hide();
		$('#thirdGrocer8').hide();
		$('#thirdGrocer9').hide();
		$('#thirdGrocer10').hide();
		$('#thirdGrocer11').hide();
		$('#thirdGrocer12').hide();
		
	}
})
$('#thirdGrocerBtn2').click(function(e){
	e.preventDefault();
	if ($('#thirdGrocer2').hasClass('active')) {
		$('#thirdGrocer2').removeClass('active').hide();
        $('.second-level-category').hide();
        $('.second-lv').css('width','220px');
	}else{
        $('.second-level-category').show();
        $('.second-lv').css('width','420px');
		$('#thirdGrocer2').addClass('active').show();
		$('#thirdGrocer').hide();
		$('#thirdGrocer1').hide();
		$('#thirdGrocer3').hide();
		$('#thirdGrocer4').hide();
		$('#thirdGrocer5').hide();
		$('#thirdGrocer6').hide();
		$('#thirdGrocer7').hide();
		$('#thirdGrocer8').hide();
		$('#thirdGrocer9').hide();
		$('#thirdGrocer10').hide();
		$('#thirdGrocer11').hide();
		$('#thirdGrocer12').hide();
	}
})
$('#thirdGrocerBtn3').click(function(e){
	e.preventDefault();
	if ($('#thirdGrocer3').hasClass('active')) {
		$('#thirdGrocer3').removeClass('active').hide();
        $('.second-level-category').hide();
        $('.second-lv').css('width','220px');
	}else{
        $('.second-level-category').show();
        $('.second-lv').css('width','420px');
		$('#thirdGrocer3').addClass('active').show();
		$('#thirdGrocer').hide();
		$('#thirdGrocer1').hide();
		$('#thirdGrocer2').hide();
		$('#thirdGrocer4').hide();
		$('#thirdGrocer5').hide();
		$('#thirdGrocer6').hide();
		$('#thirdGrocer7').hide();
		$('#thirdGrocer8').hide();
		$('#thirdGrocer9').hide();
		$('#thirdGrocer10').hide();
		$('#thirdGrocer11').hide();
		$('#thirdGrocer12').hide();
	}
})
$('#thirdGrocerBtn4').click(function(e){
	e.preventDefault();
	if ($('#thirdGrocer4').hasClass('active')) {
		$('#thirdGrocer4').removeClass('active').hide();
        $('.second-level-category').hide();
        $('.second-lv').css('width','220px');
	}else{
        $('.second-level-category').show();
        $('.second-lv').css('width','420px');
		$('#thirdGrocer4').addClass('active').show();
		$('#thirdGrocer').hide();
		$('#thirdGrocer1').hide();
		$('#thirdGrocer2').hide();
		$('#thirdGrocer3').hide();
		$('#thirdGrocer5').hide();
		$('#thirdGrocer6').hide();
		$('#thirdGrocer7').hide();
		$('#thirdGrocer8').hide();
		$('#thirdGrocer9').hide();
		$('#thirdGrocer10').hide();
		$('#thirdGrocer11').hide();
		$('#thirdGrocer12').hide();
	}
})
$('#thirdGrocerBtn5').click(function(e){
	e.preventDefault();
	if ($('#thirdGrocer5').hasClass('active')) {
		$('#thirdGrocer5').removeClass('active').hide();
        $('.second-level-category').hide();
        $('.second-lv').css('width','220px');
	}else{
        $('.second-level-category').show();
        $('.second-lv').css('width','420px');
		$('#thirdGrocer5').addClass('active').show();
		$('#thirdGrocer').hide();
		$('#thirdGrocer1').hide();
		$('#thirdGrocer2').hide();
		$('#thirdGrocer3').hide();
		$('#thirdGrocer4').hide();
		$('#thirdGrocer6').hide();
		$('#thirdGrocer7').hide();
		$('#thirdGrocer8').hide();
		$('#thirdGrocer9').hide();
		$('#thirdGrocer10').hide();
		$('#thirdGrocer11').hide();
		$('#thirdGrocer12').hide();
	}
})
$('#thirdGrocerBtn6').click(function(e){
	e.preventDefault();
	if ($('#thirdGrocer6').hasClass('active')) {
		$('#thirdGrocer6').removeClass('active').hide();
        $('.second-level-category').hide();
        $('.second-lv').css('width','220px');
	}else{
        $('.second-level-category').show();
        $('.second-lv').css('width','420px');
		$('#thirdGrocer6').addClass('active').show();
		$('#thirdGrocer').hide();
		$('#thirdGrocer1').hide();
		$('#thirdGrocer2').hide();
		$('#thirdGrocer3').hide();
		$('#thirdGrocer4').hide();
		$('#thirdGrocer5').hide();
		$('#thirdGrocer7').hide();
		$('#thirdGrocer8').hide();
		$('#thirdGrocer9').hide();
		$('#thirdGrocer10').hide();
		$('#thirdGrocer11').hide();
		$('#thirdGrocer12').hide();
	}
})
$('#thirdGrocerBtn7').click(function(e){
	e.preventDefault();
	if ($('#thirdGrocer7').hasClass('active')) {
		$('#thirdGrocer7').removeClass('active').hide();
        $('.second-level-category').hide();
        $('.second-lv').css('width','220px');
	}else{
        $('.second-level-category').show();
        $('.second-lv').css('width','420px');
		$('#thirdGrocer7').addClass('active').show();
		$('#thirdGrocer').hide();
		$('#thirdGrocer1').hide();
		$('#thirdGrocer2').hide();
		$('#thirdGrocer3').hide();
		$('#thirdGrocer4').hide();
		$('#thirdGrocer5').hide();
		$('#thirdGrocer6').hide();
		$('#thirdGrocer8').hide();
		$('#thirdGrocer9').hide();
		$('#thirdGrocer10').hide();
		$('#thirdGrocer11').hide();
		$('#thirdGrocer12').hide();
	}
})
$('#thirdGrocerBtn8').click(function(e){
	e.preventDefault();
	if ($('#thirdGrocer8').hasClass('active')) {
		$('#thirdGrocer8').removeClass('active').hide();
        $('.second-level-category').hide();
        $('.second-lv').css('width','220px');
	}else{
        $('.second-level-category').show();
        $('.second-lv').css('width','420px');
		$('#thirdGrocer8').addClass('active').show();
		$('#thirdGrocer').hide();
		$('#thirdGrocer1').hide();
		$('#thirdGrocer2').hide();
		$('#thirdGrocer3').hide();
		$('#thirdGrocer4').hide();
		$('#thirdGrocer5').hide();
		$('#thirdGrocer6').hide();
		$('#thirdGrocer7').hide();
		$('#thirdGrocer9').hide();
		$('#thirdGrocer10').hide();
		$('#thirdGrocer11').hide();
		$('#thirdGrocer12').hide();
	}
})
$('#thirdGrocerBtn9').click(function(e){
	e.preventDefault();
	if ($('#thirdGrocer9').hasClass('active')) {
		$('#thirdGrocer9').removeClass('active').hide();
        $('.second-level-category').hide();
        $('.second-lv').css('width','220px');
	}else{
        $('.second-level-category').show();
        $('.second-lv').css('width','420px');
		$('#thirdGrocer9').addClass('active').show();
		$('#thirdGrocer').hide();
		$('#thirdGrocer1').hide();
		$('#thirdGrocer2').hide();
		$('#thirdGrocer3').hide();
		$('#thirdGrocer4').hide();
		$('#thirdGrocer5').hide();
		$('#thirdGrocer6').hide();
		$('#thirdGrocer7').hide();
		$('#thirdGrocer8').hide();
		$('#thirdGrocer10').hide();
		$('#thirdGrocer11').hide();
		$('#thirdGrocer12').hide();
	}
})
$('#thirdGrocerBtn10').click(function(e){
	e.preventDefault();
	if ($('#thirdGrocer10').hasClass('active')) {
		$('#thirdGrocer10').removeClass('active').hide();
        $('.second-level-category').hide();
        $('.second-lv').css('width','220px');
	}else{
        $('.second-level-category').show();
        $('.second-lv').css('width','420px');
		$('#thirdGrocer10').addClass('active').show();
		$('#thirdGrocer').hide();
		$('#thirdGrocer1').hide();
		$('#thirdGrocer2').hide();
		$('#thirdGrocer3').hide();
		$('#thirdGrocer4').hide();
		$('#thirdGrocer5').hide();
		$('#thirdGrocer6').hide();
		$('#thirdGrocer7').hide();
		$('#thirdGrocer8').hide();
		$('#thirdGrocer9').hide();
		$('#thirdGrocer11').hide();
		$('#thirdGrocer12').hide();
	}
})
$('#thirdGrocerBtn11').click(function(e){
	e.preventDefault();
	if ($('#thirdGrocer11').hasClass('active')) {
		$('#thirdGrocer11').removeClass('active').hide();
        $('.second-level-category').hide();
        $('.second-lv').css('width','220px');
	}else{
        $('.second-level-category').show();
        $('.second-lv').css('width','420px');
		$('#thirdGrocer11').addClass('active').show();
		$('#thirdGrocer').hide();
		$('#thirdGrocer1').hide();
		$('#thirdGrocer2').hide();
		$('#thirdGrocer3').hide();
		$('#thirdGrocer4').hide();
		$('#thirdGrocer5').hide();
		$('#thirdGrocer6').hide();
		$('#thirdGrocer7').hide();
		$('#thirdGrocer8').hide();
		$('#thirdGrocer9').hide();
		$('#thirdGrocer10').hide();
		$('#thirdGrocer12').hide();
	}
})
$('#thirdGrocerBtn12').click(function(e){
	e.preventDefault();
	if ($('#thirdGrocer12').hasClass('active')) {
		$('#thirdGrocer12').removeClass('active').hide();
        $('.second-level-category').hide();
        $('.second-lv').css('width','220px');
	}else{
        $('.second-level-category').show();
        $('.second-lv').css('width','420px');
		$('#thirdGrocer12').addClass('active').show();
		$('#thirdGrocer').hide();
		$('#thirdGrocer1').hide();
		$('#thirdGrocer2').hide();
		$('#thirdGrocer3').hide();
		$('#thirdGrocer4').hide();
		$('#thirdGrocer5').hide();
		$('#thirdGrocer6').hide();
		$('#thirdGrocer7').hide();
		$('#thirdGrocer8').hide();
		$('#thirdGrocer9').hide();
		$('#thirdGrocer10').hide();
		$('#thirdGrocer11').hide();
	}
})
$('#toBenefits').click(function(e){
	e.preventDefault();
	$(this).addClass('line-active');
	$('#toHowitworks').removeClass('line-active');
	$('#toSignup').removeClass('line-active');
	$('html, body').animate({
		scrollTop: $('#benefits').offset().top
	}, 1000);
})
$('#toHowitworks').click(function(e){
	e.preventDefault();
	$(this).addClass('line-active');
	$('#toSignup').removeClass('line-active');
	$('#toBenefits').removeClass('line-active');
	$('html, body').animate({
		scrollTop: $('#howitworks').offset().top
	}, 1000);
})
$('#toSignup').click(function(e){
	e.preventDefault();
	$(this).addClass('line-active');
	$('#toBenefits').removeClass('line-active');
	$('#toHowitworks').removeClass('line-active');
	$('html, body').animate({
		scrollTop: $('#signup').offset().top
	}, 1000);
})
$('#toHowitworkstwo').click(function(e){
	e.preventDefault();
	$(this).addClass('line-active');
	$('#toClaimForm').removeClass('line-active');
	$('html, body').animate({
		scrollTop: $('#howitworks').offset().top
	}, 1000);
})
$('#toClaimForm').click(function(e){
	e.preventDefault();
	$(this).addClass('line-active');
	$('#toHowitworkstwo').removeClass('line-active');
	$('html, body').animate({
		scrollTop: $('#claimform').offset().top
	}, 1000);
})
$('#menu-collapse li a').click(function(e){
	e.preventDefault();
	$('#menu-collapse li a').removeClass('active');
	$(this).addClass('active');

})

});


$(function() {
      function maskImgs() {
        //$('.img-wrapper img').imagesLoaded({}, function() {
        $.each($('.img-wrapper img'), function(index, img) {
          var src = $(img).attr('src');
          var parent = $(img).parent();
          parent
            .css('background', 'url(' + src + ') no-repeat center center')
            .css('background-size', 'cover');
          $(img).remove();
        });
        //});
      }

      var preview = {
        init: function() {
          preview.setPreviewImg();
          preview.listenInput();
        },
        setPreviewImg: function(fileInput) {
          var path = $(fileInput).val();
          var uploadText = $(fileInput).siblings('.file-upload-text');

          if (!path) {
            $(uploadText).val('');
          } else {
            path = path.replace(/^C:\\fakepath\\/, "");
            $(uploadText).val(path);

            preview.showPreview(fileInput, path, uploadText);
          }
        },
        showPreview: function(fileInput, path, uploadText) {
          var file = $(fileInput)[0].files;

          if (file && file[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
              var previewImg = $(fileInput).parents('.file-upload-wrapper').siblings('.preview');
              var img = $(previewImg).find('img');

              if (img.length == 0) {
                $(previewImg).html('<img src="' + e.target.result + '" alt=""/>');
              } else {
                img.attr('src', e.target.result);
              }

              uploadText.val(path);
              maskImgs();
            }

            reader.onloadstart = function() {
              $(uploadText).val('uploading..');
            }

            reader.readAsDataURL(file[0]);
          }
        },
        listenInput: function() {
          $('.boxloop-photo').on('change', '.file-upload-native', function() {
            preview.setPreviewImg(this);
            console.log("oke mantap")
          });
        }
      };
      preview.init();
    });