$(document).foundation();

$(document).ready(function(){
	$('.remove-item-btn').click(removeItem);
	function removeItem(){
		$(this).parents('tr').first().remove();
	}

	$('.plus').click(plus);
	$('.minus').click(minus);

	$('.independent .plus').click(plus1);
	$('.independent .minus').click(minus1);
	$('.makin-aneh .independent .minus').click(minus2);

	function plus(){
		var cix = $(this).parents('tr').first().find('.total').val();
		cix++;
		$(this).parents('tr').first().find('.total').val(cix);
	}
	function minus(){
		var cix = $(this).parents('tr').first().find('.total').val();
		cix--;
		if (cix>=0) {
			$(this).parents('tr').first().find('.total').val(cix);
		}else if(cix<0){
			alert('Error');
		}
		
	}

	function plus1(){
		var cox = $(this).parents('.independent').find('.totalx').val();
		cox++;
		$(this).parents('.independent').find('.totalx').val(cox);
	}
	function minus1(){
		var cox = $(this).parents('.independent').find('.totalx').val();
		cox--;
		if (cox>=0) {
			$(this).parents('.independent').find('.totalx').val(cox);
		}else{
			alert('Quantity cannot less than 0 ');
		}
		
	}
	function minus2(){
		var cox = $(this).parents('.independent').find('.totalx').val();
		cox--;
		if (cox>=0) {
			$(this).parents('.independent').find('.totalx').val(cox);
		}else{
			$(this).parents('.item-box').find('.add-to-cart-btn').css('display', 'block');
			$(this).parents('.item-box').find('.add-to-cart-btn').next().css('display','none');
				// $('.add-to-cart-btn').css('display', 'block');
				// $('.add-to-cart-btn').next().css('display','none');
				$(this).parents('.independent').find('.totalx').val(1);
			
		}
		
	}

})










$(function() {
	function maskImgs() {
		//$('.img-wrapper img').imagesLoaded({}, function() {
		$.each($('.img-wrapper img'), function(index, img) {
			var src = $(img).attr('src');
			var parent = $(img).parent();
			parent
				.css('background', 'url(' + src + ') no-repeat center center')
				.css('background-size', 'cover');
			$(img).remove();
		});
		//});
	}

	var preview = {
		init: function() {
			preview.setPreviewImg();
			preview.listenInput();
		},
		setPreviewImg: function(fileInput) {
			var path = $(fileInput).val();
			var uploadText = $(fileInput).siblings('.file-upload-text');

			if (!path) {
				$(uploadText).val('');
			} else {
				path = path.replace(/^C:\\fakepath\\/, "");
				$(uploadText).val(path);

				preview.showPreview(fileInput, path, uploadText);
			}
		},
		showPreview: function(fileInput, path, uploadText) {
			var file = $(fileInput)[0].files;

			if (file && file[0]) {
				var reader = new FileReader();

				reader.onload = function(e) {
					var previewImg = $(fileInput).parents('.file-upload-wrapper').siblings('.preview');
					var img = $(previewImg).find('img');

					if (img.length == 0) {
						$(previewImg).html('<img src="' + e.target.result + '" alt=""/>');
					} else {
						img.attr('src', e.target.result);
					}

					uploadText.val(path);
					maskImgs();
				}

				reader.onloadstart = function() {
					$(uploadText).val('uploading..');
				}

				reader.readAsDataURL(file[0]);
			}
		},
		listenInput: function() {
			$('.boxloop-photo').on('change', '.file-upload-native', function() {
				preview.setPreviewImg(this);
				console.log("oke mantap")
			});
		}
	};
	preview.init();
});

$('#grocer').click(function(e){
	e.preventDefault();
	if($(this).hasClass('active')){
		$(this).removeClass('active');
		$('#secondGrocer').hide();
	}else{
		$(this).addClass('active');
		$('#secondGrocer').show();
	}
})
$('#thirdGrocerBtn').click(function(e){
	e.preventDefault();
	if ($('#thirdGrocer').hasClass('active')) {
		$('#thirdGrocer').removeClass('active').hide();
	}else{
		$('#thirdGrocer').addClass('active').show();
		$('#thirdGrocer1').hide();
		$('#thirdGrocer2').hide();
		$('#thirdGrocer3').hide();
		$('#thirdGrocer4').hide();
		$('#thirdGrocer5').hide();
		$('#thirdGrocer6').hide();
		$('#thirdGrocer7').hide();
		$('#thirdGrocer8').hide();
		$('#thirdGrocer9').hide();
		$('#thirdGrocer10').hide();
		$('#thirdGrocer11').hide();
		$('#thirdGrocer12').hide();
	}
})

$('#thirdGrocerBtn1').click(function(e){
	e.preventDefault();
	if ($('#thirdGrocer1').hasClass('active')) {
		$('#thirdGrocer1').removeClass('active').hide();
	

	}else{
		$('#thirdGrocer1').addClass('active').show();
		$('#thirdGrocer').hide();
		$('#thirdGrocer2').hide();
		$('#thirdGrocer3').hide();
		$('#thirdGrocer4').hide();
		$('#thirdGrocer5').hide();
		$('#thirdGrocer6').hide();
		$('#thirdGrocer7').hide();
		$('#thirdGrocer8').hide();
		$('#thirdGrocer9').hide();
		$('#thirdGrocer10').hide();
		$('#thirdGrocer11').hide();
		$('#thirdGrocer12').hide();
		
	}
})
$('#thirdGrocerBtn2').click(function(e){
	e.preventDefault();
	if ($('#thirdGrocer2').hasClass('active')) {
		$('#thirdGrocer2').removeClass('active').hide();
	}else{
		$('#thirdGrocer2').addClass('active').show();
		$('#thirdGrocer').hide();
		$('#thirdGrocer1').hide();
		$('#thirdGrocer3').hide();
		$('#thirdGrocer4').hide();
		$('#thirdGrocer5').hide();
		$('#thirdGrocer6').hide();
		$('#thirdGrocer7').hide();
		$('#thirdGrocer8').hide();
		$('#thirdGrocer9').hide();
		$('#thirdGrocer10').hide();
		$('#thirdGrocer11').hide();
		$('#thirdGrocer12').hide();
	}
})
$('#thirdGrocerBtn3').click(function(e){
	e.preventDefault();
	if ($('#thirdGrocer3').hasClass('active')) {
		$('#thirdGrocer3').removeClass('active').hide();
	}else{
		$('#thirdGrocer3').addClass('active').show();
		$('#thirdGrocer').hide();
		$('#thirdGrocer1').hide();
		$('#thirdGrocer2').hide();
		$('#thirdGrocer4').hide();
		$('#thirdGrocer5').hide();
		$('#thirdGrocer6').hide();
		$('#thirdGrocer7').hide();
		$('#thirdGrocer8').hide();
		$('#thirdGrocer9').hide();
		$('#thirdGrocer10').hide();
		$('#thirdGrocer11').hide();
		$('#thirdGrocer12').hide();
	}
})
$('#thirdGrocerBtn4').click(function(e){
	e.preventDefault();
	if ($('#thirdGrocer4').hasClass('active')) {
		$('#thirdGrocer4').removeClass('active').hide();
	}else{
		$('#thirdGrocer4').addClass('active').show();
		$('#thirdGrocer').hide();
		$('#thirdGrocer1').hide();
		$('#thirdGrocer2').hide();
		$('#thirdGrocer3').hide();
		$('#thirdGrocer5').hide();
		$('#thirdGrocer6').hide();
		$('#thirdGrocer7').hide();
		$('#thirdGrocer8').hide();
		$('#thirdGrocer9').hide();
		$('#thirdGrocer10').hide();
		$('#thirdGrocer11').hide();
		$('#thirdGrocer12').hide();
	}
})
$('#thirdGrocerBtn5').click(function(e){
	e.preventDefault();
	if ($('#thirdGrocer5').hasClass('active')) {
		$('#thirdGrocer5').removeClass('active').hide();
	}else{
		$('#thirdGrocer5').addClass('active').show();
		$('#thirdGrocer').hide();
		$('#thirdGrocer1').hide();
		$('#thirdGrocer2').hide();
		$('#thirdGrocer3').hide();
		$('#thirdGrocer4').hide();
		$('#thirdGrocer6').hide();
		$('#thirdGrocer7').hide();
		$('#thirdGrocer8').hide();
		$('#thirdGrocer9').hide();
		$('#thirdGrocer10').hide();
		$('#thirdGrocer11').hide();
		$('#thirdGrocer12').hide();
	}
})
$('#thirdGrocerBtn6').click(function(e){
	e.preventDefault();
	if ($('#thirdGrocer6').hasClass('active')) {
		$('#thirdGrocer6').removeClass('active').hide();
	}else{
		$('#thirdGrocer6').addClass('active').show();
		$('#thirdGrocer').hide();
		$('#thirdGrocer1').hide();
		$('#thirdGrocer2').hide();
		$('#thirdGrocer3').hide();
		$('#thirdGrocer4').hide();
		$('#thirdGrocer5').hide();
		$('#thirdGrocer7').hide();
		$('#thirdGrocer8').hide();
		$('#thirdGrocer9').hide();
		$('#thirdGrocer10').hide();
		$('#thirdGrocer11').hide();
		$('#thirdGrocer12').hide();
	}
})
$('#thirdGrocerBtn7').click(function(e){
	e.preventDefault();
	if ($('#thirdGrocer7').hasClass('active')) {
		$('#thirdGrocer7').removeClass('active').hide();
	}else{
		$('#thirdGrocer7').addClass('active').show();
		$('#thirdGrocer').hide();
		$('#thirdGrocer1').hide();
		$('#thirdGrocer2').hide();
		$('#thirdGrocer3').hide();
		$('#thirdGrocer4').hide();
		$('#thirdGrocer5').hide();
		$('#thirdGrocer6').hide();
		$('#thirdGrocer8').hide();
		$('#thirdGrocer9').hide();
		$('#thirdGrocer10').hide();
		$('#thirdGrocer11').hide();
		$('#thirdGrocer12').hide();
	}
})
$('#thirdGrocerBtn8').click(function(e){
	e.preventDefault();
	if ($('#thirdGrocer8').hasClass('active')) {
		$('#thirdGrocer8').removeClass('active').hide();
	}else{
		$('#thirdGrocer8').addClass('active').show();
		$('#thirdGrocer').hide();
		$('#thirdGrocer1').hide();
		$('#thirdGrocer2').hide();
		$('#thirdGrocer3').hide();
		$('#thirdGrocer4').hide();
		$('#thirdGrocer5').hide();
		$('#thirdGrocer6').hide();
		$('#thirdGrocer7').hide();
		$('#thirdGrocer9').hide();
		$('#thirdGrocer10').hide();
		$('#thirdGrocer11').hide();
		$('#thirdGrocer12').hide();
	}
})
$('#thirdGrocerBtn9').click(function(e){
	e.preventDefault();
	if ($('#thirdGrocer9').hasClass('active')) {
		$('#thirdGrocer9').removeClass('active').hide();
	}else{
		$('#thirdGrocer9').addClass('active').show();
		$('#thirdGrocer').hide();
		$('#thirdGrocer1').hide();
		$('#thirdGrocer2').hide();
		$('#thirdGrocer3').hide();
		$('#thirdGrocer4').hide();
		$('#thirdGrocer5').hide();
		$('#thirdGrocer6').hide();
		$('#thirdGrocer7').hide();
		$('#thirdGrocer8').hide();
		$('#thirdGrocer10').hide();
		$('#thirdGrocer11').hide();
		$('#thirdGrocer12').hide();
	}
})
$('#thirdGrocerBtn10').click(function(e){
	e.preventDefault();
	if ($('#thirdGrocer10').hasClass('active')) {
		$('#thirdGrocer10').removeClass('active').hide();
	}else{
		$('#thirdGrocer10').addClass('active').show();
		$('#thirdGrocer').hide();
		$('#thirdGrocer1').hide();
		$('#thirdGrocer2').hide();
		$('#thirdGrocer3').hide();
		$('#thirdGrocer4').hide();
		$('#thirdGrocer5').hide();
		$('#thirdGrocer6').hide();
		$('#thirdGrocer7').hide();
		$('#thirdGrocer8').hide();
		$('#thirdGrocer9').hide();
		$('#thirdGrocer11').hide();
		$('#thirdGrocer12').hide();
	}
})
$('#thirdGrocerBtn11').click(function(e){
	e.preventDefault();
	if ($('#thirdGrocer11').hasClass('active')) {
		$('#thirdGrocer11').removeClass('active').hide();
	}else{
		$('#thirdGrocer11').addClass('active').show();
		$('#thirdGrocer').hide();
		$('#thirdGrocer1').hide();
		$('#thirdGrocer2').hide();
		$('#thirdGrocer3').hide();
		$('#thirdGrocer4').hide();
		$('#thirdGrocer5').hide();
		$('#thirdGrocer6').hide();
		$('#thirdGrocer7').hide();
		$('#thirdGrocer8').hide();
		$('#thirdGrocer9').hide();
		$('#thirdGrocer10').hide();
		$('#thirdGrocer12').hide();
	}
})
$('#thirdGrocerBtn12').click(function(e){
	e.preventDefault();
	if ($('#thirdGrocer12').hasClass('active')) {
		$('#thirdGrocer12').removeClass('active').hide();
	}else{
		$('#thirdGrocer12').addClass('active').show();
		$('#thirdGrocer').hide();
		$('#thirdGrocer1').hide();
		$('#thirdGrocer2').hide();
		$('#thirdGrocer3').hide();
		$('#thirdGrocer4').hide();
		$('#thirdGrocer5').hide();
		$('#thirdGrocer6').hide();
		$('#thirdGrocer7').hide();
		$('#thirdGrocer8').hide();
		$('#thirdGrocer9').hide();
		$('#thirdGrocer10').hide();
		$('#thirdGrocer11').hide();
	}
})



$('#cook').click(function(e){
	e.preventDefault();
	if($(this).hasClass('active')){
		$(this).removeClass('active');
		$('#secondCook').hide();
	}else{
		$(this).addClass('active');
		$('#secondCook').show();
	}
})
$('#thirdCookBtn').click(function(e){
	e.preventDefault();
	if ($('#thirdCook').hasClass('active')) {
		$('#thirdCook').removeClass('active').hide();
	}else{
		$('#thirdCook').addClass('active').show();
	}
})

$('#eats').click(function(e){
	e.preventDefault();
	if($(this).hasClass('active')){
		$(this).removeClass('active');
		$('#secondEats').hide();
	}else{
		$(this).addClass('active');
		$('#secondEats').show();
	}
})
$('#thirdEatsBtn').click(function(e){
	e.preventDefault();
	if ($('#thirdEats').hasClass('active')) {
		$('#thirdEats').removeClass('active').hide();
	}else{
		$('#thirdEats').addClass('active').show();
	}
})

$('#showSideDropdown').click(function(e){
	e.preventDefault();
	$('#sideDropdown').slideToggle();
	$(this).toggleClass('text-red-bold');
})


$('.add-to-cart-btn').click(function(){
	$(this).css('display', 'none');
	$(this).next().css('display','block');
})

$('#redeem').click(function(){
	$('.redeem-field').slideToggle('fast');
})

$('.ada-lovenya').click(function(){
	$(this).toggleClass('grayscale');
})

$('.close-button').click(function(){
	$('#popupSignin').toggle();
	$('#openSIGNIN').toggleClass('text-red');
})


$('.close-button-cart').click(function(){
	$('#popupChart').toggle();
	$('#openCART').toggleClass('text-red');
})



$('.close-button-heart').click(function(){
	$('#popupHeart').toggle();
	$('#openHeart').toggleClass('text-red');
})

$('#openSIGNIN').click(function(){
	$('#popupSignin').toggle();
	$('#openSIGNIN').toggleClass('text-red');
	$('#popupChart').hide();
	$('#popupHeart').hide();
})

$('#openCART').click(function(){
	$('#popupChart').toggle();
	$('#openCART').toggleClass('text-red');
	$('#popupSignin').hide();
	$('#popupHeart').hide();
})
$('#openHeart').click(function(){
	$('#popupHeart').toggle();
	$('#openHeart').toggleClass('text-red');
	$('#popupSignin').hide();
	$('#popupChart').hide();
})

$('#forgotPassword').click(function(){
	$('#popupSignin').toggle();
	$('#openSIGNIN').toggleClass('text-red');
})

$('#openAccount').click(function(){
	$('#popupOpenAccount').toggle();
	$('#openAccount').toggleClass('text-red');
})

$('#addReview').click(function(){
	$('.panel-content-box .form-box').css('margin-top','5px');
	$('.panel-content-box .form-box p').before(
		"<textarea rows=5 placeholder='(write your review here)'></textarea><input type='text'  placeholder='(Reviews Headline) e.g The Best Carrot Ever'/><button type='button' class='button btn-aneh float-right'>SUBMIT</button></div>"
		);
	$(this).remove();
	$('p.shr').remove();
})

$('#toBenefits').click(function(e){
	e.preventDefault();
	$(this).addClass('line-active');
	$('#toHowitworks').removeClass('line-active');
	$('#toSignup').removeClass('line-active');
	$('html, body').animate({
		scrollTop: $('#benefits').offset().top
	}, 1000);
})
$('#toHowitworks').click(function(e){
	e.preventDefault();
	$(this).addClass('line-active');
	$('#toSignup').removeClass('line-active');
	$('#toBenefits').removeClass('line-active');
	$('html, body').animate({
		scrollTop: $('#howitworks').offset().top
	}, 1000);
})
$('#toSignup').click(function(e){
	e.preventDefault();
	$(this).addClass('line-active');
	$('#toBenefits').removeClass('line-active');
	$('#toHowitworks').removeClass('line-active');
	$('html, body').animate({
		scrollTop: $('#signup').offset().top
	}, 1000);
})
$('#toHowitworkstwo').click(function(e){
	e.preventDefault();
	$(this).addClass('line-active');
	$('#toClaimForm').removeClass('line-active');
	$('html, body').animate({
		scrollTop: $('#howitworks').offset().top
	}, 1000);
})
$('#toClaimForm').click(function(e){
	e.preventDefault();
	$(this).addClass('line-active');
	$('#toHowitworkstwo').removeClass('line-active');
	$('html, body').animate({
		scrollTop: $('#claimform').offset().top
	}, 1000);
})



$('#askKirin').click(function(){
	$(this).before(
		"<textarea rows=5 ></textarea><button type='button' class='button btn-aneh'>SUBMIT</button></div>"
		);
	$(this).remove();
})


	function sliderRecipe(){
		console.log('tes');
		$('.slide-bigimage').slick({
		  slidesToShow: 1,
		  slidesToScroll: 1,
		  arrows: false,
		  fade: true,
		  asNavFor: '.slider-nav'
		});
		$('.slider-desc').slick({
		  slidesToShow: 1	,
		  slidesToScroll: 1,
		  asNavFor: '.slider-for',
		  dots: true,
		  centerMode: true,
		  focusOnSelect: true
		});
	}
	sliderRecipe();

// comment cause impact to another page
// $('a[href*="#"]:not([href="#"])').click(function() {
//   if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
//     var target = $(this.hash);
//     target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
//     if (target.length) {
//       $('html, body').animate({
//         scrollTop: target.offset().top
//       }, 1000);
//       return false;
//     }
//   }
// });