$(document).foundation();

$(document).ready(function(){
	$('.remove-item-btn').click(removeItem);
	function removeItem(){
		$(this).parents('tr').first().remove();
	}

	$('.plus').click(plus);
	$('.minus').click(minus);

	$('.independent .plus').click(plus1);
	$('.independent .minus').click(minus1);
	$('.makin-aneh .independent .minus').click(minus2);

	function plus(){
		var cix = $(this).parents('tr').first().find('.total').val();
		cix++;
		$(this).parents('tr').first().find('.total').val(cix);
	}
	function minus(){
		var cix = $(this).parents('tr').first().find('.total').val();
		cix--;
		if (cix>=0) {
			$(this).parents('tr').first().find('.total').val(cix);
		}else if(cix<0){
			alert('Error');
		}
		
	}

	function plus1(){
		var cox = $(this).parents('.independent').find('.totalx').val();
		cox++;
		$(this).parents('.independent').find('.totalx').val(cox);
	}
	function minus1(){
		var cox = $(this).parents('.independent').find('.totalx').val();
		cox--;
		if (cox>=0) {
			$(this).parents('.independent').find('.totalx').val(cox);
		}else{
			alert('Quantity cannot less than 0 ');
		}
		
	}
	function minus2(){
		var cox = $(this).parents('.independent').find('.totalx').val();
		cox--;
		if (cox>=0) {
			$(this).parents('.independent').find('.totalx').val(cox);
		}else{
			$(this).parents('.item-box').find('.add-to-cart-btn').css('display', 'block');
			$(this).parents('.item-box').find('.add-to-cart-btn').next().css('display','none');
				// $('.add-to-cart-btn').css('display', 'block');
				// $('.add-to-cart-btn').next().css('display','none');
				$(this).parents('.independent').find('.totalx').val(1);
			
		}
		
	}
		

})










$(function() {
	function maskImgs() {
		//$('.img-wrapper img').imagesLoaded({}, function() {
		$.each($('.img-wrapper img'), function(index, img) {
			var src = $(img).attr('src');
			var parent = $(img).parent();
			parent
				.css('background', 'url(' + src + ') no-repeat center center')
				.css('background-size', 'cover');
			$(img).remove();
		});
		//});
	}

	var preview = {
		init: function() {
			preview.setPreviewImg();
			preview.listenInput();
		},
		setPreviewImg: function(fileInput) {
			var path = $(fileInput).val();
			var uploadText = $(fileInput).siblings('.file-upload-text');

			if (!path) {
				$(uploadText).val('');
			} else {
				path = path.replace(/^C:\\fakepath\\/, "");
				$(uploadText).val(path);

				preview.showPreview(fileInput, path, uploadText);
			}
		},
		showPreview: function(fileInput, path, uploadText) {
			var file = $(fileInput)[0].files;

			if (file && file[0]) {
				var reader = new FileReader();

				reader.onload = function(e) {
					var previewImg = $(fileInput).parents('.file-upload-wrapper').siblings('.preview');
					var img = $(previewImg).find('img');

					if (img.length == 0) {
						$(previewImg).html('<img src="' + e.target.result + '" alt=""/>');
					} else {
						img.attr('src', e.target.result);
					}

					uploadText.val(path);
					maskImgs();
				}

				reader.onloadstart = function() {
					$(uploadText).val('uploading..');
				}

				reader.readAsDataURL(file[0]);
			}
		},
		listenInput: function() {
			$('.file-upload-native').on('change', function() {
				if(this.files[0].size > 2000000) {
                    alert('The maximum image upload is 2MB. Please resize your current image and try upload again');
                    if(this.value){
                        try{
                            this.value = ''; //for IE11, latest Chrome/Firefox/Opera...
                        }catch(err){ }
                        if(this.value){ //for IE5 ~ IE10
                            var form = document.createElement('form'),
                                parentNode = this.parentNode, ref = this.nextSibling;
                            form.appendChild(this);
                            form.reset();
                            parentNode.insertBefore(this,ref);
                        }
                    }

                    console.log(this.value);
				} else {
                    preview.setPreviewImg(this);
				}

			});
		}
	};
	preview.init();
});

$('#grocer').click(function(e){
	e.preventDefault();
	if($(this).hasClass('active')){
		$(this).removeClass('active');
		$('#secondGrocer').hide();
	}else{
		$(this).addClass('active');
		$('#secondGrocer').show();
	}
})
$('#thirdGrocerBtn').click(function(e){
	e.preventDefault();
	if ($('#thirdGrocer').hasClass('active')) {
		$('#thirdGrocer').removeClass('active').hide();
	}else{
		$('#thirdGrocer').addClass('active').show();
		$('#thirdGrocer1').hide();
		$('#thirdGrocer2').hide();
		$('#thirdGrocer3').hide();
		$('#thirdGrocer4').hide();
		$('#thirdGrocer5').hide();
		$('#thirdGrocer6').hide();
		$('#thirdGrocer7').hide();
		$('#thirdGrocer8').hide();
		$('#thirdGrocer9').hide();
		$('#thirdGrocer10').hide();
		$('#thirdGrocer11').hide();
		$('#thirdGrocer12').hide();
	}
})

$('#thirdGrocerBtn1').click(function(e){
	e.preventDefault();
	if ($('#thirdGrocer1').hasClass('active')) {
		$('#thirdGrocer1').removeClass('active').hide();
        $('.second-level-category').hide();
        $('.second-lv').css('width','220px');

	}else{
        $('.second-level-category').show();
        $('.second-lv').css('width','420px');
		$('#thirdGrocer1').addClass('active').show();
		$('#thirdGrocer').hide();
		$('#thirdGrocer2').hide();
		$('#thirdGrocer3').hide();
		$('#thirdGrocer4').hide();
		$('#thirdGrocer5').hide();
		$('#thirdGrocer6').hide();
		$('#thirdGrocer7').hide();
		$('#thirdGrocer8').hide();
		$('#thirdGrocer9').hide();
		$('#thirdGrocer10').hide();
		$('#thirdGrocer11').hide();
		$('#thirdGrocer12').hide();
		
	}
})
$('#thirdGrocerBtn2').click(function(e){
	e.preventDefault();
	if ($('#thirdGrocer2').hasClass('active')) {
		$('#thirdGrocer2').removeClass('active').hide();
        $('.second-level-category').hide();
        $('.second-lv').css('width','220px');
	}else{
        $('.second-level-category').show();
        $('.second-lv').css('width','420px');
		$('#thirdGrocer2').addClass('active').show();
		$('#thirdGrocer').hide();
		$('#thirdGrocer1').hide();
		$('#thirdGrocer3').hide();
		$('#thirdGrocer4').hide();
		$('#thirdGrocer5').hide();
		$('#thirdGrocer6').hide();
		$('#thirdGrocer7').hide();
		$('#thirdGrocer8').hide();
		$('#thirdGrocer9').hide();
		$('#thirdGrocer10').hide();
		$('#thirdGrocer11').hide();
		$('#thirdGrocer12').hide();
	}
})
$('#thirdGrocerBtn3').click(function(e){
	e.preventDefault();
	if ($('#thirdGrocer3').hasClass('active')) {
		$('#thirdGrocer3').removeClass('active').hide();
        $('.second-level-category').hide();
        $('.second-lv').css('width','220px');
	}else{
        $('.second-level-category').show();
        $('.second-lv').css('width','420px');
		$('#thirdGrocer3').addClass('active').show();
		$('#thirdGrocer').hide();
		$('#thirdGrocer1').hide();
		$('#thirdGrocer2').hide();
		$('#thirdGrocer4').hide();
		$('#thirdGrocer5').hide();
		$('#thirdGrocer6').hide();
		$('#thirdGrocer7').hide();
		$('#thirdGrocer8').hide();
		$('#thirdGrocer9').hide();
		$('#thirdGrocer10').hide();
		$('#thirdGrocer11').hide();
		$('#thirdGrocer12').hide();
	}
})
$('#thirdGrocerBtn4').click(function(e){
	e.preventDefault();
	if ($('#thirdGrocer4').hasClass('active')) {
		$('#thirdGrocer4').removeClass('active').hide();
        $('.second-level-category').hide();
        $('.second-lv').css('width','220px');
	}else{
        $('.second-level-category').show();
        $('.second-lv').css('width','420px');
		$('#thirdGrocer4').addClass('active').show();
		$('#thirdGrocer').hide();
		$('#thirdGrocer1').hide();
		$('#thirdGrocer2').hide();
		$('#thirdGrocer3').hide();
		$('#thirdGrocer5').hide();
		$('#thirdGrocer6').hide();
		$('#thirdGrocer7').hide();
		$('#thirdGrocer8').hide();
		$('#thirdGrocer9').hide();
		$('#thirdGrocer10').hide();
		$('#thirdGrocer11').hide();
		$('#thirdGrocer12').hide();
	}
})
$('#thirdGrocerBtn5').click(function(e){
	e.preventDefault();
	if ($('#thirdGrocer5').hasClass('active')) {
		$('#thirdGrocer5').removeClass('active').hide();
        $('.second-level-category').hide();
        $('.second-lv').css('width','220px');
	}else{
        $('.second-level-category').show();
        $('.second-lv').css('width','420px');
		$('#thirdGrocer5').addClass('active').show();
		$('#thirdGrocer').hide();
		$('#thirdGrocer1').hide();
		$('#thirdGrocer2').hide();
		$('#thirdGrocer3').hide();
		$('#thirdGrocer4').hide();
		$('#thirdGrocer6').hide();
		$('#thirdGrocer7').hide();
		$('#thirdGrocer8').hide();
		$('#thirdGrocer9').hide();
		$('#thirdGrocer10').hide();
		$('#thirdGrocer11').hide();
		$('#thirdGrocer12').hide();
	}
})
$('#thirdGrocerBtn6').click(function(e){
	e.preventDefault();
	if ($('#thirdGrocer6').hasClass('active')) {
		$('#thirdGrocer6').removeClass('active').hide();
        $('.second-level-category').hide();
        $('.second-lv').css('width','220px');
	}else{
        $('.second-level-category').show();
        $('.second-lv').css('width','420px');
		$('#thirdGrocer6').addClass('active').show();
		$('#thirdGrocer').hide();
		$('#thirdGrocer1').hide();
		$('#thirdGrocer2').hide();
		$('#thirdGrocer3').hide();
		$('#thirdGrocer4').hide();
		$('#thirdGrocer5').hide();
		$('#thirdGrocer7').hide();
		$('#thirdGrocer8').hide();
		$('#thirdGrocer9').hide();
		$('#thirdGrocer10').hide();
		$('#thirdGrocer11').hide();
		$('#thirdGrocer12').hide();
	}
})
$('#thirdGrocerBtn7').click(function(e){
	e.preventDefault();
	if ($('#thirdGrocer7').hasClass('active')) {
		$('#thirdGrocer7').removeClass('active').hide();
        $('.second-level-category').hide();
        $('.second-lv').css('width','220px');
	}else{
        $('.second-level-category').show();
        $('.second-lv').css('width','420px');
		$('#thirdGrocer7').addClass('active').show();
		$('#thirdGrocer').hide();
		$('#thirdGrocer1').hide();
		$('#thirdGrocer2').hide();
		$('#thirdGrocer3').hide();
		$('#thirdGrocer4').hide();
		$('#thirdGrocer5').hide();
		$('#thirdGrocer6').hide();
		$('#thirdGrocer8').hide();
		$('#thirdGrocer9').hide();
		$('#thirdGrocer10').hide();
		$('#thirdGrocer11').hide();
		$('#thirdGrocer12').hide();
	}
})
$('#thirdGrocerBtn8').click(function(e){
	e.preventDefault();
	if ($('#thirdGrocer8').hasClass('active')) {
		$('#thirdGrocer8').removeClass('active').hide();
        $('.second-level-category').hide();
        $('.second-lv').css('width','220px');
	}else{
        $('.second-level-category').show();
        $('.second-lv').css('width','420px');
		$('#thirdGrocer8').addClass('active').show();
		$('#thirdGrocer').hide();
		$('#thirdGrocer1').hide();
		$('#thirdGrocer2').hide();
		$('#thirdGrocer3').hide();
		$('#thirdGrocer4').hide();
		$('#thirdGrocer5').hide();
		$('#thirdGrocer6').hide();
		$('#thirdGrocer7').hide();
		$('#thirdGrocer9').hide();
		$('#thirdGrocer10').hide();
		$('#thirdGrocer11').hide();
		$('#thirdGrocer12').hide();
	}
})
$('#thirdGrocerBtn9').click(function(e){
	e.preventDefault();
	if ($('#thirdGrocer9').hasClass('active')) {
		$('#thirdGrocer9').removeClass('active').hide();
        $('.second-level-category').hide();
        $('.second-lv').css('width','220px');
	}else{
        $('.second-level-category').show();
        $('.second-lv').css('width','420px');
		$('#thirdGrocer9').addClass('active').show();
		$('#thirdGrocer').hide();
		$('#thirdGrocer1').hide();
		$('#thirdGrocer2').hide();
		$('#thirdGrocer3').hide();
		$('#thirdGrocer4').hide();
		$('#thirdGrocer5').hide();
		$('#thirdGrocer6').hide();
		$('#thirdGrocer7').hide();
		$('#thirdGrocer8').hide();
		$('#thirdGrocer10').hide();
		$('#thirdGrocer11').hide();
		$('#thirdGrocer12').hide();
	}
})
$('#thirdGrocerBtn10').click(function(e){
	e.preventDefault();
	if ($('#thirdGrocer10').hasClass('active')) {
		$('#thirdGrocer10').removeClass('active').hide();
        $('.second-level-category').hide();
        $('.second-lv').css('width','220px');
	}else{
        $('.second-level-category').show();
        $('.second-lv').css('width','420px');
		$('#thirdGrocer10').addClass('active').show();
		$('#thirdGrocer').hide();
		$('#thirdGrocer1').hide();
		$('#thirdGrocer2').hide();
		$('#thirdGrocer3').hide();
		$('#thirdGrocer4').hide();
		$('#thirdGrocer5').hide();
		$('#thirdGrocer6').hide();
		$('#thirdGrocer7').hide();
		$('#thirdGrocer8').hide();
		$('#thirdGrocer9').hide();
		$('#thirdGrocer11').hide();
		$('#thirdGrocer12').hide();
	}
})
$('#thirdGrocerBtn11').click(function(e){
	e.preventDefault();
	if ($('#thirdGrocer11').hasClass('active')) {
		$('#thirdGrocer11').removeClass('active').hide();
        $('.second-level-category').hide();
        $('.second-lv').css('width','220px');
	}else{
        $('.second-level-category').show();
        $('.second-lv').css('width','420px');
		$('#thirdGrocer11').addClass('active').show();
		$('#thirdGrocer').hide();
		$('#thirdGrocer1').hide();
		$('#thirdGrocer2').hide();
		$('#thirdGrocer3').hide();
		$('#thirdGrocer4').hide();
		$('#thirdGrocer5').hide();
		$('#thirdGrocer6').hide();
		$('#thirdGrocer7').hide();
		$('#thirdGrocer8').hide();
		$('#thirdGrocer9').hide();
		$('#thirdGrocer10').hide();
		$('#thirdGrocer12').hide();
	}
})
$('#thirdGrocerBtn12').click(function(e){
	e.preventDefault();
	if ($('#thirdGrocer12').hasClass('active')) {
		$('#thirdGrocer12').removeClass('active').hide();
        $('.second-level-category').hide();
        $('.second-lv').css('width','220px');
	}else{
        $('.second-level-category').show();
        $('.second-lv').css('width','420px');
		$('#thirdGrocer12').addClass('active').show();
		$('#thirdGrocer').hide();
		$('#thirdGrocer1').hide();
		$('#thirdGrocer2').hide();
		$('#thirdGrocer3').hide();
		$('#thirdGrocer4').hide();
		$('#thirdGrocer5').hide();
		$('#thirdGrocer6').hide();
		$('#thirdGrocer7').hide();
		$('#thirdGrocer8').hide();
		$('#thirdGrocer9').hide();
		$('#thirdGrocer10').hide();
		$('#thirdGrocer11').hide();
	}
})

$('.search').focus(function(){
    $('#popupChart').hide();
    $('#popupSignin').hide();
    $('#popupHeart').hide();
    $('#popupOpenAccount').hide();
});

$('#cook').click(function(e){
	e.preventDefault();
	if($(this).hasClass('active')){
		$(this).removeClass('active');
		$('#secondCook').hide();
	}else{
		$(this).addClass('active');
		$('#secondCook').show();
	}
})
$('#thirdCookBtn').click(function(e){
	e.preventDefault();
	if ($('#thirdCook').hasClass('active')) {
		$('#thirdCook').removeClass('active').hide();
	}else{
		$('#thirdCook').addClass('active').show();
	}
})

$('#eats').click(function(e){
	e.preventDefault();
	if($(this).hasClass('active')){
		$(this).removeClass('active');
		$('#secondEats').hide();
	}else{
		$(this).addClass('active');
		$('#secondEats').show();
	}
})
$('#thirdEatsBtn').click(function(e){
	e.preventDefault();
	if ($('#thirdEats').hasClass('active')) {
		$('#thirdEats').removeClass('active').hide();
	}else{
		$('#thirdEats').addClass('active').show();
	}
})

$('#showSideDropdown0').click(function(e){
    $('#sideDropdown0').slideToggle();
    $(this).toggleClass('text-red-bold');
    window.location = this.getAttribute("href");
})
$('#showSideDropdown1').click(function(e){
	$('#sideDropdown1').slideToggle();
	$(this).toggleClass('text-red-bold');
	window.location = this.getAttribute("href");
})
$('#showSideDropdown2').click(function(e){
    $('#sideDropdown2').slideToggle();
    $(this).toggleClass('text-red-bold');
    window.location = this.getAttribute("href");
})
$('#showSideDropdown3').click(function(e){
    $('#sideDropdown3').slideToggle();
    $(this).toggleClass('text-red-bold');
    window.location = this.getAttribute("href");
})
$('#showSideDropdown4').click(function(e){
    $('#sideDropdown4').slideToggle();
    $(this).toggleClass('text-red-bold');
    window.location = this.getAttribute("href");
})
$('#showSideDropdown5').click(function(){
    $('#sideDropdown5').slideToggle();
    $(this).toggleClass('text-red-bold');
    window.location = this.getAttribute("href");
})
$('#showSideDropdown6').click(function(e){
    $('#sideDropdown6').slideToggle();
    $(this).toggleClass('text-red-bold');
    window.location = this.getAttribute("href");
})
$('#showSideDropdown7').click(function(e){
    $('#sideDropdown7').slideToggle();
    $(this).toggleClass('text-red-bold');
    window.location = this.getAttribute("href");
})
$('#showSideDropdown8').click(function(e){
    $('#sideDropdown8').slideToggle();
    $(this).toggleClass('text-red-bold');
    window.location = this.getAttribute("href");
})
$('#showSideDropdown9').click(function(e){
    $('#sideDropdown9').slideToggle();
    $(this).toggleClass('text-red-bold');
    window.location = this.getAttribute("href");
})
$('#showSideDropdown10').click(function(e){
    $('#sideDropdown10').slideToggle();
    $(this).toggleClass('text-red-bold');
    window.location = this.getAttribute("href");
})
$('#showSideDropdown11').click(function(e){
    $('#sideDropdown11').slideToggle();
    $(this).toggleClass('text-red-bold');
    window.location = this.getAttribute("href");
})
$('#showSideDropdown12').click(function(e){
    $('#sideDropdown12').slideToggle();
    $(this).toggleClass('text-red-bold');
    window.location = this.getAttribute("href");
})
$('#showSideDropdown13').click(function(e){
    $('#sideDropdown13').slideToggle();
    $(this).toggleClass('text-red-bold');
    window.location = this.getAttribute("href");
})

$('.add-to-cart-btn').click(function(){
	$(this).css('display', 'none');
	$(this).next().css('display','block');
})

$('.ada-lovenya').click(function(){
	$(this).toggleClass('grayscale');
})

$('.close-button').click(function(){
	$('#popupSignin').toggle();
	$('.openSIGNIN').toggleClass('text-red');
})


$('.close-button-cart').click(function(){
	$('#popupChart').toggle();
	$('#openCART').toggleClass('text-red active-now');
})



$('.close-button-heart').click(function(){
	$('#popupHeart').toggle();
	$('#openHeart').toggleClass('text-red');
})

$('.openSIGNIN').click(function(){
	$('#popupSignin').toggle();
	$('.openSIGNIN').toggleClass('text-red');
	$('#popupChart').hide();
	$('#popupHeart').hide();
})

$('#openCART').click(function(){
	$('#popupChart').toggle();
	$('#openCART').toggleClass('text-red active-now');
	$('#popupSignin').hide();
	$('#popupHeart').hide();
    $('#popupOpenAccount').hide();
})
$('#openHeart').click(function(){
	$('#popupHeart').toggle();
	$('#openHeart').toggleClass('text-red');
	$('#popupSignin').hide();
	$('#popupChart').hide();
})

$('#forgotPassword').click(function(){
	$('#popupSignin').toggle();
	$('.openSIGNIN').toggleClass('text-red');
})

$('#openAccount').click(function(){
	$('#popupOpenAccount').toggle();
	$('#openAccount').toggleClass('text-red');
    $('#popupSignin').hide();
    $('#popupHeart').hide();
    $('#popupChart').hide();
})

$('.addReview').click(function(){
	var container = $(this).parent();
	container.css('margin-top','5px');
	container.find('p').before(
        "<input type='text'  placeholder='(Reviews Headline) e.g The Best Carrot Ever' name='title'/><textarea rows=5 placeholder='(write your review here)' name='body'></textarea><button type='button' class='button btn-aneh float-right doSubmit'>SUBMIT</button><button type='button' class='btn-cancel-review button btn-aneh float-right' style='margin-right:15px;'>Cancel</button></div>"
	);
	$(this).hide();
	container.find('p.shr').hide();

});

$('body').delegate('.btn-cancel-review','click',function(){
	var parente = $(this).parent();
	parente.children('input[type="text"]').remove();
	parente.children('textarea').remove();
	parente.children('button.doSubmit').remove();
	parente.children('p').show();
	parente.children('button.addReview').show();
	$(this).remove();
});

$('#toBenefits').click(function(e){
	e.preventDefault();
	$(this).addClass('line-active');
	$('#toHowitworks').removeClass('line-active');
	$('#toSignup').removeClass('line-active');
	$('html, body').animate({
		scrollTop: $('#benefits').offset().top
	}, 1000);
})
$('#toHowitworks').click(function(e){
	e.preventDefault();
	$(this).addClass('line-active');
	$('#toSignup').removeClass('line-active');
	$('#toBenefits').removeClass('line-active');
	$('html, body').animate({
		scrollTop: $('#howitworks').offset().top
	}, 1000);
})
$('#toSignup').click(function(e){
	e.preventDefault();
	$(this).addClass('line-active');
	$('#toBenefits').removeClass('line-active');
	$('#toHowitworks').removeClass('line-active');
	$('html, body').animate({
		scrollTop: $('#signup').offset().top
	}, 1000);
})
$('#toHowitworkstwo').click(function(e){
	e.preventDefault();
	$(this).addClass('line-active');
	$('#toClaimForm').removeClass('line-active');
	$('html, body').animate({
		scrollTop: $('#howitworks').offset().top
	}, 1000);
})
$('#toClaimForm').click(function(e){
	e.preventDefault();
	$(this).addClass('line-active');
	$('#toHowitworkstwo').removeClass('line-active');
	$('html, body').animate({
		scrollTop: $('#claimform').offset().top
	}, 1000);
})

$('#askKirin').click(function(){
	$(this).before(
		"<form method='post' id='askKirinForm'><textarea rows=5 name='posttokirin'></textarea><button type='button' class='button btn-aneh submit-kirin'>SUBMIT</button>&nbsp;<button id='closeKirinForm' type='button' class='button btn-aneh' onclick='closeKirinBox();' style='background:red'>Close Form</button></div></form>"
		);
	$(this).hide();
})



/*
$('a[href*="#"]:not([href="#"])').click(function() {
  if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
    var target = $(this.hash);
    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
    if (target.length) {
      $('html, body').animate({
        scrollTop: target.offset().top
      }, 1000);
      return false;
    }
  }
});*/
