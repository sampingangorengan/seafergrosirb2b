var addItemToCart = function(id, sku, name, qty, ppi, price, image, metric) 
{

    $.ajax({
        type:'post',
        url: url('cart/new-add-item'),
        data: {
            id:id,
            sku:sku,
            name:name,
            image:image,
            metric:metric,
            price_per_item: ppi,
            qty:qty,
            price:price
        }
    }).done(function(result){
        if (result.code == '400') {
            alert(result.message);
        }
        
        drawCart();
        countCartItem();
        //console.log($('#openCART').hasClass('.active-now'));
        /*var has_cart_item = $('#openCART').find('.cart-notif');

        if (has_cart_item.length == 1) {
            var cur_value = parseInt($('.cart-notif').text());
            cur_value = cur_value + 1;
            $('.cart-notif').text(cur_value);
        } else {
            $('#openCART').append('<div class="cart-notif">1</div>');
        }*/
        $('#item_add_to_cart_wrap').html(drawItemButton(result.identifier));
        $('.control-cart-'+id).append(drawItemButton(result.identifier));
        $('.control-cart-'+id+' .list_add_to_cart').remove();
    })
}

var drawItemButton = function(data)
{
    var button = '<div class="makin-aneh" style="display: block;"> <div class="counter independent"><button class="minus decrease_cart_item_qty_out_pop">-</button> <input class="item_identifier" type="hidden" value="'+data+'" /> <input type="text" class="totalx item_qty" name="" value="1"> <button class="plus increase_cart_item_qty_out_pop">+</button> </div> </div>';
    return button;
}

var countCartItem = function(){
    $.ajax({
        type:'get',
        url: url('cart/cart-total-item')
    }).done(function(result){
        if(result == 0){
            $('.cart-notif').hide();
            $('#popupChart').attr('style','left:-5px;display:block;');
        } else {
            $('.cart-notif').text(result);
            $('.cart-notif').show();
            $('#popupChart').attr('style','left:-45px;display:block;');
        }
    });
}

var checkGroceryStock = function(id){
    var stock = null;

    var update_stock = function(newstock) {
        stock = newstock;

        return stock;
    }

    $.ajax({
        async:false,
        global:false,
        type:'get',
        url: url('cart/check-stock')+'?id='+id,
    }).done(function(result){
        update_stock(result);
        
    });

    return stock;
}

/*var drawCart = function() {
    console.log('drawing cart');
    $('.cart_item_container').html('');
    $('.cart_item_container').load(url('cart/my-cart'));
    drawCartDot();
}*/

var drawCartDot = function(){
    $.ajax({
        type:'get',
        url: url('cart/cart-total-item')
    }).done(function(result){
        if(result == 0){
            
            $('.cart-notif').hide();
        } else {
            $('.cart-notif').text(result);
            $('.cart-notif').show();
        }
    });
};

var drawCart = function() 
{
    $('.cart_item_container').html('');
    $('.cart_item_container').load(url('cart/my-cart'));
    
}

var count_total_price = function()
{
    $.ajax({
        url : url('cart/cart-total'),
        type : 'POST',
        success : function(response){
            $('#cart_popup_total').text(response);
        },
        error : function(response){
            console.log('ajax error');
        }
    })
}

$(document).ready(function() {
  // el = element
    $('.address-card form button').click(function(){
        var target = $(this).parent('form').attr('id');
        $('#modal-flash-new .text-red').html('Are you sure you want to delete this address? <br><br><a href="#" id="btn-cancel-delete-address">No</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" id="btn-ok-delete-address" target-form="'+target+'">Yes</a>');
        $('#modal-flash-new').foundation('open', 'open');
    });
    $('body').delegate('#btn-cancel-delete-address','click',function(){
        $('#modal-flash-new').foundation('close', 'close');
        return false;
    });
    $('body').delegate('#btn-ok-delete-address','click',function(){
        var target_form = $(this).attr('target-form');
        $('#'+target_form).submit();
    });
    $('input[name="search"].search').parent('form').submit(function(){
        if($(this).children('input[name="search"]').val().length <= 1){
            $('#modal-flash-new .text-red').text('Please insert minimum 2 character keyword.');
            $('#modal-flash-new').foundation('open', 'open');
            return false;
        } else {
            return true;
        }
    });
    drawCart();
    countCartItem();
    $('#modal-afterForgotPassword').click(function(){
        $(this).removeAttr('style');
        $(this).parent().removeAttr('style');
    });
    $('#modal-afterForgotPassword').parent().click(function(){
        $(this).removeAttr('style');
        $(this).children().removeAttr('style');
    })
    // Add to cart from list page
    $('.procat').on('click', '.list_add_to_cart', function() {
        var closest = $(this).closest(':has(div input)');

        var item_id = closest.find('.atc-initiate').find('.item_id').val();
        var item_name = closest.find('.atc-initiate').find('.item_name').val();
        var item_sku= closest.find('.atc-initiate').find('.item_sku').val();
        var item_image = closest.find('.atc-initiate').find('.item_image').val();
        var item_metric = closest.find('.atc-initiate').find('.item_metric').val();
        var item_price = closest.find('.atc-initiate').find('.item_price').val();
        var item_qty = 1;


        addItemToCart(item_id, item_sku, item_name, item_qty, item_price, item_price, item_image, item_metric);
    });

    $('.procat').on('click', '.increase_cart_item_qty_out_pop', function(){
        var container = $(this).parent();
        var quantity = parseInt(container.find('.item_qty').val());
        var identifier = container.find('.item_identifier').val();

        var idf = container.find('.item_identifier').val();
        var item_id = $('#'+idf+' .item_id').val();
        var quantity = $('#'+idf+' li .total').val();
        var input_qty = $('#'+idf+' li .total');
        var item_total = $('#'+idf+' .item_total');
        var stock = checkGroceryStock(idf);

        if(parseInt(quantity) > parseInt(stock)) {
            alert('Ooops we only have '+stock+' items in stock.');
            return false;
        }
        $.ajax({
            type:'post',
            url: url('cart/increase-qty'),
            data: {
                identifier: identifier,
            }
        }).done(function(result){
            $('.control-cart-'+item_id).find('.makin-aneh .item_qty').val(parseInt(quantity) + 1);
            input_qty.val(result.qty);
            item_total.text(result.total_price_item);
            count_total_price();
            // drawCart();
            countCartItem();
            container.find('.item_qty').val(result.qty);
        })

    });

    $('.procat').on('click', '.decrease_cart_item_qty_out_pop', function(){
        var container = $(this).parent();
        var quantity = parseInt(container.find('.item_qty').val());

        var idf = container.find('.item_identifier').val();
        var row = $('#'+idf);
        var item_id = $('#'+idf+' .item_id').val();
        var quantity = $('#'+idf+' li .total').val();
        var input_qty = $('#'+idf+' li .total');
        var item_total = $('#'+idf+' .item_total');
        
        container.find('.item_qty').val(quantity-1);
        $.ajax({
            type:'post',
            url: url('cart/decrease-qty'),
            data: {
                identifier: container.find('.item_identifier').val(),
            }
        }).done(function(result){
            $('.control-cart-'+item_id).find('.makin-aneh .item_qty').val(parseInt(quantity) - 1);
            input_qty.val(result.qty);
            item_total.text(result.total_price_item);
            count_total_price();
            if(result.qty == 0){
                row.remove();
                if(result.total_item == 0){
                    $('.cart_item_container').html(result.empty_cart);
                }
            }
            if (result.qty == 0) {
                console.log(container.parent().parent());
                container.parent().parent().append('<button class="button add-to-cart-btn list_add_to_cart" >ADD TO CART</button>');
                $('.makin-aneh').remove();
            }
            container.find('.item_qty').val(result.qty);
            // drawCart();
            countCartItem();
        })
    });

    $('#popupChart').on('click', '.decrease_cart_item_qty', function(){

        var row = $(this).parent().parent().parent().parent();
        var idf = $(this).parent().parent().parent().parent().attr('id');
        var item_id = $(this).parent().parent().parent().parent().find('.item_id').val();
        var quantity = $(this).parent().parent().find('li .total').val();
        var input_qty = $(this).parent().parent().find('li .total');
        var item_total = $(this).parent().parent().parent().parent().find('.item_total');

        $.ajax({
            type:'post',
            url: url('cart/decrease-qty'),
            data: {
                identifier: idf
            }
        }).done(function(response){
            $('.control-cart-'+item_id).find('.makin-aneh .item_qty').val(parseInt(quantity) - 1);
            input_qty.val(response.qty);
            item_total.text(response.total_price_item);
            count_total_price();
            if(response.qty == 0){
                row.remove();
                if(response.total_item == 0){
                    $('.cart_item_container').html(response.empty_cart);
                }
            }
            // drawCart();
            countCartItem();
        })

    });

    $('#popupChart').on('click', '.increase_cart_item_qty', function(){

        var idf = $(this).parent().parent().parent().parent().attr('id');
        var item_id = $(this).parent().parent().parent().parent().find('.item_id').val();
        var quantity = $(this).parent().parent().find('li .total').val();
        var input_qty = $(this).parent().parent().find('li .total');
        var item_total = $(this).parent().parent().parent().parent().find('.item_total');
        var stock = checkGroceryStock(idf);

        if(parseInt(quantity) > parseInt(stock)) {
            alert('Ooops we only have '+stock+' items in stock.');
            return false;
        }
        $.ajax({
            type:'post',
            url: url('cart/increase-qty'),
            data: {
                identifier: idf
            }
        }).done(function(response){
            $('.control-cart-'+item_id).find('.makin-aneh .item_qty').val(parseInt(quantity) + 1);
            input_qty.val(response.qty);
            item_total.text(response.total_price_item);
            count_total_price();
            // drawCart();
            countCartItem();
        })

    });

    $('.counter').on('click', '.decrease_cart_item_qty', function(){

        var idf = $(this).parent().parent().parent().attr('id');

        $.ajax({
            type:'post',
            url: url('cart/decrease-qty'),
            data: {
                identifier: idf
            }
        }).done(function(result){
            location.reload();
        })

    });

    $('.counter').on('click', '.increase_cart_item_qty', function(){

        var idf = $(this).parent().parent().parent().attr('id');
        var stock = checkGroceryStock(idf);

        if(parseInt(quantity) > parseInt(stock)) {
            alert('Ooops we only have '+stock+' items in stock.');
            return false;
        }

        $.ajax({
            type:'post',
            url: url('cart/increase-qty'),
            data: {
                identifier: idf
            }
        }).done(function(result){
            location.reload();
        })

    });

    $('.cpage').on('click', '.list_del_cart_item', function(){

        var idf = $(this).parent().parent().attr('id');

        $.ajax({
            type: 'get',
            url: url('cart/ajax-delete'),
            data: {
                id: idf
            }
        }).done(function(result){
            console.log(result)
            // Reload the page

            location.reload();

        })

    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
  
    var item_id = $('#item_id').val()
    var item_name = $('#item_name').val()


    // Add to cart from item page
    $('#item_add_to_cart').on('click', function() {
        var item_qty = $('#item_qty').val();
        var item_id = $('#item_id').val();
        var item_name = $('#item_name').val();
        var item_sku = $('#item_sku').val();
        var item_image = $('.item_image').val();
        var item_metric = $('.item_metric').val();
        var item_price = $('.item_price').val();

        addItemToCart(item_id, item_sku, item_name, item_qty, item_price, item_price, item_image, item_metric);
    });

    $('#item_add_to_cart_wrap').on('click', '.increase_cart_item_qty_out_pop', function(){
        var container = $(this).parent();
        var quantity = parseInt(container.find('.item_qty').val());
        var identifier = container.find('.item_identifier').val();

        var idf = container.find('.item_identifier').val();
        var item_id = $('#'+idf+' .item_id').val();
        var quantity = $('#'+idf+' li .total').val();
        var input_qty = $('#'+idf+' li .total');
        var item_total = $('#'+idf+' .item_total');
        var stock = checkGroceryStock(idf);

        if(parseInt(quantity) > parseInt(stock)) {
            alert('Ooops we only have '+stock+' items in stock.');
            return false;
        }
        $.ajax({
            type:'post',
            url: url('cart/increase-qty'),
            data: {
                identifier: identifier,
            }
        }).done(function(result){
            $('.control-cart-'+item_id).find('.makin-aneh .item_qty').val(parseInt(quantity) + 1);
            input_qty.val(result.qty);
            item_total.text(result.total_price_item);
            count_total_price();
            // drawCart();
            countCartItem();
            container.find('.item_qty').val(result.qty);
        })
    });

    $('#item_add_to_cart_wrap').on('click', '.decrease_cart_item_qty_out_pop', function(){
        var container = $(this).parent();
        var quantity = parseInt(container.find('.item_qty').val());

        var idf = container.find('.item_identifier').val();
        var row = $('#'+idf);
        var item_id = $('#'+idf+' .item_id').val();
        var quantity = $('#'+idf+' li .total').val();
        var input_qty = $('#'+idf+' li .total');
        var item_total = $('#'+idf+' .item_total');

        container.find('.item_qty').val(quantity-1);
        $.ajax({
            type:'post',
            url: url('cart/decrease-qty'),
            data: {
                identifier: container.find('.item_identifier').val(),
            }
        }).done(function(result){
            $('.control-cart-'+item_id).find('.makin-aneh .item_qty').val(parseInt(quantity) - 1);
            input_qty.val(result.qty);
            item_total.text(result.total_price_item);
            count_total_price();
            if(result.qty == 0){
                row.remove();
                if(result.total_item == 0){
                    $('.cart_item_container').html(result.empty_cart);
                }
            }
            if (result.qty == 0) {
                console.log(container.parent().parent());
                container.parent().parent().append('<button class="button btn-aneh" id="item_add_to_cart">ADD TO CART</button>');
                $('.makin-aneh').remove();
            }
            container.find('.item_qty').val(result.qty);
            // drawCart();
            countCartItem();
        })
    });



    $('#popupChart').on('click', '.list_del_cart_item_on_pop', function() {

        var item_container = $(this).parent().parent();
        var item_id = $(this).parent().parent().attr('id');
        var row = $(this).parent().parent();
        $.ajax({
            type: 'get',
            url: url('cart/ajax-delete'),
            data: {
                id: item_id
            }
        }).done(function(response){
            // Reload the page
            //if(result == 'removed') {
            row.remove();
            count_total_price();
            if(response.total_item == 0){
                $('.cart_item_container').html(response.empty_cart);
            }
            countCartItem();
            //}
        })
    })



  function matchPasswords() 
  {
    var msg = ($("input[name='signup_password_confirmation']").val() != $("input[name='signup_password']").val())
      ? "Passwords don't match"
      : ""
    $("input[name='signup_password_confirmation']").get(0).setCustomValidity(msg);
  }

  $("input[name='signup_password']").change(function() {
    matchPasswords()
  })

  $("input[name='signup_password_confirmation']").change(function() {
    matchPasswords()
  })

  $('#checkout_address_id').change(function() {
    $.ajax({
      type: 'post',
      url: url('checkout/ajax-change-address-id'),
      data: {
        address_id: $(this).val()
      }
    }).done(function(result){
      console.log(result);
      if (result.status === false) {
        alert("Bad address")
        location = url('checkout')
        return;
      }

      $('td#checkout_shipping_fee').text(result.shippingFee)
      $('td#checkout_total').text(result.total)
    })
  })

  $('.checkout_shipping_service_code').change(function(){
    if($('#checkout_address_id').val() == 0) {
        alert('Please select address');
        return false;
    }
    console.log($(this).val());
    $.ajax({
      type: 'post',
      /*url: url('checkout/ajax-change-shipping-service-code'),*/
      url: url('checkout/calculate-shipping-fare'),
      data: {
        address_id: $('#checkout_address_id').val(),
        shipping_type: $(this).val()
      },
        beforeSend: function() {

            $('#checkout_shipping_fee').html("<img src='http://seafermart.co.id/assets/images/ajax-loader.gif' />");
        }
    }).done(function(result){
        if(result.status == false){
            $('#modal-flash-new .text-red').text('Please pick your address first.');
            $('#modal-flash-new').foundation('open', 'open');
            $('input[name="shipping_service_code"]').removeAttr('checked');
            $('input[name="shipping_service_code"]').prop('checked',false);
            $('input[name="shipping_service_code"]').attr('checked',false);
        }
      $('td#checkout_shipping_fee').text(result.shippingFee);
      $('input[name="shipping_fee"]').val(result.shippingFee);
      $('td#checkout_total').text(result.total);
    })
  })

  /*$("#datepicker").datepicker({ dateFormat: 'yy-mm-dd' });*/

  $('#groceries_ask').click(function() {
    $(this).hide()
    $('form.groceries_question').show()
    $('textarea.groceries_question').focus()
  })

}) // End of doc ready


// Handling forgot password
$('.do-not-forget').click(function(e){
    e.preventDefault();
    $.ajax({
        type: 'post',
        url: url('password/email'),
        data: {
            email: $('.forget-me-not input').val()
        }
    }).done(function(result){

    });
});
$('form.forget-me-not').submit(function(e){
    $.ajax({
        type: 'post',
        url: url('password/email'),
        data: {
            email: $('.forget-me-not input').val()
        }
    }).done(function(result){
        $('#modal-forgotpassword').removeAttr('style');
        $('#modal-forgotpassword').parent().removeAttr('style');
        $('#modal-afterForgotPassword').attr('style','top: 70px; display: block;');
        $('#modal-afterForgotPassword').parent().attr('style','display:block;')
    });
    e.preventDefault();
})
