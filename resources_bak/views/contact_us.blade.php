@extends('layouts.newmaster')

@section('title', App\Page::createTitle('Contact Us'))

@section('content')
<div id="boxOfContent">
    <div class="row">
        <div class="small-12 columns absolute">
      <span class="text-grey equal margin-top-10">
        <a class="text-grey" href="{{ url('/') }}"><b>HOME</b></a> /
        <a class="text-grey" href="#"><b>CONTACT US</b></a>
      </span>
        </div>
    </div>
    <div class="contact-us-box" id="checkout">
        <div class="box-content">
            <div class="row">
                <div class="small-12 columns">
                    <h5 class="title title-margin">CONTACT US</h5>
                </div>
            </div>
            <div class="row">
                <div class="small-7 small-centered columns">
                    <center>
                        <div class="img-box">
                            <img src="{!! asset(null) !!}assets/images/img-contactus.png">
                        </div>
                        <br>
                        <h6 style="font-size: 13px;">HOW CAN WE HELP YOU?</h6>
                    </center>
                    <p style="font-size: 13px;">
                        In case your email has changed, please update it by signing in. If you have anti-spam software installed,
                        <br>
                        please make sure it does not block or filter emails sent from our domain (SeaferMart.co.id).
                        <br><br>
                        If you suspect you're still unable to receive our emails after changing the settings of those programs,
                        <br>
                        please contact your ISP (Internet Service Provider) to enable you to receive e-mails sent from our
                        <br>domain (SeaferMart.co.id).
                    </p>
                </div>
            </div>
        </div>
    </div>
    <br><br><br>
    <div class="box-content">
        <div class="row">
            <div class="small-7 small-centered">
                <div class="small-6 columns">
                    <h5 class="subtitle">REACH US HERE</h5><br>
                    <ul class="reach-us-here">
                        {{--<li>
                            <img src="{!! asset(null) !!}assets/images/ico-chat.png">
                            <span>LIVE CHAT</span>
                        </li>--}}
                        <li>
                            <img src="{!! asset(null) !!}assets/images/ico-phone.png">
                            <span>+62-21-6669-6285</span>
                        </li>
                        <li>
                            <img src="{!! asset(null) !!}assets/images/ico-line.png">
                            <span>@SeaferMart</span>
                        </li>
                        <li>
                            <img src="{!! asset(null) !!}assets/images/ico-messag2.png">
                            <span>hello@seafermart.co.id</span>
                        </li>
                        <li>
                            <img src="{!! asset(null) !!}assets/images/ico-location.png">
                            <span>PT. Seafermart Jaya Raya<br>
              Jl. Cumi Raya No.3, Muara baru<br>
              Jakarta Utara 144440, Indonesia<span>
                        </li>
                        <li>
                            <div class="gmap-box">
                                <div id="gmap-display" style="">
                                    <!-- <iframe frameborder="0" src="https://www.google.com/maps/embed/v1/place?q=Jakarta&key=AIzaSyAN0om9mFmy1QN6Wf54tXAowK4eT0ZUPrU"></iframe> -->
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="small-6 columns">
                    <div class="flash-message">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                            @if(Session::has('alert-' . $msg))

                                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                            @endif
                        @endforeach
                    </div>
                    <form method="post">
                    <h5 class="subtitle">INQUIRY FORM</h5><br>
                    <div class="form-box">
                        <div class="row">
                            <div class="small-4 columns">
                                <label for="middle-label" class="text-left middle no-padding-2">NAME*</label>
                            </div>
                            <div class="small-8 columns">
                                <input type="text"  placeholder="State your name" name="name" required="required">
                            </div>
                        </div>
                        <div class="row">
                            <div class="small-4 columns">
                                <label for="middle-label" class="text-left middle no-padding-2">EMAIL</label>
                            </div>
                            <div class="small-8 columns">
                                <input type="text"  placeholder="Type your email here" name="email" required="required">
                            </div>
                        </div>
                        <div class="row">
                            <div class="small-4 columns">
                                <label for="middle-label" class="text-left middle no-padding-2">CATEGORY</label>
                            </div>
                            <div class="small-8 columns">
                                <select class="short" name="category">
                                    <option value="orders">Orders</option>
                                    <option value="products">Products</option>
                                    <option value="cashback rewards">Cashback Rewards</option>
                                    <option value="delivery">Delivery</option>
                                    <option value="payment">Payment</option>
                                    <option value="troubleshooting">Troubleshooting</option>
                                    <option value="suggest product or seafer eats">Suggest a Product/Seafer Eats Seller</option>
                                    <option value="general inquiry">General Inquiry</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="small-4 columns">
                                <label for="middle-label" class="text-left middle no-padding-2">MESSAGE</label>
                            </div>
                            <div class="small-8 columns">
                                <textarea cols="3" rows="5" name="message" required="required"></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="small-4 columns">
                                &nbsp;
                            </div>
                            <div class="small-8 columns">
                                {{ csrf_field() }}
                                <button class="button float-right btn-aneh">SUBMIT</button>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection