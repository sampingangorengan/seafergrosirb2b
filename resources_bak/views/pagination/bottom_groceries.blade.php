
@if ($paginator->lastPage() > 1)
    <ul class="pagination text-grey">
        <li class="{{ ($paginator->currentPage() == 1) ? ' disabled' : '' }}">
            <a href="{{ $paginator->url(1).'&'.$query_string }}">Previous</a>
            {{--<a href="{{ $paginator->url(1) }}">Previous</a>--}}
        </li>

        @for ($i = 1; $i <= $paginator->lastPage(); $i++)

            @if(($i < 5) || ($i > ($paginator->lastPage() - 2)) )
                <li class="{{ ($paginator->currentPage() == $i) ? ' current' : '' }}">
                    <a href="{{ $paginator->url($i).'&'.$query_string }}"  style="line-height:23px">{{ $i }}</a>
                </li>
            @elseif($i == 5)
                <li class="{{ ($paginator->currentPage() == $i) ? ' current' : '' }}">
                    <a href="#">...</a>
                </li>
            @endif
        @endfor

        <li class="{{ ($paginator->currentPage() == $paginator->lastPage()) ? ' disabled' : '' }}">
            <a href="{{ $paginator->url($paginator->currentPage()+1).'&'.$query_string }}" >Next</a>
            {{--<a href="{{ $paginator->url($paginator->currentPage()+1) }}" >Next</a>--}}
        </li>
    </ul>
@endif
