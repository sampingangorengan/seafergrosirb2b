@extends('layouts.newmaster')

@section('title', App\Page::createTitle('About Us'))

@section('customcss')
    <link rel="stylesheet" type="text/css" href="{!! asset(null) !!}assets/css/custom.css">
@endsection

@section('content')
    <!-- Content Begin -->
    <div class="row">
        <div class="small-12 columns">
            <section>
                <div class="center">
                    <img src="{!! asset(null) !!}assets/images/404.png" style="width:auto;">
                    <p style="margin-top: 20px;">
                        The page you are looking for was<br>
                        <b>moved, renamed or might never existed.</b>
                    </p>
                    <a href="{{ url('/') }}"><button class="button primary btn-aneh">HOME</button></a>
                </div>
            </section>
        </div>
    </div>
@endsection