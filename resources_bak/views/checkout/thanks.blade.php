@extends('layouts.newmaster')

@section('content')

<div id="boxOfContent">
  <div class="box-content" id="confirm-box">
    <div class="row">
      <div class="small-9 columns small-centered">
        <div class="img-box">
          <img src="assets/images/img-popup.png">
        </div>
        <div class="subtitle">
          Thank you for shopping with <b>SEAFERMART</b>
        </div>
        <br/>
        <!-- <h4>Hi, {{ auth()->user()->name }}</h4> -->
        <p>{{ auth()->user()->name }}, the total amount you should transfer is <span class="text-red">{{ Money::display($order->grand_total) }}</span>. <br>
        Please note a unique code of <span class="text-red">{{ Money::display($unique_code) }}</span> has been added to the total amount to help us verify your payment transfer.</p>
        <!-- <p>Total amount you should transfer is <span class="text-red">{{ Money::display($order->grand_total) }}</span></p> -->
        <p>
          Please make your payment before:
          <b class="text-red">{{ LocalizedCarbon::now()->addHours(24) }}</b>
          Otherwise your orders will be automatically canceled.
        </p>
        <a href="#" class="htp">HOW TO PAY</a>
        <ul>
          <li>
            <span>Please make your payment to the following account: <b class="text-red inline">PT. Seafermart Jaya Raya – BCA 206 3232 327 / Mandiri 122 000 758 6848</b></span>
          </li>
          <li><span>Take a picture of your proof of payment and upload it to Confirm Payment form below.</span></li>
          <li><span>SeaferMart will confirm your payment within 24 hours.</span></li>
          <li><span>SeaferMart will notify you after the payment is received.</span></li>
        </ul>
        <button type="button" class="button btn-aneh gplus xyz"><a href="{{ url('orders/'.$order->id) }}">CONFIRM YOUR PAYMENT</a></button>
        <span class="or">Or</span>
        <button type="vutton" class="button btn-aneh xyz"><a href="{{ url() }}">BACK TO HOMEPAGE</a></button><br><br>
      </div>
    </div>
  </div>
</div>

@endsection