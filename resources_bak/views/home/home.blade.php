@extends('layouts.newmaster')

@section('content')
    <!-- Slider -->
    <div class="orbit orbit-custom" role="region" data-orbit>
      <ul class="orbit-container">
        <?php $i = 0 ?>
        @foreach (App\Models\Home\Slide::where('is_active', 1)->orderBy('order')->get() as $slide)
          <?php $class = '' ?>
          @if ($i == 0)
            <?php $class = 'is-active' ?>
          @endif
          <li class="{{ $class }} orbit-slide orbit-slide-custom">
            <a href="{{ $slide->link }}">
              <img class="orbit-image" src="{!! asset(null) !!}headlines/{{ $slide->file_name }}" alt="space">
              <figcaption class="orbit-caption">{{ $slide->caption }}</figcaption>
            </a>
          </li>

          <?php $i++ ?>
        @endforeach
      </ul>
      <nav class="orbit-bullets">
        <button class="is-active" data-slide="0">
          <span class="show-for-sr">First skide details.</span>
          <span class="show-for-sr">Current Slide</span>
        </button>

        @for ($i = 0; $i < App\Models\Home\Slide::where('is_active', 1)->count() - 1; $i++)
          <button data-slide="{{ $i + 1 }}"><span class="show-for-sr">Second slide details.</span></button> 
        @endfor
    </nav>
    </div>

    <!-- Main Content -->
    <div class="box-content">
      <div class="row">
        <div class="medium-3 columns">
          <div  data-equalizer="bar">
            <div class="side-card" data-equalizer-watch="bar">
              <div class="card grocer">
                <div class="card-content">
                  <div class="img-box">
                    <img src="{!! asset(null) !!}assets/images/img-grocer.png">
                  </div>
                  <p class="text-box">
                    A place to help you and your family pick and deliver the best quality of seafood right to your front doors.
                  </p>
                </div>
                <a href="{{ url('groceries?show=all') }}">
                  <button class="button">GO TO GROCER</button>
                </a>
              </div>
            </div>
            
            {{-- <div class="side-card" data-equalizer-watch="bar">
              <div class="card cook">
                <div class="card-content">
                  <div class="img-box">
                    <img src="{!! asset(null) !!}assets/images/img-cook.png">
                  </div>
                  <p class="text-box">
                  We create a community portal to house a melting pot of genius famous chefs, food bloggers, and skilled housewives who will share 
                  </p>
                </div>
                <button class="button">GO TO COOK</button>
              </div>
            </div>
            <div class="side-card" data-equalizer-watch="bar">
              <div class="card eats">
                <div class="card-content">
                  <div class="img-box">
                    <img src="{!! asset(null) !!}assets/images/img-eats.png">
                  </div>
                  <p class="text-box">
                  The ﬁrst ever food marketplace in Indonesia where we work together with local InstaFood sellers with their mouthwatering creations to 
                  </p>
                </div>
                <button class="button">GO TO EATS</button>
              </div>
            </div> --}}
          </div>
        </div>
        @if($groceries_body)
        <div class="medium-9 columns producthome">
            <div class=" panel">
                <div class="row" data-equalizer data-equalize-on="medium" id="test-eq">

                @foreach($groceries_body as $key=>$item)
                    <div class="medium-3 columns">
                        <a href="{{ url('groceries/'.$item->id.'/'.str_slug($item->name)) }}">
                            <div class="cube">
                                
                                @if( Groceries::getFirstImageUrl($item->id) != asset('assets/images/img-empty.png'))
                                <div class="img-detail">
                                    <img src="{{ Groceries::getFirstImageUrl($item->id) }}" alt="{{ $item->name }}">
                                </div>
                                @else
                                <div class="img-detail" style="min-height:190px">
                                    <img src="{{ Groceries::getFirstImageUrl($item->id) }}" alt="{{ $item->name }}">
                                </div>
                                @endif
                                <div class="detail-box bottom">
                                    <h6{{--  style="color:#353638" --}}>{{ $item->name }}</h6>
                                    <p></p>
                                    
                                    <span{{--  style="color:#353638" --}}>{{ Money::display($item->price) }}</span>
                                </div>
                            </div>   
                        </a>
                    </div>
                @endforeach

              
            
                </div>
            </div>
        </div>
        @endif

      </div>
    </div>

    <!-- Feature -->
    <div class="feature-box">
      <div class="box-content">
        <ul style="margin-left:3.25rem">
          <li><a href="#"><img src="{{ asset(null) }}assets/images/imgpsh_fullsize.png"></a></li>
          <li><a href="#"><img src="{{ asset(null) }}assets/images/imgpsh_fullsize-5.png"></a></li>
          <li><a href="#"><img src="{{ asset(null) }}assets/images/imgpsh_fullsize-3.png"></a></li>
          <li><a href="#"><img src="{{ asset(null) }}assets/images/imgpsh_fullsize-4.png"></a></li>
          <li><a href="#"><img src="{{ asset(null) }}assets/images/imgpsh_fullsize-2.png"></a></li>
        </ul>
      </div>
    </div>
@endsection