@extends('layouts.newmaster')

@section('content')

    <div id="godOfContent">
        <div class="box-content" id="checkout">
            <form method="POST">
                <div class="row">

                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="small-4 columns">
                        <h5 class="title-aneh">PAYMENT INFORMATION</h5>
                        <table>
                            <tbody>
                            <tr>
                                <td>
                                    <span class="sub-subtitle">BILLING ADDRESS</span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span style="font-weight: bold;">TITLE</span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <select name="sex">
                                        <option value="m">Mr.</option>
                                        <option value="fs">Ms.</option>
                                        <option value="f">Mrs.</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="required" style="font-weight: bold;">NAME</span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="text" class="placeholder-italic" name="user_first_name" placeholder="First Name" value="{{ old('first_name') }}" required/>
                                    <input type="text" name="user_last_name" class="placeholder-italic" placeholder="Last Name" value="{{ old('last_name') }}" required/>
                                </td>
                                <input type="hidden" name="email" class="placeholder-italic" placeholder="Email Address" value="{{ $user_email }}" required style="border: 1px solid #808184;height: 24px;"/>
                            </tr>
                            <tr>
                                <td>
                                    <span class="required" style="font-weight: bold;">PHONE NUMBER</span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="input-group">
                                        <span class="dilabelin">+62</span>
                                        <input  name="user_phone_number" class="pasangannya-label" type="text" placeholder="e.g., 8123xxx" style="height:24px;" required="required">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="required" style="font-weight: bold;">DATE OF BIRTH</span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="row">
                                        <div class="small-4 columns">
                                            <select name="dob_date" class="width-80" required>
                                                {{--<option>Date</option>--}}
                                                @for ($date = 1; $date <= 31; $date++)
                                                    <option value="{{ $date }}">{{ $date }}</option>
                                                @endfor
                                            </select>
                                        </div>
                                        <select name="dob_month" class="small-4 columns" required>
                                            {{--<option>Month</option>--}}
                                            <option value="1">January</option>
                                            <option value="2">February</option>
                                            <option value="3">March</option>
                                            <option value="4">April</option>
                                            <option value="5">May</option>
                                            <option value="6">June</option>
                                            <option value="7">July</option>
                                            <option value="8">August</option>
                                            <option value="9">September</option>
                                            <option value="10">October</option>
                                            <option value="11">November</option>
                                            <option value="12">December</option>
                                        </select>
                                        <div class="small-4 columns">
                                            <select name="dob_year" class="width-80" required>
                                                {{--<option>Year</option>--}}
                                                @for ($year = (date('Y') - 15); $year >= (date('Y') - 90); $year--)
                                                    <option value="{{ $year }}">{{ $year }}</option>
                                            @endfor
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="required" style="font-weight: bold;">PASSWORD</span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="password" name="signup_password" placeholder="" required/>
                                    <p>Note: password must contain letters and numbers, minimum 8 characters</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="required" style="font-weight: bold;">CONFIRM PASSWORD</span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="password" name="signup_password_confirmation" placeholder="" required/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <hr class="blue">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="sub-subtitle">BANK TRANSFER OPTION</span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="row">
                                        <div class="payment-box">
                                            <label for="payment1">
                                                <img src="{!! asset(null) !!}assets/images/img-bca.png" class="payment"><br>
                                                <span>123.456.7890</span><br>
                                                <span>(a/n) PT. SEAFERMART JAYA RAYA</span>
                                            </label>
                                        </div>

                                    </div><br>
                                    <div class="row">
                                        <div class="payment-box">
                                            <label for="payment2">
                                                <img src="{!! asset(null) !!}assets/images/img-mandiri.png" class="payment"><br>
                                                <span>123.456.7890</span><br>
                                                <span>(a/n) PT. SEAFERMART JAYA RAYA</span>
                                            </label>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="small-4 columns">
                        <h5 class="title-aneh">SHIPPING INFORMATION</h5>
                        <table class="payment">
                            <tbody>
                            <tr><td><span class="sub-subtitle">SHIPPING ADDRESS</span></td></tr>
                            <tr>
                                <td>
                                    <span class="required" style="font-weight: bold;"> ADDRESS NAME</span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="text" name="address_name" required="required" placeholder="e.g., Alamat Kantor, Rumah Tante">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="required" style="font-weight: bold;">RECIPIENT'S NAME</span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="text" name="address_recipient" required="required" placeholder="type recipient name here">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="required" style="font-weight: bold;">MOBILE NUMBER</span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="input-group">
                                        <span class="dilabelin">+62</span>
                                        <input  name="address_phone_number" class="pasangannya-label" type="text" placeholder="e.g., 8123xxx" style="height:24px;" required="required">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="required" style="font-weight: bold;">ADDRESS</span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="text" name="address_address" required="required" placeholder="type your address here">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="required" style="font-weight: bold;">STATE/PROVINCE</span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <select name="address_province_id" id="select_province">
                                        @foreach(App\Models\Province::where('is_active', 1)->get() as $province)
                                            <option value="{{ $province->province_id }}">{{ $province->province_name }}</option>
                                        @endforeach
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="required">CITY</span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <select name="address_city_id" id="select_city">
                                        @foreach(App\Models\City::where('province_id', 31)->orderBy('city_name', 'asc')->get() as $city)
                                            <option value="{{ $city->city_id }}" data-prov="{{ $city->province_id }}">{{ $city->city_name }}</option>
                                        @endforeach
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="required" style="font-weight: bold;">DISTRICT</span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <select name="address_area_id" id="select_area">
                                        @foreach(App\Models\Location\Area::orderBy('name', 'asc')->get() as $area)
                                            <option value="{{ $area->id }}" data-city="{{ $area->city_id }}">{{ $area->name }}</option>
                                        @endforeach
                                    </select>
                                </td>
                            </tr>
                            {{--<tr>
                                <td>
                                    <span class="required" style="font-weight: bold;">SUB DISTRICT</span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <select>
                                        <option>Please select sub district</option>
                                    </select>
                                </td>
                            </tr>--}}
                            <tr>
                                <td>
                                    <span class="required" style="font-weight: bold;">ZIP/POSTAL CODE</span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="text" name="address_postal_code" required="required"  placeholder="type your postal code here">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <hr class="blue">
                                </td>
                            </tr>
                            {{--<tr><td><span class="sub-subtitle">SHIPPING METHOD</span></td></tr>
                            <tr><td>
                                    Your shipping address is not in our recoverage area.
                                    Please contact our customer service for more
                                    informatiton
                                </td></tr>
                            <tr><td>
                                    <div class="row">
                                        <div class="small-6 columns"><br>
                                            <img src="{!! asset(null) !!}assets/images/img-jne.png">
                                        </div>
                                        <div class="small-6 columns">
                                            <div class="box">
                                                <input value="jne_oke" type="radio" name="shipping_service_code" class="checkout_shipping_service_code" required checked/>
                                                <label for="oke">OKE</label>
                                            </div>
                                            <div class="box">
                                                <input value="jne_reqular" type="radio" name="shipping_service_code" class="checkout_shipping_service_code" required checked/>
                                                <label for="regular">REGULER</label>
                                            </div>
                                            <div class="box">
                                                <input value="jne_yes" type="radio" name="shipping_service_code" class="checkout_shipping_service_code" required checked/>
                                                <label for="yes">YES</label>
                                            </div>
                                        </div>
                                    </div>
                                </td></tr>--}}
                            {{--<tr><td>
                                    <div class="row">
                                        <div class="small-6 columns">
                                            <img src="{!! asset(null) !!}assets/images/img-gojek.png">
                                        </div>
                                        <div class="small-6 columns"><br>
                                            <div class="box">
                                                <input value="gosend_same_day" type="radio" name="shipping_service_code" class="checkout_shipping_service_code" required checked/>
                                                <label for="gosend">Same Day</label>
                                            </div>

                                        </div>
                                    </div>
                                </td></tr>--}}

                            </tbody>
                        </table>
                    </div>
                    <div class="small-4 columns">
                        <h5 class="title-aneh">ORDER REVIEW</h5>
                        <table class="summary">
                            <tbody>
                            <tr><td colspan="3"><b class="text-blue size-18">SEAFER</b> <b class="text-red size-18">GROCER</b></td></tr>

                            @foreach (Carte::contents() as $cartItem)
                                <tr>
                                    <td>{{ $cartItem->name }}</td>
                                    <td>{{ $cartItem->sku }}</td>
                                    <td>{{ $cartItem->quantity }}</td>
                                    <td><span style="float:right">{{ Money::display($cartItem->price * $cartItem->quantity) }}</span></td>
                                </tr>
                            @endforeach
                            <tr>
                                <td>&nbsp</td>
                                <td>&nbsp</td>
                                <td>&nbsp</td>
                                <td>&nbsp</td>
                            </tr>
                            <tr>
                                <td colspan="3"><span style="float:left">SUB TOTAL</span></td>
                                <td><span style="float:right">{{Money::display( Carte::total()) }}</span></td>
                            </tr>
                            <tr>
                                <td colspan="3"><span style="float:left">PROMO</span></td>
                                <td id="checkout_promo"><span style="float:right">{{ $promo_applied == false ? '0' : Money::display($promo_applied) }}</span></td>
                            </tr>
                            <tr>
                                <td colspan="3"><span style="float:left">PPN (10%)</span></td>
                                <td id="checkout_ppn"><span style="float:right">{{  Money::display($ppn) }}</span></td>
                            </tr>
                            {{--<tr>
                                <td colspan="3"><span style="float:left">SHIPPING</span></td>
                                <td id="checkout_shipping_fee"><span style="float:right">{{  Money::display($shippingFee) }}</span></td>
                            </tr>--}}
                            <tr>
                                <td colspan="3"><span style="float:left">TOTAL</span></td>
                                <td id="checkout_total"><span style="float:right">{{ Money::display($total) }}</span></td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <button data-open="modal-before" class="button primary maxwidth space-3">CHECKOUT NOW</button>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp</td>
                                <td>&nbsp</td>
                                <td>&nbsp</td>
                            </tr>
                            </tbody>
                        </table>


                        {{ csrf_field() }}

                        <br><br><br>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="reveal" id="modal-after" data-reveal>
        <div class="modal-box">
            <div class="dashed-box">
                <div class="img-box">
                    <img src="{!! asset(null) !!}assets/images/img-popup.png">
                </div>
                <h5>Thank you for submitting!
                    <br>We will process your application
                </h5>
                <span>click anywhere to continue</span>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        $('#select_province').on('change', function(){
            var province_id = $(this).val();
            $.ajax({
                type: 'get',
                url: url('user/ajax-cities-by-province/'+province_id)
            }).done(function(result){
                result = jQuery.parseJSON(result);
                if(result.status == 'success') {
                    $('#select_city').html('');
                    $.each(result.cities, function(k, v) {
                        $('#select_city').append('<option value="'+k+'">'+v+'</option>');
                    });

                } else {
                    alert(result.message);
                }
            })
        });

        $('#select_city').on('change', function(){
            var city_id = $(this).val();
            $.ajax({
                type: 'get',
                url: url('user/ajax-areas-by-city/'+city_id)
            }).done(function(result){
                result = jQuery.parseJSON(result);
                if(result.status == 'success') {
                    $('#select_area').html('');
                    $.each(result.areas, function(k, v) {
                        $('#select_area').append('<option value="'+k+'">'+v+'</option>');
                    });

                } else {
                    alert(result.message);
                }
            })

        });
    </script>
@endsection