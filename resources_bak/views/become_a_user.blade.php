@extends('layouts.newmaster')

@section('title', App\Page::createTitle('Become A User'))

@section('content')
<!-- =================== -->
<!-- CONTENT -->
<!-- =================== -->

<div id="godOfContent">
    <div class="box-content" id="becomeAMember">
        <div class="row">
            <div class="small-12 columns">
                <h6 class="title">MEMBER BENEFITS</h6>
            </div>
            <div class="small-3 columns">
                <ul class="side-nav side-nav-become-box">
                    <li><a href="#" id="toBenefits" class="line-active">BENEFITS</a></li>
                    <!-- <li><a href="#" id="toHowitworks">HOW IT WORKS</a></li> -->
                    <li><a href="#" id="toSignup">SIGN UP</a></li>
                </ul>
            </div>
            <div class="small-9 columns">
                <div class="become-box">
                    <div class="row">
                        <div class="small-9 columns">
                            <p class="detail-title" style="font-size: 16px;">
                                If you are not yet a member, we wholeheartedly welcome you to be part of our Seafermart family! Simply Click the Sign Up button down here and boom..<br>
                                Welcome to SeaferMart and let's enjoy quality seafood together!
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="small-12 columns">
                            <p><b>Here are the beneﬁts that you can get from joining SeaferMart:</b></p>
    
                            {{-- s:enhancement --}}
                            <h6 class="sub-subtitle">QUALITY ASSURANCE</h6>
                            <p>
                                We at SeaferMart, are committed to delivering you the highest quality of seafood products that you and your family can enjoy. Before delivering the products to you, we pay careful attention to the following:
                            </p>
                            <b>Raw materials supply:</b>
                            <p>
                                Our fishes, prawns and other marine raw materials are carefully obtained locally from Indonesia's ocean riches and imported from other countries.From fishing/catching to sea freezing (if required) to transporting, we ensure that your fishes and other marine catches are traceable and handled with the best sustainable practices before they arrive in our cold storage for storage and processing.
                            </p>
                            <b>Storage:</b>
                            <p>
                                Keeping your seafood fresh and free from air contamination has always been our top priority. With our state-of-the-art warehousing and cold storage facilities located in Muara Baru, North Jakarta, we will ensure that your seafood products are preserved at the right temperature, whether it is frozen (-25 degrees Celsius), chilled (0-15 degrees Celsius) or stored at room temperature (25 degrees Celsius).
                            </p>
                            <b>Processing:</b>
                            <p>
                                &nbsp;
                            </p>
                            <b>Sourcing:</b>
                            <p>
                                &nbsp;
                            </p>
                            <b>Packing Delivery:</b>
                            <p>
                                &nbsp;
                            </p>
                            {{-- e:enhancement --}}

                        </div>
                    </div>

                    <div class="row">
                        <div class="small-2 columns">
                            <div class="img-box">
                                <img src="{{ asset(null) }}assets/images/img-deliv-dua.png">
                            </div>
                        </div>
                        <div class="small-10 columns no-padding">
                            <div class="detail-box">
                                <h6 class="sub-subtitle">SAME / NEXT DAY DELIVERY</h6>
                                <p>
                                    We are committed to delivering the products within the shortest time
                                    possible. Our delivery slots are as follow:
                                </p>
                                <b>For Seafer Grocer products</b>
                                <ul class="font-kecilin">
                                    <li>If you order before 10 AM, we will deliver between 12-6 PM.</li>
                                    <li>If you order before 2 PM, we will deliver between 4-10 PM.</li>
                                    <li>If you order after 2 PM, we will deliver on the next day between 9 AM-5 PM.</li>
                                </ul>
                                <!-- <b>For Seafer Eats products</b><br>
                                <p>
                                    Your products will be delivered separately from Seafer Grocer item as the sellers will directly send them to you. Your delivery schedule for Seafer Eats items will be show when you check out.
                                </p> -->
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="small-2 columns">
                            <div class="img-box">
                                <img src="{{ asset(null) }}assets/images/img-point-dua.png">
                            </div>
                        </div>
                        <div class="small-10 columns no-padding">
                            <div class="detail-box">
                                <h6 class="sub-subtitle">LOYALTY POINTS</h6>
                                <p>
                                    Our Loyalty Points come in a 1% cashback for every Rp 100.000,00 order
                                    value. You can choose to redeem your points upon checkout or continue
                                    accumulating your points and redeem it later on our website or mobile
                                    app. There is no expiry, so you can collect your points during your leisure.
                                    Happy shopping! <br><br>
                                    Read <a href="#">Terms & Conditions</a> for more information.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="small-2 columns">
                            <div class="img-box">
                                <img src="{{ asset(null) }}assets/images/img-huge-dua.png">
                            </div>
                        </div>
                        <div class="small-10 columns no-padding">
                            <div class="detail-box">
                                <h6 class="sub-subtitle">HUGE SELECTION</h6>
                                <p>
                                    We at SeaferMart, are trained to pick the best non-defective products with
                                    the highest caliber and to carefully wrap, preserve and place them in
                                    secure packaging to ensure they reach you in superior condition. You don't
                                    have to worry because our state-of-the-art warehousing and cold storage
                                    facilities will keep your chilled, frozen and perishable products stay fresh
                                    and free from air contamination. Even more fresh and hygienic tahn what
                                    you would pick up from physical stores!
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="small-2 columns">
                            <div class="img-box">
                                <img src="{{ asset(null) }}assets/images/img-assurance-dua.png">
                            </div>
                        </div>
                        <div class="small-10 columns no-padding">
                            <div class="detail-box">
                                <h6 class="sub-subtitle">QUALITY ASSURANCE</h6>
                                <p>
                                    We at SeaferMart, are trained to pick the best non-defective products with
                                    the highest caliber and to carefully wrap, preserve and place them in
                                    secure packaging to ensure they reach you in superior condition. You don't
                                    have to worry because our state-of-the-art warehousing and cold storage
                                    facilities will keep your chilled, frozen and perishable products stay fresh
                                    and free from air contamination. Even more fresh and hygienic than what
                                    you would pick up from physical stores!
                                </p>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="row">
                        <div class="small-2 columns">
                            <div class="img-box">
                                <img src="{{ asset(null) }}assets/images/img-guarantee-dua.png">
                            </div>
                        </div>
                        <div class="small-10 columns no-padding">
                            <div class="detail-box">
                                <h6 class="sub-subtitle">BEST PRICE GUARANTEE</h6>
                                <p>
                                    We want to provide you with the lowest prices, so if you ﬁnd a lower
                                    selling price on an identical non-promotional product at other stores, do
                                    let us know. <a href="#">Learn more.</a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="small-2 columns">
                            <div class="img-box">
                                <img src="{{ asset(null) }}assets/images/img-cart-dua.png">
                            </div>
                        </div>
                        <div class="small-10 columns no-padding">
                            <div class="detail-box">
                                <h6 class="sub-subtitle">DIRECT SHOP FROM SEAFER COOK AND SEAFER EATS</h6>
                                <p>
                                    Sometimes you get lost and bored with what you eat, right? Then you
                                    wonder what other food that you can try. There’s no need to worty! Here
                                    at SeaferMart, you can discover lots of food, including recipes that you can
                                    get from Seafer Cook and the now-happening food featured on Seafer Eats.
                                    Happy tummy leads to a happy day!
                                </p>
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
        <!-- <div class="row">
            <div class="small-10 small-centered columns">
                <div class="how-it-works-box">
                    <div class="content">
                        <h6 id="howitworks" class="subtitle">HOW IT WORKS</h6><br>
                        <p>
                            We have 3 diﬀerent portals to shop from. You can put everything in one cart and we will check you
                            out in one invoice. Here are the steps:
                        </p>
                        <img src="{{ asset(null) }}assets/images/img-flow.png">
                    </div>
                </div>
            </div>
        </div> -->
        <div class="row">
            <div class="small-10 small-centered columns">
                <div class="apaacoba">

                    <button class="button primary" id="signup" style="margin-left:40%"><a class="sign-up-btn" href="{{ url('user/sign-up') }}">SIGN UP NOW</a></button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection