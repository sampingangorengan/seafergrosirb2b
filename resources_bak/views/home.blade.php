@extends('layouts.newmaster')

@section('content')
    <!-- Slider -->
    <div class="orbit orbit-custom" role="region" data-orbit>
      <ul class="orbit-container">
        <?php $i = 0 ?>
        @foreach (App\Models\Home\Slide::orderBy('order')->get() as $slide)
          <?php $class = '' ?>
          @if ($i == 0)
            <?php $class = 'is-active' ?>
          @endif
          <li class="{{ $class }} orbit-slide orbit-slide-custom">
            <img class="orbit-image" src="{!! asset(null) !!}contents/{{ $slide->file_name }}" alt="space">
            <figcaption class="orbit-caption">CONNECTING PEOPLE <br> THROUGH FOOD</figcaption>
          </li>

          <?php $i++ ?>
        @endforeach
      </ul>
      <nav class="orbit-bullets">
        <button class="is-active" data-slide="0">
          <span class="show-for-sr">First skide details.</span>
          <span class="show-for-sr">Current Slide</span>
        </button>

        @for ($i = 0; $i < App\Models\Home\Slide::count() - 1; $i++)
          <button data-slide="{{ $i + 1 }}"><span class="show-for-sr">Second slide details.</span></button> 
        @endfor
    </nav>
    </div>

    <!-- Main Content -->
    <div class="box-content">
      <div class="row">
        <div class="medium-3 columns">
          <div  data-equalizer="bar">
            <div class="side-card" data-equalizer-watch="bar">
              <div class="card grocer">
                <div class="card-content">
                  <div class="img-box">
                    <img src="{!! asset(null) !!}assets/images/img-grocer.png">
                  </div>
                  <p class="text-box">
                    We believe that fresh ingredients lead to nutritious and delicious meals. We help you pick and deliver the best quality of a wide 
                  </p>
                </div>
                <button class="button">GO TO GROCER</button>
              </div>
            </div>
            <div class="side-card" data-equalizer-watch="bar">
              <div class="card cook">
                <div class="card-content">
                  <div class="img-box">
                    <img src="{!! asset(null) !!}assets/images/img-cook.png">
                  </div>
                  <p class="text-box">
                  We create a community portal to house a melting pot of genius famous chefs, food bloggers, and skilled housewives who will share 
                  </p>
                </div>
                <button class="button">GO TO COOK</button>
              </div>
            </div>
            <div class="side-card" data-equalizer-watch="bar">
              <div class="card eats">
                <div class="card-content">
                  <div class="img-box">
                    <img src="{!! asset(null) !!}assets/images/img-eats.png">
                  </div>
                  <p class="text-box">
                  The ﬁrst ever food marketplace in Indonesia where we work together with local InstaFood sellers with their mouthwatering creations to 
                  </p>
                </div>
                <button class="button">GO TO EATS</button>
              </div>
            </div>
          </div>
        </div>
        <div class="medium-9 columns">
          <div class=" panel">
            <div class="row" data-equalizer data-equalize-on="medium" id="test-eq">
              <div class="medium-6 columns">
                <a href="#">
                  <div class="cube" style="background-image:url({!! asset(null) !!}assets/images/img-1.png)">
                    <div class="detail-box bottom">
                      <h6 class="text-white">BROKOLI</h6>
                      <p class="text-white">dolor sit amet, <br>consectetur adipiscing elit.</p>
                      <span class="text-white">IDR 50K</span>
                    </div>
                  </div>   
                </a>
              </div>
              <div class="medium-6 columns">
                <a href="#">
                  <div class="cube" style="background-image:url({!! asset(null) !!}assets/images/img-2.png)">
                    <div class="detail-box bottom">
                      <h6 class="text-blue">BELL PEPPER</h6>
                      <p class="text-grey">dolor sit amet, <br> consectetur.</p> 
                      <span class="text-grey">IDR 35K</span>
                    </div>
                  </div>   
                </a>
              </div>
              <div class="medium-6 columns">
                <a href="#">
                  <div class="cube" style="background-image:url({!! asset(null) !!}assets/images/img-3.png)">
                    <div class="detail-box bottom">
                      <h6 class="text-blue">GREEN PEAS</h6>
                      <p class="text-grey">dolor sit amet,<br> consectetur </p>
                      <span class="text-grey">IDR 55K</span>
                    </div>
                  </div>   
                </a>
              </div>
              <div class="medium-6 columns">
                <div class="medium-6 columns sub-cube">
                  <a href="#">
                    <div class="cube bg-red" style="background-image:url({!! asset(null) !!}assets/images/img-sm1.png)">
                      <div class="detail-box">
                        <h6 class="text-white">BIG PROMO</h6>
                        <p class="text-white">dolor sit amet, consectur adipiscing elit. </p>
                        <!-- <span>IDR 55K</span> -->
                      </div>
                    </div>    
                  </a>
                </div>
                <div class="medium-6 columns sub-cube">
                  <a href="#">
                    <div class="cube" style="background-image:url({!! asset(null) !!}assets/images/img-sm2.png)">
                      <div class="detail-box bottom">
                        <h6 class="text-blue">LEMONS</h6>
                        <p class="text-grey">dolor sit amet,<br> consectur </p>
                        <span class="text-grey">IDR 55K</span>
                      </div>
                    </div>    
                  </a>
                </div>
              </div>
              <div class="medium-6 columns">
                <a href="#">
                  <div class="cube" style="background-image:url({!! asset(null) !!}assets/images/img-4.png)">
                    <div class="detail-box">
                      <h6 class="text-orange">CHICKEN <br> RECIPE</h6>
                      <p class="text-grey">read more</p>
                      <!-- <span class="text-grey">IDR 55K</span> -->
                    </div>
                  </div>   
                </a>
              </div>
              <div class="medium-6 columns">
                <a href="#">
                  <div class="cube" style="background-image:url({!! asset(null) !!}assets/images/img-5.png)">
                    <div class="detail-box">
                      <h6 class="text-white">DOUGH <br> MAKING <br> TIPS</h6>
                      <p class="text-white">read more </p>
                      <!-- <span class="color-white">IDR 55K</span> -->
                    </div>
                  </div>   
                </a>
              </div>
              <div class="medium-6 columns">
                <div class="medium-6 columns sub-cube">
                  <a href="#">
                    <div class="cube" style="background-image:url({!! asset(null) !!}assets/images/img-sm3.png)">
                      <div class="detail-box bottom">
                        <h6 class="text-white">CHICKEN <br> WAFFLE</h6>
                        <p class="text-white">read more</p>
                        <!-- <span>IDR 55K</span> -->
                      </div>
                    </div>    
                  </a>
                </div>
                <div class="medium-6 columns sub-cube">
                  <a href="#">
                    <div class="cube" style="background-image:url({!! asset(null) !!}assets/images/img-sm4.png)">
                      <div class="detail-box">
                        <h6 class="text-white">APPLE <br> RECIPE</h6>
                        <p class="text-white">read more</p>
                        <!-- <span>IDR 55K</span> -->
                      </div>
                    </div>    
                  </a>
                </div>
              </div>
              <div class="medium-6 columns">
                <a href="#">
                  <div class="cube" style="background-image:url({!! asset(null) !!}assets/images/img-6.png)">
                    <div class="detail-box">
                      <h6 class="text-orange">SALAD <br> RECIPE</h6>
                      <p class="text-grey">read more</p>
                      <!-- <span>IDR 55K</span> -->
                    </div>
                  </div>   
                </a>
              </div>
              <div class="medium-6 columns">
                <a href="#">
                  <div class="cube" style="background-image:url({!! asset(null) !!}assets/images/img-7.png)">
                    <div class="detail-box right">
                      <h6 class="text-green">TSUKEMEN</h6>
                      <p class="text-grey">dolor sit amet,<br>consectetur edipiscing elit </p>
                      <span class="text-grey">IDR 55K</span>
                    </div>
                  </div>   
                </a>
              </div>
              <div class="medium-6 columns">
                <a href="#">
                  <div class="cube" style="background-image:url({!! asset(null) !!}assets/images/img-8.png)">
                    <div class="detail-box">
                      <h6 class="text-green">BURGER</h6>
                      <p class="text-grey">dolor sit amet,<br> dolor sit amet, </p>
                      <span class="text-grey">IDR 55K</span>
                    </div>
                  </div>   
                </a>
              </div>
              <div class="medium-6 columns">
                <a href="#">
                  <div class="cube" style="background-image:url({!! asset(null) !!}assets/images/img-9.png)">
                    <div class="detail-box">
                      <h6 class="text-white">GREAT SUSHI</h6>
                      <p class="text-white">dolor sit amet, dolor sit amet, </p>
                      <span class="text-white">IDR 55K</span>
                    </div>
                  </div>   
                </a>
              </div>
              <div class="medium-6 columns">
                <div class="medium-6 columns sub-cube">
                  <a href="#">
                    <div class="cube" style="background-image:url({!! asset(null) !!}assets/images/img-sm5.png)">
                      <div class="detail-box bottom">
                        <h6 class="text-white">GREEN PEAS</h6>
                        <p class="text-white">dolor sit amet, dolor sit amet, </p>
                        <span class="text-white">IDR 55K</span>
                      </div>
                    </div>    
                  </a>
                </div>
                <div class="medium-6 columns sub-cube">
                  <a href="#">
                    <div class="cube" style="background-image:url({!! asset(null) !!}assets/images/img-sm6.png)">
                      <div class="detail-box bottom">
                        <h6 class="text-white">GREEN PEAS</h6>
                        <p class="text-white">dolor sit amet, dolor sit amet, </p>
                        <span class="text-white">IDR 55K</span>
                      </div>
                    </div>    
                  </a>
                </div>
             </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Feature -->
    <div class="feature-box">
      <div class="box-content">
        <ul>
          <li><a href="#"><img src="{{ asset(null) }}assets/images/imgpsh_fullsize.png"></a></li>
          <li><a href="#"><img src="{{ asset(null) }}assets/images/imgpsh_fullsize-5.png"></a></li>
          <li><a href="#"><img src="{{ asset(null) }}assets/images/imgpsh_fullsize-3.png"></a></li>
          <li><a href="#"><img src="{{ asset(null) }}assets/images/imgpsh_fullsize-4.png"></a></li>
          <li><a href="#"><img src="{{ asset(null) }}assets/images/imgpsh_fullsize-2.png"></a></li>
        </ul>
      </div>
    </div>
@endsection