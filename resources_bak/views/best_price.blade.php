@extends('layouts.newmaster')

@section('title', App\Page::createTitle('Best Price Guarantee'))

@section('customcss')
    <link rel="stylesheet" type="text/css" href="{!! asset(null) !!}assets/css/custom.css">
@endsection

@section('content')
    <div class="row">
        <div class="medium-12 columns">
    <span class="text-grey equal margin-top-10">
      <a class="text-grey" href="{!! url('/') !!}"><b>HOME</b></a> /
      <a class="text-grey" href="#"><b>BEST PRICE GUARANTEE</b></a>
    </span>
        </div>
    </div>

    <section>
        <div class="row">
            <div class="medium-12 columns">
                <h5 class="title title-margin">BEST PRICE GUARANTEE</h5>
                <div class="medium-offset-1 medium-3  columns">
                    <ul class="side-nav side-nav-become-box sidebar">
                        <li><a href="#" id="toHowitworkstwo" class="line-active">HOW IT WORKS</a></li>
                        <li><a href="" id="toClaimForm">CLAIM FORM</a></li>
                    </ul>
                </div>
                <div class="medium-6 medium-pull-2 columns">
                    <h5 id="howitworks" class="no-border subtitle">HOW IT WORKS</h5>
                    <div class="knowmore">
                        <p>
                            At SeaferMart, we want to provide the lowest prices whenever you shop with us. So if you find a lower selling price on an <span class="red">identical, in-stock, non-promotional and non-discounted product</span> at other online or physical stores in <span class="red">Jabodetabek</span> area within <span class="red">3 days</span> of purchase from SeaferMart, let us know and we will refund you the difference directly to your loyalty points.</p>
                        <p>
                            Please bear in mind that the Best Price Guarantee Refund does not apply in the event of a pricing error, typographical error, or other similar error if it is an error on the part of the store. We reserve the right to investigate the legitimacy of claims and deny the ones that are being submitted to intentionally abuse SeaferMart’s Best Price Guarantee scheme.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="blue-bg">
        <div class="row">
            <div class="medium-12 columns">
                <div class="medium-6 medium-offset-4 columns">
                    <h5 class="grey-bold margin-bottom-30">What qualifies for best price refund?</h5>
                    <div class="large-4 columns">
                        <img src="{!! asset(null) !!}assets/images/identical-stuff.png">
                        <p class="blue" style="margin-bottom: 0;">IDENTICAL STUFF</p>
                        <p>The item must be exactly the same – same brand, distributor, make, model, size, color, packaging, and net-weight.</p>
                    </div>
                    <div class="large-4 columns">
                        <img src="{!! asset(null) !!}assets/images/in-stock.png">
                        <p class="blue" style="margin-bottom: 0;">IN-STOCK</p>
                        <p>The item must be in-stock and available for immediate purchase.</p>
                    </div>
                    <div class="large-4 columns">
                        <img src="{!! asset(null) !!}assets/images/regular-selling-price.png">
                        <p class="blue" style="margin-bottom: 0;">REGULAR SELLING PRICE</p>
                        <p>The item must be sold at a regular selling price – no discount, no promo, or special offer.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section style="padding-bottom: 0px;margin-bottom: -18px;">
        <div class="row">
            <div class="medium-12 columns">
                <div class="medium-6 medium-offset-4 columns">
                    <h5 class="grey-bold margin-bottom-30">How to claim for refund?</h5>
                    <ul class="claim-refund">
                        <li>
                            1.  Take a picture that shows <span class="red">both</span> the product <span class="red">and</span> the price tag with a lower selling price than SeaferMart
                            <br>
                            <img style="margin: 15px;" src="{!! asset(null) !!}assets/images/check-uncheck-boxdua.png">
                        </li>
                        <li>
                            2.  Submit your claim online simply by filling out the form <a href="#"> here.</a>
                        </li>
                        <li>
                            3.  Kindly wait for the refund. You will be notified via email.
                        </li>
                    </ul>
                    <hr id="claim-refund">
                </div>
            </div>
        </div>
    </section>
    <section id="claim-form">
        <div class="row">
            <div class="medium-12 cloumns">
                <div class="medium-6 medium-offset-4 columns">
                    <div class="flash-message">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                            @if(Session::has('alert-' . $msg))

                                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                            @endif
                        @endforeach
                    </div> <!-- end .flash-message -->
                    @if(count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    {!! Form::open(['method'=>'POST', 'action'=>'ClaimController@store', 'files' => true]) !!}
                    <h5 id="claimform" class="no-border subtitle" style="margin-bottom: 40px;">CLAIM FORM</h5>
                    <div class="form-box">
                        <div class="row">
                            <div class="medium-5 columns">
                                <label for="middle-label" class="text-left">TYPE<span class="red">*</span></label>
                            </div>
                            <div class="medium-7 columns">
                                {!! Form::select('TYPE', ['Return&Exchange' => 'Return&Exchange', 'Refund' => 'Refund']); !!}
                                {{--<select class="short">
                                    <option>Return&Exchange</option>
                                    <option>Refund</option>
                                </select>--}}
                            </div>
                        </div>
                        <div class="row">
                            <div class="medium-5 columns">
                                <label for="middle-label" class="text-left">FIRST NAME<span class="red">*</span></label>
                            </div>
                            <div class="medium-7 columns">
                                {!! Form::text('first_name', null, ['class'=>'form-control']) !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="medium-5 columns">
                                <label for="middle-label" class="text-left">LAST NAME<span class="red">*</span></label>
                            </div>
                            <div class="medium-7 columns">
                                {!! Form::text('last_name', null, ['class'=>'form-control']) !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="medium-5 columns">
                                <label for="middle-label" class="text-left">EMAIL<span class="red">*</span></label>
                            </div>
                            <div class="medium-7 columns">
                                {!! Form::text('email', null, ['class'=>'form-control']) !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="medium-5 columns">
                                <label for="middle-label" class="text-left">ITEM NAME<span class="red">*</span></label>
                            </div>
                            <div class="medium-7 columns">
                                {!! Form::text('item_name', null, ['class'=>'form-control']) !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="medium-5 columns">
                                <label for="middle-label" class="text-left">ORDER NUMBER<span class="red">*</span></label>
                            </div>
                            <div class="medium-7 columns">
                                {!! Form::text('order_id', null, ['class'=>'form-control']) !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="medium-5 columns">
                                <label for="middle-label" class="text-left">URL link of seafermart.co.id item<span class="red">*</span></label>
                            </div>
                            <div class="medium-7 columns">
                                {!! Form::text('seafer_link', null, ['class'=>'form-control']) !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="medium-5 columns">
                                <label for="middle-label" class="text-left">Regular Selling Price you paid at Seafermart (Rp.)<span class="red">*</span></label>
                            </div>
                            <div class="medium-7 columns">
                                {!! Form::text('seafer_price', null, ['class'=>'form-control']) !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="medium-5 columns">
                                <label for="middle-label" class="text-left">Regular Selling Price you found in Other Store (Rp.)<span class="red">*</span></label>
                            </div>
                            <div class="medium-7 columns">
                                {!! Form::text('other_price', null, ['class'=>'form-control']) !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="medium-5 columns">
                                <label for="middle-label" class="text-left">URL link of product page (if item is on online store)<span class="red">*</span></label>
                            </div>
                            <div class="medium-7 columns">
                                {!! Form::text('other_link', null, ['class'=>'form-control']) !!}
                            </div>
                        </div>
                        <div class="row">
                            <div class="medium-5 columns">
                                <label for="middle-label" class="text-left">STORE LOCATION (if item is on online store)<span class="red">*</span></label>
                            </div>
                            <div class="medium-7 columns">
                                {!! Form::select('store_location', ['Hero' => 'Hero Supermarket', 'Indomaret' => 'Indomaret', 'Alfamart' => 'Alfamart']); !!}
                                {{--<select class="short">
                                    <option>7-Eleven</option>
                                    <option>Hero Supermarket</option>
                                    <option>Indomaret</option>
                                </select>--}}
                            </div>
                        </div>
                        <div class="row">
                            <div class="small-5 columns">
                                <label for="middle-label" class="text-left middle ">SCREENSHOT PROOF</label>
                            </div>
                            <div class="small-7 columns">
                                <div class="preview img-wrapper simple"></div>
                                <div class="file-upload-wrapper custom" style="position: absolute;width:10px;height:10px">
                                    <input type="file" name="file" class="file-upload-native apalah" accept="image/*" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="medium-7 medium-offset-5 columns" style="margin-top: 20px;">
                                <button class="button btn-aneh" type="submit">SUBMIT</button>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </section>
@endsection