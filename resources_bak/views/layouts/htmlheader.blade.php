	<title>
    	{{ app()->environment() == 'production' || app()->environment() == 'live' ? '' : '('.app()->environment().') ' }}
    	@yield('title', 'SEAFERMART')
  	</title>
  	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate"/>
    <meta http-equiv="Pragma" content="no-cache"/>
    <meta http-equiv="Expires" content="0"/>

	{{-- OLD --}}
	{{-- <link rel="stylesheet" type="text/css" href="{{ asset(null) }}assets/css/plugin/jquery-ui.css">
	<link rel="stylesheet" type="text/css" href="{!! asset(null) !!}assets/css/plugin/foundation.min.css">
	<link rel="stylesheet" type="text/css" href="{!! asset(null) !!}assets/css/main.css">
	<link rel="stylesheet" type="text/css" href="{!! asset(null) !!}assets/css/main.css">
	<link rel="stylesheet" type="text/css" href="{!! asset(null) !!}assets/css/custom.css">
	<link rel="stylesheet" type="text/css" href="{!! asset(null) !!}assets/css/jquery.typeahead.css">
	<link rel='stylesheet' href='{!! asset(null) !!}bower_components/glyphicons-only-bootstrap/css/bootstrap.min.css' /> --}}
	{{-- OLD --}}

	{{-- NEW --}}
	<link rel="stylesheet" type="text/css" href="{{ asset(null) }}assets/css/plugin/jquery-ui.css">
	<link rel="stylesheet" type="text/css" href="{!! asset(null) !!}assets/css/plugin/foundation.min.css">
	<link rel="stylesheet" type="text/css" href="{!! asset(null) !!}assets/css/jquery.typeahead.css">
	<link rel='stylesheet' href='{!! asset(null) !!}bower_components/glyphicons-only-bootstrap/css/bootstrap.min.css' />
	<link rel="stylesheet" type="text/css" href="{!! asset(null) !!}assets/css/main.css">
	<link rel="stylesheet" type="text/css" href="{!! asset(null) !!}assets/css/custom.css">	
	{{-- NEW --}}
	
	@yield('customcss')
	<style>
		.logo img{
			height:65px;
			margin-top:7px;
		}
		.dropdown-submenu {
			padding-left: 33px !important;
		}
		.top-bar{
			padding:0;
		}
		.feature-box ul li img{
			width:75%;
		}
		.feature-box ul li img:hover{
			transform: scale(1.1);
		}
		.sub-nav ul.left-side li a:hover{
			color:#FF0000;
		}
		.sub-nav ul.left-side li a:hover span{
			color:#FF0000 !important;
		}
		.sub-nav ul.left-side li:nth-child(1) span.blue{
			color:#0368ff !important;
		}
		.sub-nav ul.left-side li:nth-child(1) span.red{
			color:#FF0000 !important;
		}
		#grocer a .blue{
			color:#0368ff !important;
		}
		#grocer a span.blue:hover{
			color:#0368ff !important;
		}
		#grocer a span.red{
			color:#FF0000 !important;
		}
		#grocer a span.red:hover{
			color:#FF0000 !important;
		}
		.second-lv .third-lv ul li a{
			font-size:13px !important;
		}
		#popupChart button.checkout{
			background-color:#0368ff;
		}
		#popupChart button.checkout:hover{
			background-color:#0368ff;
		}
		.cube:hover:before{
			background-color:transparent;
		}
		.cart-notif{
			width:21px;
			height:20px;
			padding:5px;
			font-size:10px;
			background:red;
			color:white;
			font-weight:bold;
			line-height:10px;
			text-align:center;
			margin:0 auto;
			position:absolute;
			top:0;
			right:0;
			border-radius:20px
		}
		.payment-notif{
			width:20px;
			height:20px;
			padding:5px;
			font-size:10px;
			background:red;
			color:white;
			font-weight:bold;
			line-height:10px;
			text-align:center;
			margin:0 auto;
			position:absolute;
			top:-3px;
			right: -20px;
			border-radius:20px
		}
		.pagination .disabled{
			display:none !important;
		}
        footer .live-chat {
            position: absolute;
            width: 250px;
            right: 0;
            bottom: 0;
            border-top-right-radius: 10px;
            border-top-left-radius: 10px;
            border-bottom: 3px solid $grey;
            background-color: $primary-color;
            overflow: hidden;
            cursor: pointer;
        }
		#firechat-wrapper {
			height: 275px;
			max-width: 325px;
			padding: 10px;
			border: 1px solid #ccc;
			background-color: #fff;
			margin: 50px auto;
			text-align: center;
			-webkit-border-radius: 4px;
			-moz-border-radius: 4px;
			border-radius: 4px;
			-webkit-box-shadow: 0 5px 25px #666;
			-moz-box-shadow: 0 5px 25px #666;
			box-shadow: 0 5px 25px #666;
		}
		.typeahead__list {
			background:#fff !important;
		}
	</style>