<!-- =================== -->
<!-- FOOTER -->
<!-- =================== -->
{{--
<footer>
  <div class="box-content">
    <div class="row">
      <div class="large-2 columns no-padding">
      <h6>WHY US</h6>
    <ul>
      <li><a href="#">About Us</a></li>
      <li><a href="#">Become a Member</a></li>
      <li><a href="#">Become a Seller</a></li>
      <li><a href="#">Share Your Recipe</a></li>
      <li><a href="#">Become a Supplier</a></li>
      <li><a href="#">Best Price Guarantee</a></li>
      <li><a href="#">Become a Kirin(Careers)</a></li>
    </ul>
    </div>
    <div class="large-2 columns no-padding">
      <h6>ORDERS</h6>
      <ul>
        <li><a href="#">Loyalty Points</a></li>
        <li><a href="#">Payment</a></li>
        <li><a href="#">Delivery</a></li>
        <li><a href="#">Return & Exchange</a></li>
        <li><a href="#">Wholesale</a></li>
      </ul>
    </div>
    <div class="large-2 columns no-padding">
      <h6>CUSTOMER CARE</h6>
      <ul>
        <li><a href="#">My Account</a></li>
      <li><a href="#">Suggest a Product</a></li>
      <li><a href="#">Privacy Policy</a></li>
      <li><a href="#">Term & Conditions</a></li>
      <li><a href="#">FAQ</a></li>
      <li><a href="#">Contact Us</a></li>
      <li><a href="#">Glossary</a></li>
    </ul>
    </div>
    <div class="large-6 columns no-padding">
      <div class="form-box">
        <h6>STAY IN TOUCH</h6>
        <form action="#" method="post">
          <ul class="menu">
            <li><input type="email" name="email" placeholder="Email Address"></li>
            <li><button type="button" class="button">SEND</button></li>
          </ul>
        <ul class="socmed-list">
          <li><a href="#"><img src="{!! asset(null) !!}assets/images/ico-fb.png"></a></li>
          <li><a href="#"><img src="{!! asset(null) !!}assets/images/ico-ig.png"></a></li>
          <li><a href="#"><img src="{!! asset(null) !!}assets/images/ico-line.png"></a></li>
          <li><a href="#"><img src="{!! asset(null) !!}assets/images/ico-youtube.png"></a></li>
        </ul>
        </form>
      </div>
    </div>
    <div class="chat-box">
      <div class="large-3 columns no-padding left">
        <img src="{!! asset(null) !!}assets/images/ico-chat.png">
      </div>
      <div class="large-9 columns no-padding right">
        <span>LIVE CHAT ( 0 )</span>
      </div>
    </div>
    </div>
  </div>
</footer>--}}

<footer>
    <div class="box-content">
        <div class="row">
            <div class="large-2 columns no-padding">
                <h6>WHY US</h6>
                <ul>
                    <li><a href="{{ url('about-us') }}">About Us</a></li>
                    <li><a href="{{ url('become-member') }}">Become a Member</a></li>
                    {{--<li><a href="#">Become a Seller</a></li>
                    <li><a href="#">Share Your Recipe</a></li>--}}
                    {{--<li><a href="#">Become a Supplier</a></li>--}}
                    {{-- <li><a href="{{ url('best-price-guarantee') }}">Best Price Guarantee</a></li> --}}
                    {{--<li><a href="#">Become a Kirin(Careers)</a></li>--}}
                </ul>
            </div>
            <div class="large-2 columns no-padding">
                <h6>ORDERS</h6>
                <ul>
                    <li><a href="{{ url('loyalty-points') }}">Cashback Rewards</a></li>
                    <li><a href="{{ url('payment') }}">Payment</a></li>
                    <li><a href="{{ url('delivery') }}">Delivery</a></li>
                    <li><a href="{{ url('return-and-exchange') }}">Return & Exchange</a></li>
                    {{--<li><a href="#">Wholesale</a></li>--}}
                </ul>
            </div>
            <div class="large-2 columns no-padding">
                <h6>CUSTOMER CARE</h6>
                <ul>
                    <li><a href="{{ url('user/my-account') }}">My Account</a></li>
                    <li><a href="{{ url('suggestion') }}">Suggest a Product</a></li>
                    {{--<li><a href="#">Privacy Policy</a></li>
                    <li><a href="#">Term & Conditions</a></li>--}}
                    <li><a href="{{ url('faq') }}">FAQ</a></li>
                    <li><a href="{{ url('contact-us') }}">Contact Us</a></li>
                    {{--<li><a href="#">Glossary</a></li>--}}
                </ul>
            </div>
            <div class="large-6 columns no-padding" style="padding-bottom:130px">
                <div class="form-box">
                    <h6><a href="#" data-open="modal-flash">STAY IN TOUCH</a></h6>
                    {!! Form::open(['method'=>'POST', 'action'=>'NewsletterController@store']) !!}
                    <ul class="menu">
                        <li><input type="email" name="email" placeholder="Email Address"></li>
                        <li><button type="submit" class="button">SEND</button></li>
                    </ul>
                    <ul class="socmed-list">
                        <li><a href=" https://www.facebook.com/seafermart/"><img src="{!! asset(null) !!}assets/images/ico-fb.png" data-rjs="3"></a></li>
                        <li><a href="https://www.instagram.com/seafermart/"><img src="{!! asset(null) !!}assets/images/ico-ig.png" data-rjs="3"></a></li>
                        {{--<li><a href="http://seafermart.co.id"><img src="{!! asset(null) !!}assets/images/ico-line.png" data-rjs="3"></a></li>--}}
                        {{--<li><a href="http://seafermart.co.id"><img src="{!! asset(null) !!}assets/images/ico-youtube.png" data-rjs="3"></a></li>--}}
                    </ul>
                    {!! Form::close() !!}
                </div>
            </div>
            <div class="box_down_app">
              <a href=""><img src="{{ asset(null) }}responsive_new/assets/images/materials/img_appstore.png"></a>
              <a href=""><img src="{{ asset(null) }}responsive_new/assets/images/materials/img_playstore.png"></a>
            </div>
            <div class="box_call_bubble">
                <ul>
                  <li>
                    <a href="https://api.whatsapp.com/send?phone=02122333288">+62 (021-22333 - 288)</a>
                  </li>
                  <li>
                    <a href="tel:02122333288">+62 (021-22333 - 288)</a>
                  </li>
                </ul>
              </div>
            {{--<div class="chat-box">
                <div id="firechat-wrapper">
                </div>
                <div class="large-3 columns no-padding left">
                    <img src="{!! asset(null) !!}assets/images/ico-chat.png">
                </div>
                <div class="large-9 columns no-padding right">

                </div>
            </div>--}}
        </div>
    </div>
</footer>

<!-- POPUP -->
<div class="reveal modal-forgotpassword" id="modal-forgotpassword" data-reveal>
    <div class="modal-box">
        <div class="dashed-box">
            <form method="post" action="" class="forget-me-not">
                <h4>FORGOT PASSWORD</h4>
                <p>Put Your Email</p>
                <input type="text" name="email">
                {{ csrf_field() }}
                <button type="button" class="button btn-aneh do-not-forget" data-open='modal-afterForgotPassword'>SUBMIT</button>
            </form>
        </div>
    </div>
</div>
@if(auth()->check())
<div class="reveal modal-referralcode" id="modal-referralcode" data-reveal>
    <div class="modal-box">
        <div class="dashed-box">
            <!-- <h6><a href="{{ url('user/sign-up?refer_from='.auth()->user()->referral_code) }}">{{ url('user/sign-up?refer_from='.auth()->user()->referral_code) }}</a></h6> -->
            <p style="font-size:16px;">
              Invite your friends and enjoy 0.5% cashback from each time they shop from Seafermart!
            </p>
            <p style="font-size:12px;">(Disclaimer: This only applies for every Rp. 100,000,- purchase made from your successfully referred friends)</p>
            <div id="shareBtn" class="btn btn-success clearfix">Share to Facebook</div>
            <p>Or share by email</p>
            <!-- <a href="mailto:yourfriend@mailprovider.com?subject=Seafermart%20Referral%20Code&amp;body=Copy%20and%20paste%20this%20link%20{{ url('user/sign-up?refer_from='.auth()->user()->referral_code) }}%20to%20your%20browser%20in%20order%20to%20get%20benefit%20when%20you%20buy%20good%20things%20from%20seafermart.co.id">Send to mail</a> -->
            <form method="post" action="share-referral" id="share-referral-form">
              <input type="hidden" value="{{ url('user/sign-up?refer_from='.auth()->user()->referral_code) }}" name="referral_code">
              <input type="text" name="email" placeholder="Email">
              <button type="submit">SUBMIT</button>
            </form>
        </div>
    </div>
</div>
@endif
<div class="reveal modal-afterForgotPassword" id="modal-afterForgotPassword" data-reveal>
    <div class="modal-box">
        <div class="dashed-box">
            <div class="img-box">
                <img src="{!! asset(null) !!}assets/images/img-maskot.png">
            </div>
            <p class="text-blue">yay! the link to reset your password <br> has been sent to your email</p>
        </div>
    </div>
</div>
<div class="reveal modal-flash" id="modal-flash" data-reveal>
    <div class="modal-box">
        <div class="dashed-box">
            <div class="img-box">
                <img src="{!! asset(null) !!}assets/images/img-maskot.png">
            </div>
            @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                @if(Session::has('alert-' . $msg))
                    <p class="alert alert-{{ $msg }} {{$msg == 'danger' ? 'text-red' : 'text-blue'}}">{{ Session::get('alert-' . $msg) }}</p>
                @endif
            @endforeach
        </div>
    </div>
</div>
<div class="reveal modal-flash-new" id="modal-flash-new" data-reveal>
    <div class="modal-box">
        <div class="dashed-box">
            <div class="img-box">
                <img src="{!! asset(null) !!}assets/images/img-maskot.png">
            </div>
            <p class="alert alert-danger text-red">Please pick your address first.</p>
        </div>
    </div>
</div>
