{{-- s: Blue Bar --}}
<header>
    <div class="box-content">
        <div class="row">
            <div class="large-6 columns no-padding">
                <ul>
                    <li><b>QUALITY SEAFOOD FOR ALL</b></li>
                    {{--<li><b>Delivery within Jabodetabek only</b></li>--}}
                </ul>
            </div>
            <div class="large-6 columns no-padding">
                <ul style="float: right; padding:0; margin:0 auto;">
                    <li><a href="{{ url('about-us') }}">About Us</a></li>

                    <li><a href="{{ url('become-member') }}">Member Benefits</a></li>
                    {{--<li><a href="#">Blog</a></li>--}}
                </ul>
            </div>
        </div>
    </div>
</header>
{{-- e: Blue Bar --}}

{{-- s: Logo Bar --}}
<div class="top-bar">
    <div class="box-content">
        <div class="row">
            {{-- s: Logo --}}
            <div class="top-bar-left">
                <ul class="dropdown menu" data-dropdown-menu>
                    <li class="logo">
                        <a href="{!! url('/') !!}">
                            <img src="{!! asset(null) !!}assets/images/logo.png" data-rjs="3">
                        </a>
                    </li>
                </ul>
            </div>
            {{-- e: Logo --}}

            {{-- s: Search --}}
            <div class="top-bar-right padding">
                <ul class="menu menu-dropdown">

                    <li>
                        {!! Form::open(['method'=>'POST', 'url'=>'search']) !!}
                        <div class="typeahead__container">
                            <div class="typeahead__field">
                     
                                <span class="typeahead__query">
                                    <input class="search js-typeahead" type="search" placeholder="I'm looking for" name="search" autocomplete="off">
                                    {{-- <input class="js-typeahead"
                                           name="q"
                                           type="search"
                                           autocomplete="off"> --}}
                                </span>
                            </div>
                        </div>
                    </li>
                    <li>
                        <button type="submit" class="button search-btn"><img src="{!! asset(null) !!}assets/images/ico-search.png"></button>
                        {!! Form::close() !!}
                    </li>

                    <li>
                        <a href="#" id="openCART" data-toggle="popupChart">
                            <img src="{!! asset(null) !!}assets/images/ico-header-cart.png" class="cart">
                            @if(count(Carte::contents(true)) > 0)
                            <div class="cart-notif">{{ count(Carte::contents(true)) }}</div>
                            @else
                            <div class="cart-notif" style="display: none;"></div>
                            @endif
                            {{--<img src="{!! asset(null) !!}assets/images/dot-merah-besar.png" id="cart-notif" style="position: absolute;left:50px !important;top:15px;display:none;">--}}
                        </a>
                        <!-- ini kalau untuk empty cart style leftnya di ganti ya jadi left:-30px atau left:-20px -->
                        <div id="popupChart" data-toggler="is-hidden" class="popup-box right1 is-hidden" style="left:-45px;" data-closable>
                            <span class="text-blue">CART</span>
                            <span class="close close-button-cart"><img src="{!! asset(null) !!}assets/images/ico-close.png"></span>

                            <div class="cart_item_container">
                            </div>
                        </div>
                    </li>
                    

                    @if (auth()->check())
                    
                    <li>
                        <a href="#" id="openAccount" class="text-red">
                            <span class="fontsize-username" style="float:left;line-height:18px;">{{ auth()->user()->name }} <br>
                            @if(auth()->user()->points()->get()->count() > 0)
                            {{ Money::display(auth()->user()->points()->first()->nominal) }}
                            @else
                            Rp.0
                            @endif</span>
                            {{--<img src="{!! asset(null) !!}assets/images/ico-header-acc.png">--}}
                            @if(auth()->user()->profile_picture)
                                @if(strpos(auth()->user()->photo->file, 'facebook') !== false || strpos(auth()->user()->photo->file, 'google') !== false)
                                    <img src="{{ auth()->user()->photo->file }}" style="width:40px;height:40px;border-radius: 20px;border:1px solid blue;">
                                @else
                                    <img src="{!! asset(null) !!}profile_picture/{{ auth()->user()->photo->file }}" style="width:40px;height:40px;border-radius: 20px;border:1px solid blue;">
                                @endif
                            @else
                            <img src="{!! asset(null) !!}assets/images/ico-header-acc.png" >
                            @endif
                            {{--<img src="{!! asset(null) !!}assets/images/dot-merah-besar.png" style="position: absolute;left:115px !important;">--}}
                        </a>

                        
                             <div id="popupOpenAccount" class="popup-box-account  right2 center hide-box">
                                <ul>
                                    <li>
                                        <a href="{{ url('user/my-account') }}" style="font-size:12px;">My Accounts</a>
                                    </li>
                                    <li>
                                        <div><a href="{{ url('orders') }}"  style="font-size:12px;">Payment Confirmation</a></div>
                                        @if(\App\Models\Order::ofUser(auth()->user())->where('order_status',1)->orderBy('updated_at', 'desc')->get()->count() > 0)
                                        <div class="payment-notif">{{ \App\Models\Order::ofUser(auth()->user())->where('order_status',1)->orderBy('updated_at', 'desc')->get()->count() }}</div>
                                        @endif
                                        {{--<img src="{!! asset(null) !!}assets/images/dot-merah-kecil.png" style="position: absolute;">--}}
                                    </li>
                                    <li>
                                        <a id="log-me-out" href="{{ url('user/sign-out') }}" style="font-size:12px;">Sign Out</a>
                                    </li>
                                </ul>
                            </div>
                        

                    </li>
                    @else
                    <li>
                        <a href="#" class="openSIGNIN">SIGN IN</a>
                        <div id="popupSignin" class="popup-box-signin right2 hide-box">
                            <span class="text-blue" style="padding-left: 12px;">SIGN IN</span>
                            <span class="close close-button"><img src="{!! asset(null) !!}assets/images/ico-close.png"></span>
                            <div class="form-box" style="padding-left: 12px;">
                                
                                <form method="post" action="{!! url('user/sign-in') !!}">
                                    <input type="text" placeholder="Your Email" name="email" style="border:2px solid #808184;">
                                    <input type="password" placeholder="Password" name="password" style="border:2px solid #808184;">
                                    <div class="box">
                                        <input id="keepMe2" type="checkbox" name="remember">
                                        <label for="keepMe2" style="color:#A9A9A9;font-size: 10px;padding-bottom: -1px;">Keep me signed in</label><br>
                                    </div>
                                    {{ csrf_field() }}
                                    <button class="button" id="myLogin" style="background-color: #0368FF; position:relative; margin-left:1px;">SUBMIT</button>
                                    <a href="#" id="forgotPassword" data-open="modal-forgotpassword" style="color:   #808080;font-size:12px;margin-left:1px;">Forgot Password?</a><br>
                                </form>
                                <a href="{{ url('login/facebook') }}"><button class="button sign-in fb">
                                    <img src="{!! asset(null) !!}assets/images/ico-fb.png">Sign In with Facebook
                                </button></a><br/>
                                <a href="{{ url('login/google') }}"><button class="button sign-in gplus">
                                    <img src="{!! asset(null) !!}assets/images/ico-gplus-white.png">Sign In with Google Plus
                                </button></a><br/>
                                {{--<div class="fb-login-button" data-max-rows="1" data-size="large" data-button-type="login_with" data-show-faces="false" data-auto-logout-link="false" data-use-continue-as="true"></div>--}}
                            </div>
                        </div>
                    </li>
                    <li><a href="{{ url('user/sign-up') }}">SIGN UP</a></li>
                    @endif

                </ul>
            </div>
        </div>
    </div>
</div>
{{-- e: Logo Bar --}}

{{-- s: Menu Bar --}}
<div class="sub-nav">
    <div class="row">
        <div class="large-12 columns no-padding">
            <ul class="left-side">
                <li class="dropdown-submenu" style="float:left"><a href="#" id="grocer"> <span class="blue">SEAFER</span>&nbsp;<span class="red">GROCER</span></a>
                    <div class="second-lv grocer" id="secondGrocer">
                        <div class="box-content" style="float:left">
                            <ul>

                                <?php $i = 1;?>
                                @foreach( App\Models\Groceries\Category::orderBy('order')->get() as $category )

                                    @if(count($category->child) > 0)
                                    <li class="third-submenu-right" style="min-width:215px;">
                                        <a href="#" id="thirdGrocerBtn{{ $i }}">{{ strtoupper($category->name) }}<span class="glyphicon glyphicon-chevron-right" style="margin-left:0;float:right;font-size:10px;color:grey"></span></a>
                                            {{--<div class="third-lv" id="thirdGrocer{{ $i }}" style="left:330px;position:fixed;top:235px;height:307px">
                                            <ul>
                                                @foreach(App\Models\Groceries\Category::where('is_child', 1)->where('parent', $category->id)->get() as $child_category)
                                                <li style="padding:0 !important"><a href="{!! url('groceries/' . $child_category->uri) !!}">{{ $child_category->name}}</a></li>
                                                @endforeach

                                            </ul>
                                        </div>--}}
                                    </li>
                                    @else
                                    <li class="third-submenu-right">
                                        <a href="{!! url('groceries/' . $category->slug) !!}">{{ strtoupper($category->name) }}</a>
                                    </li>
                                    @endif
                                <?php $i = $i+1;?>
                                @endforeach

                            
                            </ul>

                        </div>
                        <div class="box-content second-level-category"  style="float:left;margin-left:0;display:none;">

                            <?php $i = 1;?>
                            @foreach( App\Models\Groceries\Category::orderBy('order')->get() as $category )

                                @if(count($category->child) > 0)


                                        <div class="third-lv" id="thirdGrocer{{ $i }}" >
                                            <ul style="margin-left:0">
                                                @foreach($category->child as $child_category)
                                                    <li style="padding:0 !important;"><a href="{!! url('groceries/' . $child_category->slug) !!}" style="font-size:12px !important">{{ strtoupper($child_category->name) }}</a></li>
                                                @endforeach

                                            </ul>
                                        </div>

                                @endif
                                <?php $i = $i+1;?>
                            @endforeach

                        </div>
                        <div style="both:clear"></div>
                    </div>
                </li>
                {{-- PHASE 2 --}}
                {{-- <li class="dropdown-submenu"><a href="#" id="cook"> SEAFER <span>COOK</span></a>
                </li>
                <li class="dropdown-submenu"><a href="#" id="eats"> SEAFER <span>EATS</span></a>
                </li> --}}
            </ul>

        </div>
    </div>  
</div>
{{-- e: Menu Bar --}}