<!DOCTYPE html>
<html>
<head>
    @include('layouts.htmlheader')
    {{-- <style type="text/css">
        #seafer-load {
            position: fixed;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            height: 100%;
            width: 100%;
            display: table;
            background: rgba(255,255,255,0.9);
            z-index: 9999999;
        }
        #seafer-load-wrap {
            display: table-cell;
            vertical-align: middle;
            text-align: center;
            color: #0368ff;
        }
    </style> --}}
</head>
<body>
    {{-- <div id="seafer-load">
        <div id="seafer-load-wrap">
            <img src="http://seafermart.co.id/assets/images/img-maskot.png">
            <p>LOADING...</p>
        </div>
    </div> --}}
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.9&appId=1900251586926025";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
    @include('layouts.header_revamp')

    @yield('content')

    @include('layouts.footer')
<div class="copyright">
  <div class="box-content">
    <span>&copy; 2018 SeaferMart All Rights Reserved</span>
  </div>
</div>

<script src="{!! asset(null) !!}assets/js/jquery.min.js"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
{{-- <script type="text/javascript">
    $(window).load(function() {
        $('#seafer-load').hide();
    });
</script> --}}
<script src="{!! asset(null) !!}assets/js/foundation.min.js"></script>
<script src="{!! asset(null) !!}assets/js/controller.js"></script>
<script src="{!! asset(null) !!}assets/js/retina.js"></script>
<script src="{!! asset(null) !!}assets/js/jquery.typeahead.js"></script>
<script src="{!! asset(null) !!}plugin/inputmask/dist/jquery.mask.min.js"></script>
@yield('scripts')
<script type="text/javascript" src="//seafermart.co.id:8000/livechat/php/app.php?widget-init.js"></script>
{{-- <!-- Firebase -->
<script src="https://www.gstatic.com/firebasejs/3.3.0/firebase.js"></script>

<!-- Firechat -->
<link rel="stylesheet" href="https://cdn.firebase.com/libs/firechat/3.0.1/firechat.min.css" />
<script src="https://cdn.firebase.com/libs/firechat/3.0.1/firechat.min.js"></script> --}}
{{-- Let's use 4 spaces since it's a Blade file --}}
<script type="text/javascript">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
    @if(Session::has('alert-' . $msg))
    $('#modal-flash').foundation('open', 'open');
    @endif
    @endforeach

    var url = function(suffix) {
        suffix = (typeof suffix !== 'undefined') ? '/' + suffix : '';
        return '{{ url() }}' + suffix;
    };

    {!! file_get_contents(public_path('js/scripts.js')) !!}

  jQuery( document ).ready(function( $ ) {
        $("#toTop").scrollToTop();
        $('#log-me-out').on('click', function(){
            phpLiveChat.endChat();
        });
        $.getJSON( "{{ url('groceries/all_data') }}", callbackFuncWithData);

        function callbackFuncWithData(data)
        {
            $.typeahead({
                input: ".search",
                minLength: 2,
                maxItem: 6,
                order: "asc",
                hint: true,
                backdrop: {
                    "background-color": "#ccc"
                },
                emptyTemplate: 'No result',
                dynamic:true,
                href:"http://seafermart.co.id/groceries/@{{id}}/@{{slug}}",
                source: {
                    name:{
                        display: "name",
                        data: data.data
                    }
                },
                callback: {
                    
                },
                debug: false
            });
        }
    });


    // for hidden chat if mobile detect browser
    // jQuery( document ).ready(function( $ ) {
    //     if ($(window).width() < 960) {
    //         console.log('Less than 960');
    //          $("iframe#customer-chat-iframe").contents().find("#customer-chat-widget").hide();
    //     }
    //     else {
    //         console.log('More than 960');
    //     }
    // });

    /*!
     jQuery scrollTopTop v1.0 - 2013-03-15
     (c) 2013 Yang Zhao - geniuscarrier.com
     license: http://www.opensource.org/licenses/mit-license.php
     */
    (function($) {
        $.fn.scrollToTop = function(options) {
            var config = {
                "speed" : 800
            };

            if (options) {
                $.extend(config, {
                    "speed" : options
                });
            }

            return this.each(function() {

                var $this = $(this);

                $(window).scroll(function() {
                    if ($(this).scrollTop() > 100) {
                        $this.fadeIn();
                    } else {
                        $this.fadeOut();
                    }
                });

                $this.click(function(e) {
                    e.preventDefault();
                    $("body, html").animate({
                        scrollTop : 0
                    }, config.speed);
                });

            });
        };
    })(jQuery);
</script>

</body>
</html>