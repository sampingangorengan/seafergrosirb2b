w@extends('layouts.newmaster')

@section('content')

  <div class="box-content" id="productDetail">
    <div class="row">
      <div class="small-12 columns">
        <h6 class="title">PRODUCT PAGE</h6>
        <center><img class="hero" src="{{ asset(null) }}assets/images/img-grocer.png"></center>
      </div>

      <div class="small-3 columns">
       <ul class="side-nav list-style">
          @foreach (Category::orderBy('order')->get() as $category)
            <li class="{{ $activeCategoryId == $category->id ? 'active' : '' }}">
              <a href="{!! url('groceries/' . $category->uri) !!}">{!! strtoupper($category->name) !!}</a>
            </li>
          @endforeach

          <li><a href="#" class="text-orange">GIFTS</a></li>
          <li><a href="#" class="text-green">MERCHANDISE</a></li>
          <li><a href="#" class="text-purple">PARTY & FAMILY</a></li>

       </ul>
      </div>
      
      <div class="small-7 columns">
        @yield('middle')
      </div>
      
      <div class="small-2 columns">
        <div class="recently-viewed-box">
          <div class="header">
            RECENTLY VIEWED
          </div>
          <div class="body">
            <ul class="recomendation-box">
              <li>
                <a href="#">
                  <div class="item">
                    <img src="{{ asset(null) }}assets/images/box5.jpg">
                  </div>
                  <span><b>Salmon Head</b></span>
                  <span>IDR 50.000/100g</span>  
                </a>
              </li>
              <li>
                <a href="#">
                  <div class="item">
                    <img src="{{ asset(null) }}assets/images/box4.jpg">
                  </div>
                  <span><b>Edamame</b></span>
                  <span>IDR 50.000/1kg</span>
                </a>
              </li>
              <li>
                <a href="#">
                  <div class="item">
                    <img src="{{ asset(null) }}assets/images/box2.png">
                  </div>
                  <span><b>Carrot</b></span>
                  <span>IDR 12.000/100g</span>
                </a>
              </li>
              <li>
                <a href="#">
                  <div class="item">
                    <img src="{{ asset(null) }}assets/images/box6.jpg">
                  </div>
                  <span><b>Bell Papper</b></span>
                  <span>IDR 5.000/100g</span>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection