<!DOCTYPE html>
<html>
<head>
  @include('layouts.htmlheader')
</head>
<body>
@include('layouts.header')

@yield('content')

@include('layouts.footer')
<div class="copyright">
  <div class="box-content">
    <span>&copy; 2018 SeaferMart All Rights Reserved</span>
  </div>
</div>

<script src="{!! asset(null) !!}assets/js/jquery.min.js"></script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="{!! asset(null) !!}assets/js/foundation.min.js"></script>
<script src="{!! asset(null) !!}assets/js/controller.js"></script>
<script src="{!! asset(null) !!}plugin/inputmask/dist/jquery.mask.min.js"></script>
@yield('scripts')

{{-- Let's use 4 spaces since it's a Blade file --}}
<script type="text/javascript">
    var url = function(suffix) {
        suffix = (typeof suffix !== 'undefined') ? '/' + suffix : '';
        return '{{ url() }}' + suffix;
    };

    {!! file_get_contents(public_path('js/scripts.js')) !!}

  jQuery( document ).ready(function( $ ) {
        $("#toTop").scrollToTop();
    });

    /*!
     jQuery scrollTopTop v1.0 - 2013-03-15
     (c) 2013 Yang Zhao - geniuscarrier.com
     license: http://www.opensource.org/licenses/mit-license.php
     */
    (function($) {
        $.fn.scrollToTop = function(options) {
            var config = {
                "speed" : 800
            };

            if (options) {
                $.extend(config, {
                    "speed" : options
                });
            }

            return this.each(function() {

                var $this = $(this);

                $(window).scroll(function() {
                    if ($(this).scrollTop() > 100) {
                        $this.fadeIn();
                    } else {
                        $this.fadeOut();
                    }
                });

                $this.click(function(e) {
                    e.preventDefault();
                    $("body, html").animate({
                        scrollTop : 0
                    }, config.speed);
                });

            });
        };
    })(jQuery);
</script>
<!-- rendering account -->
<{{--script type="text/javascript">
    $("#myLogin").on('click', function(event) {
        event.preventDefault();
        //window.location = "my-account.html";
    });
</script>--}}

</body>
</html>