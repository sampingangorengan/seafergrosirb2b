@extends('layouts.newmaster')

@section('title', App\Page::createTitle('Suggest Product'))

@section('customcss')
    <link rel="stylesheet" type="text/css" href="{!! asset(null) !!}assets/css/custom.css">
@endsection

@section('content')

    <div class="row" style="margin-top: 7px;">
        <div class="small-12 columns">
            <span class="text-grey equal">
              <a class="text-grey" href="{{ url('/') }}"><b>HOME</b></a> /
              <a class="text-grey" href="#"><b>SUGGEST A PRODUCT</b></a>

            </span>
        </div>
    </div>
    <div id="boxOfContent">

        <div class="suggest-product-box" id="checkout">

            <div class="box-content">
                <div class="row">
                    <div class="small-12 columns">
                        <h5 class="title title-margin">SUGGEST A PRODUCT</h5>
                        <p class="after-title">
                            If a product that you love is not available at SeaferMart, do let us know.<br>
                            We are happy to accommodate new products for you.
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="small-3 columns">
                        <div class="buah-box">
                            <img class="jeruk" src="{!! asset(null) !!}assets/images/img-orange-color.png">
                            <img class="brokoli" src="{!! asset(null) !!}assets/images/img-brocoly-color.png">
                        </div>
                    </div>
                    <div class="small-6 columns">
                        <!-- <div class="flash-message">
                            @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                                @if(Session::has('alert-' . $msg))

                                    <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                                @endif
                            @endforeach
                        </div> -->
                        {!! Form::open(['method'=>'POST', 'action'=>['ProductSuggestionController@store'], 'files' => true]) !!}
                        <div class="form-box">
                            <div class="row">
                                <div class="small-4 columns">
                                    <label for="middle-label" class="text-left middle "><span class="required">PRODUCT NAME</span></label>
                                </div>
                                <div class="small-8 columns">
                                    {!! Form::text('product_name', null, ['class'=>'form-control', 'required' => 'required']) !!}
                                </div>
                            </div>
                            <div class="row">
                                <div class="small-4 columns">
                                    <label for="middle-label" class="text-left middle "><span class="required">BRAND NAME</span></label>
                                </div>
                                <div class="small-8 columns">
                                    {!! Form::text('brand_name', null, ['class'=>'form-control', 'required' => 'required']) !!}
                                </div>
                            </div>
                            <div class="row">
                                <div class="small-4 columns">
                                    <label for="middle-label" class="text-left middle">CATEGORY</label>
                                </div>
                                <div class="small-8 columns">
                                    <select class="short" name="category" required="required">
                                        <option value="0">Please select a category.</option>
                                        @foreach(App\Models\Groceries\Category::orderBy('id', 'asc')->where('is_active', 1)->get() as $category)
                                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                                        @endforeach
                                        {{-- <option value="0">Instafood</option>
                                        <option value="1">Instadrink</option>
                                        <option value="2">Instagram</option> --}}
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="small-4 columns">
                                    <label for="middle-label" class="text-left middle ">COMMENTS</label>
                                </div>
                                <div class="small-8 columns">
                                    <textarea rows="3" name="comments"></textarea>
                                </div>
                            </div>
                            <div class="row">
                                <div class="small-4 columns">
                                    <label for="middle-label" class="text-left middle "><span class="required">EMAIL ADDRESS</span></label>
                                </div>
                                <div class="small-8 columns">
                                    {!! Form::text('email', null, ['class'=>'form-control', 'required' => 'required']) !!}
                                </div>
                            </div>
                            <div class="row">
                                <div class="small-4 columns">
                                    <label for="middle-label" class="text-left middle ">UPLOAD PHOTO</label>
                                </div>
                                <div class="small-8 columns">
                                    <div class="preview img-wrapper simple"></div>
                                    <div class="file-upload-wrapper custom">
                                        <input type="file" name="file" class="file-upload-native apalah" accept="image/*"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="small-4 columns">
                                    &nbsp;
                                </div>
                                <div class="small-8 columns">
                                    <button class="button primary btn-aneh">SUBMIT</button>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <div class="small-3 columns">
                        <div class="buah-box">
                            <img class="apel" src="{!! asset(null) !!}assets/images/img-apple-color.png">
                            <img class="wortel" src="{!! asset(null) !!}assets/images/img-carrot-color.png">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection