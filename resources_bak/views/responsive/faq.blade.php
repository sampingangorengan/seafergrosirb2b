@extends('responsive.layout.master')

@section('title', App\Page::createTitle('Frequently Asked Question'))

@section('customcss')
    <link rel='stylesheet' href='{{ asset(null) }}responsive/bootstrap/css/bootstrap-responsive-tabs.css'>
    <link rel="stylesheet" type="text/css" href="{{ asset(null) }}responsive/css/jquery.fancybox.min.css">
@endsection

@section('content')
<div class="main-content">
	<div class="container"  id="becomeAMember">
        <div class="row hide-768">
			<div class="col-md-12">
				<span class="text-grey equal margin-top-10">
				  <a class="text-grey" href="{{ url('/') }}"><b>HOME</b></a> /
				  <a class="text-grey" href="#"><b>FAQ</b></a>
				</span>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<h5 class="title title-margin">FAQ</h5>
				<p class="after-title hide-768">
					If a product that you love is not available at SeaferMart, do let us know.<br>
					We are happy to accommodate new products for you.
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 col-sm-4 hide-320">
                    <ul class="side-nav side-nav-become-box" id="menu-collapse">
                        <li><a class="about-us active" data-toggle="collapse" data-parent="#accordion" href="#about-us">ABOUT US</a></li>
                        <li><a href="#loyalty-rewards" data-toggle="collapse" data-parent="#accordion" class="loyalty-rewards">CASHBACK REWARDS</a></li>
                        <li><a href="#orders" data-toggle="collapse" data-parent="#accordion" class="orders">ORDERS</a></li>
                        <li><a href="#products" data-toggle="collapse" data-parent="#accordion" class="products">PRODUCTS</a></li>
                        <li><a href="#delivery" data-toggle="collapse" data-parent="#accordion" class="delivery">DELIVERY</a></li>
                        <li><a href="#payments" data-toggle="collapse" data-parent="#accordion" class="payments">PAYMENTS</a></li>
                        <li><a href="#trobuleshooting" data-toggle="collapse" data-parent="#accordion" class="trobuleshooting">TROUBLESHOOTING</a></li>
                        <li><a href="#grocer" data-toggle="collapse" data-parent="#accordion" class="grocer">ABOUT SEAFER GROCER</a></li>
                    </ul>
                </div>
                <div class="col-md-8 col-sm-8">
                    <div class="faq-box">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#about-us" aria-expanded="true">
											ABOUT US
										</a>
									</h4>
								</div>
                                <div id="about-us" class="panel-collapse collapse in">
									<div class="panel-body">
                                    <table>
                                        <tbody>
                                        <tr>
                                            <td>Q:</td>
                                            <td>What is SeaferMart?</td>
                                        </tr>
                                        <tr>
                                            <td>A:</td>
                                            <td>
                                                SeaferMart is a virtual universe that offers the best prices on a wide range of imported and local grocery as well as InstaFood with the convenience of having them delivered straight to your door.
                                                At SeaferMart, you can also share your recipes and shop directly from them. Still have questions about us? Come on, click this link and Kirin will show you around!
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table>
                                        <tbody>
                                        <tr>
                                            <td>Q:</td>
                                            <td>Why do you have Japanese kanji under your logo? Are you a Japanese online store? </td>
                                        </tr>
                                        <tr>
                                            <td>A:</td>
                                            <td>
                                                No, we are not a Japanese online store. We are an Indonesian based company that operates under PT. SeaferMart Jaya Raya. The reason why we have Japanese kanji under our logo is because we are inspired by the Japanese culture of “Omotenashi” which means giving the best service to our customers whole heartedly. We sell not only Japanese products, but also other great selection of imported and local products.
                                        </tr>
                                        </tbody>
                                    </table>
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
								  <h4 class="panel-title">
									<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#loyalty-rewards" aria-expanded="false">
										CASHBACK REWARDS
									</a>
									
									</h4>
								</div>
								<div id="loyalty-rewards" class="panel-collapse collapse">
									<div class="panel-body">
										<table>
											<tbody>
											<tr>
												<td>Q:</td>
												<td>What is shop with points?</td>
											</tr>
											<tr>
												<td>A:</td>
												<td>
													You will accumulate points as you shop with us and you can redeem your points accordingly upon checkout.<!--  For more information, please <a href="#">click here</a>. -->
											</tr>
											</tbody>
										</table>
									</div>
                                </div>
                            </div>
							<div class="panel panel-default">
								<div class="panel-heading">
								  <h4 class="panel-title">
									<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#orders" aria-expanded="false">
										ORDERS
									</a>
									</h4>
								</div>
                                <div id="orders" class="panel-collapse collapse">
									<div class="panel-body">
                                    <table>
                                        <tbody>
                                        <tr>
                                            <td>Q:</td>
                                            <td>How do I check my order status? </td>
                                        </tr>
                                        <tr>
                                            <td>A:</td>
                                            <td>You can log in to your account and check it from your order history.</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table>
                                        <tbody>
                                        <tr>
                                            <td>Q:</td>
                                            <td>Can I change my orders? And how?</td>
                                        </tr>
                                        <tr>
                                            <td>A:</td>
                                            <td>
                                                Seafer Grocer – Yes, you can change your orders if they are still in “Payment Due” or “Processed” status. You can just simply log in to your account and follow the steps shown on your order page.
                                                Seafer Eats – Any changes in Seafer Eats orders, you can contact the vendor directly from our app.</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table>
                                        <tbody>
                                        <tr>
                                            <td>Q:</td>
                                            <td>Can I cancel my order?</td>
                                        </tr>
                                        <tr>
                                            <td>A:</td>
                                            <td>
                                                Yes, you can. If you have not transferred your payment due within 24 hours, your orders will be automatically canceled.
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table>
                                        <tbody>
                                        <tr>
                                            <td>Q:</td>
                                            <td>Can I return the received items? And how?</td>
                                        </tr>
                                        <tr>
                                            <td>A:</td>
                                            <td>
                                                We do not accept any returns because we ensure you that the goods we deliver have passed through a strict QC (Quality Control).
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table>
                                        <tbody>
                                        <tr>
                                            <td>Q:</td>
                                            <td>What happens to my cart if I leave before checking out?</td>
                                        </tr>
                                        <tr>
                                            <td>A:</td>
                                            <td>
                                                If you have already an account at SeaferMart, we will save the items that you added into your cart and sync them across your devices. However, we are unable to guarantee that the items will still be in stock when you return, as placing an available item in your cart does not reserve them for your order.
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table>
                                        <tbody>
                                        <tr>
                                            <td>Q:</td>
                                            <td>When will you ship the pre-order goods?</td>
                                        </tr>
                                        <tr>
                                            <td>A:</td>
                                            <td>
                                                Seafer Grocer – Yes, you can change your orders if they are still in “Payment Due” or “Processed” status. You can just simply log in to your account and follow the steps shown on your order page.
                                                Seafer Eats – Any changes in Seafer Eats orders, you can contact the vendor directly from our app.
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table>
                                        <tbody>
                                        <tr>
                                            <td>Q:</td>
                                            <td>Can I order in bulk?</td>
                                        </tr>
                                        <tr>
                                            <td>A:</td>
                                            <td>
                                                Yes, you can. However, our order fulfillment policy for wholesale order is different from the retail one. You must first log in to our <!-- <a href="#">wholesale</a> -->wholesale page and follow the steps shown there.
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
									</div>
								</div>
                            </div>
							<div class="panel panel-default">
								<div class="panel-heading">
								  <h4 class="panel-title">
									<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#products" aria-expanded="false">
										PRODUCTS
									</a>
								  </h4>
								</div>
                                <div id="products" class="panel-collapse collapse">
									<div class="panel-body">
                                    <table>
                                        <tbody>
                                        <tr>
                                            <td>Q:</td>
                                            <td>How do you treat halal and non-halal products?</td>
                                        </tr>
                                        <tr>
                                            <td>A:</td>
                                            <td>We separate the storage and packing for halal and non-halal products.</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table>
                                        <tbody>
                                        <tr>
                                            <td>Q:</td>
                                            <td>How do you ensure that the groceries you deliver will still arrive fresh?</td>
                                        </tr>
                                        <tr>
                                            <td>A:</td>
                                            <td>
                                                Unlike other grocery delivery services, we keep our own products safe in our temperature controlled state-of-the-art warehouses. In addition to that, our products have also passed through a strict QC.
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table>
                                        <tbody>
                                        <tr>
                                            <td>Q:</td>
                                            <td>What if the product that I want to buy is out of stock?</td>
                                        </tr>
                                        <tr>
                                            <td>A:</td>
                                            <td>
                                                You can opt to be notified when the product is available again on our page.
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table>
                                        <tbody>
                                        <tr>
                                            <td>Q:</td>
                                            <td>What if you didn’t sell a product that I would like to buy?</td>
                                        </tr>
                                        <tr>
                                            <td>A:</td>
                                            <td>
                                                We’d love to hear your suggestions on what you would like to buy! Please suggest a product to us, so that we can look into stocking it for your future purchases.
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table>
                                        <tbody>
                                        <tr>
                                            <td>Q:</td>
                                            <td>How are your prices compared to stores?</td>
                                        </tr>
                                        <tr>
                                            <td>A:</td>
                                            <td>
                                                We are the cheapest!
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table>
                                        <tbody>
                                        <tr>
                                            <td>Q:</td>
                                            <td>How do you ensure that the groceries you deliver will still arrive fresh?</td>
                                        </tr>
                                        <tr>
                                            <td>A:</td>
                                            <td>
                                                Unlike other grocery delivery services, we keep our own products safe in our temperature controlled state-of-the-art warehouses. In addition to that, our products have also passed through a strict QC.
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
									</div>
								</div>
                            </div>
							<div class="panel panel-default">
								<div class="panel-heading">
								  <h4 class="panel-title">
									<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#delivery" aria-expanded="false">
										DELIVERY
									</a>
								   </h4>
								</div>
                                <div id="delivery" class="panel-collapse collapse">
									<div class="panel-body">
                                    <table>
                                        <tbody>
                                        <tr>
                                            <td>Q:</td>
                                            <td>Which areas do you serve?</td>
                                        </tr>
                                        <tr>
                                            <td>A:</td>
                                            <td>
                                                SeaferMart is currently serving Jabodetabek (Jakarta, Bogor, Depok, Tangerang, and Bekasi) area.
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table>
                                        <tbody>
                                        <tr>
                                            <td>Q:</td>
                                            <td>How fast do you deliver?</td>
                                        </tr>
                                        <tr>
                                            <td>A:</td>
                                            <td>
                                                For Seafer Grocer products<br>
                                                <ul class="circle">
                                                    <li>
                                                        If you order before 10 AM, we will deliver between 12-6 PM.
                                                    </li>
                                                    <li>
                                                        If you order before 2 PM, we will deliver between 4-10 PM.
                                                    </li>
                                                    <li>
                                                        If you order after 2 PM, we will deliver on the next day between 9 AM-5 PM.
                                                    </li>
                                                </ul><br>
                                                For Seafer Eats products
                                                <p>
                                                    Your products will be delivered separately from Seafer Grocer items as the sellers will  directly send them to you. Your delivery schedule for Seafer Eats items will be shown when   you check out.
                                                </p>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table>
                                        <tbody>
                                        <tr>
                                            <td>Q:</td>
                                            <td>how much is the delivery fee?</td>
                                        </tr>
                                        <tr>
                                            <td>A:</td>
                                            <td>
                                                The delivery fee will be shown upon your checkout on your Gojek application. You will be charged Rp 15.000,00 if your location is within 25 km, but you will have to pay extra Rp 2.500,00 per additional km.
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table>
                                        <tbody>
                                        <tr>
                                            <td>Q:</td>
                                            <td>Who will deliver my orders?</td>
                                        </tr>
                                        <tr>
                                            <td>A:</td>
                                            <td>
                                                Currently we are working with Gojek.
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table>
                                        <tbody>
                                        <tr>
                                            <td>Q:</td>
                                            <td>What if I never received my orders?</td>
                                        </tr>
                                        <tr>
                                            <td>A:</td>
                                            <td>
                                                You have an option to get a refund (transfer/cash) or full order replacement within 3 days.
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table>
                                        <tbody>
                                        <tr>
                                            <td>Q:</td>
                                            <td>What if I’m not home for delivery?</td>
                                        </tr>
                                        <tr>
                                            <td>A:</td>
                                            <td>
                                                Upon your request and delivery instructions, we can leave your order with a household assistant, security guard, friend or family. Please be mindful of frozen or perishable goods. However, we have the right to cancel any orders if/when a customer does not confirm their order prior to delivery. If after 20 minutes after arrival of Gojek drivers, a customer is unable to be reached, we have the right to reschedule or cancel the order because it has an impact on our resources and costs, which in turn means that we are unable to serve you and your fellow customers to the best of our ability.
                                                <br><br>
                                                We also have the right to charge you a restocking fee of Rp 50.000,00 for fresh and frozen items in your order that cannot be restocked. This charge will be automatically deducted from your accumulate point balance. Otherwise, if your point balance is not sufficient, the remaining penalty amount will be charge to your next order. We reserve the right to deny future service to customers who are repeatedly absent for their deliveries.
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
									</div>
                                </div>
                            </div>
							<div class="panel panel-default">
								<div class="panel-heading">
								  <h4 class="panel-title">
									<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#payments" aria-expanded="false">
										PAYMENT
									</a>
								   </h4>
								</div>
                                <div id="payments" class="panel-collapse collapse">
									<div class="panel-body">
                                    <table>
                                        <tbody>
                                        <tr>
                                            <td>Q:</td>
                                            <td>What are the payment methods at SeaferMart? And how?</td>
                                        </tr>
                                        <tr>
                                            <td>A:</td>
                                            <td>
                                                Bank transfer, ATM, M-Banking, Internet Banking, CC
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table>
                                        <tbody>
                                        <tr>
                                            <td>Q:</td>
                                            <td>How secure is your Credit Card payment method?</td>
                                        </tr>
                                        <tr>
                                            <td>A:</td>
                                            <td>
                                                We work together with a trusted and reliable payment gateway provider – VeriTrans, who has been certified with lalala
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
								  <h4 class="panel-title">
									<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#trobuleshooting" aria-expanded="false">
										TROUBLESHOOTING
									</a>
								   </h4>
								</div>
                                <div id="trobuleshooting" class="panel-collapse collapse">
									<div class="panel-body">
                                    <table>
                                        <tbody>
                                        <tr>
                                            <td>Q:</td>
                                            <td>What if I didn’t receive the activation email?</td>
                                        </tr>
                                        <tr>
                                            <td>A:</td>
                                            <td>
                                                Just wait for a few moments and check your Spam or Junk folder if you didn’t receive any email in your Inbox.
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table>
                                        <tbody>
                                        <tr>
                                            <td>Q:</td>
                                            <td>What if I forgot my password? (Updated apps & forgotten password)</td>
                                        </tr>
                                        <tr>
                                            <td>A:</td>
                                            <td>
                                                To re-set your password, follow the 'FORGOTTEN PASSWORD' instructions on the SIGN IN page. Please note, for security reasons we are unable to send your old password via email.
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table>
                                        <tbody>
                                        <tr>
                                            <td>Q:</td>
                                            <td>How do I unsubscribe from your email?</td>
                                        </tr>
                                        <tr>
                                            <td>A:</td>
                                            <td>
                                                To unsubscribe from our marketing communications, please click the unsubscribe button at the bottom of our emails.
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
									</div>
                                </div>
                            </div>
							<div class="panel panel-default">
								<div class="panel-heading">
								  <h4 class="panel-title">
									<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#grocer" aria-expanded="false">
										ABOUT SEAFER GROCER
									</a>
								   </h4>
								</div>
                                <div id="grocer" class="panel-collapse collapse">
									<div class="panel-body">
                                    <table>
                                        <tbody>
                                        <tr>
                                            <td>Q:</td>
                                            <td>What if I want to be a supplier?</td>
                                        </tr>
                                        <tr>
                                            <td>A:</td>
                                            <td>
                                                Please send your business proposal to <span class="email">supply@SeaferMart.co.id</span> and we will get back to you shortly!
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
									</div>
                                </div>
                           </div>
                        </div>
                    </div>
                </div>
		</div>
	</div>
	
</div>
@endsection

@section('scripts')
<script src="{{ asset(null) }}responsive/bootstrap/js/jquery.bootstrap-responsive-tabs.min.js"></script>
<script src="{{ asset(null) }}responsive/js/jquery.fancybox.min.js"></script>
@endsection