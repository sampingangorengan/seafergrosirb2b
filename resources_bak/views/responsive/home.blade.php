@extends('responsive.layout.master')

@section('content')
<div class="main-content producthome">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 no-padding-h">
			<!-- Slider -->
				<div id="myCarousel" class="carousel slide" data-ride="carousel">
					<!-- Indicators -->
					<ol class="carousel-indicators">
						<?php $i = 0 ?>
						@for ($i = 0; $i < App\Models\Home\Slide::where('is_active', 1)->count() - 1; $i++)
							<?php $class = '' ?>
							@if ($i == 0)
								<?php $class = 'is-active' ?>
							@endif
					  		<li data-target="#myCarousel" data-slide-to="{{ $i }}" class="{{ $class }}"></li>
					  	@endfor
					</ol>

					<!-- Wrapper for slides -->
					<div class="carousel-inner">
						<?php $i = 0 ?>
						@foreach (App\Models\Home\Slide::where('is_active', 1)->orderBy('order')->get() as $slide)
							<?php $class = '' ?>
							@if ($i == 0)
								<?php $class = 'active' ?>
							@endif
						  	<div class="item {{ $class }}">
								<img src="{!! asset(null) !!}headlines/{{ $slide->file_name }}" style="width:100%;">
						  	</div>
						  	<?php $i++ ?>
					  	@endforeach
					</div>

					<!-- Left and right controls -->
					<a class="left carousel-control" href="#myCarousel" data-slide="prev">
					  <span class="glyphicon glyphicon-chevron-left"></span>
					  <span class="sr-only">Previous</span>
					</a>
					<a class="right carousel-control" href="#myCarousel" data-slide="next">
					  <span class="glyphicon glyphicon-chevron-right"></span>
					  <span class="sr-only">Next</span>
					</a>
				</div>
			</div>
			@if($groceries_body)
			<div class="row">
				<div class="col-md-3">
					<div class="side-card">
					  <div class="card grocer">
						<div class="card-content">
						  <div class="img-box">
							<img src="{{ asset(null) }}responsive/images/img-grocer.png">
						  </div>
						  <p class="text-box">
							A place to help you pick and deliver the best quality of imported Japanese groceries right to your front doors.
						  </p>
						</div>
						<a href="{!! url('/groceries?show=all') !!}">
						  <button class="btn">GO TO GROCER</button>
						  <span>VIEW ALL</span>
						</a>
					  </div>
					</div>
				</div>
				<div class="col-md-9">
					@foreach($groceries_body as $key=>$item)

					<div class="col-md-3 col-sm-3 col-xs-4">
						<a href="{{ url('groceries/'.$item->id.'/'.str_slug($item->name)) }}">
							<div class="cube">
							  	<div class="img-detail">
							  		<img src="{{ Groceries::getFirstImageUrl($item->id) }}" alt="{{ $item->name }}">
							  	</div>
								<div class="detail-box bottom">

							  	<h6>{{ $item->name }}</h6>
							  	<p></p>
							  	<span>{{ Money::display($item->price) }}</span>
							</div>
						  </div>   
						</a>
					</div>
					
					@endforeach
				
				</div>
			</div>
			@endif
			{{-- <div class="row">
				<div class="col-md-3">
					<div class="side-card">
					  <div class="card cook">
						<div class="card-content">
						  <div class="img-box">
							<img src="{{ asset(null) }}responsive/images/img-cook.png">
						  </div>
						  <p class="text-box">
							We create a community portal to house a melting pot of genius famous chefs, food bloggers, and skilled housewives who will share
						  </p>
						</div>
						<a href="groceriese7a9.html?show=all">
						  <button class="btn">GO TO COOK</button>
						  <span>VIEW ALL</span>
						</a>
					  </div>
					</div>
				</div>
				<div class="col-md-9">
					<div class="col-md-6 col-sm-6">
						<a href="groceries/27/vegemil-gomen-camke-black-sesame-soymilk-190-ml.html">
						  <div class="cube" style="background-image:url({{ asset(null) }}responsive/contents/149391455427.png);">
							<div class="detail-box bottom">

							  <h6>Vegemil Gomen Camke (Black Sesame Soymilk) 190 ML</h6>
							  <p>
							 </p>
							  <span>Rp 11.000</span>

							</div>
						  </div>   
						</a>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-6">
						<a href="groceries/31/nongshim-bowl-noodle-kimchi-flavor-86-gr.html">
						  <div class="cube" style="background-image:url({{ asset(null) }}responsive/contents/149391268631.png);">
							<div class="detail-box bottom">

							  <h6>Nongshim Bowl Noodle Kimchi Flavor 86 GR</h6>
							  <p>
														  A mixture of fre......
													  </p>
							  <span>Rp 13.000</span>

							</div>
						  </div>   
						</a>
					</div>
					<div class="hide-320 col-md-6 col-sm-6">
						<div class="col-md-6 col-sm-6 col-xs-6">
							<a href="groceries/42/nongshim-shin-ramyun-120-gr.html">
							  <div class="cube bg-red" style="background-image:url({{ asset(null) }}responsive/contents/149391333842.png);">
								<div class="detail-box">

								  <h6>Nongshim Shin Ramyun 120 GR</h6>
								  <p>
																</p>
								  <!-- <span>IDR 55K</span> -->
								</div>
							  </div>
							</a>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-6">
							<a href="groceries/43/nonghsim-shin-ramyun-black-130-gr.html">
							  <div class="cube bg-red" style="background-image:url({{ asset(null) }}responsive/contents/149391329943.png);">
								<div class="detail-box">

								  <h6>Nonghsim Shin Ramyun Black 130 GR</h6>
								  <p>
																</p>
								  <!-- <span>IDR 55K</span> -->
								</div>
							  </div>
							</a>
						</div>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-6">
						 <a href="groceries/41/nongshim-seafood-ramyun-125-gr.html">
						  <div class="cube" style="background-image:url({{ asset(null) }}responsive/contents/149391325341.png);">
							<div class="detail-box bottom">

							  <h6>Nongshim Seafood Ramyun 125 GR</h6>
							  <p>
														  Nongshim&#039;s Seafo......
													  </p>
							  <span>Rp 12.500</span>

							</div>
						  </div>   
						</a>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3">
					<div class="side-card">
					  <div class="card eats">
						<div class="card-content">
						  <div class="img-box">
							<img src="{{ asset(null) }}responsive/images/img-eats.png">
						  </div>
						  <p class="text-box">
							The ﬁrst ever food marketplace in Indonesia where we work together with local InstaFood sellers with their mouthwatering creations to
						  </p>
						</div>
						<a href="groceriese7a9.html?show=all">
						  <button class="btn">GO TO EATS</button>
						  <span>VIEW ALL</span>
						</a>
					  </div>
					</div>
				</div>
				<div class="col-md-9">
					<div class="col-md-6 col-sm-6">
						<a href="groceries/27/vegemil-gomen-camke-black-sesame-soymilk-190-ml.html">
						  <div class="cube" style="background-image:url({{ asset(null) }}responsive/contents/149391455427.png);">
							<div class="detail-box bottom">

							  <h6>Vegemil Gomen Camke (Black Sesame Soymilk) 190 ML</h6>
							  <p>
							 </p>
							  <span>Rp 11.000</span>

							</div>
						  </div>   
						</a>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-6">
						<a href="groceries/31/nongshim-bowl-noodle-kimchi-flavor-86-gr.html">
						  <div class="cube" style="background-image:url({{ asset(null) }}responsive/contents/149391268631.png);">
							<div class="detail-box bottom">

							  <h6>Nongshim Bowl Noodle Kimchi Flavor 86 GR</h6>
							  <p>
														  A mixture of fre......
													  </p>
							  <span>Rp 13.000</span>

							</div>
						  </div>   
						</a>
					</div>
					<div class="hide-320 col-md-6 col-sm-6">
						<div class="col-md-6 col-sm-6 col-xs-6">
							<a href="groceries/42/nongshim-shin-ramyun-120-gr.html">
							  <div class="cube bg-red" style="background-image:url({{ asset(null) }}responsive/contents/149391333842.png);">
								<div class="detail-box">

								  <h6>Nongshim Shin Ramyun 120 GR</h6>
								  <p>
																</p>
								  <!-- <span>IDR 55K</span> -->
								</div>
							  </div>
							</a>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-6">
							<a href="groceries/43/nonghsim-shin-ramyun-black-130-gr.html">
							  <div class="cube bg-red" style="background-image:url({{ asset(null) }}responsive/contents/149391329943.png);">
								<div class="detail-box">

								  <h6>Nonghsim Shin Ramyun Black 130 GR</h6>
								  <p>
																</p>
								  <!-- <span>IDR 55K</span> -->
								</div>
							  </div>
							</a>
						</div>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-6">
						 <a href="groceries/41/nongshim-seafood-ramyun-125-gr.html">
						  <div class="cube" style="background-image:url({{ asset(null) }}responsive/contents/149391325341.png);">
							<div class="detail-box bottom">

							  <h6>Nongshim Seafood Ramyun 125 GR</h6>
							  <p>
														  Nongshim&#039;s Seafo......
													  </p>
							  <span>Rp 12.500</span>

							</div>
						  </div>   
						</a>
					</div>
				</div>
			</div> --}}
		</div>
	</div>
</div>
<!-- Feature -->
<div class="feature-box">
  <div class="container">
	<ul>
	  <li class="sd"><a href="#"><img src="{{ asset(null) }}responsive/images/imgpsh_fullsize.png"><span>SAME / NEXT DAY DELIVERY</span></a></li>
	  <li class="lp"><a href="#"><img src="{{ asset(null) }}responsive/images/imgpsh_fullsize-5.png"><span>CASHBACK REWARDS</span></a></li>
	  <li class="hs"><a href="#"><img src="{{ asset(null) }}responsive/images/imgpsh_fullsize-3.png"><span>HUGE SELECTION</span></a></li>
	  <li class="qa"><a href="#"><img src="{{ asset(null) }}responsive/images/imgpsh_fullsize-4.png"><span>QUALITY ASSURANCE</span></a></li>
	  <li class="bp"><a href="#"><img src="{{ asset(null) }}responsive/images/imgpsh_fullsize-2.png"><span>BEST PRICE GUARANTEE</span></a></li>
	</ul>
  </div>
</div>
@endsection