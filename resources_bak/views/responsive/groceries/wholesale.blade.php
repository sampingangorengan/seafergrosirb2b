@extends('responsive.layout.master_revamp')

@section('title', App\Page::createTitle( 'Wholesale Page' ))

@section('customcss')
    <link rel='stylesheet' href='{{ asset(null) }}responsive/bootstrap/css/bootstrap-responsive-tabs.css'>
    <link rel="stylesheet" type="text/css" href="{{ asset(null) }}responsive/css/jquery.fancybox.min.css">
@endsection

@section('content')
	<section class="wholesale-form">
	  	<div class="row">
	    	<div class="col-md-12 columns">
	      		<h5 class="title title-margin">WHOLE SALE</h5>
	      		<p class="desc with-border">If you want to be a reseller or buy our products at a wholesale volume/prices, please fill up the form below and our wholesale team will contact you immediately!</p>
    		</div>
	  	</div>
	  	<div class="row">
	  		<form method="post" action="">

	    	<div class="col-md-7 small-centered">
	      		<div class="form-box">
	        		<div class="row">
	          			<div class="col-md-4 columns">
	            			<label for="middle-label" class="text-left"><span class="required">COMPANY NAME</span></label>
	          			</div>
	          			<div class="col-md-8 columns">
	            			{!! Form::text('company_name', null, ['placeholder' => 'Company Name', 'required' => 'required']) !!}
	          			</div>
	        		</div>
	        		<div class="row">
	          			<div class="col-md-4 columns">
	            			<label for="middle-label" class="text-left"><span class="required">PIC NAME</span></label>
	          			</div>
	          			<div class="col-md-8 columns">
	            			{!! Form::text('pic_name', null, ['placeholder' => 'Person in Charge Name', 'required' => 'required']) !!}
	          			</div>
	        		</div>      
	    		</div>  	    
	    		<div class="form-box box-border" id="product_loop">
	    			<div class="boxLoopingBox"> 
		        		<div class="row">
		          			<div class="col-md-4 columns">
		            			<label for="middle-label" class="text-left">PRODUCTS YOU'RE
								INTERESTED IN <span class="ico_required">*</span></label>
		          			</div>
	          				<div class="col-md-8 columns">
		            			<div id="area_select">            	
		             				<div class="item-select">
		             					{!! Form::select('product[]', $groceries, null, ['class'=>'white', 'placeholder' => 'Select Product', 'required' => 'required']) !!}
		            				</div>
		            			</div>
		           			</div>
		        		</div>
		        		<div class="row">
		          			<div class="col-md-4 columns">
		            			<label for="middle-label" class="text-left"><span class="required">ESTIMATED VOLUME</span></label>
		          			</div>
		          			<div class="col-md-8 columns">
		            			{!! Form::text('volume[]', null, ['placeholder' => 'Volume in kg', 'required' => 'required']) !!}
								<button class="btn btn-grey" id="act_select">Tambah +</button>
		          			</div>
		        		</div> 
		    		</div>
	    		</div>
	    		<div class="form-box">
	         		<div class="row">
	          			<div class="col-md-4 columns">
	            			<label for="middle-label" class="text-left">WHO YOU ARE <span class="ico_required">*</span></label>
	          			</div>
		          		<div class="col-md-8 columns">
		           			<select name="customer_type" class="white active">
		             			<option value="reseller" >Reseller</option>
		             			<option value="distributor" >Distributor</option>
		             			<option value="hotel" >Hotel</option>
		             			<option value="cafe" >Cafe</option>
		             			<option value="restaurant" >Restaurant</option>
		             			<option value="catering" >Catering</option>
		             			<option value="supermarket" >Supermarket</option>
		             			<option value="traditional retailer" >Traditional Retailer</option>
		           			</select>
		          		</div>
		        	</div>
		      
		        	<div class="row">
		          		<div class="col-md-4 columns">
		            		<label for="middle-label" class="text-left">MESSAGES</label>
		          		</div>
		          		<div class="col-md-8 columns">
		            		<textarea name="comment" rows="3"></textarea>
		          		</div>
	        		</div>

		        	<div class="row">
			          	<div class="col-md-4 columns">
			            	&nbsp;
			          	</div>
		          		<div class="col-md-8 columns">
		           			<button class="button primary btn-aneh">SUBMIT</button>
		          		</div>
	        		</div>                                                                                  
	      		</div>
		    </div>
		</form>
		</div>  
	</section>
@endsection

@section('scripts')
<script src="{{ asset(null) }}responsive/bootstrap/js/jquery.bootstrap-responsive-tabs.min.js"></script>
<script src="{{ asset(null) }}responsive/js/jquery.fancybox.min.js"></script>
<script>

    var closeKirinBox = function(){
        $('#askKirinForm').remove();
        $('#askKirin').show()
    }

    var drawKirin = function(html){

        $('.question-container ul.question-container-content').html(html);
    }

    $(document).on("click", "#act_select",  function(e){
		e.stopPropagation();

	  $('<div class="boxLoopingBox"> <span class="ico-min"></span> <div class="row"> <div class="col-md-4 columns"> <label for="middle-label" class="text-left">PRODUCTS YOU\'\REINTERESTED IN <span class="ico_required">*</span></label> </div><div class="col-md-8 columns"> <div id="area_select">{!! Form::select('product[]', $groceries, null, ['class'=>'white', 'placeholder' => 'Select Product', 'required' => 'required']) !!}</div></div></div><div class="row"> <div class="col-md-4 columns"> <label for="middle-label" class="text-left"><span class="required">ESTIMATED VOLUME</span></label> </div><div class="col-md-8 columns">{!! Form::text('volume[]', null, ['placeholder' => 'Volume in kg', 'required' => 'required']) !!} <button class="btn btn-grey" id="act_select">Tambah +</button> </div></div></div>').appendTo( "#product_loop" );

	  	$(".ico-min").click(function(){
			$(this).parent().fadeOut(300, function(){
				$(this).remove();
			});
		});



	});

    $(document).ready(function(){
        /*$('.zoom').zoom();*/

        $('.small-thumbnail').on('click', function(){
/*                alert($(this).attr('src'));*/

            $('.item.xlarge img').attr('src', $(this).attr('src'));
        });
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('input[name="search-question"]').keyup(function(){
            var itid = $('#item_id').val();
            $.ajax({
                type:'post',
                url: "{{ url('kirin/get_questions') }}/" + itid,
                data: {
                    search:$('input[name="search-question"]').val(),
                }
            }).done(function(result){
                drawKirin(result);
            });
        });

        @if(auth()->check())
        var idf = "{{ auth()->user()->id }}";
        $.ajax({
            type:'post',
            url: "{{ url('user/make-history') }}",
            data: {
                identifier: idf,
                type:'uid',
                gid:$('#item_id').val()
            }
        });

        $('.kirin-container').on('click', '.submit-kirin', function(e){

            if($('textarea[name="posttokirin"]').val() == ''){
                e.preventDefault();
                alert('Please type your question on the text area provided.');
                return false;
            }

            console.log($('textarea[name="posttokirin"]').val());

            var itid = $('#item_id').val();
            $.ajax({
                type:'post',
                url: "{{ url('groceries/question') }}/" + itid,
                data: {
                    groceries_id: parseInt(itid),
                    question:$('textarea[name="posttokirin"]').val()
                }
            }).done(function(result){
                drawKirin(result);
                location.reload();
            });
            e.preventDefault();
        });
        @else
        var idf = "{{ Session::getId() }}";
        $.ajax({
            type:'post',
            url: "{{ url('user/make-history') }}",
            data: {
                identifier: idf,
                type:'ss',
                gid:$('#item_id').val()
            }
        });
        @endif

        $('#askKirin').click(function(){
			$(this).before(
				"<form method='post' id='askKirinForm'><textarea rows=5 name='posttokirin'></textarea><button type='button' class='button btn-aneh submit-kirin' style='color:white'>SUBMIT</button>&nbsp;<button id='closeKirinForm' type='button' class='button btn-aneh' onclick='closeKirinBox();' style='color:white;background:red'>Close Form</button></div></form>"
				);
			$(this).hide();
		});

    });

	$('.responsive-tabs').responsiveTabs({
	  accordionOn: ['xs'] // xs, sm, md, lg
	});
</script>
@endsection