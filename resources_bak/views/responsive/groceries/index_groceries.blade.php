@extends('responsive.layout.master')

@section('title', App\Page::createTitle('Groceries'))

@section('customcss')
    <link rel='stylesheet' href='{{ asset(null) }}responsive/bootstrap/css/bootstrap-responsive-tabs.css'>
    <link rel="stylesheet" type="text/css" href="{{ asset(null) }}responsive/css/jquery.fancybox.min.css">
@endsection

@section('content')
	<div class="main-content">
		<div id="productDetail" class="container">
			<div class="row">
	            <div class="col-md-11 col-centered">
	                <div class="top-bar filter-box top-bar-custome">
	                    <div class="pull-left">
	                        <ul class="nav navbar-nav">
	                            <li class="hide-320"><span class="result">{{ $count }} results</span></li>
	                            <li class="dropdown">
									<span class="dropdown-toggle ico-down  filter-list" data-toggle="dropdown"><b>Sort by: </b></span>
									<ul class="dropdown-menu vertical filter-list">
										<li>
											<div class="box">
												<input id="bestSeller" type="radio" class="square" name="a" value="best-seller" onclick="window.location='{{ Request::url() }}?{{ $query_string != '' ?  $query_string.'&' : '' }}orderBy=best-seller'" @if($orderBy == 'best-seller')checked="checked"@endif><label for="bestSeller">Best Sellers</label>
											</div>
										</li>
										<li>
											<div class="box">
												<input id="newIn" type="radio" class="square" name="a" value="latest"  onclick="window.location='{{ Request::url() }}?{{ $query_string != '' ?  $query_string.'&' : '' }}orderBy=latest-entry'" @if($orderBy == 'latest-entry')checked="checked"@endif><label for="newIn">Latest Entry</label>
											</div>
										</li>
										<li>
											<div class="box">
												<input id="highestPrice" type="radio" class="square" name="a" value="highest-price" onclick="window.location='{{ Request::url() }}?{{ $query_string != '' ?  $query_string.'&' : '' }}orderBy=highest-price'" @if($orderBy == 'highest-price')checked="checked"@endif><label for="highestPrice">Highest Price</label>
											</div>
										</li>
										<li>
											<div class="box">
												<input id="lowPrice" type="radio" class="square" name="a" value="low-price" onclick="window.location='{{ Request::url() }}?{{ $query_string != '' ?  $query_string.'&' : '' }}orderBy=lowest-price'" @if($orderBy == 'lowest-price')checked="checked"@endif><label for="lowPrice">Lowest Price</label>
											</div>
										</li>
									</ul>
									
								</li>
							</ul>
	                    </div>
						<div class="top-bar-right pull-right hide-320" style="width:42%;text-align:right">
							<div class="col-md-7 col-sm-9 col-xs-12">
								@include('responsive.layout.pagination', ['paginator' => $groceries])
							</div>
	                        
						</div>
					</div>
	                <hr class="blue margin-top-0"></hr>
	            </div>
				<div class="col-md-3 col-sm-3 hide-320">
	                <ul class="side-nav list-style" >
	                    <li><a href="groceries/get-all/meat.html" id="0">MEAT</a>
	                        <ul id="sideDropdown0" style="display:none">
								<li style="list-style:none"><a href="groceries/beef.html">Beef</a></li>
								<li style="list-style:none"><a href="groceries/lamb.html">Lamb</a></li>
								<li style="list-style:none"><a href="groceries/chicken.html">Chicken</a></li>
								<li style="list-style:none"><a href="groceries/pork.html">Pork</a></li>
								<li style="list-style:none"><a href="groceries/turkey.html">Turkey</a></li>
								<li style="list-style:none"><a href="groceries/duck.html">Duck</a></li>
							</ul>
						</li>
	                    <li><a href="groceries/get-all/seafood.html" id="1">SEAFOOD</a>
	                        <ul id="sideDropdown1" style="display:none">
								<li style="list-style:none"><a href="groceries/fish.html">Fish</a></li>
								<li style="list-style:none"><a href="groceries/crab.html">Crab</a></li>
								<li style="list-style:none"><a href="groceries/prawn-shrimp.html">Prawn/Shrimp</a></li>
								<li style="list-style:none"><a href="groceries/mussels-clams.html">Mussels &amp; Clams</a></li>
								<li style="list-style:none"><a href="groceries/lobster-crayfish.html">Lobster &amp; Crayfish</a></li>
								<li style="list-style:none"><a href="groceries/scallops.html">Scallops</a></li>
								<li style="list-style:none"><a href="groceries/squid-octopus.html">Squid/Octopus</a></li>
								<li style="list-style:none"><a href="groceries/froglegs.html">Froglegs</a></li>
							</ul>
						</li>
	                    <li><a href="groceries/get-all/dairy.html" id="2">DAIRY</a>
	                        <ul id="sideDropdown2" style="display:none">
								<li style="list-style:none"><a href="groceries/milk.html">Milk</a></li>
								<li style="list-style:none"><a href="groceries/eggs.html">Eggs</a></li>
								<li style="list-style:none"><a href="groceries/cheese.html">Cheese</a></li>
								<li style="list-style:none"><a href="groceries/yoghurt.html">Yoghurt</a></li>
								<li style="list-style:none"><a href="groceries/butter.html">Butter</a></li>
								<li style="list-style:none"><a href="groceries/margarine.html">Margarine</a></li>
							</ul>
						</li>
	                    <li><a href="groceries/get-all/beverages.html" id="3">BEVERAGES</a>
	                        <ul id="sideDropdown3" style="display:none">
								<li style="list-style:none"><a href="groceries/tea.html">Tea</a></li>
								<li style="list-style:none"><a href="groceries/coffee.html">Coffee</a></li>
								<li style="list-style:none"><a href="groceries/juice.html">Juice</a></li>
								<li style="list-style:none"><a href="groceries/water.html">Water</a></li>
								<li style="list-style:none"><a href="groceries/soda.html">Soda</a></li>
								<li style="list-style:none"><a href="groceries/energy-drink.html">Energy Drink</a></li>
								<li style="list-style:none"><a href="groceries/syrup.html">Syrup</a></li>
								<li style="list-style:none"><a href="groceries/flavored-drink.html">Flavored Drink</a></li>
							</ul>
						</li>
	                    <li><a href="groceries/get-all/snacks.html" id="4">SNACKS</a>
	                        <ul id="sideDropdown4" style="display:none">
								<li style="list-style:none"><a href="groceries/candy.html">Candy</a></li>
								<li style="list-style:none"><a href="groceries/biscuit-cookie.html">Biscuit &amp; Cookie</a></li>
								<li style="list-style:none"><a href="groceries/chocolate.html">Chocolate</a></li>
								<li style="list-style:none"><a href="groceries/chips.html">Chips</a></li>
								<li style="list-style:none"><a href="groceries/crackers.html">Crackers</a></li>
								<li style="list-style:none"><a href="groceries/dried-snack.html">Dried Snack</a></li>
								<li style="list-style:none"><a href="groceries/nuts.html">Nuts</a></li>
							</ul>
						</li>
	                    <li><a href="groceries/get-all/pantry.html" id="5">PANTRY</a>
	                        <ul id="sideDropdown5" style="display:none">
								<li style="list-style:none"><a href="groceries/soy-sauce-vinegar.html">Soy Sauce &amp; Vinegar</a></li>
								<li style="list-style:none"><a href="groceries/sauces.html">Sauces</a></li>
								<li style="list-style:none"><a href="groceries/seasoning.html">Seasoning</a></li>
								<li style="list-style:none"><a href="groceries/oil.html">Oil</a></li>
								<li style="list-style:none"><a href="groceries/dressing-topping.html">Dressing &amp; Topping</a></li>
								<li style="list-style:none"><a href="groceries/jam-spread.html">Jam &amp; Spread</a></li>
								<li style="list-style:none"><a href="groceries/honey-syrup.html">Honey &amp; Syrup</a></li>
								<li style="list-style:none"><a href="groceries/cooking-cream.html">Cooking Cream</a></li>
								<li style="list-style:none"><a href="groceries/rice.html">Rice</a></li>
								<li style="list-style:none"><a href="groceries/flour-flavoring.html">Flour &amp; Flavoring</a></li>
							</ul>
						</li>
	                    <li><a href="groceries/get-all/dry-instant-food.html" id="6">DRY &amp; INSTANT FOOD</a>
							<ul id="sideDropdown6" style="display:none">
								<li style="list-style:none"><a href="groceries/instant-food.html">Instant Food</a></li>
								<li style="list-style:none"><a href="groceries/pasta-noodle.html">Pasta &amp; Noodle</a></li>
								<li style="list-style:none"><a href="groceries/cereal-oats-grains-seeds.html">Cereal, Oats, Grains &amp; Seeds</a></li>
								<li style="list-style:none"><a href="groceries/canned-food.html">Canned Food</a></li>
								<li style="list-style:none"><a href="groceries/canned-fruits.html">Canned Fruits</a></li>
								<li style="list-style:none"><a href="groceries/other-dried-food.html">Other Dried Food</a></li>
							</ul>
						</li>
	                    <li><a href="groceries/get-all/frozen.html" id="7">FROZEN</a>
	                        <ul id="sideDropdown7" style="display:none">
								<li style="list-style:none"><a href="groceries/ice-cream.html">Ice Cream</a></li>
								<li style="list-style:none"><a href="groceries/frozen-seafood.html">Frozen Seafood</a></li>
								<li style="list-style:none"><a href="groceries/frozen-fuit.html">Frozen Fuit</a></li>
								<li style="list-style:none"><a href="groceries/frozen-pastry-cake-bread.html">Frozen Pastry, Cake &amp; Bread</a></li>
								<li style="list-style:none"><a href="groceries/frozen-vegetables.html">Frozen Vegetables</a></li>
								<li style="list-style:none"><a href="groceries/frozen-meat.html">Frozen Meat</a></li>
								<li style="list-style:none"><a href="groceries/other-frozen-food.html">Other Frozen Food</a></li>
							</ul>
						</li>
	                    <li><a href="groceries/get-all/chilled.html" id="8">CHILLED</a>
	                        <ul id="sideDropdown8" style="display:none">
								<li style="list-style:none"><a href="groceries/sausage.html">Sausage</a></li>
								<li style="list-style:none"><a href="groceries/ham.html">Ham</a></li>
								<li style="list-style:none"><a href="groceries/smoked.html">Smoked</a></li>
								<li style="list-style:none"><a href="groceries/dessert.html">Dessert</a></li>
							</ul>
						</li>
	                    <li><a href="groceries/get-all/healthy.html" id="9">HEALTHY FOOD</a>
	                        <ul id="sideDropdown9" style="display:none">
								<li style="list-style:none"><a href="groceries/milks.html">Milks</a></li>
								<li style="list-style:none"><a href="groceries/snack.html">Snack</a></li>
								<li style="list-style:none"><a href="groceries/grains.html">Grains</a></li>
								<li style="list-style:none"><a href="groceries/seeds.html">Seeds</a></li>
								<li style="list-style:none"><a href="groceries/detox-juice.html">Detox Juice</a></li>
							</ul>
						</li>
	                    <li><a href="groceries/get-all/vegetables.html" id="10">VEGETABLES</a>
	                        <ul id="sideDropdown10" style="display:none">
	                        </ul>
						</li>
	                </ul>
	            </div>

				<div class="col-md-7 col-sm-9 col-xs-12">
					@foreach($groceries as $item)
						<div class="col-md-4 col-sm-6 col-xs-6">
							<div class="item-box item-margin-bottom">
								<div class="item large--2">
									{{-- <div class="item-sale">
										<img src="{{ asset(null) }}responsive/images/ico-sale.png">
									</div>
									<div class="item-love">
										<img src="{{ asset(null) }}responsive/images/ico-love-red.png">
									</div> --}}
									<a href="{{ url('groceries/'.$item->id.'/'.str_slug($item->name)) }}">
										<img src="{{ Groceries::getFirstImageUrl($item->id) }}">
									</a>
									
								</div>
								<a href="{{ url('groceries/'.$item->id.'/'.str_slug($item->name)) }}">
	                                <div class="desc">
	                                    <span><b>{{ $item->name }}, {{ $item->value_per_package }}{{ $item->metric_id != 0 ? $item->metric->abbreviation : 'metric unknown' }}</b></span><br>
	                                </div>
	                             </a>
								<span class="after-discount">{{ Money::display($item->price) }}</span><br>
	                            <div class="procat control-cart-{{ $item->id }}">
                                    <?php
                                        $cart_items = Carte::contents();
                                        $is_there = false;
                                        $identifier = '';

                                        foreach($cart_items as $keys=>$prod){

                                            if($item->id == $prod->id){
                                                $is_there = true;
                                                $identifier = $keys;
                                                $cart_item = $prod;
                                                break;
                                            }
                                        }
                                    ?>

                                    <div class="atc-initiate">
                                        <input class="item_id" type="hidden" value="{{ $item->id }}" />
                                        <input class="item_sku" type="hidden" value="{{ $item->sku ? $item->sku : 'No SKU added' }}" />
                                        <input class="item_name" type="hidden" value="{{ $item->name }}" />
                                        @if($item->discount_id != 0)
                                            @if($item->discount->discount_type == 'percent')
                                                @if($item->price - (($item->price * $item->discount->discount_value)/100))
                                                <input class="item_price" type="hidden" value="{{ $item->price - (($item->price * $item->discount->discount_value)/100) }}" />
                                                @else
                                                <input class="item_price" type="hidden" value="0" />
                                                @endif
                                            @else
                                                @if($item->price - $item->discount->discount_value)
                                                <input class="item_price" type="hidden" value="{{ $item->price - $item->discount->discount_value }}" />
                                                @else
                                                <input class="item_price" type="hidden" value="0" />
                                                @endif
                                            @endif
                                        @else
                                            <input class="item_price" type="hidden" value="{{ $item->price }}" />
                                        @endif

                                        {{--@if($item->metric)
                                            <input class="item_metric" type="hidden" value="{{ $item->value_per_package.' '.$item->metric->abbreviation }}" />
                                        @else
                                            <input class="item_metric" type="hidden" value="{{ $item->value_per_package.' --metric not added--' }}" />
                                        @endif--}}
                                        <input class="item_image" type="hidden"  name="" value="{{ Groceries::getFirstImageUrl($item->id) }}">

                                    </div>

                                    

                                    @if($item->stock > 0)
                                        <button class="button add-to-cart-btn list_add_to_cart" >ADD TO CART</button>
                                    @else
                                        <button class="button add-to-cart-btn" style="color:white;background:#ff0000;" disabled>SOLD OUT</button>
                                    @endif
                                </div>
							</div>
						</div>
					@endforeach
	            </div>
	            @if (count($recent) > 0)
	            <div class="col-md-2 pull-right hide-768">
					<div class="recently-viewed-box">
						<div class="header">
							RECENTLY VIEWED :
						</div>
						<div class="body"><nr><br>
								<ul class="recomendation-box">
									@foreach($recent as $k=>$item)
									<li>
										<a href="{{ url('groceries/'.$item->id.'/'.str_slug($item->name)) }}">
											<div class="item" style="overflow:hidden">
												<img src="{{ Groceries::getFirstImageUrl($item->id) }}">
											</div>

											<span class="text-grey"><b>{{ $item->name }}, {{ $item->value_per_package }}</b></span>
											<span style="color:#A9A9A9;">{{ Money::display($item->price) }}/{{ $item->metric_id != 0 ? $item->metric->abbreviation : 'metric unknown' }}</span>
										</a>
									</li>
									@endforeach
								</ul>
						</div>
					</div>
	            </div>
	            @endif
				<div class="col-md-7 col-sm-9 col-xs-12 margin-bottom pull-right" style="text-align:right">
	                @include('responsive.layout.pagination', ['paginator' => $groceries])
	            </div>
	        </div>			
		</div>
	</div>
@endsection

@section('scripts')
<script src="{{ asset(null) }}responsive/bootstrap/js/jquery.bootstrap-responsive-tabs.min.js"></script>
<script src="{{ asset(null) }}responsive/js/jquery.fancybox.min.js"></script>
<script>
$('ul.nav li.dropdown').hover(function() {
  $(this).find('.dropdown-menu').stop(true, true).delay(50).fadeIn(500);
}, function() {
  $(this).find('.dropdown-menu').stop(true, true).delay(50).fadeOut(500);
});


</script>
@endsection