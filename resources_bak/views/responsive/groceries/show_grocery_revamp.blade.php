@extends('responsive.layout.master_revamp')

@section('title', App\Page::createTitle( $item->name ))

@section('customcss')
    <link rel='stylesheet' href='{{ asset(null) }}responsive/bootstrap/css/bootstrap-responsive-tabs.css'>
    <link rel="stylesheet" type="text/css" href="{{ asset(null) }}responsive_new/assets/css/jquery.fancybox.min.css">
@endsection

@section('content')
	<div class="main-content">
	<div class="container">
		<div class="row hide-320">
			<div class="col-md-12">
				<span class="text-grey equal margin-bottom-35">
				  <a class="text-grey" href="{{ url('/') }}"><b>HOME</b></a> /
				  <a class="text-grey" href="#"><b>SEAFER GROCER </b></a>
				</span>
			</div>
		
		</div>
		<div class="row">
			<div class="col-md-5 col-sm-5">
				<div class="item-box">
                    <div class="item xlarge">
						<a data-fancybox="gallery" href="{{ Groceries::getFirstImageUrl($item->id) }}" class="zoom">
							<img src="{{ Groceries::getFirstImageUrl($item->id) }}" class="image-large">
						</a>
					</div>
					<div class="thumbnail-box">
						<ul>
							
							@foreach ($item->images()->get() as $img)
							<li>
								<div class="item">
									<a data-fancybox="gallery" href="{{ asset('contents/'.$img->file_name) }}">
										<img src="{{ asset('contents/'.$img->file_name) }}" class="small-thumbnail">
									</a>
								</div>
							</li>
							@endforeach
							
						</ul>
					</div>
				</div>
			</div>
			<div class="col-md-7 col-sm-7">
				<div class="detail detail-cart">
					<h6 class="subtitle large">{{ $item->name }}, {{ $item->value_per_package }}{{ $item->metric_id != 0 ? $item->metric->abbreviation : 'metric unknown' }}</h6>
                    <br>
                    @if($item->discount_id != 0)
                            <span class="price text-right semi-bold">{{ Money::display($item->price) }}</span>
                            @if($item->discount->discount_type == 'percent')
                                @if( $item->price - (($item->price * $item->discount->discount_value)/100) > 0 )
                                <span class="price text-red semi-bold">{{ Money::display( $item->price - (($item->price * $item->discount->discount_value)/100) ) }}</span>
                                @else
                                <span class="price text-red semi-bold">{{ Money::display( 0 ) }}</span>
                                @endif
                            @else
                                @if( $item->price - $item->discount->discount_value > 0 )
                                <span class="price text-red semi-bold">{{ Money::display( $item->price - $item->discount->discount_value ) }}</span>
                                @else
                                <span class="price text-red semi-bold">{{ Money::display( 0 ) }}</span>
                                @endif
                            @endif
                        @else
                            <span class="price text-red semi-bold">{{ Money::display($item->price) }}</span>
                        @endif

					<div class="counter independent">
                            <!-- <button class="minus">-</button> -->
                            <input id="item_id" type="hidden" value="{{ $item->id }}" />
                            <input id="item_sku" type="hidden" value="{{ $item->sku ? $item->sku : 'No SKU added' }}" />
                            <input id="item_name" type="hidden" value="{{ $item->name }}" />
                            <input id="item_qty" type="hidden" class="totalx" name="" value="1">
                            @if($item->metric)
                                <input class="item_metric" type="hidden" value="{{ $item->value_per_package.' '.$item->metric->abbreviation }}" />
                            @else
                                <input class="item_metric" type="hidden" value="{{ $item->value_per_package.' --metric not added--' }}" />
                            @endif
                            @if($item->discount_id != 0)
                                @if($item->discount->discount_type == 'percent')
                                    @if($item->price - (($item->price * $item->discount->discount_value)/100))
                                        <input class="item_price" type="hidden" value="{{ $item->price - (($item->price * $item->discount->discount_value)/100) }}" />
                                    @else
                                        <input class="item_price" type="hidden" value="0" />
                                    @endif
                                @else
                                    @if($item->price - $item->discount->discount_value)
                                        <input class="item_price" type="hidden" value="{{ $item->price - $item->discount->discount_value }}" />
                                    @else
                                        <input class="item_price" type="hidden" value="0" />
                                    @endif
                                @endif
                            @else
                                <input class="item_price" type="hidden" value="{{ $item->price }}" />
                            @endif
                            <input class="item_image" type="hidden"  name="item_image" value="{{ Groceries::getFirstImageUrl($item->id) }}">
                            <!-- <button class="plus">+</button> -->
                        </div>
					<div class="add-to-cart-box">

						@if($item->stock > 0)
						<div class="row">
							<div class="col-md-6 col-sm-6 col-xs-6" id="item_add_to_cart_wrap">
								<button class="btn btn-aneh" id="item_add_to_cart">ADD TO CART</button>
							</div>
							{{-- <div class="col-md-6 col-sm-6 col-xs-6">
                                    <table class="pull-right">
                                        <tbody>
                                        <tr>
                                            <td>
                                              <a href="#"><img src="{{ asset(null) }}responsive/images/ico-love-red.png" class="loveee"></a>
                                            </td>
                                            <td><a href="#"><img src="{{ asset(null) }}responsive/images/ico-share.png"></a></td>
                                        </tr>
                                        <tr><td><b class="text-blue pull-right">112 loved</b></td><td>&nbsp;</td></tr>
                                        </tbody>
                                    </table>
                             </div> --}}
						</div>
						<span class="price-large"></span>
						@else
						<div class="row">
							<div class="col-md-6 col-sm-6 col-xs-6" id="item_add_to_cart_wrap">
								<button class="button btn-aneh" id="item_add_to_cart" disabled="" type="button" style="opacity:0.25;color:white;background:#ff0000;">SOLD OUT</button>
							</div>
						</div>
						<span class="price-large"></span>
						@endif
						<br>
		                <div class="product-info">
		                  <p>Country of Origin: <strong>{{$item->country->name}} </strong></p>
		                  @if($item->country->id == 107)
		                  <p>Domestic/Import : <strong>Domestic</strong></p>
		                  @else
							<p>Domestic/Import : <strong>Import</strong></p>
		                  @endif

		                  @if($item->cookingAdvice->count() == 0)
		                  <p>Cooking Advise : <strong>-</strong></p>
		                  @else
							<p>Cooking Advise : @foreach($item->cookingAdvice as $advice)<strong>{{ $advice->name }}</strong>, &nbsp; @endforeach</p>
		                  @endif
		                  <p><strong>Seawater</strong></p>
		                </div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="panel-group panel-cart" id="accordion">
								<div class="panel panel-default">
									<div class="panel-heading">
									  <h4 class="panel-title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true">
										  Expiry Date
										</a>
									  </h4>
									</div>
									<div id="collapse1" class="panel-collapse collapse in">
									  <div class="panel-body">
										{{ strtoupper(LocalizedCarbon::parse($item->expiry_date)->format("d m Y")) }}
									  </div>
									</div>
								  </div>
								  <div class="panel panel-default">
									<div class="panel-heading">
									  <h4 class="panel-title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="true">
										  Description
										</a>
									  </h4>
									</div>
									<div id="collapse2" class="panel-collapse collapse in" >
									  <div class="panel-body">
										{!! nl2br($item->description) !!}
									  </div>
									</div>
								  </div>
								  <div class="panel panel-default">
									<div class="panel-heading">
									  <h4 class="panel-title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="true">
										  Nutrition
										</a>
									  </h4>
									</div>
									<div id="collapse3" class="panel-collapse collapse in" >
									  <div class="panel-body">
										@if($item->nutrients == 'No Data' || substr($item->nutrients, 0, 3) == '000')
										Not Available
										@else
										{!! nl2br($item->nutrients) !!}
										@endif
									  </div>
									</div>
								  </div>
								  <div class="panel panel-default">
									<div class="panel-heading">
									  <h4 class="panel-title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="true">
										  Ingredients
										</a>
									  </h4>
									</div>
									<div id="collapse4" class="panel-collapse collapse in" >
									  <div class="panel-body">
										{!! nl2br($item->ingredients) !!}
									  </div>
									</div>
								  </div>
							</div>
							<div class="box_whole">
			                  <a href="{{ url('wholesale') }}" class="linkMore" style="padding-top:4px">Wholesale</a>
			                </div>
			                <div class="add-to-cart-box">

								@if($item->stock > 0)
								<div class="row">
									<div class="col-md-6 col-sm-6 col-xs-6" id="item_add_to_cart_wrap">
										<button class="btn btn-aneh" id="item_add_to_cart">ADD TO CART</button>
									</div>
									
								</div>
								@else
								<div class="row">
									<div class="col-md-6 col-sm-6 col-xs-6" id="item_add_to_cart_wrap">
										<button class="button btn-aneh" id="item_add_to_cart" disabled="" type="button" style="opacity:0.25;color:white;background:#ff0000;">SOLD OUT</button>
									</div>
								</div>
								@endif
							</div>
						</div>
					</div>
                </div>
			</div>
			
		</div>

		<div class="poster">
			<div class="images">
				<img src="{{ asset(null) }}responsive_new/assets/images/about-us/about-4.png">
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="recomendation-box top-line-blue ">
					<label>YOU MAY ALSO LIKE</label>
					<div class="row">
						@foreach ($recommendation as $item)
						<div class="col-md-2 col-sm-3 col-xs-4 padding-320">
							<a href="{{ url('groceries/'.$item->id.'/'.str_slug($item->name)) }}">
								<div class="item" style="background-image: url({{ Groceries::getFirstImageUrl($item->id) }});"></div>
							</a>
							<a href="{{ url('groceries/'.$item->id.'/'.str_slug($item->name)) }}">
								<span class="text-blue" style="padding-top: 10px;">{{ $item->name }}, {{ $item->value_per_package }}{{ $item->metric_id != 0 ? $item->metric->abbreviation : 'metric unknown' }}</span>
							</a>
							<span>{{ Money::display($item->price) }}</span>
						</div>
						@endforeach
						
					</div>
				</div>

				<div class="recomendation-box top-line-blue bottom-line-blue">
					<label>BRAND RELATED</label>
					<div class="row">
						@if($brand_related->count() > 0)
                        @foreach ($brand_related as $item)
						<div class="col-md-2 col-sm-3 col-xs-4 padding-320">
							<a href="{{ url('groceries/'.$item->id.'/'.str_slug($item->name)) }}">
								<div class="item" style="background-image: url({{ Groceries::getFirstImageUrl($item->id) }});"></div>
							</a>
							<a href="{{ url('groceries/'.$item->id.'/'.str_slug($item->name)) }}">
								<span class="text-blue" style="padding-top: 10px;">{{ $item->name }}, {{ $item->value_per_package }}{{ $item->metric_id != 0 ? $item->metric->abbreviation : 'metric unknown' }}</span>
							</a>
							<span>{{ Money::display($item->price) }}</span>
						</div>
						@endforeach
                        @else
                        <div class="col-md-12">
							<p>No more product from this brand to show</p>
						</div>
                        @endif
						
					</div>
				</div>


				<div class="recomendation-box">
					<label>RECENTLY VIEWED</label>
					<div class="row">

						@if($recent->count() > 0)
                            @foreach ($recent as $item)
                            <div class="col-md-2 col-sm-3 col-xs-4 padding-320">
                            	
                            	@if(0 !== $item->groceries_id && null !== $item->grocery)
                            	
								<a href="{{ url('groceries/'.$item->grocery->id.'/'.str_slug($item->grocery->name)) }}">

									<div class="item" style="background-image: url({{ Groceries::getFirstImageUrl($item->grocery->id) }});"></div>
								</a>

								<a href="{{ url('groceries/'.$item->grocery->id.'/'.str_slug($item->grocery->name)) }}">
									<span class="text-blue" style="padding-top: 10px;">{{ $item->grocery->name }}, {{ $item->grocery->value_per_package }}{{ $item->grocery->metric_id != 0 ? $item->grocery->metric->abbreviation : 'metric unknown' }}</span>
								</a>
								<span>{{ Money::display($item->grocery->price) }}</span>
								@endif
							</div>
							@endforeach
						@else
						<div class="col-md-12">
							<p>This is your first product visit</p>
						</div>
						@endif
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="flash-message">
					@foreach (['danger', 'warning', 'success', 'info'] as $msg)
                        @if(Session::has('alert-' . $msg))

                            <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                        @endif
                    @endforeach
                </div>																																						   </div>
				<div class="customer-and-reviews-box ">
					<ul class="nav nav-tabs responsive-tabs">
						<li class="tabs-title"><a href="#panel1" data-toggle="tab">CUSTOMER Q&A</a></li>
						<li class="tabs-title"><a href="#panel2" data-toggle="tab">REVIEWS</a></li>
					</ul>
					<div class="tab-content top-line-blue">
						<div class="tab-pane fade in active" id="panel1">
							<div class="row">
								<div class="col-md-6 col-sm-6">
									<div class="form-box kirin-container">
										<a href="#">Can’t find what You are looking for?</a><br><br>
										@if(auth()->check())
                                        
                                        <button class="button btn-aneh" id="askKirin" style="color:white">ASK KIRIN</button>
                                        @else
                                        <a href="{!! url('user/sign-in') !!}"><button class="button btn-aneh openSIGNIN" style="color:white">ASK KIRIN</button></a>
                                        <p>sign in is required in order to add a question</p>
                                        @endif
									</div>
								</div>
								<div class="col-md-6 col-sm-6">
                                        <div class="customer-qa-box question-container">
                                        	@if ($questions->count() > 0)
                                            <div id="slider" class="carousel slide">
                                                <ul class="carousel-inner question-container-content">
                                                    @foreach($questions as $key=>$question)
                                                    @if($key == 0 || ($key % 4) == 0)
                                                    <li class="row active item">
                                                        <div class="customer-qa-box">
                                                    @endif
                                                            <div class="qa-box">
                                                                <table>
                                                                    <tr>
                                                                        <td><span class="text-blue">Q :</span></td>
                                                                        <td><span class="text-blue">{{ $question->question }}</span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>A :</td>
                                                                        <td>{{ $question->answer ? $question->answer : 'The answer will be updated soon' }}</td>
                                                                    </tr>
                                                                </table>
                                                            </div>

                                                    @if($key % 3 == 0 || $key == $reviews->count())
	                                                    </div>
	                                                </li>
	                                                @endif
	                                                @endforeach
                                                </ul>
                                                <ul class="nav-slide">
                                                    <a class="carousel-control left pull-left" href="#slider"  data-slide="prev"><span class="show-for-sr"></span><img src="{!! asset(null) !!}responsive/images/ico-arrows-L.png"></a>
                                                    <a class="carousel-control right pull-right" href="#slider"   data-slide="next"><span class="show-for-sr"></span><img src="{!! asset(null) !!}responsive/images/ico-arrows-R.png"></a>
                                                </ul>
                                            </div>
                                            @endif
                                        </div>
                                    </div>
							</div>
						</div>

						<div class="tab-pane fade" id="panel2">
							<div class="row">
								<div class="col-md-6 col-sm-6">
									<ul class="rating list-inline">
										@if($reviews->count() >0)
                                            @for($i = 0; $i < $reviews->avg('rate'); $i++)
                                            <li><img src="{!! asset(null) !!}assets/images/ico-star-fill-color.png"></li>
                                            @endfor
                                            @else
                                            <li><img src="{!! asset(null) !!}assets/images/ico-star-border.png"></li>
                                            <li><img src="{!! asset(null) !!}assets/images/ico-star-border.png"></li>
                                            <li><img src="{!! asset(null) !!}assets/images/ico-star-border.png"></li>
                                            <li><img src="{!! asset(null) !!}assets/images/ico-star-border.png"></li>
                                            <li><img src="{!! asset(null) !!}assets/images/ico-star-border.png"></li>
                                            @endif
									</ul>																	
									@if($reviews->count() >0)
                                    <span>{{ $reviews->avg('rate') }} out of 5 stars</span><br><br>
                                    @else
                                        <span>This product has not been rated</span><br><br>
                                    @endif

									<div class="chart-box">
										<table class="chart">
											<tbody>
											<tr>
												<td>
													<p class="percentage">5 Stars</p>
												</td>
												<td class="chart">
													<div class="barchart">
														<span class="meter" style="width: {{ $reviews->count() > 0 ? ($five_star/$reviews->count())*100 : 0 }}%"></span>
													</div>
												</td>
												<td>
													<p class="percentage">{{ $reviews->count() > 0 ? ($five_star/$reviews->count())*100 : 0 }}%</p>
												</td>
											</tr>
											<tr>
												<td>
													<p class="percentage">4 Stars</p>
												</td>
												<td class="chart">
													<div class="barchart">
														<span class="meter" style="width: {{ $reviews->count() > 0 ? ($four_star/$reviews->count())*100 : 0 }}%"></span>
													</div>
												</td>
												<td>
													<p class="percentage">{{ $reviews->count() > 0 ? ($four_star/$reviews->count())*100 : 0 }}%</p>
												</td>
											</tr>
											<tr>
												<td>
													<p class="percentage">3 Stars</p>
												</td>
												<td class="chart">
													<div class="barchart">
														<span class="meter" style="width: {{ $reviews->count() > 0 ? ($three_star/$reviews->count())*100 : 0 }}%"></span>
													</div>
												</td>
												<td>
													<p class="percentage">{{ $reviews->count() > 0 ? ($three_star/$reviews->count())*100 : 0 }}%</p>
												</td>
											</tr>
											<tr>
												<td>
													<p class="percentage">2 Stars</p>
												</td>
												<td class="chart">
													<div class="barchart">
														<span class="meter" style="width: {{ $reviews->count() > 0 ? ($two_star/$reviews->count())*100 : 0 }}%"></span>
													</div>
												</td>
												<td>
													<p class="percentage">{{ $reviews->count() > 0 ? ($two_star/$reviews->count())*100 : 0 }}%</p>
												</td>
											</tr>
											<tr>
												<td>
													<p class="percentage">1 Star</p>
												</td>
												<td class="chart">
													<div class="barchart">
														<span class="meter" style="width: {{ $reviews->count() > 0 ? ($one_star/$reviews->count())*100 : 0 }}%"></span>
													</div>
												</td>
												<td>
													<p class="percentage">{{ $reviews->count() > 0 ? ($one_star/$reviews->count())*100 : 0 }}%</p>
												</td>
											</tr>
											</tbody>
										</table>
									</div>
								</div>
								<div class="col-md-6">
									<div class="customer-qa-box">
										@if($reviews->count() > 0)
										<div id="slider-rating" class="carousel slide">
                                            <ul class="carousel-inner question-container-content">
                                                @foreach($reviews as $key=>$review)
                                                	@if($key == 0 || ($key % 4) == 0)
                                                    <li class="row active item">
                                                        <div class="customer-qa-box">
                                                    @endif
	                                                        <div class="qa-box">
	                                                            <table>
	                                                                <tr>
	                                                                    <td>
	                                                                        <div class="rating-box">
	                                                                            <ul class="rating">
	                                                                                @for($i = 0; $i < $review->rate; $i++)
	                                                                                <li><img src="{!! asset(null) !!}assets/images/ico-star-fill-color.png"></li>
	                                                                                @endfor
	                                                                            </ul>
	                                                                        </div>
	                                                                    </td>
	                                                                    <td><b>{{ $review->title }}</b></td>
	                                                                </tr>
	                                                                <tr>
	                                                                    <td colspan="2"><span class="font-11">By {{ $review->user->name }} on {{ LocalizedCarbon::parse($review->created_at)->format('F, d Y') }}</span></td>
	                                                                </tr>
	                                                                <tr>
	                                                                    <td colspan="2"><span class="font-11">{{ $review->review }}</span></td>
	                                                                </tr>
	                                                            </table>
	                                                        </div>
	                                                @if($key % 3 == 0 || $key == $reviews->count())
	                                                    </div>
	                                                </li>
	                                                @endif
	                                                @endforeach
											</ul>
											<ul class="nav-slide">
                                                <a class="carousel-control left pull-left" href="#slider-rating"  data-slide="prev"><span class="show-for-sr"></span><img src="{!! asset(null) !!}responsive/images/ico-arrows-L.png"></a>
                                                <a class="carousel-control right pull-right" href="#slider-rating"   data-slide="next"><span class="show-for-sr"></span><img src="{!! asset(null) !!}responsive/images/ico-arrows-R.png"></a>
                                            </ul>
										</div>
										@endif
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script src="{{ asset(null) }}responsive/bootstrap/js/jquery.bootstrap-responsive-tabs.min.js"></script>
<script src="{{ asset(null) }}responsive/js/jquery.fancybox.min.js"></script>
<script>

    var closeKirinBox = function(){
        $('#askKirinForm').remove();
        $('#askKirin').show()
    }

    var drawKirin = function(html){

        $('.question-container ul.question-container-content').html(html);
    }

    $(document).ready(function(){
        /*$('.zoom').zoom();*/

        $('.small-thumbnail').on('click', function(){
/*                alert($(this).attr('src'));*/

            $('.item.xlarge img').attr('src', $(this).attr('src'));
        });
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('input[name="search-question"]').keyup(function(){
            var itid = $('#item_id').val();
            $.ajax({
                type:'post',
                url: "{{ url('kirin/get_questions') }}/" + itid,
                data: {
                    search:$('input[name="search-question"]').val(),
                }
            }).done(function(result){
                drawKirin(result);
            });
        });

        @if(auth()->check())
        var idf = "{{ auth()->user()->id }}";
        $.ajax({
            type:'post',
            url: "{{ url('user/make-history') }}",
            data: {
                identifier: idf,
                type:'uid',
                gid:$('#item_id').val()
            }
        });

        $('.kirin-container').on('click', '.submit-kirin', function(e){

            if($('textarea[name="posttokirin"]').val() == ''){
                e.preventDefault();
                alert('Please type your question on the text area provided.');
                return false;
            }

            console.log($('textarea[name="posttokirin"]').val());

            var itid = $('#item_id').val();
            $.ajax({
                type:'post',
                url: "{{ url('groceries/question') }}/" + itid,
                data: {
                    groceries_id: parseInt(itid),
                    question:$('textarea[name="posttokirin"]').val()
                }
            }).done(function(result){
                drawKirin(result);
                location.reload();
            });
            e.preventDefault();
        });
        @else
        var idf = "{{ Session::getId() }}";
        $.ajax({
            type:'post',
            url: "{{ url('user/make-history') }}",
            data: {
                identifier: idf,
                type:'ss',
                gid:$('#item_id').val()
            }
        });
        @endif

        $('#askKirin').click(function(){
			$(this).before(
				"<form method='post' id='askKirinForm'><textarea rows=5 name='posttokirin'></textarea><button type='button' class='button btn-aneh submit-kirin' style='color:white'>SUBMIT</button>&nbsp;<button id='closeKirinForm' type='button' class='button btn-aneh' onclick='closeKirinBox();' style='color:white;background:red'>Close Form</button></div></form>"
				);
			$(this).hide();
		});

    });

	$('.responsive-tabs').responsiveTabs({
	  accordionOn: ['xs'] // xs, sm, md, lg
	});
</script>
@endsection