@extends('responsive.layout.master')

@section('title', App\Page::createTitle('Thank You'))

@section('customcss')
    <link rel='stylesheet' href='{{ asset(null) }}responsive/bootstrap/css/bootstrap-responsive-tabs.css'>
    <link rel="stylesheet" type="text/css" href="{{ asset(null) }}responsive/css/jquery.fancybox.min.css">
@endsection

@section('content')
	<div class="main-content">
		<div class="container" id="confirm-box">
			<div class="row">
				<div class="col-md-9 col-sm-9 col-xs-10 col-centered center">
					<div class="img-box">
					  <img src="{{ asset(null) }}responsive/images/img-popup.png">
					</div>
					<div class="subtitle">
					  Thank you for shopping with <span class="htp">SEAFERMART</span>
					</div>
					<br>
					<p>{{ auth()->user()->name }}, the total amount you should transfer is <span class="text-red">{{ Money::display($order->grand_total) }}</span>. <br>
					Please note a unique code of <span class="text-red">{{ Money::display($unique_code) }}</span> has been added to the total amount to help us verify your payment transfer.</p>
					<p>
					  Please make your payment before:
					  <b class="text-red">{{ LocalizedCarbon::now()->addHours(24) }}</b>
					  Otherwise your orders will be automatically canceled.
					</p>
					<a href="#" class="htp">HOW TO PAY</a>
					<ul>
					  <li>
						<span>Please make your payment to the following account: <b class="text-red inline">PT. Seafermart Jaya Raya – BCA 206 3232 327 / Mandiri 122 000 758 6848</b></span>
					  </li>
					  <li><span>Take a picture of your proof of payment and upload it to Confirm Payment form below.</span></li>
					  <li><span>SeaferMart will confirm your payment within 24 hours.</span></li>
					  <li><span>SeaferMart will notify you after the payment is received.</span></li>
					</ul>
					<button type="button" class="btn btn-aneh gplus xyz"><a href="{{ url('orders/'.$order->id) }}">CONFIRM YOUR PAYMENT</a></button>
					<span class="or">Or</span>
					<button type="vutton" class="btn btn-aneh xyz"><a href="{{ url('/') }}">BACK TO HOMEPAGE</a></button><br><br>
				</div>
				
			</div>
		</div>
		
	</div>
@endsection

@section('scripts')
<script src="{{ asset(null) }}responsive/bootstrap/js/jquery.bootstrap-responsive-tabs.min.js"></script>
<script src="{{ asset(null) }}responsive/js/jquery.fancybox.min.js"></script>
@endsection