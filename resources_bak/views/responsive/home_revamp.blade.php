@extends('responsive.layout.master_revamp')

@section('customcss')
<link rel='stylesheet' href='{{ asset(null) }}responsive_new/assets/bootstrap/css/bootstrap-responsive-tabs.min.css'>
<link rel='stylesheet' href='{{ asset(null) }}responsive_new/assets/css/jquery.fancybox.min.css'>
@endsection

@section('content')
	<div id="godOfContent">
        <div class="newboxMenu after_clear">
            <ul id="grocerMenu" class="newleftMenu ul_nostyle">
                @foreach( App\Models\Groceries\Category::orderBy('order')->get() as $category )
                	
                    <li>
                        <a href="{!! url('groceries/' . $category->slug) !!}{{ strtoupper($category->name) }}">
                            <img src="{!! asset(null) !!}{{ $category->image->file }}">
                            {{ $category->name }}
                        </a>

                        @if(count($category->child) > 0)
                        <div class="newSubmenu">
                            <ul class="ul_nostyle">
                                @foreach($category->child as $child_category)
                                <li>
                                    <a href="{!! url('groceries/' . $child_category->slug) !!}">
                                    {{ ucfirst($child_category->name) }}
                                    </a>
                                </li>
                                @endforeach
                            </ul>
                        @endif
                    </li>
                @endforeach
			</ul>

			<!-- Slider -->
			<div class="slick-slider">
				<?php $i = 0 ?>
        		@foreach (App\Models\Home\Slide::where('is_active', 1)->orderBy('order')->get() as $slide)
          			<?php $class = '' ?>
          			@if ($i == 0)
            			<?php $class = 'is-active' ?>
          			@endif
     			<div>
		     		<img src="{!! asset(null) !!}headlines/{{ $slide->file_name }}" alt="space">
		     	</div>
		     		<?php $i++ ?>
        		@endforeach
		    </div>

		</div>

		<!-- Main Content -->
		<div class="box-content">
    
			<!-- explore product -->
			<div class="exploreProd">


				@foreach($new_groceries_body as $grocery)
				@if($grocery->grocery_list->count() > 0)
				
				<!-- Squid & Octopus  -->
				<div class="exploreProdList">
					<div class="headingTitle after_clear">
						<h1>{{ $grocery->name }}</h1>
						<a href="{!! url('groceries/' . $category->slug) !!}" class="linkMore">See all products</a>
					</div>
        			<div class="prodlistInner after_clear">
						
        				@foreach($grocery->grocery_list as $g)
          				<a class="prodItem" alt="{{ ucwords($g->name) }} title="{{ ucwords($g->name) }}" href="{{ url('groceries/'.$g->id.'/'.str_slug($g->name)) }}">

            				<img src="{{ $g->getFirstImageUrl($g->id) }}" style="max-height:65%;width:auto">
							{{-- <img src="{{ asset(null) }}responsive_new/assets/images/products/crabs.jpg"> --}}
            				<div class="prodCaption">
								{{ str_limit($g->name, $limit = 20, $end = '...') }}
              					<div class="oth_desc">{{ Money::display($g->price) }}</div>
            				</div>
          				</a>

						@endforeach

          				
        			</div>
      			</div>
    			<!-- Squid & Octopus -->
    			@endif
				@endforeach

		    </div>
		    <!-- explore product -->

  		</div>

  		<!-- Feature -->
		<div class="feature-box">
			<div class="box-content">
				<ul>
		
					<li><a href="#"><img src="{{ asset(null) }}responsive_new/assets/images/benefits/same_day.png">
					<div class="label-feature">Quality <br/> Assurance</div>
					</a></li>
					<li><a href="#"><img src="{{ asset(null) }}responsive_new/assets/images/benefits/best_price.png">
					<div class="label-feature">Best Price <br/> Guarantee</div>
					</a></li>
					<li><a href="#"><img src="{{ asset(null) }}responsive_new/assets/images/benefits/reliable_cold.png">
					<div class="label-feature">Reliable cold<br/> chain Logistics</div>
					</a></li>
					<li><a href="#"><img src="{{ asset(null) }}responsive_new/assets/images/benefits/huge_domestic.png">
					<div class="label-feature">Huge domestic/<br/>Import Selection</div>
					</a></li>
					<li><a href="#"><img src="{{ asset(null) }}responsive_new/assets/images/benefits/cashback.png">
					<div class="label-feature">Cashback/<br/>rewards</div>
					</a>
					</li>
				</ul>
			</div>
		</div>


  		
	</div>
        
	
@endsection