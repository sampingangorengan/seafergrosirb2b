	<title>
		{{ app()->environment() == 'production' || app()->environment() == 'live' ? '' : '('.app()->environment().') ' }}
    	@yield('title', 'SEAFERMART')
	</title>
  	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate"/>
    <meta http-equiv="Pragma" content="no-cache"/>
    <meta http-equiv="Expires" content="0"/>
	<link rel='stylesheet' href='{{ asset(null) }}responsive/bootstrap/css/bootstrap.min.css'>
	<link rel="stylesheet" type="text/css" href="{{ asset(null) }}responsive/css/menu.css">
	<link rel="stylesheet" type="text/css" href="{{ asset(null) }}responsive/css/jquery.typeahead.css">
	<link rel="stylesheet" type="text/css" href="{{ asset(null) }}responsive/css/main.css">
	<link rel="stylesheet" type="text/css" href="{{ asset(null) }}responsive/css/tablet.css">
	
	@yield('customcss')