<footer>
	<div class="container">
		<div class="box-content">
			<div class="row">
				<div class="menu-footer col-md-2 col-sm-2 no-padding-h">
					<h6>WHY US</h6>
					<ul class="list-inline">
						<li><a href="{!! url('about-us') !!}">About Us</a></li>
						<li><a href="{!! url('become-member') !!}">Become a Member</a></li>
						{{-- <li><a href="{!! url('best-price-guarantee') !!}">Best Price Guarantee</a></li> --}}
					</ul>
				</div>
				<div class="menu-footer col-md-2 col-sm-2 no-padding-h">
					<h6>ORDERS</h6>
					<ul class="list-inline">
						<li><a href="{!! url('loyalty-points') !!}">Cashback Rewards</a></li>
						<li><a href="{!! url('payment') !!}">Payment</a></li>
						<li><a href="{!! url('delivery') !!}">Delivery</a></li>
						<li><a href="{!! url('return-and-exchange') !!}">Return & Exchange</a></li>
										</ul>
				</div>
				<div class="menu-footer col-md-2 col-sm-2 no-padding-h">
					<h6>CUSTOMER CARE</h6>
					<ul class="list-inline">
						<li><a href="{{ url('user/my-account') }}">My Account</a></li>
						<li><a href="{{ url('suggestion') }}">Suggest a Product</a></li>
						<li><a href="{{ url('faq') }}">FAQ</a></li>
						<li><a href="{{ url('contact-us') }}">Contact Us</a></li>
										</ul>
				</div>
				<div class="col-md-6 col-sm-6 no-padding-h">
					<div class="form-box">
						<h6><a href="#" data-open="modal-flash">STAY IN TOUCH</a></h6>
						{!! Form::open(['method'=>'POST', 'action'=>'NewsletterController@store']) !!}
						<ul class="menu list-inline"">
							<li><input type="email" name="email" placeholder="Email Address"></li>
							<li><button type="submit" class="button">SEND</button></li>
						</ul>
						<ul class="socmed-list">
							<li><a href="https://www.facebook.com/seafermart/"><img src="{{ asset(null) }}responsive/images/ico-fb.png" data-rjs="3"></a></li>
							<li><a href="https://www.instagram.com/seafermart/"><img src="{{ asset(null) }}responsive/images/ico-ig.png" data-rjs="3"></a></li>
							{{-- <li><a href="https://www.facebook.com/seafermart/"><img src="{{ asset(null) }}responsive/images/ico-line.png" data-rjs="3"></a></li>
							<li><a href="https://www.instagram.com/seafermart/"><img src="{{ asset(null) }}responsive/images/ico-youtube.png" data-rjs="3"></a></li> --}}
						</ul>
						{!! Form::close() !!}
					</div>
				</div>
				<div class="box_down_app">
					<a href=""><img src="{{ asset(null) }}responsive_new/assets/images/materials/img_appstore.png"></a>
					<a href=""><img src="{{ asset(null) }}responsive_new/assets/images/materials/img_playstore.png"></a>
				</div>
				<div class="box_call_bubble">
			      <ul>
			        <li>
			          <a href="https://api.whatsapp.com/send?phone=02122333288">+62 (021-22333 - 288)</a>
			        </li>
			        <li>
			          <a href="tel:02122333288">+62 (021-22333 - 288)</a>
			        </li>
			      </ul>
			    </div>
			</div>
		</div>
	</div>
	
</footer>