	<header>
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-xs-6 no-padding-h">
					<ul>
	                    <li><b>QUALITY SEAFOOD FOR ALL</b></li>
	                    {{--<li><b>Delivery within Jabodetabek only</b></li>--}}
	                </ul>
				</div>
				<div class="col-md-6 col-xs-6 no-padding-h">
					<ul class="pull-right">
						<li><a href="{{ url('about-us') }}">About Us</a></li>

                    	<li><a href="{{ url('become-member') }}">Member Benefits</a></li>
					</ul>
				</div>
			</div>
		</div>
	</header>