<div class="sub-nav">
		<div class="container">
			<div class="row">
				<div class="col-md-12 no-padding-h">
				<!-- menu old -->
				<ul class="left-side">
					<li class="dropdown-submenu" style="float:left"><a href="#" id="grocer"> <span class="text-blue">SEAFER</span>&nbsp;<span class="text-red">GROCER</span></a>
						<div class="second-lv grocer" id="secondGrocer">
							<div class="box-content" style="float:left">
								<ul>                            
									 <li class="third-submenu-right" style="min-width:215px;">
										<a href="#" id="thirdGrocerBtn1">MEAT<span class="glyphicon glyphicon-chevron-right" style="margin-left:0;float:right;font-size:10px;color:grey"></span></a>
																				</li>
																									
									<li class="third-submenu-right" style="min-width:215px;">
										<a href="#" id="thirdGrocerBtn2">SEAFOOD<span class="glyphicon glyphicon-chevron-right" style="margin-left:0;float:right;font-size:10px;color:grey"></span></a>
																				</li>
																									
									<li class="third-submenu-right" style="min-width:215px;">
										<a href="#" id="thirdGrocerBtn3">DAIRY<span class="glyphicon glyphicon-chevron-right" style="margin-left:0;float:right;font-size:10px;color:grey"></span></a>
																				</li>
																									
									<li class="third-submenu-right" style="min-width:215px;">
										<a href="#" id="thirdGrocerBtn4">BEVERAGES<span class="glyphicon glyphicon-chevron-right" style="margin-left:0;float:right;font-size:10px;color:grey"></span></a>
																				</li>
																									
									<li class="third-submenu-right" style="min-width:215px;">
										<a href="#" id="thirdGrocerBtn5">SNACKS<span class="glyphicon glyphicon-chevron-right" style="margin-left:0;float:right;font-size:10px;color:grey"></span></a>
																				</li>
																									
									<li class="third-submenu-right" style="min-width:215px;">
										<a href="#" id="thirdGrocerBtn6">PANTRY<span class="glyphicon glyphicon-chevron-right" style="margin-left:0;float:right;font-size:10px;color:grey"></span></a>
																				</li>
																									
									<li class="third-submenu-right" style="min-width:215px;">
										<a href="#" id="thirdGrocerBtn7">DRY &amp; INSTANT FOOD<span class="glyphicon glyphicon-chevron-right" style="margin-left:0;float:right;font-size:10px;color:grey"></span></a>
																				</li>
																									
									<li class="third-submenu-right" style="min-width:215px;">
										<a href="#" id="thirdGrocerBtn8">FROZEN<span class="glyphicon glyphicon-chevron-right" style="margin-left:0;float:right;font-size:10px;color:grey"></span></a>
																				</li>
																									
									<li class="third-submenu-right" style="min-width:215px;">
										<a href="#" id="thirdGrocerBtn9">CHILLED<span class="glyphicon glyphicon-chevron-right" style="margin-left:0;float:right;font-size:10px;color:grey"></span></a>
																				</li>
																									
									<li class="third-submenu-right" style="min-width:215px;">
										<a href="#" id="thirdGrocerBtn10">HEALTHY FOOD<span class="glyphicon glyphicon-chevron-right" style="margin-left:0;float:right;font-size:10px;color:grey"></span></a></li>
																									
									<li class="third-submenu-right"><a href="groceries/vegetables.html">VEGETABLES</a></li>
								</ul>

							</div>
							<div class="box-content second-level-category"  style="float:left;margin-left:0;display:none;">
								<div class="third-lv" id="thirdGrocer1" >
									<ul style="margin-left:0">
										<li style="padding:0 !important;"><a href="groceries/beef.html" style="font-size:12px !important">BEEF</a></li>
										<li style="padding:0 !important;"><a href="groceries/lamb.html" style="font-size:12px !important">LAMB</a></li>
										<li style="padding:0 !important;"><a href="groceries/chicken.html" style="font-size:12px !important">CHICKEN</a></li>
										<li style="padding:0 !important;"><a href="groceries/pork.html" style="font-size:12px !important">PORK</a></li>
										<li style="padding:0 !important;"><a href="groceries/turkey.html" style="font-size:12px !important">TURKEY</a></li>
										<li style="padding:0 !important;"><a href="groceries/duck.html" style="font-size:12px !important">DUCK</a></li>
										
									</ul>
								</div>
								<div class="third-lv" id="thirdGrocer2" >
									<ul style="margin-left:0">
										<li style="padding:0 !important;"><a href="groceries/fish.html" style="font-size:12px !important">FISH</a></li>
										<li style="padding:0 !important;"><a href="groceries/crab.html" style="font-size:12px !important">CRAB</a></li>
										<li style="padding:0 !important;"><a href="groceries/prawn-shrimp.html" style="font-size:12px !important">PRAWN/SHRIMP</a></li>
										<li style="padding:0 !important;"><a href="groceries/mussels-clams.html" style="font-size:12px !important">MUSSELS &amp; CLAMS</a></li>
										<li style="padding:0 !important;"><a href="groceries/lobster-crayfish.html" style="font-size:12px !important">LOBSTER &amp; CRAYFISH</a></li>
										<li style="padding:0 !important;"><a href="groceries/scallops.html" style="font-size:12px !important">SCALLOPS</a></li>
										<li style="padding:0 !important;"><a href="groceries/squid-octopus.html" style="font-size:12px !important">SQUID/OCTOPUS</a></li>
										<li style="padding:0 !important;"><a href="groceries/froglegs.html" style="font-size:12px !important">FROGLEGS</a></li>
										
									</ul>
								</div>
								<div class="third-lv" id="thirdGrocer3" >
									<ul style="margin-left:0">
										<li style="padding:0 !important;"><a href="groceries/milk.html" style="font-size:12px !important">MILK</a></li>
										<li style="padding:0 !important;"><a href="groceries/eggs.html" style="font-size:12px !important">EGGS</a></li>
										<li style="padding:0 !important;"><a href="groceries/cheese.html" style="font-size:12px !important">CHEESE</a></li>
										<li style="padding:0 !important;"><a href="groceries/yoghurt.html" style="font-size:12px !important">YOGHURT</a></li>
										<li style="padding:0 !important;"><a href="groceries/butter.html" style="font-size:12px !important">BUTTER</a></li>
										<li style="padding:0 !important;"><a href="groceries/margarine.html" style="font-size:12px !important">MARGARINE</a></li>
										
									</ul>
								</div>
								<div class="third-lv" id="thirdGrocer4" >
									<ul style="margin-left:0">
										<li style="padding:0 !important;"><a href="groceries/tea.html" style="font-size:12px !important">TEA</a></li>
										<li style="padding:0 !important;"><a href="groceries/coffee.html" style="font-size:12px !important">COFFEE</a></li>
										<li style="padding:0 !important;"><a href="groceries/juice.html" style="font-size:12px !important">JUICE</a></li>
										<li style="padding:0 !important;"><a href="groceries/water.html" style="font-size:12px !important">WATER</a></li>
										<li style="padding:0 !important;"><a href="groceries/soda.html" style="font-size:12px !important">SODA</a></li>
										<li style="padding:0 !important;"><a href="groceries/energy-drink.html" style="font-size:12px !important">ENERGY DRINK</a></li>
										<li style="padding:0 !important;"><a href="groceries/syrup.html" style="font-size:12px !important">SYRUP</a></li>
										<li style="padding:0 !important;"><a href="groceries/flavored-drink.html" style="font-size:12px !important">FLAVORED DRINK</a></li>
										
									</ul>
								</div>
								<div class="third-lv" id="thirdGrocer5" >
									<ul style="margin-left:0">
										<li style="padding:0 !important;"><a href="groceries/candy.html" style="font-size:12px !important">CANDY</a></li>
										<li style="padding:0 !important;"><a href="groceries/biscuit-cookie.html" style="font-size:12px !important">BISCUIT &amp; COOKIE</a></li>
										<li style="padding:0 !important;"><a href="groceries/chocolate.html" style="font-size:12px !important">CHOCOLATE</a></li>
										<li style="padding:0 !important;"><a href="groceries/chips.html" style="font-size:12px !important">CHIPS</a></li>
										<li style="padding:0 !important;"><a href="groceries/crackers.html" style="font-size:12px !important">CRACKERS</a></li>
										<li style="padding:0 !important;"><a href="groceries/dried-snack.html" style="font-size:12px !important">DRIED SNACK</a></li>
										<li style="padding:0 !important;"><a href="groceries/nuts.html" style="font-size:12px !important">NUTS</a></li>
										
									</ul>
								</div>
								<div class="third-lv" id="thirdGrocer6" >
									<ul style="margin-left:0">
										<li style="padding:0 !important;"><a href="groceries/soy-sauce-vinegar.html" style="font-size:12px !important">SOY SAUCE &amp; VINEGAR</a></li>
										<li style="padding:0 !important;"><a href="groceries/sauces.html" style="font-size:12px !important">SAUCES</a></li>
										<li style="padding:0 !important;"><a href="groceries/seasoning.html" style="font-size:12px !important">SEASONING</a></li>
										<li style="padding:0 !important;"><a href="groceries/oil.html" style="font-size:12px !important">OIL</a></li>
										<li style="padding:0 !important;"><a href="groceries/dressing-topping.html" style="font-size:12px !important">DRESSING &amp; TOPPING</a></li>
										<li style="padding:0 !important;"><a href="groceries/jam-spread.html" style="font-size:12px !important">JAM &amp; SPREAD</a></li>
										<li style="padding:0 !important;"><a href="groceries/honey-syrup.html" style="font-size:12px !important">HONEY &amp; SYRUP</a></li>
										<li style="padding:0 !important;"><a href="groceries/cooking-cream.html" style="font-size:12px !important">COOKING CREAM</a></li>
										<li style="padding:0 !important;"><a href="groceries/rice.html" style="font-size:12px !important">RICE</a></li>
										<li style="padding:0 !important;"><a href="groceries/flour-flavoring.html" style="font-size:12px !important">FLOUR &amp; FLAVORING</a></li>
									</ul>
								</div>
								<div class="third-lv" id="thirdGrocer7" >
									<ul style="margin-left:0">
										<li style="padding:0 !important;"><a href="groceries/instant-food.html" style="font-size:12px !important">INSTANT FOOD</a></li>
										<li style="padding:0 !important;"><a href="groceries/pasta-noodle.html" style="font-size:12px !important">PASTA &amp; NOODLE</a></li>
										<li style="padding:0 !important;"><a href="groceries/cereal-oats-grains-seeds.html" style="font-size:12px !important">CEREAL, OATS, GRAINS &amp; SEEDS</a></li>
										<li style="padding:0 !important;"><a href="groceries/canned-food.html" style="font-size:12px !important">CANNED FOOD</a></li>
										<li style="padding:0 !important;"><a href="groceries/canned-fruits.html" style="font-size:12px !important">CANNED FRUITS</a></li>
										<li style="padding:0 !important;"><a href="groceries/other-dried-food.html" style="font-size:12px !important">OTHER DRIED FOOD</a></li>
										
									</ul>
								</div>
								<div class="third-lv" id="thirdGrocer8" >
									<ul style="margin-left:0">
										<li style="padding:0 !important;"><a href="groceries/ice-cream.html" style="font-size:12px !important">ICE CREAM</a></li>
										<li style="padding:0 !important;"><a href="groceries/frozen-seafood.html" style="font-size:12px !important">FROZEN SEAFOOD</a></li>
										<li style="padding:0 !important;"><a href="groceries/frozen-fuit.html" style="font-size:12px !important">FROZEN FUIT</a></li>
										<li style="padding:0 !important;"><a href="groceries/frozen-pastry-cake-bread.html" style="font-size:12px !important">FROZEN PASTRY, CAKE &amp; BREAD</a></li>
										<li style="padding:0 !important;"><a href="groceries/frozen-vegetables.html" style="font-size:12px !important">FROZEN VEGETABLES</a></li>
										<li style="padding:0 !important;"><a href="groceries/frozen-meat.html" style="font-size:12px !important">FROZEN MEAT</a></li>
										<li style="padding:0 !important;"><a href="groceries/other-frozen-food.html" style="font-size:12px !important">OTHER FROZEN FOOD</a></li>
										
									</ul>
								</div>
								<div class="third-lv" id="thirdGrocer9" >
									<ul style="margin-left:0">
										<li style="padding:0 !important;"><a href="groceries/sausage.html" style="font-size:12px !important">SAUSAGE</a></li>
										<li style="padding:0 !important;"><a href="groceries/ham.html" style="font-size:12px !important">HAM</a></li>
										<li style="padding:0 !important;"><a href="groceries/smoked.html" style="font-size:12px !important">SMOKED</a></li>
										<li style="padding:0 !important;"><a href="groceries/dessert.html" style="font-size:12px !important">DESSERT</a></li>	
									</ul>
								</div>
								<div class="third-lv" id="thirdGrocer10" >
									<ul style="margin-left:0">
										<li style="padding:0 !important;"><a href="groceries/milks.html" style="font-size:12px !important">MILKS</a></li>
										<li style="padding:0 !important;"><a href="groceries/snack.html" style="font-size:12px !important">SNACK</a></li>
										<li style="padding:0 !important;"><a href="groceries/grains.html" style="font-size:12px !important">GRAINS</a></li>
										<li style="padding:0 !important;"><a href="groceries/seeds.html" style="font-size:12px !important">SEEDS</a></li>
										<li style="padding:0 !important;"><a href="groceries/detox-juice.html" style="font-size:12px !important">DETOX JUICE</a></li>
										
									</ul>
								</div>                                                              
							</div>
							<div style="both:clear"></div>
						</div>
					</li>
				</ul>
				<!-- menu old end -->	
				</div>
			</div>
		</div>
	</div>