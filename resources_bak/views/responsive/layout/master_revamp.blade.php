<!DOCTYPE html>
<html>
<head>
    @include('responsive.layout.html_header_revamp')
</head>
<body>
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.9&appId=1900251586926025";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

    <!-- DOWNLOAD    -->
    <div class="boxDownloadApp after_clear">
        <div class="img_ico">
            <img src="{!! asset(null) !!}responsive_new/assets/images/img-maskot.png">
        </div>
        <div class="text_app">
            <span>New User?</span>
            <span>Get $5 Coupon</span>
        </div>
        <a class="download_app">
            Download
        </a>
    </div>  
    <!-- DOWNLOAD    -->
    
	@include('responsive.layout.header_revamp')

	@include('responsive.layout.topbar')

	@include('responsive.layout.subnav')

	@include('responsive.layout.sidemenu')

	@yield('content')

@include('responsive.layout.footer')

<div class="copyright">
  <div class="box-content">
	<div class="col-lg-12">
		<span>&copy; 2018 SeaferMart All Rights Reserved</span>
	</div>
  </div>
</div>

	@include('responsive.layout.modals')

{{-- <script src="{{ asset(null) }}responsive/js/jquery-1.12.4.min.js"></script> --}}
<script src="{{ asset(null) }}responsive/js/modernizr.custom.js"></script>
<script src="{{ asset(null) }}responsive/js/jquery.min.js"></script>
<script src="{{ asset(null) }}responsive/bootstrap/js/bootstrap.min.js"></script>
<script src="{{ asset(null) }}responsive_new/assets/bootstrap/js/jquery.bootstrap-responsive-tabs.min.js"></script>
<script src="{{ asset(null) }}responsive/js/jquery.typeahead.js"></script>
<script src="{{ asset(null) }}responsive_new/assets/js/jquery.fancybox.min.js"></script>
<script src="{{ asset(null) }}responsive/js/jquery.dlmenu.js"></script>
{{-- <script src="{{ asset(null) }}responsive/js/jquery-ui.min.js"></script> --}}
<script src="{{ asset(null) }}responsive_new/assets/js/slick.min.js"></script>
<script src="{{ asset(null) }}responsive_new/assets/js/main.js?{{ time() }}"></script>
<script src="{{ asset(null) }}responsive_new/assets/js/slider.js"></script>

@yield('scripts')

<script>
$(function() {
	$( '#dl-menu' ).dlmenu({
		animationClasses : { classin : 'dl-animate-in-2', classout : 'dl-animate-out-2' }
	});
});
$('#log-me-out').on('click', function(){
   	phpLiveChat.endChat();
});
// Handling forgot password
$('.do-not-forget').click(function(e){
    e.preventDefault();
    $.ajax({
        type: 'post',
        url: "{{ url('password/email') }}",
        data: {
            email: $('.forget-me-not input').val()
        }
    }).done(function(result){
        console.log(result);
    });
});
$('form.forget-me-not').submit(function(e){
    $.ajax({
        type: 'post',
        url: "{{ url('password/email') }}",
        data: {
            email: $('.forget-me-not input').val()
        }
    }).done(function(result){
        console.log('ahahahaha');
        $('#modal-forgotpassword').removeAttr('style');
        $('#modal-forgotpassword').parent().removeAttr('style');
        $('#modal-afterForgotPassword').attr('style','top: 70px; display: block;');
        $('#modal-afterForgotPassword').parent().attr('style','display:block;')
    });
    e.preventDefault();
})
$.getJSON( "{{ url('groceries/all_data') }}", callbackFuncWithData);
function callbackFuncWithData(data)
{
    $.typeahead({
        input: ".search",
        minLength: 2,
        maxItem: 6,
        order: "asc",
        hint: true,
        backdrop: {
            "opacity":1,
            "background-color": "#ffffff"
        },
        emptyTemplate: 'No result',
        dynamic:true,
        href:"http://seafermart.co.id/groceries/@{{id}}/@{{slug}}",
        source: {
            name:{
                display: "name",
                data: data.data
            }
        },
        callback: {
            
        },
        debug: false
    });
}
@foreach (['danger', 'warning', 'success', 'info'] as $msg)
    @if(Session::has('alert-' . $msg))

$(document).ready(function(){
    $("#modal-session").modal();
});
    @endif
@endforeach

</script>
</body>
</html>