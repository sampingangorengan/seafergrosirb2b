	<header>
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-xs-6 no-padding-h">
					<ul>
						<li><b>CONNECTING PEOPLE THROUGH FOOD</b></li>
					</ul>
				</div>
				<div class="col-md-6 col-xs-6 no-padding-h">
					<ul class="pull-right">
						<li><a href="become-member.html">Become a Member</a></li>
						<li><a href="suggestion.html">Suggest a Product</a></li>
					</ul>
				</div>
			</div>
		</div>
	</header>