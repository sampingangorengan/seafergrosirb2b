<!-- Modal -->
<div id="modal-forgotpassword" class="modal fade modal-forgotpassword" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-box">
        <div class="dashed-box">
            <form method="post" action="#" class="forget-me-not">
                <h4>FORGOT PASSWORD</h4>
                <p>Put Your Email</p>
                <input type="text" name="email">
                {{ csrf_field() }}
				<br/>
                <button type="button" class="btn btn-aneh do-not-forget" data-toggle="modal" data-target="#modal-afterForgotPassword" data-dismiss="modal">SUBMIT</button>
            </form>
        </div>
    </div>
    </div>

  </div>
</div>

<!-- Modal -->
<div id="modal-session" class="modal fade modal-session" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-box">
        <div class="dashed-box">

            @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                @if(Session::has('alert-' . $msg))
                    @if($msg !== 'success')
                    <h4>Oops</h4>
                    @else
                    <h4>{{$msg}}</h4>
                    @endif
                    <p>{{ Session::get('alert-' . $msg) }} </p>
                @endif
            @endforeach
            
        </div>
    </div>
    </div>

  </div>
</div>

{{-- @if(auth()->check())
<div class="reveal modal-referralcode" id="modal-referralcode" data-reveal>
    <div class="modal-box">
        <div class="dashed-box">
            <!-- <h6><a href="{{ url('user/sign-up?refer_from='.auth()->user()->referral_code) }}">{{ url('user/sign-up?refer_from='.auth()->user()->referral_code) }}</a></h6> -->
            <p style="font-size:16px;">
              Invite your friends and enjoy 0.5% cashback from each time they shop from Seafermart!
            </p>
            <p style="font-size:12px;">(Disclaimer: This only applies for every Rp. 100,000,- purchase made from your successfully referred friends)</p>
            <div id="shareBtn" class="btn btn-success clearfix">Share to Facebook</div>
            <p>Or share by email</p>
            <!-- <a href="mailto:yourfriend@mailprovider.com?subject=Seafermart%20Referral%20Code&amp;body=Copy%20and%20paste%20this%20link%20{{ url('user/sign-up?refer_from='.auth()->user()->referral_code) }}%20to%20your%20browser%20in%20order%20to%20get%20benefit%20when%20you%20buy%20good%20things%20from%20seafermart.co.id">Send to mail</a> -->
            <form method="post" action="share-referral" id="share-referral-form">
              <input type="hidden" value="{{ url('user/sign-up?refer_from='.auth()->user()->referral_code) }}" name="r eferral_code">
              <input type="text" name="email" placeholder="Email">
              <button type="submit">SUBMIT</button>
            </form>
        </div>
    </div>
</div>
@endif --}}

<!-- Modal -->
<div id="modal-afterForgotPassword" class="modal fade modal-afterForgotPassword" role="dialog">
    <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-box">
        <div class="dashed-box">
            <div class="img-box">
                <img src="{{ asset(null) }}responsive/images/img-maskot.png">
            </div>
            <p class="text-blue">yay! the link to reset your password <br> has been sent to your email</p>
        </div>
    </div>
    </div>

    </div>
</div>
<div class="mask-modal"></div>
    <div id="customer-chat-widget" class="customer-chat customer-chat-widget loading-screen" style="display:none">
	
	</div>
 </div>