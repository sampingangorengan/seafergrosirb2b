
<div class="top-bar fixed-top">
		<div class="container">
			<div class="row">
				<div class="menu-mobile col-md-1 col-sm-1 col-xs-2">
					<nav>
						<button class="hamburger-button" role="tab">
							<i title="Show navigation">menu</i>
						  </button>
					</nav>
				</div>
				<div class="col-md-4 col-sm-9 col-xs-6">
					<div class="logo">
						<a href="{{ url('/') }}">
							<img src="{{ asset(null) }}assets/images/logo.png">
							<span class="text-red">SEAFER</span>&nbsp;<span class="text-blue">MART</span>
						</a>
					</div>
				</div>
				<div class="col-md-8 col-sm-2 col-xs-4 padding-v no-padding-h">
					<div class="top-bar-right pull-right">
						@if (auth()->check())
						<div class="sub-bar sign col-md-3 col-xs-3 no-padding-h">
							<a href="#" id="openAccount" class="text-red">
	                            <span class="fontsize-username" style="float:left;line-height:18px;">{{ auth()->user()->name }} <br>
	                            @if(auth()->user()->points()->get()->count() > 0)
	                            {{ Money::display(auth()->user()->points()->first()->nominal) }}
	                            @else
	                            Rp.0
	                            @endif</span>
	                            {{--<img src="{!! asset(null) !!}assets/images/ico-header-acc.png">--}}
	                            @if(auth()->user()->profile_picture)
	                                @if(strpos(auth()->user()->photo->file, 'facebook') !== false || strpos(auth()->user()->photo->file, 'google') !== false)
	                                    <img src="{{ auth()->user()->photo->file }}" style="width:40px;height:40px;border-radius: 20px;border:1px solid blue;">
	                                @else
	                                    <img src="{!! asset(null) !!}profile_picture/{{ auth()->user()->photo->file }}" style="width:40px;height:40px;border-radius: 20px;border:1px solid blue;">
	                                @endif
	                            @else
	                            <img src="{!! asset(null) !!}assets/images/ico-header-acc.png" >
	                            @endif
	                            {{--<img src="{!! asset(null) !!}assets/images/dot-merah-besar.png" style="position: absolute;left:115px !important;">--}}
	                        </a>
	                    </div>
						@else
						<div class="sub-bar sign col-md-1 col-xs-1 no-padding-h">
							<a href="user/sign-up.html">
								SIGN UP
							</a>
						</div>
						<div class="sub-bar sign col-md-1 col-xs-1 no-padding-h">
							<div class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">SIGN IN</a>
								<div id="popupSignin" class="popup-box-signin right2 dropdown-menu">
									<span class="text-blue" style="padding-left: 12px;">SIGN IN</span>
									<span class="close close-button"><img src="{{ asset(null) }}responsive/images/ico-close.png"></span>
									<div class="form-box" style="padding-left: 12px;">
										
										<form method="post" action="{{ url('user/sign-in') }}">
											<input type="text" placeholder="Your Email" name="email" style="border:2px solid #808184;">
											<input type="password" placeholder="Password" name="password" style="border:2px solid #808184;">
											<div class="box">
												<input id="keepMe2" type="checkbox" name="remember">
												<label for="keepMe2" style="color:#A9A9A9;font-size: 12px;padding-bottom: -1px;">Keep me signed in</label><br>
											</div>
											{{ csrf_field() }}
											<button class="btn pull-left" id="myLogin">SUBMIT</button>
											<a href="#" id="forgotPassword" data-toggle="modal" data-target="#modal-forgotpassword" class="pull-left withdatepick">Forgot Password?</a><br>
										</form>
										<a href="https://www.facebook.com/v2.8/dialog/oauth?client_id=1900251586926025&amp;redirect_uri=http%3A%2F%2Fseafermart.co.id%2Flogin%2Fcallback%2Ffacebook&amp;scope=email&amp;response_type=code&amp;state=Mb88TYlBUoaxwL0QqVCvORJFCwAry4kWZa6vxumf"><button class="btn sign-in fb">
											<img src="{{ asset(null) }}responsive/images/ico-fb.png">Sign In with Facebook
										</button></a><br/>
										<a href="https://accounts.google.com/o/oauth2/auth?client_id=1014662541040-e3k5a8g6nmn8cr7be2of7ahjhcvaekp2.apps.googleusercontent.com&amp;redirect_uri=http%3A%2F%2Fseafermart.co.id%2Flogin%2Fcallback%2Fgoogle&amp;scope=openid+profile+email&amp;response_type=code&amp;state=7wqQtYIT5PGfuheAJwauGnDEBykWbDugeQw13hmy"><button class="btn sign-in gplus">
											<img src="{{ asset(null) }}responsive/images/ico-gplus-white.png">Sign In with Google Plus
										</button></a><br/>
																	</div>
								</div>
							</div>
						</div>
						@endif

						<div class="sub-bar cart-toggle col-md-1 col-xs-6 no-padding-h">
							<div class="dropdown">
								<!-- cart 320 -->
								<a href="{!! url('cart') !!}" class="cart-320">
									<img src="{{ asset(null) }}responsive/images/ico-header-cart.png" class="cart">
									<div class="cart-notif" @if(count(Carte::contents(true)) < 1)style="display: none;"@endif>
										@if(count(Carte::contents(true)) > 0)
										{{ count(Carte::contents(true)) }}
										@endif
									</div>
								</a>
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<img src="{{ asset(null) }}responsive/images/ico-header-cart.png" class="cart">
									<div class="cart-notif" style="display: none;"></div>
								</a>
								<!-- ini kalau untuk empty cart style leftnya di ganti ya jadi left:-30px atau left:-20px -->
								<div id="popupChart" class="popup-box right1 dropdown-menu" onClick="event.stopPropagation();">
									<span class="text-blue">CART</span>
									<span class="close close-button-cart"><img src="{{ asset(null) }}responsive/images/ico-close.png"></span>

									<div class="cart_item_container">
										<div class="if-empty">
											<img src="{{ asset(null) }}responsive/images/img-empty.png?59d219a0b81d3">
											<span>Your cart is still empty</span>
										</div>
									</div>
																
								</div>
							</div>
						</div>
						<div class="sub-bar search-mobile col-md-1 col-xs-6 no-padding-h">
								<a href="javascript:void(0)" id="show-search">
									<img src="{{ asset(null) }}responsive/images/ico-search.png">
								</a>
						</div>
						<div class="sub-bar search-box col-md-7 col-xs-12 no-padding-h">
							{!! Form::open(['method'=>'POST', 'url'=>'search']) !!}
								<div class="col-md-10 col-sm-11 col-xs-10 no-padding-h">
									<div class="typeahead__container">
										<div class="typeahead__field">
											<span class="typeahead__query">
												<input class="search js-typeahead" type="search" placeholder="I'm looking for" name="search" autocomplete="off">
											</span>
										</div>
									</div>
								</div>
								<div class="col-md-2 col-sm-1 col-xs-2 no-padding-h">
									<button type="submit" class="button search-btn"><img src="{{ asset(null) }}responsive/images/ico-search.png"></button>
								</div>
							{!! Form::close() !!}
							
						</div>
					</div>
				</div>
				
			</div>
			
		</div>
		
	</div>