<!-- side menu -->
<div id="side-nav">
	@if(auth()->check())
	<div class="col-md-12 no-padding-h">
		<a class="menu-account ac-profile">{{ auth()->user()->name }}</a>
	</div>
	<div class="col-md-12 no-padding-h">
		<a class="menu-myaccount" href="{{ url('/orders') }}">MY ORDERS</a>
	</div>
	<div class="col-md-12 no-padding-h">
		<a class="menu-myaccount" href="{{ url('user/my-cashback-rewards') }}">MY CASHBACK BALANCE</a>
	</div>
	<div class="col-md-12 no-padding-h">
		<a class="menu-myaccount" href="{{ url('user/my-rating-review') }}">MY RATINGS AND REVIEWS</a>
	</div>
	<div class="col-md-12 no-padding-h">
		<a class="menu-myaccount" href="{{ url('user/my-account') }}">MY ACCOUNT</a>
	</div>
	@else
	<div class="col-md-12 no-padding-h">
		<a class="menu-signin" href="{{ url('user/sign-in').'?r='.$_SERVER['REQUEST_URI'] }}">SIGN IN</a>
	</div>
	<div class="col-md-12 no-padding-h">
		<a class="menu-signup" href="{{ url('user/sign-up') }}">SIGN UP</a>
	</div>
	@endif
  	<div id="dl-menu" class="dl-menuwrapper">
		<ul class="dl-menu dl-menuopen">
			<li>
				<a href="#">SEAFER GROCER</a>
				<ul class="dl-submenu">
					@foreach( App\Models\Groceries\Category::orderBy('order')->get() as $category )
					<li>
						<a href="#{{-- {!! url('groceries/' . $category->slug) !!} --}}">{{ strtoupper($category->name) }}</a>
						<ul class="dl-submenu">
							@foreach($category->child as $child_category)
							<li><a href="{!! url('groceries/' . $child_category->slug) !!}">{{ strtoupper($child_category->name) }}</a></li>
							@endforeach
						</ul>
					</li>
					@endforeach
				</ul>
			</li>
			<li>
				<a href="#">WHY US</a>
				<ul class="dl-submenu">
					<li>
						<a href="{!! url('about-us') !!}">ABOUT US</a>
						<a href="{!! url('become-member') !!}">BECOME A MEMBER</a>
						{{-- <a href="{!! url('best-price-guarantee') !!}">BEST PRICE GUARANTEE</a> --}}
					</li>
				</ul>
			</li>
			<li>
				<a href="#">HOW IT WORKS</a>
				<ul class="dl-submenu">
					<li>
						<a href="{!! url('loyalty-points') !!}">CASHBACK REWARDS</a>
						<a href="{!! url('payment') !!}">PAYMENT</a>
						<a href="{!! url('delivery') !!}">DELIVERY</a>
						
					</li>
				</ul>
			</li>
			<li>
				<a href="#">HELP</a>
				<ul class="dl-submenu">
					<li>
						<a href="{!! url('return-and-exchange') !!}">RETURN & EXCHANGE</a>
						<a href="{!! url('suggestion') !!}">SUGGEST A PRODUCT</a>
						<a href="{!! url('faq') !!}">FAQ</a>
						<a href="{!! url('contact-us') !!}">CONTACT US</a>
					</li>
				</ul>
			</li>
			{{-- <li>
				<a href="#">SEAFER COOK</a>
				<ul class="dl-submenu">
					<li>
						<a href="#">Living Room</a>
						<ul class="dl-submenu">
							<li><a href="#">Sofas &amp; Loveseats</a></li>
							<li><a href="#">Coffee &amp; Accent Tables</a></li>
							<li><a href="#">Chairs &amp; Recliners</a></li>
							<li><a href="#">Bookshelves</a></li>
						</ul>
					</li>
					<li>
						<a href="#">Bedroom</a>
						<ul class="dl-submenu">
							<li>
								<a href="#">Beds</a>
								<ul class="dl-submenu">
									<li><a href="#">Upholstered Beds</a></li>
									<li><a href="#">Divans</a></li>
									<li><a href="#">Metal Beds</a></li>
									<li><a href="#">Storage Beds</a></li>
									<li><a href="#">Wooden Beds</a></li>
									<li><a href="#">Children's Beds</a></li>
								</ul>
							</li>
							<li><a href="#">Bedroom Sets</a></li>
							<li><a href="#">Chests &amp; Dressers</a></li>
						</ul>
					</li>
					<li><a href="#">Home Office</a></li>
					<li><a href="#">Dining &amp; Bar</a></li>
					<li><a href="#">Patio</a></li>
				</ul>
			</li>
			<li>
				<a href="#">SEAFER EATS</a>
				<ul class="dl-submenu">
					<li><a href="#">Fine Jewelry</a></li>
					<li><a href="#">Fashion Jewelry</a></li>
					<li><a href="#">Watches</a></li>
					<li>
						<a href="#">Wedding Jewelry</a>
						<ul class="dl-submenu">
							<li><a href="#">Engagement Rings</a></li>
							<li><a href="#">Bridal Sets</a></li>
							<li><a href="#">Women's Wedding Bands</a></li>
							<li><a href="#">Men's Wedding Bands</a></li>
						</ul>
					</li>
				</ul>
			</li> --}}
			
		</ul>
	</div>

	@if( auth()->check() )
	{{-- <div class="clearfix"></div> --}}
	<div class="col-md-12 no-padding-h logout">
		<a class="menu-signup" href="{{ url('user/sign-out') }}">LOGOUT</a>
	</div>
	@endif
</div>
	
{{-- </div> --}}
<!-- side menu end -->	