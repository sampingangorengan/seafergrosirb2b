@extends('responsive.layout.master')

@section('title', App\Page::createTitle('Order Claim Form'))

@section('customcss')
    <link rel='stylesheet' href='{{ asset(null) }}responsive/bootstrap/css/bootstrap-responsive-tabs.css'>
    <link rel="stylesheet" type="text/css" href="{{ asset(null) }}responsive/css/jquery.fancybox.min.css">
@endsection

@section('content')
	<div class="main-content">
		<div class="container" id="SignIn">
			<div class="row">
				<div class="col-md-12">
					<h5 class="title title-margin">ADD NEW ADDRESS</h5>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-sm-7 col-xs-10 col-centered">

	                {!! Form::open(['method'=>'POST', 'action'=>'OrderClaimController@store', 'files' => true, 'id'=>'order-claim']) !!}
		                
		                <div class="form-box">
		                    <div class="row">
		                        <div class="small-4 columns">
		                            <label for="middle-label" class="text-left">Claim Type<span class="red">*</span></label>
		                        </div>
		                        <div class="small-8 columns">
		                            <select class="short" name="is_exchange_refund" required style="width:100%;">
		                                <option value="0">Please select type of claim.</option>
		                                <option value="exchange">Return &amp; Exchange</option>
		                                <option value="refund">Refund</option>
		                            </select>
		                            
		                        </div>
		                    </div>
		                    <div class="row">
		                        <div class="small-4 columns">
		                            <label for="middle-label" class="text-left">FULL NAME<span class="red">*</span></label>
		                        </div>
		                        <div class="small-8 columns">
		                            <input type="text"  placeholder="type your full name here" name="name">
		                        </div>
		                    </div>
		                    <div class="row">
		                        <div class="small-4 columns">
		                            <label for="middle-label" class="text-left">EMAIL<span class="red">*</span></label>
		                        </div>
		                        <div class="small-8 columns">
		                            <input type="email"  placeholder="type your email here" name="email" style="height: 24px;padding: 0 .5rem;border: 1.5px solid #808184;">
		                        </div>
		                    </div>
		                    <div class="row">
		                        <div class="small-4 columns">
		                            <label for="middle-label" class="text-left">MOBILE NO.<span class="red">*</span></label>
		                        </div>
		                        <div class="small-8 columns">
		                            <input type="text"  placeholder="type your mobile number here" name="mobile">
		                        </div>
		                    </div>
		                    <div class="row">
		                        <div class="small-4 columns">
		                            <label for="middle-label" class="text-left">ORDER ID<span class="red">*</span></label>
		                        </div>
		                        <div class="small-8 columns">
		                            <input type="text"  placeholder="type your order id/number here" name="order_id">
		                        </div>
		                    </div>
		                    <div class="row">
		                        <div class="small-4 columns">
		                            <label for="middle-label" class="text-left">PRODUCT SKU<span class="red">*</span></label>
		                        </div>
		                        <div class="small-8 columns">
		                            <input type="text"  placeholder="type product sku here" name="sku">
		                        </div>
		                    </div>
		                    <div class="row">
		                        <div class="small-4 columns">
		                            <label for="middle-label" class="text-left">REASON OF CLAIM<span class="red">*</span></label>
		                        </div>
		                        <div class="small-8 columns">
		                            <select class="short" name="reason" required style="width:100%;">
		                                <option value="0">Please select reason of claim.</option>
		                                <option value="incorrect">Incorrect</option>
		                                <!-- <option value="missing">Missing</option> -->
		                                <option value="damaged">Damaged</option>
		                            </select>
		                        </div>
		                    </div>
		                    <div class="row">
		                        <div class="small-4 columns">
		                            <label for="middle-label" class="text-left">UPLOAD<span class="red">*</span></label>
		                        </div>
		                        <div class="small-8 columns">
		                            {{-- <input id="uploadFile" placeholder="" disabled="disabled"  style="border:3px solid #ffffff;"/> --}}
		                            
		                                {{--<input id="uploadBtn" type="file" class="upload" name="file" required><img src="assets/images/ico-upload.png"></input>--}}
	                                <div class="preview img-wrapper"></div>
	                                <div class="file-upload-wrapper">
	                                    <input type="file" name="file" class="file-upload-native" accept="image/*" style="top:-50px"/>
	                                    <input type="text" disabled class="file-upload-text btn-upload" style="top:-50px"/>
	                                </div>
		                            
		                        </div>
		                    </div>
		                    <div class="row">
		                        <p class="b2">
		                        <center>
		                            We reserve the right to investigate the legitimacy of complains and deny the ones that are being submitted to intentionally abuse SEAFERMART’s Refunds & Returns Policy scheme.
		                        </center>
		                        </p>
		                    </div>
		                    <div class="row">
		                        <div class="col-md-8">
		                        	<div class="col-md-6 col-sm-6 col-xs-6">
			                            <button data-open="modal-submit" class="btn btn-action blue space-3 savebutton" type="submit"  tabindex="0" style="margin-left:60%">SUBMIT</button>
			                        </div>
		                        </div>
		                    </div>
		                </div>
		                {!! Form::close() !!}
		            </div>
		        </div>
			</div>
            
		</div>
	</div>
@endsection

@section('scripts')
<script src="{{ asset(null) }}responsive/bootstrap/js/jquery.bootstrap-responsive-tabs.min.js"></script>
<script src="{{ asset(null) }}responsive/js/jquery.fancybox.min.js"></script>
@endsection