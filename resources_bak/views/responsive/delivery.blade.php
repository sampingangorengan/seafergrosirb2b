@extends('responsive.layout.master')

@section('title', App\Page::createTitle('Delivery'))

@section('customcss')
    <link rel='stylesheet' href='{{ asset(null) }}responsive/bootstrap/css/bootstrap-responsive-tabs.css'>
    <link rel="stylesheet" type="text/css" href="{{ asset(null) }}responsive/css/jquery.fancybox.min.css">
@endsection

@section('content')
<div class="main-content">
	<div class="container">
        <div class="row hide-768">
			<div class="col-md-12">
				<span class="text-grey equal margin-top-10">
				  <a class="text-grey" href="{!! url('/') !!}"><b>HOME</b></a> /
				  <a class="text-grey" href="#"><b>DELIVERY</b></a>
				</span>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<h5 class="title title-margin">DELIVERY</h5>
			</div>
		</div>
		<div class="row delivery-box">
			<div class="col-md-7 col-sm-7 col-xs-10 col-centered">
				<p>
                    We are committed to delivering the products within the shortest time possible. For orders that are placed before 3 pm, we will deliver your orders within the same day with an estimated duration of 4-5 hours from the time the order is placed. For orders that are placed after 3 pm, we will deliver your orders the next day.
                </p>
                <br/>
                <p>
                    Besides relying on our Seafer courier, we are working with Gojek and JNE to help deliver the products to you in the safest and most efficient way.
                </p>
                <br/>
                <p>
                    Before delivery, we also adopt the best practices for packing both frozen and dry products to ensure that the products are delivered to you in the best condition possible.
                </p>
                {{-- <div class="sub-content">
                    For Seafer Grocer products
                    <ul>
                        <li>If you order before 10 AM, we will deliver between 12-6 PM.</li>
                        <li>If you order before 2 PM, we will deliver between 4-10 PM.</li>
                        <li>If you order after 2 PM, we will deliver on the next day between 9 AM-5 PM.</li>
                    </ul>
                    <p>
                        For Seafer Eats products<br>
                        Your products will be delivered separately from Seafer Grocer items as the sellers will directly send them to  you. Your delivery schedule for Seafer Eats items will be shown when you check out.
                    </p>
                </div> 
                <div class="delivery-img">
                    <img src="{{ asset(null) }}responsive/images/img-gojek.png">
                    <img src="{{ asset(null) }}responsive/images/img-jne.png">
					<img src="{{ asset(null) }}responsive/images/seafer-delivery.png">
                </div>
                <p>
                    Currently we are working with Gojek and JNE to help deliver the products to you.
                </p>--}}
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script src="{{ asset(null) }}responsive/bootstrap/js/jquery.bootstrap-responsive-tabs.min.js"></script>
<script src="{{ asset(null) }}responsive/js/jquery.fancybox.min.js"></script>
@endsection