@extends('responsive.layout.master')

@section('title', App\Page::createTitle('My Orders'))

@section('customcss')
    <link rel='stylesheet' href='{{ asset(null) }}responsive/bootstrap/css/bootstrap-responsive-tabs.css'>
    <link rel="stylesheet" type="text/css" href="{{ asset(null) }}responsive/css/jquery.fancybox.min.css">
@endsection

@section('content')
	<div class="main-content">
		<div class="container" id="checkout">
			<div class="row hide-320">
				<div class="col-md-12">
					<span class="text-grey equal margin-bottom-35">
					  <a class="text-grey" href="{{ url('/') }}"><b>HOME</b></a> /
					  <a class="text-grey" href="{{ url('user/my-account') }}"><b>MY ACCOUNT</b></a> /
					  <a class="text-grey" href="#"><b>MY ORDERS </b></a>
					</span>
				</div>
			
			</div>
			<div class="row">
				<div class="col-md-12">
					<h5 class="title title-margin" style="margin-bottom:0;">MY ORDERS </h5>
	                <h5 class="center" style="margin-bottom:50px;"><span class="subtitle">HISTORY</span></h5>
				</div>
			</div>
			<div class="row">
				<div class="col-md-9 col-sm-11  col-xs-11 col-centered">
	                <div class="order-history-box">
	                    <table>
	                        <tbody>
	                        <tr>
	                            <th>Date</th>
	                            <th>Order No.</th>
	                            <th>Order Status</th>
	                            <th>Payment Bank</th>
	                            <th>Shipping Status</th>
	                        </tr>
	                        @foreach ($orders as $order)
	                            <tr>
	                                <td data-content="Date">{{ $order->created_at->format('d M Y, H:i') }}</td>
	                                <td data-content="Order No."><a href={{ url('orders/'.$order->id) }}"> #{{ $order->id }}</a></td>
	                                <td data-content="Status">{{ $order->status->description }}</td>
	                                @if ($order->payment()->where('user_confirmed', 1)->count() > 0)
	                                <td data-content="Payment Bank">{{ $order->payment()->where('user_confirmed', 1)->first()->transfer_dest_account }}</td>
	                                @else
	                                <td data-content="Payment Bank">Not Available</td>
	                                @endif
	                                
	                                @if ($order->deliveries)
	                                <td data-content="Shipping Status" style="text-transform:capitalize;">{{ ucwords($order->deliveries->status)}}</td>
	                                @else
	                                <td data-content="Shipping Status" style="text-transform:capitalize;">Not Available</td>
	                                @endif
	                            </tr>
	                            @endforeach
								
	                        </tbody>
	                    </table>
	                </div>
	            </div>
			</div>
		
	</div>
@endsection

@section('scripts')
<script src="{{ asset(null) }}responsive/bootstrap/js/jquery.bootstrap-responsive-tabs.min.js"></script>
<script src="{{ asset(null) }}responsive/js/jquery.fancybox.min.js"></script>
@endsection