@extends('responsive.layout.master')

@section('title', App\Page::createTitle('Add New Address'))

@section('customcss')
    <link rel='stylesheet' href='{{ asset(null) }}responsive/bootstrap/css/bootstrap-responsive-tabs.css'>
    <link rel="stylesheet" type="text/css" href="{{ asset(null) }}responsive/css/jquery.fancybox.min.css">
@endsection

@section('content')
	<div class="main-content">
		<div class="container" id="checkout">
			<div class="row hide-320">
				<div class="col-md-12">
					<span class="text-grey equal margin-bottom-35">
					  <a class="text-grey" href="index.html"><b>MY ACCOUNT</b></a> /
					  <a class="text-grey" href="#"><b>MY ADDRESS BOOK </b></a>/
					  <a class="text-grey " href="#"><b>ADD NEW ADDRESS</b></a>
					</span>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h5 class="title title-margin">ADD NEW ADDRESS</h5>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-sm-7 col-xs-10 col-centered">
					{!! Form::model($address,['method'=>'PATCH', 'action'=>['UserController@patchUpdateAddress', $address->id]]) !!}
						<div class="form-box top-d">
							<div class="row">
								<div class="col-md-4">
									<label for="middle-label" class="text-left middle "><span class="required">ADDRESS NAME</span></label>
								</div>
								<div class="col-md-8">
									{!! Form::text('name', null, ['class'=>'form-control', 'placeholder' => 'e.g., Alamat Kantor', 'required' => 'required']) !!}
								</div>
							</div>
							<div class="row">
								<div class="col-md-4 hide-768">
									<label for="middle-label" class="text-left middle">TITLE</label>
								</div>
								<div class="col-md-8">
									{!! Form::select('recipient_title', array('m' => 'Mr.', 'fs' => 'Ms.', 'f' => 'Mrs.'), null, ['class'=>'form-control']) !!}
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<label for="middle-label" class="text-left middle"><span class="required">RECIPIENT</span></label>
								</div>
								<div class="col-md-8">
									{!! Form::text('recipient', null, ['class'=>'form-control', 'placeholder' => 'e.g., Barry Allen', 'required' => 'required']) !!}
								</div>
							</div>
							
							<div class="row">
								<hr class="blue">
							</div>
							<div class="row">&nbsp;</div>
							<div class="row">
								<div class="col-md-4">
									<label for="middle-label" class="text-left middle"><span class="required">ADDRESS LINE1</span></label>
								</div>
								<div class="col-md-8">
									{!! Form::text('address', null, ['class'=>'form-control', 'placeholder' => 'Address', 'required' => 'required']) !!}
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<label for="middle-label" class="text-left middle">ADDRESS LINE 2</label>
								</div>
								<div class="col-md-8">
									<input class="check-input" placeholder="Address" name="address_two" type="text">
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<label for="middle-label" class="text-left middle">ADDRESS LINE 3</label>
								</div>
								<div class="col-md-8">
									<input class="check-input" placeholder="Address" name="addres_three" type="text">
								</div>
							</div>
							<div class="row">
								<div class="col-md-4 hide-768">
									<label for="middle-label" class="text-left middle"><span class="required">PROVINCE</span></label>
								</div>
								<div class="col-md-8">
									{!! Form::select('province_id', $provinces, null, ['class'=>'form-control', 'id' => 'select_province']) !!}
								</div>
							</div>
							<div class="row">
								<div class="col-md-4 hide-768">
									<label for="middle-label" class="text-left middle"><span class="required">CITY</span></label>
								</div>
								<div class="col-md-8">
									{!! Form::select('city_id', $cities, null, ['class'=>'form-control', 'id' => 'select_city']) !!}
								</div>
							</div>
							<div class="row">
								<div class="col-md-4 hide-768">
									<label for="middle-label" class="text-left middle"><span class="required">AREA</span></label>
								</div>
								<div class="col-md-8">
									{!! Form::select('area_id', $areas, null, ['class'=>'form-control', 'id' => 'select_area']) !!}
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<label for="middle-label" class="text-left middle"><span class="required">POSTAL CODE</span></label>
								</div>
								<div class="col-md-8">
									{!! Form::text('postal_code', null, ['class'=>'form-control', 'placeholder' => 'Postal Code', 'required' => 'required']) !!}
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<label for="middle-label" class="text-left middle phone"><span class="required">MOBILE NUMBER</span></label>
								</div>
								<div class="col-md-8">
									<div class="input-group">
										<span class="input-group-label">+62</span>
										{{ csrf_field() }}
                                        {{--<input class="input-group-field" type="text" name="phone_number" placeholder="8129xxx">--}}
                                        {!! Form::text('phone_number', null, ['class'=>'input-group-field', 'placeholder' => 'Address', 'required' => 'required']) !!}
									</div>
								</div>
							</div>
						<div class="row">
							<div class="col-md-4">
								<label for="middle-label" class="text-left middle hide-768" style="color:white">MAKE THIS MY</label>
							</div>
							<div class="col-md-8">
								<div class="col-md-6 col-sm-6 col-xs-6">
									<button data-open="modal-submit" class="btn btn-action blue space-3 savebutton" type="submit" aria-controls="modal-submit" aria-haspopup="true" tabindex="0">SAVE</button>
								</div>
								<div class="col-md-6 col-sm-6 col-xs-6">
									<a href="{{ url('user/my-address') }}" class="btn btn-action red space-3"><span style="color:white">CANCEL</span></a>
								</div>
							</div>
						</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
	<script src="{{ asset(null) }}responsive/bootstrap/js/jquery.bootstrap-responsive-tabs.min.js"></script>
	<script src="{{ asset(null) }}responsive/js/jquery.fancybox.min.js"></script>
	<script>
        $('#select_province').on('change', function(){
            var province_id = $(this).val();
            $.ajax({
                type: 'get',
                url: '{{ url('user/ajax-cities-by-province') }}/' + province_id
            }).done(function(result){
                result = jQuery.parseJSON(result);
                if(result.status == 'success') {
                    $('#select_city').html('');
                    $.each(result.cities, function(k, v) {
                        $('#select_city').append('<option value="'+k+'">'+v+'</option>');
                    });

                } else {
                    alert(result.message);
                }
            })
        });
        $('#select_city').on('change', function(){
            var city_id = $(this).val();
            $.ajax({
                type: 'get',
                url: '{{ url('user/ajax-areas-by-city/') }}/' + city_id
            }).done(function(result){
                result = jQuery.parseJSON(result);
                if(result.status == 'success') {
                    $('#select_area').html('');
                    $.each(result.areas, function(k, v) {
                        $('#select_area').append('<option value="'+k+'">'+v+'</option>');
                    });

                } else {
                    alert(result.message);
                }
            })

        });
        $('#form-submit-button').click(function(){
            if($('#select_area').val() == 0 || $('#select_area').val() == '0'){
                e.preventDefault();
                alert('Please select area');
                return false;
            }
            if($('#select_city').val() == 0 || $('#select_city').val() == '0'){
                e.preventDefault();
                alert('Please select city');
                return false;
            }
            if($('#select_province').val() == 0 || $('#select_province').val() == '0'){
                e.preventDefault();
                alert('Please select area');
                return false;
            }
        });
        
        /*$('.reveal').hide();
        var address_name = $(':input[name="name"]').val();
        var fname = $(':input[name="first_name"]').val();
        var lname = $(':input[name="last_name"]').val();
        var addr = $(':input[name="address_one"]').val();
        var mob = $(':input[name="phone"]').val();

        $('.submitbutton').click(function(e){
            if(address_name == '' || fname=='' || lname=='' || addr=='' || mob=='') {
                console.log('all ampty');
                this.removeAttribute('data-open');
                e.preventDefault();
            }
            e.preventDefault();
        });*/


        // url with custom callbacks
        /*$('#modal-submit').foundation('reveal', 'open', {
         url: 'http://local.seafermart/user/add-new-address',
         success: function(data) {
         alert('modal data loaded');
         },
         error: function() {
         alert('failed loading modal');
         }
         });*/

        /*$('form').submit(function(){
            $('#modal-submit').foundation('reveal', 'open', {
                url: 'http://local.seafermart/user/add-new-address',
                success: function(data) {
                    alert('modal data loaded');
                },
                error: function() {
                    alert('failed loading modal');
                }
            });
        });*/

    </script>
@endsection