@extends('responsive.layout.master')

@section('title', App\Page::createTitle('My Cashback Rewards'))

@section('customcss')
    <link rel='stylesheet' href='{{ asset(null) }}responsive/bootstrap/css/bootstrap-responsive-tabs.css'>
    <link rel="stylesheet" type="text/css" href="{{ asset(null) }}responsive/css/jquery.fancybox.min.css">
@endsection

@section('content')
	<div class="main-content">
		<div class="container">
			<div class="row hide-320">
				<div class="col-md-12">
					<span class="text-grey equal margin-bottom-35">
					  <a class="text-grey" href="{{ url('/') }}"><b>HOME</b></a> /
					  <a class="text-grey" href="{{ url('user/my-account') }}"><b>MY ACCOUNT </b></a> /
					  <a class="text-grey" href="#"><b>MY CASHBACK REWARDS </b></a>
					</span>
				</div>
			
			</div>
			 <div class="row">
	            <div class="col-md-12">
	                <h5 class="title title-margin">MY CASHBACK BALANCE</h5>
	                <div class="col-md-3 col-md-offset-2 center">
	                    <div class="img-box">
	                        <img class="img-rewads" src="{{ asset(null) }}responsive/images/loyaltypoint.png">
	                    </div>
	                </div>
	                <div class="col-md-3 center" style="margin-top: 2rem;">
	                    <p>Your Current Rewards:</p>
	                    @if(auth()->user()->points()->get()->count() > 0)
	                    <h1 class="big-blue">{{ Money::display(auth()->user()->points()->first()->nominal) }}</h1>
	                    @else
	                    <h1 class="big-blue">{{ Money::display(0) }}</h1>
	                    @endif
	                    
	                </div>
	            </div>
	        </div>
			<div class="row table-rewards">
				<div class="col-md-9 col-sm-11  col-xs-11 col-centered">
					<b class="text-green">REWARDS POINTS</b><b class="text-orange"> HISTORY</b>
	                <table class="rewards">
	                    <thead class="border-green">
	                    <tr>
	                        <th width="200"><span style="color:#808184;">Date</span></th>
	                        <th width="200"><span style="color:#808184;">Rewards Nominal</span></th>
	                        <th width="200"><span style="color:#808184;">Activities</span></th>
	                    </tr>
	                    </thead>
	                    <tbody>
	                    @foreach($cashback_rewards as $cashback)
						<tr>
	                        <td data-content="Date">{{ LocalizedCarbon::parse($cashback->created_at)->format('d F Y, H.i') }}</td>
	                        @if($cashback->obtain_from != 'redeem')
	                        <td data-content="Rewards Nominal">{{ number_format($cashback->nominal, 0, '.', ',') }}</td>
	                        @else
	                        <td style="color:red"> - {{ number_format($cashback->nominal, 0, '.', ',') }}</td>
	                        @endif
	                        @if($cashback->obtain_from == 'order')
	                            <td data-content="Activities">Order number <a href="{{ url('orders/'.$cashback->order_id) }}" target="_blank">{{ $cashback->order_id }}</a></td>
	                        @elseif($cashback->obtain_from == 'redeem')
	                            <td data-content="Activities">Redeemed on Order number <a href="{{ url('orders/'.$cashback->order_id) }}" target="_blank">{{ $cashback->order_id }}</a></td>
	                        @else
	                            <td data-content="Activities">Referral from <i>{{ $cashback->order->user()->first()->name }}</i></td>
	                        @endif
	                    </tr>
	                    @endforeach
	                    <tr>
	                        <td data-content="Date">31 Oct 16, 17.00</td>
	                        <td data-content="Rewards Nominal">500</td>
	                        <td data-content="Activities">Order number <a href="" target="_blank">234545</a></td>
	                    </tr>
	                    <tr>
	                        <td data-content="Date">31 Oct 16, 17.00</td>
	                        <td data-content="Rewards Nominal" class="text-red">500</td>
	                        <td data-content="Activities">Redeemed on Order number <a href="" target="_blank">89775</a</td>
	                    </tr>
	                    <tr>
	                        <td data-content="Date">31 Oct 16, 17.00</td>
	                        <td data-content="Rewards Nominal">500</td>
	                        <td data-content="Activities">Order number <a href="" target="_blank">234545</a></td>
	                    </tr>
	                    <tr>
	                        <td data-content="Date">31 Oct 16, 17.00</td>
	                        <td data-content="Rewards Nominal">500</td>
	                        <td data-content="Activities">Order number <a href="" target="_blank">234545</a></td>
	                    </tr>
						 </tbody>
	                </table>
	            </div>
				</div>
				<div class="row" style="margin-top: 4rem;margin-bottom: 4rem;" >
	            <div class="col-md-9 col-sm-11  col-xs-11 col-centered">
	                <div class="col-md-6 col-sm-6 center no-padding-h">
	                    <div class="sub-content" id="rules-rewards">
	                        <h6>RULES</h6>
	                        <ul>
	                            <li>
	                                Get your 0.5% cashback promo everytime you make a purchase of Rp100.000.
	                            </li>
								<li>
	                                Share your referral codes and get 0.5% cashback promo everytime thr referred account make a purchase of Rp100.000.
	                            </li>
	                        </ul>
	                    </div>
	                </div>
	                <div class="col-md-6 col-sm-6 center">
	                    <a href="#" id="referralcode" data-open="modal-referralcode"><button class="btn btn-primary btn-aneh">SHARE REFERRAL CODE</button></a><br>
	                </div>
	            </div>
	        </div>
		</div>
	</div>
@endsection

@section('scripts')
	<script src="{{ asset(null) }}responsive/bootstrap/js/jquery.bootstrap-responsive-tabs.min.js"></script>
	<script src="{{ asset(null) }}responsive/js/jquery.fancybox.min.js"></script>
	<script type="text/javascript">
        document.getElementById('shareBtn').onclick = function() {
            FB.ui({
                method: 'share',
                display: 'popup',
                quote: 'Click this link "{{ url('user/sign-up?refer_from='.auth()->user()->referral_code) }}" to get benefit when you buy good things from seafermart.co.id',
                href: '{{ url('user/sign-up?refer_from='.auth()->user()->referral_code) }}',
            }, function(response){});
        }
        $(document).ready(function(){
            $('#share-referral-form').submit(function(){
                $.ajax({
                    url : $(this).attr('action'),
                    type : 'POST',
                    dataType : 'json',
                    data : $(this).serialize(),
                    success : function(response){
                        if(response.status == 'Ok'){
                            alert('You have successfully referred your friend to Seafermart. Now you can enjoy the cashback every time they shop.');
                        } else {
                            alert(response.status);
                        }
                    }
                })
                return false;
            })
        })
    </script>
@endsection