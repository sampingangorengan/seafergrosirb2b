@extends('responsive.layout.master')

@section('title', App\Page::createTitle('My Profile'))

@section('customcss')
    <link rel='stylesheet' href='{{ asset(null) }}responsive/bootstrap/css/bootstrap-responsive-tabs.css'>
    <link rel="stylesheet" type="text/css" href="{{ asset(null) }}responsive/css/jquery.fancybox.min.css">
@endsection

@section('content')
	<div class="main-content">
		<div class="container" id="signUp">
			<div class="row">
				<div class="col-md-12">

					<h5 class="title title-margin">MY PROFILE </h5>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-sm-7 col-xs-10 col-centered">
					<div class="form-box">
						<div class="row">
							{!! Form::model($user,['method'=>'POST', 'action'=>['UserController@postEditGeneralData', $user->id], 'files' => true]) !!}
							<div class="col-md-4 hide-768">
								<label for="middle-label" class="text-left middle">PROFILE PICTURE</label>
							</div>
							<div class="col-md-8">

								@if($user->profile_picture)
                                    <div class="preview img-wrapper col-centered-768" style="background-image:url({!! asset($user->photo->file) !!})">
                                @else
                                    <div class="preview img-wrapper col-centered-768"    ></div>
                                @endif

									<div class="file-upload-wrapper">
										<input name="file" class="file-upload-native" accept="image/*" type="file">
										<input disabled class="file-upload-text btn-upload" type="text">
									</div>
							
								</div>
								
							</div>
							<div class="row">
								<div class="col-md-4">
									<label for="middle-label" class="text-left middle hide-768">TITLE</label>
								</div>
								<div class="col-md-8">
									{!! Form::select('sex', array('m' => 'Mr.', 'fs' => 'Ms.', 'f' => 'Mrs.'), null, ['class'=>'short']) !!}
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<label for="middle-label" class="text-left middle"><span class="required">FIRST NAME</span></label>
								</div>
								<div class="col-md-8">
									{!! Form::text('name', null, ['class'=>'form-control']) !!}
								</div>
							</div>
														<div class="row">
								<div class="col-md-4">
									<span for="middle-label" class="text-left middle" style="font-weight:bold;">DATE OF BIRTH</span>
								</div>
								<div class="col-md-6 end">
									<div class="row">
										<div class="col-md-12">
											<select name="dob_date" style="width:23%" required="">
												@for ($date = 1; $date <= 31; $date++)
                                                    <option value="{{ $date }}" {{ date('d', strtotime($user->dob)) == $date ? 'selected' : ''}}>{{ $date }}</option>
                                                @endfor
											</select>

											<select name="dob_month" style="width:50%" required="">
												
												<option value="1" {{ date('m', strtotime($user->dob)) == '01' ? 'selected' : ''}}>January</option>
                                                <option value="2" {{ date('m', strtotime($user->dob)) == '02' ? 'selected' : ''}}>February</option>
                                                <option value="3" {{ date('m', strtotime($user->dob)) == '03' ? 'selected' : ''}}>March</option>
                                                <option value="4" {{ date('m', strtotime($user->dob)) == '04' ? 'selected' : ''}}>April</option>
                                                <option value="5" {{ date('m', strtotime($user->dob)) == '05' ? 'selected' : ''}}>May</option>
                                                <option value="6" {{ date('m', strtotime($user->dob)) == '06' ? 'selected' : ''}}>June</option>
                                                <option value="7" {{ date('m', strtotime($user->dob)) == '07' ? 'selected' : ''}}>July</option>
                                                <option value="8" {{ date('m', strtotime($user->dob)) == '08' ? 'selected' : ''}}>August</option>
                                                <option value="9" {{ date('m', strtotime($user->dob)) == '09' ? 'selected' : ''}}>September</option>
                                                <option value="10" {{ date('m', strtotime($user->dob)) == '10' ? 'selected' : ''}}>October</option>
                                                <option value="11" {{ date('m', strtotime($user->dob)) == '11' ? 'selected' : ''}}>November</option>
                                                <option value="12" {{ date('m', strtotime($user->dob)) == '12' ? 'selected' : ''}}>December</option>
											</select>
											<select name="dob_year" style="width:25%" required="">
																								
													@for ($year = (date('Y') + 5); $year >= (date('Y') - 90); $year--)

                                                    <option value="{{ $year }}" {{ date('Y', strtotime($user->dob)) == $year ? 'selected' : ''}}>{{ $year }}</option>
                                                @endfor
												</select>
										</div>

									</div>

								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<label for="middle-label" class="text-left middle phone">MOBILE NUMBER</label>
								</div>
								<div class="col-md-8">
									<div class="input-group">
										<span class="input-group-label">+62</span>
										{!! Form::text('phone_number', null, ['class'=>'input-group-field check-input phone_number']) !!}
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12 right">
									<button class="btn btn-save btn-long">SAVE</button>
								</div>
							</div>
							{!! Form::close() !!}
							<hr class="blue">
							{!! Form::model($user,['method'=>'POST', 'action'=>['UserController@postEditEmailData', $user->id]]) !!}
							<div class="row">
								<div class="col-md-4">
									<label for="middle-label" class="text-left middle">EMAIL</label>
								</div>
								<div class="col-md-8">
									{!! Form::text('old_email', $user->email, ['class'=>'form-control']) !!}
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<label for="middle-label" class="text-left middle">NEW EMAIL</label>
								</div>
								<div class="col-md-8">
									<input placeholder="" class="check-input" name="email" type="text">
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<label for="middle-label" class="text-left middle">CONFIRM EMAIL</label>
								</div>
								<div class="col-md-8">
									<input placeholder="" class="check-input" name="email_confirmation" type="text">
								</div>
							</div>
							<div class="row">
								<div class="col-md-12 right">
									<button class="btn btn-save btn-long">SAVE</button>
								</div>
							</div>
							{!! Form::close() !!}
							<hr class="blue">
							{!! Form::model($user,['method'=>'POST', 'action'=>['UserController@postEditPasswordData', $user->id]]) !!}
							<div class="row">
								<div class="col-md-4">
									<label for="middle-label" class="text-left middle">CURRENT PASSWORD</label>
								</div>
								<div class="col-md-8">
									<input placeholder="" class="check-input"  name="old_password" type="password">
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<label for="middle-label" class="text-left middle">PASSWORD</label>
								</div>
								<div class="col-md-8">
									<input placeholder="tye your new password" class="check-input" name="password" type="password">
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<label for="middle-label" class="text-left middle">CONFIRM PASSWORD</label>
								</div>
								<div class="col-md-8">
									<input placeholder="retype your new password" class="check-input" name="password_confirmation" type="password">
								</div>
							</div>
							<div class="row">
								<div class="col-md-12 right">
									<button class="btn btn-save btn-long">SAVE</button>
								</div>
							</div>
							{!! Form::close() !!}
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
@endsection

@section('scripts')
<script src="{{ asset(null) }}responsive/bootstrap/js/jquery.bootstrap-responsive-tabs.min.js"></script>
<script src="{{ asset(null) }}responsive/js/jquery.fancybox.min.js"></script>
@endsection