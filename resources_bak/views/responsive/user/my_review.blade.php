@extends('responsive.layout.master')

@section('title', App\Page::createTitle('My Reviews'))

@section('customcss')
    <link rel='stylesheet' href='{{ asset(null) }}responsive/bootstrap/css/bootstrap-responsive-tabs.css'>
    <link rel="stylesheet" type="text/css" href="{{ asset(null) }}responsive/css/jquery.fancybox.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.2.0/jquery.rateyo.min.css">
@endsection

@section('content')
	<div class="main-content">
		<div class="container" id="myRatingsReviews">
			<div class="row hide-320">
				<div class="col-md-12">
					<span class="text-grey equal margin-bottom-35">
					  <a class="text-grey" href="index.html"><b>HOME</b></a> /
					  <a class="text-grey" href="#"><b>MY ACCOUNT </b></a> /
					  <a class="text-grey" href="#"><b>>MY RATINGS &amp; REVIEWS </b></a>
					</span>
				</div>
			
			</div>
			 <div class="row">
	            <div class="col-md-12 col-sm-10 col-xs-10 col-centered-768">
	                <h4 class="text-blue">MY RATINGS & REVIEWS</h4>

	            </div>
	        </div>
			<div class="row">
	            <div class="col-md-9 col-sm-9 col-centered">
	                    <div class="customer-and-reviews-box">
	                        <ul class="nav nav-tabs" role="tablist">
	                            <li class="tabs-title active"><a href="#panelGrocer" data-toggle="tab" aria-expanded="true">GROCER</a></li>
	                            {{-- <li class="tabs-title"><a href="#panelCook" data-toggle="tab" >COOK</a></li>
	                            <li class="tabs-title"><a href="#panelEats" data-toggle="tab" >EATS</a></li> --}}
	                        </ul>
	                        <div class="tab-content top-line-blue">
	                            <div class="tab-pane fade active in" id="panelGrocer">
	                            	@foreach($grocer_list as $item)
	                                <div class="panel-content-box">
	                                    <div class="row">
	                                        <div class="col-md-4 col-sm-4 col-xs-4 no-padding-h">
	                                            <div class="img-box">
	                                                <img src="{{ $item->groceries->getFirstImageUrl($item->groceries_id) }}">
	                                            </div>
	                                        </div>
	                                        <div class="col-md-8 col-sm-8 col-xs-8 no-padding-h control-rate">
												<div class="col-md-6 col-sm-6">
													{{ $item->groceries->name }}<br> <span>by {{ $item->groceries->brand['name'] }}</span>
												</div>
												<div class="col-md-6 col-sm-6">
													<div class="rating-box" style="margin-right:20px;">
														Ratings

														@if($item->is_reviewed == 1)
                                                        @if(null != $item->getRateByUser(auth()->user()->id, $item->groceries_id))
                                                        <input type="hidden" name="fixval" value="{{ $item->getRateByUser(auth()->user()->id, $item->groceries_id)->rate }}">
                                                        @endif
                                                        <div class="rateYostatic" style="padding:0;"></div>
                                                        
                                                        @else
                                                        <div class="rateYo" style="padding:0;"></div>
                                                        
                                                        @endif
													</div>
												</div>
												<div class="col-md-12 col-sm-12 review-320">

													@if($item->is_reviewed == 0)
													<input type="hidden" name="rate" value="0">
                                                    <input type="hidden" name="gr_id" value="{{ $item->groceries->id }}">
                                                    <input type="hidden" name="ogid" value="{{ $item->id }}">
													<div class="form-box center">
														<p class="shr">Share your thoughts with other customers</p>
														<button type="button" class="addReview btn btn-aneh">ADD REVIEW</button>
													</div>

													@elseif(false != $item->getRateByUser(auth()->user()->id, $item->groceries_id))

													<div class="form-box">
														<div class="col-md-2 col-sm-2 hide-320">
	                                                        <input type="hidden" name="rate" value="0">
	                                                        <input type="hidden" name="gr_id" value="{{ $item->groceries->id }}">
															<img src="{{ auth()->user()->photo->file }}" >
	                                                    </div>
                                                        @if (false != $item->getRateByUser(auth()->user()->id, $item->groceries_id))
	                                                    <div class="col-md-10 col-sm-10">
	                                                        <b class="text-blue">Your Comment</b><br>
	                                                        <i>{{ LocalizedCarbon::parse($item->getRateByUser(auth()->user()->id, $item->groceries_id)->created_at)->format('d F Y, H:s') }}</i><br>
	                                                        <b>{{ $item->getRateByUser(auth()->user()->id, $item->groceries_id)->title }}</b>
	                                                        <p>{{ $item->getRateByUser(auth()->user()->id, $item->groceries_id)->review }}</p>
	                                                    </div>
                                                        @endif
													</div>
													@endif
												</div>
	                                        </div>
	                                    </div>
	                                </div>
	                                @endforeach
									
	                            </div>
	                        </div>
	                    </div>
	                </div>

			</div>
		</div>
		
	</div>
@endsection

@section('scripts')
<script src="{{ asset(null) }}responsive/bootstrap/js/jquery.bootstrap-responsive-tabs.min.js"></script>
<script src="{{ asset(null) }}responsive/js/jquery.fancybox.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.2.0/jquery.rateyo.min.js"></script>
    <script>
        $(document).ready(function(){
            $(".rateYo").rateYo({
                maxValue: 5,
                numStars: 5,
                starWidth: "20px",
                rating:$(this).attr('data-value'),
                fullStar: true,
                multiColor: {
                    "startColor": "#f9e102", //RED
                    "endColor"  : "#f9e102"  //GREEN
                },
                onInit: function (rating, rateYoInstance) {

                    console.log("Ready to rate?");
                },
                onSet: function (rating, rateYoInstance) {
                    var rate_container = $(this).parent().parent().parent().parent().parent();
                    rate_container.find('input[name="rate"]').val(rating);
                }
            });


            $(".rateYostatic").rateYo({
                maxValue: 5,
                numStars: 5,
                starWidth: "20px",
                //rating: 3,//$(this).attr('data-value'),
                fullStar: true,
                readOnly: true,
                multiColor: {
                    "startColor": "#f9e102", //RED
                    "endColor"  : "#f9e102"  //GREEN
                }
            });
            $(".rateYostatic").each(function( index ) {
                var val = $(this).parent().find('input[name="fixval"]').val();
                $(this).rateYo("option", "rating", val);
            });


            $('.panel-content-box').on('click', '.doSubmit', function(){
                var container = $(this).parent().parent().parent();
                var ogid = container.find('input[name="ogid"]').val();
                var grid = container.find('input[name="gr_id"]').val();
                var rate = container.find('input[name="rate"]').val();
                var title = container.find('input[name="title"]').val();
                var message = container.find('textarea[name="body"]').val();
                // if(title && message){
                    $.ajax({
                        type: 'post',
                        url: "{{ url('reviews') }}",
                        data: {
                            grid: grid,
                            ogid:ogid,
                            rate: rate,
                            title: title,
                            message: message
                        }
                    }).done(function(result){
                        if(result == 'saved'){
                            alert('Your review has been saved');
                            location.reload();
                        } else {
                            alert('Something wrong while saving your review.')
                        }

                    })
                // } else {
                //     alert('Headline and Review Message required');
                // }
            });
        });
    </script>
@endsection