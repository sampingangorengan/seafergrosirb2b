@extends('responsive.layout.master')

@section('title', App\Page::createTitle('Add Bank Account'))

@section('customcss')
    <link rel='stylesheet' href='{{ asset(null) }}responsive/bootstrap/css/bootstrap-responsive-tabs.css'>
    <link rel="stylesheet" type="text/css" href="{{ asset(null) }}responsive/css/jquery.fancybox.min.css">
@endsection

@section('content')
	<div class="main-content">
		<div class="container">
			<div class="row hide-320">
				<div class="col-md-12">
					<span class="text-grey equal margin-bottom-35">
					  <a class="text-grey" href="index.html"><b>HOME</b></a> /
					  <a class="text-grey" href="#"><b>MY ACCOUNT </b></a> /
					  <a class="text-grey" href="#"><b>MY BANK ACCOUNT </b></a> /
					  <a class="text-grey" href="#"><b>ADD BANK ACCOUNT </b></a>
					</span>
				</div>
			
			</div>
			<div class="row">
	            <div class="col-md-12">
	                <h5 class="title title-margin">MY BANK ACCOUNT</h5>

	            </div>
	        </div>
			<div class="row">
				<div class="col-md-9 col-sm-7  col-xs-11 col-centered">
					<form method="post">
						<div class="form-box label-placeholder">
							<div class="row">
								<div class="col-md-5">
									<label for="middle-label" class="text-left">ACCOUNT NAME<span class="text-red">*</span></label>
								</div>
								<div class="col-md-7">
									<input type="text" class="check-input" placeholder="" name="account_name" required>
								</div>
							</div>
							<div class="row">
								<div class="col-md-5">
									<label for="middle-label" class="text-left">ACCOUNT NUMBER<span class="text-red">*</span></label>
								</div>
								<div class="col-md-7">
									<input type="text"  class="check-input" placeholder="" name="account_number" required>
								</div>
							</div>
							<div class="row">
								<div class="col-md-5 hide-768">
									<label for="middle-label" class="text-left">BANK NAME</label>
								</div>
								<div class="col-md-7">
									<select name="bank_name" required>
										<option value="BCA">BCA</option>
										<option value="MANDIRI">MANDIRI</option>
										<option value="BNI">BNI</option>
									</select>
								</div>
							</div>
							<div class="row">
								<div class="col-md-5">
									<label for="middle-label" class="text-left">BRANCH</label>
								</div>
								<div class="col-md-7">
									<input type="text"  class="check-input" placeholder="" name="branch">
								</div>
							</div>
							<div class="row">
								<div class="col-md-5">
									<label for="middle-label" class="text-left">ACCOUNT PASSWORD<span class="text-red">*</span></label>
								</div>
								<div class="col-md-7">
									<input type="text"  class="check-input" placeholder="" name="user_password" required>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12 right">
									<button data-open="modal-submit" class="btn btn-aneh btn-long">SAVE</button>
								</div>
							</div>
						</div>
	                </form>
	            </div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
	<script src="{{ asset(null) }}responsive/bootstrap/js/jquery.bootstrap-responsive-tabs.min.js"></script>
	<script src="{{ asset(null) }}responsive/js/jquery.fancybox.min.js"></script>
@endsection