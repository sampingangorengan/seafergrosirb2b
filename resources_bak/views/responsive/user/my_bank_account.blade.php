@extends('responsive.layout.master')

@section('title', App\Page::createTitle('My Bank Account'))

@section('customcss')
    <link rel='stylesheet' href='{{ asset(null) }}responsive/bootstrap/css/bootstrap-responsive-tabs.css'>
    <link rel="stylesheet" type="text/css" href="{{ asset(null) }}responsive/css/jquery.fancybox.min.css">
@endsection

@section('content')
	<div class="main-content">
		<div class="container">
			<div class="row hide-320">
				<div class="col-md-12">
					<span class="text-grey equal margin-bottom-35">
					  <a class="text-grey" href="{{ url('/') }}"><b>HOME</b></a> /
					  <a class="text-grey" href="{{ url('user/my-account') }}"><b>MY ACCOUNT </b></a> /
					  <a class="text-grey" href="#"><b>MY BANK ACCOUNT </b></a>
					</span>
				</div>
			
			</div>
			 <div class="row">
	            <div class="col-md-12">
	                <h5 class="title title-margin">MY BANK ACCOUNT</h5>

	            </div>
	        </div>
			<div class="row">
	            <div class="col-md-3 center hide-768">
	                <img class="left" src="{{ asset(null) }}responsive/images/bank-account-left.png">
	            </div>
	            <div class="col-md-6 col-sm-7 col-xs-10 col-centered-768">
	                <div class="col-md-6 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-2 left">
	                    @if($bank_list->count() > 0)
	                    @foreach($bank_list as $bank)
	                    <p class="semi">
	                        {{ $bank->name }} {{ $bank->branch }}
	                        <br>a/n {{ $bank->account_name }}
	                        <br>{{ $bank->account_number }}
	                        <br>
	                        
	                        @if($bank->is_preference == 0)
	                        <a  class="grey-underline" href="{{ url('user/set-bank-account-preference/'.$bank->id) }}">Set as default</a>
	                        @else
	                        <a  class="grey-underline" href="#">Default account</a>
	                        @endif 
	                        <a class="grey-underline"  href="{{ url('user/delete-bank-account/'.$bank->id) }}">delete</a>
	                    </p>
	                    @endforeach
	                    @else
	                    <p class="semi">
	                        You have not add any bank to your preference
	                    </p>
	                    @endif
	                </div>
					<div class="col-md-12 center">
						<a href="{{ url('user/add-bank-account') }}" class="btn btn-aneh">ADD BANK ACCOUNT</a>
					</div>
	            </div>
	            <div class="col-md-3 center hide-768">
	                <img class="left" src="{{ asset(null) }}responsive/images/bank-account-right.png">
	            </div>
			</div>
		</div>
		
	</div>
@endsection

@section('scripts')
<script src="{{ asset(null) }}responsive/bootstrap/js/jquery.bootstrap-responsive-tabs.min.js"></script>
<script src="{{ asset(null) }}responsive/js/jquery.fancybox.min.js"></script>
@endsection