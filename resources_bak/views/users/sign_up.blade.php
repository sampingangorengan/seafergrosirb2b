@extends('layouts.newmaster')

@section('title', App\Page::createTitle('Sign Up'))

@section('content')

<div id="godOfContent">
    <div class="box-content" id="signUp">
        <div class="row">
            <div class="large-12 columns">
                <h6 class="title">SIGN UP FORM</h6>
            </div>
        </div>
        <div class="row">
            <div class="large-3 columns no-padding center">
                <img class="pink" src="{{ asset(null) }}assets/images/img-swim.png">
            </div>

            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            <form method="post">

                <div class="large-6 columns no-padding">
                    <div class="form-box">
                        <div class="row">
                            <div class="small-4 columns">
                                <label for="middle-label" class="text-left middle">TITLE<span class="text-red" style="font-size:10px">*</span></label>
                            </div>
                            <div class="small-8 columns">
                                <select class="short" name="sex">
                                    <option value="m">Mr.</option>
                                    <option value="fs">Ms.</option>
                                    <option value="f">Mrs.</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="small-4 columns">
                                <label for="middle-label" class="text-left middle">FIRST NAME<span class="text-red" style="font-size:10px">*</span></label>
                            </div>
                            <div class="small-8 columns">
                                <input type="text" name="first_name" placeholder="" value="{{ old('first_name') }}" required/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="small-4 columns">
                                <label for="middle-label" class="text-left middle">LAST NAME<span class="text-red" style="font-size:10px">*</span></label>
                            </div>
                            <div class="small-8 columns">
                                <input type="text" name="last_name" placeholder="" value="{{ old('last_name') }}" required/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="small-4 columns">
                            <label for="middle-label" class="text-left middle">DATE OF BIRTH<span class="text-red" style="font-size:10px">*</span></label>
                        </div>
                        <div class="small-6 columns end">
                            <div class="row">
                                <div class="small-12 columns">
                                    <select name="dob_date" style="width:60px" required>
                                        {{--<option>Date</option>--}}
                                        @for ($date = 1; $date <= 31; $date++)
                                            <option value="{{ $date }}">{{ $date }}</option>
                                        @endfor
                                    </select>
                                    <select name="dob_month" style="width:122px" required>
                                        {{--<option>Month</option>--}}
                                        <option value="1">January</option>
                                        <option value="2">February</option>
                                        <option value="3">March</option>
                                        <option value="4">April</option>
                                        <option value="5">May</option>
                                        <option value="6">June</option>
                                        <option value="7">July</option>
                                        <option value="8">August</option>
                                        <option value="9">September</option>
                                        <option value="10">October</option>
                                        <option value="11">November</option>
                                        <option value="12">December</option>
                                    </select>
                                    <select name="dob_year" style="width:80px" required>
                                        {{--<option>Year</option>--}}
                                        @for ($year = (date('Y') - 15); $year >= (date('Y') - 90); $year--)
                                            <option value="{{ $year }}">{{ $year }}</option>
                                        @endfor
                                    </select>
                                </div>

                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <div class="small-4 columns">
                            <label for="middle-label" class="text-left middle">MOBILE NUMBER<span class="text-red" style="font-size:10px">*</span></label>
                        </div>
                        <div class="small-8 columns">
                            <div class="input-group">
                                <span class="input-group-label">+62</span>
                                <input class="input-group-field" type="text" name="phone_number" value="{{ old('phone_number') }}" required/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="small-4 columns">
                            <label for="middle-label" class="text-left middle">EMAIL<span class="text-red" style="font-size:10px">*</span></label>
                        </div>
                        <div class="small-8 columns">
                            <input type="email" name="email" placeholder="" value="{{ old('email') }}" required style="border: 1px solid #808184;height: 24px;"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="small-4 columns">
                            <label for="middle-label" class="text-left middle">PASSWORD<span class="text-red" style="font-size:10px">*</span></label>
                        </div>
                        <div class="small-8 columns">
                            <input type="password" name="signup_password" placeholder="" required/>
                            <p>Note: password must contain letters and numbers, minimum 8 characters</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="small-4 columns">
                            <label for="middle-label" class="text-left middle">CONFIRM PASSWORD<span class="text-red" style="font-size:10px">*</span></label>
                        </div>
                        <div class="small-8 columns">
                            <input type="password" name="signup_password_confirmation" placeholder="" required/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="small-4 columns">

                        </div>
                        <div class="small-8 columns">
                            <label for="middle-label" class="text-left middle"><span class="text-red" style="font-size:10px">*</span>) all fields with red dot are mandatory</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="large-9 large-offset-4 columns ">
                            <div class="box">
                                <input id="checkbox1" class="checkbox" type="checkbox" required>
                                <label for="checkbox1" class="agree">By submitting this form, I confirm that I have read and agree to SeaferMart's Term & Conditions and Privacy Policy
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="small-4 small-centered columns">
                            {{ csrf_field() }}
                            <button type="submit" class="button primary btn-aneh">SUBMIT</button>
                        </div>
                    </div>

                </div>
                </div>

            </form>

            <div class="large-3 columns no-padding center">
                <img class="blue" src="{{ asset(null) }}assets/images/img-swim2.png">
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script>
    var requiredCheckboxes = $(':checkbox[required]');

    requiredCheckboxes.change(function(){
        if(requiredCheckboxes.is(':checked')) {
            requiredCheckboxes.removeAttr('required');
        }
        else {
            requiredCheckboxes.attr('required', 'required');
        }
    });
</script>
@endsection