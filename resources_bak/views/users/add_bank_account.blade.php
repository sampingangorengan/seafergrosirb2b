@extends('layouts.newmaster')

@section('title', App\Page::createTitle('Add Bank Account'))

@section('customcss')
    <style>
        .btn-aneh:hover {
            background-color:#0368ff;
            
        }
    </style>
@endsection

@section('content')

    <div class="row" style="margin-bottom:35px">
        <div class="small-12 columns">
            <span class="text-grey equal margin-top-10">
                <a class="text-grey" href="{{ url('/') }}"><b>HOME</b></a> /
                <a class="text-grey" href="{{ url('user/my-account') }}"><b>MY ACCOUNT </b></a> /
                <a class="text-grey" href="{{ url('user/my-bank-account') }}"><b>MY BANK ACCOUNT</b></a> /
                <a class="text-grey" href="#"><b>ADD BANK ACCOUNT</b></a>
            </span>
        </div>
    </div>
    <section>
        <div class="row">
            <div class="small-12 columns">
                <h5 class="title title-margin">ADD BANK ACCOUNT</h5>
            </div>
            <div class="medium-6 medium-offset-3">
                <form method="post">
                <div class="form-box">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="medium-5 columns">
                            <label for="middle-label" class="text-left">ACCOUNT NAME<span class="red">*</span></label>
                        </div>
                        <div class="medium-7 columns">
                            <input type="text"  placeholder="" name="account_name" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="medium-5 columns">
                            <label for="middle-label" class="text-left">ACCOUNT NUMBER<span class="red">*</span></label>
                        </div>
                        <div class="medium-7 columns">
                            <input type="text"  placeholder="" name="account_number" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="medium-5 columns">
                            <label for="middle-label" class="text-left">BANK NAME</label>
                        </div>
                        <div class="medium-7 columns">
                            <select class="short" name="bank_name" required>
                                <option value="BCA">BCA</option>
                                <option value="MANDIRI">MANDIRI</option>
                                <option value="BNI">BNI</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="medium-5 columns">
                            <label for="middle-label" class="text-left">BRANCH</label>
                        </div>
                        <div class="medium-7 columns">
                            <input type="text"  placeholder="" name="branch">
                        </div>
                    </div>
                    <div class="row">
                        <div class="medium-5 columns">
                            <label for="middle-label" class="text-left">ACCOUNT PASSWORD<span class="red">*</span></label>
                        </div>
                        <div class="medium-7 columns">
                            <input type="password"  placeholder="" name="user_password" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="medium-offset-9 columns">
                            <button style="margin-left: 30px;" data-open="modal-submit" class="button btn-action blue space-3">SAVE</button>
                        </div>
                    </div>
                </div>
                </form>
            </div>

        </div>
    </section>

@endsection