@extends('layouts.newmaster')

@section('title', App\Page::createTitle('My Profile'))

@section('content')
    <!-- =================== -->
    <!-- CONTENT -->
    <!-- =================== -->

    <div class="row" style="margin-bottom: 35px;">
        <div class="small-12 columns">
            <span class="text-grey equal margin-top-10">
              <a class="text-grey" href="{{ url('/') }}"><b>HOME</b></a> /
              <a class="text-grey" href="{{ url('user/my-account') }}"><b>MY ACCOUNT </b></a> /
              <a class="text-grey" href="#"><b>MY PROFILE</b></a>
            </span>
        </div>
    </div>
    <div id="godOfContent">
        <div class="box-content" id="signUp">
            <div class="row">
                <div class="small-12 columns">

                    <h5 class="title title-margin">MY PROFILE </h5>
                </div>
            </div>
            <div class="row">
                <div class="small-6 small-centered  columns">
                    <div class="form-box">
                        <div class="row">
                            {!! Form::model($user,['method'=>'POST', 'action'=>['UserController@postEditGeneralData', $user->id], 'files' => true]) !!}
                            <div class="small-4 columns">
                                <label for="middle-label" class="text-left middle">PROFILE PICTURE</label>
                            </div>
                            <div class="small-8 columns">
                                @if(auth()->user()->profile_picture)
                                    @if(strpos(auth()->user()->photo->file, 'facebook') !== false || strpos(auth()->user()->photo->file, 'google') !== false)
                                        <div class="preview img-wrapper" style="background-image:url({{ auth()->user()->photo->file }})"></div>
                                        
                                    @else
                                        <div class="preview img-wrapper" style="background-image:url({!! asset(null) !!}profile_picture/{{ auth()->user()->photo->file }})"></div>
                                    @endif
                                    
                                @else
                                    <div class="preview img-wrapper"></div>
                                @endif

                                <div class="file-upload-wrapper">
                                    <input type="file" name="file" class="file-upload-native" accept="image/*" />
                                    <input type="text" disabled class="file-upload-text btn-upload" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="small-4 columns">
                                    <label for="middle-label" class="text-left middle">TITLE</label>
                                </div>
                                <div class="small-8 columns">
                                    {!! Form::select('sex', array('m' => 'Mr.', 'fs' => 'Ms.', 'f' => 'Mrs.'), null, ['class'=>'short']) !!}
                                    {{--<select class="short" name="sex">

                                        <option value="m" {{ $user->sex == 'm' ? 'selected' : '' }}>Mr.</option>
                                        <option value="fs" {{ $user->sex == 'fs' ? 'selected' : '' }}>Ms.</option>
                                        <option value="f" {{ $user->sex == 'f' ? 'selected' : '' }}>Mrs.</option>
                                    </select>--}}
                                </div>
                            </div>
                            <div class="row">
                                <div class="small-4 columns">
                                    <label for="middle-label" class="text-left middle"><span class="required">FIRST NAME</span></label>
                                </div>
                                <div class="small-8 columns">
                                    {!! Form::text('name', null, ['class'=>'form-control']) !!}
                                </div>
                            </div>
                            {{--<div class="row">
                                <div class="small-4 columns">
                                    <label for="middle-label" class="text-left middle"><span class="required">LAST NAME</span></label>
                                </div>
                                <div class="small-8 columns">
                                    {!! Form::text('last_name', null, ['class'=>'form-control']) !!}
                                </div>
                            </div>--}}
                            <div class="row">
                                <div class="small-4 columns">
                                    <label for="middle-label" class="text-left middle">DATE OF BIRTH</label>
                                </div>
                                <div class="small-6 columns end">
                                    <div class="row">
                                        <div class="small-12 columns">
                                            <select name="dob_date" style="width:60px" required>

                                                {{--<option>Date</option>--}}
                                                @for ($date = 1; $date <= 31; $date++)
                                                    <option value="{{ $date }}" {{ date('d', strtotime($user->dob)) == $date ? 'selected' : ''}}>{{ $date }}</option>
                                                @endfor
                                            </select>

                                            <select name="dob_month" style="width:122px" required>
                                                {{--<option>Month</option>--}}

                                                <option value="1" {{ date('m', strtotime($user->dob)) == '01' ? 'selected' : ''}}>January</option>
                                                <option value="2" {{ date('m', strtotime($user->dob)) == '02' ? 'selected' : ''}}>February</option>
                                                <option value="3" {{ date('m', strtotime($user->dob)) == '03' ? 'selected' : ''}}>March</option>
                                                <option value="4" {{ date('m', strtotime($user->dob)) == '04' ? 'selected' : ''}}>April</option>
                                                <option value="5" {{ date('m', strtotime($user->dob)) == '05' ? 'selected' : ''}}>May</option>
                                                <option value="6" {{ date('m', strtotime($user->dob)) == '06' ? 'selected' : ''}}>June</option>
                                                <option value="7" {{ date('m', strtotime($user->dob)) == '07' ? 'selected' : ''}}>July</option>
                                                <option value="8" {{ date('m', strtotime($user->dob)) == '08' ? 'selected' : ''}}>August</option>
                                                <option value="9" {{ date('m', strtotime($user->dob)) == '09' ? 'selected' : ''}}>September</option>
                                                <option value="10" {{ date('m', strtotime($user->dob)) == '10' ? 'selected' : ''}}>October</option>
                                                <option value="11" {{ date('m', strtotime($user->dob)) == '11' ? 'selected' : ''}}>November</option>
                                                <option value="12" {{ date('m', strtotime($user->dob)) == '12' ? 'selected' : ''}}>December</option>
                                            </select>
                                            <select name="dob_year" style="width:80px" required>
                                                {{--<option>Year</option>--}}
                                                @for ($year = (date('Y') + 5); $year >= (date('Y') - 90); $year--)

                                                    <option value="{{ $year }}" {{ date('Y', strtotime($user->dob)) == $year ? 'selected' : ''}}>{{ $year }}</option>
                                                @endfor
                                            </select>
                                        </div>

                                    </div>

                                </div>
                            </div>
                            <div class="row">
                                <div class="small-4 columns">
                                    <label for="middle-label" class="text-left middle">MOBILE NUMBER</label>
                                </div>
                                <div class="small-8 columns">
                                    <div class="input-group">
                                        {{--<span class="input-group-label">+62</span>--}}
                                        {!! Form::text('phone_number', null, ['class'=>'form-control']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="small-1 columns small-offset-10">
                                    <button class="button btn-save">SAVE</button>
                                </div>
                            </div>
                            {!! Form::close() !!}
                            <hr class="blue">
                            {!! Form::model($user,['method'=>'POST', 'action'=>['UserController@postEditEmailData', $user->id]]) !!}
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <div class="row">
                                <div class="small-4 columns">
                                    <label for="middle-label" class="text-left middle">EMAIL</label>
                                </div>
                                <div class="small-8 columns">
                                    {!! Form::text('old_email', $user->email, ['class'=>'form-control']) !!}
                                </div>
                            </div>
                            <div class="row">
                                <div class="small-4 columns">
                                    <label for="middle-label" class="text-left middle">NEW EMAIL</label>
                                </div>
                                <div class="small-8 columns">
                                    <input type="text"  placeholder="" name="email">
                                </div>
                            </div>
                            <div class="row">
                                <div class="small-4 columns">
                                    <label for="middle-label" class="text-left middle">CONFIRM EMAIL</label>
                                </div>
                                <div class="small-8 columns">
                                    <input type="text"  placeholder="" name="email_confirmation">
                                </div>
                            </div>
                            <div class="row">
                                <div class="small-1 columns small-offset-10">
                                    <button class="button btn-save">SAVE</button>
                                </div>
                            </div>
                            {!! Form::close() !!}
                            <hr class="blue">
                            {!! Form::model($user,['method'=>'POST', 'action'=>['UserController@postEditPasswordData', $user->id]]) !!}
                            <div class="row">
                                <div class="small-4 columns">
                                    <label for="middle-label" class="text-left middle">CURRENT PASSWORD</label>
                                </div>
                                <div class="small-8 columns">
                                    <input type="password"  placeholder="" name="old_password">
                                </div>
                            </div>
                            <div class="row">
                                <div class="small-4 columns">
                                    <label for="middle-label" class="text-left middle">PASSWORD</label>
                                </div>
                                <div class="small-8 columns">
                                    <input type="password"  placeholder="tye your new password" name="password">
                                </div>
                            </div>
                            <div class="row">
                                <div class="small-4 columns">
                                    <label for="middle-label" class="text-left middle">CONFIRM PASSWORD</label>
                                </div>
                                <div class="small-8 columns">
                                    <input type="password"  placeholder="retype your new password" name="password_confirmation">
                                </div>
                            </div>
                            <div class="row">
                                <div class="small-1 columns small-offset-10">
                                    <button class="button btn-save">SAVE</button>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).on("change","select",function(){
            $("option[value=" + this.value + "]", this)
                .attr("selected", true).siblings()
                .removeAttr("selected")
        });
    </script>
@endsection