@extends('layouts.newmaster')

@section('title', App\Page::createTitle('My Account'))

@section('content')

    <div id="godOfContent">
      <div class="box-content" id="checkout">
        <div class="row">
          <div class="small-5 small-centered columns"><br><br>
            <h5 class="title title-margin">MY ACCOUNT</h5>
            <p class="b2">
              <center>
                Welcome <a href="#"> {{ auth()->user()->name }}</a>! This is your SeaferMart account. <br>
                Click on the following section to manage your personal information
              </center>
            </p>
          </div>
        </div>
        <div class="row">
          <div class="small-3 columns">
            <img class="top-15" src="{{ asset(null) }}assets/images/img-market.png">
          </div>
          <div class="small-6 columns">
            <div class="menu-account-box">
              <div class="row">
                <div class="small-6 columns">
                  <table>
                    <tbody>
                      <tr>
                        <td><img src="{{ asset(null) }}assets/images/ico-maskot-dua.png"></td>
                        <td><a href="{{ url('user/my-profile') }}">MY PROFILE</a></td>
                      </tr>
                     <tr>
                      <td><img src="{{ asset(null) }}assets/images/ico-house-dua.png"></td>
                      <td><a href="{{ url('user/my-address') }}">MY ADDRESS BOOK</a></td>
                    </tr>
                    <tr>
                      <td><img src="{{ asset(null) }}assets/images/ico-chart-dua.png"></td>
                      <td><a href="{{ url('orders') }}">MY ORDERS</a></td>
                    </tr>
                    <tr>
                      <td><img src="{{ asset(null) }}assets/images/ico-point-dua.png"></td>
                      <td><a href="{{ url('user/my-cashback-rewards') }}">MY CASHBACK BALANCE</a></td>
                    </tr>
                      {{--<tr>
                        <td><img src="{{ asset(null) }}assets/images/ico-shop-dua.png"></td>
                        <td><a href="#">MY SHOP</a></td>
                      </tr>--}}
                    </tbody>
                  </table>
                </div>
                <div class="small-6 columns">
                  <table>
                    <tbody>
                      <tr>
                        <td><img src="{{ asset(null) }}assets/images/ico-cc-dua.png"></td>
                        <td><a href="{{ url('user/my-bank-account') }}">MY BANK ACCOUNT</a></td>
                      </tr>
                      {{--<tr>
                        <td><img src="{{ asset(null) }}assets/images/ico-love-dua.png"></td>
                        <td><a href="#">MY HEARTS</a></td>
                      </tr>--}}
                      <tr>
                        <td><img src="{{ asset(null) }}assets/images/ico-star-dua.png"></td>
                        <td><a href="{{ url('user/my-rating-review') }}">MY RATINGS & REVIEWS</a></td>
                      </tr>
                      <tr>
                        <td><img src="{{ asset(null) }}assets/images/ico-message-dua.png"></td>
                        <td><a href="{{ url('user/my-email-preference') }}">MY EMAIL PREFERENCES</a></td>
                      </tr>
                      {{--<tr>
                        <td><img src="{{ asset(null) }}assets/images/ico-lupus-dua.png"></td>
                        <td><a href="my-profile.html">MY RECIPE</a></td>
                      </tr>--}}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>  
          </div>
          <div class="small-3 columns">
            <img src="{{ asset(null) }}assets/images/img-seafer.png">
          </div>
        </div>
      </div>
    </div>

@endsection