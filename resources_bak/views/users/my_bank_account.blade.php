@extends('layouts.newmaster')

@section('title', App\Page::createTitle('My Bank Account'))

@section('customcss')
    <style>
        .btn-aneh:hover {
            background-color:#0368ff;
        }
    </style>
@endsection

@section('content')

    <div class="row" style="margin-bottom:35px">
        <div class="small-12 columns">
            <span class="text-grey equal margin-top-10">
              <a class="text-grey" href="{{ url('/') }}"><b>HOME</b></a> /
              <a class="text-grey" href="{{ url('user/my-account') }}"><b>MY ACCOUNT </b></a> /
              <a class="text-grey" href="#"><b>MY BANK ACCOUNT</b></a>
            </span>
        </div>
    </div>
    <section>
        <div class="row">
            <div class="small-12 columns">
                <h5 class="title title-margin">MY BANK ACCOUNT</h5>
            </div>
        </div>
        <div class="row">
            <div class="medium-3 columns center">
                <img class="left" src="{!! asset(null) !!}assets/images/bank-account-left.png">
            </div>
            <div class="medium-6 columns center">
                {{--<h5 class="grey-bold margin-bottom-30">BANK ACCOUNT LIST</h5>--}}
                <div class="medium-6 medium-offset-4 left">
                    @if($bank_list->count() > 0)
                    @foreach($bank_list as $bank)
                    <p class="semi">
                        {{ $bank->name }} {{ $bank->branch }}
                        <br>a/n {{ $bank->account_name }}
                        <br>{{ $bank->account_number }}
                        <br>
                        
                        @if($bank->is_preference == 0)
                        <a  class="grey-underline" href="{{ url('user/set-bank-account-preference/'.$bank->id) }}">Set as default</a>
                        @else
                        <a  class="grey-underline" href="#">Default account</a>
                        @endif 
                        <a class="grey-underline"  href="{{ url('user/delete-bank-account/'.$bank->id) }}">delete</a>
                    </p>
                    @endforeach
                    @else
                    <p class="semi">
                        You have not add any bank to your preference
                    </p>
                    @endif
                </div>
                <a href="{{ url('user/add-bank-account') }}" class="button blue-bg space-3 xyz">ADD BANK ACCOUNT</a>
            </div>
            <div class="medium-3 columns center">
                <img class="left" src="{!! asset(null) !!}assets/images/bank-account-right.png">
            </div>
        </div>
    </section>

@endsection