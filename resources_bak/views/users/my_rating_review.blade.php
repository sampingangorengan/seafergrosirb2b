@extends('layouts.newmaster')

@section('title', App\Page::createTitle('My Ratings & Reviews'))

@section('customcss')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.2.0/jquery.rateyo.min.css">
    <style>
        .small-9{
            width:70%;
        }
        #myRatingsReviews .panel-content-box table tbody tr td.v-align-top{
            width:60px;
        }
    </style>
@endsection


@section('content')
    <div class="row" style="margin-bottom:35px">
        <div class="small-12 columns">
            <span class="text-grey equal margin-top-10">
              <a class="text-grey" href="{{ url('/') }}"><b>HOME</b></a> /
              <a class="text-grey" href="{{ url('user/my-account') }}"><b>MY ACCOUNT </b></a> /
              <a class="text-grey" href="#"><b>MY RATINGS &amp; REVIEWS</b></a>
            </span>
        </div>
    </div>
    <div id="godOfContent">
        <div class="box-content" id="myRatingsReviews">
            <div class="row">

                <div class="small-12 columns">
                    <h4 class="text-blue">MY RATINGS & REVIEWS</h4>
                </div>
            </div>
            <div class="row">
                <div class="small-9 columns small-centered">
                    <div class="customer-and-reviews-box">
                        <ul class="tabs" data-tabs id="example-tabs">
                            <li class="tabs-title is-active"><a href="#panelGrocer" aria-selected="true">GROCER</a></li>
                            {{--<li class="tabs-title"><a href="#panelCook">COOK</a></li>
                            <li class="tabs-title"><a href="#panelEats">EATS</a></li>--}}
                        </ul>
                        <div class="tabs-content top-line-blue" data-tabs-content="example-tabs"><br>
                            <div class="tabs-panel is-active" id="panelGrocer">
                                @foreach($grocer_list as $item)
                                <div class="panel-content-box">
                                    <div class="row">
                                        <div class="large-4 columns no-padding">
                                            <div class="img-box">
                                                <img src="{{ $item->groceries->getFirstImageUrl($item->groceries_id) }}">
                                            </div>
                                        </div>
                                        <div class="large-8 columns no-padding control-rate">
                                            <table>
                                                <tbody>
                                                <tr>
                                                    <td colspan="2">
                                                        <div class="small-6 columns no-padding">
                                                            <div style="min-height:50px !important">
                                                                {{ $item->groceries->name }}<br> <span>by {{ $item->groceries->brand['name'] }}</span>
                                                            </div>
                                                        </div>
                                                        <div class="small-6 columns no-padding">
                                                            <div class="rating-box" style="margin-right:20px;">
                                                                Ratings
                                                                @if( false !== $item->getRateByUser(auth()->user()->id, $item->groceries_id))
                                                                <input type="hidden" name="fixval" value="{{ $item->getRateByUser(auth()->user()->id, $item->groceries_id)->rate }}">
                                                                <div class="rateYostatic" style="padding:0;"></div>
                                                                {{--<ul>
                                                                    <li><img src="{!! asset(null) !!}assets/images/ico-star-fill-color.png"></li>
                                                                    <li><img src="{!! asset(null) !!}assets/images/ico-star-fill-color.png"></li>
                                                                    <li><img src="{!! asset(null) !!}assets/images/ico-star-fill-color.png"></li>
                                                                    <li><img src="{!! asset(null) !!}assets/images/ico-star-fill-color.png"></li>
                                                                    <li><img src="{!! asset(null) !!}assets/images/ico-star-fill-color.png"></li>
                                                                </ul>--}}
                                                                @else
                                                                <div class="rateYo" style="padding:0;"></div>
                                                                {{--<ul>
                                                                    <li><img src="{!! asset(null) !!}assets/images/ico-star-border.png"></li>
                                                                    <li><img src="{!! asset(null) !!}assets/images/ico-star-border.png"></li>
                                                                    <li><img src="{!! asset(null) !!}assets/images/ico-star-border.png"></li>
                                                                    <li><img src="{!! asset(null) !!}assets/images/ico-star-border.png"></li>
                                                                    <li><img src="{!! asset(null) !!}assets/images/ico-star-border.png"></li>
                                                                </ul>--}}
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                @if($item->is_reviewed == 0)
                                                <tr>
                                                    <td class="v-align-top">
                                                        <input type="hidden" name="rate" value="0">
                                                        <input type="hidden" name="gr_id" value="{{ $item->groceries->id }}">
                                                        <input type="hidden" name="ogid" value="{{ $item->id }}">
                                                        <!-- <img src="assets/images/ico-header-acc.png"> --> &nbsp;
                                                    </td>
                                                    <td>
                                                        <div class="form-box">
                                                            <p class="shr">Share your thoughts with other customers</p>
                                                            <button type="button" class="addReview button btn-aneh">ADD REVIEW</button>
                                                        </div>
                                                    </td>
                                                </tr>
                                                @elseif(false !== $item->getRateByUser(auth()->user()->id, $item->groceries_id))
                                                <tr>
                                                    <td class="v-align-top">
                                                        <input type="hidden" name="rate" value="0">
                                                        <input type="hidden" name="gr_id" value="{{ $item->groceries->id }}">
                                                        @if(auth()->user()->profile_picture)
                                                            @if(strpos(auth()->user()->photo->file, 'facebook') !== false || strpos(auth()->user()->photo->file, 'google') !== false)
                                                                <img src="{{ auth()->user()->photo->file }}" style="width:40px;height:40px;border-radius: 20px;border:1px solid blue;">
                                                            @else
                                                                <img src="{!! asset(null) !!}profile_picture/{{ auth()->user()->photo->file }}" style="width:40px;height:40px;border-radius: 20px;border:1px solid blue;">
                                                            @endif
                                                        @else
                                                            <img src="{!! asset(null) !!}assets/images/ico-header-acc.png" >
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <b class="text-blue">Your Comment</b><br>
                                                        <i>{{ LocalizedCarbon::parse($item->rate->created_at)->format('d F Y, H:s') }}</i><br>
                                                        <b>{{ $item->getRateByUser(auth()->user()->id, $item->groceries_id)->title }}</b>
                                                        <p>{{ $item->getRateByUser(auth()->user()->id, $item->groceries_id)->review }}</p>
                                                    </td>
                                                </tr>
                                                @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.2.0/jquery.rateyo.min.js"></script>
    <script>
        $(document).ready(function(){
            $(".rateYo").rateYo({
                maxValue: 5,
                numStars: 5,
                starWidth: "20px",
                rating:$(this).attr('data-value'),
                fullStar: true,
                multiColor: {
                    "startColor": "#f9e102", //RED
                    "endColor"  : "#f9e102"  //GREEN
                },
                onInit: function (rating, rateYoInstance) {

                    console.log("Ready to rate?");
                },
                onSet: function (rating, rateYoInstance) {
                    var rate_container = $(this).parent().parent().parent().parent().parent();
                    rate_container.find('input[name="rate"]').val(rating);
                }
            });


            $(".rateYostatic").rateYo({
                maxValue: 5,
                numStars: 5,
                starWidth: "20px",
                //rating: 3,//$(this).attr('data-value'),
                fullStar: true,
                readOnly: true,
                multiColor: {
                    "startColor": "#f9e102", //RED
                    "endColor"  : "#f9e102"  //GREEN
                }
            });
            $(".rateYostatic").each(function( index ) {
                var val = $(this).parent().find('input[name="fixval"]').val();
                $(this).rateYo("option", "rating", val);
            });


            $('.control-rate').on('click', '.doSubmit', function(){
                var container = $(this).parent().parent().parent();
                var ogid = container.find('input[name="ogid"]').val();
                var grid = container.find('input[name="gr_id"]').val();
                var rate = container.find('input[name="rate"]').val();
                var title = container.find('input[name="title"]').val();
                var message = container.find('textarea[name="body"]').val();
                // if(title && message){
                    $.ajax({
                        type: 'post',
                        url: url('reviews'),
                        data: {
                            grid: grid,
                            ogid:ogid,
                            rate: rate,
                            title: title,
                            message: message
                        }
                    }).done(function(result){
                        if(result == 'saved'){
                            alert('Your review has been saved');
                            location.reload();
                        } else {
                            alert('Something wrong while saving your review.')
                        }

                    })
                // } else {
                //     alert('Headline and Review Message required');
                // }
            });
        });
    </script>
@endsection