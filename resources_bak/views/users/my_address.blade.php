@extends('layouts.newmaster')

@section('title', App\Page::createTitle('My Address'))

@section('customcss')
    <style>
        .btn-aneh:hover {
            background-color:#0368ff;
        }
    </style>
@endsection

@section('content')

    <div class="row" style="margin-bottom:35px">
        <div class="small-12 columns">
            <span class="text-grey equal margin-top-10">
              <a class="text-grey" href="{{ url('/') }}"><b>HOME</b></a> /
              <a class="text-grey" href="{{ url('user/my-account') }}"><b>MY ACCOUNT </b></a> /
              <a class="text-grey" href="#"><b>MY ADDRESS BOOK</b></a>
            </span>
        </div>
    </div>
    <div id="boxOfContent">
        <div class="box-content" id="checkout">
            <div class="row">
                <div class="small-12 columns">
                    <span class="text-grey equal margin-top-10">
                    </span>
                    <h5 class="title title-margin">MY ADDRESS BOOK</h5>
                </div>
            </div>
            <div class="row">
                <div class="small-3 columns">
                    <br><br><br>
                    <div class="rumah-box">
                        <img src="{!! asset(null) !!}assets/images/img-house-yellow.png">
                        <img class="left" src="{!! asset(null) !!}assets/images/img-house-bluesky.png">
                    </div>
                </div>
                <div class="small-6 columns">
                    <div class="row">
                        <div class="small-12 columns">
                            <div style="margin:0 auto;width:40%">
                                <a href ="{{ url('user/add-new-address') }}"><button class="button btn-aneh">ADD NEW ADDRESS</button></a>
                                @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                                    @if(Session::has('alert-' . $msg))

                                        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} </p>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        @if($addresses && count($addresses) > 0)
                            @foreach($addresses as $address)
                        <div class="small-6 no-padding columns">
                            <div class="address-card">
                                <h6 style="margin-bottom:5px">{{ $address->name }}</h6>
                                <p style="margin-bottom:5px">
                                    <strong>
                                        @if($address->recipient_title == 'm')
                                            Mr.&nbsp;
                                        @elseif($address->recipient_title == 'fs')
                                            Ms.&nbsp;
                                        @else
                                            Mrs.&nbsp;
                                        @endif
                                        {{ $address->recipient }}
                                    </strong>
                                </p>
                                <p style="margin-bottom:2px"><strong>+62{{ $address->phone_number }}</strong></p>
                                <p>
                                    {{ $address->address }},<br>
                                    {{ $address->area->name }}, {{ $address->area->city->city_name }}, <br>
                                    {{ $address->city->province->province_name_id }},&nbsp;Indonesia<br/>
                                    {{ $address->postal_code }}<br>
                                </p>
                                <p>
                                    <span style="float:left; font-size:13px">
                                <a class="no-padding" href="{{ url('user/edit-address/'.$address->id) }}"><b>edit</b></a>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </span>
                                    <span style="float:right">
                                
                                {!! Form::open(['method' => 'DELETE', 'action' => ['UserController@deleteAddress', $address->id], 'class'=> "pull-left", 'id'=>'form-address-'.$address->id]) !!}
                                {{--<button type="submit" class="btn btn-danger btn-sm btn-flat delete-entity-index">Delete</button>--}}
                                <button type="button"><b>delete</b></button>
                                {!! Form::close() !!}
                                
                                    </span>
                                </p>

                            </div>
                        </div>
                            @endforeach
                        @endif
                        {{--<div class="small-6 no-padding columns">
                            <div class="address-card">
                                <h6>TAMAN SURYA</h6>
                                <p>
                                    Jl. Taman Surya III F5 No.26 <br>
                                    Jakarta Barat 11830 <br>
                                    Indonesia
                                </p>
                                <a class="no-padding" href="#"><b>edit</b></a>
                                <a href="#"><b>delete</b></a>
                            </div>
                        </div>
                        <div class="small-6 no-padding columns">
                            <div class="address-card">
                                <h6>RS. FATMAWATI RAYA</h6>
                                <p>
                                    Jl. RS Fatmawati Raya No. 4H-I <br>
                                    Jakarta Selatan 12430 <br>
                                    Indonesia
                                </p>
                                <a class="no-padding" href="#"><b>edit</b></a>
                                <a href="#"><b>delete</b></a>
                            </div>
                        </div>
                        <div class="small-6 no-padding columns">
                            <div class="address-card">
                                <h6>RS. FATMAWATI RAYA</h6>
                                <p>
                                    Jl. RS Fatmawati Raya No. 4H-I <br>
                                    Jakarta Selatan 12430 <br>
                                    Indonesia
                                </p>
                                <a class="no-padding" href="#"><b>edit</b></a>
                                <a href="#"><b>delete</b></a>
                            </div>
                        </div>--}}
                    </div>
                </div>
                <div class="small-3 columns">
                    <br><br><br>
                    <div class="rumah-box">
                        <img src="{!! asset(null) !!}assets/images/img-house-blue.png">
                        <img class="right" src="{!! asset(null) !!}assets/images/img-house-red.png">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection