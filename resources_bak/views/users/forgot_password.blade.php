@extends('layouts.newmaster')

@section('title', App\Page::createTitle('Forgot Password'))

@section('content')

    <div id="godOfContent">
        <div class="box-content" id="signUp">
            <div class="row">
                <div class="large-12 columns">
                    <h6 class="title">FORGOT PASSWORD</h6>
                </div>
            </div>
            <div class="row">
                <div class="large-3 columns no-padding center">
                    <img class="pink" src="{{ asset(null) }}assets/images/img-swim.png" style="top:-20px">
                </div>

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <form method="POST" action="{{ url('/password/reset') }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="token" value="{{ $token }}">
                    <div class="large-8 columns no-padding">
                        <div class="form-box">
                            <div class="row">
                                <div class="small-4 columns">
                                    <label for="middle-label" class="text-left middle">Email</label>
                                </div>
                                <div class="small-8 columns">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ $email or old('email') }}" style="height:24px;border:1.5px solid #808184;">
                                </div>
                            </div>
                            <div class="row">
                                <div class="small-4 columns">
                                    <label for="middle-label" class="text-left middle">New Password</label>
                                </div>
                                <div class="small-8 columns">
                                    <input id="password" type="password" class="form-control" name="password">
                                </div>
                            </div>
                            <div class="row">
                                <div class="small-4 columns">
                                    <label for="middle-label" class="text-left middle">Confirm Password</label>
                                </div>
                                <div class="small-8 columns">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
                                </div>
                            </div>
                            <div class="row">
                                <div class="small-4 small-centered columns">
                                    <button type="submit" class="button primary btn-aneh">RESET PASSWORD</button>
                                </div>
                            </div>

                        </div>
                    </div>

                </form>


            </div>
        </div>
    </div>

@endsection