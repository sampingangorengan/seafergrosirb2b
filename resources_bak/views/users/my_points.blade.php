@extends('layouts.newmaster')

@section('title', App\Page::createTitle('My Cashback Rewards'))

@section('customcss')
    <style>
        .btn-aneh:hover {
            background-color:#0368ff;
        }
    </style>
@endsection

@section('content')

    <div class="row" style="margin-bottom:35px">
        <div class="small-12 columns">
            <span class="text-grey equal margin-top-10">
              <a class="text-grey" href="{{ url('/') }}"><b>HOME</b></a> /
              <a class="text-grey" href="{{ url('user/my-account') }}"><b>MY ACCOUNT </b></a> /
              <a class="text-grey" href="#"><b>MY CASHBACK REWARDS</b></a>
            </span>
        </div>
    </div>
    <section>
        <div class="row">
            <div class="small-12 columns">
                <h5 class="title title-margin">My Cashback Balance</h5>
                <div class="medium-3 medium-offset-2 columns center">
                    <div class="img-box">
                        <img src="{!! asset(null) !!}assets/images/loyaltypoint.png">
                    </div>
                </div>
                <div class="medium-2 columns center" style="margin-top: 2rem;">
                    <p>Your Current Rewards:</p>
                    @if(auth()->user()->points()->get()->count() > 0)
                    <h1 class="big-blue" style="width:500px;text-align: left;">{{ Money::display(auth()->user()->points()->first()->nominal) }}</h1>
                    @else
                    <h1 class="big-blue" style="width:500px;text-align: left;">{{ Money::display(0) }}</h1>
                    @endif
                </div>
                <div class="medium-3 medium-pull-2 columns center">
                    <div class="img-box">
                        <!-- <img src="{!! asset(null) !!}assets/images/coins.png"> -->
                    </div>
                </div>
            </div>
        </div>
        <div class="row table-rewards">
            <div class="small-10 small-centered columns">
                <b class="text-green">REWARDS POINTS</b><b class="text-orange"> HISTORY</b>
                <table class="rewards">
                    <thead class="border-green">
                    <tr>
                        <th width="200"><span style="color:#808184;">Date</span></th>
                        <th width="200"><span style="color:#808184;">Rewards Nominal</span></th>
                        <th width="200"><span style="color:#808184;">Activities</span></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($cashback_rewards as $cashback)
                    <tr>
                        <td>{{ LocalizedCarbon::parse($cashback->created_at)->format('d F Y, H.i') }}</td>
                        
                        @if($cashback->obtain_from != 'redeem')
                        
                        <td>{{ number_format($cashback->nominal, 0, '.', ',') }}</td>
                        @else
                        <td style="color:red"> - {{ number_format($cashback->nominal, 0, '.', ',') }}</td>
                        @endif
                        @if($cashback->obtain_from == 'order')
                            <td>Order number <a href="{{ url('orders/'.$cashback->order_id) }}" target="_blank">{{ $cashback->order_id }}</a></td>
                        @elseif($cashback->obtain_from == 'redeem')
                            <td>Redeemed on Order number <a href="{{ url('orders/'.$cashback->order_id) }}" target="_blank">{{ $cashback->order_id }}</a></td>
                        @else
                            <td>Referral from <i>{{ $cashback->order->user()->first()->name }}</i></td>
                        @endif
                    </tr>
                    @endforeach
                    {{--<tr>
                        <td>31 Oct 16, 17.00</td>
                        <td>500</td>
                        <td>Added by Shop</td>
                    </tr>
                    <tr>
                        <td>31 Oct 16, 17.00</td>
                        <td>500</td>
                        <td>Added by Shop</td>
                    </tr>
                    <tr>
                        <td>31 Oct 16, 17.00</td>
                        <td>500</td>
                        <td>Added by Shop</td>
                    </tr>
                    <tr>
                        <td>31 Oct 16, 17.00</td>
                        <td>500</td>
                        <td>Added by Shop</td>
                    </tr>
                    <tr>
                        <td>31 Oct 16, 17.00</td>
                        <td>500</td>
                        <td>Added by Shop</td>
                    </tr>--}}
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row" style="margin-top: 2rem;margin-bottom: 4rem;" >
            <div class="small-10 small-centered columns">
                <div class="small-6 columns">
                    <div class="sub-content" id="rules-rewards">
                        <h6>RULES</h6>
                        <ul>
                            <li>
                                Get your 0.5% cashback promo everytime you make a purchase of Rp100.000.
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="small-6 columns right" style="margin-right: -15px;">
                    <a href="#" id="referralcode" data-open="modal-referralcode" style="color:   #808080;font-size:12px;margin-left:15px;"><button class="button primary btn-aneh">SHARE REFERRAL CODE</button></a><br>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('scripts')
    <script type="text/javascript">
        document.getElementById('shareBtn').onclick = function() {
            FB.ui({
                method: 'share',
                display: 'popup',
                quote: 'Click this link "{{ url('user/sign-up?refer_from='.auth()->user()->referral_code) }}" to get benefit when you buy good things from seafermart.co.id',
                href: '{{ url('user/sign-up?refer_from='.auth()->user()->referral_code) }}',
            }, function(response){});
        }
        $(document).ready(function(){
            $('#share-referral-form').submit(function(){
                $.ajax({
                    url : $(this).attr('action'),
                    type : 'POST',
                    dataType : 'json',
                    data : $(this).serialize(),
                    success : function(response){
                        if(response.status == 'Ok'){
                            alert('You have successfully referred your friend to Seafermart. Now you can enjoy the cashback every time they shop.');
                        } else {
                            alert(response.status);
                        }
                    }
                })
                return false;
            })
        })
    </script>
@endsection