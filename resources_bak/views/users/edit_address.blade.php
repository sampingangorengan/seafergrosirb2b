@extends('layouts.newmaster')

@section('title', App\Page::createTitle('Add New Content'))

@section('content')

    <div id="godOfContent">
        <div class="box-content margin-bottom-100" id="checkout">
            <div class="row">
                <div class="small-12 columns">
                    <span class="text-grey margin-top-10">
                      <a class="text-grey margin-right-5" href="{{ url('user/my-account') }}"><b>MY ACCOUNT</b></a> /
                      <a class="text-grey margin-right-5" href="{{ url('user/my-address') }}"><b>MY ADDRESS BOOK</b></a> /
                      <a class="text-grey " href="#"><b>EDIT ADDRESS</b></a>
                    </span>
                    <h5 class="title title-margin">EDIT ADDRESS "{{ $address->name }}"</h5>
                </div>
            </div>
            <div class="row">
                <div class="small-6 small-centered">

                    {!! Form::model($address,['method'=>'PATCH', 'action'=>['UserController@patchUpdateAddress', $address->id]]) !!}
                        <div class="form-box">
                            <div class="row">
                                <div class="small-4 columns">
                                    <label for="middle-label" class="text-left middle "><span class="required">ADDRESS NAME</span></label>
                                </div>
                                <div class="small-8 columns">
                                    {{--<input type="text"  placeholder="e.g., Alamat Kantor" name="name">--}}
                                    {!! Form::text('name', null, ['class'=>'form-control', 'placeholder' => 'e.g., Alamat Kantor', 'required' => 'required']) !!}
                                </div>
                            </div>
                            <div class="row">
                                <div class="small-4 columns">
                                    <label for="middle-label" class="text-left middle">TITLE</label>
                                </div>
                                <div class="small-8 columns">
                                    {!! Form::select('recipient_title', array('m' => 'Mr.', 'fs' => 'Ms.', 'f' => 'Mrs.'), null, ['class'=>'form-control']) !!}
                                    {{--<select class="short" name="sex">
                                        <option value="m">Mr.</option>
                                        <option value="fs">Ms.</option>
                                        <option value="f">Mrs.</option>
                                    </select>--}}
                                </div>
                            </div>
                            <div class="row">
                                <div class="small-4 columns">
                                    <label for="middle-label" class="text-left middle"><span class="required">RECIPIENT</span></label>
                                </div>
                                <div class="small-8 columns">
                                    {{--<input type="text"  placeholder="First Name" name="first_name" required="required">--}}
                                    {!! Form::text('recipient', null, ['class'=>'form-control', 'placeholder' => 'e.g., Barry Allen', 'required' => 'required']) !!}
                                </div>
                            </div>
                           {{-- <div class="row">
                                <div class="small-4 columns">
                                    <label for="middle-label" class="text-left middle"><span class="required">LAST NAME</span></label>
                                </div>
                                <div class="small-8 columns">
                                    <input type="text"  placeholder="Last Name" name="last_name" required="required">
                                </div>
                            </div>--}}
                            <div class="row">
                                <hr class="blue">
                            </div>
                            <div class="row">&nbsp;</div>
                            <div class="row">
                                <div class="small-4 columns">
                                    <label for="middle-label" class="text-left middle"><span class="required">ADDRESS LINE1</span></label>
                                </div>
                                <div class="small-8 columns">
                                    {{--<input type="text"  placeholder="Address" name="address_one" required="required">--}}
                                    {!! Form::text('address', null, ['class'=>'form-control', 'placeholder' => 'Address', 'required' => 'required']) !!}
                                </div>
                            </div>
                            <div class="row">
                                <div class="small-4 columns">
                                    <label for="middle-label" class="text-left middle">ADDRESS LINE 2</label>
                                </div>
                                <div class="small-8 columns">
                                    <input type="text"  placeholder="Address" name="address_two">
                                </div>
                            </div>
                            <div class="row">
                                <div class="small-4 columns">
                                    <label for="middle-label" class="text-left middle">ADDRESS LINE 3</label>
                                </div>
                                <div class="small-8 columns">
                                    <input type="text"  placeholder="Address" name="addres_three">
                                </div>
                            </div>
                            <div class="row">
                                <div class="small-4 columns">
                                    <label for="middle-label" class="text-left middle"><span class="required">PROVINCE</span></label>
                                </div>
                                <div class="small-8 columns">
                                    {!! Form::select('province_id', $provinces, null, ['class'=>'form-control', 'id' => 'select_province']) !!}
                                    {{--<select class="short" name="province" id="select_province">
                                        @foreach(App\Models\Province::get() as $province)
                                            <option value="{{ $province->province_id }}">{{ $province->province_name }}</option>
                                        @endforeach
                                    </select>--}}
                                </div>
                            </div>
                            <div class="row">
                                <div class="small-4 columns">
                                    <label for="middle-label" class="text-left middle"><span class="required">CITY</span></label>
                                </div>
                                <div class="small-8 columns">
                                    {!! Form::select('city_id', $cities, null, ['class'=>'form-control', 'id' => 'select_city']) !!}
                                    {{--<select class="short" name="city" id="select_city">
                                        @foreach(App\Models\City::orderBy('city_name', 'asc')->get() as $city)
                                            <option value="{{ $city->city_id }}" data-prov="{{ $city->province_id }}">{{ $city->city_name }}</option>
                                        @endforeach
                                    </select>--}}
                                </div>
                            </div>
                            <div class="row">
                                <div class="small-4 columns">
                                    <label for="middle-label" class="text-left middle"><span class="required">AREA</span></label>
                                </div>
                                <div class="small-8 columns">
                                    {!! Form::select('area_id', $areas, null, ['class'=>'form-control', 'id' => 'select_area']) !!}
                                    {{--<select class="short" name="area" id="select_area">
                                        @foreach(App\Models\Location\Area::orderBy('name', 'asc')->get() as $area)
                                            <option value="{{ $area->id }}" data-city="{{ $area->city_id }}">{{ $area->name }}</option>
                                        @endforeach
                                    </select>--}}
                                </div>
                            </div>
                            <div class="row">
                                <div class="small-4 columns">
                                    <label for="middle-label" class="text-left middle">POSTAL CODE</label>
                                </div>
                                <div class="small-8 columns">
                                    {{--<input type="text"  placeholder="Postal Code" name="postal_code">--}}
                                    {!! Form::text('postal_code', null, ['class'=>'form-control', 'placeholder' => 'Postal Code', 'required' => 'required']) !!}
                                </div>
                            </div>
                            <div class="row">
                                <div class="small-4 columns">
                                    <label for="middle-label" class="text-left middle"><span class="required">MOBILE NUMBER</span></label>
                                </div>
                                <div class="small-8 columns">
                                    <div class="input-group">
                                        <span class="input-group-label">+62</span>
                                        {{ csrf_field() }}
                                        {{--<input class="input-group-field" type="text" name="phone_number" placeholder="8129xxx">--}}
                                        {!! Form::text('phone_number', null, ['class'=>'input-group-field', 'placeholder' => 'Address', 'required' => 'required']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="small-4 columns">
                                    <label for="middle-label" class="text-left middle" style="color:white">MAKE THIS MY</label>
                                </div>
                            {{--<div class="small-8 columns" style="text-align:right;">
                                <table>
                                    <tbody>
                                    <tr>
                                        <td>
                                            <div class="box text-left">
                                                <input type="checkbox" id="defaultAddress" name="default-address">
                                                <label for="defaultAddress">default address</label>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="box text-left">
                                                <input type="checkbox" id="shippingAddress" name="default-address">
                                                <label for="shippingAddress">shipping address</label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="box text-left">
                                                <input id="pickUpAddress" type="checkbox" name="default-address">
                                                <label for="pickUpAddress">pickup address</label>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="box text-left">
                                                <input id="billingAddress" type="checkbox" name="default-address">
                                                <label for="billingAddress">billing address</label>
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>--}}
                            <!-- <div class="small-6 columns"> -->
                                <button  class="button btn-action blue space-3 savebutton" id="form-submit-button" type="submit">SAVE</button>
                                <!-- </div> -->
                                <!-- <div class="small-6 columns"> -->
                                <a href="{{ url('user/my-address') }}" class="button btn-action red space-3"><span style="color:white">CANCEL</span></a>
                                <!-- </div> -->
                            </div>
                        </div>
                        <div class="reveal" id="modal-submit" data-reveal>
                            <div class="modal-box">
                                <div class="dashed-box">
                                    <div class="img-box">
                                        <img src="{!! asset(null) !!}assets/images/img-popup.png">
                                    </div>
                                    <h5>Thank you for submitting!<br />we will process your application</h5>
                                    <a href="{{ url('user/my-account') }}">Click here</a> <span>to continue</span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="small-6 columns"></div>
                            <div class="small-2 columns"></div>
                            <div class="small-4 columns">
                                <div class="row">

                                </div>
                            </div>
                        </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    </div>

@endsection

@section('scripts')
    <script>
        $('#select_province').on('change', function(){
            var province_id = $(this).val();
            $.ajax({
                type: 'get',
                url: url('user/ajax-cities-by-province/'+province_id)
            }).done(function(result){
                result = jQuery.parseJSON(result);
                if(result.status == 'success') {
                    $('#select_city').html('');
                    $.each(result.cities, function(k, v) {
                        $('#select_city').append('<option value="'+k+'">'+v+'</option>');
                    });

                } else {
                    alert(result.message);
                }
            })
        });
        $('#select_city').on('change', function(){
            var city_id = $(this).val();
            $.ajax({
                type: 'get',
                url: url('user/ajax-areas-by-city/'+city_id)
            }).done(function(result){
                result = jQuery.parseJSON(result);
                if(result.status == 'success') {
                    $('#select_area').html('');
                    $.each(result.areas, function(k, v) {
                        $('#select_area').append('<option value="'+k+'">'+v+'</option>');
                    });

                } else {
                    alert(result.message);
                }
            })

        });
        $('#form-submit-button').click(function(){
            if($('#select_area').val() == 0 || $('#select_area').val() == '0'){
                e.preventDefault();
                alert('Please select area');
                return false;
            }
            if($('#select_city').val() == 0 || $('#select_city').val() == '0'){
                e.preventDefault();
                alert('Please select city');
                return false;
            }
            if($('#select_province').val() == 0 || $('#select_province').val() == '0'){
                e.preventDefault();
                alert('Please select area');
                return false;
            }
        });
        
        /*$('.reveal').hide();
        var address_name = $(':input[name="name"]').val();
        var fname = $(':input[name="first_name"]').val();
        var lname = $(':input[name="last_name"]').val();
        var addr = $(':input[name="address_one"]').val();
        var mob = $(':input[name="phone"]').val();

        $('.submitbutton').click(function(e){
            if(address_name == '' || fname=='' || lname=='' || addr=='' || mob=='') {
                console.log('all ampty');
                this.removeAttribute('data-open');
                e.preventDefault();
            }
            e.preventDefault();
        });*/


        // url with custom callbacks
        /*$('#modal-submit').foundation('reveal', 'open', {
         url: 'http://local.seafermart/user/add-new-address',
         success: function(data) {
         alert('modal data loaded');
         },
         error: function() {
         alert('failed loading modal');
         }
         });*/

        /*$('form').submit(function(){
            $('#modal-submit').foundation('reveal', 'open', {
                url: 'http://local.seafermart/user/add-new-address',
                success: function(data) {
                    alert('modal data loaded');
                },
                error: function() {
                    alert('failed loading modal');
                }
            });
        });*/

    </script>
@endsection