@extends('layouts.newmaster')

@section('title', App\Page::createTitle('Sign In'))

@section('content')
    <!-- Slider -->
    <div class="box-content" id="signIn">
        <div class="row">
            <div class="large-12 columns">
                <button class="button primary">SIGN IN</button><br><br>
            </div>
            <div class="large-6 columns">
                <div class="form-box">
                    <form class="border-dashed" method="post">
                        <h6 style="margin-bottom:40px">RETURNING CUSTOMER</h6>
                        <div class="container">
                            <div class="row">
                                <div class="medium-4 columns">
                                    <p style="font-size:14px">EMAIL ADDRESS<span style="color:red">*</span></p>
                                </div>
                                <div class="medium-8 columns">
                                    <input type="email" name="email" style="line-height:28px;height:28px;border:1.5px solid #808184;">
                                </div>
                            </div>
                            <div class="row">
                                <div class="medium-4 columns">
                                    <p style="font-size:14px">PASSWORD<span style="color:red">*</span></p>
                                </div>
                                <div class="medium-8 columns">
                                    <input type="password" name="password">
                                </div>
                            </div>
                            <div class="row">
                                <div class="medium-4 columns">
                                    <p>&nbsp;</p>
                                </div>
                                <div class="medium-8 columns">
                                    <p style="font-size:14px;line-height:28px;">
                                        <input type="checkbox" name="remember" style="height:28px;opacity:1">
                                        Keep me logged in
                                    </p>
                                    <p style="font-size: 14px;line-height: 14px;margin-bottom: 5px;">
                                        <a href="#" id="forgotPasswords" data-open="modal-forgotpassword" style="color:   #808080;font-size:12px;margin-left:15px;">Forgot Password?</a><br>
                                    </p>

                                </div>
                            </div>
                        </div>
                        <table>
                            <tbody>

                            <tr>
                                <td>&nbsp;</td>
                                {{ csrf_field() }}
                                <td><button class="button primary submit">SUBMIT</button></td>
                            </tr>
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
            <div class="large-6 columns">
                <div class="form-box">
                    <form class="border-dashed">
                        <h6>NEW CUSTOMER</h6>
                        <p>
                            First time shopping at Seafermart <br>
                            Enter your email to continue.
                        </p>
                        <table>
                            <tbody>
                            <tr>
                                <td>EMAIL ADDRESS <span>*</span></td>
                                <td><input type="text" name="guest_email"></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td class="right"><button class="button primary checkout" id="guest-checkout">GUEST CHECKOUT</button></td>
                            </tr>
                            </tbody>
                        </table>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function(){
            $('#guest-checkout').on('click', function(e){
                e.preventDefault();
                var new_email = encodeURIComponent($('input[name="guest_email"]').val());
                console.log(new_email);
                location.href='/checkout/guest-checkout/'+new_email;
            });
        });
    </script>
@endsection