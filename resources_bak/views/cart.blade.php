@extends('layouts.newmaster')

@section('customcss')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection

@section('content')
    <div class="row" style="margin-bottom: 35px;">
        <div class="small-12 columns">
            <span class="text-grey equal margin-top-10">
              <a class="text-grey" href="#"><b>CART</b></a> /
              <a class="text-grey" href="#"><b>ORDER SUMMARY</b></a>
            </span>
        </div>
    </div>
    <div id="godOfContent">
        <div class="box-content" id="cart">
            <div class="row">
                <div class="small-12 columns">
                    <h5 class="title title-margin">ORDER SUMMARY</h5>
                </div>
            </div>
            <div class="row">
                <div class="small-10 small-centered columns">
                    <b><span class="text-blue size-18">SEAFER</span><span class="text-red size-18"> GROCER</span></b>
                    <br><br>
                    <div class="seafer-grocer">
                        <table class="bg-transparent">
                            <tbody>
                            <tr>
                                <th>ITEMS</th>
                                <th>SKU</th>
                                <th>DESCRIPTION</th>
                                <th width="15%">QUANTITY</th>
                                <th>UNIT</th>
                                <th>UNIT PRICE</th>
                                <th>SUB TOTAL</th>
                                <th>&nbsp;</th>
                            </tr>
                            {{--@foreach (Carte::whereUserId(auth()->user()->id)->get() as $cartItem)--}}
                            @foreach( $carts as $key=>$cartItem)
                                <tr id = {{ $key }}>
                                    <td>
                                        <div class="item-box">
                                            <div class="item small" style="background-image: url('{{ $cartItem->image }}');background-size: cover;background-position: center; ">
                                                {{--<img src="{{ $cartItem->image }}">--}}
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <span class="block">
                                            {{ $cartItem->sku }}
                                        </span>
                                    </td>
                                    <td>
                                        <span class="block">
                                            {{ $cartItem->name }}
                                            {{--<span class="action"><a class="text-grey" href="#">move to</a></span>
                                            <span class="icon"><img src="{!! asset(null) !!}assets/images/ico-love.png"></span>
                                            <span class="action"><a class="text-grey margin-left-30" href="#">delete</a></span>--}}
                                        </span>
                                    </td>
                                    <td width="15%">
                                        <div class="counter">
                                            <button class="minus decrease_cart_item_qty">-</button>
                                            <input type="text" class="total" value="{{ $cartItem->quantity }}">
                                            <input class="cart_id" type="hidden" value="{{ $cartItem->id }}" />
                                            <button class="plus increase_cart_item_qty">+</button>
                                        </div>
                                    </td>
                                    <td>
                                        {{ $cartItem->unit_metric }}
                                    </td>
                                    <td>{{ Money::display($cartItem->price_per_item) }}</td>
                                    <td class="item_total">{{ Money::display($cartItem->total()) }}</td>
                                    <td class="cpage">
                                        <button class="list_del_cart_item" style="color:red;font-size:20px"><strong>&times;</strong></button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div class="row">
                        <div class="small-7 columns">
                            <div class="form-box promotional">
                                <span>Promotional Code / Coupon</span>

                                @if($promo_applied == false)
                                {!! Form::open(['method'=>'POST', 'action'=>'GroceriesController@postPromoCode']) !!}
                                <input class="code" type="text" name="promo">
                                <button class="button btn-aneh use-promo-button">APPLY CODE</button>
                                {!! Form::close() !!}
                                @else
                                    {{-- <h4><strong>Promo {{ Session::get('is_promo') }} is applied</strong></h4> --}}
                                    @if($promo_detail->promo_type == 'percent')
                                    <h4><strong>Your {{ $promo_detail->code }} promo code has been claimed! A deduction of {{ number_format($promo_detail->discount_value, 0).' %' }} will be applied to your purchase!</strong></h4>
                                    @else
                                    <h4><strong>Your {{ Session::get('is_promo') }} promo code has been claimed! A deduction of {{ Money::display($promo_applied) }} will be applied to your purchase!</strong></h4>
                                    @endif
                                @endif
                            </div>
                            {{--<div class="form-box promotional">
                                <span>Want to donate? Insert your donation code here</span>
                                    {!! Form::open(['method'=>'POST', 'action'=>'GroceriesController@postDonation']) !!}
                                    <input class="code" type="text" name="donation">
                                    <button class="button btn-aneh use-donation-button">Donate</button>
                                    {!! Form::close() !!}
                            </div>--}}
                        </div>
                        <div class="small-5 columns">
                            <table class="table-total bg-transparent">
                                <tbody>
                                <tr>
                                    <input type="hidden" name="base_total" value="{{ Carte::total() }}"/>
                                    <td>SUB TOTAL</td>
                                    {{-- <td align="right" class="cashback-redeem">{{ Money::display(Session::get('is_redeem')) }}</td> --}}
                                    <td id="cart_total" align="right"><span>{{ Money::display(Carte::total()) }}</span></td>
                                    
                                </tr>
                                <tr>
                                    <td><span>CASHBACK</span></td>
                                    <td align="right" class="cashback-redeem text-red">({{ Money::display(Session::get('is_redeem')) }}) {{-- @if($is_redeem > 0) <a href="{{ url('cart/reset-redeem') }}" style="color:red;font-size:11px">reset</a>@endif --}}</td>
                                </tr>
                                <tr>
                                    <td class="vertical-align" align="right">
                                        <span>REDEEM POINTS <br> (CURRENT BALANCE: Rp <span id="current-cashback-balance">{{ Money::onlyNominal($my_points->nominal - $is_redeem) }}</span>)</span>
                                    </td>
                                    
                                    <td align="right" id='redeem-form'>
                                        @if(Session::has('is_redeem'))
                                        <a href="{{ url('cart/reset-claim') }}"><button class="button btn-primary" id="reset-cashback" style="margin-top: 15px;">RESET CLAIM</button></a>
                                        @else
                                        <form method='post' id='point-redemption' autocomplete="off">
                                        <input type="text" name="value" placeholder="Input amount" class="redeem-field" id="redeem-amount" style="height:33px;display: none;" autocomplete="off" >
                                        {{-- <input type="password" name="password" placeholder="Password" class="redeem-field" id="redeem-password" style="height:33px;display: none;"> --}}
                                        {{-- <input type="password" name="password" id="password_fake" class="hidden" autocomplete="off" style="display: none;"> --}}
                                        </form>
                                        <button class="button btn-primary" id="redeem" style="margin-top: 15px;">REDEEM</button>
                                        @endif
                                    </td>
                                    {{-- <td align="right"><td><a href="#" style="font-style: italic;color: #A9A9A9;">Edit</a><div style="padding-bottom:15px;"></div></td> --}}
                                    
                                </tr>
                                <tr>
                                    <td><span>PROMO CODE</span></td>
                                    <td id="cart_promo" style="color:#ff0000;" align="right">(Rp <span class="promo-holder">{{ $promo_applied == false ? '0' : Money::onlyNominal($promo_applied) }}</span>)</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>PPN (10%)</td>
                                    @if( (Carte::total() - $is_redeem - $promo_applied) < 0)
                                    <td id="cart_tax_of_total" align="right"><span>{{ Money::display(0) }}</span></td>
                                    @else
                                    <td id="cart_tax_of_total" align="right"><span>{{ Money::display((Carte::total() - $is_redeem - $promo_applied) /10) }}</span></td>
                                    @endif
                                </tr>
                                <tr>
                                    <td>TOTAL</td>
                                    @if($promo_applied == false)
                                        @if( ceil((Carte::total() - $is_redeem)/10) > 0 )

                                            @if( (Carte::total() - $is_redeem) + ceil((Carte::total() - $is_redeem) /10) > 0)
                                            <td id="cart_grand_total" align="right"><span>{{ Money::display( (Carte::total() - $is_redeem) + ((Carte::total() - $is_redeem) /10)) }}</span></td>
                                            @else
                                            <td id="cart_grand_total" align="right"><span>{{ Money::display(0) }}</span></td>
                                            @endif

                                        @else

                                            @if( (Carte::total() - $is_redeem) > 0)
                                            <td id="cart_grand_total" align="right"><span>{{ Money::display( (Carte::total() - $is_redeem) ) }}</span></td>
                                            @else
                                            <td id="cart_grand_total" align="right"><span>{{ Money::display(0) }}</span></td>
                                            @endif

                                        @endif
                                    @else
                                        @if( ceil((Carte::total() - $is_redeem - $promo_applied)/10) > 0 )

                                            @if( ((Carte::total() - $is_redeem - $promo_applied) + ceil((Carte::total() - $is_redeem - $promo_applied) /10))  > 0)
                                            <td id="cart_grand_total" align="right"><span>{{ Money::display( (Carte::total() - $is_redeem - $promo_applied) + ceil((Carte::total() - $is_redeem- $promo_applied) /10) ) }}</span></td>
                                            @else
                                            <td id="cart_grand_total" align="right"><span>{{ Money::display(0) }}</span></td>
                                            @endif

                                        @else

                                            @if( (Carte::total() - $is_redeem - $promo_applied)  > 0)
                                            <td id="cart_grand_total" align="right"><span>{{ Money::display( (Carte::total() - $is_redeem - $promo_applied) + ceil((Carte::total() - $is_redeem - $promo_applied) /10) ) }}</span></td>
                                            @else
                                            <td id="cart_grand_total" align="right"><span>{{ Money::display(0) }}</span></td>
                                            @endif

                                        @endif
                                    @endif
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <button class="button btn-payment" onclick="location='{{ url('checkout') }}'">
                                            PROCEED TO PAYMENT
                                        </button>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="dialog" title="" style="display:none">

    </div>
@endsection

@section('scripts')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        var checkRedeemAmount = function(){
            if($('#redeem-amount').val() == ''){
                return 0;
            } else {
                return parseFloat($('#redeem-amount').val());    
            }
            
        }

        var checkRedeemPassword = function(){
            return $('#redeem-password').val();
        }

        var checkSubtotal = function() {
            var subtotal = $('#cart_total').text().split('Rp ');
            subtotal = subtotal[1].replace('.','');
            return parseFloat(subtotal);
        }

        var checkPromo = function() {
            var promo = $('#cart_promo').text().split('Rp ');
            promo = promo[1].replace('.','');
            return parseFloat(promo);
        }

        var checkCurrentCashbackBalance = function() {
            var balance = $('#current-cashback-balance').text().replace('Rp ','');
            balance = $('#current-cashback-balance').text().replace('.','');
            return parseFloat(balance);
        }

        var calculateTax = function(a, b, c) {
            return Math.ceil(( (a - b - c) * 10)/100);
        }

        var calculateGrandTotal = function(total, tax, promo, cashback) {
            var grand = (total - cashback - promo) + tax ;
            if(grand < 0){
                grand = 0;
            }
            return grand;
        }

        var generateResetRedeem = function() {
            return '<a href="http://seafermart.co.id/cart/reset-redeem" style="color:red;font-size:11px">reset</a>';
        }

        $(document).ready(function(){
            var current_balance = checkCurrentCashbackBalance();
            console.log(current_balance);
            $('#redeem-amount').on('keyup', function(){
                var input_amount = 0;

                if( false === $.isNumeric($(this).val()) && $(this).val() !== '') {
                    showDialog('Warning', "This field must be numeric");
                    $(this).val("");
                }

                if($(this).val() == '') {
                    input_amount = 0;
                } else {
                    input_amount = parseFloat($(this).val());
                }

                if(input_amount > current_balance){
                    showDialog('Warning', "Insufficent Points.");
                    $(this).val("");
                    
                    $('.cashback-redeem').text('(Rp 0)');
                    return false;
                }

                if(input_amount >= checkSubtotal()) {

                    showDialog('Warning', "Your can only redeem your balance up to "+ (checkSubtotal() - 1) +".");
                    $(this).val("");

                    $('.cashback-redeem').text('(Rp 0)');
                    return false;
                }

                $('#cart_total').text("{{ Money::display(Carte::total()) }}");
                $('.cashback-redeem').text('(Rp '+ moneyFormat(input_amount) +')');
                $('#current-cashback-balance').text(( moneyFormat(current_balance - input_amount)) +') ');
                
                if( (checkSubtotal() - input_amount - checkPromo()) > 0 ) {
                    $('#cart_tax_of_total').text('Rp '+ moneyFormat(calculateTax( checkSubtotal(), input_amount, checkPromo() )));    
                } else {
                    $('#cart_tax_of_total').text('Rp 0');    
                }
                
                $('#cart_grand_total').text('Rp '+ moneyFormat(calculateGrandTotal( checkSubtotal(), calculateTax( checkSubtotal(), checkPromo(), input_amount ), checkPromo(), input_amount  )));
            });

            $('#redeem').click(function(e){
                if(checkRedeemAmount() != '' && checkRedeemAmount() != 0) {
                    $.ajax({
                        type:'post',
                        url: "{{ url('cart/redeem-points') }}",
                        data: {
                            value:checkRedeemAmount()
                        }
                    }).done(function(result){
                        if(result.status == 'OK') {
                            $('#cart_total').text("{{ Money::display(Carte::total()) }}");
                            $('.cashback-redeem').text('(Rp '+ moneyFormat(result.message) +')');
                            $('#current-cashback-balance').text((current_balance - result.message ));

                            if( (checkSubtotal() - result.message - checkPromo()) > 0 ) {
                                $('#cart_tax_of_total').text('Rp '+ calculateTax( checkSubtotal(), result.message, checkPromo() ));    
                            } else {
                                $('#cart_tax_of_total').text('Rp 0');    
                            }
                            $('#cart_grand_total').text('Rp '+ calculateGrandTotal( checkSubtotal(), calculateTax( checkSubtotal(), result.message, checkPromo() ), checkPromo(), result.message  ));
                            $('#redeem-form').html('<a href="{{ url('cart/reset-claim') }}"><button class="button btn-primary" id="reset-cashback" style="margin-top: 15px;">RESET CLAIM</button></a>');
                        } else {
                            showDialog('Warning', result.message);
                        }
                    });
                } else {
                    $('.redeem-field').slideDown('fast');
                    $(this).text('CLAIM');
                }
            });

            $('.use-promo-button').on('click', function(e){
                e.preventDefault();
                var form = $(this).closest("form");

                $.ajax({
                    type:'post',
                    url: form.attr('action'),
                    data: {
                        promo:form.children('input[name="promo"]').val()
                    }
                }).done(function(result){
                    var json = $.parseJSON(result);
                    if (json.status == 'fail') {
                        showDialog('Warning', json.message);
                    } else {
                        var detail = json.item;

                        console.log(detail);

                        //var prom = Object.values(json.item);

                        rprc(detail.promo_type, detail.discount_value);

                        alert("Promo code applied");

                        if(detail.promo_type == 'percent') {
                            $('.promotional').html('<span>Promotional Code/Coupon</span><h4><strong>Your '+form.children('input[name="promo"]').val()+' promo code has been claimed! A deduction of '+Math.round(detail.discount_value)+' % will be applied to your purchase!</strong></h4>');    
                        } else {
                            $('.promotional').html('<span>Promotional Code/Coupon</span><h4><strong>Your '+form.children('input[name="promo"]').val()+' promo code has been claimed! A deduction of Rp '+moneyFormat(detail.discount_value)+' will be applied to your purchase!</strong></h4>');    
                        }

                        

                    }

                })
            });

            $('.use-donation-button').on('click', function(e){
                e.preventDefault();
                var form = $(this).closest("form");

                $.ajax({
                    type:'post',
                    url: form.attr('action'),
                    data: {
                        donation:form.children('input[name="donation"]').val()
                    }
                }).done(function(result){
                    var json = $.parseJSON(result);
                    if (json.status == 'fail') {
                        showDialog('Warning', json.message);
                    } else {
                        var prom = Object.values(json.item);
                    }

                })
            });

            $('.use-referral-button').on('click', function(e){
                e.preventDefault();
                var form = $(this).closest("form");

                $.ajax({
                    type:'post',
                    url: form.attr('action'),
                    data: {
                        referral:form.children('input[name="referral"]').val()
                    }
                }).done(function(result){
                    var json = $.parseJSON(result);
                    if (json.status == 'fail') {
                        showDialog('Warning', json.message);
                    } else {
                        showDialog('Success', json.message);
                    }

                })
            });
        });

        $()

        var showDialog = function(title,message){
            $('#dialog').attr('title', title);
            $('#dialog').html('<p>'+message+'</p>');
            $('#dialog').dialog();
        }

        var rprc = function(type, value){
            var total = $('input[name="base_total"]').val();
            var promo_value = 0;
            if (type == 'percent'){
                promo_value = (total * value) / 100;
                $('.promo-holder').html(moneyFormat(promo_value));
            } else if(type == 'nominal') {
                promo_value = value;
                $('.promo-holder').html(moneyFormat(promo_value));
            }

            var prm = checkPromo();
            var subtot = checkSubtotal();
            var redeem = checkRedeemAmount();

            if (isNaN(redeem)){
                var redeem_txt = $('.cashback-redeem').text().split('Rp ');
                redeem_txt = redeem_txt[1].replace('.','');
                redeem_txt = redeem_txt.replace('(','');
                redeem_txt = redeem_txt.replace(')','');
                redeem =  parseFloat(redeem_txt);
            }

            $('#cart_total').text('Rp '+moneyFormat(subtot));

            if( (subtot - redeem - prm) > 0 ) {
                $('#cart_tax_of_total').text('Rp ' + moneyFormat(calculateTax( subtot, redeem, prm) ));    
            } else {
                $('#cart_tax_of_total').text('Rp '+moneyFormat(0));    
            }

            var ppn = Math.ceil((10*(subtot - redeem - prm))/100);

            if(ppn < 0)
            {
                var gt = (total - promo_value - redeem);    
            } else {
                var gt = (total - promo_value - redeem) + ppn;   
            }
            
            if (gt < 0) {
                gt = 0;
            }
            $('#cart_grand_total').html('Rp '+moneyFormat(gt));
        }

        var moneyFormat = function numberWithCommas(x) {
            var parts = x.toString().split(".");
            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            return parts.join(".");
        }

    </script>
@endsection