@extends('layouts.newmaster')

@section('title', App\Page::createTitle('Order Claim'))

{{-- @section('customcss')
    <link rel="stylesheet" type="text/css" href="{!! asset(null) !!}responsive/css/main.css">
@endsection --}}

@section('content')
    <div class="row">
        <div class="small-12 columns">
    <span class="text-grey equal margin-top-10">
      <a class="text-grey" href="{{ url('checkout') }}"><b>HOME</b></a> /
      <a class="text-grey" href="{{ url('return-policy') }}"><b>RETURNS & EXCHANGE POLICY</b></a> /
      <a class="text-grey" href="#"><b>FORM</b></a>
    </span>
        </div>
    </div>
    <section>
        <div class="row">
            <div class="small-12 columns">
                <h5 class="title title-margin">ORDER CLAIM FORM</h5>
            </div>
            <div class="medium-6 medium-offset-3">

                {!! Form::open(['method'=>'POST', 'action'=>'OrderClaimController@store', 'files' => true, 'id'=>'order-claim']) !!}
                <!-- <div class="check-claim">
                    <div class="row">
                        <div class="medium-10 medium-offset-2 columns">
                            <div class="medium-6 columns">
                                <div class="box">
                                    <input id="oke" type="radio" name="is_exchange_refund" value="exchange">
                                    <label for="oke" class="">RETURN &amp; EXCHANGE</label>
                                </div>
                            </div>
                            <div class="medium-6 columns">
                                <div class="box">
                                    <input id="oke1" type="radio" name="is_exchange_refund" value="refund">
                                    <label for="oke1" class="">REFUND</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->
                <div class="form-box">
                    <div class="row">
                        <div class="medium-5 columns">
                            <label for="middle-label" class="text-left">Claim Type<span class="red">*</span></label>
                        </div>
                        <div class="medium-7 columns">
                            <select class="short" name="is_exchange_refund" required style="width:100%;">
                                <option value="0">Please select type of claim.</option>
                                <option value="exchange">Return &amp; Exchange</option>
                                <!-- <option value="missing">Missing</option> -->
                                <option value="refund">Refund</option>
                            </select>
                            <!-- <input id="oke" type="radio" name="is_exchange_refund" value="exchange" style="opacity:1;margin:7px 0 16px;float:right;"> -->
                        </div>
                    </div>
                    <!-- <div class="row">
                        <div class="medium-5 columns">
                            <label for="middle-label" class="text-left">REFUND<span class="red">*</span></label>
                        </div>
                        <div class="medium-7 columns">
                            <input id="oke1" type="radio" name="is_exchange_refund" value="refund" style="opacity:1;margin:7px 0 16px;float:right;">
                        </div>
                    </div> -->
                    <div class="row">
                        <div class="medium-5 columns">
                            <label for="middle-label" class="text-left">FULL NAME<span class="red">*</span></label>
                        </div>
                        <div class="medium-7 columns">
                            <input type="text"  placeholder="type your full name here" name="name">
                        </div>
                    </div>
                    <div class="row">
                        <div class="medium-5 columns">
                            <label for="middle-label" class="text-left">EMAIL<span class="red">*</span></label>
                        </div>
                        <div class="medium-7 columns">
                            <input type="email"  placeholder="type your email here" name="email" style="height: 24px;padding: 0 .5rem;border: 1.5px solid #808184;">
                        </div>
                    </div>
                    <div class="row">
                        <div class="medium-5 columns">
                            <label for="middle-label" class="text-left">MOBILE NO.<span class="red">*</span></label>
                        </div>
                        <div class="medium-7 columns">
                            <input type="text"  placeholder="type your mobile number here" name="mobile">
                        </div>
                    </div>
                    <div class="row">
                        <div class="medium-5 columns">
                            <label for="middle-label" class="text-left">ORDER ID<span class="red">*</span></label>
                        </div>
                        <div class="medium-7 columns">
                            <input type="text"  placeholder="type your order id/number here" name="order_id">
                        </div>
                    </div>
                    <div class="row">
                        <div class="medium-5 columns">
                            <label for="middle-label" class="text-left">PRODUCT SKU<span class="red">*</span></label>
                        </div>
                        <div class="medium-7 columns">
                            <input type="text"  placeholder="type product sku here" name="sku">
                        </div>
                    </div>
                    <div class="row">
                        <div class="medium-5 columns">
                            <label for="middle-label" class="text-left">REASON OF CLAIM<span class="red">*</span></label>
                        </div>
                        <div class="medium-7 columns">
                            <select class="short" name="reason" required style="width:100%;">
                                <option value="0">Please select reason of claim.</option>
                                <option value="incorrect">Incorrect</option>
                                <!-- <option value="missing">Missing</option> -->
                                <option value="damaged">Damaged</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="small-5 columns">
                            <label for="middle-label" class="text-left">UPLOAD<span class="red">*</span></label>
                        </div>
                        <div class="small-7 columns" style="margin-bottom: -25px;">
                            <input id="uploadFile" placeholder="" disabled="disabled"  style="border:3px solid #ffffff;"/>
                            <div class="fileUpload btn btn-primary">
                                {{--<input id="uploadBtn" type="file" class="upload" name="file" required><img src="assets/images/ico-upload.png"></input>--}}
                                <div class="preview img-wrapper"></div>
                                <div class="file-upload-wrapper">
                                    <input type="file" name="file" class="file-upload-native" accept="image/*"/>
                                    <input type="text" disabled class="file-upload-text btn-upload" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <p class="b2">
                        <center>
                            We reserve the right to investigate the legitimacy of complains and deny the ones that are being submitted to intentionally abuse SEAFERMART’s Refunds & Returns Policy scheme.
                        </center>
                        </p>
                    </div>
                    <div class="row">
                        <div class="small-4 columns">
                            &nbsp;
                        </div>
                        <div class="small-8 columns">
                            <button id="button-submit" class="button primary btn-aneh">SUBMIT</button>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </section>
@endsection

@section('scripts')
    <script>

        $(document).ready(function(){

            var validate = function(){
                if(!$('select[name="is_exchange_refund"]').val() || $('select[name="is_exchange_refund"]').val() == null || $('select[name="is_exchange_refund"]').val() == '0') {
                    alert("Please select your claim type: -Refund - Return & Exchange");
                    return false;
                }

                if($('input[name="name"]').val() == "") {
                    alert("Name should not be empty");
                    return false;
                }

                if($('input[type="email"]').val() == "") {
                    console.log('email value:');
                    console.log($('input[name="email"]').val());
                    alert("Email field should not be empty");
                    return false;
                }

                var mobile_value = $('input[name="mobile"]').val();
                if( mobile_value == "" && ! $.isNumeric(mobile_value)) {
                    alert("Mobile number should not be empty and should be number only");
                    return false;
                }

                if($('input[name="order_id"]').val() == "") {
                    alert("Order number should not be empty");
                    return false;
                }

                if($('input[name="sku"]').val() == "") {
                    alert("SKU should not be empty");
                    return false;
                }

                if($('input[name="reason"]').val() == "") {
                    alert("Reason should not be empty");
                    return false;
                }

                if($('input[name="file"]').val() == "") {
                    alert("Please upload a picture of the incorrect or damaged item.");
                    return false;
                }
                if(!$('select[name="reason"]').val() || $('select[name="reason"]').val() == null || $('select[name="reason"]').val() == '0'){
                    alert("Please select reason of claim");
                    return false;
                }
                return true;
            }

            $('#order-claim').submit(function(e){

                var validation_status = validate();

                if(validation_status == true) {
                    return true;
                } else {
                    return false;
                }

            });

            $('input[name="mobile"]').keydown(function (e) {
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                     // Allow: Ctrl+A, Command+A
                    (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
                     // Allow: home, end, left, right, down, up
                    (e.keyCode >= 35 && e.keyCode <= 40)) {
                         // let it happen, don't do anything
                         return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });
        })
    </script>
@endsection