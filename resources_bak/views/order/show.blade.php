@extends('layouts.newmaster')

@section('content')

    <div id="godOfContent">
        <div class="box-content" id="checkout">
            <div class="row" style="margin-top:15px">
                <div class="small-12 columns">
            <span class="text-grey">
              <a class="text-grey" href="{{ url('user/my-account') }}"><b>MY ACCOUNT</b></a> /
              <a class="text-grey" href="{{ url('orders') }}"><b>MY ORDER</b></a> /
              <a class="text-grey" href="#"><b>MY ORDER DETAIL</b></a>
            </span>
                </div><br><br>
                <div class="row">
                    <div class="small-12 columns">
                        <h5 class="title title-margin">MY ORDER <br>
                            <span class="subtitle">DETAILS</span>
                        </h5>
                    </div>
                </div>
                <div class="row">
                    <div class="small-9 small-centered columns">
                        <div class="row">
                            <div class="medium-6 columns">
                                <div class="status">
                                    <style type="text/css">
                                    #checkout table tbody tr td {
                                        vertical-align: top;
                                        padding: 0 5px;
                                    }</style>
                                    <table width="100%">
                                        <tbody>
                                        <tr>
                                            <td width="50%">Order Date</td>
                                            <td>:</td>
                                            <td>{{ $order->created_at->format('d M Y, H:i') }}</td>
                                        </tr>
                                        <tr>
                                            <td>Order No</td>
                                            <td>:</td>
                                            <td>#{{ $order->id }}</td>
                                        </tr>
                                        <tr>
                                            <td>Order Status</td>
                                            <td>:</td>
                                            <td>{{ $order->status->description }}</td>
                                        </tr>
                                        <tr>
                                            <td>Shipping Service</td>
                                            <td>:</td>
                                            <td>{{ ucfirst($order->deliveries->delivery_type) }}</td>
                                        </tr>
                                        <tr>
                                            <td>Shipping Address</td>
                                            <td>:</td>
                                            <td>{{ $order->address_detail->address }}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            
                            @if ($order->payment()->where('user_confirmed', 0)->count() > 0)
                                <div class="medium-6 columns">
                                    <div style="padding-bottom:58px;"></div>
                                    <button type="button" class="button btn-aneh float-right confirm gplus kecil" style="padding:.6rem 1rem" id="confirmBtn" data-open="modal-confirm">CONFIRM PAYMENT</button>
                                </div>
                            @endif
                        </div>
                        <div class="table-detail-order">
                            <div class="title-catogory">
                                <b class="text-blue">SEAFER</b><b class="text-red">GROCER</b>
                            </div>

                            <table>
                                <tbody>
                                <tr class="bg-transparent">
                                    <th>Product</th>
                                    <th>&nbsp;</th>
                                    <th>SKU</th>
                                    <th>Quantity</th>
                                    <th>Unit</th>
                                    <th>Unit Price</th>
                                    <th>Total Price</th>
                                </tr>
                                @foreach ($order->groceries as $orderGroceries)

                                    <tr>
                                        @if($orderGroceries->groceries->images->count() != 0)
                                        <td><img src="{{ asset('contents/'.$orderGroceries->groceries->images->first()->file_name) }}" width="128" height="85"></td>
                                        @else
                                        <td><img src="{{ asset('assets/images/img-empty.png') }}" width="128" height="85"></td>
                                        @endif
                                        <td>{{ $orderGroceries->groceries->name }}</td>
                                        <td>{{ $orderGroceries->groceries->sku ? $orderGroceries->groceries->sku : 'No SKU added' }}</td>
                                        <td>{{ $orderGroceries->qty }}</td>
                                        <td></td>
                                        <td>{{ Money::display($orderGroceries->price) }}</td>
                                        <td>{{ Money::display($orderGroceries->price * $orderGroceries->qty) }}</td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>

                        <div class="small-4 small-offset-8 columns">
                            <div class="table-total">
                                <table>
                                    <tbody>
                                    <tr>
                                        <td>Subtotal: </td>
                                        <td align="right"><span>{{ Money::display($order->nett_total) }}</span></td>
                                    </tr>
                                    <tr>
                                        <td>Cashback Redeem: </td>
                                        <td align="right"><span class="text-red" style="margin-right:-5px">({{ Money::display($order->redeem) }})</span></td>
                                    </tr>
                                    <tr>
                                        <td>Promo:</td>
                                        <td align="right"><span style="color:#ff0000;margin-right:-5px">({{ Money::display($order->promo_value) }})</span></td>
                                    </tr>
                                    <tr>
                                        <td>PPN (10%):</td>
                                        <td align="right"><span>{{ Money::display($order->tax) }}</span></td>
                                    </tr>
                                    <tr>
                                        <td>Shipping:</td>
                                        <td align="right"><span>{{ Money::display($order->shipping_fee) }}</span></td>
                                    </tr>
                                    <tr>
                                        <td>Total:</td>
                                        @if( ($order->nett_total - $order->redeem + $order->tax - $order->promo_value) < 0)
                                        <td align="right"><span>{{ Money::display($order->shipping_fee) }}</span></td>
                                        @else
                                        <td align="right"><span>{{ Money::display($order->nett_total - $order->redeem + $order->shipping_fee + $order->tax - $order->promo_value) }}</span></td>
                                        @endif
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="small-4 small-offset-8 columns">
                            <div class="table-total">
                                <table>
                                    <tbody>
                                    <tr>
                                        <td>Unique Code:</td>
                                        <td align="right"><span>{{ Money::display($smallAdditionalFee) }}</span></td>
                                    </tr>
                                    <tr>
                                        <td>GRAND TOTAL:</td>
                                        <td align="right"><span>{{ Money::display($order->grand_total) }}</span></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"><br>
                                            <a href="{{ url('orders/'.$order->id.'/download') }}" class="text-grey"><img src="{{ asset(null) }}assets/images/ico-pdf.png"> Download your invoice as PDF file</a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="reveal" id="modal-confirm" data-reveal>
            <div class="modal-box" id="modalPayment">
                <div class="form-box">
                    <h3>Payment Confirmation for Order #{{ $order->id }}</h3>
                    <form class="border-dashed" method="post" action="{{ url('order/payment/transfer-confirmation') }}" enctype="multipart/form-data">
                        <div class="row">
                            <div class="small-4 columns">
                                <span>TRANSFERED TO</span>
                            </div>
                            <div class="small-8 columns">
                                <select name="transfer_dest_account">
                                    <option value="BCA: 2063232327">BCA: 2063232327</option>
                                    <option value="MANDIRI: 1220007586848">MANDIRI: 1220007586848</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="small-4 columns">
                                <span class="required">TRANSFERED AMOUNT</span>
                            </div>
                            <div class="small-8 columns"><input type="text" name="transfer_amount" required></div>
                        </div>
                        <div class="row">
                            <div class="small-4 columns">
                                <span class="required">DATE</span>
                            </div>
                            <div class="small-8 columns"><input type="text" id="datepicker" name="transfer_date" required></div>
                        </div>
                        <div class="row">
                            <div class="small-4 columns">
                                <span class="required">TIME</span>
                            </div>
                            <div class="small-8 columns">
                                <div class="row">
                                    <div class="small-6 columns">
                                        <div class="row">
                                            <div class="small-6 columns">
                                                <input type="text" name="transfer_time_minute" class="time" required placeholder="HH">
                                            </div>
                                            <div class="small-6 columns no-padding">
                                                <input type="text" name="transfer_time_second" class="time" required placeholder="MM">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="small-6 columns">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="small-4 columns">
                                <span class="required">FROM BANK</span>
                            </div>
                            <div class="small-8 columns"><input type="text" name="transfer_sender_bank_name" @if(null !== $bank_preference)value="{{ $bank_preference->name }}"@endif required></div>
                        </div>
                        <div class="row">
                            <div class="small-4 columns">
                                <span class="required">ACCOUNT NUMBER</span>
                            </div>
                            <div class="small-8 columns"><input type="text" name="transfer_sender_account_number" @if(null !== $bank_preference)value="{{ $bank_preference->account_number }}"@endif required></div>
                        </div>
                        <div class="row">
                            <div class="small-4 columns">
                                <span class="required">ACCOUNT NAME</span>
                            </div>
                            <div class="small-8 columns"><input type="text" name="transfer_sender_account_name" @if(null !== $bank_preference)value="{{ $bank_preference->account_name }}"@endif required></div>
                        </div>
                        <div class="row">
                            <div class="small-4 columns">
                                <span>NOTE</span>
                            </div>
                            <div class="small-8 columns"><input type="text" name="transfer_note"></div>
                        </div>
                        <div class="row">
                            <div class="small-4 columns">
                                <span>PROOF PAYMENT</span>
                            </div>
                            <div class="small-8 columns"><input type="file" name="file"></div>
                        </div>
                        <div class="row">
                            <div class="small-12 columns">
                                <div class="save-payment-box">
                                    <input type="checkbox" class="square" id="saveBank" name="set_bank_preference">
                                    <label for="saveBank">Save this bank account information for next payment</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="small-12 columns"><br>
                                <center> <button type="submit" class="button btn-aneh">CONFIRM PAYMENT</button></center>
                            </div>
                        </div>

                        <input type="hidden" name="order_number" value="{{ $order->id }}"/>
                        {{ csrf_field() }}
                    </form>
                </div>
            </div>
        </div>

@endsection

@section('scripts')
    <script>
        $(document).ready(function(){
            $('input[name="transfer_amount"]').mask("#.##0", {reverse: true});
            $('input[name="transfer_time_minute"]').mask('00');
            $('input[name="transfer_time_second"]').mask('00');
            var dateToday = new Date()
            $('#datepicker').datepicker({ 
                dateFormat: 'dd-mm-yy', 
                minDate: dateToday,
                onSelect: function(selectedDate) {
                var option = this.id == "from" ? "minDate" : "maxDate",
                    instance = $(this).data("datepicker"),
                    date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
                dates.not(this).datepicker("option", option, date);
                } 
            });
            $("input[name='transfer_sender_account_number']").keydown(function (e) {
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                     // Allow: Ctrl+A, Command+A
                    (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
                     // Allow: home, end, left, right, down, up
                    (e.keyCode >= 35 && e.keyCode <= 40)) {
                         // let it happen, don't do anything
                         return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });
            $('input[name="transfer_sender_bank_name"],input[name="transfer_sender_account_name"]').keydown(function (e) {
                e = e || window.event;
                var charCode = (typeof e.which == "undefined") ? e.keyCode : e.which;
                var charStr = String.fromCharCode(charCode);
                if (/\d/.test(charStr)) {
                    return false;
                }
            });
            $('input[name="transfer_time_minute"]').on('keydown keyup', function(e){
                if ($(this).val() > 23 
                    && e.keyCode != 46 // delete
                    && e.keyCode != 8 // backspace
                   ) {
                   e.preventDefault();
                   $(this).val(23);
                }
            });
            $('input[name="transfer_time_second"]').on('keydown keyup', function(e){
                var minute = $('input[name="transfer_time_minute"]').val();
                var limit = 59;
                if(minute > 23){
                    limit = 0;
                }
                if ($(this).val() > limit 
                    && e.keyCode != 46 // delete
                    && e.keyCode != 8 // backspace
                   ) {
                   var formattedNumber = ("0" + limit).slice(-2);
                   e.preventDefault();
                   $(this).val(formattedNumber);
                }
            });
        })
        
    </script>
@endsection