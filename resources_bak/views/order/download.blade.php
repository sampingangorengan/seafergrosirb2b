<!DOCTYPE html>
<html style="height: auto;"><head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>    Order | Seafermart</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="{{ asset('bower_components/AdminLTE/bootstrap/css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('bower_components/AdminLTE/dist/css/AdminLTE.min.css') }}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{ asset('bower_components/AdminLTE/dist/css/skins/_all-skins.min.css') }}">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<!-- ADD THE CLASS fixed TO GET A FIXED HEADER AND SIDEBAR LAYOUT -->
<!-- the fixed layout is not compatible with sidebar-mini -->
<body style="height: auto;">
<!-- Main content -->
<section class="content">


    <!-- Main content -->
    <section class="invoice">
        <!-- title row -->
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                    <i class="fa fa-globe"></i> {{ $order->getShippingAddress($order->user_address_id)->user->name }}
                    <small class="pull-right">Date: {{ LocalizedCarbon::parse($order->created_at)->format('d F Y H:i:s') }}</small>
                </h2>
            </div>
            <!-- /.col -->
        </div>
        <!-- info row -->
        <div class="row invoice-info">
            <div class="col-sm-4 invoice-col">
                From
                <address>
                    <strong>PT Seafermart Jaya Raya</strong><br>
                    Jalan Cumi Raya No. 3, Muara Baru, RT.20/RW.17, <br>
                    Penjaringan, Jakarta Utara, Kota Jkt Utara, <br>
                    Daerah Khusus Ibukota Jakarta 14440, <br>
                    Indonesia<br>
                </address>
            </div>
            <!-- /.col -->
            <div class="col-sm-4 invoice-col">
                To
                <address>
                    @if($order->user_id != 0)
                        @if($order->user->sex == 'f')
                        Mrs.&nbsp;
                        @elseif($order->user->sex == 'fs')
                        Ms.&nbsp;
                        @else 
                        Mr.&nbsp;
                        @endif
                    @else
                        @if($order->getShippingAddress($order->user_address_id)->user->sex == 'f')
                        Mrs.&nbsp;
                        @elseif($order->getShippingAddress($order->user_address_id)->user->sex == 'fs')
                        Ms.&nbsp;
                        @else 
                        Mr.&nbsp;
                        @endif
                    @endif

                    
                    
                    @if($order->getShippingAddress($order->user_address_id)->recipient != '')
                    <strong>{{ $order->getShippingAddress($order->user_address_id)->recipient }}</strong><br>
                    @else
                    <strong>{{ $order->getShippingAddress($order->user_address_id)->user->name }}</strong><br>
                    @endif

                    {{ $order->getShippingAddress($order->user_address_id)->address }},<br>

                    @if ($order->getShippingAddress($order->user_address_id)->city->city_name != '')
                    {{ $order->getShippingAddress($order->user_address_id)->area->name }},&nbsp;{{ $order->getShippingAddress($order->user_address_id)->area->city->city_name }},<br>
                    @else
                    {{ $order->getShippingAddress($order->user_address_id)->area->name }},&nbsp;-- City unavailable --<br>
                    @endif
                    @if (null != $order->getShippingAddress($order->user_address_id)->city->province)
                    {{ $order->getShippingAddress($order->user_address_id)->city->province->province_name_id }},<br>
                    @else
                    -- Province unavailable --<br>
                    @endif
                    Indonesia<br>
                    
                    {{ $order->getShippingAddress($order->user_address_id)->postal_code }}<br>
                    
                    Phone: +62{{ $order->getShippingAddress($order->user_address_id)->phone_number }}<br>
                    Email: {{ $order->getShippingAddress($order->user_address_id)->user->email }}
                </address>
            </div>
            <!-- /.col -->
            <div class="col-sm-4 invoice-col">
                <b>Order ID:</b> {{ $order->id }}<br>
                <b>Payment Due:</b> {{ LocalizedCarbon::parse($order->created_at)->addHour(24)->format('d F Y H:i:s') }}<br>
                <b>Account:</b> PT. Seafermart Jaya Raya – <strong>BCA 206 3232 327</strong>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <!-- Table row -->
        <div class="row">
            <div class="col-xs-12 table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Qty</th>
                        <th>Product</th>
                        <th>SKU</th>
                        <th>Description</th>
                        <th>Price per item</th>
                        <th>Subtotal</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($order->groceries as $grocery)
                        <tr>
                            <td>{{ $grocery->qty }}</td>
                            <td>{{ $grocery->groceries->name }}</td>
                            <td>{{ $grocery->groceries->sku ? $grocery->groceries->sku : 'No SKU added'  }}</td>
                            <td>{{ str_limit($grocery->groceries->description, 30) }}</td>
                            <td>{{ Money::display($grocery->price) }}</td>
                            <td>{{ Money::display($grocery->price * $grocery->qty) }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <div class="row">
            <!-- accepted payments column -->
            <div class="col-xs-6">
                <p class="lead">PAYMENT STATUS:</p>

                <h2>{{ $order->isPaid($order->id) == 1 ? 'PAID' : 'NOT PAID' }}</h2>

            </div>
            <!-- /.col -->
            <div class="col-xs-6">

                <div class="table-responsive">
                    <table class="table">
                        <tr>
                            <th style="width:50%">Subtotal:</th>
                            <td>{{ Money::display($order->nett_total) }}</td>
                        </tr>
                        <tr>
                            <th style="width:50%">Cashback Redeem:</th>
                            <td>{{ Money::display($order->redeem) }}</td>
                        </tr>
                        <tr>
                            <th style="width:50%">Promo:</th>
                            <td>{{ Money::display($order->promo_value) }}</td>
                        </tr>
                        <tr>
                            <th style="width:50%">PPN(10%):</th>
                            <td>{{ Money::display($order->tax) }}</td>
                        </tr>
                        <tr>
                            <th>Shipping:</th>
                            <td>{{ Money::display($order->shipping_fee) }}</td>
                        </tr>
                        <tr>
                            <th>Total:</th>
                            @if( ($order->nett_total - $order->redeem + $order->tax - $order->promo_value) < 0)
                            <td>{{ Money::display($order->shipping_fee) }}</td>
                            @else
                            <td>{{ Money::display($order->nett_total - $order->redeem + $order->shipping_fee + $order->tax - $order->promo_value) }}</td>
                            @endif
                        </tr>
                        <tr>
                            <th>Unique Code:</th>
                            <td>{{ Money::display($smallAdditionalFee) }}</td>
                        </tr>
                        <tr>
                            <th>GRAND TOTAL:</th>
                            <td>{{ Money::display($order->grand_total) }}</td>
                        </tr>
                        </tbody></table>
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

    </section>
    <!-- /.content -->
    <div class="clearfix"></div>

</section>
<!-- /.content -->

</body></html>