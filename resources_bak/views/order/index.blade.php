@extends('layouts.newmaster')

@section('customcss')
    <style>
        table tr th,table tr td{
            text-align:center;
        }
        .order-history-box table td:first-child, .order-history-box table th:first-child{
            margin-left:0;
        }
    </style>
@endsection

@section('content')
    <div class="row" style="margin-bottom:65px">
        <div class="small-12 columns">
            <span class="text-grey equal margin-top-10">
              <a class="text-grey" href="{{ url('/') }}"><b>HOME</b></a> /
              <a class="text-grey" href="{{ url('user/my-account') }}"><b>MY ACCOUNT </b></a> /
              <a class="text-grey" href="#"><b>MY ORDERS</b></a>
            </span>
        </div>
    </div>
    <div id="godOfContent">
        <div class="box-content" id="checkout">
            <div class="row">
                <div class="small-12 columns" style="text-align:center">
                    <h5 class="title title-margin" style="margin-bottom:0;">MY ORDERS </h5>
                    <h5 style="margin-bottom:50px;"><span class="subtitle">HISTORY</span></h5>
                </div>
            </div>
            <div class="small-9 small-centered columns">
                <div class="order-history-box">
                    <table>
                        <tbody>
                        <tr>
                            <th>Date</th>
                            <th>Order No.</th>
                            <th>Order Status</th>
                            <th>Payment Bank</th>
                            <th>Shipping Status</th>
                        </tr>
                        @foreach ($orders as $order)
                            <tr>
                                <td>{{ $order->created_at->format('d M Y, H:i') }}</td>
                                <td><a href="{{ url('orders/'.$order->id) }}">#{{ $order->id }}</a></td>
                                <td>{{ $order->status->description }}</td>

                                @if ($order->payment()->where('user_confirmed', 1)->count() > 0)
                                <td>{{ $order->payment()->where('user_confirmed', 1)->first()->transfer_dest_account }}</td>
                                @else
                                <td>Not Available</td>
                                @endif

                                @if ($order->deliveries)
                                <td style="text-transform:capitalize;">{{-- {{ ucfirst($order->deliveries->delivery_type) }} -  --}}{{ ucwords($order->deliveries->status)}}</td>
                                @else
                                <td style="text-transform:capitalize;">Not Available</td>
                                @endif

                                {{--<td>{!! var_dump($order->getPayment($order->id)) !!}</td>--}}
                                {{--<td>{{ ucfirst($order->deliveries->delivery_type.'-'.$order->deliveries->status) }}</td>--}}
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection