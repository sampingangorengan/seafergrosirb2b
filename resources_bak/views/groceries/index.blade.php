@extends('layouts.newmaster')

@section('customcss')
    <style>
        .add-to-cart-btn{
            width:80%;
            font-size:14px;
            height:42px;
        }
        .add-to-cart-btn:hover{
            background-color:#0368ff;
        }
        ul.pagination li.current{
            background: #0368ff;
            border-radius: 100px;
            padding:0;
            display:inline-block;
            width:40px;
            height:40px;
            font-weight: bold;

        }
        ul.pagination li.current a{
            line-height:32px;
            text-align:center;

        }
        .top-bar.filter-box {
            padding-bottom:10px;
        }
        .top-bar.filter-box span.filter-list{
            border:1px solid #808184;
            color: #808184;
        }
        .top-bar.filter-box ul.filter-list {
            background-color: white;
            border-bottom: 2px solid #808184;
            border-left: 2px solid #808184;
            border-right: 2px solid #808184;
        }
        .top-bar-custome ul.pagination li.current a{
            color:white;
        }
        .box-content{
            margin-bottom: 20px;
        }
        hr.blue {
            border:none !important;
        }
        .pagination li a{
            line-height:19px;
        }
        .item-box .item.large--2{
            overflow:hidden;
        }
    </style>
@endsection

@section('content')
    <!-- =================== -->
    <!-- CONTENT -->
    <!-- =================== -->


    <div id="godOfContent">
        <div class="box-content" id="productDetail">
            <div class="row"><br><br><br>
                <!-- <div class="small-12 columns"> -->
                <!-- <h6 class="title">PRODUCT PAGE</h6> -->
                <!-- <center><img class="hero" src="assets/images/img-grocer.png"></center> -->
                <!-- </div> -->

                <div class="small-11 small-centered columns">
                    <div class="top-bar filter-box top-bar-custome">
                        <div class="top-bar-left">
                            <ul class="dropdown menu" data-dropdown-menu>
                                <li><span class="result">{{ $count }} results</span></li>
                                <li>
                                    <span class="ico-down {{--glyphicon glyphicon-chevron-down--}} filter-list"><b>Sort by: </b></span>
                                    <ul class="menu vertical filter-list">
                                        <li>
                                            <div class="box">
                                                <input id="bestSeller" type="radio" class="square" name="a" value="best-seller" onclick="window.location='{{ Request::url() }}?{{ $query_string != '' ?  $query_string.'&' : '' }}orderBy=best-seller'" @if($orderBy == 'best-seller')checked="checked"@endif><label for="bestSeller">Best Sellers</label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="box">
                                                <input id="newIn" type="radio" class="square" name="a" value="latest"  onclick="window.location='{{ Request::url() }}?{{ $query_string != '' ?  $query_string.'&' : '' }}orderBy=latest-entry'" @if($orderBy == 'latest-entry')checked="checked"@endif><label for="newIn">Latest Entry</label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="box">
                                                <input id="highestPrice" type="radio" class="square" name="a" value="highest-price" onclick="window.location='{{ Request::url() }}?{{ $query_string != '' ?  $query_string.'&' : '' }}orderBy=highest-price'" @if($orderBy == 'highest-price')checked="checked"@endif><label for="highestPrice">Highest Price</label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="box">
                                                <input id="lowPrice" type="radio" class="square" name="a" value="low-price" onclick="window.location='{{ Request::url() }}?{{ $query_string != '' ?  $query_string.'&' : '' }}orderBy=lowest-price'" @if($orderBy == 'lowest-price')checked="checked"@endif><label for="lowPrice">Lowest Price</label>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>

                        <div class="top-bar-right" style="width:42%;text-align:right">
                            <div class="pagination-box">

                                @include('pagination.default', ['paginator' => $groceries])
                                {{--<ul class="pagination text-grey">
                                    <li><a href="#">per pages:</a></li>
                                    <li class="current"><a href="">1</a></li>
                                    <li><a href="">2</a></li>
                                    <li><a href="">3</a></li>
                                    <li><a href="">4</a></li>
                                    <li class="unavailable"><a href="">&hellip;</a></li>
                                    <li><a href="">View All</a></li>
                                </ul>--}}
                            </div>
                        </div>
                    </div>
                    <hr class="blue margin-top-0"></hr>
                </div>

                <div class="small-3 columns" style="float:left">
                    <ul class="side-nav list-style" >
                        @if(count(App\Models\Groceries\Category::get()) > 1)
                            @foreach( App\Models\Groceries\Category::orderBy('order')->get() as $key=>$category )
                                <li><a href="{{ url('groceries/get-all/'.$category->slug) }}" id="{{ $key }}">{{ strtoupper($category->name) }}</a>
                                @if($category->child)
                                    <ul id="sideDropdown{{ $key }}" style="display:none">
                                        @foreach($category->child as $child_category)
                                        <li style="list-style:none"><a href="{!! url('groceries/' . $child_category->slug) !!}">{{ $child_category->name}}</a></li>
                                        @endforeach
                                    </ul>
                                @endif
                            @endforeach
                        @else
                            <li><a href="{!! url('groceries/' . $category->slug) !!}" id="{{ strtoupper($category->name) }}">{{ strtoupper($category->name) }}</a>
                        @endif
                    </ul>
                </div>

                {{-- s: Groceries List --}}
                @if(count($groceries) > 0) 
                <?php $i = 0 ?>
                @foreach ($groceries as $item)

                @if($i == 0 || $i == 3 || $i == 6 || $i == 9 )

                @if($i == 0)
                <div class="small-7 columns" style="float:left">
                @endif
                    <div class="row margin-list left--13">
                @endif

                        <div class="small-4 columns" style="float:left">
                            <div class="item-box item-margin-bottom">
                                <div class="item large--2 left-7 {{ $item->discount_id != 0 ? 'sale' : '' }}">
                                    <a href="{{ url('groceries/'.$item->id.'/'.str_slug($item->name)) }}">
                                        <img src="{{ Groceries::getFirstImageUrl($item->id) }}">
                                    </a>
                                </div>
                                <a href="{{ url('groceries/'.$item->id.'/'.str_slug($item->name)) }}">
                                <div style="width:205px;height:45px;overflow:hidden">
                                    <span><b>{{ $item->name }}, {{ $item->value_per_package }}{{ $item->metric_id != 0 ? $item->metric->abbreviation : 'metric unknown' }}</b></span><br>
                                </div>
                                </a>

                                @if($item->discount_id != 0)
                                <span class="before-discount">{{ Money::display($item->price) }}</span>
                                    @if($item->discount->discount_type == 'percent')
                                        @if($item->price - (($item->price * $item->discount->discount_value)/100) >0)
                                        <span class="after-discount"> {{ Money::display( $item->price - (($item->price * $item->discount->discount_value)/100) )}}</span><br>
                                        @else
                                        <span class="after-discount"> {{ Money::display( 0 )}}</span><br>
                                        @endif
                                    @else
                                        @if( ($item->price - $item->discount->discount_value) > 0)
                                        <span class="after-discount"> {{ Money::display( $item->price - $item->discount->discount_value ) }}</span><br>
                                        @else
                                            <span class="after-discount"> {{ Money::display( 0 ) }}</span><br>
                                        @endif
                                    @endif
                                @else
                                <span class="after-discount"> {{ Money::display($item->price)}}</span><br>
                                @endif
                                <div class="procat control-cart-{{ $item->id }}">
                                    <?php
                                        $cart_items = Carte::contents();
                                        $is_there = false;
                                        $identifier = '';

                                        foreach($cart_items as $keys=>$prod){

                                            if($item->id == $prod->id){
                                                $is_there = true;
                                                $identifier = $keys;
                                                $cart_item = $prod;
                                                break;
                                            }
                                        }
                                    ?>

                                    <div class="atc-initiate">
                                        <input class="item_id" type="hidden" value="{{ $item->id }}" />
                                        <input class="item_sku" type="hidden" value="{{ $item->sku ? $item->sku : 'No SKU added' }}" />
                                        <input class="item_name" type="hidden" value="{{ $item->name }}" />
                                        @if($item->discount_id != 0)
                                            @if($item->discount->discount_type == 'percent')
                                                @if($item->price - (($item->price * $item->discount->discount_value)/100))
                                                <input class="item_price" type="hidden" value="{{ $item->price - (($item->price * $item->discount->discount_value)/100) }}" />
                                                @else
                                                <input class="item_price" type="hidden" value="0" />
                                                @endif
                                            @else
                                                @if($item->price - $item->discount->discount_value)
                                                <input class="item_price" type="hidden" value="{{ $item->price - $item->discount->discount_value }}" />
                                                @else
                                                <input class="item_price" type="hidden" value="0" />
                                                @endif
                                            @endif
                                        @else
                                            <input class="item_price" type="hidden" value="{{ $item->price }}" />
                                        @endif

                                        {{--@if($item->metric)
                                            <input class="item_metric" type="hidden" value="{{ $item->value_per_package.' '.$item->metric->abbreviation }}" />
                                        @else
                                            <input class="item_metric" type="hidden" value="{{ $item->value_per_package.' --metric not added--' }}" />
                                        @endif--}}
                                        <input class="item_image" type="hidden"  name="" value="{{ Groceries::getFirstImageUrl($item->id) }}">

                                    </div>

                                    

                                    @if($item->stock > 0)
                                        <button class="button add-to-cart-btn list_add_to_cart" >ADD TO CART</button>
                                    @else
                                        <button class="button add-to-cart-btn" style="color:white;background:#ff0000;" disabled>SOLD OUT</button>
                                    @endif
                                </div>
                            </div>
                        </div>

                @if($i == 2 || $i == 5 || $i == 8 || $i == 11 || $i == ($count-1))
                    </div>
                    <!-- Devider -->

                    @if($i == ($count-1))
                </div>
                    @endif


                @endif
                <?php $i = $i+1;?>
                @endforeach
                @else
                <div class="small-7 columns" style="float:left">
                    <h3 style="color:#0368ff;text-align:center;border:2px solid #0368ff;">No Results Found</h3>
                </div>
                @endif
                <div class="small-2 columns" style="float:right">
                    <div class="recently-viewed-box">
                        <div class="header">
                            RECENTLY VIEWED :
                        </div>
                        <div class="body"><nr><br>
                                <ul class="recomendation-box">

                                    @if (count($recent) > 0)

                                        @foreach($recent as $k=>$item)
                                            {{-- @if($k == 3)
                                            {!! dd($item->grocery);!!}
                                            @endif --}}
                                            @if ($item->grocery !== null && $item->grocery->brand != null)
                                            <li>
                                                <a href="{{ url('groceries/'.$item->grocery['id'].'/'.str_slug($item->grocery['name'])) }}">
                                                    <div class="item" style="overflow:hidden">
                                                        <img src="{{ Groceries::getFirstImageUrl($item->grocery['id']) }}">
                                                    </div>

                                                    <span class="text-grey"><b>{{ $item->grocery['name'] }}</b></span>
                                                    <span style="color:#A9A9A9;">{{ Money::display($item->grocery['price']) }}/{{ $item->grocery['value_per_package'] }}{{ $item->grocery['metric_id'] != 0 ? $item->grocery->metric->abbreviation : 'metric unknown' }}</span>
                                                </a>
                                            </li>
                                            @endif
                                        @endforeach
                                    @endif
                                </ul>
                        </div>
                    </div>
                </div>

                <div class="pagination-box margin-bottom float-right" style="margin-right:80px;">
                    @include('pagination.default', ['paginator' => $groceries])
                </div>
            </div>
        </div>
    </div>
@endsection