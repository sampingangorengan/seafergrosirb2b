@extends('layouts.newmaster')

@section('customcss')
    <link rel="stylesheet" type="text/css" href="{!! asset(null) !!}assets/css/plugin/jquery.fancybox.min.css">
    <style>
        .item-box .thumbnail-box ul li .item img{
            height:95px !important;
            width: auto !important;
        }
        .item.xlarge{
            overflow:hidden;
        }
        .item.xlarge img{
            height:398px;
            width: auto;
        }
    </style>
@endsection

@section('content')

    <!-- =================== -->
    <!-- CONTENT -->
    <!-- =================== -->

    <div class="row" style="margin-bottom: 35px;">
        <div class="small-12 columns">
            <span class="text-grey equal margin-top-10">
              <a class="text-grey" href="{{ url('/') }}"><b>HOME</b></a> /
              <a class="text-grey" href="#"><b>SEAFER GROCER </b></a>
            </span>
        </div>
    </div>
    <div id="godOfContent">
        <div class="box-content" id="productDetail">

            <div class="row">
                <div class="large-5 columns">
                    <div class="item-box">
                        <div class="item xlarge">
                            <a data-fancybox="gallery" href="{{ Groceries::getFirstImageUrl($item->id) }}" class="zoom">
                                <img src="{{ Groceries::getFirstImageUrl($item->id) }}" class="image-large">
                            </a>
                        </div>
                        <div class="thumbnail-box">
                            <ul>
                                @foreach ($item->images()->get() as $img)
                                    <li>
                                        <div class="item">
                                            <a data-fancybox="gallery" href="{{ asset('contents/'.$img->file_name) }}">
                                                <img src="{{ asset('contents/'.$img->file_name) }}" class="small-thumbnail">
                                            </a>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="large-7 columns">
                    <div class="detail">

                        <h6 class="subtitle large">{{ $item->name }}, {{ $item->value_per_package }}{{ $item->metric_id != 0 ? $item->metric->abbreviation : 'metric unknown' }}</h6>
                        <br>
                        @if($item->discount_id != 0)
                            <span class="price old-price semi-bold margin-right-30">{{ Money::display($item->price) }}</span>
                            @if($item->discount->discount_type == 'percent')
                                @if( $item->price - (($item->price * $item->discount->discount_value)/100) > 0 )
                                <span class="price text-red semi-bold">{{ Money::display( $item->price - (($item->price * $item->discount->discount_value)/100) ) }}</span>
                                @else
                                <span class="price text-red semi-bold">{{ Money::display( 0 ) }}</span>
                                @endif
                            @else
                                @if( $item->price - $item->discount->discount_value > 0 )
                                <span class="price text-red semi-bold">{{ Money::display( $item->price - $item->discount->discount_value ) }}</span>
                                @else
                                <span class="price text-red semi-bold">{{ Money::display( 0 ) }}</span>
                                @endif
                            @endif
                        @else
                            <span class="price text-red semi-bold">{{ Money::display($item->price) }}</span>
                        @endif

                        <div class="counter independent">
                            <!-- <button class="minus">-</button> -->
                            <input id="item_id" type="hidden" value="{{ $item->id }}" />
                            <input id="item_sku" type="hidden" value="{{ $item->sku ? $item->sku : 'No SKU added' }}" />
                            <input id="item_name" type="hidden" value="{{ $item->name }}" />
                            <input id="item_qty" type="hidden" class="totalx" name="" value="1">
                            @if($item->metric)
                                <input class="item_metric" type="hidden" value="{{ $item->value_per_package.' '.$item->metric->abbreviation }}" />
                            @else
                                <input class="item_metric" type="hidden" value="{{ $item->value_per_package.' --metric not added--' }}" />
                            @endif
                            @if($item->discount_id != 0)
                                @if($item->discount->discount_type == 'percent')
                                    @if($item->price - (($item->price * $item->discount->discount_value)/100))
                                        <input class="item_price" type="hidden" value="{{ $item->price - (($item->price * $item->discount->discount_value)/100) }}" />
                                    @else
                                        <input class="item_price" type="hidden" value="0" />
                                    @endif
                                @else
                                    @if($item->price - $item->discount->discount_value)
                                        <input class="item_price" type="hidden" value="{{ $item->price - $item->discount->discount_value }}" />
                                    @else
                                        <input class="item_price" type="hidden" value="0" />
                                    @endif
                                @endif
                            @else
                                <input class="item_price" type="hidden" value="{{ $item->price }}" />
                            @endif
                            <input class="item_image" type="hidden"  name="item_image" value="{{ Groceries::getFirstImageUrl($item->id) }}">
                            <!-- <button class="plus">+</button> -->
                        </div>
                        <div class="add-to-cart-box">

                            @if($item->stock > 0)
                            <div class="row">
                                <div class="large-6 columns" id="item_add_to_cart_wrap">
                                    {{--<span>RP 25.000,-</span><br><br>--}}
                                    <button class="button btn-aneh" id="item_add_to_cart">ADD TO CART</button>
                                </div>
                                <div class="large-6 columns"><br><br>
                                    <table>
                                        <tbody>
                                        <tr>
                                            {{--<td>
                                              <a href="#"><img src="{!! asset(null) !!}assets/images/ico-love-red.png" class="loveee"></a>
                                            </td>--}}
                                            {{--<td><a href="#"><img src="{!! asset(null) !!}assets/images/ico-share.png"></a></td>--}}
                                        </tr>
                                        {{--<tr><td><b class="text-blue float-right">112 loved</b></td><td>&nbsp;</td></tr>--}}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            @else
                            <div class="row">
                                <div class="large-6 columns">
                                    <button class="button btn-aneh" id="item_add_to_cart" disabled="" type="button" style="opacity:0.25;color:white;background:#ff0000;">SOLD OUT</button>
                                </div>
                            </div>
                            @endif
                        </div>
                        <div class="row">
                            <div class="small-12 columns">
                                <ul class="accordion" data-accordion data-multi-expand="true" data-allow-all-closed="true">
                                    <li class="accordion-item is-active" data-accordion-item>
                                        <a href="#" class="accordion-title no-border">Expiry Date</a>
                                        <div style="font-size:14px;text-align:left;" class="accordion-content no-border" data-tab-content>
                                            {{ strtoupper(LocalizedCarbon::parse($item->expiry_date)->format("d m Y")) }}
                                        </div>
                                    </li>
                                    <li class="accordion-item is-active" data-accordion-item>
                                        <a href="#" class="accordion-title no-border">Description</a>
                                        <div style="font-size:14px; text-align:left;" class="accordion-content no-border" data-tab-content>
                                            {!! nl2br($item->description) !!}
                                        </div>
                                    </li>
                                    <li class="accordion-item is-active" data-accordion-item>
                                        <a href="#" class="accordion-title no-border">Nutrition</a>

                                        <div style="font-size:14px;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;max-width: 669px;" class="accordion-content no-border" data-tab-content>
                                            @if($item->nutrients == 'No Data' || substr($item->nutrients, 0, 3) == '000')
                                            	   <p style="text-align:left;font-size:14px;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;max-width: 669px;">
                                                    Not Available
                                                </p>
                                            @else
                                                <p style="text-align:left;font-size:14px;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;max-width: 669px;">
                                                    {!! nl2br($item->nutrients) !!}
                                            		</p>
                                            @endif

                                        </div>
                                    </li>
                                    <li class="accordion-item is-active" data-accordion-item>
                                        <a href="#" class="accordion-title no-border">Ingredients</a>
                                        <div style="font-size:14px;text-align:left;" class="accordion-content no-border" data-tab-content>
                                            {!! nl2br($item->ingredients) !!}
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="small-12 columns">
                    <div class="recomendation-box top-line-blue bottom-line-blue">
                        <label>YOU MAY ALSO LIKE</label>
                        <div class="row">
                            @foreach ($recommendation as $item)
                                <div class="small-2 columns">
                                    <a href="{{ url('groceries/'.$item->id.'/'.str_slug($item->name)) }}">
                                    <div class="item" style="background-image: url({{ Groceries::getFirstImageUrl($item->id) }});background-size:cover;background-position:center;height:140px;">
                                            {{--<img src="{{ Groceries::getFirstImageUrl($item->id) }}" class="image-thumbnail">--}}
                                    </div>
                                    </a>
                                    <a href="{{ url('groceries/'.$item->id.'/'.str_slug($item->name)) }}">
                                        <span class="text-blue" style="padding-top: 10px;">{{ $item->name }}, {{ $item->value_per_package }}{{ $item->metric_id != 0 ? $item->metric->abbreviation : 'metric unknown' }}</span>
                                    </a>
                                    <span>{{ Money::display($item->price) }}</span>
                                </div>
                            @endforeach

                        </div>
                    </div>

                    <div class="recomendation-box top-line-blue bottom-line-blue">
                        <label>BRAND RELATED</label>
                        <div class="row">
                            @if($brand_related->count() > 0)
                            @foreach ($brand_related as $item)
                                <div class="small-2 columns">
                                    <a href="{{ url('groceries/'.$item->id.'/'.str_slug($item->name)) }}">
                                    <div class="item"  style="background-image: url({{ Groceries::getFirstImageUrl($item->id) }});background-size:cover;background-position:center;height:140px;">
                                            {{--<img src="{{ Groceries::getFirstImageUrl($item->id) }}" class="image-thumbnail">--}}
                                    </div>
                                    </a>
                                    <a href="{{ url('groceries/'.$item->id.'/'.str_slug($item->name)) }}">
                                        <span class="text-blue" style="padding-top: 10px;">{{ $item->name }}, {{ $item->value_per_package }}{{ $item->metric_id != 0 ? $item->metric->abbreviation : 'metric unknown' }}</span>
                                    </a>
                                    <span>RP {{ $item->price }}</span>
                                </div>
                            @endforeach
                            @else
                                <p>No more product from this brand to show</p>
                            @endif
                        </div>
                    </div>


                    <div class="recomendation-box">
                        <label>RECENTLY VIEWED</label>
                        <div class="row">

                            @if($recent->count() > 0)
                            @foreach ($recent as $item)
                                <div class="small-2 columns" style="float:left">
                                    <a href="{{ url('groceries/'.$item->grocery['id'].'/'.str_slug($item->grocery['name'])) }}">
                                    <div class="item" style="overflow:hidden;background-image: url({{ Groceries::getFirstImageUrl($item->grocery['id']) }});background-size:cover;background-position:center;height:140px;">
                                            {{--<img src="{{ Groceries::getFirstImageUrl($item->grocery['id']) }}" class="image-thumbnail">--}}
                                    </div>
                                    </a>
                                    <a href="{{ url('groceries/'.$item->grocery['id'].'/'.str_slug($item->grocery['name'])) }}">
                                        <span class="text-blue" style="padding-top: 10px;">{{ $item->grocery['name'] }}, {{ $item->grocery['value_per_package'] }}{{ $item->grocery['metric_id'] != 0 ? $item->grocery->metric->abbreviation : 'metric unknown' }}</span>
                                    </a>
                                    <span>{{ Money::display($item->grocery['price']) }}</span>
                                </div>
                            @endforeach
                            @else
                                <p>This is your first product visit</p>
                            @endif
                        </div>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="small-12">
                    <div class="flash-message">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                            @if(Session::has('alert-' . $msg))

                                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                            @endif
                        @endforeach
                    </div>
                    <div class="customer-and-reviews-box ">
                        <ul class="tabs" data-tabs id="example-tabs">
                            <li class="tabs-title is-active"><a href="#panel1" aria-selected="true">CUSTOMER Q&A</a></li>
                            <li class="tabs-title"><a href="#panel2">REVIEWS</a></li>
                        </ul>
                        <div class="tabs-content top-line-blue" data-tabs-content="example-tabs"><br>

                            {{-- Kirin Tab --}}
                            <div class="tabs-panel is-active" id="panel1">
                                <div class="row">
                                    <div class="large-6 columns">
                                        <div class="form-box kirin-container">
                                            {{--<input type="text" name="search-question" placeholder="Q: Have a question? Search for answers">--}}
                                            @if(auth()->check())
                                            {{--<a href="#">Can’t find what You are looking for?</a><br><br>--}}
                                            <button class="button btn-aneh" id="askKirin">ASK KIRIN</button>
                                            @else
                                            <button class="button btn-aneh openSIGNIN">ASK KIRIN</button>
                                            <p>sign in is required in order to add a question</p>
                                            @endif
                                        </div>
                                    </div>
                                    @if($questions->count()>0)
                                    <div class="large-6 columns">
                                        <div class="customer-qa-box question-container">
                                            <div class="orbit" role="region" aria-label="Favorite Space Pictures" data-orbit>
                                                <ul class="orbit-container question-container-content">

                                                    @if ($questions->count() > 0)

                                                        @foreach($questions as $key=>$question)
                                                            @if($key == 0 || $key == 4 || $key == 8 || $key == 12)
                                                                <li class="{{ $key == 0 ? 'is-active' : '' }} orbit-slide">
                                                                    <div class="customer-qa-box">
                                                                        @endif
                                                                        <div class="qa-box">
                                                                            <table>
                                                                                <tr>
                                                                                    <td><span class="text-blue">Q :</span></td>
                                                                                    <td><span class="text-blue">{{ $question->question }}</span></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>A :</td>
                                                                                    <td>{{ $question->answer ? $question->answer : 'The answer will be updated soon' }}</td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>

                                                                        @if($key == 3 || $key == 7 || $key == 11 || $key == 12)
                                                                    </div>
                                                                </li>
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                    {{--<li class="orbit-slide">
                                                        <div class="customer-qa-box">
                                                            <div class="qa-box">
                                                                <table>
                                                                    <tr>
                                                                        <td><span class="text-blue">Q :</span></td>
                                                                        <td><span class="text-blue">Is this spicy?</span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>A :</td>
                                                                        <td>Yes.</td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                            <div class="qa-box">
                                                                <table>
                                                                    <tr>
                                                                        <td><span class="text-blue">Q :</span></td>
                                                                        <td><span class="text-blue">Is this spicy?</span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>A :</td>
                                                                        <td>Yes.</td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                            <div class="qa-box">
                                                                <table>
                                                                    <tr>
                                                                        <td><span class="text-blue">Q :</span></td>
                                                                        <td><span class="text-blue">Is this spicy?</span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>A :</td>
                                                                        <td>Yes.</td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                            <div class="qa-box">
                                                                <table>
                                                                    <tr>
                                                                        <td><span class="text-blue">Q :</span></td>
                                                                        <td><span class="text-blue">Is this spicy?</span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>A :</td>
                                                                        <td>Yes.</td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li class="orbit-slide">
                                                        <div class="customer-qa-box">
                                                            <div class="qa-box">
                                                                <table>
                                                                    <tr>
                                                                        <td><span class="text-blue">Q :</span></td>
                                                                        <td><span class="text-blue">Is this spicy?</span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>A :</td>
                                                                        <td>Yes.</td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                            <div class="qa-box">
                                                                <table>
                                                                    <tr>
                                                                        <td><span class="text-blue">Q :</span></td>
                                                                        <td><span class="text-blue">Is this spicy?</span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>A :</td>
                                                                        <td>Yes.</td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                            <div class="qa-box">
                                                                <table>
                                                                    <tr>
                                                                        <td><span class="text-blue">Q :</span></td>
                                                                        <td><span class="text-blue">Is this spicy?</span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>A :</td>
                                                                        <td>Yes.</td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                            <div class="qa-box">
                                                                <table>
                                                                    <tr>
                                                                        <td><span class="text-blue">Q :</span></td>
                                                                        <td><span class="text-blue">Is this spicy?</span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>A :</td>
                                                                        <td>Yes.</td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li class="orbit-slide">
                                                        <div class="customer-qa-box">
                                                            <div class="qa-box">
                                                                <table>
                                                                    <tr>
                                                                        <td><span class="text-blue">Q :</span></td>
                                                                        <td><span class="text-blue">Is this spicy?</span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>A :</td>
                                                                        <td>Yes.</td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                            <div class="qa-box">
                                                                <table>
                                                                    <tr>
                                                                        <td><span class="text-blue">Q :</span></td>
                                                                        <td><span class="text-blue">Is this spicy?</span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>A :</td>
                                                                        <td>Yes.</td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                            <div class="qa-box">
                                                                <table>
                                                                    <tr>
                                                                        <td><span class="text-blue">Q :</span></td>
                                                                        <td><span class="text-blue">Is this spicy?</span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>A :</td>
                                                                        <td>Yes.</td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                            <div class="qa-box">
                                                                <table>
                                                                    <tr>
                                                                        <td><span class="text-blue">Q :</span></td>
                                                                        <td><span class="text-blue">Is this spicy?</span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>A :</td>
                                                                        <td>Yes.</td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </li>--}}



                                                </ul>
                                                <ul class="orbit-container small">
                                                    <button class="orbit-previous"><span class="show-for-sr"></span><img src="{!! asset(null) !!}assets/images/ico-arrows-L.png"></button>
                                                    <button class="orbit-next"><span class="show-for-sr"></span><img src="{!! asset(null) !!}assets/images/ico-arrows-R.png"></button>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>

                            {{-- Review Tab --}}
                            <div class="tabs-panel" id="panel2">
                                <div class="row">
                                     {{-- Review Statistics --}}
                                    <div class="large-6 columns">
                                        <ul class="rating">
                                            @if($reviews->count() >0)
                                            @for($i = 0; $i < $reviews->avg('rate'); $i++)
                                            <li><img src="{!! asset(null) !!}assets/images/ico-star-fill-color.png"></li>
                                            @endfor
                                            @else
                                            <li><img src="{!! asset(null) !!}assets/images/ico-star-border.png"></li>
                                            <li><img src="{!! asset(null) !!}assets/images/ico-star-border.png"></li>
                                            <li><img src="{!! asset(null) !!}assets/images/ico-star-border.png"></li>
                                            <li><img src="{!! asset(null) !!}assets/images/ico-star-border.png"></li>
                                            <li><img src="{!! asset(null) !!}assets/images/ico-star-border.png"></li>
                                            @endif
                                            {{--<li><img src="{!! asset(null) !!}assets/images/ico-star-fill-color.png"></li>
                                            <li><img src="{!! asset(null) !!}assets/images/ico-star-fill-color.png"></li>
                                            <li><img src="{!! asset(null) !!}assets/images/ico-star-fill-color.png"></li>
                                            <li><img src="{!! asset(null) !!}assets/images/ico-star-border.png"></li>--}}
                                            {{--<li><span class="persen">108%</span></li>--}}
                                        </ul>
                                        @if($reviews->count() >0)
                                        <span>{{ $reviews->avg('rate') }} out of 5 stars</span><br><br>
                                        @else
                                            <span>This product haven't been rated</span><br><br>
                                        @endif
                                        <div class="chart-box">
                                            <table class="chart">
                                                <tbody>
                                                <tr>
                                                    <td>
                                                        <p class="percentage">5 Stars</p>
                                                    </td>
                                                    <td class="chart">
                                                        <div class="barchart">
                                                            <span class="meter" style="width: {{ $reviews->count() > 0 ? ($five_star/$reviews->count())*100 : 0 }}%"></span>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <p class="percentage">{{ $reviews->count() > 0 ? ($five_star/$reviews->count())*100 : 0 }}%</p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p class="percentage">4 Stars</p>
                                                    </td>
                                                    <td class="chart">
                                                        <div class="barchart">
                                                            <span class="meter" style="width: {{ $reviews->count() > 0 ? ($four_star/$reviews->count())*100 : 0 }}%"></span>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <p class="percentage">{{ $reviews->count() > 0 ? ($four_star/$reviews->count())*100 : 0 }}%</p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p class="percentage">3 Stars</p>
                                                    </td>
                                                    <td class="chart">
                                                        <div class="barchart">
                                                            <span class="meter" style="width: {{ $reviews->count() > 0 ? ($three_star/$reviews->count())*100 : 0 }}%"></span>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <p class="percentage">{{ $reviews->count() > 0 ? ($three_star/$reviews->count())*100 : 0 }}%</p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p class="percentage">2 Stars</p>
                                                    </td>
                                                    <td class="chart">
                                                        <div class="barchart">
                                                            <span class="meter" style="width: {{ $reviews->count() > 0 ? ($two_star/$reviews->count())*100 : 0 }}%"></span>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <p class="percentage">{{ $reviews->count() > 0 ? ($two_star/$reviews->count())*100 : 0 }}%</p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p class="percentage">1 Star</p>
                                                    </td>
                                                    <td class="chart">
                                                        <div class="barchart">
                                                            <span class="meter" style="width: {{ $reviews->count() > 0 ? ($one_star/$reviews->count())*100 : 0 }}%"></span>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <p class="percentage">{{ $reviews->count() > 0 ? ($one_star/$reviews->count())*100 : 0 }}%</p>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                    {{-- Review List --}}
                                    <div class="large-6 columns">
                                        <div class="customer-qa-box" style="height:400px; width: 500px; overflow:hidden;">
                                            <div id="owl-example" class="owl-carousel">
                                                @if($reviews->count() > 0)
                                                    @foreach($reviews as $review)
                                                <div class="owl-slide">
                                                    <div class="customer-qa-box">
                                                        <div class="qa-box">
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <div class="rating-box">
                                                                            <ul class="rating">
                                                                                @for($i = 0; $i < $review->rate; $i++)
                                                                                <li><img src="{!! asset(null) !!}assets/images/ico-star-fill-color.png"></li>
                                                                                @endfor
                                                                            </ul>
                                                                        </div>
                                                                    </td>
                                                                    <td><b>{{ $review->title }}</b></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2"><span class="font-11">By {{ $review->user->name }} on {{ LocalizedCarbon::parse($review->created_at)->format('F, d Y') }}</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2"><span class="font-11">{{ $review->review }}</span></td>
                                                                </tr>
                                                            </table>
                                                        </div>



                                                    </div>
                                                </div>
                                                    @endforeach
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('scripts')
    <script src="{!! asset(null) !!}assets/js/jquery.fancybox.min.js"></script>
    <script>

        var closeKirinBox = function(){
            $('#askKirinForm').remove();
            $('#askKirin').show()
        }

        var drawKirin = function(html){

            $('.question-container ul.question-container-content').html(html);
        }

        $(document).ready(function(){
            /*$('.zoom').zoom();*/

            $('.small-thumbnail').on('click', function(){
/*                alert($(this).attr('src'));*/

                $('.item.xlarge img').attr('src', $(this).attr('src'));
            });
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('input[name="search-question"]').keyup(function(){
                var itid = $('#item_id').val();
                $.ajax({
                    type:'post',
                    url: url('kirin/get_questions/'+itid),
                    data: {
                        search:$('input[name="search-question"]').val(),
                    }
                }).done(function(result){
                    drawKirin(result);
                });
            });

            @if(auth()->check())
            var idf = "{{ auth()->user()->id }}";
            $.ajax({
                type:'post',
                url: url('user/make-history'),
                data: {
                    identifier: idf,
                    type:'uid',
                    gid:$('#item_id').val()
                }
            });

            $('.kirin-container').on('click', '.submit-kirin', function(e){

                if($('textarea[name="posttokirin"]').val() == ''){
                    e.preventDefault();
                    alert('Please type your question on the text area provided.');
                    return false;
                }

                var itid = $('#item_id').val();
                $.ajax({
                    type:'post',
                    url: url('groceries/question/'+itid),
                    data: {
                        groceries_id: parseInt(itid),
                        question:$('textarea[name="posttokirin"]').val()
                    }
                }).done(function(result){
                    drawKirin(result);
                    location.reload();
                });
                e.preventDefault();
            });
            @else
            var idf = "{{ Session::getId() }}";
            $.ajax({
                type:'post',
                url: url('user/make-history'),
                data: {
                    identifier: idf,
                    type:'ss',
                    gid:$('#item_id').val()
                }
            });
            @endif

        });
    </script>
@endsection