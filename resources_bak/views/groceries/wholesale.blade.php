@extends('layouts.newmaster')

@section('title', App\Page::createTitle('Add Bank Account'))

@section('customcss')
    <style>
        .btn-aneh:hover {
            background-color:#0368ff;
        }
    </style>
@endsection

@section('content')

    {{-- <div class="row" style="margin-bottom:35px">
        <div class="small-12 columns">
            <span class="text-grey equal margin-top-10">
                <a class="text-grey" href="{{ url('/') }}"><b>HOME</b></a> /
                <a class="text-grey" href="{{ url('user/my-account') }}"><b>MY ACCOUNT </b></a> /
                <a class="text-grey" href="{{ url('user/my-bank-account') }}"><b>MY BANK ACCOUNT</b></a> /
                <a class="text-grey" href="#"><b>ADD BANK ACCOUNT</b></a>
            </span>
        </div>
    </div> --}}
    <section>
        <div class="row">
            <div class="small-12 columns">
                <h5 class="title title-margin">Wholesale Form</h5>
            </div>
            <div class="medium-6 medium-offset-3">
                <form method="post">
                <div class="form-box">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="medium-5 columns">
                            <label for="middle-label" class="text-left">PIC NAME<span class="red">*</span></label>
                        </div>
                        <div class="medium-7 columns">
                            <input type="text"  placeholder="" name="pic_name" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="medium-5 columns">
                            <label for="middle-label" class="text-left">PIC EMAIL ADDRESS<span class="red">*</span></label>
                        </div>
                        <div class="medium-7 columns">
                            <input type="text"  placeholder="" name="pic_email" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="medium-5 columns">
                            <label for="middle-label" class="text-left">PRODUCT<span class="red">*</span></label>
                        </div>
                        <div class="medium-7 columns">
                            <input type="text"  placeholder="" name="products" required>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="medium-5 columns">
                            <label for="middle-label" class="text-left">VOLUME (in kg)</label>
                        </div>
                        <div class="medium-7 columns">
                            <input type="text"  placeholder="" name="branch">
                        </div>
                    </div>
                    <div class="row">
                        <div class="medium-5 columns">
                            <label for="middle-label" class="text-left">CUSTOMER TYPE</label>
                        </div>
                        <div class="medium-7 columns">
                            <select class="short" name="bank_name" required>
                                <option value="reseller">Reseller</option>
                                <option value="distributor">Distributor</option>
                                <option value="hotel">Hotel</option>
                                <option value="restaurant">Restaurant</option>
                                <option value="cafe">Cafe</option>
                                <option value="catering">Catering</option>
                                <option value="supermarket">Supermarket</option>
                                <option value="traditional_retailer">Traditional Retailer</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="medium-5 columns">
                            <label for="middle-label" class="text-left">OTHER COMMENT<span class="red">*</span></label>
                        </div>
                        <div class="medium-7 columns">
                            <textarea rows="3" name="comments"></textarea>  
                        </div>
                    </div>
                    <div class="row">
                        <div class="medium-offset-9 columns">
                            <button style="margin-left: 30px;" data-open="modal-submit" class="button btn-action blue space-3">SAVE</button>
                        </div>
                    </div>
                </div>
                </form>
            </div>

        </div>
    </section>

@endsection