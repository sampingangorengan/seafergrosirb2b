@extends('layouts.newmaster')

@section('content')

    <div id="godOfContent">
        <div class="box-content" id="productDetail">
            <div class="row">
                <div class="small-12 columns"><br>
                    <span class="text-grey margin-bottom-30">
            <a class="text-grey margin-right-5" href="index.html"><b>HOME</b></a> /
            <a class="text-grey margin-left-30" href="#"><b>SEAFER GROCER</b></a> /
            <a class="text-grey margin-left-30" href="#"><b>SEAFOOD</b></a> /
            <a class="text-grey margin-left-30" href="#"><b>FISH</b></a>
          </span>
                </div>
            </div>
            <div class="row">
                <div class="large-5 columns">
                    <div class="item-box">
                        <div class="item xlarge">
                            <img src="{{ Groceries::getFirstImageUrl($item->id) }}">
                        </div>
                        <div class="thumbnail-box">
                            <ul>
                                @foreach ($item->images()->get() as $img)
                                    <li>
                                        <div class="item">
                                            <img src="{{ asset('contents/'.$img->file_name) }}">
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="large-7 columns">
                    <div class="detail">
                        <h6 class="subtitle large">{{ $item->name }}</h6>
                        <span class="price semi-bold margin-right-30">{{ Money::display($item->price) }}</span>
                        <div class="counter independent">
                            <button class="minus">-</button>
                            <input id="item_id" type="hidden" value="{{ $item->id }}" />
                            <input id="item_name" type="hidden" value="{{ $item->name }}" />
                            <input id="item_qty" type="text" class="totalx" name="" value="1">
                            <button class="plus">+</button>
                        </div>
                        <div class="add-to-cart-box">
                            <div class="row">
                                <div class="large-6 columns">
                                    <button id="item_add_to_cart" class="button btn-aneh">ADD TO CART</button>
                                </div>
                                <div class="large-6 columns"><br><br>
                                    <table>
                                        <tbody>
                                        <tr>
                                            <td>
                                                <a href="{{ url('groceries-to-love?id='.$item->id) }}"><img src="{{ asset(null) }}assets/images/ico-love-red.png" class="loveee"></a>
                                            </td>
                                            <td><a href="#"><img src="{{ asset(null) }}assets/images/ico-share.png"></a></td>
                                        </tr>
                                        <tr><td><b class="text-blue float-right">{{ Groceries::getLoveCount($item->id) }} loved</b></td><td>&nbsp;</td></tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="small-12 columns">
                                <ul class="accordion" data-accordion data-multi-expand="true" data-allow-all-closed="true">
                                    <li class="accordion-item is-active" data-accordion-item>
                                        <a href="#" class="accordion-title no-border">Description</a>
                                        <div style="font-size:12px;" class="accordion-content no-border" data-tab-content>
                                            {!! nl2br($item->description) !!}
                                        </div>
                                    </li>
                                    <li class="accordion-item is-active" data-accordion-item>
                                        <a href="#" class="accordion-title no-border">Nutrition</a>
                                        <div style="font-size:12px;" class="accordion-content no-border" data-tab-content>
                                            {!! nl2br($item->nutrients) !!}
                                        </div>
                                    </li>
                                    <li class="accordion-item is-active" data-accordion-item>
                                        <a href="#" class="accordion-title no-border">Indgridients</a>
                                        <div style="font-size:12px;" class="accordion-content no-border" data-tab-content>
                                            {!! nl2br($item->ingredients) !!}
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="small-12 columns">
                    <div class="recomendation-box top-line-blue">
                        <label>YOU MAY ALSO LIKE</label>
                        <div class="row">
                            <div class="small-2 columns">
                                <div class="item">
                                    <img src="{{ asset(null) }}assets/images/box6.jpg">
                                </div>
                                <span class="text-blue" style="padding-top: 10px;">Carrot, 30g</span>
                                <span>RP 2.500</span>
                            </div>
                            <div class="small-2 columns">
                                <div class="item">
                                    <img src="{{ asset(null) }}assets/images/box5.jpg">
                                </div>
                                <span class="text-blue" style="padding-top: 10px;">Carrot, 30g</span>
                                <span>RP 2.500</span>
                            </div>
                            <div class="small-2 columns">
                                <div class="item">
                                    <img src="{{ asset(null) }}assets/images/box3.jpg">
                                </div>
                                <span class="text-blue" style="padding-top: 10px;">Carrot, 30g</span>
                                <span>RP 2.500</span>
                            </div>
                            <div class="small-2 columns">
                                <div class="item">
                                    <img src="{{ asset(null) }}assets/images/box2.png">
                                </div>
                                <span class="text-blue" style="padding-top: 10px;">Carrot, 30g</span>
                                <span>RP 2.500</span>
                            </div>
                            <div class="small-2 columns">
                                <div class="item">
                                    <img src="{{ asset(null) }}assets/images/box1.jpg">
                                </div>
                                <span class="text-blue" style="padding-top: 10px;">Carrot, 30g</span>
                                <span>RP 2.500</span>
                            </div>
                            <div class="small-2 columns">
                                <div class="item">
                                    <img src="{{ asset(null) }}assets/images/box3.jpg">
                                </div>
                                <span class="text-blue" style="padding-top: 10px;">Carrot, 30g</span>
                                <span>RP 2.500</span>
                            </div>
                        </div>
                    </div>
                    <div class="recomendation-box top-line-blue bottom-line-blue">
                        <label>BRAND RELATED</label>
                        <div class="row">
                            <div class="small-2 columns">
                                <div class="item">
                                    <img src="{{ asset(null) }}assets/images/box1.jpg">
                                </div>
                                <span class="text-blue" style="padding-top: 10px;">Carrot, 30g</span>
                                <span>RP 2.500</span>
                            </div>
                            <div class="small-2 columns">
                                <div class="item">
                                    <img src="{{ asset(null) }}assets/images/box2.png">
                                </div>
                                <span class="text-blue" style="padding-top: 10px;">Carrot, 30g</span>
                                <span>RP 2.500</span>
                            </div>
                            <div class="small-2 columns">
                                <div class="item">
                                    <img src="{{ asset(null) }}assets/images/box3.jpg">
                                </div>
                                <span class="text-blue" style="padding-top: 10px;">Carrot, 30g</span>
                                <span>RP 2.500</span>
                            </div>
                            <div class="small-2 columns">
                                <div class="item">
                                    <img src="{{ asset(null) }}assets/images/box5.jpg">
                                </div>
                                <span class="text-blue" style="padding-top: 10px;">Carrot, 30g</span>
                                <span>RP 2.500</span>
                            </div>
                            <div class="small-2 columns">
                                <div class="item">
                                    <img src="{{ asset(null) }}assets/images/box6.jpg">
                                </div>
                                <span class="text-blue" style="padding-top: 10px;">Carrot, 30g</span>
                                <span>RP 2.500</span>
                            </div>
                            <div class="small-2 columns">
                                <div class="item">
                                    <img src="{{ asset(null) }}assets/images/box1.jpg">
                                </div>
                                <span class="text-blue" style="padding-top: 10px;">Carrot, 30g</span>
                                <span>RP 2.500</span>
                            </div>
                        </div>
                    </div>
                    <div class="recomendation-box">
                        <label>RECENTLY VIEWED</label>
                        <div class="row">
                            <div class="small-2 columns">
                                <div class="item">
                                    <img src="{{ asset(null) }}assets/images/box1.jpg">
                                </div>
                                <span class="text-blue" style="padding-top: 10px;">Carrot, 30g</span>
                                <span>RP 2.500</span>
                            </div>
                            <div class="small-2 columns">
                                <div class="item">
                                    <img src="{{ asset(null) }}assets/images/box2.png">
                                </div>
                                <span class="text-blue" style="padding-top: 10px;">Carrot, 30g</span>
                                <span>RP 2.500</span>
                            </div>
                            <div class="small-2 columns">
                                <div class="item">
                                    <img src="{{ asset(null) }}assets/images/box3.jpg">
                                </div>
                                <span class="text-blue" style="padding-top: 10px;">Carrot, 30g</span>
                                <span>RP 2.500</span>
                            </div>
                            <div class="small-2 columns">
                                <div class="item">
                                    <img src="{{ asset(null) }}assets/images/box5.jpg">
                                </div>
                                <span class="text-blue" style="padding-top: 10px;">Carrot, 30g</span>
                                <span>RP 2.500</span>
                            </div>
                            <div class="small-2 columns">
                                <div class="item">
                                    <img src="{{ asset(null) }}assets/images/box6.jpg">
                                </div>
                                <span class="text-blue" style="padding-top: 10px;">Carrot, 30g</span>
                                <span>RP 2.500</span>
                            </div>
                            <div class="small-2 columns">
                                <div class="item">
                                    <img src="{{ asset(null) }}assets/images/box1.jpg">
                                </div>
                                <span class="text-blue" style="padding-top: 10px;">Carrot, 30g</span>
                                <span>RP 2.500</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="small-12">
                    <div class="customer-and-reviews-box ">
                        <ul class="tabs" data-tabs id="example-tabs">
                            <li class="tabs-title is-active"><a href="#panel1" aria-selected="true">CUSTOMER & QA</a></li>
                            <li class="tabs-title"><a href="#panel2">REVIEWS</a></li>
                        </ul>
                        <div class="tabs-content top-line-blue" data-tabs-content="example-tabs"><br>
                            <div class="tabs-panel is-active" id="panel1">
                                <div class="row">
                                    <div class="large-6 columns">
                                        <div class="form-box">
                                            <input type="text" name="" placeholder="Q: Have a question? Search for answers">
                                            <a href="#">Can’t find what You are looking for?</a><br><br>
                                            <button class="button btn-aneh" id="groceries_ask">ASK KIRIN</button>

                                            <form class="groceries_question" method="post" style="display: none" action="{{ url('groceries/question/'.$item->id) }}">
                                                <textarea name="question" rows="5" class="groceries_question"></textarea>
                                                <input type="hidden" name="groceries_id" value="{{ $item->id }}"/>
                                                {{ csrf_field() }}
                                                <button type="submit" class="button btn-aneh">SUBMIT</button>
                                            </form>

                                        </div>
                                    </div>
                                    <div class="large-6 columns">
                                        <div class="customer-qa-box">
                                            <div class="orbit" role="region" aria-label="Favorite Space Pictures" data-orbit>
                                                <ul class="orbit-container">

                                                    <li class="is-active orbit-slide">
                                                        <div class="customer-qa-box">
                                                            <div class="qa-box">
                                                                <table>
                                                                    @foreach (App\Models\Groceries\Question::whereGroceriesId($item->id)->orderBy('updated_at', 'desc')->get() as $q)
                                                                        <tr>
                                                                            <td><span class="text-blue">Q :</span></td>
                                                                            <td><span class="text-blue">{!! nl2br($q->question) !!}</span></td>
                                                                        </tr>

                                                                        @if ($q->answer)
                                                                            <tr>
                                                                                <td>A :</td>
                                                                                <td>{!! nl2br($q->answer) !!}</td>
                                                                            </tr>
                                                                        @endif
                                                                    @endforeach
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                                <ul class="orbit-container small">
                                                    <button class="orbit-previous"><span class="show-for-sr"></span><img src="{{ asset(null) }}assets/images/ico-arrows-L.png"></button>
                                                    <button class="orbit-next"><span class="show-for-sr"></span><img src="{{ asset(null) }}assets/images/ico-arrows-R.png"></button>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tabs-panel" id="panel2">
                                <div class="row">
                                    <div class="large-6 columns">
                                        <ul class="rating">
                                            <li><img src="{{ asset(null) }}assets/images/ico-star-fill-color.png"></li>
                                            <li><img src="{{ asset(null) }}assets/images/ico-star-fill-color.png"></li>
                                            <li><img src="{{ asset(null) }}assets/images/ico-star-fill-color.png"></li>
                                            <li><img src="{{ asset(null) }}assets/images/ico-star-fill-color.png"></li>
                                            <li><img src="{{ asset(null) }}assets/images/ico-star-border.png"></li>
                                            <li><span class="persen">108%</span></li>
                                        </ul>
                                        <span>4 out of 5 stars</span><br><br>
                                        <div class="chart-box">
                                            <table class="chart">
                                                <tbody>
                                                <tr>
                                                    <td>
                                                        <p class="percentage">5 Stars</p>
                                                    </td>
                                                    <td class="chart">
                                                        <div class="barchart">
                                                            <span class="meter" style="width: 49%"></span>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <p class="percentage">49%</p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p class="percentage">4 Stars</p>
                                                    </td>
                                                    <td class="chart">
                                                        <div class="barchart">
                                                            <span class="meter" style="width: 44%"></span>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <p class="percentage">44%</p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p class="percentage">3 Stars</p>
                                                    </td>
                                                    <td class="chart">
                                                        <div class="barchart">
                                                            <span class="meter" style="width: 41%"></span>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <p class="percentage">41%</p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p class="percentage">2 Stars</p>
                                                    </td>
                                                    <td class="chart">
                                                        <div class="barchart">
                                                            <span class="meter" style="width: 40%"></span>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <p class="percentage">40%</p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p class="percentage">1 Star</p>
                                                    </td>
                                                    <td class="chart">
                                                        <div class="barchart">
                                                            <span class="meter" style="width: 22%"></span>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <p class="percentage">22%</p>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                    <div class="large-6 columns">
                                        <div class="customer-qa-box" style="height:400px; width: 500px; overflow:hidden;">
                                            <div id="owl-example" class="owl-carousel">
                                                <div class="owl-slide">
                                                    <div class="customer-qa-box">
                                                        <div class="qa-box">
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <div class="rating-box">
                                                                            <ul class="rating">
                                                                                <li><img src="{{ asset(null) }}assets/images/ico-star-fill.png"></li>
                                                                                <li><img src="{{ asset(null) }}assets/images/ico-star-fill.png"></li>
                                                                                <li><img src="{{ asset(null) }}assets/images/ico-star-fill.png"></li>
                                                                                <li><img src="{{ asset(null) }}assets/images/ico-star-fill.png"></li>
                                                                                <li><img src="{{ asset(null) }}assets/images/ico-star-fill.png"></li>
                                                                            </ul>
                                                                        </div>
                                                                    </td>
                                                                    <td><b>Best Carrot Ever!</b></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2"><span class="font-11">By Kimmy on April, 23 2016</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2"><span class="font-11">Crunchy, Nice, Orange, wow!</span></td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div class="qa-box">
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <div class="rating-box">
                                                                            <ul class="rating">
                                                                                <li><img src="{{ asset(null) }}assets/images/ico-star-fill.png"></li>
                                                                                <li><img src="{{ asset(null) }}assets/images/ico-star-fill.png"></li>
                                                                                <li><img src="{{ asset(null) }}assets/images/ico-star-fill.png"></li>
                                                                                <li><img src="{{ asset(null) }}assets/images/ico-star-fill.png"></li>
                                                                                <li><img src="{{ asset(null) }}assets/images/ico-star-fill.png"></li>
                                                                            </ul>
                                                                        </div>
                                                                    </td>
                                                                    <td><b>Best Carrot Ever!</b></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2"><span class="font-11">By Kimmy on April, 23 2016</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2"><span class="font-11">Crunchy, Nice, Orange, wow!</span></td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div class="qa-box">
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <div class="rating-box">
                                                                            <ul class="rating">
                                                                                <li><img src="{{ asset(null) }}assets/images/ico-star-fill.png"></li>
                                                                                <li><img src="{{ asset(null) }}assets/images/ico-star-fill.png"></li>
                                                                                <li><img src="{{ asset(null) }}assets/images/ico-star-fill.png"></li>
                                                                                <li><img src="{{ asset(null) }}assets/images/ico-star-fill.png"></li>
                                                                                <li><img src="{{ asset(null) }}assets/images/ico-star-fill.png"></li>
                                                                            </ul>
                                                                        </div>
                                                                    </td>
                                                                    <td><b>Best Carrot Ever!</b></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2"><span class="font-11">By Kimmy on April, 23 2016</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2"><span class="font-11">Crunchy, Nice, Orange, wow!</span></td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div class="qa-box">
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <div class="rating-box">
                                                                            <ul class="rating">
                                                                                <li><img src="{{ asset(null) }}assets/images/ico-star-fill.png"></li>
                                                                                <li><img src="{{ asset(null) }}assets/images/ico-star-fill.png"></li>
                                                                                <li><img src="{{ asset(null) }}assets/images/ico-star-fill.png"></li>
                                                                                <li><img src="{{ asset(null) }}assets/images/ico-star-fill.png"></li>
                                                                                <li><img src="{{ asset(null) }}assets/images/ico-star-fill.png"></li>
                                                                            </ul>
                                                                        </div>
                                                                    </td>
                                                                    <td><b>Best Carrot Ever!</b></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2"><span class="font-11">By Kimmy on April, 23 2016</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2"><span class="font-11">Crunchy, Nice, Orange, wow!</span></td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="owl-slide">
                                                    <div class="customer-qa-box">
                                                        <div class="qa-box">
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <div class="rating-box">
                                                                            <ul class="rating">
                                                                                <li><img src="{{ asset(null) }}assets/images/ico-star-fill.png"></li>
                                                                                <li><img src="{{ asset(null) }}assets/images/ico-star-fill.png"></li>
                                                                                <li><img src="{{ asset(null) }}assets/images/ico-star-fill.png"></li>
                                                                                <li><img src="{{ asset(null) }}assets/images/ico-star-fill.png"></li>
                                                                                <li><img src="{{ asset(null) }}assets/images/ico-star-fill.png"></li>
                                                                            </ul>
                                                                        </div>
                                                                    </td>
                                                                    <td><b>Best Carrot Ever!</b></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2"><span class="font-11">By Kimmy on April, 23 2016</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2"><span class="font-11">Crunchy, Nice, Orange, wow!</span></td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div class="qa-box">
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <div class="rating-box">
                                                                            <ul class="rating">
                                                                                <li><img src="{{ asset(null) }}assets/images/ico-star-fill.png"></li>
                                                                                <li><img src="{{ asset(null) }}assets/images/ico-star-fill.png"></li>
                                                                                <li><img src="{{ asset(null) }}assets/images/ico-star-fill.png"></li>
                                                                                <li><img src="{{ asset(null) }}assets/images/ico-star-fill.png"></li>
                                                                                <li><img src="{{ asset(null) }}assets/images/ico-star-fill.png"></li>
                                                                            </ul>
                                                                        </div>
                                                                    </td>
                                                                    <td><b>Best Carrot Ever!</b></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2"><span class="font-11">By Kimmy on April, 23 2016</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2"><span class="font-11">Crunchy, Nice, Orange, wow!</span></td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div class="qa-box">
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <div class="rating-box">
                                                                            <ul class="rating">
                                                                                <li><img src="{{ asset(null) }}assets/images/ico-star-fill.png"></li>
                                                                                <li><img src="{{ asset(null) }}assets/images/ico-star-fill.png"></li>
                                                                                <li><img src="{{ asset(null) }}assets/images/ico-star-fill.png"></li>
                                                                                <li><img src="{{ asset(null) }}assets/images/ico-star-fill.png"></li>
                                                                                <li><img src="{{ asset(null) }}assets/images/ico-star-fill.png"></li>
                                                                            </ul>
                                                                        </div>
                                                                    </td>
                                                                    <td><b>Best Carrot Ever!</b></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2"><span class="font-11">By Kimmy on April, 23 2016</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2"><span class="font-11">Crunchy, Nice, Orange, wow!</span></td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div class="qa-box">
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <div class="rating-box">
                                                                            <ul class="rating">
                                                                                <li><img src="{{ asset(null) }}assets/images/ico-star-fill.png"></li>
                                                                                <li><img src="{{ asset(null) }}assets/images/ico-star-fill.png"></li>
                                                                                <li><img src="{{ asset(null) }}assets/images/ico-star-fill.png"></li>
                                                                                <li><img src="{{ asset(null) }}assets/images/ico-star-fill.png"></li>
                                                                                <li><img src="{{ asset(null) }}assets/images/ico-star-fill.png"></li>
                                                                            </ul>
                                                                        </div>
                                                                    </td>
                                                                    <td><b>Best Carrot Ever!</b></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2"><span class="font-11">By Kimmy on April, 23 2016</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2"><span class="font-11">Crunchy, Nice, Orange, wow!</span></td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="owl-slide">
                                                    <div class="customer-qa-box">
                                                        <div class="qa-box">
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <div class="rating-box">
                                                                            <ul class="rating">
                                                                                <li><img src="{{ asset(null) }}assets/images/ico-star-fill.png"></li>
                                                                                <li><img src="{{ asset(null) }}assets/images/ico-star-fill.png"></li>
                                                                                <li><img src="{{ asset(null) }}assets/images/ico-star-fill.png"></li>
                                                                                <li><img src="{{ asset(null) }}assets/images/ico-star-fill.png"></li>
                                                                                <li><img src="{{ asset(null) }}assets/images/ico-star-fill.png"></li>
                                                                            </ul>
                                                                        </div>
                                                                    </td>
                                                                    <td><b>Best Carrot Ever!</b></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2"><span class="font-11">By Kimmy on April, 23 2016</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2"><span class="font-11">Crunchy, Nice, Orange, wow!</span></td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div class="qa-box">
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <div class="rating-box">
                                                                            <ul class="rating">
                                                                                <li><img src="{{ asset(null) }}assets/images/ico-star-fill.png"></li>
                                                                                <li><img src="{{ asset(null) }}assets/images/ico-star-fill.png"></li>
                                                                                <li><img src="{{ asset(null) }}assets/images/ico-star-fill.png"></li>
                                                                                <li><img src="{{ asset(null) }}assets/images/ico-star-fill.png"></li>
                                                                                <li><img src="{{ asset(null) }}assets/images/ico-star-fill.png"></li>
                                                                            </ul>
                                                                        </div>
                                                                    </td>
                                                                    <td><b>Best Carrot Ever!</b></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2"><span class="font-11">By Kimmy on April, 23 2016</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2"><span class="font-11">Crunchy, Nice, Orange, wow!</span></td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div class="qa-box">
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <div class="rating-box">
                                                                            <ul class="rating">
                                                                                <li><img src="{{ asset(null) }}assets/images/ico-star-fill.png"></li>
                                                                                <li><img src="{{ asset(null) }}assets/images/ico-star-fill.png"></li>
                                                                                <li><img src="{{ asset(null) }}assets/images/ico-star-fill.png"></li>
                                                                                <li><img src="{{ asset(null) }}assets/images/ico-star-fill.png"></li>
                                                                                <li><img src="{{ asset(null) }}assets/images/ico-star-fill.png"></li>
                                                                            </ul>
                                                                        </div>
                                                                    </td>
                                                                    <td><b>Best Carrot Ever!</b></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2"><span class="font-11">By Kimmy on April, 23 2016</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2"><span class="font-11">Crunchy, Nice, Orange, wow!</span></td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div class="qa-box">
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <div class="rating-box">
                                                                            <ul class="rating">
                                                                                <li><img src="{{ asset(null) }}assets/images/ico-star-fill.png"></li>
                                                                                <li><img src="{{ asset(null) }}assets/images/ico-star-fill.png"></li>
                                                                                <li><img src="{{ asset(null) }}assets/images/ico-star-fill.png"></li>
                                                                                <li><img src="{{ asset(null) }}assets/images/ico-star-fill.png"></li>
                                                                                <li><img src="{{ asset(null) }}assets/images/ico-star-fill.png"></li>
                                                                            </ul>
                                                                        </div>
                                                                    </td>
                                                                    <td><b>Best Carrot Ever!</b></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2"><span class="font-11">By Kimmy on April, 23 2016</span></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="2"><span class="font-11">Crunchy, Nice, Orange, wow!</span></td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection