@extends('layouts.newmaster')

@section('title', App\Page::createTitle('About Us'))

@section('customcss')
    <link rel="stylesheet" type="text/css" href="{!! asset(null) !!}assets/css/custom.css">
@endsection

@section('content')
    <section>
        <div class="row align-center">
            <h5 class="title title-margin">ABOUT US</h5>
            <div class="small-12 columns">
                <img class="center" src="{!! asset(null) !!}assets/images/about-us/about-1.png"></a>
            </div>
            <div class="small-12 columns">
                <img class="center" src="{!! asset(null) !!}assets/images/about-us/about-2.png"></a>
            </div>
            <div class="small-12 columns">
                <img class="center" src="{!! asset(null) !!}assets/images/about-us/about-3.png"></a>
            </div>
            <div class="small-12 columns">
                <img style="margin-left: 100px;" class="center" src="{!! asset(null) !!}assets/images/about-us/about-4.png"></a>
            </div>
            <div class="small-12 columns">
                <img style="margin-left: 167px;" class="center" src="{!! asset(null) !!}assets/images/about-us/about-5.png"></a>
            </div>
            <div class="small-12 columns">
                <img style="margin-left: 154px;" class="center" src="{!! asset(null) !!}assets/images/about-us/about-6.png"></a>
            </div>
        </div>
    </section>
@endsection