@extends('layouts.newmaster')

@section('title', App\Page::createTitle('Return and Exchange'))

{{--@section('customcss')
    <link rel="stylesheet" type="text/css" href="{!! asset(null) !!}assets/css/custom.css">
@endsection--}}

@section('content')
    <div class="row">
        <div class="small-12 columns">
            <span class="text-grey equal margin-top-10">
              <a class="text-grey" href="{{ url('/') }}"><b>HOME</b></a> /
              <a class="text-grey" href="#"><b>RETURN &amp; EXCHANGE POLICY</b></a>
            </span>
        </div>
    </div>
    <div class="delivery-box" id="checkout">
        <div class="box-content">
            <div class="row">
                <div class="small-12 columns">
                    <h5 class="title title-margin">RETURN &amp; EXCHANGE POLICY</h5>
                </div>
            </div>
            <div class="row">
                <div class="small-7 small-centered columns">
                    <p>
                        Return, exchange, or refund can be made under these following premises. Please read carefully.
                    </p>
                    <div class="sub-content" id="return-exchange">
                        <ul>
                            <li>
                                <h6>INCORRECT</h6>
                                <ul style="margin-left: 0;">
                                    <p>If you receive an incorrect item(s), you may request for a <span class="red">return or refund <span style="text-decoration: underline;">within 24 hours</span></span> after you receive the item(s) by submitting a claim form that you can fill out <a href="{{ url('order-claim') }}"> here</a>. Your request will be processed <span class="red" style="text-decoration: underline;">within 24 hours</span> <span style="font-style: italic;">(excluding Saturday, Sunday and public holidays)</span> from the time you submit your complain. </p><p>*This policy does not extend to any items which have been opened or whose seal has been broken and perishable items (such as chilled, bakery, or vegetable items).</p>
                                </ul>
                            </li>
                            <!-- <li>
                                <h6>MISSING</h6>
                                <ul style="margin-left: 0;">
                                    <p>
                                        If one of the items that you ordered did not arrive, please submit a claim form that you can fill out <a href="{{ url('order-claim') }}"> here</a>. We will process your request and send you the missing item(s) <span class="red" style="text-decoration: underline;">within 24 hours</span><span style="font-style: italic;"> (excluding Saturday, Sunday and public holidays)</span> from the time you submit your complain.
                                    </p>
                                </ul>
                            </li> -->
                            <li>
                                <h6>DAMAGED</h6>
                                <ul style="margin-left: 0;">
                                    <p>If you receive an incorrect item(s), you may request for a <span class="red">return or refund <span style="text-decoration: underline;">within 24 hours</span></span> after you receive the item(s) by submitting a claim form that you can fill out <a href="{{ url('order-claim') }}"> here</a>. Your request will be processed <span class="red" style="text-decoration: underline;">within 24 hours</span> <span style="font-style: italic;">(excluding Saturday, Sunday and public holidays)</span> from the time you submit your complain. </p><p>*This policy does not extend to any items which have been opened or whose seal has been broken and perishable items (such as chilled, bakery, or vegetable items).</p>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top: 3rem;">
                <div class="small-4 columns">
                    &nbsp;
                </div>
                <div class="small-8 columns">
                    <a href="{{ url('order-claim') }}" ><button class="button primary btn-aneh">ORDER CLAIM FORM</button></a>
                </div>
            </div>
        </div>
    </div>
@endsection