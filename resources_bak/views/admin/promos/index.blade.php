@extends('admin.layouts.master')

@section('title')
    Promo | Control Room
@endsection

@section('content-header')
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))

                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
        @endforeach
    </div>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            List Promos
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ url('admin/promos') }}"> Promos</a></li>
            <li class="active"> List Promos</li>
        </ol>
        <br/>
        <a href="{{ url('admin/promos/create') }}"><button class="btn btn-flat btn-sm btn-info">Create New Promo</button></a>
        <br/>
        <br/>
    </section>
@endsection

@section('content')

    <div class="box">
        <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Title</th>
                    <th>Code</th>
                    <th>Description</th>
                    <th>Period</th>
                    <th>Status</th>
                    <th>Options</th>
                </tr>
                </thead>
                <tbody>
                @foreach($promos as $promo)
                    <tr>
                        <td>{{ $promo->title }}</td>
                        <td>{{ $promo->code }}</td>
                        <td>{{ str_limit($promo->description, 20) }}</td>
                        <td>{{ LocalizedCarbon::parse($promo->start_date)->format('d-m-Y').' - '.LocalizedCarbon::parse($promo->end_date)->format('d-m-Y') }}</td>
                        <td>{{ $promo->isRunning($promo->id) }}</td>
                        <td>
                            <a href="{{ url('admin/promos/'.$promo->id) }}" class="pull-left"><button type="button" class="btn btn-primary btn-sm btn-flat">View</button>&nbsp;</a>
                            <a href="{{ url('admin/promos/'.$promo->id.'/edit') }}" class="pull-left"><button type="button" class="btn btn-primary btn-sm btn-flat">Edit</button>&nbsp;</a>
                            {!! Form::open(['method' => 'DELETE', 'action' => ['Admin\\PromoController@destroy', $promo->id], 'class'=> "pull-left"]) !!}
                            <button type="submit" class="btn btn-danger btn-sm btn-flat delete-entity-index">Delete</button>
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach

                </tbody>
                <tfoot>
                <tr>
                    <th>Title</th>
                    <th>Code</th>
                    <th>Description</th>
                    <th>Period</th>
                    <th>Status</th>
                    <th>Options</th>
                </tr>
                </tfoot>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->

    @include('admin.layouts.delete_modal')
@endsection

@section('myscript')
    <!-- DataTables -->
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script>
        $("#example1").DataTable();
        $(".delete-entity-index").on('click', function(e){
            var $form=$(this).closest('form');
            e.preventDefault();
            $('#confirm').modal({ backdrop: 'static', keyboard: false })
                .on('click', '#delete', function() {
                    $form.trigger('submit'); // submit the form
                });
        })
    </script>
@endsection