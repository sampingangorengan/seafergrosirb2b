@extends('admin.layouts.master')

@section('title')
    Create New Promo | Control Room
@endsection

@section('mycss')
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="{!! asset('bower_components/AdminLTE') !!}/plugins/datepicker/datepicker3.css">
@endsection

@section('content-header')
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))

                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
        @endforeach
    </div>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Create New Promo
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ url('admin/promos') }}">Promos</a></li>
            <li class="active">Create New Promo</li>
        </ol>
    </section>
@endsection

@section('content')
    <div class="box box-primary">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    <!-- form start -->
        {!! Form::open(['method'=>'POST', 'action'=>'Admin\\PromoController@store']) !!}
        <div class="box-body">
            <div class="form-group">
                <label for="nameInput">Promo Title</label>
                {!! Form::text('title', null, ['class'=>'form-control', 'placeholder' => 'e.g., "Valentine\'s Day Fiesta" or "Crab, It\'s Father\'s Day"', 'required' => 'required']) !!}
            </div>
            <div class="form-group">
                <label for="nameInput">Promo Code</label>
                {!! Form::text('code', null, ['class'=>'form-control', 'placeholder' => 'e.g, "VLTN140217", "CRABTODAY"', 'required' => 'required']) !!}
            </div>
            <div class="form-group">
                <label for="nameInput">Promo Description</label>
                {!! Form::textarea('description', null, ['class'=>'form-control', 'placeholder' => 'Explain about the promo here', 'required' => 'required']) !!}
            </div>
            <div class="form-group">
                <label for="nameInput">Promo Type</label>
                {!! Form::select('promo_type', array('percent' => 'Percentage', 'nominal' => 'Nominal'), null, ['class'=>'form-control']) !!}
            </div>
            <div class="form-group">
                <label for="nameInput">Promo Discount Value</label>
                {!! Form::text('discount_value', null, ['class'=>'form-control', 'placeholder' => 'e.g., 80, 20000, 5', 'required' => 'required']) !!}
            </div>
            <div class="form-group">
                <label for="nameInput">Promo Quota</label>
                {!! Form::text('quota', null, ['class'=>'form-control', 'placeholder' => 'e.g., 80, 20000, 5', 'required' => 'required']) !!}
            </div>
            <div class="form-group">
                <label for="nameInput">Promo Start Date</label>
                {!! Form::text('start_date', null, ['class'=>'form-control datepicker', 'placeholder' => 'Pick the start date', 'required' => 'required']) !!}
            </div>
            <div class="form-group">
                <label for="nameInput">Promo End Date</label>
                {!! Form::text('end_date', null, ['class'=>'form-control datepicker', 'placeholder' => 'Pick the end date', 'required' => 'required']) !!}
            </div>


        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            {{ csrf_field() }}
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
        {!! Form::close() !!}
    </div>
    <!-- /.box -->
@endsection

@section('myscript')
    <!-- DataTables -->
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <!-- bootstrap datepicker -->
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datepicker/bootstrap-datepicker.js"></script>
    <script>
        $("#example1").DataTable();
        //Date picker
        var dateToday = new Date();
        $('.datepicker').datepicker({
            autoclose: true,
            startDate: dateToday
        });
    </script>
@endsection