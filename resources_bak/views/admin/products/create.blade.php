@extends('admin.layouts.master')

@section('title')
    Create Groceries | Control Room
@endsection

@section('mycss')
    <link rel="stylesheet" href="{{ asset('assets/css/select2.css') }}">
@endsection

@section('content-header')
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))
                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
        @endforeach
    </div>

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Add New Groceries
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ url('admin/groceries') }}"> Groceries</a></li>
            <li class="active"> Add New Groceries</li>
        </ol>
        <br/>
    </section>
@endsection

@section('content')
    <div class="box box-primary">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::open(['method'=>'POST', 'action'=>'Admin\\ProductController@store', 'files' => true]) !!}
            <div class="box-body">
                <div class="form-group">
                    <label for="nameInput">Name</label>
                    {!! Form::text('name', null, ['value' => old('name'), 'class'=>'form-control', 'placeholder' => 'Grocery Name', 'id' => 'grocery_name','required' => 'required', 'onkeyup' => 'saveValue(this);']) !!}
                </div>
                <div class="form-group">
                    <label for="inputStock">Brand</label>
                    {!! Form::select('brand', $brands, null, ['class'=>'form-control', 'placeholder' => 'Grocery Brand', 'id' => 'grocery_brand', 'required' => 'required']) !!}
                </div>
                <div class="form-group">
                    <label for="inputStock">SKU</label>
                    {!! Form::text('sku', null, ['value' => old('sku'), 'class'=>'form-control', 'placeholder' => 'SKU', 'id' => 'grocery_sku', 'onkeyup' => 'saveValue(this);']) !!}
                </div>
                <div class="form-group">
                    <label for="inputStock">Barcode</label>
                    {!! Form::text('barcode', null, ['value'=> old('barcode'),'class'=>'form-control', 'placeholder' => 'Barcode', 'id' => 'grocery_barcode', 'onkeyup' => 'saveValue(this);']) !!}
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Category</label>
                    <br/>
                    {!! Form::select('category[]', $categories, null, ['class'=>'form-control categorybox', 'required' => 'required', 'multiple'=>true]) !!}
                </div>
                <div class="form-group">
                    <label for="inputStock">Country of Origin</label>
                    {!! Form::select('coo',$country, 107, ['class'=>'form-control', 'id' => 'grocery_coo', 'required' => 'required']) !!}
                </div>
                <div class="form-group">
                    <label for="inputStock">Freshwater/Seawater</label>
                    {!! Form::select('fresh_sea_water',['0' => 'Non Seawater/Freshwater', '1' => 'Seawater', '2' => 'Freshwater'], null, ['class'=>'form-control', 'id' => 'grocery_fsw', 'required' => 'required']) !!}
                </div>
                <div class="form-group">
                    <label for="inputStock">Cooking advices</label>
                    {!! Form::select('cooking_advice[]', $cookingAdvice, null, ['class'=>'form-control mytags', 'multiple'=>true]) !!}
                </div>
                <div class="form-group">
                    <label>Description</label>
                    {!! Form::textarea('description', null, ['value'=> old('description'),'class'=>'form-control', 'placeholder' => 'Description', 'id' => 'grocery_description', 'row' => '3', 'onkeyup' => 'saveValue(this);']) !!}
                </div>
                <div class="form-group">
                    <label>Nutrition Facts</label>
                    {!! Form::textarea('nutrients', null, ['value'=> old('nutrients'),'class'=>'form-control', 'placeholder' => 'Nutrients', 'row' => '3', 'id' => 'grocery_nutrient', 'onkeyup' => 'saveValue(this);']) !!}
                </div>
                <div class="form-group">
                    <label>Ingredients</label>
                    {!! Form::textarea('ingredients', null, ['value'=> old('ingredients'),'class'=>'form-control', 'placeholder' => 'Ingredients', 'row' => '3', 'id' => 'grocery_ingredient', 'onkeyup' => 'saveValue(this);']) !!}
                </div>
                <div class="form-group">
                    <label for="inputStock">Youtube Link</label>
                    {!! Form::text('youtube_link', null, ['value'=> old('ytl'),'class'=>'form-control', 'placeholder' => 'Paste your youtube url here', 'id' => 'grocery_ytl', 'onkeyup' => 'saveValue(this);']) !!}
                </div>
                <div class="form-group">
                    <label for="inputStock">Quantity per package</label>
                    {!! Form::text('qpp', null, ['value'=> old('qpp'),'class'=>'form-control', 'placeholder' => 'e.g, 1, 100, 1.5, 2.5  (must be number greater than zero)', 'id' => 'grocery_qpp', 'required' => 'required', 'onkeyup' => 'saveValue(this);']) !!}
                </div>
                <div class="form-group">
                    <label for="inputStock">Package metric</label>
                    {!! Form::select('metric',$metrics, null, ['class'=>'form-control', 'placeholder' => 'Metric', 'id' => 'grocery_metric', 'required' => 'required']) !!}
                </div>
                <div class="form-group">
                    <label for="price">Price</label>
                    {!! Form::text('price', null, ['value'=> old('price'),'class'=>'form-control', 'placeholder' => 'Price', 'id' => 'grocery_price', 'required' => 'required', 'onkeyup' => 'saveValue(this);']) !!}
                </div>
                <div class="form-group">
                    <label for="nameInput">Expiry Date</label>
                    {!! Form::text('expiry_date', null, ['value'=> old('expiry_date'),'class'=>'form-control', 'placeholder' => 'Pick the expiry date', 'id' => 'grocery_expiry', 'onkeyup' => 'saveValue(this);']) !!}
                </div>
                <div class="form-group">
                    <label for="inputStock">Stock</label>
                    {!! Form::text('stock', null, ['value'=> old('stock'),'class'=>'form-control', 'placeholder' => 'Stock', 'id' => 'grocery_stock', 'required' => 'required', 'onkeyup' => 'saveValue(this);']) !!}
                </div>
                <div class="form-group">
                    <label for="inputStock">Minimum Stock Alert</label>
                    {!! Form::text('minimum_stock_alert', null, ['value'=> old('minimum_stock_alert'),'class'=>'form-control', 'placeholder' => 'Stock', 'id' => 'grocery_minimum_stock', 'required' => 'required', 'onkeyup' => 'saveValue(this);']) !!}
                </div>
                <div class="form-group">
                    <label for="inputStock">Tags</label>
                    {!! Form::select('tags[]', $tags, null, ['class'=>'form-control mytags', 'multiple'=>true]) !!}
                </div>
                <div class="form-group">
                    <label for="inputStock">Running Discount</label>
                    {!! Form::select('discount',$discounts, null, ['class'=>'form-control', 'placeholder' => 'Discount', 'id' => 'grocery_discount']) !!}
                </div>
                <div class="form-group">
                    <label for="inputStock">Product Image</label>
                    {!! Form::file('image_one', null,['class'=>'form-control', 'placeholder' => 'Image', 'id' => 'image_one']) !!}
                    {!! Form::file('image_two', null,['class'=>'form-control', 'placeholder' => 'Image', 'id' => 'image_two']) !!}
                    {!! Form::file('image_three', null,['class'=>'form-control', 'placeholder' => 'Image', 'id' => 'image_three']) !!}
                    {!! Form::file('image_four', null,['class'=>'form-control', 'placeholder' => 'Image', 'id' => 'image_four']) !!}
                    {!! Form::file('image_five', null,['class'=>'form-control', 'placeholder' => 'Image', 'id' => 'image_five']) !!}
                </div>
                <div class="form-group">
                    <label for="inputStock">Product Poster</label>
                    {!! Form::file('poster_one', null,['class'=>'form-control', 'placeholder' => 'Poster', 'id' => 'poster_one']) !!}
                    {!! Form::file('poster_two', null,['class'=>'form-control', 'placeholder' => 'Poster', 'id' => 'poster_two']) !!}
                    {!! Form::file('poster_three', null,['class'=>'form-control', 'placeholder' => 'Poster', 'id' => 'poster_three']) !!}
                    {!! Form::file('poster_four', null,['class'=>'form-control', 'placeholder' => 'Poster', 'id' => 'poster_four']) !!}
                    {!! Form::file('poster_five', null,['class'=>'form-control', 'placeholder' => 'Poster', 'id' => 'poster_five']) !!}
                    {!! Form::file('poster_six', null,['class'=>'form-control', 'placeholder' => 'Poster', 'id' => 'poster_six']) !!}
                    {!! Form::file('poster_seven', null,['class'=>'form-control', 'placeholder' => 'Poster', 'id' => 'poster_seven']) !!}
                </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        {!! Form::close() !!}
    </div>
    <!-- /.box -->
@endsection

@section('myscript')
    <!-- DataTables -->
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script src="{!! asset('assets/js/select2.full.js') !!}"></script>
    <script src="{!! asset('assets/js/accounting.min.js') !!}"></script>
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datepicker/bootstrap-datepicker.js"></script>
    <script>
        var getPrice = function() {
            var dirty_price = $('#grocery_price').val();
            dirty_price = dirty_price.replace('.00','');
            dirty_price = dirty_price.replace(',','');
            return dirty_price;
        }
        var drawPrice = function(value) {
            $('#grocery_price').val(value);
            return 'ok';
        }
        $("#example1").DataTable();
        $(".mytags").select2({
            tags: true,
            placeholder: "Select or write your own"
        });
        $(".categorybox").select2();
        $('.datepicker').datepicker({
            autoclose: true,
            /*format:"yyyy.M.dd"*/
        });
        // expire date
        var dateToday = new Date();
        $('#grocery_expiry').datepicker({
            autoclose: true,
            startDate: dateToday
        });

        $('#grocery_price').keyup(function(event) {
            console.log(getPrice());
            result = accounting.formatMoney(getPrice(), "", 0);
            /*result = accounting.formatMoney(
             getPrice(), {
             symbol: "",
             precision: 0,
             format: "%s%v"
             }
             )*/

            drawPrice(result);
        });

        $('#grocery_price').keypress(function(event){
            if(event.which != 8 && isNaN(String.fromCharCode(event.which))){
                event.preventDefault(); //stop character from entering input
            }
        });

        @if (count($errors) <= 0)
        document.getElementById("grocery_name").value = getSavedValue("grocery_name");    // set the value to this input
        document.getElementById("grocery_sku").value = getSavedValue("grocery_sku");
        document.getElementById("grocery_barcode").value = getSavedValue("grocery_barcode");
        document.getElementById("grocery_description").value = getSavedValue("grocery_description");
        document.getElementById("grocery_nutrient").value = getSavedValue("grocery_nutrient");
        document.getElementById("grocery_ingredient").value = getSavedValue("grocery_ingredient");
        document.getElementById("grocery_qpp").value = getSavedValue("grocery_qpp");
        document.getElementById("grocery_ytl").value = getSavedValue("grocery_ytl");
        document.getElementById("grocery_ytl").value = getSavedValue("grocery_fsw");
        document.getElementById("grocery_ytl").value = getSavedValue("grocery_coo");
        document.getElementById("grocery_price").value = getSavedValue("grocery_price");
        document.getElementById("grocery_expiry").value = getSavedValue("grocery_expiry");
        document.getElementById("grocery_stock").value = getSavedValue("grocery_stock");
        document.getElementById("grocery_minimum_stock").value = getSavedValue("grocery_minimum_stock");
        @else
        saveValue(document.getElementById('grocery_name'));
        saveValue(document.getElementById('grocery_sku'));
        saveValue(document.getElementById('grocery_barcode'));
        saveValue(document.getElementById('grocery_description'));
        saveValue(document.getElementById('grocery_nutrient'));
        saveValue(document.getElementById('grocery_ingredient'));
        saveValue(document.getElementById('grocery_qpp'));
        saveValue(document.getElementById('grocery_ytl'));
        saveValue(document.getElementById('grocery_fsw'));
        saveValue(document.getElementById('grocery_coo'));
        saveValue(document.getElementById('grocery_price'));
        saveValue(document.getElementById('grocery_expiry'));
        saveValue(document.getElementById('grocery_stock'));
        saveValue(document.getElementById('grocery_minimum_stock'));
        @endif
        /* Here you can add more inputs to set value. if it's saved */

        //Save the value function - save it to localStorage as (ID, VALUE)
        function saveValue(e){
            var id = e.id;  // get the sender's id to save it .
            var val = e.value; // get the value.
            localStorage.setItem(id, val);// Every time user writing something, the localStorage's value will override .
        }

        //get the saved value function - return the value of "v" from localStorage.
        function getSavedValue(v){
            if (localStorage.getItem(v) === null) {
                return "";// You can change this to your default value.
            }
            return localStorage.getItem(v);
        }

        $('form').on('submit', function(e){
            e.preventDefault();

            var price = getPrice();
            $('#grocery_price').val(price);

            localStorage.clear();

            $(this).unbind('submit').submit();
        });
    </script>
@endsection