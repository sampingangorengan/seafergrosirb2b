@extends('admin.layouts.master')

@section('title')
    Import from CSV | Control Room
@endsection

@section('mycss')
    <link rel="stylesheet" href="{{ asset('assets/css/select2.css') }}">
@endsection

@section('content-header')
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))
                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
        @endforeach
    </div>

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Import Groceries from CSV
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ url('admin/groceries') }}"> Groceries</a></li>
            <li class="active"> Import from CSV</li>
        </ol>
        <br/>
    </section>
@endsection

@section('content')
    <div class="box box-primary">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::open(['method'=>'POST', 'files' => true]) !!}
        <div class="box-body">
            <div class="form-group">
                <label for="inputStock">Select CSV file</label>
                {!! Form::file('file', null,['class'=>'form-control', 'placeholder' => 'File', 'id' => 'file']) !!}
            </div>
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
        {!! Form::close() !!}
    </div>
    <!-- /.box -->
@endsection

@section('myscript')
    <!-- DataTables -->
@endsection