@extends('admin.layouts.master')

@section('title')
    Edit Groceries | Control Room
@endsection

@section('mycss')
    <link rel="stylesheet" href="{{ asset('assets/css/select2.css') }}">
@endsection

@section('content-header')
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))

                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
        @endforeach
    </div>

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Edit Groceries
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ url('admin/groceries') }}"> Groceries</a></li>
            <li class="active"> Edit Groceries</li>
        </ol>
        <br/>
    </section>
@endsection

@section('content')
    <div class="box box-primary">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::model($product,['method'=>'PATCH', 'action'=>['Admin\\ProductController@update', $product->id]]) !!}
        <div class="box-body">
            <div class="form-group">
                <label for="nameInput">Name</label>
                {!! Form::text('name', null, ['class'=>'form-control', 'placeholder' => 'Grocery Name', 'required' => 'required']) !!}
            </div>
            <div class="form-group">
                <label for="inputStock">Brand</label>
                {!! Form::select('brand', $brands, null, ['class'=>'form-control brand-box', 'placeholder' => 'Grocery Brand', 'required' => 'required']) !!}
            </div>
            <div class="form-group">
                <label for="inputStock">SKU</label>
                {!! Form::text('sku', null, ['class'=>'form-control', 'placeholder' => 'SKU']) !!}
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Category</label>
                <br/>
                {!! Form::select('category[]', $categories, null, ['class'=>'form-control categorybox', 'required' => 'required', 'multiple' => true]) !!}
            </div>
            <div class="form-group">
                <label for="inputStock">Country of Origin</label>
                {!! Form::select('country_id',$countries, null, ['class'=>'form-control', 'id' => 'grocery_coo']) !!}
            </div>
            <div class="form-group">
                <label for="inputStock">Freshwater/Seawater</label>
                {!! Form::select('fresh_sea_water',['0' => 'Non Seawater/Freshwater', '1' => 'Seawater', '2' => 'Freshwater'], null, ['class'=>'form-control', 'id' => 'grocery_fsw']) !!}
            </div>
            <div class="form-group">
                    <label for="inputStock">Cooking advices</label>
                    {!! Form::select('cooking_advice[]', $cookingAdvice, null, ['class'=>'form-control mytags', 'multiple'=>true]) !!}
                </div>
            <div class="form-group">
                <label>Description</label>
                {!! Form::textarea('description', null, ['class'=>'form-control', 'placeholder' => 'Description', 'row' => '3']) !!}
            </div>
            <div class="form-group">
                <label>Nutrition Facts</label>
                {!! Form::textarea('nutrients', null, ['class'=>'form-control', 'placeholder' => 'Nutrients', 'row' => '3']) !!}
            </div>
            <div class="form-group">
                <label>Ingredients</label>
                {!! Form::textarea('ingredients', null, ['class'=>'form-control', 'placeholder' => 'Ingredients', 'row' => '3']) !!}
            </div>
            <div class="form-group">
                    <label for="inputStock">Youtube Link</label>
                    {!! Form::text('youtube_link', null, ['class'=>'form-control', 'placeholder' => 'Paste your youtube url here', 'id' => 'grocery_ytl', 'onkeyup' => 'saveValue(this);']) !!}
                </div>
            <div class="form-group">
                <label for="inputStock">Quantity per package</label>
                {!! Form::text('value_per_package', null, ['class'=>'form-control', 'placeholder' => 'e.g, 1, 100, 1.5, 2.5  (must be number greater than zero)', 'required' => 'required']) !!}
            </div>
            <div class="form-group">
                <label for="inputStock">Package metric</label>
                {!! Form::select('metric_id',$metrics, null, ['class'=>'form-control', 'placeholder' => 'Metric', 'required' => 'required']) !!}
            </div>
            <div class="form-group">
                <label for="price">Price</label>
                {!! Form::text('price', null, ['class'=>'form-control', 'placeholder' => 'Price', 'required' => 'required']) !!}
            </div>
            <div class="form-group">
                <label for="nameInput">Expiry Date</label>
                {!! Form::text('expiry_date', null, ['class'=>'form-control','id' => 'grocery_expiry', 'placeholder' => 'Pick the expiry date']) !!}
            </div>
            <div class="form-group">
                <label for="inputStock">Stock</label>
                {!! Form::text('stock', null, ['class'=>'form-control', 'placeholder' => 'Stock', 'required' => 'required']) !!}
            </div>
            <div class="form-group">
                <label for="inputStock">Minimum Stock Alert</label>
                {!! Form::text('minimum_stock_alert', null, ['class'=>'form-control', 'placeholder' => 'Stock', 'required' => 'required']) !!}
            </div>
            <div class="form-group">
                <label for="inputStock">Tags</label>
                {!! Form::select('tags[]',$tags, null, ['class'=>'form-control mytags', 'multiple' => true]) !!}
            </div>
            <div class="form-group">
                <label for="inputStock">Discount</label>
                {!! Form::select('discount_id',$discounts, null, ['class'=>'form-control', 'placeholder' => 'Discount']) !!}
            </div>
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
        {!! Form::close() !!}
    </div>
    <!-- /.box -->
@endsection

@section('myscript')
    <!-- DataTables -->
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script src="{!! asset('assets/js/select2.full.js') !!}"></script>
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datepicker/bootstrap-datepicker.js"></script>
    <script>
        var initList = function(values, section) {
            console.log(values);
            if(section == 'category') {
                $(".categorybox").val(values).trigger('change');    
            } else if(section == 'brand') {
                $(".brand-box").val(values[0]).trigger('change');    
            }
            
        }
        /*var loadCategory = function() {
            var li = $("#select2-category-ld-results");
            li.select2({
                placeholder: "Placeholder"
            });

            var unselected = li.find('option:not(:selected)');
            var selected = [];
            for (var i = 0; i < unselected.length; i++) {
                selected[i] = { id: unselected[i].value, text: unselected[i].text };
            }
            li.select2('data', selected);
        }*/
        $("#example1").DataTable();
        $(".mytags").select2({
            tags: true,
            placeholder: "Select or write your own",
        });
        $(".categorybox").select2();
        $('.datepicker').datepicker({
            autoclose: true
        });
        // expire date
        var dateToday = new Date();
        $('#grocery_expiry').datepicker({
            autoclose: true,
            startDate: dateToday
        });
        
        /*$(".categorybox").select2().select2("val", '1');*/
        $.getJSON('{{ url('admin/groceries/existing-groceries-item/'.$product->id.'/brand') }}', function(opts){
            initList(opts, 'brand');
        })

        $.getJSON('{{ url('admin/groceries/existing-groceries-item/'.$product->id.'/category') }}', function(opts){
            initList(opts, 'category');
        })

    </script>
@endsection