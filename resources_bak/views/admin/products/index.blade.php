@extends('admin.layouts.master')

@section('title')
    Groceries | Control Room
@endsection

@section('content-header')
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))

                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
        @endforeach
    </div>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            List Groceries
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ url('admin;/groceries') }}">Groceries</a></li>
            <li class="active">List Groceries<li>
        </ol>
    </section>
    <br/>
@endsection

@section('content')

    <div class="box">
    {{--<div class="box-header">
        <h3 class="box-title">Data Table With Full Features</h3>
    </div>--}}
    <!-- /.box-header -->
        <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Stock</th>
                    <th>Minimum stock alert</th>
                    <th>Description</th>
                    <th>Category</th>
                    <th>last update at</th>
                    <th>Options</th>
                </tr>
                </thead>
                <tbody>
                @foreach($groceries as $grocery)
                    <tr>
                        <td>{{ $grocery->name }} @if($grocery->stock <= $grocery->minimum_stock_alert)&nbsp;&nbsp;&nbsp;&nbsp;<span style="color:red;"><i>Low on stock</i></span>@endif</td>
                        <td>{{ Money::display($grocery->price) }}</td>
                        <td>{{ $grocery->stock }}</td>
                        <td>{{ $grocery->minimum_stock_alert }}</td>
                        <td>{{ str_limit($grocery->description, 20) }}</td>
                        <td>{{ $grocery->category->count() == 0 ? 'Not Available' : 'Available' }}</td>
                        <td>{{ LocalizedCarbon::instance($grocery->updated_at) }}</td>

                        <td>
                            <a href="{{ url('admin/groceries/'.$grocery->id) }}" class="pull-left"><button type="button" class="btn btn-sm btn-info">View</button>&nbsp;</a>
                            <a href="{{ url('admin/groceries/'.$grocery->id.'/edit') }}" class="pull-left"><button type="button" class="btn btn-primary btn-sm btn-flat">Edit</button>&nbsp;</a>
                            {!! Form::open(['method' => 'DELETE', 'action' => ['Admin\\ProductController@destroy', $grocery->id], 'class'=> "pull-left"]) !!}
                            {{--{{ csrf_field() }}--}}
                            <button type="submit" class="btn btn-danger btn-sm btn-flat delete-entity-index">Delete</button>
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach

                </tbody>
                <tfoot>
                <tr>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Stock</th>
                    <th>Minimum stock alert</th>
                    <th>Description</th>
                    <th>Category</th>
                    <th>last update at</th>
                    <th>Options</th>
                </tr>
                </tfoot>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->

    @include('admin.layouts.delete_modal')
@endsection

@section('myscript')
    <!-- DataTables -->
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script>
        $("#example1").DataTable();
        $(".delete-entity-index").on('click', function(e){
            var $form=$(this).closest('form');
            e.preventDefault();
            $('#confirm').modal({ backdrop: 'static', keyboard: false })
                .on('click', '#delete', function() {
                    $form.trigger('submit'); // submit the form
                });
        })
    </script>
@endsection