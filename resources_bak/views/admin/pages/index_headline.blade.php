@extends('admin.layouts.master')

@section('title')
    Headlines | Control Room
@endsection

@section('content-header')
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))

                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
        @endforeach
    </div>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            List Headlines
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ url('admin/headlines') }}"> Headlines</a></li>
            <li class="active"> List Headlines</li>
        </ol>
        <br/>
        <a href="{{ url('admin/headlines/create') }}"><button class="btn btn-flat btn-sm btn-info">Create New Headline</button></a>
        <br/>
        <br/>
    </section>
@endsection

@section('content')

    <div class="box">
        <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Image</th>
                    <th>Caption</th>
                    <th>Link</th>
                    <th>Status</th>
                    <th>Options</th>
                </tr>
                </thead>
                <tbody>
                @foreach($headlines as $headline)
                    <tr>
                        <td><img src="{!! asset('headlines/'.$headline->file_name) !!}" alt="{{ $headline->file_name }}" width="500px"></td>
                        <td>{{ $headline->caption }}</td>
                        <td>{{ $headline->link }}</td>
                        <td><span class="{{ $headline->is_active == 1 ? 'text-green' : 'text-red' }}">{{ $headline->is_active == 1 ? 'ACTIVE' : 'INACTIVE' }}</span></td>
                        <td>
                            <a href="{{ url('admin/headlines/'.$headline->id.'/edit') }}" class="pull-left"><button type="button" class="btn btn-primary btn-sm btn-flat">Edit</button>&nbsp;</a>
                            {!! Form::open(['method' => 'DELETE', 'action' => ['Admin\\HomeSlidesController@destroy', $headline->id], 'class'=> "pull-left"]) !!}
                            <button type="submit" class="btn btn-danger btn-sm btn-flat delete-entity-index">Delete</button>
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach

                </tbody>
                <tfoot>
                <tr>
                    <th>Image</th>
                    <th>Caption</th>
                    <th>Link</th>
                    <th>Status</th>
                    <th>Options</th>
                </tr>
                </tfoot>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->

    @include('admin.layouts.delete_modal')
@endsection

@section('myscript')
    <!-- DataTables -->
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script>
        $("#example1").DataTable();
        $(".delete-entity-index").on('click', function(e){
            var $form=$(this).closest('form');
            e.preventDefault();
            $('#confirm').modal({ backdrop: 'static', keyboard: false })
                .on('click', '#delete', function() {
                    $form.trigger('submit'); // submit the form
                });
        })
    </script>
@endsection