@extends('admin.layouts.master')

@section('title')
    Create Category Level One | Control Room
@endsection

@section('content-header')
    <div class="box-header with-border">
        <h3 class="box-title">Create Category Level One</h3>
    </div>
@endsection

@section('content')
    <div class="box box-primary">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <!-- form start -->
        {!! Form::open(['method'=>'POST', 'action'=>'Admin\\ParentCategoryController@store', 'files' => True]) !!}
        <div class="box-body">
            <div class="form-group">
                <label for="nameInput">Category Name</label>
                {!! Form::text('name', null, ['class'=>'form-control', 'placeholder' => 'Category Name', 'required' => 'required']) !!}
            </div>
            <div class="form-group">
                <label for="nameInput">Category Order</label>
                {!! Form::text('order', null, ['class'=>'form-control', 'placeholder' => 'e.g., 3 (must be integer)', 'required' => 'required']) !!}
            </div>
            <div class="form-group">
                <label for="nameInput">Category Image</label>
                {!! Form::file('file', null, ['class'=>'form-control']) !!}
                <p style="color:red;margin-left:2px;">*)The best size for this image is 120 x 120px</p>
            </div>
            <div class="form-group">
                <label for="nameInput">Category Status</label>
                {!! Form::select('is_active', array(1 => 'Active', 0 => 'Inactive'), null, ['class'=>'form-control']) !!}
            </div>


        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            {{ csrf_field() }}
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
        {!! Form::close() !!}
    </div>
    <!-- /.box -->
@endsection

@section('myscript')
    <!-- DataTables -->
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script>
        $("#example1").DataTable();
    </script>
@endsection