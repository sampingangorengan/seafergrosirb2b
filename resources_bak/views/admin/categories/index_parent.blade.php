@extends('admin.layouts.master')

@section('title')
    Category Level One | Control Room
@endsection

@section('content-header')
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))

                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
        @endforeach
    </div>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            List Category Level One
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ url('admin/parent-category') }}">Categories</a></li>
            <li class="active">List Categories Level One</li>
        </ol>
        <br/>
        <a href="{{ url('admin/parent-category/create') }}"><button class="btn btn-flat btn-sm btn-info">Create New Category Level One</button></a>
        <br/>

    </section>
@endsection

@section('content')

<br/>
<div class="box">
    {{--<div class="box-header">
        <h3 class="box-title">Data Table With Full Features</h3>
    </div>--}}
    <!-- /.box-header -->
    <div class="box-body">
        <table id="example1" class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>Name</th>
                <th>Slug</th>
                <th>Order</th>
                <th>Child</th>
                <th>Status</th>
                <th>Options</th>
            </tr>
            </thead>
            <tbody>
            @foreach($categories as $category)
            <tr>
                <td>{{ $category->name }}</td>
                <td>{{ $category->slug }}</td>
                <td>{{ $category->order }}</td>
                <td>
                    <ul>
                        @foreach($category->child as $child)
                        <li>{{ $child->name }}</li>
                        @endforeach
                    </ul>
                </td>
                <td><span class="{{ $category->is_active == 1 ? 'text-green' : 'text-red' }}">{{ $category->is_active == 1 ? 'ACTIVE' : 'INACTIVE' }}</span></td>

                <th>
                    <a href="{{ url('admin/parent-category/'.$category->id.'/edit') }}" class="pull-left"><button type="button" class="btn btn-sm btn-flat btn-primary">Edit</button>&nbsp;</a>
                    {!! Form::open(['method' => 'DELETE', 'action' => ['Admin\\ParentCategoryController@destroy', $category->id], 'class'=> "pull-left"]) !!}
                    <button type="submit" class="btn btn-danger btn-sm btn-flat delete-entity-index">Delete</button>
                    {!! Form::close() !!}
                </th>
            </tr>
            @endforeach

            </tbody>
            <tfoot>
            <tr>
                <th>Name</th>
                <th>Slug</th>
                <th>Order</th>
                <th>Child</th>
                <th>Status</th>
                <th>Options</th>
            </tr>
            </tfoot>
        </table>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->
@include('admin.layouts.delete_modal')
@endsection

@section('myscript')
<!-- DataTables -->
<script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
    $("#example1").DataTable();
    $(".delete-entity-index").on('click', function(e){
        var $form=$(this).closest('form');
        e.preventDefault();
        $('#confirm').modal({ backdrop: 'static', keyboard: false })
            .on('click', '#delete', function() {
                $form.trigger('submit'); // submit the form
            });
    })
</script>
@endsection