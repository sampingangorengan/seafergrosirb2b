@extends('admin.layouts.master')

@section('title')
    Claims | Control Room
@endsection

@section('content-header')
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))

                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
        @endforeach
    </div>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Show Complain #{{ $claim->id }}
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ url('admin/complaints') }}"> Claims</a></li>
            <li class="active"> Claim Detail</li>
        </ol>
    </section>
@endsection

@section('content')
    <!-- Custom Tabs -->
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_1" data-toggle="tab">General Information</a></li>
            <li><a href="#tab_2" data-toggle="tab">Head to Head Comparison</a></li>

            {{--<li><a href="#tab_5" data-toggle="tab">Upload Image</a></li>--}}
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    Actions <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    {{--<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Edit</a></li>--}}

                    <li role="presentation">
                        {!! Form::open(['method' => 'DELETE', 'action' => ['Admin\\ComplainController@destroy', $claim->id], 'class'=> "pull-left"]) !!}
                        <a role="menuitem" tabindex="-1" href="#" class="delete-entity-index">Delete</a>
                        {!! Form::close() !!}
                    </li>
                    {{--<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Create New</a></li>--}}
                    {{--<li role="presentation" class="divider"></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" target="_blank" href="{{ url('groceries/'.$product->id.'/'.$product->name) }}">View on frontend</a></li>--}}
                </ul>
            </li>
            <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab_1">

                <div class="box box-solid">
                    <div class="box-header with-border">
                        <h1 class="box-title">Claim for item: {{ $claim->item_name }}</h1>
                    </div>
                    <!-- /.box-header -->
                    @if($claim->photo)
                        <div class="box-body">
                            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                <ol class="carousel-indicators">

                                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>

                                </ol>
                                <div class="carousel-inner">
                                        <div class="item active">
                                            <img src="{{ $claim->photo }}" alt="{{ $claim->item_name }}">
                                        </div>
                                </div>
                                {{--<a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                                    <span class="fa fa-angle-left"></span>
                                </a>
                                <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                                    <span class="fa fa-angle-right"></span>
                                </a>--}}
                            </div>
                        </div>
                        <!-- /.box-body -->
                    @endif

                <!-- /.box-header -->
                    <div class="box-body no-padding">
                        <table class="table table-striped">
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>Property</th>
                                <th>Value</th>
                            </tr>
                            <tr>
                                <td>1.</td>
                                <td>Name</td>
                                <td>
                                    {{ $claim->first_name.' '.$claim->last_name }}
                                </td>
                            </tr>
                            <tr>
                                <td>2.</td>
                                <td>Email</td>
                                <td>
                                    {{ $claim->email }}
                                </td>
                            </tr>
                            <tr>
                                <td>3.</td>
                                <td>Order id</td>
                                <td>
                                    {{ $claim->order_id }}
                                </td>
                            </tr>
                            <tr>
                                <td>4.</td>
                                <td>Item name</td>
                                <td>
                                    {{ $claim->item_name }}
                                </td>
                            </tr>
                            <tr>
                                <td>5.</td>
                                <td>Complaint Date</td>
                                <td>
                                    {{ $claim->created_at }}
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <!-- /.tab-pane -->
            <div class="tab-pane" id="tab_2">
                <div class="box-body no-padding">
                    <table class="table table-striped">
                        <tr>
                            <th style="width: 10px">#</th>
                            <th>Comparison</th>
                            <th>Seafer's</th>
                            <th>Other's</th>
                        </tr>
                        <tr>
                            <td>1.</td>
                            <td>Price</td>
                            <td>
                                {{ Money::display($claim->seafer_price) }}
                            </td>
                            <td>
                                {{ Money::display($claim->other_price) }}
                            </td>
                        </tr>
                        <tr>
                            <td>2.</td>
                            <td>Link</td>
                            <td><a href="{{ $claim->seafer_link }}">{{ $claim->seafer_link }}</a></td>
                            <td><a href="{{ $claim->other_link }}">{{ $claim->other_link }}</a></td>
                        </tr>
                        <tr>
                            <td>4.</td>
                            <td>Location</td>
                            <td colspan="2">
                                {{ $claim->store_location }}
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

        {{--<div class="tab-pane" id="tab_5">
            <form method="post"
                Select your file:
                <input type="file" name="file" class="file-upload-native apalah" accept="image/*" />
                <div class="box-footer">
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            {!! Form::close() !!}
        </div>--}}
        <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->
    </div>
    <!-- nav-tabs-custom -->
    @include('admin.layouts.delete_modal')
@endsection

@section('myscript')
    <!-- DataTables -->
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script>
        $("#example1").DataTable();
        $(".delete-entity-index").on('click', function(e){
            var $form=$(this).closest('form');
            e.preventDefault();
            $('#confirm').modal({ backdrop: 'static', keyboard: false })
                .on('click', '#delete', function() {
                    $form.trigger('submit'); // submit the form
                });
        })
    </script>
@endsection