@extends('admin.layouts.master')

@section('title')
    Transaction Detail | Control Room
@endsection

@section('content-header')
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))

                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
        @endforeach
    </div>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Transaction Detail
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ url('admin/orders/'.$payment->order_id) }}"> Order #{{ $payment->order_id }}</a></li>
            <li class="active"> Transaction Detail</li>
        </ol>
    </section>
@endsection

@section('content')
    <!-- Custom Tabs -->
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_1" data-toggle="tab">General Information</a></li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane active" id="tab_1">
                
                <div class="box box-solid">
                    <div class="box-header with-border">
                        <h1 class="box-title pull-left">Order #{{ $payment->order_id }}</h1>
                        
                        <div class="pull-right">
                            @if( ($payment->payment_status_id != 2 && $payment->payment_status_id != 3 && $payment->order->order_status != '4' && $payment->order->order_status != '5') )
                            <a href="{{ url('admin/transactions/step-one/'.$payment->id.'?action=accept') }}">
                                <button type="button" class="btn-success">Accept Payment</button>
                            </a>
                            <a href="{{ url('admin/transactions/step-one/'.$payment->id.'?action=reject') }}">
                                <button type="button" class="btn-danger">Reject Payment</button>
                            </a>
                            @endif
                            @if(($payment->payment_status_id == 3 && $payment->order->order_status == 6 ))
                            <a href="{{ url('admin/transactions/step-one/'.$payment->id.'?action=accept') }}">
                                <button type="button" class="btn-success">Accept Payment</button>
                            </a>
                            @endif
                            @if($payment->order->order_status != '4' && $payment->order->order_status != '5')
                            <a href="{{ url('admin/transactions/step-one/'.$payment->id.'?action=void') }}">
                                <button type="button" class="btn-danger">Void Order</button>
                            </a>
                            @endif
                        </div>
                        
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body no-padding">
                        <table class="table table-striped">
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>Property</th>
                                <th>Value</th>
                            </tr>
                            <tr>
                                <td>1.</td>
                                <td>Payee</td>
                                <td>
                                    {{ $payment->transfer_sender_account_name }}
                                </td>
                            </tr>
                            <tr>
                                <td>2.</td>
                                <td>Account Number</td>
                                <td>
                                    {{ $payment->transfer_sender_account_number }}
                                </td>
                            </tr>
                            <tr>
                                <td>3.</td>
                                <td>Transfer Date</td>
                                <td>
                                    {{ LocalizedCarbon::parse($payment->transfer_date)->format('d F Y') }}
                                </td>
                            </tr>
                            <tr>
                                <td>4.</td>
                                <td>Transfer Time</td>
                                <td>
                                    {{ $payment->transfer_time }}
                                </td>
                            </tr>
                            <tr>
                                <td>5.</td>
                                <td>Transfer Destination</td>
                                <td>
                                    {{ $payment->transfer_dest_account }}
                                </td>
                            </tr>
                            <tr>
                                <td>6.</td>
                                <td>Transfer Amount</td>
                                <td>
                                    {{ Money::display($payment->transfer_amount) }}
                                </td>
                            </tr>
                            <tr>
                                <td>6.</td>
                                <td>Transfer Note</td>
                                <td>
                                    {{ $payment->transfer_note }}
                                    
                                </td>
                            </tr>
                            <tr>
                                <td>7.</td>
                                <td>Order Grand Total</td>
                                <td>
                                    {{ Money::display($payment->order->grand_total) }}
                                </td>
                            </tr>
                            <tr>
                                <td>8.</td>
                                <td>Payment Status</td>
                                <td>
                                    @if($payment->is_approved == 0)
                                        @if($payment->payment_status_id == 1)
                                            <span class="text-blue">NEW PAYMENT</span>
                                        @elseif($payment->payment_status_id == 2)
                                            <span class="text-blue">APPROVED</span>
                                        @elseif($payment->payment_status_id == 3)
                                            <span class="text-blue">REJECTED</span>
                                        @elseif($payment->payment_status_id == 4)
                                            <span class="text-blue">WAITING FOR REMANING</span>
                                        @elseif($payment->payment_status_id == 5)
                                            <span class="text-blue">DELAY APPROVAL</span>
                                        @elseif($payment->payment_status_id == 6)
                                            <span class="text-blue">3 DAYS APPROVAL</span>
                                        @endif
                                    @elseif($payment->is_approved == 1)
                                        <span class="text-green">APPROVED</span>
                                    @else
                                        <span class="text-red">REJECTED</span>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td>9.</td>
                                <td>Screen Shot</td>
                                <td>
                                    @if($payment->transfer_receipt_file != '')
                                    <img src="{!! asset(null) !!}payment_proof/{{ $payment->proof->file }}" style="width:60%"/>
                                    @else
                                    No Screenshot
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td>10.</td>
                                <td>Confirmed</td>
                                <td>
                                    @if($payment->user_confirmed != 1)
                                    Payment confirmation not sent
                                    @else
                                    Payment confirmation sent
                                    @endif
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <!-- /.tab-pane -->

        </div>
        <!-- /.tab-content -->
    </div>
    <!-- nav-tabs-custom -->
    @include('admin.layouts.delete_modal')
    <div class="modal fade" id="reject-transaction" role="dialog" aria-labelledby="confirmDeleteLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            {{-- {{ url('admin/transactions/'.$payment->id.'/reject-transaction') }} --}}
                {!! Form::open(['method' => 'POST', 'action' => 'Admin\\PaymentController@rejectTransaction', 'class'=> ""]) !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">State your reason for canceling this payment</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id" value="{{ $payment->id }}">
                    {!! Form::text('notes', null, ['class'=>'form-control', 'placeholder' => 'Type your reason', 'id' => 'rejection-reason','required' => 'required']) !!}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-danger" >Reject</button>
                </div>
                {!! Form::close() !!}
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
@endsection

@section('myscript')
    <!-- DataTables -->
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script>
        $("#example1").DataTable();
        $("#reject-transaction-btn").on('click', function(e){
            e.preventDefault();
            $('#reject-transaction').modal({ backdrop: 'static', keyboard: false });
        })
    </script>
@endsection