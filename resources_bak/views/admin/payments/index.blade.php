@extends('admin.layouts.master')

@section('title')
    Transaction | Control Room
@endsection

@section('content-header')
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))

                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
        @endforeach
    </div>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Payment List
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ url('admin/orders/'.$order) }}"> Order #{{ $order }}</a></li>
            <li class="active"> List Payment</li>
        </ol>
        <br/>
    </section>
@endsection

@section('content')

    <div class="box">
        <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Order Id</th>
                    <th>Payee</th>
                    <th>Account Number</th>
                    <th>Transfer Date</th>
                    <th>Transfer Destination</th>
                    <th>Status</th>
                    <th>Options</th>
                </tr>
                </thead>
                <tbody>
                @foreach($payments as $payment)
                    <tr>
                        <td><a href="{{ url('admin/orders', $payment->order_id) }}">{{ $payment->order_id }}</a></td>
                        <td>{{ $payment->transfer_sender_account_name }}</td>
                        <td>{{ $payment->transfer_sender_account_number }}</td>
                        <td>{{ LocalizedCarbon::parse($payment->transfer_date)->format('d F Y') }}</td>
                        <td>{{ $payment->transfer_dest_account }}</td>

                        @if($payment->is_approved == 0)
                        <td><p><strong>WAITING APPROVAL</strong></p></td>
                        @elseif($payment->is_approved == 1)
                        <td><p style="color:green"><strong>APPROVED</strong></p></td>
                        @elseif($payment->is_approved == 2)
                        <td><p style="color:red"><strong>REJECTED</strong></p></td>
                        @elseif($payment->is_approved == 3)
                        <td><p style="color:blue"><strong>UNIQUE CODE DIFFERENCE</strong></p></td>
                        @elseif($payment->is_approved == 4)
                        <td><p style="color:blue"><strong>LESS PAYMENT</strong></p></td>
                        @endif
                        <td>
                            <a href="{{ url('admin/transactions/'.$payment->id) }}" class="pull-left"><button type="button" class="btn btn-primary btn-sm btn-flat">View Payment Detail</button>&nbsp;</a>
                        </td>
                    </tr>
                @endforeach

                </tbody>
                <tfoot>
                <tr>
                    <th>Order Id</th>
                    <th>Payee</th>
                    <th>Account Number</th>
                    <th>Transfer Date</th>
                    <th>Transfer Destination</th>
                    <th>Status</th>
                    <th>Options</th>
                </tr>
                </tfoot>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
@endsection

@section('myscript')
    <!-- DataTables -->
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script>
        $("#example1").DataTable();
    </script>
@endsection