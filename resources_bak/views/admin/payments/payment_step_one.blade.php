@extends('admin.layouts.master')

@section('title')
    Transaction Detail | Control Room
@endsection

@section('content-header')
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))

                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
        @endforeach
    </div>
    <!-- Content Header (Page header) -->
    <section class="content-header">

        <ol class="breadcrumb">
            <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ url('admin/orders/'.$payment->order_id) }}"> Order #{{ $payment->order_id }}</a></li>
            <li class="active"> Transaction Detail</li>
        </ol>

    </section>
@endsection

@section('content')
    <!-- Main content -->
    <section class="invoice">
        <div class="box box-solid">
            <div class="box-header with-border">
                <h1 class="box-title">Send Message</h1>
            </div>

            <!-- /.box-header -->
            <div class="box-body no-padding">
                <br/>
                
                {!! Form::open(['method'=>'POST', 'action'=>'Admin\\PaymentController@processStepOne']) !!}
                <div class="form-group">
                    <label>To:</label><br/>
                    
                    @if($payment->order->user_id != 0)
                    <p>{{ $payment->order->user->name }} ({{ $payment->order->user->email }})</p>
                    @else
                    <label>{{ $payment->order->getShippingAddress($payment->order->user_address_id)->user->name }}</label><br/>
                    <label>{{ $payment->order->getShippingAddress($payment->order->user_address_id)->user->email }}</label><br/>
                    @endif
                </div>
                <div class="form-group">
                    <label for="inputStock">Payment Condition (Will be used as Subject of the email)</label>
                    {!! Form::select('type',$action_option, null, ['class'=>'form-control', 'placeholder' => 'Select '.ucfirst($choice).' Condition', 'id' => 'select-condition']) !!}
                    <input type="hidden" name="payment_id" value="{{ $payment->id }}" >
                </div>
                <div class="form-group">
                    <label for="inputStock" id='message-label'>Message</label>
                    {!! Form::textarea('message', null, ['class'=>'form-control', 'placeholder' => 'Input your message', 'id' => 'message-condition']) !!}
                </div>
                <button type="submit" class="btn btn-primary" id="do-next">Next Step</button>
                {!! Form::close() !!}
            </div>
        </div>
        <!-- /.box -->

    </section>

    @include('admin.layouts.delete_modal')
    <div class="modal fade" id="reject-transaction" role="dialog" aria-labelledby="confirmDeleteLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            {{-- {{ url('admin/transactions/'.$payment->id.'/reject-transaction') }} --}}
                {!! Form::open(['method' => 'POST', 'action' => 'Admin\\PaymentController@rejectTransaction', 'class'=> ""]) !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">State your reason for canceling this payment</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id" value="{{ $payment->id }}">
                    {!! Form::text('notes', null, ['class'=>'form-control', 'placeholder' => 'Type your reason', 'id' => 'rejection-reason','required' => 'required']) !!}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-danger" >Reject</button>
                </div>
                {!! Form::close() !!}
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
@endsection

@section('myscript')
    <!-- DataTables -->
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script>
        $("#example1").DataTable();
        $("#select-condition").on('change', function(){
            if($('#select-condition').val() == 1) {
                $('#message-condition').hide();
                $('#message-label').text('Message will be send from template');
            } else if($('#select-condition').val() == 2) {
                $('#message-condition').hide();
                $('#message-label').text('Message will be send from template');
            } else if($('#select-condition').val() == 3) {
                $('#message-condition').hide();
                $('#message-label').text('Message will be send from template');
            } else if($('#select-condition').val() == 4) {
                $('#message-condition').hide();
                $('#message-label').text('Message will be send from template');
            } else if($('#select-condition').val() == 5) {
                $('#message-condition').show();
                $('#message-label').text('Message');
            } else if($('#select-condition').val() == 6) {
                $('#message-condition').show();
                $('#message-label').text('Message');
            } else if($('#select-condition').val() == 7) {
                $('#message-condition').hide();
                $('#message-label').text('Message will be send from template');
            } 
        });
        $("#do-next").on('click', function(e){
            if($('#select-condition').val() == '') {
                e.preventDefault();
                alert('Type must be selected');
                return false;
            } else {
                return true;
            }
        })
    </script>
@endsection