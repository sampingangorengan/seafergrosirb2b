<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                @if(auth()->user()->profile_picture)
                    <img src="{!! asset(null) !!}{{ auth()->user()->photo->file }}" class="img-circle">
                @else
                    <img src="{!! asset(null) !!}assets/images/ico-header-acc.png" >
                @endif
            </div>
            <div class="pull-left info">
                <p>{{ auth()->user()->name }}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>

            {{-- s: Account Management--}}
            <li class="treeview {{ $active_menu == 'users' ? 'active' : '' }}">
                <a href="#">
                    <i class="fa fa-user"></i> <span>Account Management</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li{{ $active_submenu == 'users/' ? ' class=active' : '' }}><a href="{{ url('admin/users') }}"><i class="fa fa-circle-o"></i>List User</a></li>
                    <li{{ $active_submenu == 'roles/' ? ' class=active' : '' }}><a href="{{ url('admin/roles') }}"><i class="fa fa-circle-o"></i>List Roles</a></li>
                </ul>
            </li>
            {{-- e: Account Management--}}

            {{-- s: Groceries Management--}}
            <li class="treeview {{ $active_menu == 'groceries' || $active_menu == 'metrics' ? 'active' : '' }}">
                <a href="#">
                    <i class="fa fa-apple"></i> <span>Groceries Management</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li{{ $active_submenu == 'groceries/create' ? ' class=active' : '' }}><a href="{{ url('admin/groceries/create') }}"><i class="fa fa-circle-o"></i> Create Groceries<i class="gronotif fa fa-circle text-red" style="display:none"></i></a></li>
                    <li{{ $active_submenu == 'groceries/' ? ' class=active' : '' }}><a href="{{ url('admin/groceries/import-csv') }}"><i class="fa fa-circle-o"></i> Import CSV</a></li>
                    <li{{ $active_submenu == 'groceries/' ? ' class=active' : '' }}><a href="{{ url('admin/groceries') }}"><i class="fa fa-circle-o"></i> List Groceries</a></li>
                    <li{{ $active_submenu == 'groceries/' ? ' class=active' : '' }}><a href="{{ url('admin/groceries?get=sold') }}"><i class="fa fa-circle-o"></i> List Sold Groceries</a></li>
                    <li{{ $active_submenu == 'groceries/minimum' ? ' class=active' : '' }}><a href="{{ url('admin/groceries/minimum') }}"><i class="fa fa-circle-o"></i> List of Minimum Stock Groceries</a></li>
                    {{-- <li{{ $active_submenu == 'metrics/create' ? ' class=active' : '' }}><a href="{{ url('admin/metrics/create') }}"><i class="fa fa-balance-scale"></i> Create New Metric</a></li> --}}
                    <li{{ $active_submenu == 'metrics/' ? ' class=active' : '' }}><a href="{{ url('admin/metrics') }}"><i class="fa fa-balance-scale"></i> List Metric</a></li>
                </ul>
            </li>
            {{-- e: Groceries Management--}}

            {{-- s: Brand Management--}}
            <li class="treeview {{ $active_menu == 'brands' ? 'active' : '' }}">
                <a href="#">
                    <i class="fa fa-th-large"></i> <span>Brand Management</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li{{ $active_submenu == 'brands/create' ? ' class=active' : '' }}><a href="{{ url('admin/brands/create') }}"><i class="fa fa-circle-o"></i>Create New Brands</a></li>
                    <li{{ $active_submenu == 'brands/' ? ' class=active' : '' }}><a href="{{ url('admin/brands') }}"><i class="fa fa-circle-o"></i>List Brands</a></li>
                </ul>
            </li>
            {{-- e: Brand Management--}}

            {{-- s: Brand Management--}}
            <li class="treeview {{ $active_menu == 'promos' || $active_menu == 'discounts' || $active_menu == 'headlines' || $active_menu == 'newsletters' ? 'active' : '' }}">
                <a href="#">
                    <i class="fa fa-th-large"></i> <span>Promo Management</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li{{ $active_submenu == 'promos/create' ? ' class=active' : '' }}><a href="{{ url('admin/promos/create') }}"><i class="fa fa-circle-o"></i>Create New Promo</a></li>
                    <li{{ $active_submenu == 'promos/' ? ' class=active' : '' }}><a href="{{ url('admin/promos') }}"><i class="fa fa-circle-o"></i>List Promos</a></li>
                    <li{{ $active_submenu == 'discounts/create' ? ' class=active' : '' }}><a href="{{ url('admin/discounts/create') }}"><i class="fa fa-circle-o"></i>Create New Discounts</a></li>
                    <li{{ $active_submenu == 'discounts/' ? ' class=active' : '' }}><a href="{{ url('admin/discounts') }}"><i class="fa fa-circle-o"></i>List Discounts</a></li>
                    <li{{ $active_submenu == 'headlines/create' ? ' class=active' : '' }}><a href="{{ url('admin/headlines/create') }}"><i class="fa fa-circle-o"></i>Create New Headline</a></li>
                    <li{{ $active_submenu == 'headlines/' ? ' class=active' : '' }}><a href="{{ url('admin/headlines') }}"><i class="fa fa-circle-o"></i>List Headline Promos</a></li>
                    <li{{ $active_submenu == 'charity-voucher/create' ? ' class=active' : '' }}><a href="{{ url('admin/charity-voucher/create') }}"><i class="fa fa-circle-o"></i>Create New Charity Voucher</a></li>
                    <li{{ $active_submenu == 'charity-voucher/' ? ' class=active' : '' }}><a href="{{ url('admin/charity-voucher') }}"><i class="fa fa-circle-o"></i>List Charity Vouchers</a></li>
                    <li{{ $active_submenu == 'newsletters/' ? ' class=active' : '' }}><a href="{{ url('admin/newsletters') }}"><i class="fa fa-circle-o"></i>List Newsletter Subscriber</a></li>
                </ul>
            </li>
            {{-- e: Brand Management--}}

            {{-- s: Category Management--}}
            <li class="treeview {{ $active_menu == 'parent-category' || $active_menu == 'child-category' ? 'active' : '' }}">
                <a href="#">
                    <i class="fa fa-sitemap"></i> <span>Category Management</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li{{ $active_submenu == 'parent-category/create' ? ' class=active' : '' }}><a href="{{ url('admin/parent-category/create') }}"><i class="fa fa-folder-o"></i>Create Category Level One</a></li>
                    <li{{ $active_submenu == 'parent-category/' ? ' class=active' : '' }}><a href="{{ url('admin/parent-category/') }}"><i class="fa fa-folder-o"></i>List Category Level One</a></li>
                    <li{{ $active_submenu == 'child-category/create' ? ' class=active' : '' }}><a href="{{ url('admin/child-category/create') }}"><i class="fa fa-folder-open-o"></i>Create Category Level Two</a></li>
                    <li{{ $active_submenu == 'child-category/' ? ' class=active' : '' }}><a href="{{ url('admin/child-category/') }}"><i class="fa fa-folder-open-o"></i>List Category Level Two</a></li>
                </ul>
            </li>
            {{-- e: Category Management--}}

            {{-- s: Order Management--}}
            <li class="treeview {{ $active_menu == 'orders' ? 'active' : '' }}">
                <a href="#">
                    <i class="fa fa-cart-plus"></i> <span>Order Management</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    {{-- <li>
                        <a href="{{ url('admin/orders') }}">
                            <i class="fa fa-cart-arrow-down"></i>List All Order
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('admin/orders?status=orders') }}">
                            <i class="fa fa-cart-arrow-down"></i>Orders
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('admin/orders?status=confirmed') }}">
                            <i class="fa fa-cart-arrow-down"></i>Confirmed
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('admin/orders?status=completed') }}">
                            <i class="fa fa-cart-arrow-down"></i>Completed
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('admin/orders?status=rejected') }}">
                            <i class="fa fa-cart-arrow-down"></i>Rejected
                        </a>
                    </li> --}}

                    <li>
                        <a href="{{ url('admin/orders?status=orders') }}">
                            <i class="fa fa-cart-arrow-down"></i>New
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('admin/orders?status=confirmed') }}">
                            <i class="fa fa-cart-arrow-down"></i>Paid
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('admin/orders?status=completed') }}">
                            <i class="fa fa-cart-arrow-down"></i>Completed
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('admin/orders?status=rejected') }}">
                            <i class="fa fa-cart-arrow-down"></i>Cancelled
                        </a>
                    </li>
                </ul>
            </li>
            {{-- e: Order Management--}}

            {{-- s: Transaction Management--}}
            {{-- <li class="treeview {{ $active_menu == 'transactions' ? 'active' : '' }}">
                <a href="#">
                    <i class="fa fa-money"></i> <span>Transaction Management</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li{{ $active_submenu == 'transactions' ? ' class=active' : '' }}><a href="{{ url('admin/transactions/add-manual') }}"><i class="fa fa-star"></i>Add Transaction Manually</a></li>
                    <li{{ $active_submenu == 'transactions' ? ' class=active' : '' }}><a href="{{ url('admin/transactions') }}"><i class="fa fa-star"></i>List All Transaction</a></li>
                    <li><a href="{{ url('admin/transactions?get=pending') }}"><i class="fa fa-star-half-empty"></i>Pending Approval Transaction</a></li>
                    <li><a href="{{ url('admin/transactions?get=complete') }}"><i class="fa fa-thumbs-o-up"></i>Approved Transaction</a></li>
                </ul>
            </li> --}}
            {{-- e: Transaction Management--}}

            {{-- s: Complaint Management--}}
            <li class="treeview {{ $active_menu == 'claims' ? 'active' : '' }}">
                <a href="#">
                    <i class="fa fa-child"></i> <span>Claim Management</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('admin/claims') }}"><i class="fa fa-circle-o"></i>List Best Price Claims</a></li>
                    <li>
                        <a href="{{ url('admin/order-claims') }}">
                            <i class="fa fa-cart-arrow-down"></i>List Order Claim
                        </a>
                    </li>
                </ul>
            </li>
            {{-- e: Complaint Management--}}

            {{-- s: Complaint Management--}}
            <li class="treeview">
                <a href="http://seafermart.co.id:8000/livechat/php/app.php?admin" target="_blank">
                    <i class="fa fa-phone"></i> <span>Live Chat</span>
                </a>
            </li>
            {{-- e: Complaint Management--}}

        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
