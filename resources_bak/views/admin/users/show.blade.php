@extends('admin.layouts.master')

@section('title')
    Users | Control Room
@endsection

@section('content-header')
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))

                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
        @endforeach
    </div>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            {{ $user->name }}'s Detail
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ url('admin/complaints') }}"> Claims</a></li>
            <li class="active"> User Detail</li>
        </ol>
    </section>
@endsection

@section('content')

    <!-- Custom Tabs -->
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_1" data-toggle="tab">General Information</a></li>
            <li><a href="#tab_2" data-toggle="tab">Cashback History</a></li>
            <li><a href="#tab_3" data-toggle="tab">Email Preference</a></li>
            {{-- <li><a href="#dropdown" data-toggle="tab">Order List</a></li> --}}

            {{--<li><a href="#tab_5" data-toggle="tab">Upload Image</a></li>--}}
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    Actions <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="{{ url('admin/users/'.$user->id.'/edit') }}"><button type="button" class="btn-sm btn-primary">Edit</button></a></li>


                    <li role="presentation">
                        {!! Form::open(['method' => 'DELETE', 'action' => ['Admin\\UserController@destroy', $user->id], 'class'=> ""]) !!}
                        <button type="submit" class="btn btn-danger btn-sm btn-flat delete-entity-index" style="margin:0 auto;">Delete</button>
                        {!! Form::close() !!}
                    </li>
                    {{--<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Create New</a></li>--}}
                    {{--<li role="presentation" class="divider"></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" target="_blank" href="{{ url('groceries/'.$product->id.'/'.$product->name) }}">View on frontend</a></li>--}}
                </ul>
            </li>
            <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab_1">

                <div class="box box-solid">
                    <div class="box-header with-border">
                        <h1 class="box-title">User Detail:</h1>
                    </div>
                    <!-- /.box-header -->
                    @if($user->photo)
                        <div class="box-body">
                            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                <ol class="carousel-indicators">

                                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>

                                </ol>
                                <div class="carousel-inner">
                                        <div class="item active">
                                            <img src="{{ $user->photo }}" alt="{{ $user->item_name }}">
                                        </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    @endif

                <!-- /.box-header -->
                    <div class="box-body no-padding">
                        <table class="table table-striped">
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>Property</th>
                                <th>Value</th>
                            </tr>
                            <tr>
                                <td>1.</td>
                                <td>Name</td>
                                <td>
                                    {{ $user->name }}
                                </td>
                            </tr>
                            <tr>
                                <td>2.</td>
                                <td>Email</td>
                                <td>
                                    {{ $user->email }}
                                </td>
                            </tr>
                            <tr>
                                <td>3.</td>
                                <td>Gender</td>
                                <td>
                                    @if($user->sex == 'm')
                                    Male
                                    @else
                                    Female
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td>4.</td>
                                <td>Date of Birth</td>
                                <td>
                                    {{ $user->dob }}
                                </td>
                            </tr>
                            <tr>
                                <td>5.</td>
                                <td>Phone Number</td>
                                <td>
                                    {{ $user->phone_number }}
                                </td>
                            </tr>
                            <tr>
                                <td>6.</td>
                                <td>Role</td>
                                <td>
                                    @if($user->role == 1)
                                    Administrator
                                    @else
                                    User
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td>7.</td>
                                <td>Cashback Balance</td>
                                <td>
                                    @if(null == $cashback_point)
                                    {{ Money::display(0) }}
                                    @else
                                    {{ Money::display($cashback_point->nominal) }}
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td>8.</td>
                                <td>Status</td>
                                <td>
                                    @if($user->is_active == 1)
                                    Email confirmed
                                    @else
                                    Email not confirmed
                                    @endif
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <div class="tab-pane" id="tab_2">

                <div class="box box-solid">
                    <div class="box-header with-border">
                        <h1 class="box-title">Cashback History: </h1>
                    </div>
                    <!-- /.box-header -->
                    
                <!-- /.box-header -->
                    <div class="box-body no-padding">
                        <table class="table table-striped">
                            <tr>
                                <th width="200"><span style="color:#808184;">Date</span></th>
                                <th width="200"><span style="color:#808184;">Rewards Nominal</span></th>
                                <th width="200"><span style="color:#808184;">Activities</span></th>
                            </tr>

                            @foreach($cashback_rewards as $cashback)
                            <tr>
                                <td>{{ LocalizedCarbon::parse($cashback->created_at)->format('d F Y, H.i') }}</td>
                                @if($cashback->obtain_from != 'redeem')
                                <td>{{ $cashback->nominal }}</td>
                                @else
                                <td style="color:red"> - {{ $cashback->nominal }}</td>
                                @endif
                                @if($cashback->obtain_from == 'order')
                                    <td>Order number <a href="{{ url('orders/'.$cashback->order_id) }}" target="_blank">{{ $cashback->order_id }}</a></td>
                                @elseif($cashback->obtain_from == 'redeem')
                                    <td>Redeemed on Order number <a href="{{ url('orders/'.$cashback->order_id) }}" target="_blank">{{ $cashback->order_id }}</a></td>
                                @else
                                    <td>Referral from <i>{{ $cashback->order->user()->first()->name }}</i></td>
                                @endif
                            </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <div class="tab-pane" id="tab_3">

                <div class="box box-solid">
                    <div class="box-header with-border">
                        <h1 class="box-title">Email Preference: </h1>
                    </div>
                    <!-- /.box-header -->
                    
                <!-- /.box-header -->
                    <div class="box-body no-padding">
                        <table class="table table-striped">
                            <tr>
                                <th width="200"><span style="color:#808184;">Status</span></th>
                                <th width="200"><span style="color:#808184;">News Type</span></th>
                            </tr>
                            <tr>
                                <td><span @if(auth()->user()->email_preference->latest_news == 1) class="fa fa-check" @else class="fa fa-close" @endif></span></td>
                                <td>Latest News</td>
                            </tr>
                            <tr>
                                <td><span @if(auth()->user()->email_preference->promo == 1) class="fa fa-check" @else class="fa fa-close" @endif></span></td>
                                <td>Promo</td>
                            </tr>
                            <tr>
                                <td><span @if(auth()->user()->email_preference->editorial == 1) class="fa fa-check" @else class="fa fa-close" @endif></span></td>
                                <td>Editorial</td>
                            </tr>
                            <tr>
                                <td><span @if(auth()->user()->email_preference->new_product == 1) class="fa fa-check" @else class="fa fa-close" @endif></span></td>
                                <td>New Product</td>
                            </tr>
                            <tr>
                                <td><span @if(auth()->user()->email_preference->low_stock == 1) class="fa fa-check" @else class="fa fa-close" @endif></span></td>
                                <td>Low Stock</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
        <!-- /.tab-content -->
    </div>
    <!-- nav-tabs-custom -->
    @include('admin.layouts.delete_modal')
@endsection

@section('myscript')
    <!-- DataTables -->
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script>
        $("#example1").DataTable();
        $(".delete-entity-index").on('click', function(e){
            var $form=$(this).closest('form');
            e.preventDefault();
            $('#confirm').modal({ backdrop: 'static', keyboard: false })
                .on('click', '#delete', function() {
                    $form.trigger('submit'); // submit the form
                });
        })
    </script>
@endsection