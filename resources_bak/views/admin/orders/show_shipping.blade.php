@extends('admin.layouts.master')

@section('title')
    Shipping Information | Control Room
@endsection

@section('content-header')
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))

                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
        @endforeach
    </div>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Shipping Information for Order #{{ $order->id }}
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ url('admin/orders/'.$order->id) }}"> Order #{{ $order->id }}</a></li>
            <li class="active"> Shipping Information</li>
        </ol>
    </section>
@endsection

@section('content')
    <!-- Custom Tabs -->
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_1" data-toggle="tab">Shipping Information</a></li>
            <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane active" id="tab_1">
                
                <div class="box box-solid">
                    <div class="box-header with-border">
                        <h1 class="box-title pull-left">Order #{{ $order->id }}</h1>
                        @if($order->deliveries->status == 'not made' || $order->deliveries->status == 'cancelled')
                            @if($order->deliveries->delivery_type == 'gojek')
                                <a href="{{ url('admin/orders/order-gojek/'.$order->id) }}" style="color:white">
                                    <button type="button" class="btn btn-success pull-right" style="margin-right: 5px;">
                                        <i class="fa fa-credit-card"></i> Call Gojek For Delivery
                                    </button>
                                </a>
                            @elseif($order->deliveries->delivery_type == 'jne')
                                <button id="insert-awb" type="button" class="btn btn-success pull-right insert-awb" style="margin-right: 5px;">
                                    <i class="fa fa-credit-card"></i> Insert AWB
                                </button>
                            @else
                                <a href="{{ url('admin/orders/dispatch-courier/'.$order->id) }}" style="color:white">
                                    <button type="button" class="btn btn-success pull-right" style="margin-right: 5px;">
                                        <i class="fa fa-credit-card"></i> Dispatch Seafer Courier
                                    </button>
                                </a>
                            @endif
                        @endif

                        @if($order->deliveries->delivery_type == 'gojek' && $order->deliveries->getDeliveryDetail($order->deliveries->detail_id, 'gojek')->status == 'Rejected')
                            <br/>
                            <a href="{{ url('admin/transactions/step-one/'.$order->payment()->first()->id.'?action=void') }}">
                                <button type="button" class="btn-danger">Void Rejected Order</button>
                            </a>
                        @endif

                        @if($order->deliveries->delivery_type == 'Seafer Courier' && $order->deliveries->status == 'on delivery')
                            <br/>
                            <a href="{{ url('admin/orders/finish-courier/'.$order->id) }}">
                                <button type="button" class="btn-success"><i class="fa fa-credit-card"></i> Finish This Order</button>
                            </a>
                        @endif
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body no-padding">
                        <table class="table table-striped">
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>Property</th>
                                <th>Value</th>
                            </tr>
                            <tr>
                                <td>1.</td>
                                <td>Delivery Method</td>
                                <td>
                                    {{ ucfirst($shipping->delivery_type) }}
                                </td>
                            </tr>
                            <tr>
                                <td>2.</td>
                                <td>Delivery Type</td>
                                <td>
                                    @if($shipping->delivery_type == 'Seafer Courier')
                                    Same Day Delivery
                                    @else
                                    {{ ucwords($shipping->getDeliveryDetail($shipping->detail_id, $shipping->delivery_type)->booking_type) }}
                                    @endif
                                    
                                </td>
                            </tr>
                            <tr>
                                <td>3.</td>
                                <td>Delivery Status</td>
                                <td>
                                    @if($shipping->delivery_type == 'Seafer Courier')
                                    {{ ucwords($shipping->status) }}
                                    @else
                                    {{ ucwords($shipping->getDeliveryDetail($shipping->detail_id, $shipping->delivery_type)->status) }}
                                    @endif
                                    
                                </td>
                            </tr>
                            @if($shipping->status == 'on delivery' && $shipping->delivery_type == 'gojek')
                            <tr>
                                <td>4.</td>
                                <td>Gojek ID</td>
                                <td>
                                    {{ $shipping->getDeliveryDetail($shipping->detail_id, $shipping->delivery_type)->gojek_id }}
                                </td>
                            </tr>
                            <tr>
                                <td>5.</td>
                                <td>Gojek Booking Code</td>
                                <td>
                                    {{ $shipping->getDeliveryDetail($shipping->detail_id, $shipping->delivery_type)->gojek_order }}
                                </td>
                            </tr>
                            <tr>
                                <td>6.</td>
                                <td>Gojek Status</td>
                                <td>
                                    {{ $shipping->getDeliveryDetail($shipping->detail_id, $shipping->delivery_type)->gojek_order }}
                                </td>
                            </tr>
                            <tr>
                                <td>7.</td>
                                <td>Gojek Driver ID</td>
                                <td>
                                    @if(null != $shipping->getDeliveryDetail($shipping->detail_id, $shipping->delivery_type)->driver_id)
                                    {{ $shipping->getDeliveryDetail($shipping->detail_id, $shipping->delivery_type)->driver_id }}
                                    @else
                                    Not Available
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td>8.</td>
                                <td>Gojek Driver Name</td>
                                <td>
                                    @if(null != $shipping->getDeliveryDetail($shipping->detail_id, $shipping->delivery_type)->driver_id)
                                    {{ $shipping->getDeliveryDetail($shipping->detail_id, $shipping->delivery_type)->driver_name }}
                                    @else
                                    Not Available
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td>9.</td>
                                <td>Gojek Driver Phone</td>
                                <td>
                                    @if(null != $shipping->getDeliveryDetail($shipping->detail_id, $shipping->delivery_type)->driver_phone)
                                    {{ $shipping->getDeliveryDetail($shipping->detail_id, $shipping->delivery_type)->driver_phone }}
                                    @else
                                    Not Available
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td>10.</td>
                                <td>Gojek Driver Photo</td>
                                <td>
                                    @if(null != $shipping->getDeliveryDetail($shipping->detail_id, $shipping->delivery_type)->driver_photo)
                                    <img src="{{ $shipping->getDeliveryDetail($shipping->detail_id, $shipping->delivery_type)->driver_photo }}" height='200' width='auto'>
                                    @else
                                    Not Available
                                    @endif
                                </td>
                            </tr>
                            @elseif($shipping->status == 'on delivery' && $shipping->delivery_type == 'jne')
                            <tr>
                                <td>4.</td>
                                <td>AWB Code</td>
                                <td>
                                    @if(null != $shipping->booking_unique)
                                    {{ $shipping->booking_unique }}
                                    @else
                                    Not Available
                                    @endif
                                </td>
                            </tr>
                            @endif
                            
                        </table>
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->
    </div>
    <!-- nav-tabs-custom -->
    @include('admin.layouts.delete_modal')

    <div class="modal fade" id="awb">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Insert AWB</h4>
                </div>
                <form method="post" action="{{ url('admin/orders/insert-awb/'.$order->id) }}">
                <div class="modal-body">
                    {{ csrf_field() }}
                    <p>AWB number: <input type="text" name="awb_receipt" required/></p>
                    <input type="hidden" value="22"/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-danger" id="modal=submit">Submit</button>
                </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    </div>
    <!-- /.example-modal -->
@endsection

@section('myscript')
    <!-- DataTables -->
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script>
        $("#insert-awb").on('click', function(e){
            console.log($("#insert-awb").text());
            $('#awb').modal({ backdrop: 'static', keyboard: true });
        })
    </script>
@endsection