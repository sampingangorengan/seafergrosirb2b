@extends('admin.layouts.master')

@section('title')
    Order | Control Room
@endsection

@section('content-header')
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))
                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
        @endforeach
    </div>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Order Detail
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ url('admin/orders') }}">Order</a></li>
            <li class="active">View Order</li>
        </ol>
    </section>
@endsection

@section('content')

    <!-- Main content -->
    <section class="invoice">
        <!-- title row -->
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header">
                    @if(null != $order->getShippingAddress($order->user_address_id)->user)
                    <i class="fa fa-globe"></i> {{ $order->getShippingAddress($order->user_address_id)->user->name }}
                    @else
                    <i class="fa fa-globe"></i> {{ $order->getShippingAddress($order->user_address_id)->userById($order->getShippingAddress($order->user_address_id)->user_id)->name }}
                    @endif
                    {{-- <small class="pull-right">Date: {{ LocalizedCarbon::instance($order->updated_at)->addDays(2)->diffForHumans() }}</small> --}}
                </h2>
            </div>
            <!-- /.col -->
        </div>
        <!-- info row -->
        <div class="row invoice-info">
            <div class="col-sm-4 invoice-col">
                From
                <address>
                    <strong>PT Seafermart Jaya Raya</strong><br>
                    Jalan Cumi Raya No. 3, Muara Baru, RT.20/RW.17, <br>
                    Penjaringan, Jakarta Utara, Kota Jkt Utara, <br>
                    Daerah Khusus Ibukota Jakarta 14440, <br>
                    Indonesia<br>
                    {{--Phone: (804) 123-5432<br>
                    Email: info@almasaeedstudio.com--}}
                </address>
            </div>
            <!-- /.col -->
            <div class="col-sm-4 invoice-col">
                To
                <address>
                    @if($order->user_id != 0)
                        @if($order->getShippingAddress($order->user_address_id)->recipient_title == 'f')
                        Mrs.&nbsp;
                        @elseif($order->getShippingAddress($order->user_address_id)->recipient_title == 'fs')
                        Ms.&nbsp;
                        @else 
                        Mr.&nbsp;
                        @endif
                    @else
                        @if($order->getShippingAddress($order->user_address_id)->recipient_title == 'f')
                        Mrs.&nbsp;
                        @elseif($order->getShippingAddress($order->user_address_id)->recipient_title == 'fs')
                        Ms.&nbsp;
                        @else 
                        Mr.&nbsp;
                        @endif
                    @endif

                    
                    
                    @if($order->getShippingAddress($order->user_address_id)->recipient != '')
                    <strong>{{ $order->getShippingAddress($order->user_address_id)->recipient }}</strong><br>
                    @else
                        @if(null != $order->getShippingAddress($order->user_address_id)->user)
                    <strong>{{ $order->getShippingAddress($order->user_address_id)->user->name }}</strong><br>
                        @else 
                    <strong>{{ $order->getShippingAddress($order->user_address_id)->userById($order->getShippingAddress($order->user_address_id)->user_id)->name }}</strong><br>
                        @endif
                    @endif

                    
                    {{ $order->getShippingAddress($order->user_address_id)->address }},<br>

                    
                    @if ($order->getShippingAddress($order->user_address_id)->city->city_name != '')
                    {{ $order->getShippingAddress($order->user_address_id)->area->name }},&nbsp;{{ $order->getShippingAddress($order->user_address_id)->city->city_name }},<br>
                    @else
                    {{ $order->getShippingAddress($order->user_address_id)->area->name }},&nbsp;-- City unavailable --<br>
                    @endif

                    
                    @if (null != $order->getShippingAddress($order->user_address_id)->city->province)
                    {{ $order->getShippingAddress($order->user_address_id)->city->province->province_name_id }},<br>
                    @else
                    -- Province unavailable --<br>
                    @endif
                    Indonesia<br>

                    {{ $order->getShippingAddress($order->user_address_id)->postal_code }}<br>
                    
                    Phone: +62{{ $order->getShippingAddress($order->user_address_id)->phone_number }}<br>
                        @if(null != $order->getShippingAddress($order->user_address_id)->user)
                    Email: {{ $order->getShippingAddress($order->user_address_id)->user->email }}
                        @else 
                    Email:{{ $order->getShippingAddress($order->user_address_id)->userById($order->getShippingAddress($order->user_address_id)->user_id)->email }}
                        @endif
                    
                </address>
            </div>
            <!-- /.col -->
            <div class="col-sm-4 invoice-col">
                {{--<b>Invoice #007612</b><br>
                <br>--}}
                <b>Order ID:</b> {{ $order->id }}<br>
                <b>Payment Due:</b> {{ LocalizedCarbon::instance($order->updated_at)->addDays(1)->format('d F Y H:i:s') }}<br>
                @if(null != $order->getShippingAddress($order->user_address_id)->user)
                <b>Account:</b> {{ $order->getShippingAddress($order->user_address_id)->user->email }}
                @else 
                <b>Account:</b>{{ $order->getShippingAddress($order->user_address_id)->userById($order->getShippingAddress($order->user_address_id)->user_id)->email }}
                @endif
                
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <!-- Table row -->
        <div class="row">
            <div class="col-xs-12 table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Qty</th>
                        <th>Product</th>
                        <th>SKU</th>
                        <th>Description</th>
                        <th>Subtotal</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($order->groceries as $grocery)
                    <tr>
                        <td>{{ $grocery->qty }}</td>
                        <td>{{ $grocery->groceries->name }}</td>
                        <td>{{ $grocery->groceries->sku ? $grocery->groceries->sku : 'No SKU added'  }}</td>
                        <td>{{ str_limit($grocery->groceries->description, 20) }}</td>
                        <td>{{ Money::display($grocery->price) }}</td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <div class="row">
            <!-- accepted payments column -->
            <div class="col-xs-6">
                <p class="lead" style="margin-bottom:5px;">Order STATUS:</p>
                <h2 style="margin-top:0;">{{ ucfirst($order->status->description) }}</h2>
                <p class="lead" style="margin-top:20px;margin-bottom:5px;">Delivery Choice:</p>
                @if ($order->deliveries)
                    <h2  style="margin-top:0;">{{ ucfirst($order->deliveries->delivery_type) }}</h2>
                @else
                    <h2  style="margin-top:0;">Not Available</h2>
                @endif

                @if($order->order_status == 7 || $order->order_status == 4)
                <p class="lead" style="margin-top:20px;margin-bottom:5px;">Delivery Status:</p>
                @if ($order->deliveries)
                    <h2  style="margin-top:0;">{{ ucwords($order->deliveries->status) }}</h2>
                @else
                    <h2  style="margin-top:0;">Not Available</h2>
                @endif
                @endif


                {{--<p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                    Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem plugg
                    dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.
                </p>--}}
            </div>
            <!-- /.col -->
            <div class="col-xs-6">
                @if($order->order_status == 1)
                <p class="lead">Payment time to cancellation <span id="demo" style="color:red"></span></p>
                @endif

                <div class="table-responsive">
                    <table class="table">
                        <tr>
                            <th style="width:50%">Subtotal:</th>
                            <td>{{ Money::display($order->nett_total) }}</td>
                        </tr>
                        <tr>
                            <th style="width:50%">Cashback Redeem:</th>
                            <td>{{ Money::display($order->redeem) }}</td>
                        </tr>
                        <tr>
                            <th style="width:50%">Promo:</th>
                            <td>{{ Money::display($order->promo_value) }}</td>
                        </tr>
                        <tr>
                            <th style="width:50%">PPN (10%):</th>
                            <td>{{ Money::display($order->tax) }}</td>
                        </tr>
                        <tr>
                            <th>Shipping:</th>
                            <td>{{ Money::display($order->shipping_fee) }}</td>
                        </tr>
                        <tr>
                            <th>Total:</th>
                            @if( ($order->nett_total - $order->redeem + $order->tax - $order->promo_value) < 0)
                            <td><span>{{ Money::display($order->shipping_fee) }}</span></td>
                            @else
                            <td><span>{{ Money::display($order->nett_total - $order->redeem + $order->shipping_fee + $order->tax - $order->promo_value) }}</span></td>
                            @endif
                            
                        </tr>
                        <tr>
                            <th>&nbsp;</th>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <th>Unique code:</th>
                            <td>{{ Money::display($order->unique_code) }}</td>
                        </tr>
                        <tr>
                            <th>Grand Total:</th>
                            <td>{{ Money::display($order->grand_total) }}</td>
                        </tr>
                    </table>
                </div>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->

        <!-- this row will not appear when printing -->
        <div class="row no-print">
            <div class="col-xs-12">

                
                {{-- @if($order->order_status == 2)
                    @if($order->deliveries->delivery_type == 'gojek')
                    <a href="{{ url('admin/orders/order-gojek/'.$order->id) }}" style="color:white">
                        <button type="button" class="btn btn-success pull-right" style="margin-right: 5px;">
                            <i class="fa fa-credit-card"></i> Call Gojek For Delivery
                        </button>
                    </a>
                    @elseif($order->deliveries->delivery_type == 'jne')
                        <button id="insert-awb" type="button" class="btn btn-success pull-right insert-awb" style="margin-right: 5px;">
                            <i class="fa fa-credit-card"></i> Insert AWB
                        </button>

                    @endif
                @endif --}}

                <button type="button" class="btn btn-primary pull-right insert-awb" style="margin-right: 5px;">
                    @if($total_user_payment > 0)
                    <a href="{{ url('admin/transactions/'.$order->getFirstUserPayment($order->id)->id) }}" style="color:white">
                        <i class="fa fa-download"></i> View Payment
                    </a>
                    @else
                        @if(null != $order->getPayment($order->id))
                        <a href="{{ url('admin/transactions/'.$order->getPayment($order->id)->id) }}" style="color:white">
                            <i class="fa fa-download"></i> View Payment
                        </a>
                        @else
                        <a href="#" style="color:white">
                            <i class="fa fa-download"></i> View Payment
                        </a>
                        @endif
                    @endif
                </button>

                @if($order->order_status == 7)
                <button type="button" class="btn btn-primary pull-right insert-awb" style="margin-right: 5px;">
                    
                    <a href="{{ url('admin/orders/shipping/'.$order->id) }}" style="color:white">
                        <i class="fa fa-ship"></i> View Shipping
                    </a>
                    
                </button>
                @endif
            </div>
        </div>
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>

    <div class="modal fade" id="awb">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Insert AWB</h4>
                </div>
                <form method="post" action="{{ url('admin/orders/insert-awb/'.$order->id) }}">
                <div class="modal-body">
                    {{ csrf_field() }}
                    <p>AWB number: <input type="text" name="awb_receipt" required/></p>
                    <input type="hidden" value="22"/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-danger" id="modal=submit">Submit</button>
                </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    </div>
    <!-- /.example-modal -->


@endsection

@section('myscript')
    <!-- DataTables -->
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script>
        $("#example1").DataTable();
        $("#insert-awb").on('click', function(e){
            console.log($("#insert-awb").text());
            $('#awb').modal({ backdrop: 'static', keyboard: true });
        })
    </script>
    @if($order->order_status == 1)
    <script>
        // Set the date we're counting down to
        var countDownDate = new Date("{{ LocalizedCarbon::parse($order->updated_at)->addHours(24)->format('F d, Y H:i:s') }}").getTime();

        // Update the count down every 1 second
        var x = setInterval(function() {

        // Get todays date and time
        var now = new Date().getTime();

        // Find the distance between now an the count down date
        var distance = countDownDate - now;

        // Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        // Display the result in the element with id="demo"
        document.getElementById("demo").innerHTML = days + "<span style='color:#589b7'>d</span> - " + hours + "<span style='color:#589b7'>h</span> - "
        + minutes + "<span style='color:#589b7'>m</span> - " + seconds + "<span style='color:#589b7'>s</span> ";

        // If the count down is finished, write some text 
        if (distance < 0) {
        clearInterval(x);
        document.getElementById("demo").innerHTML = "EXPIRED";
        }
        }, 1000);
    </script>
    @endif
@endsection