@extends('admin.layouts.master')

@section('title')
    Order | Control Room
@endsection

@section('content-header')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Order List
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ url('admin/orders') }}">Order</a></li>
            <li class="active">List Order</li>
        </ol>
    </section>
    <br/>
@endsection

@section('content')

    <div class="box">

        <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Order ID</th>
                    <th>Items</th>
                    <th>GRAND TOTAL</th>
                    <th>Delivery</th>
                    <th>Order Status</th>
                    <th>Order Date</th>
                    <th>Options</th>

                </tr>
                </thead>
                <tbody>
                @foreach($orders as $order)
                    <tr>
                        <td>{{ $order->id }}</td>
                        <td>

                            @if($order->groceries->count() > 0)
                            <ul>
                            @foreach($order->groceries as $order_detail)
                                <li>{{ $order_detail->groceries->name }}</li>
                            @endforeach

                            </ul>
                            @endif
                        </td>
                        <td>{{ Money::display($order->grand_total) }}</td>
                        @if ($order->deliveries)
                            <td>{{ ucfirst($order->deliveries->delivery_type) }}</td>
                        @else
                            <td>Not Available</td>
                        @endif
                        <td>{{ ucfirst($order->status->description) }}</td>
                        <td>{{ LocalizedCarbon::parse($order->created_at)->format('d F Y H:i') }}</td>
                        <th>
                            <a href="{{ url('admin/orders/'.$order->id) }}"><button type="button" class="btn btn-info">View</button></a>
                            {{--<button type="button" class="btn btn-primary">Edit</button>
                            <button type="button" class="btn btn-danger">Delete</button>--}}

                        </th>
                    </tr>
                @endforeach

                </tbody>
                <tfoot>
                <tr>
                    <th>Order ID</th>
                    <th>Items</th>
                    <th>GRAND TOTAL</th>
                    <th>Delivery</th>
                    <th>Order Status</th>
                    <th>Order Date</th>
                    <th>Options</th>
                </tr>
                </tfoot>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
    @include('admin.layouts.delete_modal')
@endsection

@section('myscript')
    <!-- DataTables -->
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script>
        $("#example1").DataTable(
            {
                "order": [[ 5, "desc" ]]
            }
        );
        $(".delete-entity-index").on('click', function(e){
            var $form=$(this).closest('form');
            e.preventDefault();
            $('#confirm').modal({ backdrop: 'static', keyboard: false })
                .on('click', '#delete', function() {
                    $form.trigger('submit'); // submit the form
                });
        })
    </script>
@endsection