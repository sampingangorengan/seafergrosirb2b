@extends('layouts.newmaster')

@section('title', App\Page::createTitle('Loyalty Points'))

@section('customcss')
    <link rel="stylesheet" type="text/css" href="{!! asset(null) !!}assets/css/custom.css">
@endsection

@section('content')
    <div id="boxOfContent">
        <div class="row">
            <div class="small-12 columns absolute">
    <span class="text-grey equal margin-top-10">
      <a class="text-grey" href="{{ url('/') }}"><b>HOME</b></a> /
      <a class="text-grey" href="{{ url('loyalty-points') }}"><b>CASHBACK REWARDS</b></a>
    </span>
            </div>
        </div>
        <div class="loyalty-points-box" id="checkout">
            <div class="box-content">
                <div class="row">
                    <div class="small-12 columns">
                        <h5 class="title title-margin">CASHBACK REWARDS</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="small-7 small-centered columns">
                        <center>
                            <div class="img-box">
                                <img src="{!! asset(null) !!}assets/images/loyaltypoint.png">
                            </div>
                        </center>
                        <p>
                            Our Loyalty Points come in a 0.5% cashback for every Rp 100.000,00 order value. You can choose to redeem your points upon checkout or continue accumulating your points and redeem it later on our website or mobile app. There is no expiry, so you can collect your points during your leisure. Happy shopping! <br><br>
                            Read <a href="#">Terms & Conditions</a> for more information.

                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection