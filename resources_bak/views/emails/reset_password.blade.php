@extends('layouts.newmaster')

@section('title', App\Page::createTitle('Forgot Password'))

@section('customcss')
    <style>
        /*button{
            width:300px;
        }*/
        label{
            font-size:14px;
        }
    </style>
@endsection

@section('content')

    <div id="godOfContent">
        <div class="box-content" id="signUp">
            <div class="row">
                <div class="large-12 columns">
                    <h6 class="title">FORGOT PASSWORD</h6>
                </div>
            </div>
            <div class="row">
                <div class="large-3 columns no-padding center">
                    <img class="pink" src="{{ asset(null) }}assets/images/img-swim.png" style="top:-20px">
                </div>

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                    <div class="large-6 columns no-padding">
                        <div class="form-box">

                            <form method="POST" action="{{ url('/password/reset') }}">
                                {!! csrf_field() !!}
                                <input type="hidden" name="token" value="{{ $token }}">

                                @if (count($errors) > 0)
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                @endif

                                <div class="row">
                                    <div class="small-4 columns">
                                        <label for="middle-label" class="text-left middle" style="font-size:13px">EMAIL</label>
                                    </div>
                                    <div class="small-8 columns">
                                        <input type="email" name="email" placeholder="" value="{{ old('email') }}" required style="border: 1px solid #808184;height: 27px;"/>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="small-4 columns">
                                        <label for="middle-label" class="text-left middle" style="font-size:13px">NEW PASSWORD</label>
                                    </div>
                                    <div class="small-8 columns">
                                        <input type="password" name="password" placeholder="password must contain letters and/or numbers only" required>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="small-4 columns">
                                        <label for="middle-label" class="text-left middle" style="font-size:13px">CONFIRM NEW PASSWORD</label>
                                    </div>
                                    <div class="small-8 columns">
                                        <input type="password" name="password_confirmation" placeholder="password must contain letters and/or numbers only" required>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="small-4 small-centered columns">
                                        {{ csrf_field() }}
                                        <button type="submit" class="button primary btn-aneh" style="width:300px">RESET PASSWORD</button>
                                    </div>
                                </div>
                            </form>
                                {{--<div>
                                    Email
                                    <input type="email" name="email" value="{{ old('email') }}">
                                </div>

                                <div>
                                    Password
                                    <input type="password" name="password">
                                </div>

                                <div>
                                    Confirm Password
                                    <input type="password" name="password_confirmation">
                                </div>

                                <div>
                                    <button type="submit">
                                        Reset Password
                                    </button>
                                </div>--}}
                            </form>


                        </div>
                    </div>



                <div class="large-3 columns no-padding center">
                    <img class="blue" src="{{ asset(null) }}assets/images/img-swim2.png">
                </div>
            </div>
        </div>
    </div>

@endsection
