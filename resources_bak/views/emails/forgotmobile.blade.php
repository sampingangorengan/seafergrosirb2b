<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>Hi, {{ $user->name }}!</h2>
<div>
    <p>Youre doing a forgot password here's the new password for your account
        <b>{{ $random }}</b>
    </p>
    <br>
    <p>Love,</p>
    <p>Kirin from SEAFERMART</p>
</div>

<div>
    <p><a href="{{ url('contact-us') }}">Contact Us</a></p>
    <p>This is an automatically generated email – please do not reply to it. If you have any immediate
        queries please email hello@SEAFERMART.co.id</p>
</div>

</body>
</html>