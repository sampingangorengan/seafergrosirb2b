<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>[NO-REPLY] Inquiry Received</h2>

<div>
    Thank you for reaching out to us. We'll reply your message as soon as possible. Meanwhile, do not reply this message as it won't give you answer to your question.
</div>

</body>
</html>