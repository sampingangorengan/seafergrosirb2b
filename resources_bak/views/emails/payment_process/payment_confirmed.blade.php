<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>Hi, {{ $name }}!</h2>

<div>
    <p>Thank you for shopping at SEAFERMART! Your payment has been confirmed and the order is being processed.</p>
    <br/>
    <p>For further assistance, feel free to reach out to us at</p>
    <p>Email: hello@seafermart.co.id</p>
    <p>Line: @SEAFERMART</p>
    <br>
    <br>
    <p>Love,</p>
    <p>Kirin from SEAFERMART</p>
</div>

</body>
</html>