<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>Hi, {{ $user->name }}!</h2>

<div>

    <p>Your account has been verified. You’re registered as {{ $user->name }}.liem@gmail.com. Don’t forget to
        put in your email and password every time you log in to your SEAFERMART account. Now you
        can start shopping with us!</p>
    <br/>
    <p><strong>More friends, more benefits!</strong></p>
    <p>Enjoy up to 1% cashback from your REFERRED friends every time they shop at SEAFERMART.</p>
    <br>
    <p><strong>Here’s why you should shop at SEAFERMART:</strong></p>
    <ul>
        <li>We provide you with the Best Price Guarantee that offers the lowest prices compared to other grocery stores based in Jakarta.*</li>
        <li>You can accumulate points that will later come in a 1% cashback for every Rp 100.000,00 order value.</li>
        <li>There’s no need to worry because we have a huge selection of Japanese grocery for you!</li>
        <li>The quality of our products is best assured because our team are trained to pick the best non-defective products with the highest caliber and to carefully wrap, preserve and place them in secure packaging to ensure they reach you in superior condition.</li>
        <li>We offer you same day delivery because we are committed to delivering the products within the shortest time possible.</li>
    </ul>
    <br/>
    <p><strong>Need help?</strong></p>
    <p>Jump to our Frequently Asked Questions (FAQs) page, call xxxx xxxx or email hello@SEAFERMART.co.id for further assistance</p>
    <p>We look forward to giving you the best online shopping experience!</p>
    <br/>
    <p>Happy Shopping!</p>
    <p>Kirin from SEAFERMART</p>
</div>

<div>
    <p><a href="{{ url('contact-us') }}">Contact Us</a></p>
    <p>This is an automatically generated email – please do not reply to it. If you have any immediate
        queries please email hello@SEAFERMART.co.id</p>
</div>

</body>
</html>