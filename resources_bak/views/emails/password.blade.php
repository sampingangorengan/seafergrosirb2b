{{--
Click here to reset your password: <a href="{{ $link = url('password/reset', $token) }}"> Click me! </a>--}}
<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>Hi, {{ $user->name }}!</h2>

<div>

    <p>Please click this link below to reset your password.</p>
    <p><a href="{{ url('password/reset', $token) }}"> Click me! </a></p>
    <br>
    <p>Love,</p>
    <p>Kirin from SEAFERMART</p>
</div>

</body>
</html>