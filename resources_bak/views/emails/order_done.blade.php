<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>Hi, {{ $user->name }}!</h2>

<div>
    <p>Thank you for shopping at SEAFERMART! Your order has been placed.</p>
    <p>To complete your transaction, please make your payment within 24 hours of checkout via transfer
        through ATM, internet baking, or mobile banking. Otherwise your order will be automatically cancelled.</p>
    <br/>
    <p><strong>Follow the steps below</strong></p>
    <ol>
        <li>Please make your payment to the following account PT. SEAFERMART Jaya Raya – BCA 206 3232 327</li>
        <li>Take a picture of your proof of payment and upload it to Confirm Payment form.</li>
        <li>SEAFERMART will confirm your payment within 24 hours.</li>
        <li>SEAFERMART will notify you after the payment is received.</li>
    </ol>
    <br/>
    <p>For further payment assistance, feel free to reach out to us at</p>
    <p>Email: hello@seafermart.co.id</p>
    <p>Line: @SEAFERMART</p>
    <p>Phone: </p>
    <br>
    <br>
    <p>Love,</p>
    <p>Kirin from SEAFERMART</p>
</div>

</body>
</html>