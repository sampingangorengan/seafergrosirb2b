@extends('layouts.newmaster')

@section('title', App\Page::createTitle('Delivery'))

@section('customcss')
    <link rel="stylesheet" type="text/css" href="{!! asset(null) !!}assets/css/custom.css">
@endsection

@section('content')
    <div id="boxOfContent">
        <div class="row">
            <div class="small-12 columns absolute">
                <span class="text-grey equal margin-top-10">
                  <a class="text-grey" href="{{ url('/') }}"><b>HOME</b></a> /
                  <a class="text-grey" href="{{ url('delivery') }}"><b>DELIVERY</b></a>
                </span>
            </div>
        </div>
        <div class="delivery-box" id="checkout">
            <div class="box-content">
                <div class="row">
                    <div class="small-12 columns">
                        <h5 class="title title-margin">DELIVERY</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="small-7 small-centered columns">
                        <p>
                            We are committed to delivering the products within the shortest time possible. For orders that are placed before 3 pm, we will deliver your orders within the same day with an estimated duration of 4-5 hours from the time the order is placed. For orders that are placed after 3 pm, we will deliver your orders the next day.
                        </p>
                        <br/>
                        <p>
                            Besides relying on our Seafer courier, we are working with Gojek and JNE to help deliver the products to you in the safest and most efficient way.
                        </p>
                        <br/>
                        <p>
                            Before delivery, we also adopt the best practices for packing both frozen and dry products to ensure that the products are delivered to you in the best condition possible.
                        </p>
                        {{-- <div class="sub-content">
                            For Seafer Grocer products
                            <ul>
                                <li>If you order before 10 AM, we will deliver between 12-6 PM.</li>
                                <li>If you order before 2 PM, we will deliver between 4-10 PM.</li>
                                <li>If you order after 2 PM, we will deliver on the next day between 9 AM-5 PM.</li>
                            </ul>
                            <p>
                                For Seafer Eats products<br>
                                Your products will be delivered separately from Seafer Grocer items as the sellers will directly send them to  you. Your delivery schedule for Seafer Eats items will be shown when you check out.
                            </p>
                        </div> 
                        <div class="delivery-img">
                            <img src="{!! asset(null) !!}assets/images/img-gojek.png">
                            <img src="{!! asset(null) !!}assets/images/img-jne.png">
                        </div>
                        <p>
                            Currently we are working with Gojek and JNE to help deliver the products to you.
                        </p>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection