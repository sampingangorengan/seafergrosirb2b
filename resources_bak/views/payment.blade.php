@extends('layouts.newmaster')

@section('title', App\Page::createTitle('Payment'))

@section('customcss')
    <link rel="stylesheet" type="text/css" href="{!! asset(null) !!}assets/css/custom.css">
@endsection

@section('content')
    <div id="boxOfContent">
        <div class="row">
            <div class="small-12 columns absolute">
                <span class="text-grey equal margin-top-10">
                  <a class="text-grey" href="{{ url('/') }}"><b>HOME</b></a> /
                  <a class="text-grey" href="{{ url('delivery') }}"><b>PAYMENT</b></a>
                </span>
            </div>
        </div>
        <div class="delivery-box" id="checkout">
            <div class="box-content">
                <div class="row">
                    <div class="small-12 columns">
                        <h5 class="title title-margin">PAYMENT</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="small-7 small-centered columns">
                        <p>
                            We are committed to delivering the products within the shortest time possible after payment is made using the following steps:
                        </p>
                        <div class="sub-content">

                            <ol>
                                <li>Please make your payment to the following account: PT. Seafermart Jaya Raya – BCA 206 3232 327 / Bank Panin 1535007529</li>
                                <li>Take a picture of your proof of payment and upload it to Confirm Payment form.</li>
                                <li>SeaferMart will notify you after the payment is made.</li>
                                <li>SeaferMart will confirm your payment immediately within 24 hours and process your order.</li>
                                
                            </ol>
                        </div>
                        {{-- <div class="payment-img">
                            <img src="{!! asset(null) !!}assets/images/img-bca.png" class="payment">
                             <img src="{!! asset(null) !!}assets/images/img-mandiri.png" class="payment">
                        </div> --}}
                        <p>
                            We thank you for your timely and responsible payment.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection