@extends('frontend.layout.master')

@section('title', App\Page::createTitle('Groceries'))

@section('customcss')
    <link rel='stylesheet' href='assets/plugin/bootstrap/css/bootstrap-responsive-tabs.css'>
    <link rel="stylesheet" type="text/css" href="assets/plugin/fancybox/css/jquery.fancybox.min.css">
@endsection

@section('content')
	<div class="main-content">
		<div id="productDetail" class="container">
			<div class="row">
	            <div class="col-md-11 col-centered">
	                <div class="top-bar filter-box top-bar-custome">
	                    <div class="pull-left">
	                        <ul class="nav navbar-nav">
	                            <li class="hide-320"><span class="result">{{ $count }} results</span></li>
	                            <li class="dropdown">
									<span class="dropdown-toggle ico-down  filter-list" data-toggle="dropdown"><b>Sort by: </b></span>
									<ul class="dropdown-menu vertical filter-list">
										<li>
											<div class="box">
												<input id="bestSeller" type="radio" class="square" name="a" value="best-seller" onclick="window.location='{{ Request::url() }}?{{ $query_string != '' ?  $query_string.'&' : '' }}orderBy=best-seller'" @if($orderBy == 'best-seller')checked="checked"@endif><label for="bestSeller">Best Sellers</label>
											</div>
										</li>
										<li>
											<div class="box">
												<input id="newIn" type="radio" class="square" name="a" value="latest"  onclick="window.location='{{ Request::url() }}?{{ $query_string != '' ?  $query_string.'&' : '' }}orderBy=latest-entry'" @if($orderBy == 'latest-entry')checked="checked"@endif><label for="newIn">Latest Entry</label>
											</div>
										</li>
										<li>
											<div class="box">
												<input id="highestPrice" type="radio" class="square" name="a" value="highest-price" onclick="window.location='{{ Request::url() }}?{{ $query_string != '' ?  $query_string.'&' : '' }}orderBy=highest-price'" @if($orderBy == 'highest-price')checked="checked"@endif><label for="highestPrice">Highest Price</label>
											</div>
										</li>
										<li>
											<div class="box">
												<input id="lowPrice" type="radio" class="square" name="a" value="low-price" onclick="window.location='{{ Request::url() }}?{{ $query_string != '' ?  $query_string.'&' : '' }}orderBy=lowest-price'" @if($orderBy == 'lowest-price')checked="checked"@endif><label for="lowPrice">Lowest Price</label>
											</div>
										</li>
									</ul>

								</li>
							</ul>
	                    </div>
						<div class="top-bar-right pull-right hide-320" style="width:42%;text-align:right">
							{{-- <div class="col-md-7 col-sm-9 col-xs-12">
								@include('frontend.layout.pagination', ['paginator' => $groceries])
							</div> --}}
						</div>
					</div>
	                <hr class="blue margin-top-0"></hr>
	            </div>
				<div class="col-md-3 col-sm-3 hide-320">
	                <ul class="side-nav list-style" >

                        @if(count(App\Models\Groceries\Category::get()) > 1)
                            @foreach( App\Models\Groceries\Category::orderBy('order', 'asc')->get() as $key=>$category )
                                <li><a href="{{ url('groceries/get-all/'.$category->slug) }}" id="{{ $key }}" @if($activeCategoryId->parent_id == $category->id) class="active" @endif>{{ strtoupper($category->name) }}</a>
                                @if($category->child)
                                    <ul id="sideDropdown{{ $key }}" @if($activeCategoryId->parent_id != $category->id) style="display:none" @endif>
                                        @foreach($category->childByOrder($category->id) as $child_category)
                                        <li style="list-style:none"><a href="{!! url('groceries/' . $child_category->slug) !!}" @if($activeCategoryId->id == $child_category->id) class="active" @endif>{{ $child_category->name}}</a></li>
                                        @endforeach
                                    </ul>
                                @endif
                            @endforeach
                        @else
                            <li><a href="{!! url('groceries/' . $category->slug) !!}" id="{{ strtoupper($category->name) }}">{{ strtoupper($category->name) }}</a>
                        @endif
                    </ul>
	            </div>

				<div class="col-md-7 col-sm-9 col-xs-12">
					@if(count($groceries) > 1)
					@foreach($groceries as $item)
						<div class="col-md-4 col-sm-6 col-xs-6">
							<div class="item-box item-margin-bottom">
								<div class="item large--2">
									@if($item->discount_id != 0)
									<div class="item-sale">
										<img src="assets/images/ico-sale.png">
									</div>
									@endif

									{{--<div class="item-love">
										<img src="assets/images/ico-love-red.png">
									</div>--}}

									<a href="{{ url('groceries/'.$item->id.'/'.str_slug($item->name)) }}">
										<img src="{{ Groceries::getFirstImageUrl($item->id) }}">
									</a>
								</div>
								@if($item->isFavourites() != 1)
									<img src="assets/images/ico-star-border.png" class="favourites add-to-favourites-btn list_add_to_favourite" alt="" data-g-id="{{ $item->id }}"/>
								@else

									<img src="assets/images/ico-star-fill-color.png" class="favourites remove-from-favourites-btn list_remove_from_favourite" alt="" data-g-id="{{ $item->id }}"/>
								@endif
								<a href="{{ url('groceries/'.$item->id.'/'.str_slug($item->name)) }}">
	                                <div class="desc">
	                                    <span><b>{{ $item->name }}, {{ $item->value_per_package }}{{ $item->metric_id != 0 ? $item->metric->abbreviation : 'metric unknown' }}</b></span><br>
	                                </div>
								</a>
								<div class="packing" style="height:40px;">
									{{$item->packing}}
								</div>
								<div class="block-price">

								@if($item->discount_id != 0)
								<span class="before-discount">{{ Money::display($item->price) }}</span>
                                    @if($item->discount->discount_type == 'percent')
                                        @if($item->price - (($item->price * $item->discount->discount_value)/100) >0)
                                        <span class="after-discount"> {{ Money::display( $item->price - (($item->price * $item->discount->discount_value)/100) )}}</span><br>
                                        @else
                                        <span class="after-discount"> {{ Money::display( 0 )}}</span><br>
                                        @endif
                                    @else
                                        @if( ($item->price - $item->discount->discount_value) > 0)
                                        <span class="after-discount"> {{ Money::display( $item->price - $item->discount->discount_value ) }}</span><br>
                                        @else
                                            <span class="after-discount"> {{ Money::display( 0 ) }}</span><br>
                                        @endif
                                    @endif
                                @else
                                <span class="after-discount"> {{ Money::display($item->price)}}</span><br>
								@endif
								</div>
	                            <div class="procat control-cart-{{ $item->id }}">
                                    <?php
                                        $cart_items = Carte::contents();
                                        $is_there = false;
                                        $identifier = '';

                                        foreach($cart_items as $keys=>$prod){

                                            if($item->id == $prod->id){
                                                $is_there = true;
                                                $identifier = $keys;
                                                $cart_item = $prod;
                                                break;
                                            }
                                        }
                                    ?>

                                    <div class="atc-initiate">
										<input class="item_id" type="hidden" value="{{ $item->id }}" />
                                        <input class="item_sku" type="hidden" value="{{ $item->sku ? $item->sku : 'No SKU added' }}" />
                                        <input class="item_name" type="hidden" value="{{ $item->name }}" />

                                        @if($item->discount_id != 0)
                                            @if($item->discount->discount_type == 'percent')
                                                @if($item->price - (($item->price * $item->discount->discount_value)/100))
                                                <input class="item_price" type="hidden" value="{{ $item->price - (($item->price * $item->discount->discount_value)/100) }}" />
                                                @else
                                                <input class="item_price" type="hidden" value="0" />
                                                @endif
                                            @else
                                                @if($item->price - $item->discount->discount_value)
                                                <input class="item_price" type="hidden" value="{{ $item->price - $item->discount->discount_value }}" />
                                                @else
                                                <input class="item_price" type="hidden" value="0" />
                                                @endif
                                            @endif
                                        @else
                                            <input class="item_price" type="hidden" value="{{ $item->price }}" />
                                        @endif

                                        {{--@if($item->metric)
                                            <input class="item_metric" type="hidden" value="{{ $item->value_per_package.' '.$item->metric->abbreviation }}" />
                                        @else
                                            <input class="item_metric" type="hidden" value="{{ $item->value_per_package.' --metric not added--' }}" />
                                        @endif--}}
                                        <input class="item_image" type="hidden"  name="" value="{{ Groceries::getFirstImageUrl($item->id) }}">

                                    </div>
									@if($item->stock > 0)
                                        <button class="button add-to-cart-btn list_add_to_cart" >ADD TO CART</button>

                                    @else
                                        <button class="button add-to-cart-btn" style="color:white;background:#ff0000;" disabled>SOLD OUT</button>
                                    @endif


                                </div>
							</div>
						</div>
					@endforeach
					@else
					<h3 class="text-blue text-center"><b>No Product Available on this category<b></h3>
					@endif
	            </div>
	            @if (count($recent) > 0)
	            <div class="col-md-2 pull-right hide-768">
					<div class="recently-viewed-box">
						<div class="header">
							RECENTLY VIEWED :
						</div>
						<div class="body"><nr><br>
								<ul class="recomendation-box">
									@foreach($recent as $k=>$item)
									@if($item->grocery['id'] != 0)
									<li>
										<a href="{{ url('groceries/'.$item->grocery['id'].'/'.str_slug($item->grocery['name'])) }}">
											<div class="item" style="overflow:hidden">
												<img src="{{ Groceries::getFirstImageUrl($item->grocery['id']) }}">
											</div>
											<span class="text-grey"><b>{{ $item->grocery['name'] }}, {{ $item->grocery['value_per_package'] }}</b></span>
											<span style="color:#A9A9A9;">{{ Money::display($item->grocery['price']) }}/{{ $item->grocery['metric_id'] != 0 ? $item->grocery->metric['abbreviation'] : 'metric unknown' }}</span>
										</a>
									</li>
									@endif
									@endforeach
								</ul>
						</div>
					</div>
	            </div>
	            @endif
				{{-- <div class="col-md-7 col-sm-9 col-xs-12 margin-bottom pull-right" style="text-align:right">
	                @include('frontend.layout.pagination', ['paginator' => $groceries])
	            </div> --}}
	        </div>
		</div>
	</div>
@endsection

@section('scripts')
<script src="assets/plugin/bootstrap/js/jquery.bootstrap-responsive-tabs.min.js"></script>
<script src="assets/plugin/fancybox/js/jquery.fancybox.min.js"></script>
<script>
$('ul.nav li.dropdown').hover(function() {
  $(this).find('.dropdown-menu').stop(true, true).delay(50).fadeIn(500);
}, function() {
  $(this).find('.dropdown-menu').stop(true, true).delay(50).fadeOut(500);
});


</script>
@endsection
