$(document).ready(function(){


	function goTop(){
		$(".gotop .btn-rounded").click(function(){
			var body = $("html, body");
			body.stop().animate({scrollTop:0}, 500, 'swing', function() {
			   // alert("Finished animating");
			});
		});
	}

	$( '<div class="gotop"><div class="btn-rounded"></div></div>' ).insertBefore( $( "footer" ) );

	goTop();

  $(".slick-slider").slick({
    infinite: false,
    dots: true

  });
  function sliderRecipe(){

  		var elem = $(".slide-bigimage"),
        caps = elem.find(".list-slider");
        chooseme = $(".box-nav-slide");
        index = chooseme.find(".page span:eq(0)");
        count = chooseme.find(".page span:eq(1)").html("0"+elem.find(".list-slider").length);

        var zero = elem.find(".list-slider").length;
        if(elem.find(".list-slider").length<10){
        	zero = '0'+zero;
        }

		$('.slide-bigimage').slick({
		  slidesToShow: 1,
		  slidesToScroll: 1,
		  arrows: true,
		  asNavFor: '.slider-desc',
		  prevArrow: ".arrow .arr-prev",
      	  nextArrow: ".arrow .arr-next",
		});
		$('.slider-desc').slick({
		  slidesToShow: 1	,
		  slidesToScroll: 1,
		  asNavFor: '.slide-bigimage',
		  fade:true,
		  arrows:false
		});

		elem.on('beforeChange', function(slick, direction, currentSlide, nextSlide){
        // animCaption("in", nextSlide);

	        var zerobefore = parseInt(nextSlide)+1;
	        if(zerobefore<10){
	        	zerobefore = '0'+zerobefore;
	        }
	        index.html(zerobefore);
         });
	}
	sliderRecipe();

	function sliderProduk(){
		$('.slider-produk').slick({
		  slidesToShow: 1,
		  slidesToScroll: 1,
		  arrows:true,
		  prevArrow: ".arrow-prod .arrow-prev",
      	  nextArrow: ".arrow-prod .arrow-next",
		});
	}
	sliderProduk();

	function sliderProdDet(){
		$(".prod-detail .thumbnail-box .item").on('click', function(){
			var tar = $(this).find('a').attr('targethref');
			$(this).parents(".prod-detail").find(".xlarge a").attr('href', tar);
			if($(this).hasClass('video')){
				$(this).parents(".prod-detail").find(".xlarge a").addClass('video');
			}else{
				$(this).parents(".prod-detail").find(".xlarge a").removeClass('video');
			}
		})
	}
	sliderProdDet();



});

$(window).scroll(function() {
    var height = $(window).scrollTop();

	if ($(window).width() < 768) {
	    if(height  > 50) {
	        $(".top-bar").addClass("shadow");
	        $(".boxDownloadApp").addClass("hide");
	    }else{
	    	$(".top-bar").removeClass("shadow");
	        $(".boxDownloadApp").removeClass("hide");
	    }
	}

	if(height  > 100) {
		$(".gotop").fadeIn();
	}else{
		$(".gotop").fadeOut();
	}
});
