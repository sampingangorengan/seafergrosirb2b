<?php

namespace App;

class Page
{
    public static function createTitle($title)
    {
        return $title.' | Foodis';
    }
}
