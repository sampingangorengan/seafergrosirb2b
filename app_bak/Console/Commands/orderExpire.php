<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use App\Models\Order;
use App\Models\Order\Payment;

class orderExpire extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:expire';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will change order with status of awaiting payment into canceled if the status have not change in 24 hours';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function getOrderRecords()
    {
        return DB::table('orders')->where('order_status', 1)->where(DB::raw('HOUR(TIMEDIFF(NOW(), `updated_at`))'),'>', 24)->get();
        /*return DB::table('orders')->where('order_status', 1)->where(DB::raw('HOUR(TIMEDIFF(NOW(), `updated_at`))'),'>', 24)->update(['order_status' => 5, 'updated_at' => date('Y-m-d H:i:s')]);*/
    }

    public function getPayments(Order $order)
    {
        $payment = Payment::where('order_id', $order->id)->orderBy('updated_at', 'desc')->get();

        if($payment->count() > 0)
        {
            return $payment;
        }

        return false;
    }

    public function returnPaymentToCashback(Order $order) {
        return 'ok';
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        return DB::table('orders')->where('order_status', 1)->where(DB::raw('HOUR(TIMEDIFF(NOW(), `updated_at`))'),'>', 24)->update(['order_status' => 5, 'updated_at' => date('Y-m-d H:i:s')]);
    }

    public function handleBackup() {
        $order_list = $this->getOrderRecords();

        if($order_list->count() > 0) {
            foreach($order_list as $order) {

                $payments = $this->getPayments($order->id);
                if($payments) {
                    if($payments->count() > 0) {
                        $payment_order = array();
                        foreach($payments as $payment) {
                            if($payment->is_approved == 2) {

                                # approve payment
                                $payment->is_approved = 1;
                                $payment->save();

                                #approve order
                                $order->order_status = 7;
                            } elseif($payment->is_approved == 3) {
                                # approve payment
                                $payment->is_approved = 0;
                                $payment->save();

                                #approve order
                                $order->order_status = 7;
                            }
                        }
                    } else {
                        if($payments->is_approved == 2) {
                                # approve payment
                                $payments->is_approved = 1;
                                $payments->save();

                                #approve order
                                $order->order_status = 7;
                            } elseif($payments->is_approved == 3) {
                                # approve payment
                                $payments->is_approved = 0;
                                $payments->save();

                                #approve order
                                $order->order_status = 7;
                            }
                    }
                } else {
                    $order->order_status = 5;
                    $order->save();
                }
            }
        }
    }
}
