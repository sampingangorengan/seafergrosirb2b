<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Delivery;
use App\Models\GojekDetail;
use App\Models\Order;
use App\Models\Notification;
use GuzzleHttp\Client;
use DB;

class UpdateGojekStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'gojek:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update gojek status of shipment every minute';

    private function getGojekStatus($order_number) {
        $client = new Client([
            // Base URI is used with relative requests
            'base_uri' => 'https://kilat-api.gojekapi.com',
            // You can set any number of default request options.
            'timeout'  => 60.0,
        ]);

        $headers = ['Client-ID' => env('GOJEK_CLIENT'), 'Pass-Key' => ENV('GOJEK_PASSKEY'), 'Content-Type' => 'application/json'];
        try {
            $res = $client->request('GET', '/gojek/v2/booking/orderno/'.$order_number, compact('headers'));
        }
        catch (\Exception $e) {
            return null;
        }

        $response = json_decode($res->getBody()->getContents());

        return $response;
    }

    private function updateGojekDetail(Order $order, $id, $gojek_status) {

        $status = DB::table('gojek_details')
            ->where('id', $id)
            ->update([
                'gojek_id' => $gojek_status->id,
                'gojek_order' => $gojek_status->orderNo,
                'status' => $gojek_status->status,
                'driver_id' => $gojek_status->driverId,
                'driver_name' => $gojek_status->driverName,
                'driver_phone' => $gojek_status->driverPhone,
                'driver_photo' => $gojek_status->driverPhoto,
                'total_price' => $gojek_status->totalPrice,
                #'receiver_name' => $gojek_status->receiver_name,
                'order_created_time' => $gojek_status->orderCreatedTime,
                'order_dispatch_time' => $gojek_status->orderDispatchTime,
                'order_arrival_time' => $gojek_status->orderArrivalTime,
                'order_closed_time' => $gojek_status->orderClosedTime,
                'seller_address_name' => $gojek_status->sellerAddressName,
                'seller_address_detail' => $gojek_status->sellerAddressDetail,
                'buyer_address_name' => $gojek_status->buyerAddressName,
                'buyer_address_detail' => $gojek_status->buyerAddressDetail,
                'cancel_description' => $gojek_status->cancelDescription,
                'booking_type' => $gojek_status->bookingType,
                'store_order_id' => $order->id
            ]);

        if($gojek_status->status == 'Completed') {
            $delivery = DB::table('deliveries')->where('detail_id', $id)->update(['status' => 'delivered','updated_at' => date('Y-m-d H:i:s')]);

            $order_item = Order::find($order->id);
            $order_item->order_status = 4;
            $order_item->save();

            $notif = new Notification;
            $notif->user_id = $order->user_id;
            $notif->title = 'Order Delivery Update';
            $notif->description = 'Your order has been delivered';
            $notif->link = url('orders/'.$order->id);
            $notif->is_read = 0;
            $notif->save();
        } elseif($gojek_status->status == 'Cancelled') {
            $delivery = DB::table('deliveries')->where('detail_id', $id)->update(['status' => 'cancelled','updated_at' => date('Y-m-d H:i:s')]);

            /*$order_item = Order::find($order->id);
            $order_item->order_status = 5;
            $order_item->save();*/

            /*$notif = new Notification;
            $notif->user_id = $order->user_id;
            $notif->title = 'Order Delivery Update';
            $notif->description = 'Your order has been cancelled';
            $notif->link = url('orders/'.$order->id);
            $notif->is_read = 0;
            $notif->save();*/
        } elseif($gojek_status->status == 'Rejected') {
            $delivery = DB::table('deliveries')->where('detail_id', $id)->update(['status' => 'on delivery','updated_at' => date('Y-m-d H:i:s')]);

            /*$order_item = Order::find($order->id);
            $order_item->order_status = 5;
            $order_item->save();*/

            /*$notif = new Notification;
            $notif->user_id = $order->user_id;
            $notif->title = 'Order Delivery Update';
            $notif->description = 'Your order failed its delivery';
            $notif->link = url('orders/'.$order->id);
            $notif->is_read = 0;
            $notif->save();*/
        } 

        return $status;
    }

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $deliveries = Delivery::where('status', 'on delivery')->where('delivery_type', 'gojek')->get();
        foreach($deliveries as $delivery) {
            $detail = $delivery->getDeliveryDetail($delivery->detail_id, $delivery->delivery_type);
            if(null != $detail) {
                $detail_gjk = $this->getGojekStatus($detail->gojek_order);
                if(null !== $detail_gjk) {
                    $stat = $this->updateGojekDetail($delivery->order, $delivery->detail_id, $detail_gjk);
                }
            }
        }
    }
}
