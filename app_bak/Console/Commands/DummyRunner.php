<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\User;

class DummyRunner extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dummy:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Running all custom dummy commands';

    private function getDef() {
        return 'def';
    }

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function getUser() {
        return new User;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $i = User::first();
        var_dump($this->getUser()->first()->email);
        var_dump($this->getDef());
        return 'This is the dummy handler';
    }
}
