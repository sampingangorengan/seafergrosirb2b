<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Order\Payment;
use DB;

class ThreeDaysPaymentNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notify:threedays';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notify admin to check the payment that needs 3 working days to be received';

    private function sendEmailNotification(Payment $payment) {

        if(null == $payment) {
            return false;
        }

        $data = array(
            'name' => $payment->order->user->name,
            'order_number' => $payment->order->id,
            'payment_id' => $payment->id
        );

        try{
            $status = Mail::send('emails.payment_process.threedays_notification', $data, function ($message) {

                $message->from("hello@seafermart.co.id", "Seafermart Payment System");
                $message->to('cs@seafermart.co.id')->subject("Payment Checking Notice - 3 days approval");

            });
        } catch (\Exception $exception) {
            $status = false;
        }

        return $status;
    }

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $payments = Payment::where('payment_status_id', 6)->where('notify_admin', 1)->where(DB::raw('HOUR(TIMEDIFF(NOW(), `updated_at`))'),'>', 72)->get();
        
        if($payments->count() > 0){
            foreach($payments as $payment) {
                $status = $this->sendEmailNotification($payment);
                if($status) {
                    $payment->notify_admin = 0;
                    $payment->save();
                }
            }
        }
    }
}
