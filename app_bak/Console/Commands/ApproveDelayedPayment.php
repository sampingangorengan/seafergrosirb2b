<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Order\Payment;
use App\Models\Order;

class ApproveDelayedPayment extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'payment:ucapproval';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Approving payment which the remaining paid amount is less than or equal to the unique code';

    private function sendEmailNotification(Payment $payment) {

        if(null == $payment) {
            return false;
        }

        $data = array(
            'name' => $payment->order->user->name,
            'email' => $payment->order->user->email,
        );

        try{
            $status = Mail::send('emails.payment_process.payment_confirmed', $data, function ($message) use ($data) {

                $message->from("hello@seafermart.co.id", "Seafermart Inquiry");
                $message->to($data['email'])->subject("Payment Confirmed");

            });
        } catch (\Exception $exception) {
            $request->session()->flash('alert-danger', 'Failed to send your success email. Please try again in a few moments!');

            $status = false;
        }

        return $status;
    }

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $payments = Payment::where('payment_status_id', 5)->where(DB::raw('HOUR(TIMEDIFF(NOW(), `updated_at`))'),'>', 24)->get();
        
        if($payments->count() > 0){
            foreach($payments as $payment) {
                
                    $payment->payment_status_id = 2;
                    $payment->is_approved = 1;
                    $payment->save();

                    $status = $this->sendEmailNotification($payment);
                    if($status) {
                        $order = $payment->order;
                        $order->order_status = 7;
                        $order->save();
                    }
            }
        }
    }
}
