<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Order;

class JneTimeCompletion extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:jnecomplete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set order with jne as delivery method to complete after 3 days of on delivery status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $orders = Order::where('order_status', 7)->get();

        foreach($orders as $order) {
            if($order->deliveries->status == 'on delivery' && $order->deliveries->delivery_type == 'jne') {
                $expired_date = strtotime($order->deliveries->updated_at) + 259200;
                $now = strtotime(date('Y-m-d H:i:s'));
                if ( $expired_date <= $now ) {
                    echo $order->id;
                    $order->order_status = 4;
                    $order->save();

                    $order->deliveries->status = 'delivered';
                    $order->deliveries->save();
                }
            } elseif ($order->deliveries->status == 'on delivery' && $order->deliveries->delivery_type == 'gojek') {
                $expired_date = strtotime($order->deliveries->updated_at) + 86400;
                $now = strtotime(date('Y-m-d H:i:s'));
                if ( $expired_date <= $now ) {
                    echo $order->id;
                    $order->order_status = 4;
                    $order->save();

                    $order->deliveries->status = 'delivered';
                    $order->deliveries->save();
                }
            } else {
                $expired_date = strtotime($order->deliveries->updated_at) + 86400;
                $now = strtotime(date('Y-m-d H:i:s'));
                if ( $expired_date <= $now ) {
                    echo $order->id;
                    $order->order_status = 4;
                    $order->save();

                    $order->deliveries->status = 'delivered';
                    $order->deliveries->save();
                }
            }
        }

        return true;
    }
}
