<?php

namespace App\Libraries;

use GuzzleHttp\Client;

class Gosend
{
    public static function getFee($originLatLong, $destLatLong, $serviceName)
    {
        $headers = ['Client-ID' => 'seafermart-engine', 'Pass-Key' => 'U2VhZmVybWFydFN0QGchbmc=', 'Content-Type' => 'application/json'];
        $body = '{
            "routeRequests": [
                {
                    "serviceType":"10",
                    "originLatLong":"'.$originLatLong.'",
                    "destinationLatLong":"'.$destLatLong.'"
                }],
                "shipment_method":"'.$serviceName.'"
        }';

        $client = new Client;
        $res = $client->request('POST', 'https://go-kilat-staging.gojekapi.com/gojek/v2/calculate/price', compact('headers', 'body'));
        return json_decode($res->getBody())->totalPrice;
    }
}
