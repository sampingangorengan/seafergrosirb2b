<?php

namespace App\Libraries;

class Money
{
    public function display($number)
    {
        return 'Rp '.number_format($number, 0, '', '.');
    }

    public function onlyNominal($number){
        return number_format($number, 0, '', '.');
    }
}
