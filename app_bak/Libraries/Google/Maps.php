<?php

namespace App\Libraries\Google;

use GuzzleHttp\Client;

class Maps
{
    public static function getLatLong($addressText)
    {
        $url = 'http://maps.googleapis.com/maps/api/geocode/json?sensor=true&address='.$addressText;
        $client = new Client;
        $res = $client->request('GET', $url);
        $results = json_decode($res->getBody())->results;
        if (count($results) == 0) {
            return false;
        }

        return $results[0]->geometry->location->lat.','.$results[0]->geometry->location->lng;
    }
}
