<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        app()->bind('cart', function() {
           return new \App\Models\Cart;
        });

        app()->bind('category', function() {
           return new \App\Models\Groceries\Category;
        });
        
        app()->bind('groceries', function() {
           return new \App\Models\Groceries;
        });
        
        app()->bind('money', function() {
           return new \App\Libraries\Money;
        });

        app()->bind('order', function() {
            return new \App\Models\Order;
        });
    }
}
