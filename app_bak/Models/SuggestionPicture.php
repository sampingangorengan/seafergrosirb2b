<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SuggestionPicture extends Model
{
    //
    protected $table = 'suggestion_pictures';
    protected $fillable = ['file'];

    protected $uploads = '/suggestion_picture/';

    public function getFileAttribute($photo) {
        return $this->uploads.$photo;
    }
}
