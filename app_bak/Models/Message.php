<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $table = 'messages';

    protected $fillable = ['from_id','to_id','content', 'subject', 'is_read'];

    public function sender() {
        return $this->belongsTo('App\Models\MessageUser');
    }
}


