<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MessageUser extends Model
{
    protected $table = 'mirrormx_customer_chat_user';

    protected $fillable = ['name','mail','password','image','info','roles','last_activity'];
}
