<?php

namespace App\Models\Groceries;

use Illuminate\Database\Eloquent\Model;

class Love extends Model
{
    protected $fillable = ['groceries_id', 'user_id'];
    protected $table = 'groceries_loves';
}
