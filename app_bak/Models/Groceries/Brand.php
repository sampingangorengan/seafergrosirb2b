<?php

namespace App\Models\Groceries;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    //
    use Sluggable;

    protected $fillable = ['name', 'slug', 'is_active'];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name',
                'unique' => 'true'
            ]
        ];
    }
}
