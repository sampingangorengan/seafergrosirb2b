<?php

namespace App\Models\Groceries;

use Illuminate\Database\Eloquent\Model;

class Groceries_poster extends Model
{
    protected $table = 'groceries_poster';

    protected $fillable = ['order', 'file','created_at','updated_at'];

    protected $uploads = '/groceries_poster/';

    public function getFileAttribute($photo) {
        /*if(strpos($photo, 'http')){
            return $photo;
        } else {
            return $this->uploads.$photo;
        }*/
        return $photo;
    }
}
