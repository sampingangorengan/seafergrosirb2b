<?php

namespace App\Models\Groceries;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $table = 'groceries_images';

    protected $fillable = ['file_name', 'groceries_id'];
}
