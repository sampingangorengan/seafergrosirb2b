<?php

namespace App\Models\Groceries;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    //
    protected $table = 'ratings';

    protected $fillable = ['rate', 'title', 'review', 'groceries_id', 'user_id', 'is_active'];

    public function grocery(){
        return $this->belongsTo('App\Models\Groceries');
    }

    public function user(){
        return $this->belongsTo('App\Models\User');
    }

    public function scopeOfRate($query, $rate){
        return $query->where('rate', $rate)->where('is_active', 1);
    }
}
