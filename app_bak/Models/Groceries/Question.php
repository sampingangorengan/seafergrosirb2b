<?php

namespace App\Models\Groceries;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $table = 'groceries_questions';
}
