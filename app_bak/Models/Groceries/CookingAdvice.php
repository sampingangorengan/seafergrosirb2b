<?php

namespace App\Models\Groceries;

use Illuminate\Database\Eloquent\Model;

class CookingAdvice extends Model
{
    protected $table = 'cooking_advices';

    protected $fillable = ['name', 'created_at', 'updated_at'];

    public function groceries() {
        return $this->belongsToMany('App\Models\Groceries', 'groceries_cooking_advice', 'cooking_advice_id', 'groceries_id');
    }
}
