<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BestPriceClaim extends Model
{
    //
    protected $table = 'best_price_claims';

    protected $fillable = ['first_name', 'last_name', 'email', 'item_name', 'order_id', 'seafer_price', 'seafer_link', 'other_price', 'other_link', 'store_location', 'claim_screenshot'];

    public function photo() {
        /*return $this->belongsTo('App\Models\User\ProfilePicture');*/
        return $this->hasOne('App\Models\ClaimScreenshot','id' ,'claim_screenshot');
    }
}
