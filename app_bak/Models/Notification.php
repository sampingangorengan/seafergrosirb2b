<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $table = 'notifications';

    protected $fillable = ['user_id', 'description', 'link', 'is_read', 'created_at', 'updated_at'];

    public function user() {
    	return $this->belongsTo('App\Models\User');
    }
}
