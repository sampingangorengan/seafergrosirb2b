<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Promo extends Model
{
    //
    use Sluggable;

    protected $table = 'promos';

    protected $fillable = ['title', 'slug', 'code', 'description', 'start_date', 'end_date', 'quota', 'quota_used', 'discount_value', 'promo_type'];

    public function isRunning($id){

        $promo = $this->find($id);

        if($promo) {
            $start = strtotime($promo->start_date);
            $end = strtotime($promo->end_date);

            $now = strtotime('now');
            if( $now > $start && $now < $end) {
                return 'RUNNING';
            }
        }

        return 'INACTIVE';


    }

    /**
     * Return the sluggable configuration arra  y for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'code',
                'unique' => 'true'
            ]
        ];
    }
}
