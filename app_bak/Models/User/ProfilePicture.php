<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

class ProfilePicture extends Model
{
    //
    protected $table = 'profile_pictures';
    protected $fillable = ['file','created_at','updated_at'];

    protected $uploads = '/profile_picture/';

    public function getFileAttribute($photo) {
        /*if(strpos($photo, 'http')){
            return $photo;
        } else {
            return $this->uploads.$photo;
        }*/
        return $photo;
    }

}
