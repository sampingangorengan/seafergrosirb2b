<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

class CashbackPoint extends Model
{
    //
    protected $table = 'cashback_points';

    protected $fillable = ['user_id', 'nominal'];

    public function user() {
        return $this->belongsTo('App\Models\User');
    }
}
