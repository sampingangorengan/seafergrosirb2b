<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

class ViewHistory extends Model
{
    //
    protected $table = 'view_histories';

    protected $fillable = ['identifier', 'is_session', 'is_uid'];

    public function getUser() {

    }

    public function getSession() {

    }

    public function grocery() {
        return $this->hasOne('App\Models\Groceries', 'id', 'groceries_id');
    }
}
