<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DonationVoucher extends Model
{
    //
    protected $table = 'donation_vouchers';

    protected $fillable = ['title', 'slug', 'code', 'description', 'start_date', 'end_date', 'is_active', 'quota', 'quota_used', 'value', 'value_type', 'value_collected'];

    public function isRunning($id){

        $voucher = $this->find($id);

        if($voucher) {
            $start = strtotime($voucher->start_date);
            $end = strtotime($voucher->end_date);

            $now = strtotime('now');
            if( $now > $start && $now < $end) {
                if($voucher->is_active == 1){
                    return 'RUNNING';    
                } else {
                    return 'INACTIVE';
                }
            } else {
                return 'OUT OF TIME RANGE';
            }
        } else {
            return 'Can\'t find this voucher information.';
        }

    }

    public function orders() {
        return $this->hasMany('App\Models\Order');
    }
}
