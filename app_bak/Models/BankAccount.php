<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BankAccount extends Model
{
    //
    protected $table = 'bank_accounts';

    protected $fillable = ['bank_name', 'bank_short', 'account_number', 'account_owner'];
}
