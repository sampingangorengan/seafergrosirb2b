<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword, SoftDeletes;


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password', 'dob', 'phone_number', 'profile_picture', 'sex', 'role','registration_code', 'is_active', 'facebook_id', 'google_id', 'referral_code', 'refer_from', 'deleted_at'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public function photo() {
        /*return $this->belongsTo('App\Models\User\ProfilePicture');*/
        return $this->hasOne('App\Models\User\ProfilePicture','id' ,'profile_picture');
    }

    public function roles() {
        return $this->belongsTo('App\Models\Role', 'role', 'id');
    }

    public function isAdmin() {

        if ($this->roles->id == 1)
            return true;

        return false;
    }

    public function banks(){
        return $this->hasMany('App\Models\User\UserBank');
    }

    public function points() {
        return $this->hasOne('App\Models\User\CashbackPoint');
    }

    public function rewards() {
        return $this->hasMany('App\Models\User\CashbackReward');
    }

    public function orders() {
        return $this->hasMany('App\Models\Order');
    }

    public function addresses() {
        return $this->hasMany('App\Models\User\Address');
    }

    public function email_preference() {
        return $this->hasOne('App\Models\User\EmailPreference', 'user_id', 'id');
    }
}
