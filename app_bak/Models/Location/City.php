<?php

namespace App\Models\Location;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = 'loc_cities';

    public function province()
    {
        return $this->belongsTo(\App\Models\Location\Province::class);
    }
}
