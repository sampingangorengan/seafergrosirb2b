<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
    //
    protected $table = 'discounts';

    protected $fillable = ['title', 'description', 'start_date', 'end_date'];

    public function isRunning($id){

        $discount = $this->find($id);

        if($discount) {
            $start = strtotime($discount->start_date);
            $end = strtotime($discount->end_date);

            $now = strtotime('now');
            if( $now > $start && $now < $end) {
                return 'RUNNING';
            }
        }

        return 'INACTIVE';


    }
}
