<?php

namespace App\Models;

use App\Models\Order\OrderStatus;
use App\Models\Order\Payment;
use App\Models\User\Address;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public function isPaid($id)
    {
        return Payment::whereOrderId($id)->where('is_approved', '<>', 2)->orderBy('created_at', 'desc')->first() ? true : false;
    }

    public function getPayment($id)
    {
        //return $this->hasMany('App\Models\Order\Payment');
        return Payment::whereOrderId($id)->where('user_confirmed', 0)->orderBy('created_at', 'desc')->first();
    }

    public function getFirstUserPayment($id)
    {
        //return $this->hasMany('App\Models\Order\Payment');
        return Payment::whereOrderId($id)->where('user_confirmed', 1)->orderBy('updated_at', 'desc')->first();
    }

    public function payment() {
        return $this->hasMany('App\Models\Order\Payment');
    }

    public function status(){
        return $this->belongsTo('App\Models\Order\OrderStatus', 'order_status', 'id');
    }

    public function groceries()
    {
        return $this->hasMany('App\Models\Order\Groceries');
    }

    public function getItemTotal($id)
    {
        $order = self::find($id);
        $total = 0;
        foreach ($order->groceries as $orderGroceries) {
            $total += $orderGroceries->price * $orderGroceries->qty;
        }

        return $total;
    }

    public function getTotal($id)
    {
        $order = self::find($id);
        return $this->getItemTotal($id) + $order->shipping_fee;
    }

    public function user() {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function getShippingAddress($id){
        $address = Address::find($id);
        #dd($address);
        return $address;
    }

    public function scopeOfUser($query, $user)
    {
        $addressIds = Address::whereUserId($user->id)->lists('id');
        return $this->whereIn('user_address_id', $addressIds);
    }

    public function deliveries() {
        return $this->hasOne('App\Models\Delivery');
    }

    public function donations()
    {
        return $this->hasMany('App\Models\DonationVoucher');
    }
}
