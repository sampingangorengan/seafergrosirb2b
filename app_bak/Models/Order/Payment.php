<?php

namespace App\Models\Order;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table = 'order_payments';

    protected $fillable = ['order_id', 'method', 'transfer_dest_account', 'transfer_sender_account_number', 'transfer_sender_account_name', 'transfer_date', 'transfer_time', 'transfer_note', 'transfer_receipt_file', 'is_approved', 'transfer_amount'];

    public function order() {
        return $this->belongsTo('App\Models\Order', 'order_id');
    }

    public function proof() {
        return $this->hasOne('App\Models\PaymentProof', 'id', 'transfer_receipt_file');
    }

    public function status() {
        return $this->hasOne('App\Models\PaymentStatus', 'code', 'payment_status_id');
    }
}
