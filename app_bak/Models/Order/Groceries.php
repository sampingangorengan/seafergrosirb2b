<?php

namespace App\Models\Order;

use App\Models\Groceries\Rating;
use Illuminate\Database\Eloquent\Model;

class Groceries extends Model
{
    protected $table = 'order_groceries';

    protected $fillable = ['is_reviewed', 'rating'];

    public function groceries()
    {
        return $this->belongsTo(\App\Models\Groceries::class);
    }

    public function rate(){
        return $this->belongsTo('App\Models\Groceries\Rating', 'groceries_id', 'groceries_id');
    }

    public static function getRateByUser($user_id, $groceries_id)
    {
        
        $rate = Rating::where('user_id', $user_id)->where('groceries_id', $groceries_id)->first();

        if(null != $rate) {
        	return $rate;
        }

        return false;
    }
}
