<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentProof extends Model
{
    //
    protected $table = 'payment_proofs';
    protected $fillable = ['file'];

    protected $uploads = '/payment_proof/';

    public function getFileAttribute($photo) {
        /*if(strpos($photo, 'http')){
            return $photo;
        } else {
            return $this->uploads.$photo;
        }*/
        return $photo;
    }
}
