<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
use Jenssegers\Agent\Agent;
use Illuminate\Support\Facades\Mail;
use App\Models\Country;
use Knp\Snappy\Pdf;



Route::get('home', function() {
    return redirect('/');
});

/*
    
    START: Responsive html to php porting routes

 */
Route::get('resp/home', function() {
    
    return view('responsive.home');
});
Route::get('resp/about', function() {
    echo 'Hello World';
    die();
    #return view('responsive.about');
});
Route::get('resp/add_bank_account', function() {
    
    return view('responsive.user.add_bank_account');
});
Route::get('resp/add_new_address', function() {
    
    return view('responsive.user.add_new_address');
});
Route::get('resp/become_member', function() {
    
    return view('responsive.become_member');
});
Route::get('resp/best_price', function() {
    
    return view('responsive.best_price');
});
Route::get('resp/cart', function() {
    
    return view('responsive.order.cart');
});
Route::get('resp/cashback_balance', function() {
    
    return view('responsive.user.cashback_balance');
});
Route::get('resp/cashback_rewards', function() {
    
    return view('responsive.cashback_rewards');
});
Route::get('resp/contact', function() {
    
    return view('responsive.contact_us');
});
Route::get('resp/delivery', function() {
    
    return view('responsive.delivery');
});
Route::get('resp/faq', function() {
    
    return view('responsive.faq');
});
Route::get('resp/my_account', function() {
    
    return view('responsive.user.my_account');
});
Route::get('resp/my_address_book', function() {
    
    return view('responsive.user.my_address_book');
});
Route::get('resp/my_bank_account', function() {
    
    return view('responsive.user.my_bank_account');
});
Route::get('resp/my_email', function() {
    
    return view('responsive.user.my_email');
});
Route::get('resp/my_order', function() {
    
    return view('responsive.user.my_order');
});
Route::get('resp/my_order_detail', function() {
    
    return view('responsive.user.my_order_detail');
});
Route::get('resp/my_profile', function() {
    
    return view('responsive.user.my_profile');
});
Route::get('resp/my_review', function() {
    
    return view('responsive.user.my_review');
});
Route::get('resp/payment', function() {
    $agent = new Agent();
    if ($agent->isDesktop()) {
        if($agent->isAndroidOS()) {
            return view('responsive.payment');
        }
        return view('responsive.payment');
    } else {
        return view('responsive.payment');
    }
});
Route::get('resp/groceries', function() {
    
    return view('responsive.groceries.index_groceries');
});
Route::get('resp/grocery', function() {
    
    return view('responsive.groceries.show_grocery');
});
Route::get('resp/return', function() {
    
    return view('responsive.return_exchange');
});
Route::get('resp/signup', function() {
    
    return view('responsive.signup');
});
Route::get('resp/suggestion', function() {
    
    return view('responsive.suggestion');
});
Route::get('resp/thanks', function() {
    
    return view('responsive.thankyou');
});
/*
    
    END: Responsive html to php porting routes

 */

Route::get('country', function() {
    
    $countries = Countries::getList('en', 'json');
    $decoded_countries = json_decode($countries);
    
    foreach($decoded_countries as $key=>$c) {
        $country = new Country;
        $country->code = $key;
        $country->name = $c;
        $country->save();
        echo $c.' saved';
        echo '<br/>';
        unset($country);
    }
});

// Password reset link request routes...
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

Route::get('search', 'GroceriesController@search');
Route::post('search', 'GroceriesController@search');


Route::controller('order/payment', 'Order\PaymentController');
Route::get('groceries/get-all/{parent}', 'GroceriesController@parentcategory');
// Item page
Route::get('groceries/{id}/{name}', 'GroceriesController@show');


Route::get('groceries/all', 'GroceriesController@showall');
Route::get('groceries/all_data', 'GroceriesController@getAjaxSearchAutocomplete');

// Category page

Route::get('groceries/{category_name}', 'GroceriesController@category');


// Item list
Route::get('groceries', 'GroceriesController@index');

Route::get('groceries-to-love', function() {
    App\Models\Groceries\Love::firstOrCreate([
        'groceries_id' => request('id'),
        'user_id'      => auth()->user()->id
    ]);
    
    return back();
});

Route::post('kirin/get_questions/{identifier}', 'GroceriesController@postAjaxQuestion');
Route::post('groceries/question/{id}', 'GroceriesController@postQuestion');

Route::post('promo', 'GroceriesController@postPromoCode');
Route::post('donation', 'GroceriesController@postDonation');
Route::post('referral', 'GroceriesController@postReferralCode');

Route::resource('reviews', 'RatingController');
Route::resource('wholesale', 'WholesaleController');

Route::get('orders/{id}', 'OrderController@show');
Route::get('orders/{id}/download', 'OrderController@download');
Route::get('orders/showPrint/{id}', 'OrderController@showPrint');
Route::get('orders', function() {
    if (!auth()->check()) {
        return redirect('user/sign-up');
    }
    $orders = App\Models\Order::ofUser(auth()->user())->orderBy('updated_at', 'desc')->get();
    $agent = new Agent();
    if ($agent->isDesktop()) {
        if($agent->isAndroidOS()) {
            return view('responsive.user.my_order', compact('orders'));
        }
        return view('order.index', compact('orders'));
    } else {
        return view('responsive.user.my_order', compact('orders'));
    }
    
});

/*
|--------------------------------------------------------------------------
| Static Routes
|--------------------------------------------------------------------------
|
|
*/
Route::get('become-member', function() {
    return view('responsive.become_member');
    $agent = new Agent();
    if ($agent->isDesktop()) {
        if($agent->isAndroidOS()) {
            return view('responsive.become_member');
        }
        return view('become_a_user');
    } else {
        return view('responsive.become_member');
    }
    
});

Route::get('about-us', function(){
    $agent = new Agent();
    if ($agent->isDesktop()) {
        if($agent->isAndroidOS()) {
            return view('responsive.about');
        }
        return view('about_us');  
    } else {
        return view('responsive.about');
    }
    
});

Route::get('best-price-guarantee', function(){
    $agent = new Agent();
    if ($agent->isDesktop()) {
        if($agent->isAndroidOS()) {
            return view('responsive.best_price');
        }
        return view('best_price');
    } else {
        return view('responsive.best_price');
    }
});

Route::post('best-price-guarantee', function(){
    $agent = new Agent();
    if ($agent->isDesktop()) {
        if($agent->isAndroidOS()) {
            return view('responsive.best_price');
        }
        return view('best_price');
    } else {
        return view('responsive.best_price');
    }
});

Route::get('return-and-exchange', function(){
    $agent = new Agent();
    if ($agent->isDesktop()) {
        if($agent->isAndroidOS()) {
            return view('responsive.return_exchange');
        }
        return view('return_and_exchange');
    } else {
        return view('responsive.return_exchange');
    }
});

Route::get('delivery', function(){
    $agent = new Agent();
    if ($agent->isDesktop()) {
        if($agent->isAndroidOS()) {
            return view('responsive.delivery');
        }
        return view('delivery');
    } else {
        return view('responsive.delivery');
    }
});

Route::get('payment', function(){
    $agent = new Agent();
    if ($agent->isDesktop()) {
        if($agent->isAndroidOS()) {
            return view('responsive.payment');
        }
        return view('payment');
    } else {
        return view('responsive.payment');
    }
});

Route::get('faq', function(){
    $agent = new Agent();
    if ($agent->isDesktop()) {
        if($agent->isAndroidOS()) {
            return view('responsive.faq');
        }
        return view('faq');
    } else {
        return view('responsive.faq');
    }
});

Route::get('contact-us', function(){
    $agent = new Agent();
    if ($agent->isDesktop()) {
        if($agent->isAndroidOS()) {
            return view('responsive.contact_us');
        }
        return view('contact_us');
    } else {
        return view('responsive.contact_us');
    }
});
Route::post('contact-us', function(Illuminate\Http\Request $request){

    $data = array(
        'name' => $request->name,
        'email' => $request->email,
        'msg' => $request->message,
        'category' => $request->category
    );

    try{
        Mail::send('emails.contact_us', $data, function ($message) use ($request) {

            $message->from("hello@seafermart.co.id", "Seafermart Inquiry");
            //$message->to('cs@seafermart.co.id')->subject($request->category.' Inquiry');
            $message->to('cs@seafermart.co.id')->subject(ucfirst($request->category));

        });
    } catch (\Exception $exception) {
        $request->session()->flash('alert-danger', 'Failed to send your inquiry. Please drop us an email of your enquiry on cs@seafermart.co.id!');
    }


    // add additional sending for debug purpose
    try{
        Mail::send('emails.contact_us', $data, function ($message) use ($request) {

            $message->from("hello@seafermart.co.id", "Seafermart Inquiry");
            //$message->to('cs@seafermart.co.id')->subject($request->category.' Inquiry');
            $message->to('airlanggatirta@gmail.com')->subject($request->category.' Inquiry');

        });
    } catch (\Exception $exception) {
        $request->session()->flash('alert-danger', 'Failed to send your inquiry. Please drop us an email of your enquiry on cs@seafermart.co.id!');
    }

    $request->session()->flash('alert-success', 'Your inquiry has been successfully sent!');

    return back();

});

Route::get('sendemail', function () {

    $data = array(
        'name' => "Testing Seafer Sendout",
    );

    try{

        Mail::send('test_email', $data, function ($message) {

            $message->from('hello@seafermart.co.id', 'Greetings from Mars');

            $message->to('wardlattelover@gmail.com')->subject('Tes ngirim2 test email pake smtp Google');

        });

        return "nice!";

    } catch (\Exception $exception) {
        dd($exception->getMessage());
    }

    return "Your email has been sent successfully";

});

Route::get('testpdf', function() {
    $snappy = new Pdf('/usr/local/sbin/wkhtmltopdf');
    /*header('Content-Type: application/pdf');
    header('Content-Disposition: attachment; filename="invoice.pdf"');*/
    try {
        $pdf = $snappy->getOutput(url('resp/about'));
        dd($pdf);
        #return $pdf;
        return new Response(
            $pdf,
            200,
            array(
                'Content-Type'          => 'application/pdf',
                'Content-Disposition'   => 'attachment; filename="invoice.pdf"'
            )
        );
    } catch (\Exception $e) {
        echo $e->getMessage();  
    }
});

Route::get('loyalty-points', function(){
    $agent = new Agent();
    if ($agent->isDesktop()) {
        if($agent->isAndroidOS()) {
            return view('responsive.cashback_rewards');
        }
        return view('loyalty_points');
    } else {
        return view('responsive.cashback_rewards');
    }
});

Route::resource('claim', 'ClaimController');
Route::resource('newsletter', 'NewsletterController');
Route::resource('suggestion', 'ProductSuggestionController');
Route::resource('order-claim', 'OrderClaimController');

Route::controller('cart', 'CartController');
Route::get('cart/cart-controller/{id}', 'CartController@getCartController/$id');

//Social Login
Route::get('/login/{provider?}',[
    'uses' => 'Auth\AuthController@getSocialAuth',
    'as'   => 'auth.getSocialAuth'
]);


Route::get('/login/callback/{provider?}',[
    'uses' => 'Auth\AuthController@getSocialAuthCallback',
    'as'   => 'auth.getSocialAuthCallback'
]);

Route::get('admin/login', function(){
    return view('admin.users.login');
});

Route::get('admin/static_info', function(){
    return view('admin.users.login');
});

Route::get('admin/static_info/update', function(){
    return view('admin.users.login');
});

Route::group(['prefix'=>'admin', 'namespace'=> 'Admin', 'middleware' => 'admin'], function() {
    Route::get('/', 'AdminController@index');

    Route::get('logout', function() {
        auth()->logout();
        return redirect('admin/login');
    });

    Route::get('user/profile', 'UserController@showProfile')->name('profile');
    Route::post('groceries/storeImage/{id}','ProductController@store_image');
    Route::get('groceries/deleteimage/{id}/{im_id}','ProductController@delete_image');
    Route::post('groceries/store-poster/{id}','ProductController@store_poster');
    Route::get('groceries/delete-poster/{id}/{ps_id}','ProductController@delete_poster');
    Route::get('groceries/import-csv', 'ProductController@csv_import');
    Route::post('groceries/import-csv', 'ProductController@csv_import');
    Route::get('groceries/minimum', 'ProductController@minimum');
    Route::get('groceries/init', 'ProductController@initialTransferToSearchable');
    Route::get('groceries/existing-groceries-item/{id}/{result}', 'ProductController@getExistingCategories');

    
    Route::get('transactions/{id}/approve-transaction', 'PaymentController@approveTransaction');
    Route::get('transactions/{id}/threedays-transaction', 'PaymentController@threeDaysApproval');
    Route::get('transactions/step-one/{id}', 'PaymentController@extraStepOne');
    Route::get('transactions/step-two/{id}', 'PaymentController@extraStepTwo');
    Route::get('transactions/add-manual', 'PaymentController@addTransactionManually');
    Route::post('transactions/process-step-one', 'PaymentController@processStepOne');
    Route::post('transactions/reject-transaction', 'PaymentController@rejectTransaction');
    Route::post('transactions/handle-email-transaction', 'PaymentController@handleWithEmail');


    Route::get('orders/order-gojek/{id}', 'OrderController@doGojek');
    Route::post('orders/insert-awb/{id}', 'OrderController@insertAwb');
    Route::get('orders/dispatch-courier/{id}', 'OrderController@dispatchInternalCourier');
    Route::get('orders/finish-courier/{id}', 'OrderController@finishCourier');
    Route::get('orders/get-gojek-status/{order_number}', 'OrderController@getGojekStatus');
    Route::get('orders/void-order/{id}', 'OrderController@voidOrder');
    Route::get('orders/shipping/{id}', 'OrderController@indexShipping');


    Route::get('test/point', 'PaymentController@getDummyCashback');

    Route::resource('roles', 'RoleController');
    Route::resource('users','UserController');
    Route::resource('groceries','ProductController');
    Route::resource('parent-category','ParentCategoryController');
    Route::resource('child-category','ChildCategoryController');
    Route::resource('cooking-advice','CookingAdviceController');
    Route::resource('brands','BrandController');
    Route::resource('orders','OrderController');
    Route::resource('transactions','PaymentController');
    Route::resource('promos','PromoController');
    Route::resource('discounts','DiscountController');
    Route::resource('complaints','ComplainController');
    Route::resource('claims','ClaimController');
    Route::resource('comments','CommentController');
    Route::resource('metrics', 'MetricController');
    Route::resource('tags', 'TagController');
    Route::resource('headlines', 'HomeSlidesController');
    Route::resource('newsletters', 'NewsletterController');
    Route::resource('charity-voucher', 'DonationVoucherController');
    Route::resource('order-claims', 'OrderClaimController');

});

Route::group(['prefix'=>'api', 'namespace'=> 'Api'], function() {
    Route::get('/', 'ApiController@index');
    Route::controller('categories', 'CategoryController');
    Route::controller('homeslide', 'HomeslideController');
    Route::controller('user', 'UserController');
    Route::controller('cart', 'CartController');
    Route::controller('product', 'ProductController');
    Route::controller('message', 'MessageController');
    Route::controller('wishlist', 'WishlistController');
    Route::controller('review', 'ReviewController');
    Route::controller('order', 'OrderController');
    Route::controller('notification', 'NotificationController');
    Route::controller('voucher', 'VoucherController');
});

Route::controller('checkout', 'CheckoutController');
Route::controller('user', 'UserController');
Route::controller('/', 'HomeController');
