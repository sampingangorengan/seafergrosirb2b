<?php

namespace App\Http\Controllers;

use App\Models\ProductSuggestion;
use Illuminate\Http\Request;
use App\Models\SuggestionPicture;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Agent;

class ProductSuggestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Agent::isDesktop()) {
            if(Agent::isAndroidOS()) {
                return view('responsive.suggestion');
            }
            return view('suggest_product');
        } else {
            return view('responsive.suggestion');
        }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return redirect('suggestion');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'product_name'        => 'required',
            'brand_name'         => 'required',
            'email'             => 'required|email',
            'category'         => 'required',
            'file'          => 'mimes:jpeg,bmp,png',
        ]);

        $suggest = new ProductSuggestion();
        $suggest->product_name = $request->product_name;
        $suggest->product_brand = $request->brand_name;
        $suggest->email = $request->email;
        $suggest->category_id = $request->category;
        $suggest->comments = $request->comments;

        if($file = $request->file('file')) {
            $name = time().$file->getClientOriginalName();

            $file->move('suggestion_picture', $name);

            $photo = SuggestionPicture::create(['file' => $name]);

            $suggest->suggestion_picture = (int) $photo->id;
        }

        $suggest->save();

        $request->session()->flash('alert-success', 'Product suggestion was successfully submitted!');

        return redirect('suggestion');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
