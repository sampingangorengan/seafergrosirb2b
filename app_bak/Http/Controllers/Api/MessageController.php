<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use App\Models\Message;
use App\Models\User;
use Validator;

class MessageController extends Controller
{

    private function make_response($status = true, $message = '', $data = null) {
        if(null == $data) {
            $response = array(
                'status' => $status,
                'msg' => $message
            );
        } else {
            $response = array(
                'data' => $data,
                'status' => $status,
                'msg' => $message
            );
        }

        return (new Response($response, 200))
        ->header('Content-Type', 'application/json');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function postList(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userid'          => 'required',
        ]);

        if ($validator->fails()) {
            return $this->make_response(false, 'Please complete the required parameters', $validator->errors());
        }

        if(null != $request->userid) {
            $user = User::find($request->userid);
        } else {
            return $this->make_response(false, 'User not found');
        }

        if(null != $request->type && $request->type == 'sent') {
            $messages = Message::where('from_id', $user->id)->where('deleted_by_sender', 0);
        } else {
            $messages = Message::where('to_id', $user->id)->where('deleted_by_recipient', 0);    
        }
        
        $order = 'asc';
        if(null !== $request->order_by) {
            if(null !== $request->order) {
                $order = $request->order;
            }

            if(! in_array($request->order, ['asc', 'desc', 'ascending', 'descending'])) {
                return $this->make_response(false, 'Order parameter can only be "asc" or "desc" or "ascending" or "descending"');
            }

            $messages = $messages->orderBy($request->order_by, $request->order)->get();
        } else {
            $messages = $messages->orderBy('created_at', 'desc')->get();
        }

        if($messages->count() > 0){
            foreach($messages as $message) {
                $sender_detail = User::find($message->from_id);
        
                $message->sender_detail = array(
                    'name' => $sender_detail->name,
                    'image' => $sender_detail->photo->file,
                    'role' => $sender_detail->roles->name
                );

                $recipient_detail = User::find($message->to_id);
                
                $message->recipient_detail = array(
                    'name' => $recipient_detail->name,
                    'image' => $recipient_detail->photo->file,
                    'role' => $sender_detail->roles->name
                );
            }

            return $this->make_response(true, 'Messages are available', $messages);
        } else {
            return $this->make_response(false, 'Messages empty');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function postListTrash(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userid'          => 'required',
        ]);

        if ($validator->fails()) {
            return $this->make_response(false, 'Please complete the required parameters', $validator->errors());
        }

        if(null != $request->userid) {
            $user = User::find($request->userid);
        } else {
            return $this->make_response(false, 'User not found');
        }

        $messages = Message::where('from_id', $user->id)->where('deleted_by_sender', 1)->get();

        $messages_two = Message::where('to_id', $user->id)->where('deleted_by_recipient', 1)->get();

        $merged_messages = $messages->merge($messages_two);

        if($merged_messages->count() > 0){

            foreach($merged_messages as $message) {
                $sender_detail = User::find($message->from_id);
        
                $message->sender_detail = array(
                    'name' => $sender_detail->name,
                    'image' => $sender_detail->photo->file,
                    'role' => $sender_detail->roles->name
                );

                $recipient_detail = User::find($message->to_id);
                
                $message->recipient_detail = array(
                    'name' => $recipient_detail->name,
                    'image' => $recipient_detail->photo->file,
                    'role' => $sender_detail->roles->name
                );
            }

            return $this->make_response(true, 'Messages are available', $merged_messages);
        } else {
            return $this->make_response(false, 'Messages empty');
        }
    }

    /**
     * Write a new message.
     *
     * @return \Illuminate\Http\Response
     */
    public function postCreate(Request $request) {

        $validator = Validator::make($request->all(), [
            'userid'          => 'required',
            'content'      => 'required',
            'to_id'         => 'required',
        ]);

        if ($validator->fails()) {
            return $this->make_response(false, 'Please complete the required parameters', $validator->errors());
        }

        if(null != $request->userid) {
            $user = User::find($request->userid);
        } else {
            return $this->make_response(false, 'User not found');
        }

        if(null != $request->to_id) {
            $dest_user = User::find($request->to_id)->count();
            if($dest_user < 1) {
                return $this->make_response(false, 'Destination user not found');
            }
        } 

        $message = new Message;
        $message->from_id = $user->id;
        $message->to_id = $request->to_id;
        $message->subject = $request->subject;
        $message->content = $request->content;
        $message->is_read = 0;
        $message->save();

        $message = Message::find($message->id)->toArray();

        return $this->make_response(true, 'Success creating message', $message);
    }

    /**
     * Write a new message.
     *
     * @return \Illuminate\Http\Response
     */
    public function postRead(Request $request, $id) {
        $validator = Validator::make($request->all(), [
            'userid'          => 'required',
        ]);

        if ($validator->fails()) {
            return $this->make_response(false, 'Please complete the required parameters', $validator->errors());
        }

        $user = User::find($request->userid);

        if(null == $user) {
            return $this->make_response(false, 'User not found');
        }

        $message = Message::find($id);

        if(null == $message) {
            return $this->make_response(false, 'Message not found');
        }

        if($user->id != $message->from_id && $user->id != $message->to_id) {
            return $this->make_response(false, 'You are not allowed to view this message');
        }

        /*if($user->id == $message->from_id && $message->deleted_by_sender == 1) {
            return $this->make_response(false, 'Message not found');
        }

        if($user->id == $message->to_id && $message->deleted_by_recipient == 1) {
            return $this->make_response(false, 'Message not found');
        }*/


        $sender_detail = User::find($message->from_id);
        
        $message->sender_detail = array(
            'name' => $sender_detail->name,
            'image' => $sender_detail->photo->file,
            'role' => $sender_detail->roles->name
        );

        $recipient_detail = User::find($message->to_id);
        
        $message->recipient_detail = array(
            'name' => $recipient_detail->name,
            'image' => $recipient_detail->photo->file,
            'role' => $sender_detail->roles->name
        );
        

        return $this->make_response(true, 'Message is available', $message);
    }

    /**
     * Write a new message.
     *
     * @return \Illuminate\Http\Response
     */
    public function postDelete(Request $request, $id) {

        $validator = Validator::make($request->all(), [
            'userid'          => 'required',
        ]);

        if ($validator->fails()) {
            return $this->make_response(false, 'Please complete the required parameters', $validator->errors());
        }

        $user = User::find($request->userid);

        if(null == $user) {
            return $this->make_response(false, 'User not found');
        }

        $message = Message::find($id);

        if(null == $message) {
            return $this->make_response(false, 'Message not found');
        }

        if($user->id != $message->from_id && $user->id != $message->to_id) {
            return $this->make_response(false, 'You are not allowed to tamper this message');
        }
        
        if($user->id == $message->from_id) {
            $message->deleted_by_sender = 1;
        } elseif($user->id == $message->to_id) {
            $message->deleted_by_recipient = 1;
        }

        $message->save();

        /*if($message->deleted_by_sender == 1 && $message->deleted_by_recipient == 1) {
            $stat = $message->delete();
        }*/

        return $this->make_response(true, 'Message successfully deleted');
        
    }
}
