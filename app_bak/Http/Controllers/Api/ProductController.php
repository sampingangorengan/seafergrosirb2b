<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Groceries;
use App\Models\Groceries\Brand;
use App\Models\Groceries\Category;
use App\Models\Groceries\ChildCategory;
use App\Models\Groceries\Metric;
use App\Models\Groceries\Rating;
use App\Models\ProductSuggestion;
use App\Models\User;
use App\Models\User\ViewHistory;
use App\Models\SuggestionPicture;
use Illuminate\Http\Response;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Input;
use DB;
use Validator;

class ProductController extends Controller
{
    private function make_response($status = true, $message = '', $data = null) {
        if(null == $data) {
            $response = array(
                'status' => $status,
                'msg' => $message
            );
        } else {
            $response = array(
                'data' => $data,
                'status' => $status,
                'msg' => $message
            );
        }

        return (new Response($response, 200))
        ->header('Content-Type', 'application/json');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function postList(Request $request){
        $category_id = $request->category_id;
        $level = $request->level;
        $sort_by = $request->sort_by;
        $products = collect();
        
        $orderBy = 'updated_at';
        $orderType = 'desc';
        if(isset($sort_by)){
            switch ($sort_by) {
                case 'highest-price':
                    $orderBy = 'price';
                    $orderType = 'desc';
                    break;
                case 'lowest-price':
                    $orderBy = 'price';
                    $orderType = 'asc';
                    break;
                case 'best-seller':
                    $orderBy = 'sold';
                    $orderType = 'desc';
                    break;
                case 'latest-entry':
                    $orderBy = 'updated_at';
                    $orderType = 'desc';
                    break;
                default:
                    $orderBy = 'updated_at';
                    $orderType = 'desc';
                    break;
            }
        }
        if($level == 'parent'){
            $category = DB::table('child_categories')
                        ->where('parent_id','=',$category_id)
                        ->get();
            foreach($category as $ct){
                $product = DB::table('groceries_category')
                            ->select('grc.*','metrics.name as metric_name','promos.promo_type','promos.discount_value')
                            ->leftJoin('groceries as grc','grc.id','=','groceries_category.groceries_id')
                            ->leftJoin('metrics','metrics.id','=','grc.metric_id')
                            ->leftJoin('promos','promos.id','=','grc.discount_id')
                            ->leftJoin('brands','brands.id','=','grc.brand_id')
                            ->where('groceries_category.category_id','=',$ct->id)
                            ->where('grc.id','!=','null')
                            ->orderBy($orderBy, $orderType)
                            ->get();
                foreach($product as $p){
                    if($p->discount_value != null && $p->discount_value > 0){
                        if($p->promo_type == 'percent'){
                            $p->price_after_discount = $p->price - ($p->price * $p->discount_value / 100);
                        } else if($p->promo_type == 'nominal'){
                            $p->price_after_discount = $p->price - $p->discount_value;
                        }
                    }
                    $images = DB::table('groceries_images')
                                ->where('groceries_id','=',$p->id)
                                ->get();
                    if(count($images) > 0){
                        foreach($images as $img){
                            $img->image_url = url('/contents/'.$img->file_name);
                        }
                        $p->images = $images;

                    } else {
                        $p->images = 'No Images Available';
                    }
                }
                if(count($product) > 0){
                    $products = $products->merge($product);
                }
            }
        } else if($level == 'child') {
            $products = DB::table('groceries_category')
            ->select('grc.*','metrics.name as metric_name','promos.promo_type','promos.discount_value')
            ->leftJoin('groceries as grc','grc.id','=','groceries_category.groceries_id')
            ->leftJoin('metrics','metrics.id','=','grc.metric_id')
            ->leftJoin('promos','promos.id','=','grc.discount_id')
            ->where('groceries_category.category_id','=',$category_id)
            ->orderBy($orderBy, $orderType)
            ->get();
            foreach($products as $p){
                if($p->discount_value != null && $p->discount_value > 0){
                    if($p->promo_type == 'percent'){
                        $p->price_after_discount = $p->price - ($p->price * $p->discount_value / 100);
                    } else if($p->promo_type == 'nominal'){
                        $p->price_after_discount = $p->price - $p->discount_value;
                    }
                }
                $images = DB::table('groceries_images')
                            ->where('groceries_id','=',$p->id)
                            ->get();
                if(count($images) > 0){
                    foreach($images as $img){
                        $img->image_url = url('/contents/'.$img->file_name);
                    }
                    $p->images = $images;

                } else {
                    $p->images = 'No Images Available';
                }
            }
        }
        if(count($products) > 0){
            $response = array(
                'data' => $products,
                'sort_by' => $sort_by,
                'status' => true,
                'msg' => 'Products available'
            );
        } else {
            $response = array(
                'status' => false,
                'sort_by' => $sort_by,
                'msg' => 'Products not available'
            );
        }
        return (new Response($response, 200))
        ->header('Content-Type', 'application/json');
    }
    public function postDetails(Request $request){
        $product_id = $request->product_id;
        if($product_id){
            $product = DB::table('groceries')
            ->select('groceries.*','metrics.name as metric_name','brands.name as brand_name','child_categories.name as child_category_name','groceries_categories.name as main_category_name')
            ->leftJoin('metrics','metrics.id','=','groceries.metric_id')
            ->leftJoin('brands','brands.id','=','groceries.brand_id')
            ->leftJoin('groceries_category','groceries_category.groceries_id','=','groceries.id')
            ->leftJoin('child_categories','child_categories.id','=','groceries_category.category_id')
            ->leftJoin('groceries_categories','groceries_categories.id','=','child_categories.parent_id')
            ->where('groceries.id','=',$product_id)
            ->first();

            if(null == $product) {
                return $this->make_response(false, 'Grocery not found');
            }
            if($product){
                $images = DB::table('groceries_images')
                ->where('groceries_id','=',$product->id)
                ->get();
                if(count($images) > 0){
                    foreach($images as $img){
                        $img->image_url = url('/contents/'.$img->file_name);
                    }
                    $product->images = $images;

                } else {
                    $product->images = 'No Images Available';
                }
                
                if(null != $product->brand_id){
                    $product->brand_name = Brand::find($product->brand_id)->name;    
                }
                
                if(null != $product->metric_id) {
                    $product->metric_name = Metric::find($product->metric_id)->abbreviation;    
                }

                $product->product_url = 'http://seafermart.co.id/groceries/'.$product->id.'/'.$product->slug;

                $response = array(
                    'data' => $product,
                    'status' => true,
                    'msg' => 'Products available'
                );
            } else {
                $response = array(
                    'status' => false,
                    'msg' => 'Products not available'
                );
            }
        } else {
            $response = array(
                'status' => false,
                'msg' => 'Products not available'
            );
        }
        return (new Response($response, 200))
        ->header('Content-Type', 'application/json');
    }
    public function postSearch(Request $request){
        $keyword = $request->keyword;
        $sort_by = $request->sort_by;

        $orderBy = 'updated_at';
        $orderType = 'desc';
        if(isset($sort_by)){
            switch ($sort_by) {
                case 'highest-price':
                    $orderBy = 'price';
                    $orderType = 'desc';
                    break;
                case 'lowest-price':
                    $orderBy = 'price';
                    $orderType = 'asc';
                    break;
                case 'latest-entry':
                    $orderBy = 'updated_at';
                    $orderType = 'desc';
                    break;
                default:
                    $orderBy = 'updated_at';
                    $orderType = 'desc';
                    break;
            }
        }
        if($keyword && strlen($keyword) > 2 && isset($keyword) && $keyword != null && $keyword != ''){
            $products = DB::table('groceries as grc')
            ->select('grc.*','metrics.name as metric_name','promos.promo_type','promos.discount_value')
            ->leftJoin('metrics','metrics.id','=','grc.metric_id')
            ->leftJoin('promos','promos.id','=','grc.discount_id')
            ->where('grc.name','LIKE','%'.$keyword.'%')
            ->orderBy($orderBy, $orderType)
            ->get();
            foreach($products as $p){
                if($p->discount_value != null && $p->discount_value > 0){
                    if($p->promo_type == 'percent'){
                        $p->price_after_discount = $p->price - ($p->price * $p->discount_value / 100);
                    } else if($p->promo_type == 'nominal'){
                        $p->price_after_discount = $p->price - $p->discount_value;
                    }
                }
                $images = DB::table('groceries_images')
                            ->where('groceries_id','=',$p->id)
                            ->get();
                if(count($images) > 0){
                    foreach($images as $img){
                        $img->image_url = url('/contents/'.$img->file_name);
                    }
                    $p->images = $images;

                } else {
                    $p->images = 'No Images Available';
                }
            }
            if(count($products) > 0){
                $response = array(
                    'data' => $products,
                    'sort_by' => $sort_by,
                    'status' => true,
                    'msg' => 'Products available'
                );
            } else {
                $response = array(
                    'status' => false,
                    'sort_by' => $sort_by,
                    'msg' => 'Products not available'
                );
            }
        } else {
            $response = array(
                'status' => false,
                'sort_by' => $sort_by,
                'msg' => 'Please input keyword with minimal 3 character'
            );
        }
        return (new Response($response, 200))
        ->header('Content-Type', 'application/json');
    }
    public function postRelatedBrand(Request $request) {
        $validator = Validator::make($request->all(), [
            'groceries_id'          => 'required',
        ]);

        if ($validator->fails()) {
            return $this->make_response(false, 'Please complete the required parameters', $validator->errors());
        }

        if(null != $request->groceries_id) {
            $groceries = Groceries::find($request->groceries_id);
        } else {
            return $this->make_response(false, 'Grocery not found');
        }
        
        $brands = Groceries::where('brand_id', $groceries->brand->id)->orderBy('updated_at', 'desc')->skip(0)->take(6)->get();

        foreach($brands as $brand) {
            $brand->image = Groceries::getFirstImageUrl($brand->id);

            if(null !== $brand->brand_id) {
                $brand->brand_name = Brand::find($brand->brand_id)->name;    
            }

            if(null !== $brand->brand_id) {
                $brand->metric_name = Metric::find($brand->metric_id)->abbreviation;
            }
        }

        if($brand->count() > 0){
            return $this->make_response(true, 'Brand are available', $brands);
        } else {
            return $this->make_response(true, 'Brand empty');
        }
    }
    public function postMayLike(Request $request) {
        $validator = Validator::make($request->all(), [
            'groceries_id'          => 'required',
        ]);

        if ($validator->fails()) {
            return $this->make_response(false, 'Please complete the required parameters', $validator->errors());
        }

        if(null != $request->groceries_id) {
            $groceries = Groceries::find($request->groceries_id);
        } else {
            return $this->make_response(false, 'Grocery not found');
        }
        
        $may_like = Groceries::orderByRaw('RAND()')->skip(0)->take(6)->get();

        foreach($may_like as $item) {
            $item->image = Groceries::getFirstImageUrl($item->id);

            if(null !== $item->brand_id) {
                $item->brand_name = Brand::find($item->brand_id)->name;    
            }

            if(null !== $item->brand_id) {
                $item->metric_name = Metric::find($item->metric_id)->abbreviation;
            }
        }

        if($may_like->count() > 0){
            return $this->make_response(true, 'May like products are available', $may_like);
        } else {
            return $this->make_response(true, 'May like products empty');
        }
    }

    public function postRecentlyViewed(Request $request) {
        $validator = Validator::make($request->all(), [
            'userid'          => 'required',
        ]);

        if ($validator->fails()) {
            return $this->make_response(false, 'Please complete the required parameters', $validator->errors());
        }

        $user = User::find($request->userid);

        if(null == $user) {
            return $this->make_response(false, 'User not found');
        }
        
        $recent = ViewHistory::where('identifier', $user->id)->orderBy('created_at', 'desc')->skip(0)->take(6)->get();

        if(null == $recent) {
            return $this->make_response(true, 'Recently viewed like products are empty');
        }

        if($recent->count() > 0){
            foreach($recent as $item) {
                $item->groceries_detail = Groceries::find($item->groceries_id);
                $item->groceries_detail->image = Groceries::getFirstImageUrl($item->groceries_id);
            }
            return $this->make_response(true, 'Recently viewed products are available', $recent);
        } else {
            return $this->make_response(true, 'Recently viewed like products are empty');
        }
    }
    public function postBrandFromCategory(Request $request) {
        $category_id = $request->category_id;
        $level = $request->level;
        if($category_id && $level){
            if($level == 'parent'){
                $data = DB::table('groceries_categories')
                            ->select('brands.name as brand_name','brands.slug as brand_slug','brands.id as brand_id')
                            ->leftJoin('child_categories','child_categories.parent_id','=','groceries_categories.id')
                            ->leftJoin('groceries_category','groceries_category.category_id','=','child_categories.id')
                            ->leftJoin('groceries','groceries.id','=','groceries_category.groceries_id')
                            ->leftJoin('brands','brands.id','=','groceries.brand_id')
                            ->where('groceries_categories.id','=',$category_id)
                            ->whereNotNull('brands.id')
                            ->whereNotNull('brands.name')
                            ->groupBy('brands.id')
                            ->get();
            } else {
                $data = DB::table('groceries_category')
                            ->select('brands.name as brand_name','brands.slug as brand_slug','brands.id as brand_id')
                            ->leftJoin('groceries','groceries.id','=','groceries_category.groceries_id')
                            ->leftJoin('brands','brands.id','=','groceries.brand_id')
                            ->where('groceries_category.category_id','=',$category_id)
                            ->whereNotNull('brands.id')
                            ->whereNotNull('brands.name')
                            ->groupBy('brands.id')
                            ->get();
            }
            if($data){
                return $this->make_response(true,'Brands data found',$data);
            } else {
                return $this->make_response(false,'Brands data found',null);
            }
        } else {
            return $this->make_response(false,'Please complete parameters',null);
        }
    }
    public function postSuggestProduct(Request $request) {
        //
        $validator = Validator::make($request->all(), [
            'product_name'        => 'required',
            'brand_name'         => 'required',
            'email'             => 'required|email',
            'category'         => 'required',
            'suggestion_picture'   => 'required|mimes:jpeg,bmp,png',
            'comments'  => 'required',
            'file'          => 'mimes:jpeg,bmp,png',
        ]);

        if ($validator->fails()) {
            return $this->make_response(false, 'Please complete the required parameters', $validator->errors());
        }

        $suggest = new ProductSuggestion();
        $suggest->product_name = $request->product_name;
        $suggest->product_brand = $request->brand_name;
        $suggest->email = $request->email;
        $suggest->category_id = $request->category;
        $suggest->comments = $request->comments;

        if($file = $request->file('suggestion_picture')) {
            $name = time().$file->getClientOriginalName();

            $file->move('suggestion_picture', $name);

            $photo = SuggestionPicture::create(['file' => $name]);

            $suggest->suggestion_picture = (int) $photo->id;
        }

        $status = $suggest->save();

        if($status) {
            return $this->make_response(true, 'Successfully add suggestion', $suggest);    
        } else {
            return $this->make_response(false, 'Failed to add suggestion');
        }
    }
    public function postFilteredProduct(Request $request) {
        $validator = Validator::make($request->all(), [
            'filter_type'          => 'required',
            'filter_value'         => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return $this->make_response(false, 'Please complete the required parameters');
        }

        if ($request->filter_type == 'brand') {
            $groceries = Groceries::where('brand_id', $request->filter_value)->orderBy('updated_at', 'desc')->get();
            if ($groceries->count() > 0) {
                foreach($groceries as $item) {

                    $item->image = Groceries::getFirstImageUrl($item->id);

                    if(null !== $item->brand_id) {
                        $item->brand_name = Brand::find($item->brand_id)->name;    
                    }

                    if(null !== $item->brand_id) {
                        $item->metric_name = Metric::find($item->metric_id)->abbreviation;
                    }

                    return $this->make_response(true, 'Groceries are available', $groceries);
                }
            } else {
                return $this->make_response(false, 'Grocery not available');
            }
        } elseif ($request->filter_type == 'rate') {
            $groceries = Rating::where('rate', $request->filter_value)->orderBy('updated_at', 'desc')->get();

            if ($groceries->count() > 0) {
                foreach($groceries as $item) {
                    $item->grocery_detail = Groceries::find($item->groceries_id);
                    if(null !== $item->grocery_detail){
                        $item->grocery_detail->image = Groceries::getFirstImageUrl($item->groceries_id);  
                        if(null != $item->grocery_detail->brand_id){
                            $item->grocery_detail->brand_name = Brand::find($item->grocery_detail->brand_id)->name;    
                        }
                        if(null != $item->grocery_detail->metric_id) {
                            $item->grocery_detail->metric_name = Metric::find($item->grocery_detail->metric_id)->abbreviation;    
                        }
                        $item->grocery_detail->product_url = 'http://seafermart.co.id/groceries/'.$item->grocery_detail->id.'/'.$item->grocery_detail->slug;  
                    }
                }
                return $this->make_response(true, 'Groceries are available', $groceries);
            } else {
                return $this->make_response(false, 'Grocery not available');
            }
        } elseif ($request->filter_type == 'category') {
            $category = ChildCategory::find($request->filter_value);

            $groceries = $category->groceries;

            if ($groceries->count() > 0) {
                foreach($groceries as $item) {
                    $item->grocery_detail = Groceries::find($item->groceries_id);
                    if(null !== $item->grocery_detail){
                        $item->grocery_detail->image = Groceries::getFirstImageUrl($item->groceries_id);    
                    }
                    $images = DB::table('groceries_images')
                                ->where('groceries_id','=',$item->id)
                                ->get();
                    if(count($images) > 0){
                        foreach($images as $img){
                            $img->image_url = url('/contents/'.$img->file_name);
                        }
                        $item->images = $images;
                    } else {
                        $item->images = 'No Images Available';
                    }
                    if(null != $item->brand_id){
                        $item->brand_name = Brand::find($item->brand_id)->name;    
                    }
                    if(null != $item->metric_id) {
                        $item->metric_name = Metric::find($item->metric_id)->abbreviation;    
                    }
                    $item->product_url = 'http://seafermart.co.id/groceries/'.$item->id.'/'.$item->slug;
                }
                return $this->make_response(true, 'Groceries are available', $groceries);
            } else {
                return $this->make_response(false, 'Grocery not available');
            }
        } else {
            return $this->make_response(false, 'Unknown filter type');
        }

        $groceries = Groceries::find($request->groceries_id);
        if(null == $groceries){
            return $this->make_response(false, 'Grocery not found');
        }
    }
}
