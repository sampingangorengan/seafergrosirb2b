<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Groceries;
use App\Models\User;
use App\Models\Cart;
use App\Models\Order;
use App\Models\Order\Groceries as OrderGroceries;
use App\Models\PaymentProof;
use App\Models\Promo;
use App\Models\PromoHistory;
use App\Models\Order\Payment;
use App\Models\User\CashbackReward;
use App\Models\User\CashbackPoint;
use App\Models\User\Address;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Mail;
use Validator;

class OrderController extends Controller
{
	private function make_response($status = true, $message = '', $data = null) {
        if(null == $data) {
            $response = array(
                'status' => $status,
                'msg' => $message
            );
        } else {
            $response = array(
                'data' => $data,
                'status' => $status,
                'msg' => $message
            );
        }

        return (new Response($response, 200))
        ->header('Content-Type', 'application/json');
    }

    private function updateAfterRedeem(Order $order,$value,$userid) {

        $cashback = new CashbackReward;
        $cashback->user_id = $userid;
        $cashback->order_id = $order->id;
        $cashback->obtain_from = 'redeem';
        $cashback->nominal = $value;
        $status = $cashback->save();

        if($status) {
            $points = CashbackPoint::where('user_id', $userid)->first();
            $points->nominal = $points->nominal - $value;
            $stat = $points->save();

            if($stat) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private function setGojekDetail($type) {

        $delivery_detail_id = DB::table('gojek_details')->insertGetId(
            [
                'status' => 'not made',
                'booking_type' => $type,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]
        );
        return $delivery_detail_id;
    }

    private function setJneDetail($type) {
        $delivery_detail_id = DB::table('jne_details')->insertGetId(
            [
                'status' => 'not made',
                'booking_type' => $type,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]
        );

        return $delivery_detail_id;
    }

    public function postConfirmPayment(Request $request) {
    	$validator = Validator::make($request->all(), [
            'userid'          					=> 'required',
            'transfer_amount'					=> 'required|numeric',
            'transfer_date'      				=> 'required',
            'transfer_time_minute'         		=> 'required|numeric',
            'transfer_time_second'         		=> 'required|numeric',
            'transfer_sender_bank_name'     	=> 'required',
            'transfer_sender_account_number'    => 'required',
            'transfer_sender_account_name'      => 'required',
            'file'          					=> 'mimes:jpeg,bmp,png',
            'order_number' 						=> 'required|numeric',
        ]);

        if ($validator->fails()) {
            return $this->make_response(false, 'Please complete the required parameters', $validator->errors());
        }

        if(null != $request->userid) {
            $user = User::find($request->userid);
        } else {
            return $this->make_response(false, 'User not found');
        }

        $order = Order::find($request->order_number);

        if(null == $order) {
        	return $this->make_response(false, 'Order not found');
        }

        $payment = new Payment;
        $payment->order_id = $order->id;
        $payment->method = 'transfer';
        $payment->transfer_dest_account = $request->transfer_dest_account;
        $payment->transfer_sender_bank_name = strtoupper($request->transfer_sender_bank_name);
        $payment->transfer_sender_account_number = $request->transfer_sender_account_number;
        $payment->transfer_sender_account_name = $request->transfer_sender_account_name;
        $payment->transfer_date = $request->transfer_date;
        $payment->transfer_time = $request->transfer_time_minute.':'.$request->transfer_time_second;
        $payment->transfer_note = $request->transfer_note;
        $payment->transfer_amount = $request->transfer_amount;
        $payment->notify_admin = 0;
        $payment->user_confirmed = 1;
        $payment->transfer_dest_account = 'BCA: 2063232327';

        if($file = $request->file('file')) {

            $name = time() . $order->id . '.' . $file->getClientOriginalExtension();

            $file->move('payment_proof', $name);

            $photo = PaymentProof::create(['file' => $name]);

            $payment->transfer_receipt_file = (int) $photo->id;
        }

        $status = $payment->save();

        if($status) {
            $order->order_status = 6;
            $order->save();
            
        	return $this->make_response(true, 'Successfully send payment confirmation', Payment::find($payment->id));
        } else {
        	return $this->make_response(false, 'Failed sending payment confirmation');
        }

    }

    public function postCheckout(Request $request) {

        $validator = Validator::make($request->all(), [
            'userid'                => 'required',
            'address_id'            => 'required',
            'shipping_service_code' => 'required',
            'shipping_fee'          => 'required',
            'use_promo'             => 'required',
            'promo_id'              => 'required',
            'promo_value'           => 'required',
            'redeem_cashback'       => 'required',
            'total_before_tax'      => 'required',
            'grand_total'           => 'required',
            'tax'                   => 'required',
            'unique_code'           => 'required',
        ]);

        if ($validator->fails()) {
            return $this->make_response(false, 'Please complete the required parameters', $validator->errors());
        }

        $user = User::find($request->userid);

        if(null == $user) {
            return $this->make_response(false, 'User not found');
        }

        $user_cart = Cart::where('user_id', $user->id)->get();

        if(null == $user_cart) {
            $this->make_response(false, 'Cart not found');
        }

        // Insert to "orders" table
        $ord_id = DB::table('orders')->insertGetId(
            [
                'user_address_id' => $request->address_id,
                'user_id' => $user->id,
                'shipping_service_code' => $request->shipping_service_code,
                'shipping_fee' => $request->shipping_fee,
                'order_status' => 1,
                'is_using_promo' => $request->promo_id,
                'promo_value' => $request->promo_value,
                'redeem' => $request->redeem_cashback,
                'nett_total' => $request->total_before_tax,
                'tax' => $request->tax,
                'grand_total' => $request->grand_total,
                'unique_code' => $request->unique_code,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]
        );

        $default_payment = new Payment;
        $default_payment->order_id = $ord_id;
        $default_payment->method = 'transfer';
        $default_payment->transfer_dest_account = 'Not Available';
        $default_payment->transfer_sender_account_number = 'Not Available';
        $default_payment->transfer_sender_account_name = 'Not Available';
        $default_payment->transfer_date = date('Y-m-d');
        $default_payment->transfer_time = '00:00:01';
        $default_payment->transfer_note = 'Not Available';
        $default_payment->transfer_receipt_file = '';
        $default_payment->is_approved = 0;
        $default_payment->transfer_amount = 0;
        $default_payment->transfer_sender_bank_name = 'Not Available';
        $default_payment->payment_status_id = 0;
        $default_payment->notify_admin = 0;
        $default_payment->user_confirmed = 0;
        $default_payment->save();


        if($request->promo_id != 0) {
            $promo = Promo::find($use_promo);

            $promo_history = new PromoHistory;
            $promo_history->user_id = $user->id;
            $promo_history->order_id = $ord_id;
            $promo_history->promo_id = $promo->id;
            $promo_history->promo_code = $promo->code;
            $promo_history->save();
        }

        // Insert to "order_groceries" table
        
        foreach ($user_cart as $cartGroceries) {

            if(null == Groceries::find($cartGroceries->groceries_id)) {
                $product = Groceries::where('price', $cartGroceries->price)->where('sku', $cartGroceries->sku)->where('name', $cartGroceries->name)->first();
                $orderGroceries = new OrderGroceries;
                $orderGroceries->order_id = $ord_id;
                $orderGroceries->groceries_id = $product->id;
                $orderGroceries->price = $cartGroceries->price;
                $orderGroceries->qty = $cartGroceries->quantity;
                $orderGroceries->save();
            } else {
                $orderGroceries = new OrderGroceries;
                $orderGroceries->order_id = $ord_id;
                $orderGroceries->groceries_id = $cartGroceries->groceries_id;
                $orderGroceries->price = $cartGroceries->price;
                $orderGroceries->qty = $cartGroceries->quantity;
                $orderGroceries->save();
            }

            // update stock and quantity
            $update_stock_sold = Groceries::find($cartGroceries->groceries_id);
            $update_stock_sold->stock = $update_stock_sold->stock - $cartGroceries->quantity;
            $update_stock_sold->sold = $update_stock_sold->sold + $cartGroceries->quantity;
            $update_stock_sold->save();

        }

        foreach($user_cart as $item) {
            $item->delete();
        }

        $order = Order::find($ord_id);

        $redeem = $this->updateAfterRedeem($order, $request->redeem_cashback, $user->id);

        $detail_id = null;
        $delivery_type = null;
        if (request('shipping_service_code') == 'gosend_sameday') {
            $detail_id = $this->setGojekDetail('Same Day');
            $delivery_type = 'gojek';
            $delivery = $order->deliveries()->create([
                'delivery_type' => $delivery_type,
                'detail_id' => $detail_id,
                'status' => 'not made',
                'order_id' => $order->id
            ]);
        } elseif (request('shipping_service_code') == 'gosend_instant') {
            $detail_id = $this->setGojekDetail('Instant Courier');
            $delivery_type = 'gojek';
            $delivery = $order->deliveries()->create([
                'delivery_type' => $delivery_type,
                'detail_id' => $detail_id,
                'status' => 'not made',
                'order_id' => $order->id
            ]);
        } elseif (request('shipping_service_code') == 'jne_reg') {
            $detail_id = $this->setJneDetail('Reguler');
            $delivery_type = 'jne';
            $delivery = $order->deliveries()->create([
                'delivery_type' => $delivery_type,
                'detail_id' => $detail_id,
                'status' => 'not made',
                'order_id' => $order->id
            ]);
        } elseif (request('shipping_service_code') == 'jne_yes') {
            $detail_id = $this->setJneDetail('YES');
            $delivery_type = 'jne';
            $delivery = $order->deliveries()->create([
                'delivery_type' => $delivery_type,
                'detail_id' => $detail_id,
                'status' => 'not made',
                'order_id' => $order->id
            ]);
        } else {
            $delivery_type = 'Seafer Courier';
            $delivery = $order->deliveries()->create([
                'delivery_type' => $delivery_type,
                'detail_id' => NULL,
                'status' => 'not made',
                'order_id' => $order->id
            ]);
        }

        try{
            Mail::send('emails.order_done', compact('user'), function ($message) use ($user) {

                $message->from('hello@seafermart.co.id', '[NO-REPLY] Order Submitted');

                $message->to($user->email);

            });
        }catch(Exception $e) {

        }

        return $this->make_response(true, 'Order successfully made',$ord_id);
    }

    public function postOrderDetailBackup(Request $request){

        $validator = Validator::make($request->all(), [
            'orderid'                => 'required',
        ]);

        if ($validator->fails()) {
            return $this->make_response(false, 'Please complete the required parameters', $validator->errors());
        }

        $order = Order::find($request->orderid);

        if(null == $order) {
            return $this->make_response(false, 'Order not found');
        }

        $order->user_details = User::withTrashed()->find($order->user_id);

        if(null == $order->user_details) {
            return $this->make_response(false, 'Ooops this is an old order, with old structure. try to open a late order or create a new one.');   
        }

        $order->address_details = Address::find($order->user_address_id);

        $order->groceries = OrderGroceries::where('order_id', $order->id)->get();

        if($order->groceries->count() > 0) {
            foreach($order->groceries as $grc){

                $grc->metric_name = Metric::find($grc->metric_id);
            }
        }


        $check = DB::table('orders')->select('orders.*','order_statuses.description as order_status_desc')
                                    ->leftJoin('order_statuses','order_statuses.id','=','orders.order_status')
                                    ->where('orders.id','=',$orderid)
                                    ->first();
        if($check){
            $check->user_details = DB::table('users')->where('id','=',$check->user_id)->first();
            $check->address_details = DB::table('user_addresses')->where('id','=',$check->user_address_id)->first();
            $check->groceries = DB::table('order_groceries')
                                ->select('order_groceries.*','groceries.name as product_name','groceries.price as product_price','metrics.name as metric_name')
                                ->leftJoin('groceries','groceries.id','=','order_groceries.groceries_id')
                                ->leftJoin('metrics','metrics.id','=','groceries.metric_id')
                                ->where('order_id','=',$check->id)
                                ->get();
            $shipping_code = $check->shipping_service_code;
            if($shipping_code == 'jne_reg'){
                $check->shipping_service_description = 'JNE Regular';
            } elseif($shipping_code == 'jne_yes'){
                $check->shipping_service_description = 'JNE Yes';
            } elseif($shipping_code == 'jne_oke'){
                $check->shipping_service_description = 'JNE Oke';
            } elseif ($shipping_code == 'gosend_sameday') {
                $check->shipping_service_description = 'Gosend Sameday Courier';
            } elseif($shipping_code == 'gosend_instant') {
                $check->shipping_service_description = 'Gosend Instant Courier';
            } elseif($shipping_code == 'internal') {
                $check->shipping_service_description = 'Seafer Sameday Courier';
            } else {

                $check->shipping_service_description = 'Courier Undefined';
            }
            $get_cashback = DB::table('cashback_rewards')
                            ->where('order_id','=',$check->id)
                            ->first();
            if($get_cashback){
                $check->cashback = $get_cashback->nominal;
            } else {
                $check->cashback = 0;
            }
            return $this->make_response(true,'Order found',$check);
        } else {
            return $this->make_response(false,'Order not found',null);
        }
    }

    public function postOrderDetail(Request $request){
        $orderid = $request->orderid;
        $order = Order::find($request->orderid);
        if(null == $order) {
            return $this->make_response(false,'Order not found',null);
        }
        $check = DB::table('orders')->select('orders.*','order_statuses.description as order_status_desc')
                                    ->leftJoin('order_statuses','order_statuses.id','=','orders.order_status')
                                    ->where('orders.id','=',$orderid)
                                    ->first();
        if($check){
            $check->user_details = DB::table('users')->where('id','=',$check->user_id)->first();
            $check->address_details = DB::table('user_addresses')->where('id','=',$check->user_address_id)->first();
            $check->groceries = DB::table('order_groceries')
                                ->select('order_groceries.*','groceries.name as product_name','groceries.price as product_price','metrics.name as metric_name')
                                ->leftJoin('groceries','groceries.id','=','order_groceries.groceries_id')
                                ->leftJoin('metrics','metrics.id','=','groceries.metric_id')
                                ->where('order_id','=',$check->id)
                                ->get();
            $shipping_code = $check->shipping_service_code;
            if($shipping_code == 'jne_reg'){
                $check->shipping_service_description = 'JNE Regular';
            } elseif($shipping_code == 'jne_yes'){
                $check->shipping_service_description = 'JNE Yes';
            } elseif($shipping_code == 'jne_oke'){
                $check->shipping_service_description = 'JNE Oke';
            } elseif ($shipping_code == 'gosend_sameday') {
                $check->shipping_service_description = 'Gojek';
            } elseif($shipping_code == 'gosend_instant') {
                $check->shipping_service_description = 'Gosend Instant Courier';
            } elseif($shipping_code == 'internal') {
                $check->shipping_service_description = 'Seafer Sameday Courier';
            } else {
                if($order->deliveries->delivery_type == 'gojek') {
                    $check->shipping_service_description = 'Gojek';
                } else {
                    $check->shipping_service_description = 'Courier Undefined';    
                }
                
            }
            $get_cashback = DB::table('cashback_rewards')
                            ->where('order_id','=',$check->id)
                            ->first();
            if($get_cashback){
                $check->cashback = $get_cashback->nominal;
            } else {
                $check->cashback = 0;
            }

            foreach($check->groceries as $item){
                $item->product_image = Groceries::getFirstImageUrl($item->groceries_id);
            }

            return $this->make_response(true,'Order found',$check);
        } else {
            return $this->make_response(false,'Order not found',null);
        }
    }

    public function postShippingPrice(Request $request) {
        $address_id = $request->address_id;
        $shipping_code = $request->shipping_code;
        $address = Address::find($address_id);
        if($address){
            if($shipping_code == 'jne_reg' || $shipping_code == 'jne_yes'){
                if(isset($address->city->city_name)){
                    $city = $address->city->city_name;
                } else {
                    $city = false;
                }
                if($city){
                    $phrase = explode(' ', $city);
                    $client = new Client([
                        // Base URI is used with relative requests
                        'base_uri' => 'http://api.jne.co.id:8889/',
                        // You can set any number of default request options.
                        'timeout'  => 2.0,
                    ]);
                    $body_array = array('username' => env('JNE_USERNAME'), 'api_key' => env('JNE_API_KEY') );
                    $body = \GuzzleHttp\json_encode($body_array);

                    $res = $client->request('POST', '/tracing/seafermart/origin/key/'.strtolower($phrase[0]), [
                        'form_params' => [
                            'username' => env('JNE_USERNAME'),
                            'api_key' => env('JNE_API_KEY'),
                        ]
                    ]);

                    if ($res->getReasonPhrase() == "OK") {
                        $city_detail = json_decode($res->getBody())->detail;
                    } else {
                        $city_detail = false;
                    }


                    if(false !== $city_detail) {

                        $res = $client->request('POST', '/tracing/seafermart/price/', [
                            'form_params' => [
                                'username' => env('JNE_USERNAME'),
                                'api_key' => env('JNE_API_KEY'),
                                'from' => 'CGK10000',
                                'thru' => $city_detail[0]->code,
                                'weight' => 1
                            ]
                        ]);

                        if($res->getReasonPhrase() == "OK"){
                            $all_result = json_decode($res->getBody());
                            $price_result = $all_result->price;

                            if ($shipping_code == 'jne_yes') {
                                
                                $shippingFee = floatval($price_result[4]->price);
                                
                            } elseif ($shipping_code == 'jne_reg') {
                                
                                $shippingFee = floatval($price_result[2]->price);

                            }
                        }
                    }
                } else {
                    $shippingFee = 0;
                }
            } else if($shipping_code == 'gosend_sameday'){
                $shippingFee = floatval(15000);
            } else {
                $shippingFee = floatval(20000);
            }
            if($shippingFee != 0){
                return $this->make_response(true,'Shipping price found',$shippingFee);
            } else {
                return $this->make_response(false,'Address not supported city name not found',null);
            }
        } else {
            return $this->make_response(false,'Address not found',null);
        }
    }

    public function getShippingMethod(){
        $sm = array(
            'JNE' => array('jne_reg','jne_yes'),
            'GOSEND' => array('gosend_sameday'),
            'SEAFER' => array('seafer_sameday')
        );
        return $this->make_response(true,'Shipping Method List',$sm);
    }
}