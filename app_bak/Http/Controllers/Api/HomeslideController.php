<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\Home\Slide;
use Illuminate\Http\Response;
use App\Models\Groceries;

class HomeslideController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex()
    {
        $slide = Slide::where('is_active', 1)->get();
        
        $status = 400;
        $content_type = 'application/json';

        if($slide) {
            foreach($slide as $row){
                $row->file_url = url().'/headlines/'.$row->file_name;
            }
            $status = 200;
            $response = array(
                'data' => $slide,
                'status' => true,
                'msg' => 'Home slide available'
            );
        } else {
            $response = array(
                'status' => true,
                'msg' => 'No data available'
            );
        }

        return (new Response($response, $status))
                    ->header('Content-Type', $content_type);
    }
    public function getHomeProduct()
    {
        $parent_category = DB::table('groceries_categories')
        ->select('groceries_categories.*','parent_category_images.file as image_category_file')
        ->leftJoin('parent_category_images','groceries_categories.parent_category_image_id','=','parent_category_images.id')
        ->get();
        $products = collect();
        $pid = array();
        foreach($parent_category as $pc){
            $category = DB::table('child_categories')
            ->where('parent_id','=',$pc->id)
            ->get();
            foreach($category as $ct){
                DB::enableQueryLog();
                $product = DB::table('groceries_category')
                        ->select('grc.*','metrics.name as metric_name','promos.promo_type','promos.discount_value')
                        ->leftJoin('groceries as grc','grc.id','=','groceries_category.groceries_id')
                        ->leftJoin('metrics','metrics.id','=','grc.metric_id')
                        ->leftJoin('promos','promos.id','=','grc.discount_id')
                        ->where('groceries_category.category_id','=',$ct->id)
                        ->where('grc.id','!=','null')
                        ->whereNotIN('grc.id',$pid)
                        ->get();
                if(count($product) > 0){
                    foreach($product as $p){
                        if($p->discount_value != null && $p->discount_value > 0){
                            if($p->promo_type == 'percent'){
                                $p->price_after_discount = $p->price - ($p->price * $p->discount_value / 100);
                            } else if($p->promo_type == 'nominal'){
                                $p->price_after_discount = $p->price - $p->discount_value;
                            }
                        }
                        $images = DB::table('groceries_images')
                                    ->where('groceries_id','=',$p->id)
                                    ->get();
                        if(count($images) > 0){
                            foreach($images as $img){
                                $img->image_url = url('/contents/'.$img->file_name);
                            }
                            $p->images = $images;
    
                        } else {
                            $p->images = 'No Images Available';
                        }
                        if(!in_array($p->id,$pid)){
                            array_push($pid,$p->id);
                        }
                    }
                    $products = $products->merge($product);
                }
            }
            $pc->products = $products;
        }
        $response =  array(
            'status' => true,
            'data' => $parent_category,
            'msg' => 'Data is available',
            'product_id' => $pid
        );
        return (new Response($response, 200))->header('Content-Type', 'application/json');
    }
    public function getFeaturedProduct()
    {
        $groceries = new Groceries;
        $product = $groceries->where('stock','>', 0)->orderBy('created_at')->skip(0)->take(5)->get();
        foreach($product as $p){
            if($p->discount_value != null && $p->discount_value > 0){
                if($p->promo_type == 'percent'){
                    $p->price_after_discount = $p->price - ($p->price * $p->discount_value / 100);
                } else if($p->promo_type == 'nominal'){
                    $p->price_after_discount = $p->price - $p->discount_value;
                }
            }
            $images = DB::table('groceries_images')
                        ->where('groceries_id','=',$p->id)
                        ->get();
            if(count($images) > 0){
                foreach($images as $img){
                    $img->image_url = url('/contents/'.$img->file_name);
                }
                $p->images = $images;

            } else {
                $p->images = 'No Images Available';
            }
        }
        $response =  array(
            'status' => true,
            'data' => $product,
            'msg' => 'Data is available'
        );
        return (new Response($response, 200))
        ->header('Content-Type', 'application/json');
    }
}
