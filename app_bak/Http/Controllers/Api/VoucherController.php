<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;

class VoucherController extends Controller
{
    private function make_response($status = true, $message = '', $data = null) {
        if(null == $data) {
            $response = array(
                'status' => $status,
                'msg' => $message
            );
        } else {
            $response = array(
                'data' => $data,
                'status' => $status,
                'msg' => $message
            );
        }

        return (new Response($response, 200))
        ->header('Content-Type', 'application/json');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function postValidate(Request $request)
    {
        $userid = $request->userid;
        $promocode = $request->promocode;

        if($userid){
            if($promocode){
                $promo = DB::table('promos')
                        ->select('promos.*')
                        ->where('promos.code','=',$promocode)
                        ->first();
                if($promo){
                    return $this->make_response(true,'Promo code found',$promo);
                } else {
                    return $this->make_response(false,'Promo code not found',null);
                }
            } else {
                return $this->make_response(false,'Please input promo code',null);
            }
        } else {
            return $this->make_response(false,'Please input userid',null);
        }
    }
}