<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Response;
use App\Models\Cart;
use App\Models\City;
use App\Models\Location\Area;
use App\Models\Order;
use App\Models\Province;
use App\Models\User;
use App\Models\User\Address;
use App\Models\User\ProfilePicture;
use Auth;
use Carte;
use Socialite;
use Validator;

class UserController extends Controller
{
    private function make_response($status = true, $message = '', $data = null) {
        if(null == $data) {
            $response = array(
                'status' => $status,
                'msg' => $message
            );
        } else {
            $response = array(
                'data' => $data,
                'status' => $status,
                'msg' => $message
            );
        }

        return (new Response($response, 200))
        ->header('Content-Type', 'application/json');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function postSignIn(Request $request)
    {
        $email = $request->email;
        $password = $request->password;
        $users = DB::table('users')
                ->where('email','=',$email)
                ->first();
        if($users){
            if (\Hash::check($password, $users->password)){
                $users->plain_password = $password;
                $user_name = explode(' ', $users->name, 2);
                $users->first_name = $user_name[0];
                if(count($user_name) > 1){
                    $users->last_name = $user_name[1];
                } else {
                    $users->last_name = null;
                }
                $response = array(
                    'data' => $users,
                    'status' => true,
                    'msg' => 'Login Success'
                );
            } else {
                $response = array(
                    'status' => false,
                    'msg' => 'Wrong Password'
                );
            }
        } else {
            $response = array(
                'status' => false,
                'msg' => 'Login Failed'
            );
        }
        return (new Response($response, 200))
        ->header('Content-Type', 'application/json');
    }

    public function postSignUp(Request $request)
    {
        $fullname = $request->fullname;
        $email = $request->email;
        $phone = $request->phone;
        $dob = $request->dob;
        $gender = $request->gender;
        $password = $request->password;
        $c_password = $request->c_password;
        $confirmation_code = str_random(30);
        $check_user = DB::table('users')
                        ->where('email','=',$email)
                        ->first();
        if($fullname && $email && $phone && $dob && $gender && $password && $c_password){
            if(!$check_user){
                if($password == $c_password){
                    $saved_password = \Hash::make($password);
                    $insert = DB::table('users')->insertGetId(
                        [
                        'name' => $fullname,
                        'email' => $email, 
                        'phone_number' => $phone,
                        'dob' => date('Y-m-d',strtotime($dob)),
                        'sex' => $gender,
                        'password' => $saved_password,
                        'is_active' => 0,
                        'registration_code' => $confirmation_code
                        ]
                    );
                    if($insert){
                        $userdata = DB::table('users')
                        ->where('id','=',$insert)
                        ->first();

                        $user = $userdata;

                        try{
                            Mail::send('emails.signup', compact('confirmation_code', 'user'), function ($message) use ($user) {

                                $message->from('hello@seafermart.co.id', '[NO-REPLY] Kirin from SEAFERMART');

                                $message->to($user->email)->subject('Verify your email address');

                            });
                        }catch(Exception $e) {
                            $response = array(
                            'status' => false,
                            'msg' => $e,
                        );
                        }

                        $response = array(
                            'data' => $userdata,
                            'status' => true,
                            'msg' => 'Register success'
                        );
                    } else {
                        $response = array(
                            'status' => false,
                            'msg' => 'Register failed when save the data'
                        );
                    }
                } else {
                    $response = array(
                        'status' => false,
                        'msg' => 'Password doesnt match'
                    );
                }
            } else {
                $response = array(
                    'status' => false,
                    'msg' => 'Email already used by other user'
                );
            }
        } else {
            $response = array(
                'status' => false,
                'msg' => 'Please complete parameter'
            );
        }

        return (new Response($response, 200))
        ->header('Content-Type', 'application/json');
    }

    public function postForgotPassword(Request $request)
    {
        $email = $request->email;
        $user = DB::table('users')
        ->where('email','=',$email)
        ->first();
        if($user){
            $random = substr(md5(microtime()),rand(0,26),6);
            $password = \Hash::make($random);
            Mail::send('emails.forgotmobile', compact('user','random'), function ($message) use ($user) {
                
                                $message->from('hello@seafermart.co.id', '[NO-REPLY] Kirin from SEAFERMART');
                
                                $message->to($user->email)->subject('Forgot Password');
                
                            });
            DB::table('users')
                ->where('id', $user->id)
                ->update(['password' => $password]);
            $data = array(
                'email' => $email
            );
            $response = array(
                'data' => $data,
                'status' => true,
                'msg' => 'New password sent to email '.$email
            );
        } else {
            $response = array(
                'status' => false,
                'msg' => 'No user found with this email'
            );
        }
        return (new Response($response, 200))
        ->header('Content-Type', 'application/json');
    }

    public function postListAddressBook(Request $request) {
        $validator = Validator::make($request->all(), [
            'user_id'          => 'required',
        ]);

        if ($validator->fails()) {
            return $this->make_response(false, 'Please complete the required parameters', $validator->errors());
        } else {
            $user = User::find($request->user_id);
            if(null == $user) {
                return $this->make_response(false, 'User not found');
            } else {
                $addresses = Address::where('user_id', $user->id)->where('deleted', 0)->get();
                if (null == $addresses) {
                    return $this->make_response(false, 'Address not found');
                } else {
                    foreach ($addresses as $address) {
                        $address->area_name = Area::find($address->area_id)->name;
                        if($address->coordinates){
                            $coordinates = explode(',',$address->coordinates);
                            $address->lat = $coordinates[0];
                            $address->long = $coordinates[1];
                        } else {
                            $address->lat = 0;
                            $address->long = 0;
                        }
                        if(null !== $address->city_id || $address->city_id !== 0) {
                            $city = City::find($address->city_id);
                            $address->city_name = $city->city_name;
                            $address->province_name = Province::find($city->province_id)->province_name;    
                        } else {
                            $address->city_name = 'Not Available';
                            $address->province_name = 'Not Available';
                        }
                        
                    }
                    return $this->make_response(true, 'Address is available', $addresses);
                }
            }
        }
    }

    public function postAddAddressBook(Request $request) {

        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'recipient_title' => 'required',
            'first_name'  => 'required',
            'address'   => 'required',
            'area'  => 'required',
            'city'  => 'required',
            'postal_code'   => 'required',
            'phone_number'  => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return $this->make_response(false, 'Please complete the required parameters', $validator->errors());
        }
        
        $user = User::find($request->user_id);

        if(null == $user) {
            return $this->make_response(false, 'User not found');
        }
        
        if(null !== $request->last_name) {
            $name = $request->first_name.' '.$request->last_name;
        } else {
            $name = $request->first_name;
        }

        $address = new Address;
        $address->user_id = $user->id;
        $address->name = $name;
        $address->recipient_title = $request->recipient_title;
        $address->recipient = $request->recipient_name;
        $address->address = $request->address;
        $address->area_id = $request->area;
        $address->city_id = $request->city;
        $address->postal_code = $request->postal_code;
        $address->phone_number = $request->phone_number;
        $address->coordinates = $request->lat.','.$request->long;
        $status = $address->save();

        if($status) {
            return $this->make_response(true, 'Successfully add address', $address);    
        } else {
            return $this->make_response(false, 'Failed to add address');
        }
    }

    public function postUpdateAddressBook(Request $request, $id) {
        $validator = Validator::make($request->all(), [
            'user_id'          => 'required',
        ]);

        if ($validator->fails()) {
            return $this->make_response(false, 'Please complete the required parameters', $validator->errors());
        }
        
        $user = User::find($request->user_id);

        if(null == $user) {
            return $this->make_response(false, 'User not found');
        }

        $address = Address::find($id);

        if(null == $address) {
            return $this->make_response(false, 'Address not found');
        }

        if($address->deleted == 1) {
            return $this->make_response(false, 'Address has been deleted');   
        }

        if($address->user_id != $user->id) {
            return $this->make_response(false, 'Address owner does not match user id');      
        }

        $input = $request->all();
        $input['coordinates'] = $request->lat.','.$request->long;
        $status = $address->update($input);

        if($status) {
            return $this->make_response(true, 'Successfully update address', $address);    
        } else {
            return $this->make_response(false, 'Failed to update address');
        }
        
    }

    public function postDeleteAddressBook(Request $request, $id) {
        $validator = Validator::make($request->all(), [
            'user_id'          => 'required',
        ]);

        if ($validator->fails()) {
            return $this->make_response(false, 'Please complete the required parameters', $validator->errors());
        }
        
        $user = User::find($request->user_id);

        if(null == $user) {
            return $this->make_response(false, 'User not found');
        }

        $address = Address::find($id);

        if(null == $address) {
            return $this->make_response(false, 'Address not found');
        }

        if($address->deleted == 1) {
            return $this->make_response(false, 'Address has been deleted');   
        }

        if($address->user_id != $user->id) {
            return $this->make_response(false, 'Address owner does not match user id');      
        }

        $address->deleted = 1;

        $status = $address->save();

        if($status) {
            return $this->make_response(true, 'Successfully delete address', $address);    
        } else {
            return $this->make_response(false, 'Failed to delete address');
        }
    }

    public function postPointBalance(Request $request)
    {
        $user_id = $request->user_id;
        if($user_id){
            $user_point = DB::table('cashback_points')
                        ->where('user_id','=',$user_id)
                        ->first();
            if($user_point){
                return $this->make_response(true,'User Point found',$user_point);
            } else {
                return $this->make_response(false,'User not found',null);
            }
        } else {
            return $this->make_response(false,'Please input user id',null);
        }
    }

    public function postPointBalanceHistory(Request $request)
    {
        $user_id = $request->user_id;
        if($user_id){
            $user_point = DB::table('cashback_rewards')
                        ->where('user_id','=',$user_id)
                        ->get();
            if($user_point){
                foreach ($user_point as $up) {
                    $order_details = DB::table('orders')
                                            ->where('id','=',$up->order_id)
                                            ->first();
                    if($order_details){
                        $up->order_details = $order_details;
                    } else {
                        $up->order_details = 'Order details not found.';
                    }
                }
                return $this->make_response(true,'User Point History found',$user_point);
            } else {
                return $this->make_response(false,'User Point History not found',null);
            }
        } else {
            return $this->make_response(false,'Please input user id',null);
        }
    }

    public function postTransactionHistory(Request $request)
    {
        $user_id = $request->user_id;
        if($user_id){
            $user_point = DB::table('orders')
                        ->select('orders.*','order_statuses.description as order_status_desc')
                        ->leftJoin('order_statuses','order_statuses.id','=','orders.order_status')
                        ->where('user_id','=',$user_id)
                        ->get();
            foreach ($user_point as $row) {
                $row->order_no = '#'.$row->id;
            }
            if($user_point){
                return $this->make_response(true,'User Transaction History found',$user_point);
            } else {
                return $this->make_response(false,'User Transaction History not found',null);
            }
        } else {
            return $this->make_response(false,'Please input user id',null);
        }
    }

    public function postEditProfile(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'user_id'          => 'required',
            'profile_picture'          => 'mimes:jpeg,bmp,png',
        ]);

        if ($validator->fails()) {
            return $this->make_response(false, 'Please complete the required parameters', $validator->errors());
        }
        
        $user = User::find($request->user_id);

        if(null == $user) {
            return $this->make_response(false, 'User not found');    
        }

        if (null !== $request->new_email || null !== $request->current_email || null !== $request->confirm_email) {
            if(null == $request->current_email || null == $request->confirm_email) {
                return $this->make_response(false, 'Current email and Confirm email must be provided in order to change email.');
            } else {

                if($request->current_email !== $user->email){
                    return $this->make_response(false, 'Current email does not match user id email');
                }

                if($request->new_email == $request->confirm_email) {
                    $user->email = $request->new_email;
                } else {
                    return $this->make_response(false, 'New email not matched the confirmed email');
                }
            }

        }

        if(null !== $request->first_name && null !== $request->last_name) {
            $user->name = $request->first_name.' '.$request->last_name;
        }

        if (null !== $request->dob) {
            $user->dob = date('Y-m-d',strtotime($request->dob));    
        }

        if(null !== $request->sex) {
            $user->sex = $request->sex;
        }

        if($file = $request->file('profile_picture')) {
            $name = time().$file->getClientOriginalName();

            $file->move('profile_picture', $name);

            $photo = ProfilePicture::create(['file' => $name]);

            $user->profile_picture = (int) $photo->id;
            $profile_picture_url = DB::table('profile_pictures')
                                        ->where('id','=',$photo->id)
                                        ->first();
            if($profile_picture_url){
                $user->profile_picture = $profile_picture_url->id;
            } else {
                $user->profile_picture = NULL;
            }
        }

        $status = $user->save();

        if(! $status) {
            return $this->make_response(false, "Update profile failed!");
        }

        return $this->make_response(true, 'Update profile succeed', $user);
    }

    public function postSocialAuth($provider=null)
    {

        if(!config("services.$provider")) abort('404'); //just to handle providers that doesn't exist

        return Socialite::driver('google')->stateless()->with(['code' => ''])->user();

        /*if($provider == 'facebook') {
            return Socialite::driver('facebook')->redirect();
        } else {
            return Socialite::driver('google')->redirect();
        }*/

    }
    public function getSocialAuthCallback(Request $request, $provider=null)
    {
        /*if($user = $this->socialite->with($provider)->user()){
            dd($user);
        }else{
            return 'something went wrong';
        }*/

        if($request->input('error')) {
            $error = $request->input('error');
            $error_code = $request->input('error_code');
            $error_description = $request->input('error_description');

            $request->session()->flash('alert-danger', $error_description);

            redirect('/');
        }

        try {
            $user = Socialite::driver($provider)->user();
        } catch (\Exception $e) {

            return redirect('login/'.$provider);
        }

        $auth_user = $this->findOrCreateUser($user, $provider);

        Auth::login($auth_user, true);

        $merge_cart = $this->sync_cart(Auth::user()->id);

        return $this->make_response(true, 'User is available', $auth_user);

    }

    public function postArea(Request $request) {

        $validator = Validator::make($request->all(), [
            'area_id'          => 'numeric',
        ]);

        if ($validator->fails()) {
            return $this->make_response(false, 'Please complete the required parameters', $validator->errors());
        }

        if (null == $request->area_id) {
            $areas = Area::get();
        } else {
            $areas = Area::find($request->area_id);
        }

        if (null == $areas) {
            return $this->make_response(false, 'Area Not Found');
        }

        if (null == $request->area_id) {
            return $this->make_response(true, 'Areas are available', $areas);
        } else {
            return $this->make_response(true, 'Area is available', $areas);
        }
        
    }

    public function postCity(Request $request) {

        $validator = Validator::make($request->all(), [
            'city_id'          => 'numeric',
        ]);

        if ($validator->fails()) {
            return $this->make_response(false, 'Please complete the required parameters', $validator->errors());
        }

        if (null == $request->city_id) {
            $cities = City::get();
        } else {
            $cities = City::find($request->city_id);
        }

        if (null == $cities) {
            return $this->make_response(false, 'City Not Found');
        }

        if (null == $request->city_id) {
            return $this->make_response(true, 'Cities are available', $cities);
        } else {
            return $this->make_response(true, 'City is available', $cities);
        }
    }

    public function postProvince(Request $request) {

        $validator = Validator::make($request->all(), [
            'province_id'          => 'numeric',
        ]);

        if ($validator->fails()) {
            return $this->make_response(false, 'Please complete the required parameters', $validator->errors());
        }

        if (null == $request->province_id) {
            $provinces = Province::get();
        } else {
            $provinces = Province::find($request->province_id);
        }

        if (null == $provinces) {
            return $this->make_response(false, 'Province Not Found');
        }

        if (null == $request->province_id) {
            return $this->make_response(true, 'Provinces are available', $provinces);
        } else {
            return $this->make_response(true, 'Province is available', $provinces);
        }
    }

    public function postBank(Request $request){
        $userid = $request->userid;
        $check = DB::table('user_banks')->where('user_id','=',$userid)->get();
        if($check){
            return $this->make_response(true, 'Bank Account found for this account', $check);
        } else {
            return $this->make_response(false, 'Bank account not found for this account', null);
        }
    }
    public function postAddBank(Request $request){
        $userid = $request->userid;
        $bank_name = $request->bank_name;
        $branch = $request->branch;
        $account_name = $request->account_name;
        $account_number = $request->account_number;
        $preference = $request->preference;
        $created_date = date('Y-m-d H:i:s');
        $update_date = date('Y-m-d H:i:s');
        if($userid && $bank_name && $account_name && $account_number){
            if(!$preference){
                $preference = 0;
            } else {
                $data_update_non_default = array(
                    'is_preference' => 0,
                    'updated_at' => $update_date
                );
                $update_non_default = DB::table('user_banks')->where('user_id','=',$userid)->update($data_update_non_default);
            }
            $data_insert = array(
                'name' => $bank_name,
                'branch' => $branch,
                'account_name' => $account_name,
                'account_number' => $account_number,
                'user_id' => $userid,
                'is_preference' => $preference,
                'created_at' => $created_date,
                'updated_at' => $update_date
            );
            $insert = DB::table('user_banks')->insert($data_insert);
            if($insert){  
                return $this->make_response(true, 'Success save bank account', $data_insert);
            } else {
                return $this->make_response(false, 'Failed save bank account', null);
            }
        } else {
            return $this->make_response(false, 'Please complete parameters', null);
        }
    }
    public function postUpdateBank(Request $request){
        $userid = $request->userid;
        $bank_id = $request->bank_id;
        $bank_name = $request->bank_name;
        $branch = $request->branch;
        $account_name = $request->account_name;
        $account_number = $request->account_number;
        $preference = $request->preference;
        $update_date = date('Y-m-d H:i:s');
        if($userid && $bank_id && $bank_name && $account_name && $account_number){
            $check_bank = DB::table('user_banks')
                            ->where('id','=',$bank_id)
                            ->where('user_id','=',$userid)
                            ->first();
            if($check_bank) {
                if(!$preference){
                    $preference = 0;
                } else {
                    $data_update_non_default = array(
                        'is_preference' => 0,
                        'updated_at' => $update_date
                    );
                    $update_non_default = DB::table('user_banks')->where('user_id','=',$userid)->update($data_update_non_default);
                }
                $data_update = array(
                    'name' => $bank_name,
                    'branch' => $branch,
                    'account_name' => $account_name,
                    'account_number' => $account_number,
                    'is_preference' => $preference,
                    'updated_at' => $update_date
                );
                $update = DB::table('user_banks')->where('id','=',$bank_id)->update($data_update);
                if($update){  
                    $updated_bank = DB::table('user_banks')->where('id','=',$bank_id)->first();
                    return $this->make_response(true, 'Success update bank account', $updated_bank);
                } else {
                    return $this->make_response(false, 'Failed update bank account', null);
                }
            } else {
                return $this->make_response(false, 'Bank not found', null);
            }
        } else {
            return $this->make_response(false, 'Please complete parameters', null);
        }
    }
    public function postDeleteBank(Request $request){
        $bank_id = $request->bank_id;
        if($bank_id){
            $check = DB::table('user_banks')->where('id','=',$bank_id)->first();
            if($check) {
                $delete = DB::table('user_banks')->where('id','=',$bank_id)->delete();
                if($delete){
                    return $this->make_response(true, 'Bank Account successfully deleted');
                } else {
                    return $this->make_response(false, 'Bank Account failed to dellete');
                }
            } else {
                return $this->make_response(false, 'Bank Account not found', null);
            }
        } else {
            return $this->make_response(false, 'Please insert parameters', null);
        }
    }
    public function postDefaultBank(Request $request){
        $bank_id = $request->bank_id;
        $userid = $request->userid;
        $update_date = date('Y-m-d H:i:s');
        if($bank_id && $userid){
            $check = DB::table('user_banks')->where('id','=',$bank_id)->where('user_id','=',$userid)->first();
            if($check) {
                $data_update_default = array(
                    'is_preference' => 1,
                    'updated_at' => $update_date
                );
                $data_update_non_default = array(
                    'is_preference' => 0,
                    'updated_at' => $update_date
                );
                $update_non_default = DB::table('user_banks')->where('user_id','=',$userid)->update($data_update_non_default);
                $update_default = DB::table('user_banks')->where('id','=',$bank_id)->update($data_update_default);
                if($update_default){  
                    return $this->make_response(true, 'Success update default bank account', null);
                } else {
                    return $this->make_response(false, 'Failed update default bank account', null);
                }
            } else {
                return $this->make_response(false, 'Bank Account not found', null);
            }
        } else {
            return $this->make_response(false, 'Please complete parameters', null);
        }
    }
    public function postMyRatingReviews(Request $request){
        $userid = $request->userid;
        if($userid){
            $ratings = DB::table('ratings')
                        ->select('ratings.*','groceries.*','groceries.id as groceries_id')
                        ->leftJoin('groceries','groceries.id','=','ratings.groceries_id')
                        ->where('user_id','=',$userid)
                        ->get();
            if($ratings){
                return $this->make_response(true, 'Data Found', $ratings);
            } else {
                return $this->make_response(false, 'No Data Found', null);
            }
        } else {
            return $this->make_response(false, 'Please input userid', null);
        }
    }
    public function postMyProfile(Request $request){
        $userid = $request->userid;
        if($userid){
            $userprofile = DB::table('users')
                        ->select('users.*','cashback_points.nominal as current_balance')
                        ->leftJoin('cashback_points','cashback_points.user_id','=','users.id')
                        ->where('user_id','=',$userid)
                        ->first();
            if($userprofile){
                $profile_picture = DB::table('profile_pictures')
                                    ->where('id','=',$userprofile->profile_picture)
                                    ->first();
                if($profile_picture){
                    if(file_exists($profile_picture->file)){
                        $userprofile->profile_picture_url = $profile_picture->file;
                    } else {
                        $userprofile->profile_picture_url = url('profile_picture/'.$profile_picture->file);
                    }
                } else {
                    $userprofile->profile_picture_url = null;
                }
                return $this->make_response(true, 'Data Found', $userprofile);
            } else {
                return $this->make_response(false, 'No Data Found', null);
            }
        } else {
            return $this->make_response(false, 'Please input userid', null);
        }
    }
    public function postUpdatePassword(Request $request){
        $old_pass = $request->old_pass;
        $new_pass = $request->new_pass;
        $confirm_pass = $request->confirm_pass;
        $userid = $request->userid;
        if($old_pass && $new_pass && $confirm_pass && $userid){
            $checkuser = DB::table('users')
                            ->where('id','=',$userid)
                            ->first();
            if($checkuser){
                if(\Hash::check($old_pass, $checkuser->password)){
                    if($new_pass == $confirm_pass){
                        $saved_password = \Hash::make($new_pass);
                        $data_update = array(
                            'password' => $saved_password
                        );
                        $update_pass = DB::table('users')
                                        ->where('id','=',$userid)
                                        ->update($data_update);
                        if($update_pass){
                            return $this->make_response(true,'Password update success',null);
                        } else {
                            return $this->make_response(false,'Password update failed',null);
                        }
                    } else {
                        return $this->make_response(false,'New Password and Confirm Password doesnt match',null);
                    }
                } else {
                    return $this->make_response(false,'Old password is wrong',null);
                }
            } else {
                return $this->make_response(false,'User not found',null);
            }
        } else {
            return $this->make_response(false,'Please complete parameters',null);
        }
    }
    public function postGetEmailPref(Request $request){
        $user_id = $request->user_id;
        if($user_id){
            $check = DB::table('email_preferences')->where('user_id','=',$user_id)->first();
            if($check){
                return $this->make_response(true, 'Data Found', $check);
            } else {
                return $this->make_response(false, 'Data Not Found');
            }
        } else {
            return $this->make_response(false, 'Please complete parameters');
        }
    }
    public function postSetEmailPref(Request $request){
        $user_id = $request->user_id;
        $promo = $request->promo;
        $new_product = $request->new_product;
        $editorial = $request->editorial;
        $low_stock = $request->low_stock;
        $latest_news = $request->latest_news;
        $created_at = date('Y-m-d H:i:s');
        $updated_at = date('Y-m-d H:i:s');
        if($user_id){
            $check_user = DB::table('users')->where('id','=',$user_id)->first();
            if($check_user){
                $check_data = DB::table('email_preferences')->where('user_id','=',$user_id)->first();
                if($check_data){
                    $pref_update = array(
                        'promo' => $promo,
                        'new_product' => $new_product,
                        'editorial' => $editorial,
                        'low_stock' => $low_stock,
                        'latest_news' => $latest_news,
                        'updated_at' => $updated_at
                    );
                    $set_pref = DB::table('email_preferences')->where('id','=',$check_data->id)->update($pref_update);
                } else {
                    $pref_insert = array(
                        'user_id' => $user_id,
                        'promo' => $promo,
                        'new_product' => $new_product,
                        'editorial' => $editorial,
                        'low_stock' => $low_stock,
                        'latest_news' => $latest_news,
                        'updated_at' => $updated_at,
                        'created_at' => $created_at
                    );
                    $set_pref = DB::table('email_preferences')->insert($pref_insert);
                }
                $data_pref = DB::table('email_preferences')->where('user_id','=',$user_id)->first();
                if($set_pref) {
                    return $this->make_response(true, 'Success Set Email Preference',$data_pref);
                } else {
                    return $this->make_response(false, 'Failed set Email Preference');
                }
            } else {
                return $this->make_response(false, 'User Not Found');
            }
        } else {
            return $this->make_response(false, 'Please complete parameters');
        }
    }
}
