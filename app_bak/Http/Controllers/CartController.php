<?php

namespace App\Http\Controllers;

use App\Models\Promo;
use App\Models\PaymentStatus;
use App\Models\User\CashbackPoint;
use Illuminate\Http\Request;
use Carte;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Money;
use App\Http\Requests;
use App\Http\Controllers\Controller;
#use App\Libraries\Money;
use App\Models\Cart;
use App\Models\Groceries;
use App\Models\Location\Area;
use App\Models\SearchableGroceries;
use Agent;

class CartController extends Controller
{
    public function __construct()
    {
        if ( ! auth()->check()) {
            return response([], 401);
        }
    }

    private function check_stock($requested_quantity, $id) {
        $groceries = Groceries::find($id);

        if((int) $groceries->stock == 0) {
            return array(
                'status' => '_ERR',
                'code' => 400,
                'message' => 'This product is out of stock.'
            );
        }
        
        if((int) $groceries->stock >= (int) $requested_quantity) {
            return array(
                'status' => 'OK',
                'code' => 200,
                'message' => 'Item available'
            );
        } else {
            return array(
                'status' => '_ERR',
                'code' => 400,
                'message' => 'There is only '.$groceries->stock.' items remaining.'
            );
            
        }
    }

    private function sync_cart($userid) {
        $db_cart = Cart::where('user_id', $userid)->get();
        $session_cart = Carte::contents();

        if (null !== $db_cart && empty($session_cart)) {

            //add to session
            foreach($db_cart as $d_cart) {
                $item = Carte::insert([
                    'id'            => $d_cart->groceries_id,
                    'name'          => $d_cart->name,
                    'sku'           => $d_cart->sku,
                    'price_per_item'=> floatval($d_cart->price_per_item),
                    'unit_metric'   => $d_cart->unit_metric,
                    'price'         => floatval($d_cart->price),
                    'image'         => $d_cart->image,
                    'quantity'      => intval($d_cart->qty)
                ]);
            }

        } elseif(null == $db_cart && !empty($session_cart)) {

            if(null == $session_cart->unit_metric) {
                $unit_metric = 'NA';
            } else {
                $unit_metric = $session_cart->unit_metric;
            }
            // let it
            foreach($session_cart as $item){
                $cart = new Cart;
                $cart->user_id = $userid;
                $cart->groceries_id = $item->id;
                $cart->qty = $item->quantity;
                $cart->is_read = 0;
                $cart->name = $item->name;
                $cart->sku = $item->sku;
                $cart->price_per_item = $item->price_per_item;
                $cart->unit_metric = $unit_metric;
                $cart->price = $item->price;
                $cart->image = $item->image;
                $cart->quantity = $item->quantity;

                $status = $cart->save();
            }
        }

        return Carte::contents(true);
    }

    public function getAjaxDelete()
    {
        $id = request('id');
        $item = Carte::item($id);

        $removal = 'removing cart.';

        if(auth()->check()){

            $check_db = DB::table('carts')->where('user_id', auth()->user()->id)->where('groceries_id', $item->id)->get();

            if(!empty($check_db)){

                $remove = DB::table('carts')->where('user_id', auth()->user()->id)->where('groceries_id', $item->id)->delete();

                if ($remove) {
                    $removal = 'cart db deleted.';
                } else {
                    $removal = 'cart db not deleted.';
                }


            } else {
                $recheck_db = DB::table('carts')->where('user_id', auth()->user()->id)->where('id', $item->id)->get();

                if(!empty($recheck_db)) {
                    $remove = DB::table('carts')->where('user_id', auth()->user()->id)->where('id', $item->id)->delete();

                    if ($remove) {
                        $removal = 'cart db deleted.';
                    } else {
                        $removal = 'cart db not deleted.';
                    }
                } else {
                    $removal = 'cart db not found.';
                }
            }
        }

        $item->remove();

        $removal .= 'cart session removed';
        $total_item = 0;
        $items = Carte::contents(true);
        
        foreach(Carte::contents(true) as $cart_item){
            $quantity = $cart_item['quantity'];
            $total_item = $total_item + $quantity;
        }
        $empty_cart = $this->emptyCart();
        return compact('removal','total_item','empty_cart');
    }

    public function getAjaxUpdateQty()
    {
        return Cart::updateQty(request('id'), request('count')) ?: response(['msg' => 'Bad request'], 400);
    }

    public function getIndex(Request $request)
    {
        if(Agent::isDesktop()) {
            if(false === auth()->check()) {
                return redirect('/user/sign-in');
            }
        }

        $carts = Carte::contents();

        $my_points = null;
        if(auth()->check()) {
            $my_points = CashbackPoint::where('user_id', auth()->user()->id)->first();    
        }
        
        if( (null == $my_points || $my_points->count() < 0) ) {
            if(auth()->check()){
                $pt = DB::table('cashback_points')->insertGetId(
                    [
                        'user_id' => auth()->user()->id,
                        'nominal' => 0,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]
                );
                $my_points =  CashbackPoint::find($pt);    
            } else {
                $my_points = new CashbackPoint;
                $my_points->nominal = 0;
            }
            
        }
        
        if(Session::has('is_redeem')) {
            $is_redeem = Session::get('is_redeem');
        } else {
            $is_redeem = 0;
        }

        if(count($carts) > 0) {
            $promo_applied = false;
            $promo_detail = false;
            if(Session::has('is_promo')) {
                $promo = Promo::where('code', Session::get('is_promo'))->first();
                $promo_detail = $promo;

                if ($promo->promo_type == 'percent') {
                    $promo_applied = (Carte::total() * $promo->discount_value) / 100;
                } else {
                    $promo_applied = $promo->discount_value;
                }
            }

            if(Agent::isDesktop()) {
                if(Agent::isAndroidOS()) {
                    return view('responsive.order.cart', compact('carts', 'promo_applied','my_points', 'is_redeem', 'promo_detail'));    
                }

                return view('cart', compact('carts', 'promo_applied','my_points', 'is_redeem', 'promo_detail'));    
            } else {
                return view('responsive.order.cart', compact('carts', 'promo_applied','my_points', 'is_redeem', 'promo_detail'));
            }

        } else {
            if(! Agent::isDesktop()) {
                $promo_applied = false;
                $promo_detail = false;
                return view('responsive.order.cart', compact('carts', 'promo_applied','my_points', 'is_redeem', 'promo_detail'));
            }
        }

        return redirect('/');
    }

    public function postAddItem()
    {
        $id = request('id');
        $qty = request('qty');

        $availability = $this->check_stock($qty, $id);

        if($availability['status'] == '_ERR') {
            return response($availability)->header('Content-Type', 'application/json');
        }

        // Hack attempt
        if ($qty < 1) {
            return [
                'status' => 'OK'
            ];
        }

        $cart = Cart::whereUserId(auth()->user()->id)->whereGroceriesId($id)->first();

        // Already exists
        if ( ! is_null($cart)) {
            $cart->qty += $qty;

        // New
        } else {
            $cart = new Cart;
            $cart->user_id = auth()->user()->id;
            $cart->groceries_id = $id;
            $cart->qty = $qty;
        }

        $cart->save();

        $item = Groceries::findOrFail($id);

        return [
            'status' => 'OK'
        ];
    }

    public function postRedeemPoints(Request $request) {
        
        $current_balance = CashbackPoint::where('user_id', auth()->user()->id)->first();
        
        $value = floatval($request->value);
        if($value < 0) {
            $response = array('status' => 'ERR', 'status_code' => 400, 'message' => 'Insufficent points input');
            return json_encode($response); 
        }

        if($value < floatval($current_balance->nominal)) {
            if($value == 0) {
                $request->session()->forget('is_redeem');
                $response = array('status' => 'OK', 'status_code' => 200, 'message' => $value);
                return response()->json($response);
            } else {
                $redeem_applied = false;
                $request->session()->put('is_redeem', $value);

                $response = array('status' => 'OK', 'status_code' => 200, 'message' => $value);
                return response()->json($response);
            }

        } else {
            $response = array('status' => 'ERR', 'status_code' => 400, 'message' => 'Insufficent points input');
            return response()->json($response);  
        }
    }

    public function getResetClaim(Request $request){
        if(Session::has('is_redeem')) {
            $request->session()->forget('is_redeem');
            return back();
        } else {
            return back();
        }
    }

    public function getMyCart()
    {

        $contents = Carte::contents(true);


        if(false !=  auth()->check()) {
            $cart = $this->sync_cart(auth()->user()->id);
            $contents = $cart;
        }

        if(empty($contents)){
            return $this->emptyCart();
        }

        return $this->mycartPopup($contents);
    }

    public function mycartPopup($cart_content)
    {

        $item_content = '';

        $total = Carte::total();
        foreach ($cart_content as $key=>$cart) {
            $gr = Groceries::find($cart['id']);
            $item_content .= '<tr class="cartItem" id="'.$key.'">
                <td><a href="'.url('groceries/'.$gr->id.'/'.str_slug($gr->name)).'"><div class="img-item" style="background-image:url('.$cart['image'].');background-size:cover"></div></a></td>
                <td><a href="'.url('groceries/'.$gr->id.'/'.str_slug($gr->name)).'">'.$cart['name'].'</a><br>'.Money::display($cart['price']) .'</td>
                <td>
                    <ul>
                        <li><button class="minus decrease_cart_item_qty">-</button></li>
                        <li>
                            <input type="text" class="total" value="'.$cart['quantity'].'">
                            <input class="cart_id" type="hidden" value="'.$key.'" />
                        </li>
                        <li><button class="plus increase_cart_item_qty">+</button></li>
                    </ul>
                </td>
                <td class="item_total">'.Money::display($cart['price_per_item'] * $cart['quantity']).'</td>
                <td>
                    <input class="item_id" type="hidden" value="'.$cart['id'].'" />
                    <button class="remove-item-btn list_del_cart_item_on_pop">&times;</button>
                </td>
            </tr>';
        }



        $cart = '<div class="form-box">
                        <table>
                            <tbody class="popupBodyCart">'.$item_content.'</tbody>
                        </table>
                    </div>
                    <div class="row margin-top">
                        <div class="large-6 columns">
                            <a class="view-more" href="'.url('cart').'">view more</a>
                        </div>
                        <div class="large-6 columns">
                            <div class="large-6 columns no-padding">
                                <span class="sub-total">SUB TOTAL</span>   
                            </div>
                            <div class="large-6 columns no-padding">
                                <span id="cart_popup_total" class="idr">'.Money::display($total).'</span>
                            </div>
                            <button class="button checkout maxwidth" onclick="location=\''.url('cart').'\'">CHECK OUT NOW</button>
                        </div>
                    </div>';

        return $cart;
    }

    public function postCartTotal(Request $request)
    {   
        $total = Carte::total();
        $return = Money::display($total);
        return $return;
    }

    public function emptyCart()
    {
        $template = '<div class="if-empty"><img src="'.asset(null).'assets/images/img-empty.png?'.uniqid().'">
                        <span>Your cart is still empty</span>
                    </div>';

        return $template;
    }

    public function getCartTotalItem()
    {
        $total_item = 0;
        $items = Carte::contents(true);
    
        foreach(Carte::contents(true) as $cart_item){
            $quantity = $cart_item['quantity'];
            $total_item = $total_item + $quantity;
        }
        
        return $total_item;
    }

    public function getTotalPrice() 
    {
        $total = Carte::total(false);

        return $total;
    }

    public function postNewAddItem(Request $request)
    {
        $availability = $this->check_stock($request->qty, $request->id);

        if($availability['status'] == '_ERR') {
            return response($availability)->header('Content-Type', 'application/json');
        }

        $item = Carte::insert([
            'id'            => $request->id,
            'name'          => $request->name,
            'sku'           => $request->sku,
            'price_per_item'=> (float)($request->price_per_item),
            'unit_metric'   => $request->metric,
            'price'         => (float)($request->price),
            'image'         => $request->image,
            'quantity'      => (int)($request->qty)
        ]);

        if(auth()->check()) {

            if(null == $request->metric) {
                $unit_metric = 'NA';
            } else {
                $unit_metric = $request->metric;
            }

            $cart = Cart::whereUserId(auth()->user()->id)->whereGroceriesId($request->id)->first();

            // Already exists
            if ( ! is_null($cart)) {
                $cart->qty += $request->qty;

            // New
            } else {
                $cart = new Cart;
                $cart->user_id = auth()->user()->id;
                $cart->groceries_id =$request->id;
                $cart->qty = (int) $request->qty;
                $cart->is_read = 0;
                $cart->name = $request->name;
                $cart->sku = $request->sku;
                $cart->price_per_item = $request->price_per_item;
                $cart->unit_metric = $unit_metric;
                $cart->price = (float) $request->price;
                $cart->image = $request->image;
                $cart->quantity = (int) $request->qty;
            }

            $status = $cart->save();
        }

        return response(['status' => 'OK', 'code' => 200, 'message' => 'Item added', 'identifier' => $item])->header('Content-Type', 'application/json');
    }

    public function postIncreaseQty(Request $request)
    {
        $item = Carte::item($request->identifier);

        $item->quantity = $item->quantity + 1;

        if(auth()->check()){
            $check_db = DB::table('carts')->where('user_id', auth()->user()->id)->where('groceries_id', $item->id);

            if(!empty($check_db->get())){
                $update = $check_db->update(['quantity' => $item->quantity, 'qty' => $item->quantity, 'updated_at' => date('Y-m-d H:i:s')]);
            }
        }
        $total_price_item = Money::display($item->quantity * $item->price_per_item);
        $qty = $item->quantity;
        return compact('qty','total_price_item');
    }

    public function getCheckStock(Request $request) {
        $item = Carte::item($request->input('id'));
        $grocery = Groceries::find($item->id);
        if(null == $grocery)
            return 0;
        return $grocery->stock;
    }

    public function getTest() 
    {
        $grandtotal = 0;
        foreach( Carte::contents() as $item) {
            $price_per_product = ($item->price * $item->quantity);
            $grandtotal = $grandtotal + $price_per_product;
        }
        return $grandtotal;
    }

    public function postDecreaseQty(Request $request)
    {
        $item = Carte::item($request->identifier);
        if($item->quantity > 1) {
            $item->quantity = $item->quantity - 1;

            if(auth()->check()){
                $check_db = DB::table('carts')->where('user_id', auth()->user()->id)->where('groceries_id', $item->id);

                if(!empty($check_db->get())){
                    $update = $check_db->update(['quantity' => $item->quantity, 'qty' => $item->quantity, 'updated_at' => date('Y-m-d H:i:s')]);
                }
            }
            $total_price_item = Money::display($item->quantity * $item->price_per_item);
            $qty = $item->quantity;
            return compact('qty','total_price_item');
        } else {
            if(auth()->check()){
                $check_db = DB::table('carts')->where('user_id', auth()->user()->id)->where('id', $item->id);

                if(!empty($check_db->get())){
                    $remove = $check_db->delete();
                }
            }
            $item->remove();
            $qty = 0;
            $total_item = 0;
            $items = Carte::contents(true);
        
            foreach(Carte::contents(true) as $cart_item){
                $quantity = $cart_item['quantity'];
                $total_item = $total_item + $quantity;
            }
            $empty_cart = $this->emptyCart();
            return compact('qty','total_item','empty_cart');
        }

    }

    public function postNewRemoveItem(Request $request)
    {
        $item = Carte::item($request->identifier);

        if(auth()->check()){
            $check_db = DB::table('carts')->where('user_id', auth()->user()->id)->where('groceries_id', $item->id);

            if(!empty($check_db->get())){
                $remove = $check_db->delete();
            }
        }

        $item->remove();

    }

    public function postReadUnread(Request $request) 
    {
        $id = $request->id;
        $read_status = $request->read;

        $cart = Cart::findOrFail($id);
        $cart->is_read = (int) $read_status;
        $cart->save();

        return ['status' => 'OK'];
    }

    public function getDestroyCart()
    {
        DB::enableQueryLog();

        $query = DB::table('orders')->where('order_status', 1)->where(DB::raw('HOUR(TIMEDIFF(NOW(), `created_at`))'),'>', 24)->get();

        $laQuery = DB::getQueryLog();
        
        dd($laQuery);
        Carte::destroy();

        return 'destroyed';
    }

    public function getRandomCart()
    {
        $statuses  = array(
            array(
                'code' => 0,
                'description' => 'New Payment',
            ),
            array(
                'code' => 1,
                'description' => 'Approved',
            ),
            array(
                'code' => 2,
                'description' => 'Rejected',
            ),
            array(
                'code' => 3,
                'description' => 'Waiting Completion'
            ),
            array(
                'code' => 4,
                'description' => 'Delay Approval'
            ),
        );

        foreach($statuses as $status) {
            $payment = new PaymentStatus;
            $payment->code = $status['code'];
            $payment->description = $status['description'];
            $payment->save();
        }
        
        return 'all saved!';

    }

    public function getTestNew(Request $request){
        $request->session()->forget('is_promo');
        Session::forget('is_promo');
        dd('ok');
    }

    public function getMigrateSlug() {
        $searchable = SearchableGroceries::all();

        foreach($searchable as $sc) {
            if(null != $sc->groceries) {
                $slug = $sc->groceries->slug;
                
                if($slug != '') {
                    $sc->slug = $slug;    
                } else {
                    $sc->slug = null;
                }
                
                $sc->save();  
            }
            
            /*echo '<pre>';
            echo $slug;
            echo $sc->groceries->id;
            echo '</pre>';*/

            /**/
        }
        dd($searchable);
    }
}
