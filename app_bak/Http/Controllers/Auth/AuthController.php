<?php

namespace App\Http\Controllers\Auth;


use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Auth;
use Socialite;
use App\Models\Cart;
use Carte;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    private function sync_cart($userid) {
        $db_cart = Cart::where('user_id', $userid)->get();
        $session_cart = Carte::contents();

        if(null != $db_cart && ! empty($session_cart)) {

            //add quantity if exist
            foreach($db_cart as $cart) {
                foreach($session_cart as $s_cart){
                    if($cart->groceries_id == $s_cart->id && $cart->sku == $s_cart->sku) {

                        $cart->qty = $cart->qty + $s_cart->quantity;
                        $cart->quantity = $cart->quantity + $s_cart->quantity;
                        $status = $cart->save();

                        Carte::item($s_cart->identifier)->remove();
                    }

                }
            }

            foreach(Carte::contents() as $item){

                if(null == $item->unit_metric) {
                    $unit_metric = 'NA';
                } else {
                    $unit_metric = $item->unit_metric;
                }

                $cart = new Cart;
                $cart->user_id = $userid;
                $cart->groceries_id = $item->id;
                $cart->qty = $item->quantity;
                $cart->is_read = 0;
                $cart->name = $item->name;
                $cart->sku = $item->sku;
                $cart->price_per_item = $item->price_per_item;
                $cart->unit_metric = $unit_metric;
                $cart->price = $item->price;
                $cart->image = $item->image;
                $cart->quantity = $item->quantity;
                $status = $cart->save();

                $item->remove();
            }

            $updated_db_cart = Cart::where('user_id', $userid)->get();

            foreach($updated_db_cart as $d_cart){

                $item = Carte::insert([
                    'id'            => $d_cart->groceries_id,
                    'name'          => $d_cart->name,
                    'sku'           => $d_cart->sku,
                    'price_per_item'=> (float)($d_cart->price_per_item),
                    'unit_metric'   => $d_cart->unit_metric,
                    'price'         => (float)($d_cart->price),
                    'image'         => $d_cart->image,
                    'quantity'      => (int)($d_cart->qty)
                ]);

            }
        }

        elseif(null != $db_cart && empty($session_cart)) {
            $updated_db_cart = Cart::where('user_id', $userid)->get();

            foreach($updated_db_cart as $d_cart){

                $item = Carte::insert([
                    'id'            => $d_cart->groceries_id,
                    'name'          => $d_cart->name,
                    'sku'           => $d_cart->sku,
                    'price_per_item'=> (float)($d_cart->price_per_item),
                    'unit_metric'   => $d_cart->unit_metric,
                    'price'         => (float)($d_cart->price),
                    'image'         => $d_cart->image,
                    'quantity'      => (int)($d_cart->qty)
                ]);

            }
        }

        return Carte::contents(true);
    }

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    public function getLogin(){
        return view('admin.users.login');
    }

    public function getSocialAuth($provider=null)
    {
        if(!config("services.$provider")) abort('404'); //just to handle providers that doesn't exist

        if($provider == 'facebook') {
            return Socialite::driver('facebook')->redirect();
        } else {
            return Socialite::driver('google')->redirect();
        }

    }


    public function getSocialAuthCallback(Request $request, $provider=null)
    {
        /*if($user = $this->socialite->with($provider)->user()){
            dd($user);
        }else{
            return 'something went wrong';
        }*/

        if($request->input('error')) {
            $error = $request->input('error');
            $error_code = $request->input('error_code');
            $error_description = $request->input('error_description');

            $request->session()->flash('alert-danger', $error_description);

            redirect('/');
        }

        try {
            $user = Socialite::driver($provider)->user();
        } catch (\Exception $e) {

            return redirect('login/'.$provider);
        }

        $auth_user = $this->findOrCreateUser($user, $provider);

        Auth::login($auth_user, true);

        $merge_cart = $this->sync_cart(Auth::user()->id);

        return redirect('/');

    }

    /**
     * Return user if exists; create and return if doesn't
     *
     * @param $facebookUser
     * @return User
     */
    private function findOrCreateUser($socialUser, $type)
    {
        $request = new Request;
        if($type == 'facebook') {
            $authUser = User::where('facebook_id', $socialUser->id)->first();
        } else {
            $authUser = User::where('google_id', $socialUser->id)->first();
        }



        if ($authUser){
            if(null == $authUser->fb_token && $type == 'facebook') {
                $authUser->fb_token = $socialUser->token;
                $authUser->save();
            } elseif (null == $authUser->google_token && $type == 'google') {
                $authUser->google_token = $socialUser->token;   
                $authUser->save();
            }
            return $authUser;
        }

        if($type == 'facebook') {
            $additional_data = $socialUser->user;

            if($additional_data['gender'] == 'male') {
                $gender = 'm';
            } else {
                $gender = 'f';
            }

            $check_email = User::where('email', $socialUser->email)->get();

            $refer_from = null;
            if(null !== $request->session()->get('refer_from') || ! empty($request->session()->get('refer_from'))) {
                $refer = $request->session()->get('refer_from');
                $refer_from_user = User::where('referral_code', $refer)->first();
                if(null !== $refer_from_user || !empty($refer_from_user)) {
                    $refer_from = $refer_from_user->id;
                }
            }

            if ($check_email->count() <= 0 || null == $check_email) {
                $profpic_id = DB::table('profile_pictures')->insertGetId(
                    ['file' => $socialUser->avatar, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]
                );

                $sex = 'm';
                if( $additional_data['gender'] == 'm' || $additional_data['gender'] == 'f' ) {
                    $sex = $additional_data['gender'];
                }

                $fname = explode(' ', $socialUser->name);
                return User::create([
                    'name' => $socialUser->name,
                    'email' => $socialUser->email,
                    'facebook_id' => $socialUser->id,
                    'fb_token' => $socialUser->token,
                    'profile_picture' => $profpic_id,
                    'sex' => $sex,
                    'role' => 2,
                    'is_active' => 1,
                    'dob' => '1970-02-01 00:00:01',
                    'phone_number' => 'Not Available',
                    'password' => $socialUser->token,
                    'referral_code' => strtoupper($fname[0]).uniqid(),
                    'refer_from' => $refer_from
                ]);

                $request->session()->forget('refer_from');
            } else {
                $profpic_id = DB::table('profile_pictures')->insertGetId(
                    ['file' => $socialUser->avatar, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]
                );
                DB::table('users')->where('email', $socialUser->email)->update(['facebook_id' => $socialUser->id, 'profile_picture' => $profpic_id,'updated_at' => date('Y-m-d H:i:s')]);
                $check_email = User::where('email', $socialUser->email)->get();
                $request->session()->forget('refer_from');
                return $check_email;
            }
        } else {
            $refer_from = null;
            if(null !== $request->session()->get('refer_from') || ! empty($request->session()->get('refer_from'))) {
                $refer = $request->session()->get('refer_from');
                $refer_from_user = User::where('referral_code', $refer)->first();
                if(null !== $refer_from_user || !empty($refer_from_user)) {
                    $refer_from = $refer_from_user->id;
                }
            }

            $check_email = User::where('email', $socialUser->email)->get();

            if ($check_email->count() <= 0 || null == $check_email) {
                $profpic_id = DB::table('profile_pictures')->insertGetId(
                    ['file' => $socialUser->avatar, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]
                );
                $fname = explode(' ', $socialUser->name);
                return User::create([
                    'name' => $socialUser->name,
                    'email' => $socialUser->email,
                    'google_id' => $socialUser->id,
                    'google_token' => $socialUser->token,
                    'profile_picture' => $profpic_id,
                    'sex' => 'm',
                    'role' => 2,
                    'is_active' => 1,
                    'dob' => '1970-02-01 00:00:01',
                    'phone_number' => 'Not Available',
                    'password' => $socialUser->token,
                    'referral_code' => strtoupper($fname[0]).uniqid(),
                    'refer_from' => $refer_from
                ]);

                $request->session()->forget('refer_from');
            } else {
                $profpic_id = DB::table('profile_pictures')->insertGetId(
                    ['file' => $socialUser->avatar, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]
                );
                DB::table('users')->where('email', $socialUser->email)->update(['google_id' => $socialUser->id, 'profile_picture' => $profpic_id, 'updated_at' => date('Y-m-d H:i:s')]);
                $check_email = User::where('email', $socialUser->email)->get();
                $request->session()->forget('refer_from');
                return $check_email;

            }
        }
    }
}
