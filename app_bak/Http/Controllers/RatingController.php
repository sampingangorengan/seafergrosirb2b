<?php

namespace App\Http\Controllers;

use App\Models\Groceries\Rating;
use App\Models\Order\Groceries;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class RatingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'rate'          => 'required',
            'grid'      => 'required',
            'ogid'      => 'required'
        ]);
        if(!$request->title)
            $request->title = ' ';
        if(!$request->message)
            $request->message = ' ';
        $id = DB::table('ratings')->insertGetId(
            [
                'rate' => intval($request->rate),
                'title' => $request->title,
                'review' => $request->message,
                'user_id' => auth()->user()->id,
                'groceries_id' => intval($request->grid),
                'is_active' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]
        );

        $groceries = Groceries::find($request->ogid);
        $groceries->update(['is_reviewed'=>1, 'rating' => $id]);

        if($id) {
            return 'saved';
        } else {
            return 'failed';
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
