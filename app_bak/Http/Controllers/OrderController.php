<?php

namespace App\Http\Controllers;

use App\Models\User\UserBank;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use GuzzleHttp\Tests\Psr7\Str;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\User\Address;
use Knp\Snappy\Pdf;
use Agent;

class OrderController extends Controller
{
    public function show(Request $request, $id)
    {
        if (false === auth()->check()) {
            return redirect('/');
        }

        $order = Order::find($id);
        if(null == $order) {
            abort(404);
        }

        if(auth()->user()->id != $order->user_id) {
            $request->session()->flash('alert-danger', 'You are not the owner of this order id.');
            return redirect('/');
        }
        $order->address_detail = Address::find($order->user_address_id);
        $smallAdditionalFee = $order->unique_code;
        $bank_preference = UserBank::where('user_id', auth()->user()->id)->where('is_preference', 1)->first();

        if(Agent::isDesktop()) {
            if(Agent::isAndroidOS()) {
                return view('responsive.user.my_order_detail', compact('order', 'smallAdditionalFee', 'bank_preference'));
            }
            return view('order/show', compact('order', 'smallAdditionalFee', 'bank_preference'));
        } else {
            return view('responsive.user.my_order_detail', compact('order', 'smallAdditionalFee', 'bank_preference'));
        }
        
    }

    public function download($id)
    {
        if (false === auth()->check()) {
            return redirect('/');
        }
        $snappy = new Pdf('/usr/local/sbin/wkhtmltopdf');
        /*header('Content-Type: application/pdf');
        header('Content-Disposition: attachment; filename="invoice.pdf"');*/
        try {
            $pdf = $snappy->getOutput(url('orders/showPrint', $id));
            #return $pdf;
            return new Response(
                $pdf,
                200,
                array(
                    'Content-Type'          => 'application/pdf',
                    'Content-Disposition'   => 'attachment; filename="invoice.pdf"'
                )
            );
        } catch (\Exception $e) {
            echo $e->getMessage();  
        }

    }

    public function showPrint($id) {
        $order = Order::find($id);
        $smallAdditionalFee = $order->unique_code;
        return view('order/download', compact('order', 'smallAdditionalFee'));
    }
}
