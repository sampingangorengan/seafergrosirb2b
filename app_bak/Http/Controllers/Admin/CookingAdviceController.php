<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Groceries\CookingAdvice;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class CookingAdviceController extends Controller
{
    protected $active_menu = null;
    protected $active_submenu = null;

    public function getActiveMenu($active_menu) {
        $this->active_menu = $active_menu;

        return $this->active_menu;
    }

    public function getActiveSubmenu($active_menu) {
        $this->active_menu = $active_menu;

        return $this->active_menu;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));
        $advices = CookingAdvice::orderBy('created_at', 'asc')->get();
        return view('admin.products.index_cooking_advice', compact('advices', 'active_menu', 'active_submenu'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));
        
        return view('admin.products.create_cooking_advice', compact('active_menu', 'active_submenu'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'          => 'required',
        ]);

        $cookingAdvice = new CookingAdvice;
        $cookingAdvice->name = $request->name;

        $status = $cookingAdvice->save();

        if($status) {
            $request->session()->flash('alert-success', 'Cooking Advice was successfully added!');
        } else {
            $request->session()->flash('alert-danger', 'Oops, something is wrong during adding cooking advice!');

            return back();
        }

        return redirect('admin/cooking-advice');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));
        $cookingAdvice = CookingAdvice::find($id);
        return view('admin.products.edit_cooking_advice', compact('cookingAdvice', 'active_menu', 'active_submenu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'          => 'required',
        ]);
        $cookingAdvice = CookingAdvice::find($id);

        $status = $cookingAdvice->update($input);

        if($status) {
            $request->session()->flash('alert-success', 'Cooking advice was successfully updated!');
        } else {
            $request->session()->flash('alert-danger', 'Oops, something is wrong during updating cooking advice!');

            return back();
        }


        return redirect('admin/cooking-advice');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cookingAdvice = CookingAdvice::find($id);
        
        $status = $cookingAdvice->delete();

        if($status) {
            $request->session()->flash('alert-success', 'Category was successfully deleted!');
        } else {
            $request->session()->flash('alert-danger', 'Oops, something is wrong during deleting category!');

            return back();
        }

        return redirect('admin/child-category');
    }
}
