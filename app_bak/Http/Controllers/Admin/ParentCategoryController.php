<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Groceries\Category;
use App\Models\Groceries\ChildCategory;
use App\Models\Groceries\ParentCategoryImage;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use File;

class ParentCategoryController extends Controller
{
    protected $active_menu = null;
    protected $active_submenu = null;

    public function getActiveMenu($active_menu) {
        $this->active_menu = $active_menu;

        return $this->active_menu;
    }

    public function getActiveSubmenu($active_menu) {
        $this->active_menu = $active_menu;

        return $this->active_menu;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));
        $categories = Category::get();
        return view('admin.categories.index_parent', compact('categories', 'active_menu', 'active_submenu'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));
        return view('admin.categories.create_parent', compact('active_menu', 'active_submenu'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'name'          => 'required',
            'order'         => 'required|numeric',
            'is_active'     => 'required|numeric',
            'file'          => 'mimes:jpeg,bmp,png'
        ]);

        $category = new Category;
        $category->name = $request->name;
        $category->order = $request->order;
        $category->is_active = $request->is_active;

        if($file = $request->file('file')) {
            $name = time().$file->getClientOriginalName();

            $file->move('parent_category_images', $name);

            $photo = ParentCategoryImage::create(['file' => $name]);

            $category->parent_category_image_id = (int) $photo->id;
        }

        $status = $category->save();

        if($status) {
            $request->session()->flash('alert-success', 'Category was successfully added!');
        } else {
            $request->session()->flash('alert-danger', 'Oops, something is wrong during adding category!');

            return back();
        }

        return redirect('admin/parent-category');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        //
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));
        $category = Category::find($id);
        return view('admin.categories.show_parent', compact('category', 'active_menu', 'active_submenu'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        //
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));
        $category = Category::find($id);
        return view('admin.categories.edit_parent', compact('category', 'active_menu', 'active_submenu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $this->validate($request, [
            'name'          => 'required',
            'order'         => 'required|numeric',
            'file'          => 'mimes:jpeg,bmp,png'
        ]);

        $category = Category::find($id);

        $input = $request->all();

        if($file = $request->file('file')) {
            $name = time().$file->getClientOriginalName();

            $file->move('parent_category_images', $name);

            $photo = ParentCategoryImage::create(['file' => $name]);

            $old_image = $category->image;

            if (null != $old_image) {
                if( File::exists(app_path().'/../public'.$old_image->file) ) {
                    File::delete(app_path().'/../public'.$old_image->file);
                }
                
                $old_image->delete();    
            }

            $input['parent_category_image_id'] = (int) $photo->id;
            
            unset($input['file']);

        } else {
            unset($input['file']);
            $input['parent_category_image_id'] = $category->parent_category_image_id;
        }

        $status = $category->update($input);

        if($status) {
            $request->session()->flash('alert-success', 'Category was successfully added!');
        } else {
            $request->session()->flash('alert-danger', 'Oops, something is wrong during adding category! '.$status);

            return back();
        }

        return redirect('admin/parent-category');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        //
        $category = Category::find($id);
        
        if($category->child->count() > 0) {
            foreach($category->child as $child) {
                $child->delete();
            }
        }
        $status = $category->delete();

        if($status) {
            $request->session()->flash('alert-success', 'Category was successfully deleted!');
        } else {
            $request->session()->flash('alert-danger', 'Oops, something is wrong during deleting category!');

            return back();
        }

        return redirect('admin/parent-category');
    }
}
