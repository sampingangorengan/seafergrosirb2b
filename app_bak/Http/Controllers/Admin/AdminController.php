<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    protected $active_menu = null;
    protected $active_submenu = null;

    public function getActiveMenu($active_menu) {
        $this->active_menu = $active_menu;

        return $this->active_menu;
    }

    public function getActiveSubmenu($active_menu) {
        $this->active_menu = $active_menu;

        return $this->active_menu;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));
        if (!auth()->check())
            return redirect('/admin/login');

        if (auth()->user()->role != 1)
            return redirect('/admin/login');

        return view('admin.index', compact('active_menu', 'active_submenu'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if (!auth()->check())
            return redirect('/admin/login');

        if (auth()->user()->role != 1)
            return redirect('/admin/login');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if (!auth()->check())
            return redirect('/admin/login');

        if (auth()->user()->role != 1)
            return redirect('/admin/login');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        if (!auth()->check())
            return redirect('/admin/login');

        if (auth()->user()->role != 1)
            return redirect('/admin/login');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        if (!auth()->check())
            return redirect('/admin/login');

        if (auth()->user()->role != 1)
            return redirect('/admin/login');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function login(Request $request) {
        $credentials = ['email' => $request->email, 'password' => $request->password];


        $remember = request('remember') ? true : false;

        // Login fails
        if ( ! auth()->attempt($credentials, $remember)) {

            if(URL::previous() != url('admin/login')) {
                return back();
            } else {
                return redirect('/');
            }
        }

        if(auth()->user()->role != 1) {
            auth()->logout();
            return redirect('admin/login');
        }
        if(URL::previous() == url('admin/login'))
            return redirect('admin');


        return redirect('/');
    }
}


