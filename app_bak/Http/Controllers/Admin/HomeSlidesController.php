<?php

namespace App\Http\Controllers\Admin;

use App\Models\Home\Slide;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class HomeSlidesController extends Controller
{
    protected $active_menu = null;
    protected $active_submenu = null;

    public function getActiveMenu($active_menu) {
        $this->active_menu = $active_menu;

        return $this->active_menu;
    }

    public function getActiveSubmenu($active_menu) {
        $this->active_menu = $active_menu;

        return $this->active_menu;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));
        $headlines = Slide::get();
        return view('admin.pages.index_headline', compact('headlines', 'active_menu', 'active_submenu'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));
        return view('admin.pages.create_headline', compact('active_menu', 'active_submenu'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'file'          => 'required|mimes:jpeg,bmp,png',
            'link'          => 'required',
            'order'          => 'required|numeric',
            'is_active'     => 'required',
        ]);

        $status = false;

        if($file = $request->file('file')) {
            $name = time().$file->getClientOriginalName();

            $file->move('headlines', $name);

            $status = Slide::create([
                'file_name' => $name,
                'link' => $request->link,
                'caption' => $request->caption,
                'order' => $request->order,
                'is_active' => $request->is_active
            ]);
        }

        if($status) {
            $request->session()->flash('alert-success', 'Headline was successfully added!');
        } else {
            $request->session()->flash('alert-danger', 'Oops something\'s wrong while creating headline.');
        }

        return redirect('admin/headlines');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        //
        //$brand = Brand::findOrFail($id);
        //return view('admin.products.show_brand');
        return redirect('admin/headlines', compact('active_menu', 'active_submenu'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        //
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));
        $headline = Slide::find($id);
        return view('admin.pages.edit_headline',compact('headline', 'active_menu', 'active_submenu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
            'link'          => 'required',
            'order'          => 'required|numeric',
            'is_active'     => 'required',
        ]);

        $headline = Slide::findOrFail($id);

        $input = $request->all();

        $status = $headline->update($input);

        if($status) {
            $request->session()->flash('alert-success', 'Headline was successfully updated!');
        } else {
            $request->session()->flash('alert-danger', 'Oops, something is wrong during updating headline!');

            return back();
        }


        return redirect('admin/headlines');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        //
        $status = Slide::find($id)->delete();

        if($status) {
            $request->session()->flash('alert-success', 'Headline was successfully deleted!');
        } else {
            $request->session()->flash('alert-danger', 'Oops, something is wrong during deleting headline!');

            return back();
        }

        return redirect('admin/headlines');
    }
}
