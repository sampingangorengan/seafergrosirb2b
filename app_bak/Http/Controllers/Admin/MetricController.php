<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Groceries\Metric;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class MetricController extends Controller
{
    protected $active_menu = null;
    protected $active_submenu = null;

    public function getActiveMenu($active_menu) {
        $this->active_menu = $active_menu;

        return $this->active_menu;
    }

    public function getActiveSubmenu($active_menu) {
        $this->active_menu = $active_menu;

        return $this->active_menu;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));
        $metrics = Metric::get();
        return view('admin/products/index_metric', compact('metrics', 'active_menu', 'active_submenu'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));
        return view('admin/products/create_metric', compact('active_menu', 'active_submenu'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'name' => 'required',
            'abbreviation' => 'required',
            'status'    => 'required'
        ]);

        $metric = new Metric;
        $metric->name = $request->name;
        $metric->abbreviation = $request->abbreviation;
        $metric->is_active = $request->status;

        $status = $metric->save();

        if($status) {
            $request->session()->flash('alert-success', 'Metric was successfully added!');
        } else {
            $request->session()->flash('alert-danger', 'Oops something\'s wrong while creating metric.');
        }

        return redirect('admin/metrics');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        //
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));
        $metric = Metric::find($id);
        return view('admin.products.edit_metric', compact('metric', 'active_menu', 'active_submenu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
            'name' => 'required',
            'abbreviation' => 'required',
            'is_active'    => 'required'
        ]);

        $metric = Metric::findOrFail($id);

        $input = $request->all();

        $status = $metric->update($input);

        if($status) {
            $request->session()->flash('alert-success', 'Metric was successfully updated!');
        } else {
            $request->session()->flash('alert-danger', 'Oops something\'s wrong while updating metric.');
        }

        return redirect('admin/metrics');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        //
        $status = Metric::findOrFail($id)->delete();

        if($status) {
            $request->session()->flash('alert-success', 'Brand was successfully deleted!');
        } else {
            $request->session()->flash('alert-danger', 'Oops, something is wrong during deleting brand!');

            return back();
        }

        return redirect('admin/metrics');
    }
}
