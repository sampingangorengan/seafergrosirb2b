<?php

namespace App\Http\Controllers\Admin;

use App\Models\Order;
use App\Models\Order\Payment;
use App\Models\GojekDetail;
use App\Models\Groceries;
use App\Models\Notification;
use App\Models\User\Address;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Money;

class OrderController extends Controller
{
    protected $active_menu = null;
    protected $active_submenu = null;

    private function updateGojekDetail($id, $detail) {

        $gojek_status = $this->getGojekStatus($detail->orderNo);

        $status = DB::table('gojek_details')
            ->where('id', $id)
            ->update([
                'gojek_id' => $gojek_status->id,
                'gojek_order' => $gojek_status->orderNo,
                'status' => $gojek_status->status,
                'driver_id' => $gojek_status->driverId,
                'driver_name' => $gojek_status->driverName,
                'driver_phone' => $gojek_status->driverPhone,
                'driver_photo' => $gojek_status->driverPhoto,
                'total_price' => $gojek_status->totalPrice,
                #'receiver_name' => $gojek_status->receiver_name,
                'order_created_time' => $gojek_status->orderCreatedTime,
                'order_dispatch_time' => $gojek_status->orderDispatchTime,
                'order_arrival_time' => $gojek_status->orderArrivalTime,
                'order_closed_time' => $gojek_status->orderClosedTime,
                'seller_address_name' => $gojek_status->sellerAddressName,
                'seller_address_detail' => $gojek_status->sellerAddressDetail,
                'buyer_address_name' => $gojek_status->buyerAddressName,
                'buyer_address_detail' => $gojek_status->buyerAddressDetail,
                'cancel_description' => $gojek_status->cancelDescription,
                'booking_type' => $gojek_status->bookingType,
                #'store_order_id' => $gojek_status->order_id
            ]);

        $delivery = DB::table('deliveries')->where('detail_id', $id)->update(['booking_unique' => $detail->id, 'status' => 'on delivery','updated_at' => date('Y-m-d H:i:s')] );

        return $status;
    }

    public function getActiveMenu($active_menu) {
        $this->active_menu = $active_menu;

        return $this->active_menu;
    }

    public function getActiveSubmenu($active_menu) {
        $this->active_menu = $active_menu;

        return $this->active_menu;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));
        if(null !=  $request->get('status')){
            if($request->get('status') == 'orders') {
                $orders = Order::where('order_status', 1)->orWhere('order_status', 6)->orderBy('order_status', 'asc')->get();
            } elseif($request->get('status') == 'confirmed') {
                $orders = Order::where('order_status', 7)->orWhere('order_status',2)->orWhere('order_status',3)->orderBy('order_status', 'desc')->get();
            } elseif($request->get('status') == 'rejected')  {
                $orders = Order::where('order_status', 5)->orderBy('order_status', 'asc')->get();
            } elseif($request->get('status') == 'completed') {
                $orders = Order::where('order_status', 4)->orderBy('order_status', 'asc')->get();
            } else {
                $orders = Order::where('order_status', $request->get('status'))->get();  
            }

        }else{
            $orders = Order::get();
        }

        return view('admin.orders.index', compact('orders', 'active_menu', 'active_submenu'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));
        return view('admin.orders.create', compact('active_menu', 'active_submenu'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        //
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));
        $order = Order::find($id);
        $total_user_payment = Payment::where('order_id', $id)->where('user_confirmed', 1)->count();

        return view('admin.orders.show', compact('order', 'active_menu', 'active_submenu', 'total_user_payment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        //
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));
        $order = Order::find($id);

        return view('admin.orders.edit', compact('order', 'active_menu', 'active_submenu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function doGojek(Request $request, $id)
    {
        $order = Order::find($id);

        # check delivery
        if($order->deliveries->status == 'cancelled' && $order->deliveries->delivery_tye == 'gojek') {
            $old_gojek_order = GojekDetail::find($order->deliveries->detail_id);
            $old_gojek_order->delete();
            $delivery_detail_id = DB::table('gojek_details')->insertGetId(
                [
                    'status' => 'not made',
                    'booking_type' => $type,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ]
            );
            $order->deliveries->detail_id = $delivery_detail_id;
            $order->deliveries->save();
        }


        $originLatLong = '-6.102789, 106.803789';
        $defaultAddress = $order->getShippingAddress($order->user_address_id);

        if ($defaultAddress) {

            if(null == $defaultAddress->coordinates) {
                $client = new Client();
            
                $address = urlencode($defaultAddress->address)
                    .'+'.urlencode($defaultAddress->area->name)
                    . '+' . urlencode($defaultAddress->area->city->city_name)
                    . '+' . urlencode($defaultAddress->area->city->province->province_name_id)
                    .'+'.urlencode($defaultAddress->postal_code);

                $address = str_replace(' ', '+', $address);
                
                $url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' . $address . '&key=AIzaSyCRDhe-WvUmE8Y96Gb6Glj-CPc9lfjC420';
                
                $res = $client->request('GET', $url);
                $results = json_decode($res->getBody())->results;
                $destLatLong = $results[0]->geometry->location->lat . ',' . $results[0]->geometry->location->lng;    
            } else {
                $destLatLong = $defaultAddress->coordinates;
            }
            

            $items = $order->groceries;
            $list_item = '';
            foreach ($items as $item) {
                $list_item .= $item->groceries->name.',';
            }

            //$order->getShippingAddress($order->user_address_id)->user->name;

            $recipient_name = null;
            if($defaultAddress->recipient !== '' && $defaultAddress->recipient !== null) {
                $recipient_name = $defaultAddress->recipient;
            } else {
                $recipient_name = $defaultAddress->user->name;
            }

            $recipient_phone = null;
            if($defaultAddress->phone_number !== '' && $defaultAddress->phone_number !== null) {
                $recipient_phone = $defaultAddress->phone_number;
            } else {
                $recipient_phone = $defaultAddress->user->phone;
            }

            unset($client);

            $client = new Client([
                // Base URI is used with relative requests
                'base_uri' => 'https://kilat-api.gojekapi.com',
                // You can set any number of default request options.
                'timeout'  => 60.0,
            ]);

            $headers = ['Client-ID' => env('GOJEK_CLIENT'), 'Pass-Key' => ENV('GOJEK_PASSKEY'), 'Content-Type' => 'application/json'];
            $body = '{'.
                    '"paymentType": 3,'.
                    '"collection_location": "pickup",'.
                    '"shipment_method": "SameDay",'.
                    '"routes": ['.
                    '{'.
                        '"originName": "Seafermart Warehouse",'.
                        '"originNote": "",'.
                        '"originContactName": "Seafermart",'.
                        '"originContactPhone": "02166696285",'.
                        '"originLatLong": "'.$originLatLong.'",'.
                        '"originAddress": "Jalan Cumi Raya No. 3, Muara Baru, RT.20/RW.17, Penjaringan, Jakarta Utara, Kota Jkt Utara, Daerah Khusus Ibukota Jakarta 14440",'.
                        '"destinationName": "'.$defaultAddress->area->name.'",'.
                        '"destinationNote": "string",'.
                        '"destinationContactName": "'.$recipient_name.'",'.
                        //'"destinationContactPhone": "'.$recipient_phone.'",'.
                        '"destinationContactPhone": "081081081081",'.
                        '"destinationLatLong": "'.$destLatLong.'",'.
                        '"destinationAddress": "'.$defaultAddress->address.'",'.
                        '"item": "'.$list_item.'",'.
                        '"storeOrderId": "'.$order->id.'"'.
                    '}'.
                ']'.
            '}';

            try{
                $res = $client->request('POST', '/gojek/booking/v3/makeBooking', compact('headers', 'body'));
            } catch (\Exception $e) {
                if($e->getResponse()->getReasonPhrase() == "Conflict") {
                    $request->session()->flash('alert-danger', 'Oops couldn\'t order Gojek for this order. You can only made request 2 minutes after the first request.');
                } else {
                    $res = $e->getResponse()->getHeader('Error-Message');
                    $message = $res[0];
                    $request->session()->flash('alert-danger', 'Oops couldn\'t order Gojek for this order. '.$message);
                }

                return back();
            }

            $booking = json_decode($res->getBody()->getContents());
            /*var_dump($booking);
            var_dump($booking->orderNo);
            dd($booking);*/


            $gojek = $order->deliveries->detail_id;
            $save_gojek = $this->updateGojekDetail($gojek, $booking);

            if($save_gojek) {

                $request->session()->flash('alert-success', 'Gojek was successfully ordered! The driver will contact you as soon as the system found you one.');

            } else {
                $request->session()->flash('alert-danger', 'Oops something\'s wrong during ordering gojek.');
            }

            return redirect('admin/orders/'.$order->id);

        }
    }

    public function getGojekStatus($order_number) {
        $client = new Client([
            // Base URI is used with relative requests
            'base_uri' => 'https://kilat-api.gojekapi.com',
            // You can set any number of default request options.
            'timeout'  => 60.0,
        ]);

        $headers = ['Client-ID' => env('GOJEK_CLIENT'), 'Pass-Key' => ENV('GOJEK_PASSKEY'), 'Content-Type' => 'application/json'];
        $res = $client->request('GET', '/gojek/v2/booking/orderno/'.$order_number, compact('headers'));

        $response = json_decode($res->getBody()->getContents());

        return $response;
    }

    public function insertAwb(Request $request, $id){
        $order = Order::find($id);
        $delivery_id = $order->deliveries->id;

        $delivery = DB::table('deliveries')->where('id', $delivery_id)->update(['booking_unique' => $request->awb_receipt, 'status' => 'on delivery','updated_at' => date('Y-m-d H:i:s')] );

        if($delivery) {
            $request->session()->flash('alert-success', 'The order is now on delivery');
        } else {
            $request->session()->flash('alert-danger', 'Oops something\'s wrong during inserting awb.');
        }

        return redirect('admin/orders/'.$id);
    }

    public function dispatchInternalCourier(Request $request, $id) 
    {
        $order = Order::find($id);
        $delivery_id = $order->deliveries->id;

        $delivery = DB::table('deliveries')->where('id', $delivery_id)->update(['booking_unique' => 'Internal Delivery', 'status' => 'on delivery','updated_at' => date('Y-m-d H:i:s')] );

        if($delivery) {
            $request->session()->flash('alert-success', 'The order is now on delivery');
        } else {
            $request->session()->flash('alert-danger', 'Oops something\'s wrong during dispatching courier.');
        }

        return redirect('admin/orders/'.$id);
    }

    public function finishCourier(Request $request, $id) {
        $order = Order::find($id);
        $delivery_id = $order->deliveries->id;

        $delivery = DB::table('deliveries')->where('id', $delivery_id)->update(['booking_unique' => 'Internal Delivery', 'status' => 'delivered','updated_at' => date('Y-m-d H:i:s')] );

        $order->order_status = 4;
        $order->save();

        $notif = new Notification;
        $notif->user_id = $order->user_id;
        $notif->title = 'Order Delivery Update';
        $notif->description = 'Your order has been delivered';
        $notif->link = url('orders/'.$order->id);
        $notif->is_read = 0;
        $notif->save();

        if($delivery) {
            $request->session()->flash('alert-success', 'The order is now completed');
        } else {
            $request->session()->flash('alert-danger', 'Oops something\'s wrong during completing this order.');
        }

        return redirect('admin/orders/'.$id);
    }

    public function voidOrder(Request $request, $id) {
        $order = Order::find($id);

        foreach ($order->groceries as $gc) {
            $g = Groceries::find($gc->groceries_id);
            $g->stock = $g->stock + $gc->qty;
            $g->sold = $g->sold - $gc->qty;
            $g->save();
        }

        if (null!==$order) {
            $status = DB::table('orders')->where('id', $id)->update(['order_status' => 5] );

            if($status){
                $request->session()->flash('alert-success', 'The order is canceled');
            } else {
                $request->session()->flash('alert-danger', 'Oops something\'s wrong during canceling this order.');
            }
        }

        return redirect('admin/orders/'.$id);
    }

    public function indexShipping(Request $request, $id) {
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));

        $order = Order::find($id);
        $shipping = $order->deliveries;

        return view('admin.orders.show_shipping', compact('order', 'active_menu', 'active_submenu', 'shipping'));

    }
}
