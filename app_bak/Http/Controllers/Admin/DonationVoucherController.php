<?php

namespace App\Http\Controllers\Admin;

use App\Models\DonationVoucher;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class DonationVoucherController extends Controller
{

    protected $active_menu = null;
    protected $active_submenu = null;

    public function getActiveMenu($active_menu) {
        $this->active_menu = $active_menu;

        return $this->active_menu;
    }

    public function getActiveSubmenu($active_menu) {
        $this->active_menu = $active_menu;

        return $this->active_menu;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));
        $vouchers = DonationVoucher::get();
        return view('admin.promos.index_donation', compact('vouchers','active_menu', 'active_submenu'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));
        return view('admin.promos.create_donation', compact('active_menu', 'active_submenu'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'title'          => 'required',
            'code'           => 'required|unique:donation_vouchers',
            'start_date'     => 'required',
            'end_date'      => 'required',
            'quota'         => 'required|numeric',
            'value_type'    => 'required',
            'value'=> 'required'
        ]);

        $st_date = explode('/',$request->start_date);
        $en_date = explode('/',$request->end_date);

        $voucher = new DonationVoucher;
        $voucher->title = $request->title;
        $voucher->code = $request->code;
        $voucher->description = $request->description;
        $voucher->value_type = $request->value_type;
        $voucher->value = $request->value;
        $voucher->quota = $request->quota;
        $voucher->start_date = $st_date[2].'-'.$st_date[0].'-'.$st_date[1];
        $voucher->end_date = $en_date[2].'-'.$en_date[0].'-'.$en_date[1];


        $status = $voucher->save();

        if($status) {
            $request->session()->flash('alert-success', 'Charity voucher was successfully added!');
        } else {
            $request->session()->flash('alert-danger', 'Oops something\'s wrong while creating voucher.');
        }

        return redirect('admin/charity-voucher');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        //
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));
        $voucher = DonationVoucher::find($id);
        return view('admin.promos.show_donation', compact('voucher', 'active_menu', 'active_submenu'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        //
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));
        $voucher = DonationVoucher::find($id);
        return view('admin.promos.edit_donation', compact('voucher', 'active_menu', 'active_submenu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
            'title'          => 'required',
            'code'           => 'required|unique:donation_vouchers,code,'.$id,
            'start_date'     => 'required',
            'end_date'      => 'required',
            'quota'         => 'required|numeric',
            'value_type'    => 'required',
            'value'         => 'required',
            'is_active' => 'required'
        ]);

        $st_date = explode('/',$request->start_date);
        $en_date = explode('/',$request->end_date);

        $voucher = DonationVoucher::find($id);

        $input = $request->all();

        if(count($st_date) == 1) {
            $std = explode(' ', $request->start_date);
            $input['start_date'] = $std[0];
        } else {
            $input['start_date'] = $st_date[2].'-'.$st_date[0].'-'.$st_date[1];    
        }

        if(count($en_date) == 1) {
            $end = explode(' ', $request->end_date);
            $input['end_date'] = $end[0];
        } else {
            $input['end_date'] = $en_date[2].'-'.$en_date[0].'-'.$en_date[1];
        }

        $status = $voucher->update($input);

        if($status) {
            $request->session()->flash('alert-success', 'Charity voucher was successfully updated!');
        } else {
            $request->session()->flash('alert-danger', 'Oops, something is wrong during updating voucher!');

            return back();
        }


        return redirect('admin/charity-voucher');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        //
        $status = DonationVoucher::find($id)->delete();

        if($status) {
            $request->session()->flash('alert-success', 'Charity voucher was successfully deleted!');
        } else {
            $request->session()->flash('alert-danger', 'Oops, something is wrong during deleting voucher!');

            return back();
        }

        return redirect('admin/charity-voucher');
    }
}
