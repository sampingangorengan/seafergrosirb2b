<?php

namespace App\Http\Controllers\Admin;

use App\Models\Discount;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class DiscountController extends Controller
{
    protected $active_menu = null;
    protected $active_submenu = null;

    public function getActiveMenu($active_menu) {
        $this->active_menu = $active_menu;

        return $this->active_menu;
    }

    public function getActiveSubmenu($active_menu) {
        $this->active_menu = $active_menu;

        return $this->active_menu;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));
        $discounts = Discount::get();
        return view('admin.promos.index_discount', compact('discounts','active_menu', 'active_submenu'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));
        return view('admin.promos.create_discount', compact('active_menu', 'active_submenu'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'title'          => 'required',
            'start_date'     => 'required',
            'end_date'      => 'required',
            'discount_type'    => 'required',
            'discount_value'=> 'required'
        ]);

        $st_date = explode('/',$request->start_date);
        $en_date = explode('/',$request->end_date);

        $discount = new Discount;
        $discount->title = $request->title;
        $discount->description = $request->description;
        $discount->discount_type = $request->discount_type;
        $discount->discount_value = $request->discount_value;
        $discount->start_date = $st_date[2].'-'.$st_date[0].'-'.$st_date[1];
        $discount->end_date = $en_date[2].'-'.$en_date[0].'-'.$en_date[1];


        $status = $discount->save();

        if($status) {
            $request->session()->flash('alert-success', 'Discount was successfully added!');
        } else {
            $request->session()->flash('alert-danger', 'Oops something\'s wrong while creating discount.');
        }

        return redirect('admin/discounts');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        //
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));
        $discount = Discount::findOrFail($id);
        return view('admin.promos.show_discount', compact('discount','active_menu', 'active_submenu'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        //
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));
        $discount = Discount::findOrFail($id);
        return view('admin.promos.edit_discount', compact('discount', 'active_menu', 'active_submenu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
            'title'          => 'required',
            'start_date'     => 'required',
            'end_date'      => 'required',
            'discount_type'    => 'required',
            'discount_value'=> 'required'
        ]);

        $st_date = explode('/',$request->start_date);
        $en_date = explode('/',$request->end_date);

        $discount = Discount::find($id);

        $input = $request->all();

        $input['start_date'] = $st_date[2].'-'.$st_date[0].'-'.$st_date[1];
        $input['end_date'] = $en_date[2].'-'.$en_date[0].'-'.$en_date[1];

        $status = $discount->update($input);

        if($status) {
            $request->session()->flash('alert-success', 'Discount was successfully updated!');
        } else {
            $request->session()->flash('alert-danger', 'Oops, something is wrong during updating discount!');

            return back();
        }


        return redirect('admin/discounts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        //
        $status = Discount::find($id)->delete();

        if($status) {
            $request->session()->flash('alert-success', 'Discount was successfully deleted!');
        } else {
            $request->session()->flash('alert-danger', 'Oops, something is wrong during deleting discount!');

            return back();
        }

        return redirect('admin/discounts');
    }
}
