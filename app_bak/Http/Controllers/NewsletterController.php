<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Newsletter;
use App\Models\Newsletter as MyNews;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class NewsletterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $this->validate($request, [
            'email' => 'required|email',
        ]);

        $check_email = MyNews::where('email', $request->email)->first();

        if(null == $check_email) {
            $newsletter = new MyNews;
            $newsletter->email = $request->email;
            $status = $newsletter->save();

            if($status) {
                $request->session()->flash('alert-success', 'You are succesfully subscribed to our newsletter. We\'ll be sending you information regarding our products');
            } else {
                $request->session()->flash('alert-danger', 'Failed subscribing newsletter. Contact our customer service for further assistance.');
            }
        } else {
            $request->session()->flash('alert-danger', 'You have been subsscribed to our newsletter.');
        }

        #Newsletter::subscribe($request->email);

        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
