<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Groceries;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Groceries\Category;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Agent;

class HomeController extends Controller
{
    private function generateCategory() {
        $category = new Category;

        $gc = collect();
        $ct = collect();

        foreach($category->get() as $c) {
            if($c->child->count() > 0) {
                //$c->grocery_list = collect();

                foreach($c->child as $ch) {
                    if($ch->groceries->count() > 0) {
                        #dd($ch->groceries->take(4)->count());
                        if($ch->groceries->take(5)->count() > 0) {
                            foreach($ch->groceries->take(5) as $item) {
                                $gc->push($item);
                                if($gc->count() == 5) {
                                    break 2;
                                }
                            }
                        }
                        
                    }
                }
            }
            $c->grocery_list = $gc;

            $gc = collect();

            $ct->push($c);
        }

        unset($category);
        unset($gc);

        return $ct;
    }

    public function getIndex()
    {
        $groceries = new Groceries;
        
        

        $groceries_body = $groceries->where('stock','>', 0)->orderBy('created_at')->skip(0)->take(8)->get();

        $new_groceries_body = $this->generateCategory();
        $counter = 0;


        return view('responsive.home_revamp', compact('groceries_body', 'new_groceries_body', 'counter'));
        #return view('home.home', compact('groceries_body', 'new_groceries_body'));
        /*if(Agent::isDesktop()) {
            if(Agent::isAndroidOS()) {
                return view('responsive.home', compact('groceries_body'));
            }
            return view('home.home', compact('groceries_body', 'new_groceries_body'));
        } else {
            return view('responsive.home', compact('groceries_body'));
        }*/
        
    }
}
