<?php

namespace App\Http\Controllers;

use App\Models\BestPriceClaim;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\ClaimScreenshot;

class ClaimController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name'        => 'required',
            'last_name'         => 'required',
            'email'             => 'required|email',
            'item_name'         => 'required',
            'order_id'          => 'required',
            'seafer_price'      => 'required|numeric',
            'seafer_link'       => 'required',
            'other_price'       => 'required|numeric',
            'other_link'        => 'required',
            'store_location'    => 'required',
            'file'          => 'mimes:jpeg,bmp,png',
        ]);

        $claim = new BestPriceClaim;
        $claim->first_name = $request->first_name;
        $claim->last_name = $request->last_name;
        $claim->email = $request->email;
        $claim->item_name= $request->item_name;
        $claim->order_id = $request->order_id;
        $claim->seafer_price = $request->seafer_price;
        $claim->seafer_link = $request->seafer_link;
        $claim->other_price = $request->other_price;
        $claim->other_link = $request->other_link;
        $claim->store_location = $request->store_location;

        if($file = $request->file('file')) {
            $name = time().$file->getClientOriginalName();

            $file->move('claim_screenshot', $name);

            $photo = ClaimScreenshot::create(['file' => $name]);

            $claim->claim_screenshot = (int) $photo->id;
        }

        $claim->save();

        $request->session()->flash('alert-success', 'Claim was successful submitted!');

        return redirect('best-price-guarantee');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
