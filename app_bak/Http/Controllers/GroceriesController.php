<?php

namespace App\Http\Controllers;

use App\Models\DonationVoucher;
use App\Models\Groceries\Rating;
use App\Models\SearchableGroceries;
use App\Models\Location\Province;
use App\Models\User;
use App\Models\User\ViewHistory;
use DB;
use App\Models\Promo;
use App\Models\PromoHistory;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Groceries;
use App\Models\Groceries\Category;
use App\Models\Groceries\ChildCategory;
use App\Models\Groceries\Question;
#use Jenssegers\Agent\Agent;
use Agent;
use Carte;

/**
 * URI: /groceries
 *
 * Remember, "groceries" is not the plural form of "grocery".
 */
class GroceriesController extends Controller
{

    private $activeCategoryId = null;

    private function parseReferralCode($code) {

        $clean_code = substr($code,3);
        $de_code = base64_decode($clean_code);
        $split_code = explode('-and-', $de_code);

        unset($clean_code);
        unset($de_code);

        return $split_code;

    }

    public function category(Request $request, $uri)
    {
        $this->activeCategoryId = ChildCategory::where('slug', $uri)->first();
        return $this->index($request);
    }

    public function index(Request $request)
    {
        $groceries = new Groceries;

        $query_params = $request->input();

        $activeCategoryId = $this->activeCategoryId;

        if (!is_null($activeCategoryId) && $activeCategoryId->count() > 0) {
            $groceries = $this->activeCategoryId->groceries();
        }
        $orderBy = '';
        // List order
        if (isset($query_params['orderBy'])) {
            switch ($query_params['orderBy']) {
                case 'highest-price':
                    $orderBy = 'price';
                    $orderType = 'desc';
                    break;
                case 'lowest-price':
                    $orderBy = 'price';
                    $orderType = 'asc';
                    break;
                case 'latest-entry':
                    $orderBy = 'updated_at';
                    $orderType = 'desc';
                    break;
                default:
                    $orderBy = null;
                    break;
            }
            if ($orderBy) {
                $groceries = $groceries->orderBy($orderBy, $orderType);
                $orderBy = $query_params['orderBy'];
            }
        }

        $groceries = $groceries->paginate(12);

        /*foreach($groceries as $grocer){
            echo $grocer->price;
            echo '</br>';
        }

        die();*/

        if(auth()->check()){
            $recent = ViewHistory::where('identifier', auth()->user()->id)->orderBy('created_at', 'desc')->skip(0)->take(6)->get();
        } else {
            $recent = ViewHistory::where('identifier', Session::getId())->orderBy('created_at', 'desc')->skip(0)->take(6)->get();
        }


        $linked_query = $request->input();
        if(!is_null($request->input('page'))){
            unset($linked_query['page']);
        }
        $query_string = '';
        $i = 0;
        foreach($linked_query as $k=>$v) {
            if($i == (count($linked_query) -1) ){
                $query_string .= $k.'='.$v;
            } else {
                $query_string .= $k.'='.$v.'&';
            }
            $i++;
        }

        //$groceries = $groceries->paginate(12);
        $count = count($groceries);

        $page = Input::get('page', 1); // Get the ?page=1 from the url
        $perPage = 15; // Number of items per page
        $offset = ($page * $perPage) - $perPage;

        /*$paginator =  new LengthAwarePaginator(
            array_slice($groceries, $offset, $perPage, true), // Only grab the items we need
            count($groceries), // Total items
            $perPage, // Items per page
            $page, // Current page
            ['path' => URL::full(), 'query' => $url_query] // We need this so we can keep all old query parameters from the url
        );*/

        if(Agent::isDesktop()) {
            if(Agent::isAndroidOS()) {
                return view('responsive.groceries.index_groceries', compact('activeCategoryId', 'count', 'groceries', 'url_query','orderBy', 'recent', 'query_string'));
            }

            return view('groceries.index', compact('activeCategoryId', 'count', 'groceries', 'url_query','orderBy', 'recent', 'query_string'));
        } else {
            return view('responsive.groceries.index_groceries', compact('activeCategoryId', 'count', 'groceries', 'url_query','orderBy', 'recent', 'query_string'));
        }
        
    }

    public function show($id)
    {

        $activeCategoryId = $this->activeCategoryId;
        $item = Groceries::findOrFail($id);

        if(auth()->check()){
            $recent = ViewHistory::where('identifier', auth()->user()->id)->orderBy('created_at', 'desc')->skip(0)->take(6)->get();
        } else {
            $recent = ViewHistory::where('identifier', Session::getId())->orderBy('created_at', 'desc')->skip(0)->take(6)->get();
        }

        $recommendation = Groceries::orderByRaw('RAND()')->skip(0)->take(6)->get();
        $brand_related = Groceries::where('brand_id', $item->brand->id)->orderBy('updated_at', 'desc')->skip(0)->take(6)->get();

        $questions = Question::where('groceries_id', $item->id)->orderBy('created_at', 'desc')->get();
        $reviews = Rating::where('groceries_id', $item->id)->where('is_active', 1)->orderBy('created_at', 'desc')->get();

        // For statistic purpose
        $one_star = Rating::where('groceries_id', $item->id)->ofRate(1)->get()->count();
        $two_star = Rating::where('groceries_id', $item->id)->ofRate(2)->get()->count();
        $three_star = Rating::where('groceries_id', $item->id)->ofRate(3)->get()->count();
        $four_star = Rating::where('groceries_id', $item->id)->ofRate(4)->get()->count();
        $five_star = Rating::where('groceries_id', $item->id)->ofRate(5)->get()->count();

        return view('responsive.groceries.show_grocery_revamp', compact(
                    'activeCategoryId',
                    'item',
                    'recommendation',
                    'brand_related',
                    'recent',
                    'reviews',
                    'questions',
                    'one_star',
                    'two_star',
                    'three_star',
                    'four_star',
                    'five_star'
                ));
        if(Agent::isDesktop()) {
            if(Agent::isAndroidOS()) {
                return view('responsive.groceries.show_grocery', compact(
                    'activeCategoryId',
                    'item',
                    'recommendation',
                    'brand_related',
                    'recent',
                    'reviews',
                    'questions',
                    'one_star',
                    'two_star',
                    'three_star',
                    'four_star',
                    'five_star'
                ));
            }

            return view('groceries.show', compact(
                'activeCategoryId',
                'item',
                'recommendation',
                'brand_related',
                'recent',
                'reviews',
                'questions',
                'one_star',
                'two_star',
                'three_star',
                'four_star',
                'five_star'
            )); 
        } else {
            return view('responsive.groceries.show_grocery', compact(
                'activeCategoryId',
                'item',
                'recommendation',
                'brand_related',
                'recent',
                'reviews',
                'questions',
                'one_star',
                'two_star',
                'three_star',
                'four_star',
                'five_star'
            ));
        }
        
    }

    public function showall()
    {
        $current_url = URL::full();
        $split_current_url = explode('?', $current_url);

        if (array_has($split_current_url, 1)){
            $url_query = $split_current_url[1];
        } else {
            $url_query = '';
        }


        $activeCategoryId = $this->activeCategoryId;

        // List order
        if (request('order')) {
            switch (request('order')) {
                case 'highest-price':
                    $orderBy = 'price';
                    $orderType = 'desc';
                    break;
                case 'lowest-price':
                    $orderBy = 'price';
                    $orderType = 'asc';
                    break;
                case 'latest-entry':
                    $orderBy = 'updated_at';
                    $orderType = 'desc';
                    break;
                default:
                    $orderBy = null;
                    break;
            }
            if ($orderBy) {
                $groceries = DB::table('groceries')->orderBy($orderBy, $orderType)->paginate(12);
            }
        }

        $groceries = DB::table('groceries')->paginate(12);

        $orderBy = request('order');

        $count = count($groceries);

        return view('groceries.index', compact('activeCategoryId', 'count', 'groceries', 'url_query','orderBy'));
    }

    public function parentcategory(Request $request, $parent) {
    
        $parent_category = Category::where('slug', $parent)->first();
        $active_parent_category = $parent_category->name;

        $child_category = $parent_category->child;
        
        $i = 0;
        $stack = array();
        foreach($child_category as $sub_category) {
            $stack[$i] = $sub_category->groceries()->get();
            $i++;
        }
        #dd($stack[1]);

        $base_collection = $stack[0];
        #$base_collection = $base_collection->merge($stack[1]);
        for($i = 1; $i < count($stack);$i++){
            $base_collection = $base_collection->merge($stack[$i]);
        }

        $base_collection = $base_collection->sortBy('name');
        $count = count($base_collection);

        $query_params = $request->input();

        $orderBy = '';
        // List order
        if (isset($query_params['sortBy'])) {
            switch ($query_params['sortBy']) {
                case 'highest-price':
                    $orderBy = 'price';
                    $orderType = 'desc';
                    break;
                case 'lowest-price':
                    $orderBy = 'price';
                    $orderType = 'asc';
                    break;
                case 'latest-entry':
                    $orderBy = 'updated_at';
                    $orderType = 'desc';
                    break;
                default:
                    $orderBy = null;
                    break;
            }
            if ($orderBy) {
                $groceries = $base_collection->sortBy($orderBy, $orderType);
                $orderBy = $query_params['sortBy'];
            }
        }

        $groceries = $base_collection;

        if(auth()->check()){
            $recent = ViewHistory::where('identifier', auth()->user()->id)->orderBy('created_at', 'desc')->skip(0)->take(6)->get();
        } else {
            $recent = ViewHistory::where('identifier', Session::getId())->orderBy('created_at', 'desc')->skip(0)->take(6)->get();
        }


        $linked_query = $request->input();
        if(!is_null($request->input('page'))){
            unset($linked_query['page']);
        }
        $query_string = '';
        $i = 0;
        foreach($linked_query as $k=>$v) {
            if($i == (count($linked_query) -1) ){
                $query_string .= $k.'='.$v;
            } else {
                $query_string .= $k.'='.$v.'&';
            }
            $i++;
        }

        #dd($recent);

        return view('groceries.index_parent_category', compact('active_parent_category','activeCategoryId', 'count', 'groceries', 'url_query','orderBy', 'recent', 'query_string'));

    }

    public function getAjaxSearchAutocomplete(Request $request) {

        $groceries = SearchableGroceries::all();
        $groceries_array = array("data" => array());
        foreach($groceries as $grocery){
            $slug = 'product';
            if(null != $grocery->slug) {
                $slug = $grocery->slug;
            }
            array_push($groceries_array["data"],array("id" => $grocery->groceries_id,"name" => $grocery->name, "slug" => $slug));
        }
        return json_encode($groceries_array);
    }

    public function search(Request $request) {
        $groceries = new Groceries;

        $query_params = $request->input();


        $activeCategoryId = $this->activeCategoryId;

        if (!is_null($activeCategoryId) && $activeCategoryId->count() > 0) {
            $groceries = $this->activeCategoryId->groceries();
        }
        $orderBy = '';
        // List order
        if (isset($query_params['orderBy'])) {
            switch ($query_params['orderBy']) {
                case 'highest-price':
                    $orderBy = 'price';
                    $orderType = 'desc';
                    break;
                case 'lowest-price':
                    $orderBy = 'price';
                    $orderType = 'asc';
                    break;
                case 'latest-entry':
                    $orderBy = 'updated_at';
                    $orderType = 'desc';
                    break;
                default:
                    $orderBy = null;
                    break;
            }
            if ($orderBy) {
                $groceries = $groceries->orderBy($orderBy, $orderType);
                $orderBy = $query_params['orderBy'];
            }
        }

        #DB::enableQueryLog();
        $groceries = $groceries->where('name', 'like', '%' . $query_params['search'] . '%')->orderByRaw('CASE WHEN name like "'.$query_params['search'].'%" THEN 0
               WHEN name like "'.$query_params['search'].'%" THEN 1
               WHEN name like "% '.$query_params['search'].'%" THEN 2
               ELSE 3
          END')->paginate(12);
        #dd(DB::getQueryLog());

        if(auth()->check()){
            $recent = ViewHistory::where('identifier', auth()->user()->id)->orderBy('created_at', 'desc')->skip(0)->take(6)->get();
        } else {
            $recent = ViewHistory::where('identifier', Session::getId())->orderBy('created_at', 'desc')->skip(0)->take(6)->get();
        }


        $linked_query = $request->input();
        if(!is_null($request->input('page'))){
            unset($linked_query['page']);
        }
        $query_string = '';
        $i = 0;
        foreach($linked_query as $k=>$v) {
            if($i == (count($linked_query) -1) ){
                $query_string .= $k.'='.$v;
            } else {
                $query_string .= $k.'='.$v.'&';
            }
            $i++;
        }
        $orderBy = $request->order;
        $count = $groceries->count();

        if(Agent::isDesktop()) {
            if(Agent::isAndroidOS()) {
                return view('responsive.groceries.index_groceries', compact('activeCategoryId', 'count', 'groceries','orderBy','query_string', 'recent'));
            }
            return view('groceries.index', compact('activeCategoryId', 'count', 'groceries','orderBy','query_string', 'recent'));    
        } else {
            return view('responsive.groceries.index_groceries', compact('activeCategoryId', 'count', 'groceries','orderBy','query_string', 'recent'));
        }
        
    }

    public function postQuestion(Request $request, $id)
    {
        $q = new Question;
        $q->groceries_id = $id;
        $q->user_id = auth()->user()->id;
        $q->question = request('question');
        $q->save();

        $request->session()->flash('alert-success', 'Your question has been successfully sent!');
        return $this->postAjaxQuestion($request, $id);
    }

    public function postPromoCode(Request $request) {

        $code = $request->promo;
        $promo = Promo::where('code', $code)->first();

        if($promo) {
            $hist = PromoHistory::where('user_id', auth()->user()->id)->where('promo_id', $promo->id)->first();
            if(null != $hist) {
                return json_encode(array('status'=>'fail', 'message'=>'Ooops, you can only use this promo once!'));
            }

            if ($promo->isRunning($promo->id) != 'RUNNING')
                return json_encode(array('status'=>'fail', 'message'=>'Ooops, promo period has ended!'));

            if($promo->quota > 0) {
                $request->session()->put('is_promo', $request->promo);
                return json_encode(array('status' => 'success', 'item' => $promo->toArray()));
            } else {
                return json_encode(array('status'=>'fail', 'message'=>'Ooops, promo already meet its quota'));
            }
        }

        return json_encode(array('status'=>'fail', 'message'=>'Ooops, promo code not found!'));
    }

    public function postDonation(Request $request) {

        $code = $request->donation;

        $voucher = DonationVoucher::where('code', $code)->first();


        if($voucher) {
            if ($voucher->isRunning($voucher->id) != 'RUNNING')
                return json_encode(array('status'=>'fail', 'message'=>'Ooops, donation period has ended!'));

            if($voucher->quota > 0) {
                $request->session()->put('donation', $request->donation);
                return json_encode(array('status' => 'success', 'item' => $voucher->toArray()));
            } else {
                return json_encode(array('status'=>'fail', 'message'=>'Ooops, donation already meet its goal target'));
            }
        }

        return json_encode(array('status'=>'fail', 'message'=>'Ooops, donation code not found!'));
    }

    public function postReferralCode(Request $request) {

        $code = $request->referral;

        $code_info = $this->parseReferralCode($request->referral);

        $user_refer = User::find($code_info[0]);

        if($user_refer) {
            $request->session()->put('referral', $request->referral);

            return json_encode(array('status' => 'success', 'message'=>'Yay! Referral code added'));
        }

        return json_encode(array('status'=>'fail', 'message'=>'Ooops, invalid referral code!'));
    }

    public function postAjaxQuestion(Request $request, $identifier) {

        $search = $request->search;

        if($search){
            $questions = Question::where('groceries_id', $identifier)->where('question', 'LIKE', '%'.$search.'%')->orderBy('created_at', 'desc')->get();
        } else {
            $groceries = Groceries::find($identifier);
            $questions = $groceries->questions()->orderBy('created_at', 'desc')->get();
        }

        return view('groceries.ask_kirin_tab', compact('questions', 'search'));
    }
}
