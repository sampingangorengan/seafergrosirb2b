<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Groceries\Wholesale;
use App\Models\Groceries;
use Illuminate\Support\Facades\Mail;

class WholesaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groceries = Groceries::lists('name', 'id');
        
        return view('responsive.groceries.wholesale', compact('groceries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $wholesale = new Wholesale;
        $wholesale->company_name = $request->company_name;
        $wholesale->pic_name = $request->pic_name;
        $wholesale->customer_type = $request->customer_type;
        $wholesale->comment = $request->comment;

        $wp = array();
        $product = $request->product;
        $volume = $request->volume;
        foreach ($request->product as $key => $value) {
            $wp[$key] = array($product[$key] => $volume[$key]);
        }

        $wholesale->product = serialize($wp);

        $status = $wholesale->save();

        $message = array(
            'company' =>  $request->company_name,
            'pic' => $request->pic_name,
            'type' => $request->customer_type,
            'msg' => $request->comment
        );

        $products = array();

        for($i=0;$i<count($wp);$i++) {
            foreach($wp[$i] as $key=>$value) {
                $grocery = Groceries::find($key);
                $products[$i] = array(
                    'name' => $grocery->name,
                    'url' => url('groceries/'.$grocery->id.'/'.str_slug($grocery->name)),
                    'volume' => $value
                );
            }
        }

        $message['products'] = $products;

        if($status) {
            try{
                Mail::queue('emails.wholesale', $message, function ($message) {

                    $message->from('hello@seafermart.co.id', '[NO-REPLY] Kirin from SEAFERMART');

                    $message->to($request->pic_email)->subject('Seafermart Wholesale Request');

                });

                Mail::queue('emails.wholesale', $message, function ($message) {

                    $message->from('hello@seafermart.co.id', '[NO-REPLY] Kirin from SEAFERMART');

                    $message->to('hello@seafermart.co.id')->subject('Seafermart Wholesale Request');

                });

                $request->session()->flash('alert-success', 'Congratulation! You\'re wholesale request has been sent. We will get back to you for further action.');
            }catch(Exception $e) {
                $request->session()->flash('alert-danger', 'Failed to send your wholesale request. Contact our customer service for further assistance.');
            }
        } else {
            $request->session()->flash('alert-danger', 'Failed to send your wholesale request. Contact our customer service for further assistance.');
        }

        return redirect('wholesale');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
