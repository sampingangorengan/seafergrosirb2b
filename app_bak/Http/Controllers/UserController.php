<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\CashbackReward;
use App\Models\Location\Area;
use App\Models\City;
use App\Models\Groceries\Rating;
use App\Models\Order;
use App\Models\Province;
use App\Models\User\EmailPreference;
use App\Models\User\UserBank;
use App\Models\User\ViewHistory;
use App\Models\User\ReferralHistory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Auth\Passwords\TokenRepositoryInterface;
use GuzzleHttp\Client;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\User;
use Agent;
use Carte;
use App\Models\User\Address;
use App\Models\User\ProfilePicture;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\URL;

class UserController extends Controller
{

    use ResetsPasswords;

    private function sync_cart($userid) {
        $db_cart = Cart::where('user_id', $userid)->get();
        $session_cart = Carte::contents();

        if(null != $db_cart && ! empty($session_cart)) {

            //add quantity if exist
            foreach($db_cart as $cart) {
                foreach($session_cart as $s_cart){
                    if($cart->groceries_id == $s_cart->id && $cart->sku == $s_cart->sku) {

                        $cart->qty = $cart->qty + $s_cart->quantity;
                        $cart->quantity = $cart->quantity + $s_cart->quantity;
                        $status = $cart->save();

                        Carte::item($s_cart->identifier)->remove();
                    }
                }
            }


            foreach(Carte::contents() as $item){

                $cart = new Cart;
                $cart->user_id = $userid;
                $cart->groceries_id = $item->id;
                $cart->qty = $item->quantity;
                $cart->is_read = 0;
                $cart->name = $item->name;
                $cart->sku = $item->sku;
                $cart->price_per_item = $item->price_per_item;
                if(null == $item->unit_metric) {
                    $cart->unit_metric = 'n/a';
                } else {
                    $cart->unit_metric = $item->unit_metric;
                }
                
                $cart->price = $item->price;
                $cart->image = $item->image;
                $cart->quantity = $item->quantity;
                $status = $cart->save();

                $item->remove();
            }

            $updated_db_cart = Cart::where('user_id', $userid)->get();

            foreach($updated_db_cart as $d_cart){

                $item = Carte::insert([
                    'id'            => $d_cart->groceries_id,
                    'name'          => $d_cart->name,
                    'sku'           => $d_cart->sku,
                    'price_per_item'=> floatval($d_cart->price_per_item),
                    'unit_metric'   => $d_cart->unit_metric,
                    'price'         => floatval($d_cart->price),
                    'image'         => $d_cart->image,
                    'quantity'      => intval($d_cart->qty)
                ]);

            }
        } elseif(null != $db_cart && empty($session_cart)) {
            $updated_db_cart = Cart::where('user_id', $userid)->get();

            foreach($updated_db_cart as $d_cart){

                $item = Carte::insert([
                    'id'            => $d_cart->groceries_id,
                    'name'          => $d_cart->name,
                    'sku'           => $d_cart->sku,
                    'price_per_item'=> floatval($d_cart->price_per_item),
                    'unit_metric'   => $d_cart->unit_metric,
                    'price'         => floatval($d_cart->price),
                    'image'         => $d_cart->image,
                    'quantity'      => intval($d_cart->qty)
                ]);

            }
        }

        return Carte::contents(true);
    }

    private function checkAddressValidity(Address $address) {

        $client = new Client();
            
        $str_address = urlencode($address->address)
            .'+'.urlencode($address->area->name)
            . '+' . urlencode($address->city->city_name)
            . '+' . urlencode($address->city->province->province_name_id)
            .'+'.urlencode($address->postal_code);

        $string_address = str_replace(' ', '+', $str_address);
        
        $url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' . $string_address . '&key=AIzaSyCRDhe-WvUmE8Y96Gb6Glj-CPc9lfjC420';
        
        $res = $client->request('GET', $url);
        
        $results = json_decode($res->getBody())->results;

        if(null !== $results && !empty($results)) {
            return $results[0]->geometry->location->lat.', '.$results[0]->geometry->location->lng;
        }

        return false;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getSignIn(Request $request)
    {
        $redirect = null;
        if(null !== $request->input('r')) {
            $redirect = $request->input('r');
        }

        if(Agent::isDesktop()) {
            return view('users.sign_in');
        } else {
            return view('responsive.signin', compact('redirect'));
        }
        
    }

    public function getSignUp(Request $request)
    {
        if(null !== $request->input('refer_from')) {
            $request->session()->put('refer_from', $request->refer_from);
        }

        if(auth()->check()) {
            $request->session()->flash('alert-danger', 'Oops, you already have an account logged in the house!');
            return redirect('/');
        }

        if(Agent::isDesktop()) {
            if(Agent::isAndroidOS()) {
                return view('responsive.signup');
            }

            return view('users.sign_up');
        } else {
            return view('responsive.signup');
        }
        
    }

    public function getSignOut()
    {
        auth()->logout();
        Carte::destroy();
        Session::flush();
        return redirect('/');
    }

    public function getForgotPassword()
    {
        return view('users.forgot_password');
    }

    public function getMyProfile(){
        if (auth()->check()) {
            $user = User::findOrFail(auth()->user()->id);
            if(Agent::isDesktop()) {
                if(Agent::isAndroidOS()) {
                    return view('responsive.user.my_profile', compact('user'));
                }
                return view('users.my_profile', compact('user'));
            } else {
                return view('responsive.user.my_profile', compact('user'));
            }
            
        }
        return redirect('/');
    }

    public function getMyAccount() {
        if (auth()->check()) {
            if(Agent::isDesktop()) {
                if(Agent::isAndroidOS()) {
                    return view('responsive.user.my_account');
                }
                return view('users.my_account');    
            } else {
                return view('responsive.user.my_account');    
            }
        }
        return redirect('user/sign-up');
    }

    public function getMyAddress() {
        if (auth()->check()) {
            $addresses = Address::where('user_id', auth()->user()->id)->where('deleted', 0)->get();
            /*if($addresses->count() > 0) {
                foreach($addresses as $addr){
                    $got_order = Order::where('user_address_id', $addr->id)->count();
                    if($got_order > 0) {
                        $addr->can_delete = false;
                    } else {
                        $addr->can_delete = true;
                    }
                }
            }*/
            if(Agent::isDesktop()) {
                if(Agent::isAndroidOS()) {
                    return view('responsive.user.my_address_book', compact('addresses'));
                }
                return view('users.my_address', compact('addresses'));
            } else {
                return view('responsive.user.my_address_book', compact('addresses'));
            }
            
        }
        return redirect('user/sign-up');
    }

    public function getAddNewAddress() {
        if (auth()->check()) {
            if(Agent::isDesktop()) {
                if(Agent::isAndroidOS()) {
                    return view('responsive.user.add_new_address', compact('addresses'));
                }
                return view('users.add_new_address');
            } else {
                return view('responsive.user.add_new_address', compact('addresses'));
            }
            
        }
        return redirect('user/sign-up');
    }

    public function getEditAddress(Request $request, $id) {

        if (auth()->check()) {
            $address = Address::find($id);
            $areas = Area::orderBy('name', 'asc')->lists('name', 'id')->all();
            $cities = City::orderBy('city_name', 'asc')->lists('city_name', 'city_id')->all();
            $provinces = Province::orderBy('province_name', 'asc')->lists('province_name', 'province_id')->all();

            if(Agent::isDesktop()) {
                if(Agent::isAndroidOS()) {
                    return view('responsive.user.edit_address', compact('address', 'areas', 'cities', 'provinces'));
                }
                return view('users.edit_address', compact('address', 'areas', 'cities', 'provinces'));
            } else {
                return view('responsive.user.edit_address', compact('address', 'areas', 'cities', 'provinces'));
            }
        }
        return redirect('user/sign-up');
    }

    public function getMyBankAccount() {
        if (auth()->check()) {
            $bank_list = auth()->user()->banks()->get();

            if(Agent::isDesktop()) {
                return view('users.my_bank_account', compact('bank_list'));    
            } else {
                return view('responsive.user.my_bank_account', compact('bank_list'));
            }
            
        }
        return redirect('user/sign-up');
    }

    public function getAddBankAccount() {
        if(Agent::isDesktop()) {
            return view('users.add_bank_account');
        } else {
            return view('responsive.user.add_bank_account');
        }
        
    }

    public function postAddBankAccount(Request $request) {
        $this->validate($request, [
            'account_name'      => 'required',
            'account_number'    => 'required|numeric',
            'bank_name'         => 'required',
            'user_password'     => 'required',
        ]);

        if (\Hash::check($request->user_password, auth()->user()->password)){
            $bank = new UserBank;
            $bank->account_name = $request->account_name;
            $bank->account_number = $request->account_number;
            $bank->name = $request->bank_name;
            $bank->user_id = auth()->user()->id;
            if (null != $request->branch) {
                $bank->branch = $request->branch;
            }
            $status = $bank->save();

            if($status) {
                $request->session()->flash('alert-success', 'Bank info added to your account.');
            } else {
                $request->session()->flash('alert-danger', 'Failed adding bank info to your account. Contact our customer service for further assistance.');
            }

            return redirect('user/my-bank-account');
        } else {
            $request->session()->flash('alert-danger', 'Failed adding bank info to your account. Invalid user password.');
            #return redirect('user/add-bank-account');
            return back();
        }
    }

    public function getSetBankAccountPreference(Request $request, $id) {

        if(! auth()->check()) {
            return redirect('/');
        }

        $bank_account = UserBank::find($id);
        if(null == $bank_account) {
            $request->session()->flash('alert-danger', 'Oops, Terjadi kesalahan pada infomasi akun bank. dimohon untuk menghubungi customer service kami untuk bantuan!');
        } else {
            if(auth()->user()->id != $bank_account->user_id) {
                $request->session()->flash('alert-danger', 'Oops, anda bukan pemilik akun ini!');
            } else {
                $clear_existing = DB::table('user_banks')->where('user_id', $bank_account->user_id)->where('is_preference', 1)->update(['is_preference' => 0]);
                $add_existing = DB::table('user_banks')->where('id', (int) $id)->update(['is_preference'=> 1]);

                if($add_existing) {
                    $request->session()->flash('alert-success', 'Akun bank ini berhasil dijadikan sebagai default');
                } else {
                    $request->session()->flash('alert-danger', 'Oops, Terjadi kesalahan pada infomasi akun bank. dimohon untuk menghubungi customer service kami untuk bantuan!');
                }
            }
        }

        return redirect('user/my-bank-account');
    }

    public function getDeleteBankAccount(Request $request, $id) {
        if(! auth()->check()) {
            return redirect('/');
        }

        $bank_account = UserBank::find($id);
        if(null == $bank_account) {
            $request->session()->flash('alert-danger', 'Oops, Terjadi kesalahan pada infomasi akun bank. dimohon untuk menghubungi customer service kami untuk bantuan!');
        } else {
            if(auth()->user()->id != $bank_account->user_id) {
                $request->session()->flash('alert-danger', 'Oops, anda bukan pemilik akun ini!');
            } else {
                $delete_existing = DB::table('user_banks')->where('id', (int) $id)->delete();

                if($delete_existing) {
                    $request->session()->flash('alert-success', 'Akun bank berhasil dihapus');
                } else {
                    $request->session()->flash('alert-danger', 'Oops, Terjadi kesalahan pada infomasi akun bank. dimohon untuk menghubungi customer service kami untuk bantuan!');
                }
            }
        }

        return redirect('user/my-bank-account');
    }

    public function getMyCashbackRewards(Request $request) {

        if(! auth()->check()){
            return redirect('/');
        }

        $cashback_rewards = auth()->user()->rewards()->orderBy('created_at', 'desc')->get();

        if(Agent::isDesktop()) {
            if(Agent::isAndroidOS()) {
                return view('responsive.user.cashback_balance', compact('cashback_rewards'));
            }
            return view('users.my_points', compact('cashback_rewards'));
        } else {
            return view('responsive.user.cashback_balance', compact('cashback_rewards'));
        }
    }

    public function getMyRatingReview() {
        if (auth()->check()) {
            
            $orders = Order::ofUser(auth()->user())->orderBy('created_at', 'desc')->get();
            $my_review = array();
            $my_reviews_raw = Rating::where('user_id', auth()->user()->id)->get();

            if(null !== $my_reviews_raw) {
                foreach($my_reviews_raw as $rv) {
                    array_push($my_review, $rv->groceries_id);    
                }
            }

            $reviewd = array();

            $grocer_list = array();
            foreach($orders as $order){
                foreach($order->groceries as $grocer) {
                    if(! in_array($grocer->groceries_id, $reviewd)) {
                        if(in_array($grocer->groceries_id, $my_review)) {
                            $grocer->is_reviewed = 1;
                        }
                        array_push($grocer_list,$grocer);
                    }

                    array_push($reviewd, $grocer->groceries_id);
                }
            }

            if(Agent::isDesktop()) {
                if(Agent::isAndroidOS()) {
                    return view('responsive.user.my_review', compact('grocer_list', 'my_review'));
                }
                return view('users.my_rating_review', compact('grocer_list'));    
            } else {
                return view('responsive.user.my_review', compact('grocer_list'));
            }
            
        }
        return redirect('user/sign-up');
    }

    public function getMyEmailPreference() {
        if (auth()->check()) {
            if(Agent::isDesktop()) {
                if(Agent::isAndroidOS()) {
                    return view('responsive.user.my_email');
                }
                return view('users.my_email_preference');    
            } else {
                return view('responsive.user.my_email');
            }
            
        }
        return redirect('user/sign-up');
    }

    public function postSignIn(Request $request)
    {
        $credentials = ['email' => $request->email, 'password' => $request->password];

        $remember = request('remember') ? true : false;

        // Login fails
        if ( ! auth()->attempt($credentials, $remember)) {
            
            if(URL::previous() != url('user/sign-in')) {
                $request->session()->flash('alert-danger', 'Oops, email and password does not match!');
                return back();
            } else {
                return redirect('cart');
            }
        }

        
        $merge_cart = $this->sync_cart(auth()->user()->id);

        if(URL::previous() == url('user/sign-in'))
        {
            /*if(Agent::isDesktop()) {
                return redirect('cart');    
            } else {*/
                if(null == $request->input('r')) {
                    return redirect('/');
                } else {
                    return redirect($request->input('r'));
                }
            #}
        } elseif (URL::previous() == url('admin/login')) {

            return redirect('admin');
        } elseif (URL::previous() == url('cart')) {
            return redirect('checkout');
        } 
        else {

            if(null == $request->input('r')) {
                return redirect('/');    
            } else {
                return redirect($request->input('r'));
            }
        }
    }

    public function postSignUp(Request $request)
    {
        $this->validate($request, [
            'first_name'      => 'required',
            'last_name'       => 'required',
            'email'           => 'required|unique:users|email',
            'dob_date'        => 'required',
            'dob_month'        => 'required',
            'dob_year'        => 'required',
            'signup_password' => 'required|between:8,12|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9]).*$/',
            'phone_number'    => 'numeric',
            'sex'             => 'required'
        ]);

        $fname = explode(' ',request('first_name'));
        $referral_code = strtoupper($fname[0]).uniqid();

        $refer_from = null;
        if(null !== $request->session()->get('refer_from') || ! empty($request->session()->get('refer_from'))) {
            $refer = $request->session()->get('refer_from');
            $refer_from_user = User::where('referral_code', $refer)->first();
            if(null !== $refer_from_user || !empty($refer_from_user)) {
                $refer_from = $refer_from_user->id;
            }
        }

        $confirmation_code = str_random(30);
        $user = new User;
        $user->name = request('first_name').' '.request('last_name');
        $user->email = request('email');
        $user->password = \Hash::make(request('signup_password'));
        $user->sex = request('sex');
        $user->role = 2;
        $user->dob = request('dob_year').'-'.request('dob_month').'-'.request('dob_date');
        $user->phone_number = '+62'.request('phone_number');
        $user->is_active = 0;
        $user->registration_code = $confirmation_code;
        $user->referral_code = $referral_code;
        $user->refer_from = $refer_from;
        $user->save();

        $email_preference = new EmailPreference;
        $email_preference->user_id = $user->id;
        $email_preference->save();


        $request->session()->forget('refer_from');

        try{
            Mail::send('emails.signup', compact('confirmation_code', 'user'), function ($message) use ($user) {

                $message->from('hello@seafermart.co.id', '[NO-REPLY] Kirin from SEAFERMART');

                $message->to($user->email)->subject('Verify your email address');

            });
            $request->session()->flash('alert-success', 'Congratulation! You are one step away from completing registration process. Please check your email and verify your account');
        }catch(Exception $e) {
            $request->session()->flash('alert-danger', 'Oops, email and password does not match!');
        }

        return redirect('/');
    }

    public function getVerifyAccount(Request $request, $confirmation_code) {
        if( ! $confirmation_code)
        {
            $request->session()->flash('alert-danger', 'Oops, confirmation code not found! We are unable to verify your email address');
            redirect('/');
        }

        $user = User::whereRegistrationCode($confirmation_code)->first();

        if ( ! $user)
        {
            $request->session()->flash('alert-danger', 'Oops, confirmation code not found! We are unable to verify your email address');
            redirect('/');
        }
        
        $user->is_active = 1;
        $user->registration_code = null;
        $status = $user->save();

        if($status){
            $request->session()->flash('alert-success', 'Congratulation, your account is now active!');
        } else {
            $request->session()->flash('alert-danger', 'Oops, we are unable to verify your email address');
        }

        auth()->login($user);
        return redirect('/');
    }

    public function postMakeHistory(Request $request){

        if ($request->type == 'uid') {
            $history = ViewHistory::where('identifier', $request->identifier)->where('is_uid', 1)->get();
        } elseif ($request->type == 'ss'){
            $history = ViewHistory::where('identifier', $request->identifier)->where('is_session', 1)->get();
        } else {
            return 'Whoaa!Hang on there!';
        }

        if ($history->count() > 6) {
            $status = ViewHistory::where('identifier', $request->identifier)->orderBy('created_at', 'asc')->first()->delete();
        }
        if($history->count() > 0) {
            foreach( $history as $his) {
                if ($his->groceries_id == $request->gid) {
                    return 'already, mate!';
                }
            }
        }

        if ($request->type == 'uid') {
            $hist = new ViewHistory;
            $hist->identifier = $request->identifier;
            $hist->is_uid = 1;
            $hist->groceries_id = intval($request->gid);
            $hist->save();
        } elseif ($request->type == 'ss'){
            $hist = new ViewHistory;
            $hist->identifier = $request->identifier;
            $hist->is_session = 1;
            $hist->groceries_id = intval($request->gid);
            $hist->save();
        }

        return 'alrighty';
    }

    public function postAddNewAddress(Request $request) {

        if (auth()->check()) {

            $validator = Validator::make($request->all(), [
                'name'  => 'required',
                'first_name' => 'required',
                'address_one'   => 'required',
                'area'  => 'required',
                'city'  => 'required',
                'postal_code'   => 'required',
                'recipient_title' => 'required',
                'phone_number'  => 'required|numeric',
            ]);

            if ($validator->fails()) {
                return redirect('user/add-new-address')
                            ->withErrors($validator)
                            ->withInput();
            }



            $address = new Address;
            $address->user_id = auth()->user()->id;
            $address->name = request('name');
            $address->recipient_title = request('recipient_title');
            $address->recipient = request('first_name').' '.request('last_name');
            $address->address = request('address_one').' '.request('address_two').' '.request('address_three');
            $address->area_id = request('area');
            $address->city_id = request('city');
            $address->postal_code = request('postal_code');
            $address->phone_number = request('phone_number');

            $string_address = $this->checkAddressValidity($address);
            if($string_address) {
                $address->coordinates = $string_address;
            } else {
                $request->session()->flash('alert-danger', 'Oops, We can\'t find the coordinates of your address. It will trouble you in the delivery if you proceed with this address.');
                return redirect('user/add-new-address');
            }

            $address->save();

            #return view('users.add_new_address');
        }
        return redirect('user/my-address');
    }

    public function getAjaxAreasByCity($id) {
        $area = Area::where('city_id', $id)->orderBy('name', 'asc')->lists('name', 'id')->all();

        if(count($area)>0){
            $result = array('status' => 'success', 'areas' => $area);
        } else {
            $result = array('status' => 'fail', 'message' => 'Area not found for this city');
        }
        return json_encode($result);
    }

    public function getAjaxCitiesByProvince($id){
        $city = City::where('province_id', $id)->where('is_active', 1)->orderBy('city_name_full', 'asc')->lists('city_name_full', 'city_id')->all();

        if(count($city)>0){
            $result = array('status' => 'success', 'cities' => $city);
        } else {
            $result = array('status' => 'fail', 'message' => 'City not found for this province');
        }
        return json_encode($result);
    }

    public function patchUpdateAddress(Request $request, $id) {
        if (auth()->check()) {

            $address = Address::find($id);
            $input = $request->all();

            $input['user_id ']= auth()->user()->id;
            unset($input['_token']);
            unset($input['_method']);

            $status = $address->update($input);


            #return view('users.add_new_address');
        }
        return redirect('user/my-address');
    }

    public function deleteAddress(Request $request, $id) {
        if (auth()->check()) {

            try {
                /*$status = Address::find($id)->delete();*/
                $status = DB::table('user_addresses')->where('id', $id)->update(['deleted' => 1, 'updated_at' => date('Y-m-d H:i:s')]);
                $request->session()->flash('alert-success', 'Address succesfully deleted');
            } catch (\Exception $e) {
                $request->session()->flash('alert-danger', 'Oops, you can\'t delete this address as it being used for one of your order!');
            }

        }
        return redirect('user/my-address');
    }

    public function postEditGeneralData(Request $request, $id) {
        $this->validate($request, [
            'file'          => 'mimes:jpeg,bmp,png',
        ]);
        $user = User::findOrFail($id);

        $input = $request->all();

        $input['dob'] = $input['dob_year'].'-'.$input['dob_month'].'-'.$input['dob_date'];
        unset($input['dob_year']);
        unset($input['dob_month']);
        unset($input['dob_date']);

        if($file = $request->file('file')) {
            $name = time().$file->getClientOriginalName();

            $file->move('profile_picture', $name);

            $photo = ProfilePicture::create(['file' => $name]);

            $input['profile_picture'] = (int) $photo->id;
        }
        unset($input['file']);
        $status = $user->update($input);

        if($status) {
            $request->session()->flash('alert-success', 'Yay! Your profile is now updated');
        } else {
            $request->session()->flash('alert-danger', 'Oops, something is wrong during updating your profile!');
        }

        return redirect('user/my-profile');
    }

    public function postEditEmailData(Request $request, $id) {
        $user = User::findOrFail($id);

        $input = $request->all();

        $this->validate($request, [
            'old_email'   => 'required|email',
            'email'       => 'required|unique:users|email|confirmed',
        ]);

        $status = $user->update($input);

        if($status) {
            $request->session()->flash('alert-success', 'Yay! Your email is now updated');
        } else {
            $request->session()->flash('alert-danger', 'Oops, something is wrong during updating your email!');
        }

        return redirect('user/my-profile');
    }

    public function postEditPasswordData(Request $request, $id) {
        $user = User::findOrFail($id);

        $input = $request->all();

        Validator::extend('old_password', function ($attribute, $value, $parameters, $validator) {

            return \Hash::check($value, current($parameters));

        });

        $this->validate($request, [
            'old_password' => 'required|old_password:' . auth()->user()->password,
            'password' => 'required|between:8,12|alpha_num|confirmed',
        ]);

        $input['password'] = \Hash::make($input['password']);
        $status = $user->update($input);

        if($status) {
            $request->session()->flash('alert-success', 'Yay! Your password is now updated');
        } else {
            $request->session()->flash('alert-danger', 'Oops, something is wrong during updating your password!');
        }

        return redirect('user/my-profile');
    }

    public function getInitReferral() {
        $users = User::where('referral_code', NULL)->get();

        foreach($users as $user) {
            $fname = explode(' ', $user->name);
            $status = DB::table('users')->where('id', $user->id)->update(['referral_code' => strtoupper($fname[0]).uniqid()]);
        }

        return 'ok';
    }

    public function postShareReferral(Request $request) {
        $referral_code = request('referral_code');
        $email = explode(',',request('email'));

        foreach($email as $em) {
            if (ReferralHistory::where('user_id', auth()->user()->id)->where('email_address', $em)->get()->count() !== 0) {
                return array('status' => 'You have already sent invitation to '.$em.'. Please remove this address and try to send it again.');
            }
        }

        try{
            Mail::send('emails.share_referral', compact('referral_code'), function ($message) use ($email) {
                $message->from('hello@seafermart.co.id', 'Referral Code');
                $message->to($email);
            });

            foreach($email as $em) {
                if (ReferralHistory::where('user_id', auth()->user()->id)->where('email_address', $em)->get()->count() == 0) {
                    $referral = new ReferralHistory;
                    $referral->user_id = auth()->user()->id;
                    $referral->email_address = $em;
                    $referral->save();
                } else {
                    return array('status' => 'You have already sent invitation to this email address');
                } 
            }
            
            
            return array('status' => 'Ok');
        }catch(Exception $e) {
            return array('status' => 'Failed to send email. Please try again in a few moment');
        }
    }

    public function postUpdateEmailPreference(Request $request) {
        if (! auth()->check()) {
            return back();
        }

        $validator = Validator::make($request->all(), [
            'type'  => 'required',
            'new_status' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return array('status' => 'Failed to update the newsletter. Please provide type and try again in a few moment');
        }

        $id = auth()->user()->id;
        $current_preference = EmailPreference::where('user_id', $id)->first();

        if(null == $current_preference) {
            $ep = new EmailPreference;
            $ep->user_id = $id;
            $ep->save();
            $current_preference = EmailPreference::where('user_id', $id)->first();
        }

        if($request->type == 'promo') {
            $current_preference->promo = $request->new_status;
        } elseif($request->type == 'latest_news') {
            $current_preference->latest_news = $request->new_status;
        } elseif($request->type == 'editorial') {
            $current_preference->editorial = $request->new_status;
        } elseif($request->type == 'low_stock') {
            $current_preference->low_stock = $request->new_status;
        } elseif($request->type == 'new_product') {
            $current_preference->new_product = $request->new_status;
        }
        
        $current_preference->save();

        return array('status' => 'OK');

    }
}
