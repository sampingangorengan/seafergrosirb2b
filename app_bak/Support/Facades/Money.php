<?php

namespace App\Support\Facades;

class Money extends \Illuminate\Support\Facades\Facade
{
    protected static function getFacadeAccessor()
    {
        return 'money';
    }
}
