<?php

namespace App\Support\Facades;

class Order extends \Illuminate\Support\Facades\Facade
{
    protected static function getFacadeAccessor()
    {
        return 'order';
    }
}
