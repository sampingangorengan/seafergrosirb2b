<?php

namespace App\Support\Facades;

class Groceries extends \Illuminate\Support\Facades\Facade
{
    protected static function getFacadeAccessor()
    {
        return 'groceries';
    }
}
