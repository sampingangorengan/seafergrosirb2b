<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClaimScreenshot extends Model
{
    //
    protected $table = 'claim_screenshots';

    protected $fillable = ['file'];
}
