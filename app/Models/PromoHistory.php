<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PromoHistory extends Model
{
    //
    protected $table = 'promo_histories';

    protected $fillable = ['user_id', 'order_id', 'promo_id', 'promo_code'];

    public function user(){
        return $this->belongsTo('App\Models\User');
    }

    public function order() {
    	return $this->belongsTo('App\Models\Order');
    }

	public function promo() {
    	return $this->belongsTo('App\Models\Promo');
    }    
}
