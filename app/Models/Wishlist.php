<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Wishlist extends Model
{
    protected $table = 'wishlists';

    protected $fillable = ['user_id', 'groceries_id'];

    public function user() {
    	return $this->belongsTo('App\Models\User');
    }

    public function groceries() {
    	return $this->belongsTo('App\Models\Groceries');
    }
}
