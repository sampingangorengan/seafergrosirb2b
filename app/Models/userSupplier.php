<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class userSupplier extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{

    use Authenticatable, Authorizable, CanResetPassword;
    protected $table = 'user_supplier';
    protected $fillable = ['company_name', 'bussiness_entity', 'billing_address', 'province', 'city', 'area', 'phone_number', 'email', 'website', 'category', 'commoditas', 'selling_method', 'type_of_goods', 'country'];

    public function isSupplier(){
        if($this->userStatus == "supplier"){
            return true;
        }
        return false;
    }


}
/*last*/