<?php

namespace App\Models\StaticPage;

use Illuminate\Database\Eloquent\Model;

class StaticImage extends Model
{
    protected $table = 'static_images';

    protected $fillable = ['page_id', 'image_name', 'image_location', 'updated_at', 'created_at'];
}