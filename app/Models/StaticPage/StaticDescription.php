<?php

namespace App\Models\StaticPage;

use Illuminate\Database\Eloquent\Model;

class StaticDescription extends Model
{
    protected $table = 'static_descriptions';

    protected $fillable = ['page_id', 'description_position', 'description_content', 'updated_at', 'created_at'];
}
