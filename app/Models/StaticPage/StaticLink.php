<?php

namespace App\Models\StaticPage;

use Illuminate\Database\Eloquent\Model;

class StaticLink extends Model
{
    protected $table = 'static_links';

    protected $fillable = ['page_id', 'link_content', 'updated_at', 'created_at'];
}
