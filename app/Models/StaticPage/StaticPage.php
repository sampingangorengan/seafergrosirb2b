<?php

namespace App\Models\StaticPage;

use Illuminate\Database\Eloquent\Model;

class StaticPage extends Model
{
    protected $table = 'static_pages';

    protected $fillable = ['page_name', 'page_link', 'updated_at', 'created_at'];

    public function descriptions() {
    	return $this->hasMany('App\Models\StaticPage\StaticDescription', 'page_id', 'id')->orderBy('description_position', 'asc');
    }

    public function links() {
    	return $this->hasMany('App\Models\StaticPage\StaticLink', 'page_id', 'id');	
    }

    public function images() {
    	return $this->hasMany('App\Models\StaticPage\StaticImage', 'page_id', 'id');	
    }
}
