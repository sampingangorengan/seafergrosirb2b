<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JneDetail extends Model
{
    //
    protected $table = 'jne_details';

    protected $fillable = ['unique_number','date','pod_receiver','receiver_name','city_name','pod_date','pod_status', 'status', 'booking_type'];

    public function delivery() {
        return $this->belongsTo('App\Models\Delivery');
    }
}
