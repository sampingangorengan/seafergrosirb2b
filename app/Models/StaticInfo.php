<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StaticInfo extends Model
{
    protected $table = 'static_infos';

    protected $fillable = ['type', 'content', 'link'];
}
