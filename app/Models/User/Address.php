<?php

namespace App\Models\User;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;


class Address extends Model
{
    protected $table = 'user_addresses';

    protected $fillable = ['user_id', 'name', 'address', 'area_id', 'postal_code', 'phone_number', 'city_id', 'province_id', 'recipient', 'recipient_title'];

    public function area()
    {
        return $this->belongsTo(\App\Models\Location\Area::class);
    }

    public function city() {
        return $this->belongsTo('App\Models\City');
    }

    public function province() {
        return $this->belongsTo('App\Models\Province');
    }

    public static function getLongAddress($id)
    {
        $address = self::find($id);
        return $address->address.', '.$address->area->name.', '.$address->area->city->name
            .', '.$address->area->city->province->name.' '.$address->postal_code;
    }

    public function user(){
        return $this->belongsTo('App\Models\User', 'user_id','id');
    }

    public function userById($user_id) {
        $user = User::withTrashed()->find($user_id);
        return $user;
    }
}
