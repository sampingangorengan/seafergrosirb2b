<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

class CashbackReward extends Model
{
    //
    protected $table = 'cashback_rewards';

    protected $fillable = ['user_id','order_id','obtain_from','nominal'];

    public function user(){
        return $this->belongsTo('App\Models\User');
    }

    public function order(){
    	return $this->belongsTo('App\Models\Order');
    }
}
