<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

class ReferralHistory extends Model
{
    //
    protected $table = 'referral_histories';

    protected $fillable = ['user_id', 'email_address'];
}
