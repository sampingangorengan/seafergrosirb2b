<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

class Tempo extends Model
{
    protected $table = 'user_tempo';


    public static function addUserToTempoReview(){
        $user = auth()->user();

        $this->user_id = $user->id;
        $this->aggrement = "";
        $this->status = "pending";

        return $this->save();
    }

    public function user(){
        return $this->belongsTo('App\Models\User');
    }
}
