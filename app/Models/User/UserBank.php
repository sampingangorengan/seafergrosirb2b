<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

class UserBank extends Model
{
    //
    protected $table = 'user_banks';

    protected $fillable = ['name', 'branch', 'account_name', 'account_number', 'user_id', 'is_preference'];

    public function users() {
        return $this->belongsTo('App\Models\User');
    }
}
