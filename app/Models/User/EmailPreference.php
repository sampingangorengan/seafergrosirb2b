<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

class EmailPreference extends Model
{
    protected $table = 'email_preferences';

    protected $fillable = ['user_id','promo','new_product', 'editorial', 'low_stock', 'latest_news'];

    public function user() {
    	return $this->belongsTo('App\Models\User');
    }
}
