<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword, SoftDeletes;


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['profile_picture','company_name', 'brand_name', 'business_entity', 'billing_address', 'choose_shipping_address', 'province', 'city', 'area', 'postal_code', 'shipping_address', 'shipping_province', 'shipping_city', 'shipping_area', 'shipping_postal_code', 'company_phone', 'company_email', 'website', 'category', 'sex', 'password', 'nama', 'email', 'ktp', 'upload_npwp', 'phone_number'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public function photo() {
        /*return $this->belongsTo('App\Models\User\ProfilePicture');*/
        return $this->hasOne('App\Models\User\ProfilePicture','id' ,'profile_picture');
    }

    public function roles() {
        return $this->belongsTo('App\Models\Role', 'role', 'id');
    }

    public function isAdmin() {

        if ($this->roles->id == 1)
            return true;

        return false;
    }

    public function banks(){
        return $this->hasMany('App\Models\User\UserBank');
    }

    public function points() {
        return $this->hasOne('App\Models\User\CashbackPoint');
    }

    public function rewards() {
        return $this->hasMany('App\Models\User\CashbackReward');
    }

    public function orders() {
        return $this->hasMany('App\Models\Order');
    }

    public function addresses() {
        return $this->hasMany('App\Models\User\Address');
    }

    public function email_preference() {
        return $this->hasOne('App\Models\User\EmailPreference', 'user_id', 'id');
    }

    public function favourites(){
        return $this->belongsToMany('App\Models\Groceries','groceries_favourites','user_id','groceries_id')->withTimestamps()->withPivot('id','usual_qty');;
    }

    public function tempo(){
        return $this->hasOne('App\Models\User\Tempo');
    }

    public function hasTempo(){
        if($this->tempo()->exists()){
            return true;
        }
        return false;

    }

    public function checkTempo(){
        if($this->tempo()->exists()){
            if($this->tempo->status == "pending" || $this->tempo->status == "unsuccessful" || $this->tempo->status == "unapplied") return false;
            return true;
        }
        return false;
    }



}
