<?php

namespace App\Models\Location;

use Illuminate\Database\Eloquent\Model;

/**
 * Area = Kecamatan (Indonesian)
 */
class Area extends Model
{
    protected $table = 'loc_areas';

    public function city()
    {
        return $this->belongsTo('App\Models\City');
    }
}
