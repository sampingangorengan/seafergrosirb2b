<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GojekDetail extends Model
{
    //
    protected $table = 'gojek_details';
    protected $fillable = ['gojek_id','gojek_order','status','driver_id','driver_name','driver_photo','total_price','receiver_name'.'order_created_time','order_dispatch_time','order_arrival_time','seller_address_detail','seller_address_name','buyer_address_detail','buyer_address_name','store_order_id','cancel_description','booking_type'];

    public function delivery() {
        return $this->belongsTo('App\Models\Delivery', 'id', 'detail_id');
    }
}