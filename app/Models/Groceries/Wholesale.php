<?php

namespace App\Models\Groceries;

use Illuminate\Database\Eloquent\Model;

class Wholesale extends Model
{
    //
    protected $table = 'wholesale';

    protected $fillable = ['company_name', 'pic_name', 'product', 'customer_type', 'comment'];
}
