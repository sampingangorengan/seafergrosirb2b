<?php

namespace App\Models\Groceries;

use App\Models\Groceries\ChildCategory;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use Sluggable;

    protected $table = 'groceries_categories';

    protected $fillable = ['name', 'slug', 'order', 'is_active', 'parent_category_image_id'];

    public function child(){
        //return $this->belongsToMany('App\Models\Groceries\ChildCategory', 'id', 'parent_id');
        return $this->hasMany('App\Models\Groceries\ChildCategory', 'parent_id', 'id');
    }

    public function childByOrder($id) {
        $category = self::find($id);
        
        $childs = ChildCategory::orderBy('order', 'asc')->where('parent_id', $id)->get();
        
        return $childs;
    }

    public function image() {
        return $this->hasOne('App\Models\Groceries\ParentCategoryImage', 'id', 'parent_category_image_id');
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => "name",
                'unique' => 'true'
            ]
        ];
    }
}
