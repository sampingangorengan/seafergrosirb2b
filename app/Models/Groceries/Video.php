<?php

namespace App\Models\Groceries;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $table = 'videos';

   protected $fillable = ['file_name', 'groceries_id', 'created_at', 'updated_at'];
}
