<?php

namespace App\Models\Groceries;

use Illuminate\Database\Eloquent\Model;

class ChildCategoryImage extends Model
{
    protected $table = 'child_category_images';
    protected $fillable = ['file'];

    protected $uploads = '/child_category_images/';

    public function getFileAttribute($photo) {
        return $this->uploads.$photo;
    }
}
