<?php

namespace App\Models\Groceries;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class ChildCategory extends Model
{
    //
    use Sluggable;

    protected $table='child_categories';
    protected $fillable = ['name', 'slug', 'parent_id', 'order', 'is_active', 'child_category_image_id'];

    public function parent() {
        return $this->belongsTo('App\Models\Groceries\Category');
    }

    public function groceries() {
        return $this->belongsToMany('App\Models\Groceries', 'groceries_category', 'category_id', 'groceries_id')->where('deleted', 0);
    }

    public function image() {
        return $this->hasOne('App\Models\Groceries\ChildCategoryImage', 'id', 'child_category_image_id');
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => "name",
                'unique' => 'true'
            ]
        ];
    }
}
