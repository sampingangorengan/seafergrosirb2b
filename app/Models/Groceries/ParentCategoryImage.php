<?php

namespace App\Models\Groceries;

use Illuminate\Database\Eloquent\Model;

class ParentCategoryImage extends Model
{
    protected $table = 'parent_category_images';
    protected $fillable = ['file'];

    protected $uploads = '/parent_category_images/';

    public function getFileAttribute($photo) {
        return $this->uploads.$photo;
    }
}
