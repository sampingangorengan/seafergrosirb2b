<?php

namespace App\Models\Groceries;

use Illuminate\Database\Eloquent\Model;

class Metric extends Model
{
    //
    protected $fillable = ['name', 'abbreviation', 'is_active'];

    public function product() {
        return $this->belongsToMany('App\Models\Groceries');
    }
}
