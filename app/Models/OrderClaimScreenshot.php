<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderClaimScreenshot extends Model
{
    //
    protected $table = 'order_claim_screenshots';

    protected $fillable = ['file'];
}
