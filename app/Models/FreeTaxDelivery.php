<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FreeTaxDelivery extends Model
{
    protected $table = 'free_tax_deliveries';

    protected $fillable = ['minimum_spending', 'quota', 'usage', 'remaining', 'start_date', 'end_date', 'type', 'is_active'];

    public static function isFreeDeliveryApplied($price) {
    	$latest = self::where('type', 'delivery')->first();

    	$date_requirement = 0;
    	$spending_requirement = 0;
    	$quota_requirement = 0;
    	$active_requirement = 0;

    	# date requirement
    	$today = date('Y-m-d');
    	if (($today > $latest->start_date) && ($today < $latest->end_date)){
		    $date_requirement = 1;
		}

		# spending requirement
		if($latest->minimum_spending < $price) {
			$spending_requirement = 1;
		}

		# quota requirement
		if($latest->remaining > 0) {
			$quota_requirement = 1;
		}

		# active requirement
		if($latest->is_active == 1) {
			$active_requirement = 1;
		}

		if($date_requirement == 1 && $spending_requirement == 1 && $quota_requirement == 1 && $active_requirement) {
			return true;
			#return false;
		}
		return false;
		#return true;
		

    }
}
