<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SearchableGroceries extends Model
{
    //
    protected $table = 'searchable_groceries';

    protected $fillable = ['groceries_id','name','is_active'];

    public function groceries() {
    	return $this->belongsTo('App\Models\Groceries');
    }
}
