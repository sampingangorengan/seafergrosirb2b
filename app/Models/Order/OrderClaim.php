<?php

namespace App\Models\Order;

use Illuminate\Database\Eloquent\Model;

class OrderClaim extends Model
{
    //
    protected $table = 'order_claims';

    protected $fillable = ['name', 'email', 'order_id', 'product_name', 'reason', 'image','status','mobile'];

    public function order() {
        return $this->belongsTo('App\Models\Order');
    }

    public function photo() {
        return $this->belongsTo('App\Models\OrderClaimScreenshot', 'image', 'id');
    }
}
