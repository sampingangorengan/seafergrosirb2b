<?php

namespace App\Models\Home;

use Illuminate\Database\Eloquent\Model;

class Slide extends Model
{
    protected $table = 'home_slides';

    protected $fillable = ['file_name', 'link', 'order', 'caption','is_active'];
}
