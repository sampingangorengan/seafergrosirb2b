<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductSuggestion extends Model
{
    //
    protected $table = 'product_suggestions';
    protected $fillable = ['email', 'product_name', 'product_brand', 'category_id', 'comments', 'suggestion_picture', 'is_read'];

    protected $uploads = '/suggestion_picture/';

    public function getFileAttribute($photo) {
        return $this->uploads.$photo;
    }

    public function photo() {
        return $this->hasOne('App\Models\SuggestionPicture','id' ,'suggestion_picture');
    }
}
