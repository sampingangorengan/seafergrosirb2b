<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use App\Models\Groceries\Image;
use App\Models\Groceries\Video;
use App\Models\Groceries\Groceries_poster as Poster;
use App\Models\Groceries\Love;
use Illuminate\Database\Eloquent\Model;

class Groceries extends Model
{

    use Sluggable;

    public $isFavourites = false;

    protected $fillable = ['name', 'price', 'stock', 'minimum_stock_alert', 'description', 'nutrients', 'ingredients', 'category_id', 'sku', 'value_per_package', 'metric_id', 'brand_id', 'discount_id', 'slug', 'expiry_date', 'youtube_link', 'fresh_sea_water','nature','state','range_price','cooking_advice', 'price_after_discount', 'country_id','availabity_note','packing'];

    public function brand() {
        return $this->belongsTo('App\Models\Groceries\Brand');
    }

    public function category()
    {
        /*return $this->belongsToMany('App\Models\Groceries\ChildCategory');*/
        return $this->belongsToMany('App\Models\Groceries\ChildCategory', 'groceries_category', 'groceries_id', 'category_id');
    }

    public function getLoveCount($id)
    {
        return Love::whereGroceriesId($id)->count();
    }

    public function images()
    {
        return $this->hasMany(\App\Models\Groceries\Image::class);
    }

    public function cookingAdvice()
    {
        /*return $this->belongsToMany('App\Models\Groceries\ChildCategory');*/
        return $this->belongsToMany('App\Models\Groceries\CookingAdvice', 'groceries_cooking_advice', 'groceries_id', 'cooking_advice_id');
    }

    public function country() {
        return $this->belongsTo('App\Models\Country');
    }

    public static function getFirstImageUrl($id)
    {
        $image = Image::whereGroceriesId($id)->first();
        if (is_null($image)) {
            return asset('assets/images/blank-product.png');
        }

        return asset('contents/'.$image->file_name);
    }

    public function getAllImages($id){
        $images = Image::whereGroceriesId($id)->get();

        /*if ($images->count() == 0) {
            return asset('assets/images/img-empty.png');
        }*/

        return $images;
    }

    public function getYoutubeId($yt_url) {
        $youtube_video = parse_url(urldecode($yt_url));
        if(array_key_exists('query', $youtube_video)) {
            parse_str($youtube_video['query'], $query);
            if(count($query) > 0){
                return $query['v'];
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public function getAllPosters($id) {
        $images = Poster::whereGroceriesId($id)->get();

        /*if ($images->count() == 0) {
            return asset('assets/images/img-empty.png');
        }*/

        return $images;
    }

    public function tags(){
        return $this->belongsToMany('App\Models\Tag', 'groceries_tag', 'groceries_id', 'tag_id');
    }

    public function metric() {
        return $this->belongsTo('App\Models\Groceries\Metric');
    }

    public function discount() {
        return $this->belongsTo('App\Models\Discount');
    }

    public function video()
    {
        return $this->hasMany(\App\Models\Groceries\Video::class);
    }

    public function getFirstVideo($g_id) {
        $vid = Video::where('groceries_id', $g_id)->orderBy('id', 'desc')->first();

        return $vid;
    }

    public function getActiveDiscount($id) {
        $grocery = self::find($id);
        $discount = Discount::find($grocery->discount_id);
        return $discount;
    }

    public function questions() {
        return $this->hasMany('App\Models\Groceries\Question', 'groceries_id', 'id');
    }

    public function ratings(){
        return $this->hasMany('App\Models\Groceries\Rating','groceries_id','id');
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    public function supplier(){
        return $this->hasOne('App\Models\userSupplier','id','supplier_id');
    }

    public function suppliers(){
        return $this->belongsToMany('App\Models\userSupplier','groceries_suppliers','groceries_id','supplier_id');
    }

    public function favourites(){
        return $this->belongsToMany('App\Models\User','groceries_favourites','groceries_id','user_id')->withPivot('usual_qty','id');
    }

    public function isFavourites(){
        return $this->favourites->contains(auth()->user());
    }


    public function countRating(){
        $ratings = $this->ratings;
        $total_rate = (float) $ratings->count() * 5.0;
        $current_rate = 0.0;
        foreach($ratings as $rating){
            $current_rate += (float) $rating->rate;
        }
        $rate = $current_rate / $total_rate * 5.0;

        return $rate;
    }
}
