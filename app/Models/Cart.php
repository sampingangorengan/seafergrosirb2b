<?php

namespace App\Models;

use Money;
use App\Models\Groceries;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    public function groceries()
    {
        return $this->belongsTo(Groceries::class);
    }

    public static function getTotal($userId, $raw = false)
    {
        $cart = self::whereUserId($userId)->get();
        if ($cart->count() == 0) {
            return Money::display(0);
        }

        $total = 0;
        foreach ($cart as $cartItem) {
            $item = Groceries::find($cartItem->groceries_id);
            $total += $item->price * $cartItem->qty;
        }

        return ($raw === true) ? $total : Money::display($total);
    }

    public static function getGrandTotal($userId)
    {
        $total = self::getTotal($userId, true);
        $tax = self::getTaxOfTotal($userId, true);

        return Money::display($total + $tax);
    }

    public static function getTaxOfTotal($userId, $raw = false)
    {
        $tax = self::getTotal($userId, true) / 10;
        return ($raw === true) ? $tax : Money::display($tax);
    }

    /**
     * Update cart item quantity
     * @param  int $id
     * @param  int $count Could be a negative value
     * @return mixed
     */
    public static function updateQty($id, $count)
    {
        $cart = self::find($id);

        // carts.qty cannot be 0 or negative
        if ($cart->qty < 2 && $count < 1) {
            return false;
        }

        $cart->qty += $count;
        $cart->save();

        $itemTotal = Money::display($cart->qty * Groceries::find($cart->groceries_id)->price);
        $total = Cart::getTotal(auth()->user()->id);
        $taxOfTotal = self::getTaxOfTotal(auth()->user()->id);
        $grandTotal = self::getGrandTotal(auth()->user()->id);

        return compact('itemTotal', 'total', 'taxOfTotal', 'grandTotal');
    }
}
