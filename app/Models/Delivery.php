<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\GojekDetail;
use App\Models\JneDetail;

class Delivery extends Model
{
    //
    protected $table = 'deliveries';

    protected $fillable = ['delivery_type','detail_id','status', 'order_id', 'booking_unique'];

    public function getDeliveryDetail($detail_id, $type) {
        if($type == 'gojek') {
        	$detail = GojekDetail::find($detail_id);
        	return $detail;
        } elseif($type == 'jne') {
        	$detail = JneDetail::find($detail_id);
        	return $detail;
        } else {
            return "Undefined";
        }
    }

    public function order() {
        return $this->belongsTo('App\Models\Order');
    }
}
