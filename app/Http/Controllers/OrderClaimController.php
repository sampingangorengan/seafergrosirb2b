<?php

namespace App\Http\Controllers;

use App\Models\Notification;
use App\Models\Order;
use App\Models\Order\OrderClaim;
use App\Models\OrderClaimScreenshot;
use App\Models\StaticPage\StaticPage;
use App\Models\StaticPage\StaticDescription;
use App\Models\StaticPage\StaticImage;
use App\Models\StaticPage\StaticLink;
use Illuminate\Http\Request;

use Agent;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;


class OrderClaimController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $descriptions = StaticDescription::where('page_id', 7)->get();
        $description = array();
        foreach($descriptions as $desc){
            $description[$desc->description_position] = $desc;
        }
        
        return view('frontend.claim_order', compact('description'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'name'              => 'required',
            'is_exchange_refund'=> 'required|not_in:0',
            'email'             => 'required|email',
            'mobile'            => 'required|numeric',
            'order_id'          => 'required',
            'product_name'      => 'required',
            'reason'            => 'required|not_in:0',
            'file'              => 'mimes:jpeg,bmp,png',
        ]);

        /*$this->validate($request, [
            'name'              => 'required',
            'is_exchange_refund'=> 'required',
            'email'             => 'required|email',
            'mobile'            => 'required',
            'order_id'          => 'required',
            'sku'               => 'required',
            'reason'            => 'required',
            'file'              => 'required',
        ]);*/



        if ($validator->fails()) {
            $string_error = '';

            /*foreach($validator->errors()->getMessages() as $error) {
                $string_error .= $error[0].' ';
            }*/

            $request->session()->flash('alert-danger', 'All field is required. Please make sure that you have filled all the field');

            return back();
        }

        $order = Order::find($request->order_id);

        if(null == $order) {
            $request->session()->flash('alert-danger', 'Order not found.');

            return back();
        }

        $notif = new Notification;
        $notif->user_id = $order->user->id;
        $notif->title = 'Order Claim On Process';
        $notif->description = 'Claim for order #'.$order->id.' has been submitted';
        $notif->link = url('orders/'.$order->id);
        $notif->is_read = 0;
        $notif->save();

        $claim = new OrderClaim;
        $claim->name = $request->name;
        $claim->is_exchange_refund = $request->is_exchange_refund;
        $claim->email = $request->email;
        $claim->mobile = $request->mobile;
        $claim->order_id = $request->order_id;
        $claim->product_name = $request->product_name;
        $claim->reason = $request->reason;
        $claim->status = 'new';


        if($file = $request->file('file')) {
            $name = time().$file->getClientOriginalName();

            $file->move('order_claim_screenshots', $name);

            $photo = OrderClaimScreenshot::create(['file' => $name]);

            $claim->image = (int) $photo->id;
        }

        $claim->save();

        $request->session()->flash('alert-success', 'Your claim was successful submitted! We\'ll process your claim and get in touch with you very soon.');

        if(! Agent::isDesktop()) {
            return redirect('return-and-exchange');            
        }

        return redirect('order-claim');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        /*dd($request->all());*/
        dd($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
