<?php

namespace App\Http\Controllers;

use App\Models\User\UserBank;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use GuzzleHttp\Tests\Psr7\Str;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\User\Address;
use Knp\Snappy\Pdf;
use Agent;

class OrderController extends Controller
{
    public function show(Request $request, $id)
    {
        if (false === auth()->check()) {
            return redirect('/');
        }

        $order = Order::find($id);
        if(null == $order) {
            abort(404);
        }

        if(auth()->user()->id != $order->user_id) {
            $request->session()->flash('alert-danger', 'You are not the owner of this order id.');
            return redirect('/');
        }

        if($order->isMeasured == 0){
            $request->session()->flash('alert-info', 'Please wait for your order to be measured before you pay.');
        }
        $order->address_detail = Address::find($order->user_address_id);
        $smallAdditionalFee = $order->unique_code;
        $bank_preference = UserBank::where('user_id', auth()->user()->id)->where('is_preference', 1)->first();
        return view('frontend.user.my_order_detail', compact('order', 'smallAdditionalFee', 'bank_preference'));

    }

    public function download($id)
    {
        if (false === auth()->check()) {
            return redirect('/');
        }

        # on live
        $snappy = new Pdf(base_path('vendor/h4cc/wkhtmltopdf-amd64/bin/wkhtmltopdf-amd64'));

        # on dev
        #$snappy = new Pdf('/usr/local/bin/wkhtmltopdf');

        /*header('Content-Type: application/pdf');
        header('Content-Disposition: attachment; filename="invoice.pdf"');*/
        try {
            $pdf = $snappy->getOutput(url('orders/showPrint', $id));
            return new Response(
                $pdf,
                200,
                array(
                    'Content-Type'          => 'application/pdf',
                    'Content-Disposition'   => 'attachment; filename="invoice.pdf"'
                )
            );
        } catch (\Exception $e) {
            echo $e->getMessage();
        }

    }

    public function showPrint($id) {
        $order = Order::find($id);
        $smallAdditionalFee = $order->unique_code;
        return view('frontend.order.download', compact('order', 'smallAdditionalFee'));
    }
}
