<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Groceries\Category;
use App\Models\Groceries\ChildCategory;
use Illuminate\Http\Response;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex()
    {
        $category = Category::select('groceries_categories.*','parent_category_images.file as image_category_file')
        ->leftJoin('parent_category_images','groceries_categories.parent_category_image_id','=','parent_category_images.id')
        ->where('is_active', 1)->orderBy('order')->get();

        $status = 400;
        $content_type = 'application/json';

        if($category) {
            foreach($category as $ct){
                $ct->child = ChildCategory::select('child_categories.*','child_category_images.file as image_category_file')
                ->leftJoin('child_category_images','child_categories.child_category_image_id','=','child_category_images.id')
                ->where('parent_id', $ct->id)->get();
            }
            $status = 200;
            $response = array(
                'data' => $category,
                'status' => true,
                'msg' => 'Category is available'
            );
        } else {
            $response = array(
                'status' => false,
                'msg' => 'No data available'
            );
        }

        return (new Response($response, $status))
                    ->header('Content-Type', $content_type);
    }
}
