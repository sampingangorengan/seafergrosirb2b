<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Groceries;
use App\Models\User;
use App\Models\Cart;
use App\Models\Order;
use App\Models\Order\Groceries as OrderGroceries;
use App\Models\PaymentProof;
use App\Models\Notification;
use App\Models\Promo;
use App\Models\PromoHistory;
use App\Models\Order\Payment;
use App\Models\User\CashbackReward;
use App\Models\User\CashbackPoint;
use App\Models\User\Address;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Mail;
use Validator;
use Money;
use LocalizedCarbon;

class OrderController extends Controller
{
	private function make_response($status = true, $message = '', $data = null) {
        if(null == $data) {
            $response = array(
                'status' => $status,
                'msg' => $message
            );
        } else {
            $response = array(
                'data' => $data,
                'status' => $status,
                'msg' => $message
            );
        }

        return (new Response($response, 200))
        ->header('Content-Type', 'application/json');
    }

    private function updateAfterRedeem(Order $order,$value,$userid) {

        $cashback = new CashbackReward;
        $cashback->user_id = $userid;
        $cashback->order_id = $order->id;
        $cashback->obtain_from = 'redeem';
        $cashback->nominal = $value;
        $status = $cashback->save();

        if($status) {
            $points = CashbackPoint::where('user_id', $userid)->first();
            $points->nominal = $points->nominal - $value;
            $stat = $points->save();

            if($stat) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private function setGojekDetail($type) {

        $delivery_detail_id = DB::table('gojek_details')->insertGetId(
            [
                'status' => 'not made',
                'booking_type' => $type,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]
        );
        return $delivery_detail_id;
    }

    private function setJneDetail($type) {
        $delivery_detail_id = DB::table('jne_details')->insertGetId(
            [
                'status' => 'not made',
                'booking_type' => $type,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]
        );

        return $delivery_detail_id;
    }

    public function postConfirmPayment(Request $request) {
    	$validator = Validator::make($request->all(), [
            'userid'          					=> 'required',
            'transfer_amount'					=> 'required|numeric',
            'transfer_date'      				=> 'required',
            'transfer_time_minute'         		=> 'required|numeric',
            'transfer_time_second'         		=> 'required|numeric',
            'transfer_sender_bank_name'     	=> 'required',
            'transfer_sender_account_number'    => 'required',
            'transfer_sender_account_name'      => 'required',
            'file'          					=> 'mimes:jpeg,bmp,png',
            'order_number' 						=> 'required|numeric',
        ]);

        if ($validator->fails()) {
            return $this->make_response(false, 'Please complete the required parameters', $validator->errors());
        }

        if(null != $request->userid) {
            $user = User::find($request->userid);
        } else {
            return $this->make_response(false, 'User not found');
        }

        $order = Order::find($request->order_number);

        if(null == $order) {
        	return $this->make_response(false, 'Order not found');
        }

        if($order->isMeasured == 0){
            return $this->make_response(false, 'Your order hasn\'t been measured');
        }

        $payment = Payment::where('order_id',$order->id)->first();
        $payment->order_id = $order->id;

        $payment->transfer_dest_account = $request->transfer_dest_account;
        $payment->transfer_sender_bank_name = strtoupper($request->transfer_sender_bank_name);
        $payment->transfer_sender_account_number = $request->transfer_sender_account_number;
        $payment->transfer_sender_account_name = $request->transfer_sender_account_name;
        $payment->transfer_date = $request->transfer_date;
        $payment->transfer_time = $request->transfer_time_minute.':'.$request->transfer_time_second;
        $payment->transfer_note = $request->transfer_note;
        $payment->transfer_amount = $request->transfer_amount;
        $payment->notify_admin = 0;
        $payment->user_confirmed = 1;
        $payment->transfer_dest_account = 'BCA: 2063232327';

        if($file = $request->file('file')) {

            $name = time() . $order->id . '.' . $file->getClientOriginalExtension();

            $file->move('payment_proof', $name);

            $photo = PaymentProof::create(['file' => $name]);

            $payment->transfer_receipt_file = (int) $photo->id;
        }

        $status = $payment->save();

        if($status) {
            $order->order_status = 6;
            $order->save();

            $notif = new Notification;
            $notif->user_id = $order->user_id;
            $notif->title = 'Payment Proof Submitted';
            $notif->description = 'Bukti pembayaran berhasil dikirim';
            $notif->link = url('orders/'.$order->id);
            $notif->is_read = 0;
            $notif->save();

        	return $this->make_response(true, 'Successfully send payment confirmation', Payment::find($payment->id));
        } else {
        	return $this->make_response(false, 'Failed sending payment confirmation');
        }

    }



    public function postCheckout(Request $request) {

        $validator = Validator::make($request->all(), [
            'userid'                => 'required',
            'address_id'            => 'required',
            'shipping_service_code' => 'required',
            'shipping_fee'          => 'required',
            'use_promo'             => 'required',
            'promo_id'              => 'required',
            'promo_value'           => 'required',
            'total_before_tax'      => 'required',
            'grand_total'           => 'required',
            'tax'                   => 'required',
            'unique_code'           => 'required',
            'delivery_date'         => 'required',
            'delivery_note'         => 'required',
            'payment_option'        => 'required'
        ]);

        if ($validator->fails()) {
            return $this->make_response(false, 'Please complete the required parameters', $validator->errors());
        }

		if($request->total_before_tax < 500000){
			return $this->make_response(false, 'Minimum basket size must be Rp500,000 to be able to check out', $validator->errors());
		}




        $user = User::find($request->userid);


        if(null == $user) {
            return $this->make_response(false, 'User not found');
        }

        if($request->payment_option == "tempo"){
            if($user->tempo == null || $user->tempo->status != "approved"){
                return $this->make_response(false, 'You havent been approved for tempo payment', $validator->errors());
            }
        }

        $user_cart = Cart::where('user_id', $user->id)->get();


        if(null == $user_cart) {
            $this->make_response(false, 'Cart not found');
        }


        // Insert to "orders" table
        $ord_id = DB::table('orders')->insertGetId(
            [
                'user_address_id' => $request->address_id,
                'user_id' => $user->id,
                'shipping_service_code' => "internal",
                'shipping_fee' => $request->shipping_fee,
                'order_status' => 1,
                'is_using_promo' => $request->promo_id,
                'promo_value' => $request->promo_value,
                'redeem' => "",
                'nett_total' => $request->total_before_tax,
                'tax' => $request->tax,
                'grand_total' => $request->grand_total,
                'unique_code' => $request->unique_code,
                'delivery_date' => $request->delivery_date,
                'delivery_notes' => $request->delivery_note,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]
        );
		$res["order_id"] = $ord_id;


        $default_payment = new Payment;
        $default_payment->order_id = $ord_id;
        $default_payment->method = $request->payment_option;
        $default_payment->transfer_dest_account = 'Not Available';
        $default_payment->transfer_sender_account_number = 'Not Available';
        $default_payment->transfer_sender_account_name = 'Not Available';
        $default_payment->transfer_date = date('Y-m-d');
        $default_payment->transfer_time = '00:00:01';
        $default_payment->transfer_note = 'Not Available';
        $default_payment->transfer_receipt_file = '';
        $default_payment->is_approved = 0;
        $default_payment->transfer_amount = 0;
        $default_payment->transfer_sender_bank_name = 'Not Available';
        $default_payment->payment_status_id = 0;
        $default_payment->notify_admin = 0;
        $default_payment->user_confirmed = 0;
        $default_payment->save();


        if($request->promo_id != 0) {
            $promo = Promo::find($use_promo);

            $promo_history = new PromoHistory;
            $promo_history->user_id = $user->id;
            $promo_history->order_id = $ord_id;
            $promo_history->promo_id = $promo->id;
            $promo_history->promo_code = $promo->code;
            $promo_history->save();
        }

        // Insert to "order_groceries" table
        $isMeasured = 1;
        foreach ($user_cart as $cartGroceries) {

            if(null == Groceries::find($cartGroceries->groceries_id)) {
                $product = Groceries::where('price', $cartGroceries->price)->where('sku', $cartGroceries->sku)->where('name', $cartGroceries->name)->first();
                $orderGroceries = new OrderGroceries;
                $orderGroceries->order_id = $ord_id;
                $orderGroceries->groceries_id = $product->id;
                $orderGroceries->price = $cartGroceries->price;
                $orderGroceries->qty = $cartGroceries->quantity;
                $orderGroceries->save();

                if(($product->range_price != null || $product->range_price != ""  || $product->range_price != " ") && $isMeasured == 1){
                    $isMeasured = 0;
                }

            } else {

                $orderGroceries = new OrderGroceries;
                $orderGroceries->order_id = $ord_id;
                $orderGroceries->groceries_id = $cartGroceries->groceries_id;
                $orderGroceries->price = $cartGroceries->price;
                $orderGroceries->qty = $cartGroceries->quantity;
                $orderGroceries->save();


            }

            // update stock and quantity
            $update_stock_sold = Groceries::find($cartGroceries->groceries_id);
            $update_stock_sold->stock = $update_stock_sold->stock - $cartGroceries->quantity;
            $update_stock_sold->sold = $update_stock_sold->sold + $cartGroceries->quantity;
            $update_stock_sold->save();
            $cartGroceries->delete();

        }

        $notif = new Notification;
        $notif->user_id = $user->id;
        $notif->title = 'New Order Made';
        $notif->description = 'Pesanan anda berhasil dibuat';
        $notif->link = url('orders/'.$ord_id);
        $notif->is_read = 0;
        $notif->save();

        $order = Order::find($ord_id);
        $order->isMeasured = $isMeasured;
        $order->save();

        $order->deliveries()->create([
            'delivery_type' => "Not Defined",
            'detail_id' => NULL,
            'status' => 'not made',
            'order_id' => $order->id
        ]);

        $mes = "";
        $res["payment"] = $request->payment_option;



		if($request->payment_option== "cod"){
			$mes .=
			"<p>mohon melakukan pembayaran secara tunai saat pengiriman pesanan Anda telah tiba.</p>";
		}else{
			$mes = "<br/>
				<p><strong>Mohon melakukan pembayaran sesuai berikut:</strong></p>
				<ol style='text-align:left;'>
					<li>Silakan lakukan pembayaran Anda ke akun berikut: PT. Seafer Grosir Jaya Raya - BCA 206 3232 327 / Bank Panin 1535007529</li>
					<li>Ambil gambar bukti pembayaran Anda dan unggah ke formulir Konfirmasi Pembayaran.</li>
					<li>Seafer Grosir akan memberi tahu Anda setelah pembayaran dilakukan.</li>
					<li>Seafer Grosir akan mengkonfirmasi pembayaran Anda segera dalam 24 jam dan memproses pesanan Anda.</li>
					<li>Kami berterima kasih atas pembayaran tepat waktu dan bertanggung jawab Anda.</li>
				</ol>";

			if($request->payment_option == "tempo"){
				$tempoPaymentDate =  LocalizedCarbon::now()->addDays($user->tempo->duration);
                $res["tempoPaymentDate"] = $tempoPaymentDate;
                $res["tempoDuration"] = $user->tempo->duration;

				$mes .= "<br>";

				$mes .= "<p style='color:#ff0335 !important'>Pembayaran anda akan jatuh tempo pada <b>".$tempoPaymentDate->format("d F Y H:m:s")."</b>.</p>";

			}else{
				$tempoPaymentDate =  LocalizedCarbon::now()->addDays(1);
                $res["transferPaymentDate"] = $tempoPaymentDate;

				$mes .= "<br>";

				$mes .= "<p style='color:#ff0335 !important'>Lakukan Pembayaran transfer anda sebelum <b>".$tempoPaymentDate->format("d F Y H:m:s")."</b>.</p>";

			}

			if(!$isMeasured){
				$mes .= "<br>";
				$mes .= "<p style='color:#ff0335 !important'>Harga pesanan anda akan segera kami update setelah melakukan penimbangan pada beberapa pesanan anda.</p>";
			}
		}
        $order_number = $ord_id;
        // //Mail
        try{
            Mail::queue('emails.order_done', compact('user','mes','order'), function ($message) use ($user) {

                $message->from('hello@seafermart.co.id', '[NO-REPLY] Order Submitted');

                $message->to($user->email);

            });
        }catch(Exception $e) {
            return $this->make_response(false, 'ERROR SENDING MAIL',$ord_id);
        }


        try{
            Mail::queue('emails.admin_order_cod', compact('user','order_number'), function ($message) use ($user) {

                $message->from('hello@seafermart.co.id', '[NO-REPLY] New COD Order');

                $message->to('hello@seafermart.co.id');

            });
        }catch(Exception $e) {
            return $this->make_response(false, 'ERROR SENDING MAIL',$ord_id);
        }

        return $this->make_response(true, 'Order successfully made',$res);
    }

    public function postOrderDetailBackup(Request $request){

        $validator = Validator::make($request->all(), [
            'orderid'                => 'required',
        ]);

        if ($validator->fails()) {
            return $this->make_response(false, 'Please complete the required parameters', $validator->errors());
        }

        $order = Order::find($request->orderid);

        if(null == $order) {
            return $this->make_response(false, 'Order not found');
        }

        $order->user_details = User::withTrashed()->find($order->user_id);

        if(null == $order->user_details) {
            return $this->make_response(false, 'Ooops this is an old order, with old structure. try to open a late order or create a new one.');
        }

        $order->address_details = Address::find($order->user_address_id);

        $order->groceries = OrderGroceries::where('order_id', $order->id)->get();

        if($order->groceries->count() > 0) {
            foreach($order->groceries as $grc){

                $grc->metric_name = Metric::find($grc->metric_id);
            }
        }



        $check = DB::table('orders')->select('orders.*','order_statuses.description as order_status_desc')
                                    ->leftJoin('order_statuses','order_statuses.id','=','orders.order_status')
                                    ->where('orders.id','=',$orderid)
                                    ->first();
        if($check){
            $check->user_details = DB::table('users')->where('id','=',$check->user_id)->first();
            $check->address_details = DB::table('user_addresses')->where('id','=',$check->user_address_id)->first();
            $check->groceries = DB::table('order_groceries')
                                ->select('order_groceries.*','groceries.name as product_name','groceries.price as product_price','metrics.name as metric_name')
                                ->leftJoin('groceries','groceries.id','=','order_groceries.groceries_id')
                                ->leftJoin('metrics','metrics.id','=','groceries.metric_id')
                                ->where('order_id','=',$check->id)
                                ->get();
            $shipping_code = $check->shipping_service_code;
            if($shipping_code == 'jne_reg'){
                $check->shipping_service_description = 'JNE Regular';
            } elseif($shipping_code == 'jne_yes'){
                $check->shipping_service_description = 'JNE Yes';
            } elseif($shipping_code == 'jne_oke'){
                $check->shipping_service_description = 'JNE Oke';
            } elseif ($shipping_code == 'gosend_sameday') {
                $check->shipping_service_description = 'Gosend Sameday Courier';
            } elseif($shipping_code == 'gosend_instant') {
                $check->shipping_service_description = 'Gosend Instant Courier';
            } elseif($shipping_code == 'internal') {
                $check->shipping_service_description = 'Seafer Sameday Courier';
            } else {

                $check->shipping_service_description = 'Courier Undefined';
            }
            $get_cashback = DB::table('cashback_rewards')
                            ->where('order_id','=',$check->id)
                            ->first();
            if($get_cashback){
                $check->cashback = $get_cashback->nominal;
            } else {
                $check->cashback = 0;
            }
            return $this->make_response(true,'Order found',$check);
        } else {
            return $this->make_response(false,'Order not found',null);
        }
    }

    public function postOrderDetail(Request $request){
        $orderid = $request->orderid;
        $order = Order::find($request->orderid);
        if(null == $order) {
            return $this->make_response(false,'Order not found',null);
        }
        $check = DB::table('orders')->select('orders.*','order_statuses.description as order_status_desc')
                                    ->leftJoin('order_statuses','order_statuses.id','=','orders.order_status')
                                    ->where('orders.id','=',$orderid)
                                    ->first();
        if($check){
            $check->user_details = DB::table('users')
                                        ->leftJoin('user_tempo','users.id','=','user_tempo.user_id')
                                        ->where('users.id','=',$check->user_id)->first();
            $check->address_details = DB::table('user_addresses')->where('id','=',$check->user_address_id)->first();
            $check->groceries = DB::table('order_groceries')
                                ->select('order_groceries.*','groceries.name as product_name','groceries.price as product_price','metrics.name as metric_name')
                                ->leftJoin('groceries','groceries.id','=','order_groceries.groceries_id')
                                ->leftJoin('metrics','metrics.id','=','groceries.metric_id')
                                ->where('order_id','=',$check->id)
                                ->get();


            $check->shipping_service_description = 'Seafer Grosir Courier';

            $check->payment = DB::table('order_payments')
									->select('method','transfer_date')
									->where('order_id','=',$check->id)
									->first();

            if($check->payment->method == "tempo"){
                $check->payment->paymnent_due = LocalizedCarbon::parse($check->created_at)->addDays($check->user_details->duration)->format('Y-m-d h:i:s');
            }else
            if($check->payment->method == "transfer"){
                $check->payment->paymnent_due = LocalizedCarbon::parse($check->created_at)->addDays(1)->format('Y-m-d h:i:s');
            }



            $get_cashback = DB::table('cashback_rewards')
                            ->where('order_id','=',$check->id)
                            ->first();
            if($get_cashback){
                $check->cashback = $get_cashback->nominal;
            } else {
                $check->cashback = 0;
            }

            foreach($check->groceries as $item){
                $item->product_image = Groceries::getFirstImageUrl($item->groceries_id);
            }

            return $this->make_response(true,'Order found',$check);
        } else {
            return $this->make_response(false,'Order not found',null);
        }
    }

    public function postShippingPrice(Request $request) {
        $address_id = $request->address_id;
        $shipping_code = $request->shipping_code;
        $address = Address::find($address_id);
        if($address){
            if($shipping_code == 'jne_reg' || $shipping_code == 'jne_yes'){
                if(isset($address->city->city_name)){
                    $city = $address->city->city_name;
                } else {
                    $city = false;
                }
                if($city){
                    $phrase = explode(' ', $city);
                    $client = new Client([
                        // Base URI is used with relative requests
                        'base_uri' => 'http://api.jne.co.id:8889/',
                        // You can set any number of default request options.
                        'timeout'  => 2.0,
                    ]);
                    $body_array = array('username' => env('JNE_USERNAME'), 'api_key' => env('JNE_API_KEY') );
                    $body = \GuzzleHttp\json_encode($body_array);

                    $res = $client->request('POST', '/tracing/seafermart/origin/key/'.strtolower($phrase[0]), [
                        'form_params' => [
                            'username' => env('JNE_USERNAME'),
                            'api_key' => env('JNE_API_KEY'),
                        ]
                    ]);

                    if ($res->getReasonPhrase() == "OK") {
                        $city_detail = json_decode($res->getBody())->detail;
                    } else {
                        $city_detail = false;
                    }


                    if(false !== $city_detail) {

                        $res = $client->request('POST', '/tracing/seafermart/price/', [
                            'form_params' => [
                                'username' => env('JNE_USERNAME'),
                                'api_key' => env('JNE_API_KEY'),
                                'from' => 'CGK10000',
                                'thru' => $city_detail[0]->code,
                                'weight' => 1
                            ]
                        ]);

                        if($res->getReasonPhrase() == "OK"){
                            $all_result = json_decode($res->getBody());
                            $price_result = $all_result->price;

                            if ($shipping_code == 'jne_yes') {

                                $shippingFee = floatval($price_result[4]->price);

                            } elseif ($shipping_code == 'jne_reg') {

                                $shippingFee = floatval($price_result[2]->price);

                            }
                        }
                    }
                } else {
                    $shippingFee = 0;
                }
            } else if($shipping_code == 'gosend_sameday'){
                $shippingFee = floatval(15000);
            } else {
                $shippingFee = floatval(20000);
            }
            if($shippingFee != 0){
                return $this->make_response(true,'Shipping price found',$shippingFee);
            } else {
                return $this->make_response(false,'Address not supported city name not found',null);
            }
        } else {
            return $this->make_response(false,'Address not found',null);
        }
    }

    public function getShippingMethod(){
        $sm = array(
            'JNE' => array('jne_reg','jne_yes'),
            'GOSEND' => array('gosend_sameday'),
            'SEAFER' => array('seafer_sameday')
        );
        return $this->make_response(true,'Shipping Method List',$sm);
    }
}
