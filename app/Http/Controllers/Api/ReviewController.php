<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use App\Models\Groceries\Rating;
use App\Models\Groceries;
use App\Models\User;
use Validator;

class ReviewController extends Controller
{

    private function make_response($status = true, $message = '', $data = null) {
        if(null == $data) {
            $response = array(
                'status' => $status,
                'msg' => $message
            );
        } else {
            $response = array(
                'data' => $data,
                'status' => $status,
                'msg' => $message
            );
        }

        return (new Response($response, 200))->header('Content-Type', 'application/json');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function postList(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'groceries_id'          => 'required',
        ]);

        if ($validator->fails()) {
            return $this->make_response(false, 'Please complete the required parameters');
        }

        $groceries = Groceries::find($request->groceries_id);

        if(null == $groceries){
            return $this->make_response(false, 'Grocery not found');
        }

        $review = Rating::where('groceries_id', $groceries->id);

        $order = 'asc';
        if(null !== $request->order_by) {
            if(null !== $request->order) {
                $order = $request->order;
            }

            if(! in_array($request->order, ['asc', 'desc', 'ascending', 'descending'])) {
                return $this->make_response(false, 'Order parameter can only be "asc" or "desc" or "ascending" or "descending"');
            }

            $review = $review->orderBy($request->order_by, $request->order)->get();
        } else {
            $review = $review->orderBy('created_at', 'desc')->get();
        }

        if($review->count() > 0){

            foreach($review as $item) {
                $udetail = User::find($item->user_id);
                $item->user_detail = array(
                        'name' => $udetail->company_name ?? "-",
                        'pic' => url('profile_picture/'.$udetail->photo->file)
                    );
            }

            return $this->make_response(true, 'Review are available', $review);
        } else {
            return $this->make_response(false, 'Review empty');
        }

        return $this->make_response(false, 'Review empty');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function postMyList(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id'          => 'required',
        ]);

        if ($validator->fails()) {
            return $this->make_response(false, 'Please complete the required parameters');
        }

        $user = User::find($request->user_id);

        if(null == $user){
            return $this->make_response(false, 'User not found');
        }

        $review = Rating::where('user_id', $user->id);

        $order = 'asc';
        if(null !== $request->order_by) {
            if(null !== $request->order) {
                $order = $request->order;
            }

            if(! in_array($request->order, ['asc', 'desc', 'ascending', 'descending'])) {
                return $this->make_response(false, 'Order parameter can only be "asc" or "desc" or "ascending" or "descending"');
            }

            $review = $review->orderBy($request->order_by, $request->order)->get();
        } else {
            $review = $review->orderBy('created_at', 'desc')->get();
        }

        if($review->count() > 0){

            foreach($review as $item) {
                $item->grocery_detail = Groceries::find($item->groceries_id);
                if(null !== $item->grocery_detail){
                   $item->grocery_detail->image = Groceries::getFirstImageUrl($item->groceries_id);
                }

            }

            return $this->make_response(true, 'Review are available', $review);
        } else {
            return $this->make_response(false, 'Review empty');
        }

        return $this->make_response(false, 'Review empty');
    }
}
