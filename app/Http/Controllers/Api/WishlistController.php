<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Order;
use App\Models\Wishlist;
use Illuminate\Support\Facades\DB;
use App\Models\Groceries;
use App\Models\Groceries\Brand;
use App\Models\Groceries\Metric;
use Validator;
use Illuminate\Http\Response;

class WishlistController extends Controller
{
    private function make_response($status = true, $message = '', $data = null) {
        if(null == $data) {
            $response = array(
                'status' => $status,
                'msg' => $message
            );
        } else {
            $response = array(
                'data' => $data,
                'status' => $status,
                'msg' => $message
            );
        }

        return (new Response($response, 200))
        ->header('Content-Type', 'application/json');
    }

    private function check_bought_groceries(Groceries $groceries, $user_id) {
        $order_lists = Order::where('user_id', $user_id)->get();

        foreach ($order_lists as $order) {
            $order_groceries = $order->groceries;
            foreach ($order_groceries as $oc) {
                if ($groceries->id == $oc->groceries_id) {
                    return True;
                }
            }
        }

        return False;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function postList(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userid'          => 'required',
        ]);

        if ($validator->fails()) {
            return $this->make_response(false, 'Please complete the required parameters');
        }

        if(null != $request->userid) {
            $user = User::find($request->userid);
        } else {
            return $this->make_response(false, 'User not found');
        }

        $wishlist = Wishlist::where('user_id', $user->id);

        $order = 'asc';
        if(null !== $request->order_by) {
            if(null !== $request->order) {
                $order = $request->order;
            }

            if(! in_array($request->order, ['asc', 'desc', 'ascending', 'descending'])) {
                return $this->make_response(false, 'Order parameter can only be "asc" or "desc" or "ascending" or "descending"');
            }

            $wishlist = $wishlist->orderBy($request->order_by, $request->order)->get();
        } else {
            $wishlist = $wishlist->orderBy('created_at', 'desc')->get();
        }

        if($wishlist->count() > 0){

            foreach ($wishlist as $key => $value) {
                if(null !== $request->filter_type) {
                    
                    if($request->filter_type == 'discount') {
                        if($value->groceries->discount_id != 0 && $value->groceries->discount_id != null) {
                            $value->grocery_detail = Groceries::find($value->groceries_id);
                            $value->grocery_detail->image = Groceries::getFirstImageUrl($value->grocery_detail->id);
                            $value->grocery_detail->brand_name = Brand::find($value->grocery_detail->brand_id)->name;
                            $value->grocery_detail->metric_unit = Metric::find($value->grocery_detail->metric_id)->abbreviation;
                            unset($value->groceries);
                        } else {
                            unset($wishlist[$key]);
                        }
                    } elseif ($request->filter_type == 'not bought') {
                        if($this->check_bought_groceries($value->groceries, $user->id)) {
                            $value->grocery_detail = Groceries::find($value->groceries_id);
                            $value->grocery_detail->image = Groceries::getFirstImageUrl($value->grocery_detail->id);
                            $value->grocery_detail->brand_name = Brand::find($value->grocery_detail->brand_id)->name;
                            $value->grocery_detail->metric_unit = Metric::find($value->grocery_detail->metric_id)->abbreviation;
                            unset($value->groceries);
                        } else {
                            unset($wishlist[$key]);
                        }
                    } elseif ($request->filter_type == 'bought'){
                        if(!$this->check_bought_groceries($value->groceries, $user->id)) {
                            $value->grocery_detail = Groceries::find($value->groceries_id);   
                            $value->grocery_detail->image = Groceries::getFirstImageUrl($value->grocery_detail->id);
                            $value->grocery_detail->brand_name = Brand::find($value->grocery_detail->brand_id)->name;
                            $value->grocery_detail->metric_unit = Metric::find($value->grocery_detail->metric_id)->abbreviation;
                            unset($value->groceries);
                        } else {
                            unset($wishlist[$key]);
                        }
                    } elseif ($request->filter_type == 'all') {
                        $value->grocery_detail = Groceries::find($value->groceries_id);
                        $value->grocery_detail->image = Groceries::getFirstImageUrl($value->grocery_detail->id);
                        $value->grocery_detail->brand_name = Brand::find($value->grocery_detail->brand_id)->name;
                        $value->grocery_detail->metric_unit = Metric::find($value->grocery_detail->metric_id)->abbreviation;
                        unset($value->groceries);
                    }
                    
                } else {
                    $value->grocery_detail = Groceries::find($value->groceries_id);
                    $value->grocery_detail->image = Groceries::getFirstImageUrl($value->grocery_detail->id);
                    $value->grocery_detail->brand_name = Brand::find($value->grocery_detail->brand_id)->name;
                    $value->grocery_detail->metric_unit = Metric::find($value->grocery_detail->metric_id)->abbreviation;
                    unset($value->groceries);
                }
            }
            /*




            foreach ($wishlist as $w) {
                $w->product = DB::table('groceries')
                                ->where('id','=',$w->groceries_id)
                                ->first();
                $metric = DB::table('metrics')
                                ->where('id','=',$w->product->metric_id)
                                ->first();
                $brand = DB::table('brands')
                                ->where('id','=',$w->product->brand_id)
                                ->first();
                if($metric){
                    $w->product->metric_name = $metric->name;
                } else {
                    $w->product->metric_name = 'Metric Not Found';
                }
                if($brand){
                    $w->product->brand_name = $brand->name;
                } else {
                    $w->product->brand_name = 'brand Not Found';
                }
                $images = DB::table('groceries_images')
                            ->where('groceries_id','=',$w->groceries_id)
                            ->get();

                if(count($images) > 0){
                    foreach($images as $img){
                        $img->image_url = url('/contents/'.$img->file_name);
                    }
                    $w->product->product_images = $images;

                } else {
                    $w->product->product_images = 'No Images Available';
                }
            }*/
            if($wishlist->count() > 0) {
                return $this->make_response(true, 'Wishlists are available', $wishlist);    
            } else {
                return $this->make_response(true, 'Wishlist empty');    
            }
            
        } else {
            return $this->make_response(true, 'Wishlist empty');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function postCreate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userid'          => 'required',
            'groceries_id'      => 'required',
        ]);

        if ($validator->fails()) {
            return $this->make_response(false, 'Please complete the required parameters');
        }

        if(null != $request->userid) {
            $user = User::find($request->userid);
        } else {
            return $this->make_response(false, 'User not found');
        }

        if(null != $request->groceries_id) {
            $grocer = Groceries::find($request->to_id);
            
            if(null != $grocer) {
                return $this->make_response(false, 'Groceries not found');
            }
        } 

        $wishlist = new Wishlist;
        $wishlist->user_id = $user->id;
        $wishlist->groceries_id = $request->groceries_id;
        $wishlist->save();

        $wishlist = Wishlist::find($wishlist->id)->toArray();

        return $this->make_response(true, 'Success creating wishlist', $wishlist);
    }

    public function postDelete(Request $request, $id) {
        $validator = Validator::make($request->all(), [
            'userid'          => 'required',
        ]);

        if ($validator->fails()) {
            return $this->make_response(false, 'Please complete the required parameters');
        }

        $user = User::find($request->userid);

        if(null == $user) {
            return $this->make_response(false, 'User not found');
        }

        $wishlist = Wishlist::find($id);

        if(null == $wishlist) {
            return $this->make_response(false, 'Wishlist not found');
        }

        if($user->id != $wishlist->user_id) {
            return $this->make_response(false, 'You are not allowed to tamper this wishlist');
        }

        $wishlist->delete();
        return $this->make_response(true, 'Wishlist successfully deleted');
        
    }
}
