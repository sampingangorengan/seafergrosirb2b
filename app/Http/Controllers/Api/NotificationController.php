<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use App\Models\Groceries\Rating;
use App\Models\Groceries;
use App\Models\User;
use App\Models\Notification;
use Validator;

class NotificationController extends Controller
{

    private function make_response($status = true, $message = '', $data = null) {
        if(null == $data) {
            $response = array(
                'status' => $status,
                'msg' => $message
            );
        } else {
            $response = array(
                'data' => $data,
                'status' => $status,
                'msg' => $message
            );
        }

        return (new Response($response, 200))->header('Content-Type', 'application/json');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function postList(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'userid'          => 'required',
        ]);

        if ($validator->fails()) {
            return $this->make_response(false, 'Please complete the required parameters');
        }

        $user = User::find($request->userid);

        if(null == $user){
            return $this->make_response(false, 'User not found');
        }

        $notifications = Notification::where('user_id', $user->id);

        $order = 'asc';
        if(null !== $request->order_by) {
            if(null !== $request->order) {
                $order = $request->order;
            }

            if(! in_array($request->order, ['asc', 'desc', 'ascending', 'descending'])) {
                return $this->make_response(false, 'Order parameter can only be "asc" or "desc" or "ascending" or "descending"');
            }

            $notifications = $notifications->orderBy($request->order_by, $request->order)->get();
        } else {
            $notifications = $notifications->orderBy('created_at', 'desc')->get();
        }

        if($notifications->count() > 0){

            foreach($notifications as $item) {
                $udetail = User::find($item->user_id);
                $pic = "";

                if($udetail->photo != NULL){
                    if(!is_file($udetail->photo->file)){
                        $pic= url('profile_picture/'.$udetail->photo->file);
                    }
                }else{
                    $pic = "";
                }

                $item->user_detail = array(
                        'name' => $udetail->name,
                        'pic' => $pic
                    );
            }
            

            return $this->make_response(true, 'Notifications are available', $notifications);
        } else {
            return $this->make_response(true, 'Notification empty');
        }

        return $this->make_response(true, 'Notification empty');
    }
}
