<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Sarav\Multiauth\Foundation\AuthenticatesAndRegistersUsers;
use App\Models\userSupplier;
use App\Models\Groceries;
use App\Models\Country;
use Illuminate\Http\Response;
use App\Models\Location\Area;
use App\Models\City;
use App\Models\Province;
use App\Models\Order\Groceries as OrderGroceries;
//use App\Models\User;

class SupplierController extends Controller
{

    private function make_response($status = true, $message = '', $data = null) {
        if(null == $data) {
            $response = array(
                'status' => $status,
                'msg' => $message
            );
        } else {
            $response = array(
                'data' => $data,
                'status' => $status,
                'msg' => $message
            );
        }

        return (new Response($response, 200))
        ->header('Content-Type', 'application/json');
    }

    public function getProducts(Request $request,$supplier_id){
        //print_r(auth()->user('supplier')->user_name);
        //die();
        $supplier_id;
        $groceries = Groceries::where('supplier_id',$supplier_id)->get();

        foreach ($groceries as $grocery) {
            $grocery->rating = $grocery->countRating();
            $grocery->productSold = \   App\Models\Order\Groceries::
                                where('groceries_id',$grocery->id)
                                ->sum('qty');
        }
        $response = array(
            'data' => $groceries,
            'status' => true,
            'msg' => "success"
        );
        return (new Response($response, 200))
        ->header('Content-Type', 'application/json');
    }

    public function postChangePassword(Request $request){
        $old_pass = $request->old_pass;
        $new_pass = $request->new_pass;
        $confirm_pass = $request->confirm_pass;
        $userid = $request->supplier_id;
        if($old_pass && $new_pass && $confirm_pass && $userid){
            $checkuser = DB::table('user_supplier')
                            ->where('id','=',$userid)
                            ->first();
            if($checkuser){
                if(\Hash::check($old_pass, $checkuser->password)){
                    if($new_pass == $confirm_pass){
                        $saved_password = \Hash::make($new_pass);
                        $data_update = array(
                            'password' => $saved_password
                        );
                        $update_pass = DB::table('user_supplier')
                                        ->where('id','=',$userid)
                                        ->update($data_update);
                        if($update_pass){
                            return $this->make_response(true,'Password update success',null);
                        } else {
                            return $this->make_response(false,'Password update failed',null);
                        }
                    } else {
                        return $this->make_response(false,'New Password and Confirm Password doesnt match',null);
                    }
                } else {
                    return $this->make_response(false,'Old password is wrong',null);
                }
            } else {
                return $this->make_response(false,'User not found',null);
            }
        } else {
            return $this->make_response(false,'Please complete parameters',null);
        }
    }

    public function postEditProfile(Request $request){

    }

    public function postMyProfile(Request $request){
            $supplier = userSupplier::where('id',$request->supplier_id)->get()->except(['password','email_verif','userStatus']);
        if(reset($supplier)){
            if($supplier[0]->is_active == 0){
                $response = array(
                    'status' => false,
                    'msg' => "you need to active your account first"
                );
            }else{
                $response = array(
                    'data' => $supplier,
                    'status' => true,
                    'msg' => "success"
                );
            }
        }else{
            $response = array(
                'status' => false,
                'msg' => "user not found"
            );
        }

        return (new Response($response, 200))
        ->header('Content-Type', 'application/json');

    }
}
/*last*/
