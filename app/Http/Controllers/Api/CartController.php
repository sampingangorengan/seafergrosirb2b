<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use App\Models\Groceries;
use App\Models\User;
use App\Models\Cart;
use Validator;

class CartController extends Controller
{
    private function make_response($status = true, $message = '', $data = null) {
        if(null == $data) {
            $response = array(
                'status' => $status,
                'msg' => $message
            );
        } else {
            $response = array(
                'data' => $data,
                'status' => $status,
                'msg' => $message
            );
        }

        return (new Response($response, 200))
        ->header('Content-Type', 'application/json');
    }

    private function check_stock($requested_quantity, $id) {
        $groceries = Groceries::find($id);

        if((int) $groceries->stock >= (int) $requested_quantity) {
            return array(
                'status' => 'OK',
                'code' => 200,
                'message' => 'Item available'
            );
        } else {
            return array(
                'status' => '_ERR',
                'code' => 400,
                'message' => 'There is only '.$groceries->stock.' items remaining.'
            );

        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function postListCart(Request $request)
    {
        $userid = $request->userid;
        if($userid){
            $cart = DB::table('carts')
            ->select('carts.*','groceries.range_price')
            ->leftJoin('groceries','carts.groceries_id','=','groceries.id')
            ->where('user_id','=',$userid)
            ->get();
            if($cart){
                $response = array(
                    'data' => $cart,
                    'status' => true,
                    'msg' => 'Cart is available'
                );
            } else {
                $response = array(
                    'status' => false,
                    'msg' => 'Cart Empty'
                );
            }
        } else {
            $response = array(
                'status' => false,
                'msg' => 'Please complete parameter'
            );
        }
        return (new Response($response, 200))
        ->header('Content-Type', 'application/json');
    }

    public function postAddToCart(Request $request) {

        $validator = Validator::make($request->all(), [
            'userid'          => 'required',
            'groceries_id'    => 'required|numeric',
            'name'            => 'required',
            'qty'             => 'required|numeric',
            'sku'             => 'required',
            'price_per_item'  => 'required|numeric',
            'price'           => 'required|numeric',
            'image'           => 'required',
            'metric'          => 'required'
        ]);

        if ($validator->fails()) {
            return $this->make_response(false, 'Please complete the required parameters', $validator->errors()->toArray());
        }

        if(null != $request->userid) {
            $user = User::find($request->userid);
        } else {
            return $this->make_response(false, 'User not found');
        }

        $availability = $this->check_stock($request->qty, $request->groceries_id);

        if($availability['status'] == '_ERR') {
            return $this->make_response(false, 'Item is out of stock.');
        }

        if($user) {

            if(null == $request->metric) {
                $unit_metric = 'NA';
            } else {
                $unit_metric = $request->metric;
            }

            $cart = Cart::whereUserId($user->id)->whereGroceriesId($request->groceries_id)->first();

            if ( ! is_null($cart)) {

                $cart->qty += $request->qty;

            } else {
                $cart = new Cart;
                $cart->user_id = $user->id;
                $cart->groceries_id =$request->groceries_id;
                $cart->qty = (int) $request->qty;
                $cart->is_read = 0;
                $cart->name = $request->name;
                $cart->sku = $request->sku;
                $cart->price_per_item = $request->price_per_item;
                $cart->unit_metric = $unit_metric;
                $cart->price = (float) $request->price;
                $cart->image = $request->image;
                $cart->quantity = (int) $request->qty;
            }

            $status = $cart->save();

            return $this->make_response(true, 'Success adding item to cart', $cart);
        }
    }

    public function postUpdateQty(Request $request){
        $user_id = $request->userid;
        $product_id = $request->product_id;
        $qty = $request->quantity;

        $check = DB::table('carts')
                ->where('user_id','=',$user_id)
                ->where('groceries_id','=',$product_id);
        if($check->first()){
            if($qty > 0){
                $update = $check->update([
                                'quantity' => $qty,
                                'qty' => $qty,
                                'updated_at' => date('Y-m-d H:i:s')
                            ]);
                $updated_cart = DB::table('carts')
                                ->where('id','=',$check->first()->id)->first();
                if($update){
                    return $this->make_response(true, 'Success update cart item', $updated_cart);
                } else {
                    return $this->make_response(false, 'Failed update cart item');
                }
            } else {
                $delete = DB::table('carts')->where('id','=',$check->first()->id)->delete();
                if($delete){
                    return $this->make_response(true, 'Success remove cart item');
                } else {
                    return $this->make_response(false, 'Failed remove cart item');
                }
            }
        } else {
            return $this->make_response(false, 'Cart item not found');
        }
    }

    public function postDeleteCartItem(Request $request){
        $user_id = $request->userid;
        $product_id = $request->product_id;

        $check = DB::table('carts')
                ->where('user_id','=',$user_id)
                ->where('groceries_id','=',$product_id);
        if($check->first()){
            $delete = DB::table('carts')->where('id','=',$check->first()->id)->delete();
            if($delete){
                return $this->make_response(true, 'Success remove cart item');
            } else {
                return $this->make_response(false, 'Failed remove cart item');
            }
        } else {
            return $this->make_response(false, 'Cart item not found');
        }
    }
}
