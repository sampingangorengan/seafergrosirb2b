<?php

namespace App\Http\Controllers;

use App\Models\ProductSuggestion;
use Illuminate\Http\Request;
use App\Models\SuggestionPicture;
use App\Models\StaticPage\StaticPage;
use App\Models\StaticPage\StaticDescription;
use App\Models\StaticPage\StaticImage;
use App\Models\StaticPage\StaticLink;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Agent;
use Validator;

class ProductSuggestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $descriptions = StaticDescription::where('page_id', 9)->get();
        $description = array();
        foreach($descriptions as $desc){
            $description[$desc->description_position] = $desc;
        }
        $images = StaticImage::where('page_id', 9)->lists('image_name', 'image_location');
        return view('frontend.suggestion', compact('description','images'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return redirect('suggestion');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'product_name'      => 'required',
            'brand_name'        => 'required',
            'category'          => 'required',
            'email'             => 'required|email',
            'file'              => 'mimes:jpeg,bmp,png',
        ]);

        if ($validator->fails()) {
            return redirect('suggestion')
                        ->withErrors($validator)
                        ->withInput();
        }

        if($request->category == 0){
            $request->session()->flash('alert-danger', 'You have to choose the category in order to proceed!');
            return redirect('suggestion');
        }

        $suggest = new ProductSuggestion();
        $suggest->product_name = $request->product_name;
        $suggest->product_brand = $request->brand_name;
        $suggest->email = $request->email;
        $suggest->category_id = $request->category;
        $suggest->comments = $request->comments;



        if($file = $request->file('file')) {
            $name = time().$file->getClientOriginalName();

            $file->move('suggestion_picture', $name);

            $photo = SuggestionPicture::create(['file' => $name]);

            $suggest->suggestion_picture = (int) $photo->id;
        }

        $suggest->save();


        Mail::queue('emails.suggest', compact('suggest'), function ($message) use ($suggest) {
            $message->from('hello@seafermart.co.id', '[NO-REPLY] Kirin from Seafer Grosir');
            $message->to('hello@seafermart.co.id')->subject('Product Suggestion');
        });



        $request->session()->flash('alert-success', 'Product suggestion was successfully submitted!');

        return redirect('suggestion');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
