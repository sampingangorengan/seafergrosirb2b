<?php

namespace App\Http\Controllers;

use App\Models\FreeTaxDelivery;
use App\Models\Groceries;
use App\Models\Promo;
use App\Models\PromoHistory;
use App\Models\User;
use App\Models\Notification;
use App\Models\User\CashbackReward;
use App\Models\User\CashbackPoint;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Libraries\Gosend;
use App\Libraries\Google\Maps as GoogleMaps;
use App\Models\Cart;
use App\Models\Order;
use App\Models\Order\Groceries as OrderGroceries;
use App\Models\Order\Payment;
use App\Models\User\Address;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use LocalizedCarbon;
use Money;
use Carte;
use Agent;

class CheckoutController extends Controller
{
    private $originLatLong = '-6.2083992,106.8213355';

    function __construct(){
        $this->middleware('checkout',['only' => ['getIndex']]);
    }

    private function updateAfterRedeem(Order $order,$value) {

        $cashback = new CashbackReward;
        $cashback->user_id = auth()->user()->id;
        $cashback->order_id = $order->id;
        $cashback->obtain_from = 'redeem';
        $cashback->nominal = $value;
        $status = $cashback->save();

        if($status) {
            $points = CashbackPoint::where('user_id', auth()->user()->id)->first();
            $points->nominal = $points->nominal - $value;
            $stat = $points->save();

            if($stat) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private function determineCODFare() {
        $price = 0;
        $total_weight = 0;
        foreach(Carte::contents(true) as $cart){

            $grocery = Groceries::find($cart['id']);
            if($grocery->value_per_package > 0) {
                if ($grocery->metric->name == 'gram') {
                    $total_weight = $cart['quantity'] * ($grocery->value_per_package * 0.001);
                } elseif($grocery->metric->name == 'mililiter') {
                    $total_weight = $cart['quantity'] * ($grocery->value_per_package * 0.001);
                } else {
                    $total_weight  = $cart['quantity'] * $grocery->value_per_package;
                }
            } else {
                $total_weight = $total_weight + 1;
            }
        }

        $price = $total_weight * 20000;
        return $price;
    }

    private function setGojekDetail($type) {

        $delivery_detail_id = DB::table('gojek_details')->insertGetId(
            [
                'status' => 'not made',
                'booking_type' => $type,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]
        );
        return $delivery_detail_id;
    }

    private function setJneDetail($type) {
        $delivery_detail_id = DB::table('jne_details')->insertGetId(
            [
                'status' => 'not made',
                'booking_type' => $type,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]
        );

        return $delivery_detail_id;
    }

    private function save_user_detail($payload) {
        $confirmation_code = str_random(30);
        $user = new User;
        $user->name = $payload['name'];
        $user->email = $payload['email'];
        $user->password = \Hash::make($payload['password']);
        $user->sex = $payload['sex'];
        $user->dob = $payload['dob'];
        $user->phone_number = $payload['phone_number'];
        $user->is_active = 1;
        $user->registration_code = $payload['confirmation_code'];
        $save_user = $user->save();

        $saved_user = null;
        if ($save_user) {
            $saved_user = User::whereRegistrationCode($payload['confirmation_code'])->first();

            $confirmation_code = $payload['confirmation_code'];
            $user = $saved_user;

            Mail::send('emails.signup', compact('confirmation_code', 'user'), function ($message) use ($user) {

                $message->from('hello@seafermart.co.id', '[NO-REPLY] Kirin from Seafer Grosir');

                $message->to($user->email)->subject('Verify your email address');

            });

            auth()->login($saved_user);


            return $saved_user;
        } else {
            return false;
        }
    }

    private function save_address_detail($payload) {
        $address = new Address;
        $address->user_id = $payload['user_id'];
        $address->name = $payload['name'];
        $address->recipient = $payload['recipient'];
        $address->address = $payload['address'];
        $address->area_id = $payload['area_id'];
        $address->city_id = $payload['city_id'];
        $address->province_id = $payload['province_id'];
        $address->postal_code = $payload['postal_code'];
        $address->phone_number = $payload['phone_number'];
        $save_address = $address->save();

        $saved_address = null;
        if ($save_address) {
            $saved_address = Address::where('user_id', $payload['user_id'])->where('name', $payload['name'])->get();
            return $saved_address;
        } else {
            return false;
        }
    }

    private function checkAddressValidity(Address $address) {

        $client = new Client();

        $str_address = urlencode($address->address)
            .'+'.urlencode($address->area->name)
            . '+' . urlencode($address->city->city_name)
            . '+' . urlencode($address->city->province->province_name_id)
            .'+'.urlencode($address->postal_code);

        $string_address = str_replace(' ', '+', $str_address);

        $url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' . $string_address . '&key=AIzaSyCRDhe-WvUmE8Y96Gb6Glj-CPc9lfjC420';

        $res = $client->request('GET', $url);

        $results = json_decode($res->getBody())->results;

        if(null !== $results && !empty($results)) {
            return $results[0]->geometry->location->lat.', '.$results[0]->geometry->location->lng;
        }

        return false;
    }


    public function getIndex(Request $request)
    {
        if (!auth()->check()) {
            return redirect('/user/sign-in?r=checkout');
        }

        if(count(Carte::contents(true)) < 1) {
            $request->session()->flash('alert-danger', 'Your cart is empty. Let\'s fill it up first.');
            return redirect('/');
        }

        $shippingFee = 0;

        session(compact('shippingFee'));

        $promo_applied = false;
        if(Session::has('is_promo')) {
            $promo = Promo::where('code', Session::get('is_promo'))->first();

            if ($promo->promo_type == 'percent') {
                $promo_applied = (Carte::total() * $promo->discount_value) / 100;
            } else {
                $promo_applied = $promo->discount_value;
            }
        }


        $total = Carte::total() + (float) $shippingFee ;

        # free delivery notice
        $free_delivery = FreeTaxDelivery::isFreeDeliveryApplied($total);

        if($free_delivery) {
            $request->session()->flash('alert-info', 'You have a chance to get free shipping fee on this order. Hurry up to finish your order now! This offer is time limited.');
        }


        if($promo_applied != false)
            $total = $total - $promo_applied;

        if($total < 0){
            $total = 0;
        }

        return view('frontend.order.checkout', compact('shippingFee', 'total', 'ppn', 'promo_applied','free_delivery'));

    }

    public function postIndex(Request $request)
    {
        # define address, shipping method and shipping fee
        $delivery_date = request('delivery_date');
        # validate that shipping method and address is filled properly
        $mes = "";

        $user = auth()->user();

        if(request('paymt_opt') == "tempo"){
            if($user->tempo == null || $user->tempo->status != "approved"){
                $request->session()->flash('alert-danger', 'You havent been approved for tempo payment');
                return back();
            }
        }
        if(!$delivery_date){
            $request->session()->flash('alert-danger', 'Please select Delivery Date');
            return back();
        } else {
            # check if the address exist
            $this->validate($request, [
                'address_id' => 'required|exists:user_addresses,id,user_id,'.auth()->user()->id,
            ]);
            # check if there is promo applied
            $promo_applied = 0;
            $use_promo = 0;
            if(Session::has('is_promo')) {
                $promo = Promo::where('code', Session::pull('is_promo'))->first();

                $use_promo = $promo->id;

                if ($promo->promo_type == 'percent') {
                    $promo_applied = ( (Carte::total()) * $promo->discount_value) / 100;
                } else {
                    $promo_applied = $promo->discount_value;
                }

                $promo->quota = $promo->quota - 1;
                $promo->quota_used = $promo->quota_used + 1;

                $promo->save();
            }



            $unique_code = (int) rand(11,99);



            # calculate total price before shipping
            $total_before_shipping = Carte::total()  - $promo_applied;
            if($total_before_shipping < 0){
                $total_before_shipping = 0;
            }

            $shipping_fee = 0;
            # if free delivery is on, then make shipping fee 0
            if(FreeTaxDelivery::isFreeDeliveryApplied($total_before_shipping)) {
                $shipping_fee = 0;
                $free_shipping = FreeTaxDelivery::where('type', 'delivery')->first();
                $free_shipping->remaining = $free_shipping->remaining - 1;
                $free_shipping->usage = $free_shipping->usage + 1;
                $free_shipping->save();
            }

            # total after shipping fee and unique code
            $total = $total_before_shipping + $shipping_fee + $unique_code;

            if($total < 0) {
                $total = 0;
            }


            // Insert to "orders" table
            $ord_id = DB::table('orders')->insertGetId(
                [
                    'user_address_id' => request('address_id'),
                    'user_id' => auth()->user()->id,
                    'shipping_service_code' => "internal",
                    'shipping_fee' => $shipping_fee,
                    'order_status' => 1,
                    'is_using_promo' => $use_promo,
                    'nett_total' => (double) Carte::total(),
                    'unique_code' => $unique_code,
                    'redeem' => 0,
                    'tax' => 0,
                    'promo_value' => $promo_applied,
                    'grand_total' => (double) $total,
                    'delivery_date' => request('delivery_date'),
                    'delivery_notes' => request('delivery_note') ?? "",
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ]
            );

            $default_payment = new Payment;
            $default_payment->order_id = $ord_id;
            $default_payment->method = request('paymt_opt');
            $default_payment->transfer_dest_account = 'Not Available';
            $default_payment->transfer_sender_account_number = 'Not Available';
            $default_payment->transfer_sender_account_name = 'Not Available';
            $default_payment->transfer_date = date('Y-m-d');
            $default_payment->transfer_time = '00:00:01';
            $default_payment->transfer_note = 'Not Available';
            $default_payment->transfer_receipt_file = '';
            $default_payment->is_approved = 0;
            $default_payment->transfer_amount = 0;
            $default_payment->transfer_sender_bank_name = 'Not Available';
            $default_payment->payment_status_id = 0;
            $default_payment->notify_admin = 0;
            $default_payment->user_confirmed = 0;
            $default_payment->save();



            if($use_promo != 0) {
                $promo = Promo::find($use_promo);

                $promo_history = new PromoHistory;
                $promo_history->user_id = auth()->user()->id;
                $promo_history->order_id = $ord_id;
                $promo_history->promo_id = $promo->id;
                $promo_history->promo_code = $promo->code;
                $promo_history->save();
            }

            $isMeasured = 1;

            // Insert to "order_groceries" table
            foreach (Carte::contents() as $cartGroceries) {

                if(null == Groceries::find($cartGroceries->id)) {
                    $product = Groceries::where('price', $cartGroceries->price)->where('sku', $cartGroceries->sku)->where('name', $cartGroceries->name)->first();
                    $orderGroceries = new OrderGroceries;
                    $orderGroceries->order_id = $ord_id;
                    $orderGroceries->groceries_id = $product->id;
                    $orderGroceries->price = $cartGroceries->price;
                    $orderGroceries->qty = $cartGroceries->quantity;
                    $orderGroceries->save();

                    if($product->range_price != null && $product->range_price != ""  && $product->range_price != " " && $product->range_price != 0){
                        $isMeasured = 0;
                    }
                } else {
                    $orderGroceries = new OrderGroceries;
                    $orderGroceries->order_id = $ord_id;
                    $orderGroceries->groceries_id = $cartGroceries->id;
                    $orderGroceries->price = $cartGroceries->price;
                    $orderGroceries->qty = $cartGroceries->quantity;
                    $orderGroceries->save();
                    $product = Groceries::find($cartGroceries->id);
                    if($product->range_price != null && $product->range_price != ""  && $product->range_price != " " && $product->range_price != 0){
                        $isMeasured = 0;
                    }
                }

                // update stock and quantity
                $update_stock_sold = Groceries::find($cartGroceries->id);
                $update_stock_sold->stock = $update_stock_sold->stock - $cartGroceries->quantity;
                $update_stock_sold->sold = $update_stock_sold->sold + $cartGroceries->quantity;
                $update_stock_sold->save();

            }

            // Clean up "carts" table
            Carte::destroy();
            $cart_removal = DB::table('carts')->where('user_id', auth()->user()->id)->delete();

            $order = Order::find($ord_id);
            $order->isMeasured = $isMeasured;
            $order->save();


            //$redeem = $this->updateAfterRedeem($order, $redeem_value);


            $order->deliveries()->create([
                'delivery_type' => "",
                'detail_id' => NULL,
                'status' => 'not made',
                'order_id' => $order->id
            ]);


            if(Session::has('is_promo')) {
                Session::forget('is_promo');
            }

            $notif = new Notification;
            $notif->user_id = $user->id;
            $notif->title = 'New Order Made';
            $notif->description = 'Pesanan anda berhasil dibuat';
            $notif->link = url('orders/'.$order->id);
            $notif->is_read = 0;
            $notif->save();

            // Set Payment confirmation Message
            if(request('paymt_opt') == "cod"){
                $mes .=
                "<p>mohon melakukan pembayaran secara tunai saat pengiriman pesanan Anda telah tiba.</p>";
            }else{
                $mes = "<br/>
                    <p><strong>Mohon melakukan pembayaran sesuai berikut:</strong></p>
                    <ol style='text-align:left;'>
                        <li>Silakan lakukan pembayaran Anda ke akun berikut: PT. Seafermart Jaya Raya - BCA 206 3232 327 / Bank Panin 1535007529</li>
                        <li>Ambil gambar bukti pembayaran Anda dan unggah ke formulir Konfirmasi Pembayaran.</li>
                        <li>Seafer Grosir akan memberi tahu Anda setelah pembayaran dilakukan.</li>
                        <li>Seafer Grosir akan mengkonfirmasi pembayaran Anda segera dalam 24 jam dan memproses pesanan Anda.</li>
                        <li>Kami berterima kasih atas pembayaran tepat waktu dan bertanggung jawab Anda.</li>
                    </ol>";

                if(request('paymt_opt') == "tempo"){
                    $tempoPaymentDate =  LocalizedCarbon::now()->addDays($user->tempo->duration);

                    $mes .= "<br>";

                    $mes .= "<p style='color:#ff0335 !important'>Pembayaran anda akan jatuh tempo pada <b>".$tempoPaymentDate->format("d F Y H:m:s")."</b>.</p>";

                }else{
                    $tempoPaymentDate =  LocalizedCarbon::now()->addDays(1);

                    $mes .= "<br>";

                    $mes .= "<p style='color:#ff0335 !important'>Lakukan Pembayaran transfer anda sebelum <b>".$tempoPaymentDate->format("d F Y H:m:s")."</b>.</p>";

                }

                if(!$isMeasured){
                    $mes .= "<br>";
                    $mes .= "<p style='color:#ff0335 !important'>Harga pesanan anda akan segera kami update setelah melakukan penimbangan pada beberapa pesanan anda.</p>";
                }
            }
            //
            // //Mail
            try{
                Mail::queue('emails.order_done', compact('user','mes','order'), function ($message) use ($user,$order) {

                    $message->from('hello@seafermart.co.id', '[NO-REPLY] Pesanan Seafer Grosir Anda Order #'.$order->id.' Telah di Proses!');

                    $message->to($user->email);

                });
            }catch(Exception $e) {

            }

            $order_number = $ord_id;
            try{
                Mail::queue('emails.admin_order_cod', compact('user','order_number'), function ($message) use ($user) {

                    $message->from('hello@seafermart.co.id', '[NO-REPLY] New COD Order');

                    $message->to('hello@seafermart.co.id');
                });
            }catch(Exception $e) {

            }


            return view('frontend.thankyou', compact('order','unique_code','mes'));

        }
    }


    public function getGuestCheckout(Request $request, $email) {

        $user_email = urldecode($email);
        $user = User::whereEmail($user_email)->first();

        if($user) {
            $request->session()->flash('alert-danger', 'This email has been taken. Please use forgot password to reset your password.');
            return back();
        }

        $shippingFee = 15000;

        $promo_applied = false;
        if(Session::has('is_promo')) {
            $promo = Promo::where('code', Session::get('is_promo'))->first();

            if ($promo->promo_type == 'percent') {
                $promo_applied = (Carte::total() * $promo->discount_value) / 100;
            } else {
                $promo_applied = $promo->discount_value;
            }
        }

        $ppn = 0;

        $total = Carte::total() + $shippingFee ;

        if($promo_applied != false)
            $total = $total - $promo_applied;


        return view('frontend.order.checkout', compact('promo_applied', 'ppn', 'total', 'shippingFee', 'user_email'));
    }





    public function postGuestCheckout(Request $request) {

        $this->validate($request, [
            'user_first_name'      => 'required',
            'user_last_name'       => 'required',
            'email'           => 'required|unique:users|email',
            'user_phone_number'     => 'required|numeric',
            'dob_date'        => 'required',
            'dob_month'       => 'required',
            'dob_year'        => 'required',
            'signup_password'      => 'required|between:8,12|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9]).*$/',
            'user_phone_number'    => 'numeric',
            'sex'                  => 'required',
            "address_name"          => "required",
            "address_recipient"     => "required",
            "address_phone_number"  => "numeric",
            "address_address"       => "required",
            "address_province_id"   => "required",
            "address_city_id"       => "required",
            "address_area_id"       => "required",
            "address_postal_code"   => "required"
        ]);

        $confirmation_code = str_random(30);

        $user_payload = array(
            'name' => $request->user_first_name.' '.$request->user_last_name,
            'email' => $request->email,
            'password' => \Hash::make($request->signup_password),
            'sex' => $request->sex,
            'dob' => $request->dob_year.'-'.$request->dob_month.'-'.$request->dob_date,
            'phone_number' => $request->user_phone_number,
            'confirmation_code' => $confirmation_code
        );

        $user = $this->save_user_detail($user_payload);


        $address_payload = array(
            'user_id'       => $user->id,
            'name'          => $request->address_name,
            'recipient'     => $request->address_recipient,
            'address'       => $request->address_address,
            'province_id'   => $request->address_province_id,
            'city_id'       => $request->address_city_id,
            'area_id'       => $request->address_area_id,
            'postal_code'   => $request->address_postal_code,
            'phone_number'  => $request->address_phone_number,
        );

        $address = $this->save_address_detail($address_payload);

        return redirect('checkout');

    }


    public function postNewAddress(Request $request)
    {

        $address = new Address;
        $address->user_id = auth()->user()->id;
        $address->recipient_title = request('recipient_title');
        $address->recipient = request('recipient');
        $address->name = request('name');
        $address->address = request('address');
        $address->area_id = request('area_id');
        $address->city_id = request('city_id');
        $address->province_id = request('province_id');
        $address->postal_code = request('postal_code');
        $address->phone_number = request('phone_number');

        if(request('coordinates') == 0) {
            $address->coordinates = NULL;
            $addr_stat = $this->checkAddressValidity($address);

            if($addr_stat) {
                $address->coordinates = $addr_stat;
            }
        } else {
            $address->coordinates = str_replace(')','', str_replace('(', '', request('coordinates')));
        }

        $address->save();

        return redirect('checkout');
    }

    public function postAjaxChangeAddressId()
    {
        $address = Address::find(request('address_id'));

        $addressText = $address->area->name
            .'+'.$address->area->city->city_name
            .'+'.$address->area->city->province->province_name_id
            .'+'.$address->postal_code;
        $addressText = str_replace(' ', '+', $addressText);

        /*$destLatLong = GoogleMaps::getLatLong($addressText);
        if ($destLatLong === false) {
            $status = false;
            return compact('status');
        }*/

        $shippingFee = 0;//Gosend::getFee($this->originLatLong, $destLatLong, 'SameDay');

        // Put to session
        session(compact('shippingFee'));

        /*$total = Money::display(Cart::getTotal(auth()->user()->id, true) + $shippingFee);*/
        $promo_applied = 0;
        if(Session::has('is_promo')) {
            $promo = Promo::where('code', Session::get('is_promo'))->first();

            if ($promo->promo_type == 'percent') {
                $promo_applied = (Carte::total() * $promo->discount_value) / 100;
            } else {
                $promo_applied = $promo->discount_value;
            }
        }

        $redeem_value = 0;
        if(Session::has('is_redeem')) {
            $redeem_value  = Session::get('is_redeem');
        }

        $ppn = 0;

        $total = Carte::total() - $promo_applied - $redeem_value + $shippingFee + $ppn;

        $total = Money::display($total);
        $shippingFee = Money::display($shippingFee);
        $status = true;

        return compact('shippingFee', 'total', 'status');
    }

    public function postAjaxChangeShippingServiceCode()
    {
        $address = Address::find(request('address_id'));

        $addressText = $address->area->name
            .'+'.$address->area->city->name
            .'+'.$address->area->city->province->name
            .'+'.$address->postal_code;
        $addressText = str_replace(' ', '+', $addressText);

        $destLatLong = GoogleMaps::getLatLong($addressText);
        if ($destLatLong === false) {
            $status = false;
            return compact('status');
        }

        switch (request('shipping_service_code')) {
            case 'gosend_same_day':
                $serviceName = 'SameDay';
                break;
            case 'gosend_instant':
                $serviceName = 'Instant';
                break;
            default:
                $serviceName = 'SameDay';
                break;
        }

        $shippingFee = Gosend::getFee($this->originLatLong, $destLatLong, $serviceName);

        // Put to session
        session(compact('shippingFee'));

        /*$total = Money::display(Cart::getTotal(auth()->user()->id, true) + $shippingFee);*/
        $promo_applied = false;
        if(Session::has('is_promo')) {
            $promo = Promo::where('code', Session::get('is_promo'))->first();

            if ($promo->promo_type == 'percent') {
                $promo_applied = (Carte::total() * $promo->discount_value) / 100;
            } else {
                $promo_applied = $promo->discount_value;
            }
        }

        $ppn = 0;

        $total = Carte::total() + $shippingFee + $ppn;

        if(FreeTaxDelivery::isFreeDeliveryApplied($total - $ppn)) {
            $shippingFee = 0;
        }

        if($promo_applied != false)
            $total = $total - $promo_applied;

        $total = Money::display($total + $shippingFee);
        $shippingFee = Money::display($shippingFee);
        $status = true;

        return compact('shippingFee', 'total', 'status');
    }

    public function postCalculateShippingFare(Request $request){
        $type = $request->shipping_type;
        $address_id = $request->address_id;

        $address = Address::find($address_id);

        $redeem_value = 0;
        if(Session::has('is_redeem')) {
            $redeem_value = Session::get('is_redeem');
        }



        $promo_applied = 0;
        if(Session::has('is_promo')) {
            $promo = Promo::where('code', Session::get('is_promo'))->first();

            if ($promo->promo_type == 'percent') {
                $promo_applied = ( Carte::total() * $promo->discount_value) / 100;
            } else {
                $promo_applied = $promo->discount_value;
            }
        }

        if(null === $address) {
            $ppn = 0;
            $total = Money::display(Carte::total()-$redeem_value-$promo_applied+$ppn);
            $shippingFee = 0;
            $status = false;

            return compact('shippingFee', 'total', 'status');
        }

        $shipping_fee = 20000;

        $free_delivery = FreeTaxDelivery::isFreeDeliveryApplied(Carte::total() - $redeem_value - $promo_applied);

        $type_by_vendor = explode('_', $type);

        if($type_by_vendor[0] == 'jne') {

            $city = $address->city->city_name;
            $phrase = explode(' ', $city);
            $client = new Client([
                // Base URI is used with relative requests
                'base_uri' => 'http://api.jne.co.id:8889/',
                // You can set any number of default request options.
                'timeout'  => 2.0,
            ]);
            $body_array = array('username' => env('JNE_USERNAME'), 'api_key' => env('JNE_API_KEY') );
            $body = \GuzzleHttp\json_encode($body_array);

            $res = $client->request('POST', '/tracing/seafermart/origin/key/'.strtolower($phrase[0]), [
                'form_params' => [
                    'username' => env('JNE_USERNAME'),
                    'api_key' => env('JNE_API_KEY'),
                ]
            ]);

            if ($res->getReasonPhrase() == "OK") {
                $city_detail = json_decode($res->getBody())->detail;
            } else {
                $city_detail = false;
            }


            if(false !== $city_detail) {

                $res = $client->request('POST', '/tracing/seafermart/price/', [
                    'form_params' => [
                        'username' => env('JNE_USERNAME'),
                        'api_key' => env('JNE_API_KEY'),
                        'from' => 'CGK10000',
                        'thru' => $city_detail[0]->code,
                        'weight' => 1
                    ]
                ]);

                if($res->getReasonPhrase() == "OK"){
                    $all_result = json_decode($res->getBody());
                    $price_result = $all_result->price;

                    if($type_by_vendor[1] == 'oke') {
                        $ppn = 0;
                        if($ppn < 0){
                            $ppn = 0;
                        }
                        $total_before_shipping = Carte::total() - $redeem_value + $ppn - $promo_applied;
                        if($total_before_shipping < 0) {
                            $total_before_shipping = 0;
                        }


                        if($free_delivery) {
                            $shippingFee = Money::display(0);
                            $total = Money::display($total_before_shipping);
                        } else {
                            $total = $total_before_shipping + floatval($price_result[3]->price);
                            $shippingFee = Money::display(floatval($price_result[3]->price));
                        }

                        $status = true;

                        if($total < 0) {
                            $total = 0;
                        }
                        $total = Money::display($total);

                        return compact('shippingFee', 'total', 'status');
                    } elseif ($type_by_vendor[1] == 'yes') {
                        $ppn = ceil(0.1 * (Carte::total() - $redeem_value - $promo_applied));
                        if($ppn < 0){
                            $ppn = 0;
                        }
                        $total_before_shipping = Carte::total() - $redeem_value + $ppn - $promo_applied;
                        if($total_before_shipping < 0) {
                            $total_before_shipping = 0;
                        }

                        if($free_delivery) {
                            $shippingFee = Money::display(0);
                            $total = Money::display($total_before_shipping);
                        } else {
                            $total = $total_before_shipping + floatval($price_result[4]->price);
                            $shippingFee = Money::display(floatval($price_result[4]->price));
                        }

                        $status = true;
                        if($total < 0) {
                            $total = 0;
                        }
                        $total = Money::display($total);

                        return compact('shippingFee', 'total', 'status');
                    } elseif ($type_by_vendor[1] == 'reg') {
                        $ppn = 0;
                        if($ppn < 0){
                            $ppn = 0;
                        }
                        $total_before_shipping = Carte::total() - $redeem_value + $ppn - $promo_applied;
                        if($total_before_shipping < 0) {
                            $total_before_shipping = 0;
                        }

                        if($free_delivery) {
                            $total = Money::display($total_before_shipping);
                            $shippingFee = Money::display(0);
                        } else {
                            $total = $total_before_shipping + floatval($price_result[2]->price);
                            $shippingFee = Money::display(floatval($price_result[2]->price));
                        }
                        $status = true;
                        if($total < 0) {
                            $total = 0;
                        }
                        $total = Money::display($total);

                        return compact('shippingFee', 'total', 'status');
                    }
                }
            }
        } elseif ($type_by_vendor[0] == 'gosend') {
            $ppn = 0;
            if($ppn < 0){
                $ppn = 0;
            }
            $total_before_shipping = Carte::total() - $redeem_value + $ppn - $promo_applied;
            if($total_before_shipping < 0) {
                $total_before_shipping = 0;
            }

            if($free_delivery) {
                $shippingFee = Money::display(0);
                $total = Money::display($total_before_shipping);
            } else {
                $total = Money::display($total_before_shipping + floatval(15000));
                $shippingFee = Money::display(floatval(15000));
            }

            $status = true;

            if($total < 0) {
                $total = 0;
            }

            return compact('shippingFee', 'total', 'status');
        } else {
            $ppn = 0;
            if($ppn < 0){
                $ppn = 0;
            }

            $total_before_shipping = Carte::total() - $redeem_value + $ppn - $promo_applied;
            if($total_before_shipping < 0) {
                $total_before_shipping = 0;
            }

            if($free_delivery) {
                $shippingFee = Money::display(0);
                $total = Money::display($total_before_shipping);
            } else {
                $total = Money::display($total_before_shipping + $this->determineCODFare());
                $shippingFee = Money::display($this->determineCODFare());
            }

            $status = true;

            if($total < 0) {
                $total = 0;
            }

            return compact('shippingFee', 'total', 'status');
        }
    }
}
