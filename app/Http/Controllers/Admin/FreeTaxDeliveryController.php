<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\FreeTaxDelivery;
use Validator;

class FreeTaxDeliveryController extends Controller
{
    protected $active_menu = null;
    protected $active_submenu = null;

    public function getActiveMenu($active_menu) {
        $this->active_menu = $active_menu;

        return $this->active_menu;
    }

    public function getActiveSubmenu($active_menu) {
        $this->active_menu = $active_menu;

        return $this->active_menu;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));

        if (!auth()->check())
            return redirect('/admin/login');

        if (auth()->user()->role != 1)
            return redirect('/admin/login');

        $freebie = FreeTaxDelivery::where('type', 'delivery')->first();

        return view('admin.promos.show_free_tax_delivery', compact('active_menu', 'active_submenu', 'freebie'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        //
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));

        if (!auth()->check())
            return redirect('/admin/login');

        if (auth()->user()->role != 1)
            return redirect('/admin/login');

        $freebie = FreeTaxDelivery::where('type', 'delivery')->first();

        return view('admin.promos.edit_free_tax_delivery', compact('active_menu', 'active_submenu', 'freebie'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'minimum_spending'  => 'required',
            'start_date'     => 'required',
            'end_date'      => 'required',
            'is_active'=> 'required',
            'quota' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect('admin/freebies/'.$id.'/edit')
                        ->withErrors($validator)
                        ->withInput();
        }

        $freebie = FreeTaxDelivery::find($id);

        $input = $request->all();

        $input['remaining'] = $input['quota'] - $freebie->usage;
        $input['is_active'] = (int) $input['is_active'];

        $status = $freebie->update($input);

        if($status) {
            $request->session()->flash('alert-success', 'Free delivery settings was successfully updated!');
        } else {
            $request->session()->flash('alert-danger', 'Oops, something is wrong during updating free delivery settings!');

            return back();
        }


        return redirect('admin/freebies');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}


