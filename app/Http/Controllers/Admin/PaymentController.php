<?php

namespace App\Http\Controllers\Admin;

use App\Models\Order;
use App\Models\PaymentProof;
use App\Models\Order\Payment;
use App\Models\User;
use App\Models\User\CashbackPoint;
use App\Models\User\CashbackReward;
use App\Models\Notification;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class PaymentController extends Controller
{
    protected $active_menu = null;
    protected $active_submenu = null;

    private function cash_to_point($cash, $user_id, $additional = null) {
        $user_destination = User::find($user_id);

        if(null !== $additional && isset($additional['order_id'])) {
            $order = Order::find($additional['order_id']);
            $cbr = new CashbackReward;
            $cbr->user_id = $user_destination->id;
            $cbr->order_id = $order->id;
            $cbr->obtain_from = $additional['reason'];
            $cbr->nominal = floatval($cash);
            $cbr->save();

            $user_point = CashbackPoint::where('user_id', $user_destination->id)->first();

            if (null == $user_point) {
                $calc = new CashbackPoint;
                $calc->user_id = $user_destination->id;
                $calc->nominal = floatval($cash);
                $calc->save();
            } else {
                DB::table('cashback_points')->where('user_id', $user_destination->id)->update(['nominal' => floatval($cash)+$user_point['nominal'], 'updated_at' => date('Y-m-d H:i:s')]);
            }
            return true;
        }
    }

    private function process_referral($order) {
        $user_order = User::find($order->user_id);
        $user_referral = User::find($user_order->refer_from);

        $gt = $order->nett_total;

        if ($gt >= 100000) {
            $cb = ($gt - ($gt % 100000)) * 0.005;

            $cbr = new CashbackReward;
            $cbr->user_id = $user_referral->id;
            $cbr->order_id = $order->id;
            $cbr->obtain_from = 'referral';
            $cbr->nominal = $cb;
            $cbr->save();

            $user_point = CashbackPoint::where('user_id', $user_referral->id)->first();

            if (null == $user_point) {
                $calc = new CashbackPoint;
                $calc->user_id = $user_referral->id;
                $calc->nominal = $cb;
                $calc->save();
            } else {
                DB::table('cashback_points')->where('user_id', $user_referral->id)->update(['nominal' => $cb+$user_point['nominal'], 'updated_at' => date('Y-m-d H:i:s')]);
            }
            return true;

        }
        return true;
    }

    private function process_cashback($order) {
        $gt = $order->nett_total;

        if ($gt >= 100000) {
            $cb = ($gt - ($gt % 100000)) * 0.005;

            $cbr = new CashbackReward;
            $cbr->user_id = $order->user_id;
            $cbr->order_id = $order->id;
            $cbr->obtain_from = 'order';
            $cbr->nominal = $cb;
            $cbr->save();

            $user_point = CashbackPoint::where('user_id', $order->user_id)->first();

            if (null == $user_point) {
                $calc = new CashbackPoint;
                $calc->user_id = $order->user_id;
                $calc->nominal = $cb;
                $calc->save();
            } else {
                DB::table('cashback_points')->where('user_id', $order->user_id)->update(['nominal' => $cb+$user_point['nominal'], 'updated_at' => date('Y-m-d H:i:s')]);
            }

            return true;

        }
        return true;
    }

    private function sendEmailToUser(Payment $payment, $input_array) {

        $data = array(
            'name' => $payment->order->user->name,
            'email' => $payment->order->user->email,
            'order' => $payment->order->id,
            'msg' => $input_array['message'],
            'subject' => $input_array['subject']
        );

        try{
            Mail::send('emails.general', $data, function ($message) use ($payment, $input_array) {

                $message->from("hello@seafermart.co.id", "Seafer Grosir Payment Notice");

                $message->to($payment->order->user->email)->subject(ucfirst($input_array['subject']));

            });
            return true;
        } catch (\Exception $exception) {
            return false;
        }
    }

    private function conditionOneHandler($params = array()) {
        $payment = Payment::find($params['payment_id']);

        $payment->is_approved = 1;
        $payment->payment_status_id = 2;
        $status = $payment->save();

        if($status) {
            $order = Order::find($payment->order_id);
            $order->order_status = 7;
            $stat = $order->save();

            $notif = new Notification;
            $notif->user_id = $payment->order->user->id;
            $notif->title = 'Order Status Update';
            $notif->description = 'Your payment for order #'.$order->id.' has been confirmed';
            $notif->link = url('orders/'.$order->id);
            $notif->is_read = 0;
            $notif->save();

            $data = array(
                'name' => $payment->order->user->name,
                'email' => $payment->order->user->email,
                'order' => $order->id
            );

            try{
                $status = Mail::queue('emails.payment_process.payment_confirmed', $data, function ($message) use ($data,$order) {

                    $message->from("hello@seafermart.co.id", "Seafer Grosir Inquiry");
                    $message->to($data['email'])->subject("Pembayaran Seafer Grosir Order #".$order->id." telah berhasil dan dikonfirmasi!");

                });
            } catch (\Exception $exception) {
                Session::flash('alert-danger', 'Failed to send your success email. Please try again in a few moments!');

                return redirect('admin/orders/'.$payment->order_id);
            }

            $cashback = $this->process_cashback($order);

            if(null !== $order->user()->first()->refer_from){
                $this->process_referral($order);
            }

            if($stat && $cashback) {
                Session::flash('alert-success', 'Transaction was successfully approved!');
            } else {
                Session::flash('alert-danger', 'Oops something\'s wrong during approving transaction.');
            }
        } else {
            Session::flash('alert-danger', 'Oops something\'s wrong during approving transaction.');
        }

        return redirect('admin/orders/'.$payment->order_id);
    }

    private function conditionTwoHandler($params = array()) {
        $payment = Payment::find($params['payment_id']);

        $data = array(
            'subject' => $params['subject'],
            'message' => $params['message'],
            'remaining_amount' => $params['remaining_amount']
        );

        $status = $this->sendEmailToUser($payment, $data);

        $notif = new Notification;
        $notif->user_id = $payment->order->user->id;
        $notif->title = 'Order Status Update';
        $notif->description = 'You miss your unique code amount in payment for order #'.$payment->order_id;
        $notif->link = url('orders/'.$payment->order_id);
        $notif->is_read = 0;
        $notif->save();

        # Set payment status to delay approval
        $payment->payment_status_id = 2;
        $payment->save();

        # Set order status to awaiting payment
        $order = $payment->order;
        $order->order_status = 7;
        $order->save();

        Session::flash('alert-success', 'Transaction was successfully approved!');

        return redirect('admin/orders/'.$payment->order_id);
    }

    private function conditionThreeHandler($params = array()) {
        $payment = Payment::find($params['payment_id']);

        $data = array(
            'name' => $payment->order->user->name,
            'email' => $payment->order->user->email,
            'order' => $payment->order_id
        );

        $notif = new Notification;
        $notif->user_id = $payment->order->user->id;
        $notif->title = 'Order Status Update';
        $notif->description = 'Your payment for order #'.$payment->order_id.' has been confirmed';
        $notif->link = url('orders/'.$payment->order_id);
        $notif->is_read = 0;
        $notif->save();

        try{
            $status = Mail::send('emails.payment_process.payment_confirmed', $data, function ($message) use ($data) {

                $message->from("hello@seafermart.co.id", "Seafer Grosir Inquiry");
                $message->to($data['email'])->subject("Payment Confirmed");

            });
        } catch (\Exception $exception) {
    
            Session::flash('alert-danger', 'Failed to send your success email. Please try again in a few moments!');

            return redirect('admin/orders/'.$payment->order_id);
        }



        unset($data);

        $data = array(
            'subject' => $params['subject'],
            'message' => $params['message'],
            'remaining_amount' => $params['remaining_amount']
        );

        $status = $this->sendEmailToUser($payment, $data);
        $payment->is_approved = 1;
        $payment->payment_status_id = 2;
        $status = $payment->save();

        if($status) {
            $order = Order::find($payment->order_id);
            $order->order_status = 7;
            $stat = $order->save();

            $cashback = $this->process_cashback($order);

            if(null !== $order->user()->first()->refer_from){
                $this->process_referral($order);
            }

            $stat = $this->cash_to_point(floatval($params['remaining_amount']), $order->user_id, array('order_id' => $order->id, 'reason' => 'remaining change'));

            if($stat && $cashback) {
                Session::flash('alert-success', 'Transaction was successfully approved!');
            } else {
                Session::flash('alert-danger', 'Oops something\'s wrong during sending email and approving transaction.');
            }
        } else {
            Session::flash('alert-danger', 'Oops something\'s wrong during approving transaction.');
        }

        return redirect('admin/orders/'.$payment->order_id);
    }

    private function conditionFourHandler($params = array()) {
        $payment = Payment::find($params['payment_id']);

        $data = array(
            'subject' => $params['subject'],
            'message' => $params['message'],
            'remaining_amount' => $params['remaining_amount']
        );
        $status = $this->sendEmailToUser($payment, $data);

        $notif = new Notification;
        $notif->user_id = $payment->order->user->id;
        $notif->title = 'Order Status Update';
        $notif->description = 'Your payment for order #'.$payment->order_id.' has been rejected';
        $notif->link = url('orders/'.$payment->order_id);
        $notif->is_read = 0;
        $notif->save();

        # Set payment status to 3
        $payment->is_approved = 0;
        $payment->payment_status_id = 3;
        $payment->save();

        # Set order status to awaiting payment
        $order = $payment->order;
        $order->order_status = 5;
        $order->save();

        $stat = $this->cash_to_point(floatval($params['remaining_amount']), $order->user_id, array('order_id' => $order->id, 'reason' => 'remaining change'));

        if($stat) {
            Session::flash('alert-success', 'Transaction was successfully rejected!');
        } else {
            Session::flash('alert-danger', 'Oops something\'s wrong during sending email and rejecting transaction.');
        }

        return redirect('admin/orders/'.$payment->order_id);
    }

    private function conditionFiveHandler($params = array()) {
        $payment = Payment::find($params['payment_id']);

        $data = array(
            'subject' => $params['subject'],
            'message' => $params['message'],
            'remaining_amount' => $params['remaining_amount']
        );
        $status = $this->sendEmailToUser($payment, $data);

        $notif = new Notification;
        $notif->user_id = $payment->order->user->id;
        $notif->title = 'Order Status Update';
        $notif->description = 'Your payment for order #'.$payment->order_id.' has been rejected';
        $notif->link = url('orders/'.$payment->order_id);
        $notif->is_read = 0;
        $notif->save();

        # Set payment status to 3
        $payment->is_approved = 0;
        $payment->payment_status_id = 3;
        $payment->save();

        # Set order status to awaiting payment
        $order = $payment->order;
        $order->order_status = 6;
        $stat = $order->save();

        if($stat) {
            Session::flash('alert-success', 'Transaction was successfully rejected!');
        } else {
            Session::flash('alert-danger', 'Oops something\'s wrong during sending email and rejecting transaction.');
        }

        #$stat = $this->cash_to_point(floatval($params['remaining_amount']), $order->user_id, array('order_id' => $order->id, 'reason' => 'remaining change'));

        return redirect('admin/orders/'.$payment->order_id);
    }

    private function conditionSixHandler($params = array()) {
        $payment = Payment::find($params['payment_id']);

        $data = array(
            'subject' => $params['subject'],
            'message' => $params['message'],
            'remaining_amount' => $params['remaining_amount']
        );
        $status = $this->sendEmailToUser($payment, $data);

        $notif = new Notification;
        $notif->user_id = $payment->order->user->id;
        $notif->title = 'Order Status Update';
        $notif->description = 'Your order #'.$payment->order_id.' has been cancelled by admin';
        $notif->link = url('orders/'.$payment->order_id);
        $notif->is_read = 0;
        $notif->save();

        $payment->transfer_note = $payment->transfer_note.' - [ADMIN]: '.$params['message'];
        $payment->is_approved = 0;
        $payment->payment_status_id = 3;

        $status = $payment->save();

        if($status) {
            $order = Order::find($payment->order_id);
            $order->order_status = 5;
            $stat = $order->save();

            $stat = $this->cash_to_point(floatval($params['remaining_amount']), $order->user_id, array('order_id' => $order->id, 'reason' => 'remaining change'));

            if($stat) {
                Session::flash('alert-success', 'Transaction was successfully cancelled!');
            } else {
                Session::flash('alert-danger', 'Oops something\'s wrong during cancelling transaction.');
            }

        } else {
            Session::flash('alert-danger', 'Oops something\'s wrong during cancelling transaction.');
        }

        return redirect('admin/orders/'.$payment->order_id);
    }

    private function conditionSevenHandler($params = array()) {
        $payment = Payment::find($params['payment_id']);

        $data = array(
            'subject' => $params['subject'],
            'message' => $params['message'],
            'remaining_amount' => $params['remaining_amount']
        );
        $status = $this->sendEmailToUser($payment, $data);

        $notif = new Notification;
        $notif->user_id = $payment->order->user->id;
        $notif->title = 'Order Status Update';
        $notif->description = 'Failed delivery for order #'.$payment->order_id;
        $notif->link = url('orders/'.$payment->order_id);
        $notif->is_read = 0;
        $notif->save();

        $payment->transfer_note = $payment->transfer_note.' - [ADMIN]: '.$params['message'];
        $payment->is_approved = 0;
        $payment->payment_status_id = 3;

        $status = $payment->save();

        if($status) {
            $order = Order::find($payment->order_id);
            $order->order_status = 5;
            $stat = $order->save();

            $stat = $this->cash_to_point(floatval($params['remaining_amount']), $order->user_id, array('order_id' => $order->id, 'reason' => 'remaining change'));

            if($stat) {
                Session::flash('alert-success', 'Transaction was successfully cancelled!');
            } else {
                Session::flash('alert-danger', 'Oops something\'s wrong during cancelling transaction.');
            }

        } else {
            Session::flash('alert-danger', 'Oops something\'s wrong during cancelling transaction.');
        }

        return redirect('admin/orders/'.$payment->order_id);
    }

    public function getActiveMenu($active_menu) {
        $this->active_menu = $active_menu;

        return $this->active_menu;
    }

    public function getActiveSubmenu($active_menu) {
        $this->active_menu = $active_menu;

        return $this->active_menu;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));
        $params = Input::get('order');
        if ($params){
            $payments = Payment::where('order_id', (int) $params)->where('user_confirmed', 1)->get();
            $order = $params;
        } else{
            $payments = Payment::get();
        }

        return view('admin.payments.index', compact('payments', 'active_menu', 'active_submenu','order'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));
        return view('admin.payments.create', 'active_menu', 'active_submenu');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'transfer_amount'=> 'required|numeric',
            'transfer_date'      => 'required',
            'transfer_time_minute'         => 'required|numeric',
            'transfer_time_second'         => 'required|numeric',
            'transfer_sender_bank_name'     => 'required',
            'transfer_sender_account_number'   => 'required',
            'transfer_sender_account_name'     => 'required',
            'file'          => 'mimes:jpeg,bmp,png',
            'order_number' => 'required|numeric'
        ]);

        $order = Order::find($request->order_number);
        if(null === $order && auth()->user()->id != $order->user_id ) {
            $request->session()->flash('alert-danger', 'Invalid order number!');
            return back();
        }

        $date = '1970-01-02';
        if($request->transfer_date){
            $rq = trim($request->transfer_date);
            $rq = str_replace('-', '/', $request->transfer_date);
            $td = explode('/',$request->transfer_date);
            $date = $td[2].'-'.$td[0].'-'.$td[1];
        }

        $amount = str_replace('.','',$request->transfer_amount);

        $payment = new Payment;
        $payment->order_id = $order->id;
        $payment->method = 'transfer';
        $payment->transfer_dest_account = request('transfer_dest_account');
        $payment->transfer_sender_bank_name = strtoupper(request('transfer_sender_bank_name'));
        $payment->transfer_sender_account_number = request('transfer_sender_account_number');
        $payment->transfer_sender_account_name = request('transfer_sender_account_name');
        $payment->transfer_date = $date;
        $payment->transfer_time = request('transfer_time_minute').':'.request('transfer_time_second');
        $payment->transfer_note = request('transfer_note');
        $payment->transfer_amount = $amount;

        if($file = $request->file('file')) {

            $name = time() . $order->id . '.' . $file->getClientOriginalExtension();

            $file->move('payment_proof', $name);

            $photo = PaymentProof::create(['file' => $name]);

            $payment->transfer_receipt_file = (int) $photo->id;
        }

        $status = $payment->save();



        if($order->count()>1)
        {
            $order->order_status = 6;
            $order->save();
        }

        $request->session()->flash('alert-success', 'Thank you for your payment confirmation');
        return redirect('admin/transaction/'.$payment->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        //

        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));
        $payment = Payment::find($id);

        return view('admin.payments.show', compact('payment', 'active_menu', 'active_submenu'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        //
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));
        $payment = Payment::findOrFail($id);
        return view('admin.payments.edit', compact('payment','active_menu', 'active_submenu'));
    }

    public function addTransactionManually(Request $request) {
        $order_id = null;
        if($request->input('ord')) {
            $order_id = $request->input('ord');
        }
        $order_list = Order::lists('id', 'id')->all();
        if($order_list == null) {
            $order_list = array();
        }
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));

        return view('admin.payments.create', compact('active_menu', 'active_submenu', 'order_id', 'order_list'));
    }

    /*public function approveTransaction(Request $request, $id) {

        $payment = Payment::find($id);

        $payment->is_approved = 1;
        $payment->payment_status_id = 2;
        $status = $payment->save();

        if($status) {
            $order = Order::find($payment->order_id);
            $order->order_status = 7;
            $stat = $order->save();

            $data = array(
                'name' => $payment->order->user->name,
                'email' => $payment->order->user->email,
            );

            try{
                $status = Mail::send('emails.payment_process.payment_confirmed', $data, function ($message) use ($data) {

                    $message->from("hello@seafermart.co.id", "Seafermart Inquiry");
                    $message->to($data['email'])->subject("Payment Confirmed");

                });
            } catch (\Exception $exception) {
                $request->session()->flash('alert-danger', 'Failed to send your success email. Please try again in a few moments!');

                return redirect('admin/orders/'.$payment->order_id);
            }

            $cashback = $this->process_cashback($order);

            if(null !== $order->user()->first()->refer_from){
                $this->process_referral($order);
            }

            if($stat && $cashback) {
                $request->session()->flash('alert-success', 'Transaction was successfully approved!');
            } else {
                $request->session()->flash('alert-danger', 'Oops something\'s wrong during approving transaction.');
            }
        } else {
            $request->session()->flash('alert-danger', 'Oops something\'s wrong during approving transaction.');
        }

        return redirect('admin/orders/'.$payment->order_id);
    }*/

    public function rejectTransaction(Request $request) {

        $payment = Payment::find($request->id);
        $payment->transfer_note = $payment->transfer_note.' - [ADMIN]: '.$request->notes;
        $payment->is_approved = 0;
        $payment->payment_status_id = 2;

        $status = $payment->save();

        if($status) {
            $order = Order::find($payment->order_id);
            $order->order_status = 1;
            $stat = $order->save();

            if($stat) {
                $request->session()->flash('alert-success', 'Transaction was successfully rejected!');
            } else {
                $request->session()->flash('alert-danger', 'Oops something\'s wrong during rejecting transaction.');
            }

        } else {
            $request->session()->flash('alert-danger', 'Oops something\'s wrong during rejecting transaction.');
        }

        return redirect('admin/orders/'.$payment->order_id);
    }

    public function threeDaysApproval(Request $request, $id) {
        $payment = Payment::find($id);

        $payment->payment_status_id = 6;
        $payment->notify_admin = 1;
        $status = $payment->save();

        if($status) {
            $order = $payment->order;
            $order->order_status = 9;
            $order->save();

            $request->session()->flash('alert-success', 'Transaction was successfully approved!');
        } else {
            $request->session()->flash('alert-danger', 'Oops something\'s wrong during approving transaction.');
        }

        return redirect('admin/transaction/'.$payment->id);
    }

    public function handleWithEmail(Request $request) {
        $payment = Payment::find($request->payment_id);

        if($request->type == 1) {
            $passed_params = array(
                'payment_id' => $request->payment_id
            );
            $this->conditionOneHandler($passed_params);
        } elseif($request->type == 2) {
            $message = "<p>
            Pembayaran pesanan Order #".$payment->order->id." anda telah berhasil dan dikonfirmasi oleh team kami. Namun, kami perhatikan bahwa Anda tidak mentransfer jumlah kode unik yang ditambahkan ke jumlah total dalam pesanan Anda. Untuk order berikutnya, kami berharap Anda bs mentransfer jumlah kode unik dalam pesanan Anda sehingga kami dapat mengidentifikasi bank transfer Anda dengan lebih mudah.
            </p>";
            $passed_params = array(
                'payment_id' => $request->payment_id,
                'subject' => 'Pembayaran Seafer Grosir Order #'.$payment->order->id.' telah berhasil dan dikonfirmasi!',
                'message' => $message,
                'remaining_amount' => $request->remaining
            );
            $this->conditionTwoHandler($passed_params);
        } elseif($request->type == 3) {
            $message = "<p>
            Pembayaran pesanan Order #".$payment->order->id." anda telah berhasil dan dikonfirmasi oleh team kami. Namun, karena jumlah yang Anda transfer lebih dari jumlah yang disyaratkan, kami akan mentransfer kembali jumlah ekstra yang Anda transfer melalui transfer bank sebesar ".$request->remaining."
            </p>";
            $passed_params = array(
                'payment_id' => $request->payment_id,
                'subject' => 'Pembayaran Seafer Grosir Order #'.$payment->order->id.' telah berhasil dan dikonfirmasi!',
                'message' => $message,
                'remaining_amount' => $request->remaining
            );
            $this->conditionThreeHandler($passed_params);
        } elseif($request->type == 4) {
            $message = "<p>Pesanan Order #".$payment->order->id." Anda telah dibatalkan karena pembayaran yang Anda lakukan kurang dari jumlah total yang disyaratkan dan jumlah nya kurang dari Rp.10000. Oleh karena itu, sisa pembayaran anda tidak dapat dilakukan via bank transfer dan pesanan anda telah dibatalkan. Pembayaran Anda akan dikembalikan ke rekening anda via bank transfer dan silakan membuat pesanan ulang sekali lagi.</p>";

            $passed_params = array(
                'payment_id' => $request->payment_id,
                'subject' => 'Pembayaran Seafer Grosir Order #'.$payment->order->id.' telah dibatalkan',
                'message' => $message,
                'remaining_amount' => $request->remaining
            );
            $this->conditionFourHandler($passed_params);
        } elseif($request->type == 5) {
            $message = "<p>
            Pembayaran yang Anda lakukan untuk pesanan Order #".$payment->order->id." kurang dari jumlah total yang disyaratkan. Mohon segera melakukan pembayaran sisa sebesar Rp.".$request->remaining." via bank transfer dan beritahu customer service kami jika pembayaran telah dilakukan untuk mencegah pesanan dibatalkan.</p>
            <p>Jika Anda membutuhkan bantuan lebih lanjut, silakan hubungi layanan pelanggan kami. Terima Kasih atas pesanan anda, Sahabat Seafer Grosir!
            </p>";
            $passed_params = array(
                'payment_id' => $request->payment_id,
                'subject' => ' Pembayaran Seafer Grosir Order #'.$payment->order->id.' kurang dari jumlah yang disyratkan',
                'message' => $message,
                'remaining_amount' => $request->remaining
            );
            $this->conditionFiveHandler($passed_params);
        } elseif($request->type == 6) {
            $info = $request->message;

            $message = "
            <p>Pesanan Order #".$payment->order->id." Anda telah dibatalkan karena
            ".$info."</p>
            <p>Jika Anda membutuhkan bantuan lebih lanjut, silakan hubungi layanan pelanggan kami. Kami minta maaf atas ketidaknyamanan ini dan terima Kasih atas pesanan anda, Sahabat Seafer Grosir!</p>";
            $passed_params = array(
                'payment_id' => $request->payment_id,
                'subject' => 'Pembayaran Seafer Grosir Order #'.$payment->order->id.' telah dibatalkan',
                'message' => $message,
                'remaining_amount' => $request->remaining
            );
            $this->conditionSixHandler($passed_params);
        } elseif($request->type == 7) {
            $message = '<p>
            Pesanan Order #'.$payment->order->id.' Anda telah dibatalkan karena pengiriman pesanan anda gagal setelah kurir kami mencoba mengirim pesanan anda kedua kalinya. Jika Anda sudah melakukan pembayaran, Seafer Grosir akan mengembalikan pembayaran ke rekening anda via bank transfer dan silakan membuat pesanan ulang sekali lagi.
            </p>';
            $passed_params = array(
                'payment_id' => $request->payment_id,
                'subject' => 'Pembayaran Seafer Grosir Order #'.$payment->order->id.' telah dibatalkan karena pengiriman gagal.',
                'message' => $message,
                'remaining_amount' => $request->remaining
            );
            $this->conditionSevenHandler($passed_params);
        }

        return redirect('admin/orders/'.$payment->order_id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        //
        $payment = Payment::find($id);

        $order = Order::find($payment->order_id);

        $order->order_status = 1;
        $order->save();

        $status = $payment->delete();

        if($status) {
            $request->session()->flash('alert-success', 'Transaction was successfully deleted!');
        } else {
            $request->session()->flash('alert-danger', 'Oops, something is wrong during deleting transaction!');

            return back();
        }

        return redirect('admin/transaction');
    }

    public function extraStepOne(Request $request, $id) {
        $payment = Payment::find($id);

        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));

        if($request->get('action') == 'accept') {
            $action_option = array(
                1 => 'Paid amount is correct',
                2 => 'Unique Code amount not transferred',
                3 => 'Paid amount is more than required',
            );
        } elseif($request->get('action') == 'reject') {
            $action_option = array(
                4 => 'Order Cancellation for insufficient payment amount (99<less amount<10000)',
                5 => 'Request for transfer of amount due to insufficient payment (less amount> 10000)',
            );
        } elseif ($request->get('action') == 'void') {
            $action_option = array(
                6 => 'Order has been cancelled (for manual void)',
                7 => 'Failed Delivery (For failed logistics delivery)'
            );
        } else {
            return redirect('admin/transactions/'.$id);
        }

        $choice = $request->get('action');
        /*$action_option = array(
            1 => 'Paid amount is correct',
            2 => 'Unique Code amount not transferred',
            3 => 'Paid amount is more than required',
            4 => 'Order Cancellation for insufficient payment amount (99<less amount<10000)',
            5 => 'Request for transfer of amount due to insufficient payment (less amount> 10000)',
            6 => 'Order has been cancelled (for manual void)',
            7 => 'Failed Delivery (For failed logistics delivery)'
        );*/

        return view('admin.payments.payment_step_one', compact('payment', 'active_menu', 'active_submenu', 'action_option', 'choice'));
    }

    public function processStepOne(Request $request) {
        $data_one = array(
            'type' => $request->type,
            'payment_id' => $request->payment_id,
            'message' => $request->message
        );
        $request->session()->put('data_one', $data_one);

        return redirect('admin/transactions/step-two/'.$data_one['payment_id']);
    }

    public function extraStepTwo(Request $request, $id) {

        if(! $request->session()->has('data_one')) {
            return redirect('admin/transactions/'.$id);
        }
        $prev_data = $request->session()->get('data_one');

        if($prev_data['payment_id'] != $id) {
            return redirect('admin/transactions/'.$id);
        }

        $payment = Payment::find($id);

        $payments = Payment::where('order_id', $payment->order->id)->where('user_confirmed', 1)->get();
        $total_paid = 0;
        if($payments->count() > 0) {
            foreach ($payments as $p) {
                $total_paid = $total_paid + $p->transfer_amount;
            }
        }

        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));

        return view('admin.payments.payment_step_two', compact('payment', 'active_menu', 'active_submenu', 'prev_data', 'total_paid'));
    }
}
