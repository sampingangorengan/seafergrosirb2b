<?php

namespace App\Http\Controllers\Admin;

use App\Models\Order;
use App\Models\User;
use App\Models\Order\Groceries as Order_groceries;
use App\Models\Order\Payment;
use App\Models\GojekDetail;
use App\Models\Groceries;
use App\Models\Notification;
use App\Models\User\Address;
use App\Http\Requests;
use App\Models\User\Tempo;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Mail;

use Money;

class OrderController extends Controller
{


    protected $active_menu = null;
    protected $active_submenu = null;

    private function updateGojekDetail($id, $detail) {

        $gojek_status = $this->getGojekStatus($detail->orderNo);

        $status = DB::table('gojek_details')
            ->where('id', $id)
            ->update([
                'gojek_id' => $gojek_status->id,
                'gojek_order' => $gojek_status->orderNo,
                'status' => $gojek_status->status,
                'driver_id' => $gojek_status->driverId,
                'driver_name' => $gojek_status->driverName,
                'driver_phone' => $gojek_status->driverPhone,
                'driver_photo' => $gojek_status->driverPhoto,
                'total_price' => $gojek_status->totalPrice,
                #'receiver_name' => $gojek_status->receiver_name,
                'order_created_time' => $gojek_status->orderCreatedTime,
                'order_dispatch_time' => $gojek_status->orderDispatchTime,
                'order_arrival_time' => $gojek_status->orderArrivalTime,
                'order_closed_time' => $gojek_status->orderClosedTime,
                'seller_address_name' => $gojek_status->sellerAddressName,
                'seller_address_detail' => $gojek_status->sellerAddressDetail,
                'buyer_address_name' => $gojek_status->buyerAddressName,
                'buyer_address_detail' => $gojek_status->buyerAddressDetail,
                'cancel_description' => $gojek_status->cancelDescription,
                'booking_type' => $gojek_status->bookingType,
                #'store_order_id' => $gojek_status->order_id
            ]);

        $delivery = DB::table('deliveries')->where('detail_id', $id)->update(['booking_unique' => $detail->id, 'status' => 'on delivery','updated_at' => date('Y-m-d H:i:s')] );

        return $status;
    }

    public function addPaymentOption(Request $request){
        $user = User::find($request->user_id);
        if(in_array("tempo",$request->payment_method)){
            $tempo = new Tempo();
            $tempo->user_id = $user->id;
            $tempo->aggrement = "";
            $tempo->status = "pending";
            $tempo->save();
        }
        $user->payment_method = json_encode($request->payment_method);
        $user->save();
        $request->session()->flash('alert-success', 'Successfully update payment for '. $user->company_name );
        return redirect('admin/users/'.$user->id);
    }

    public function getActiveMenu($active_menu) {
        $this->active_menu = $active_menu;

        return $this->active_menu;
    }

    public function getActiveSubmenu($active_menu) {
        $this->active_menu = $active_menu;

        return $this->active_menu;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));
        if(null !=  $request->get('status')){
            if($request->get('status') == 'orders') {
                $orders = Order::where('order_status', 1)->orWhere('order_status', 6)->orderBy('order_status', 'asc')->get();
            } elseif($request->get('status') == 'confirmed') {
                $orders = Order::where('order_status', 7)->orWhere('order_status',2)->orWhere('order_status',3)->orderBy('order_status', 'desc')->get();
            } elseif($request->get('status') == 'rejected')  {
                $orders = Order::where('order_status', 5)->orderBy('order_status', 'asc')->get();
            } elseif($request->get('status') == 'completed') {
                $orders = Order::where('order_status', 4)->orderBy('order_status', 'asc')->get();
            } elseif($request->get('status') == 'pending'){
                $orders = Order::where('order_status',10)->orderBy('order_status','asc')->get();
            }elseif($request->get('status') == 'cod') {

                $order_one = Order::where('shipping_service_code', 'internal')->where('order_status', 6)->orderBy('order_status', 'asc')->get();
                $order_two = Order::where('shipping_service_code', 'internal')->where('order_status', 6)->orderBy('order_status', 'asc')->get();

                $orders = $order_one->merge($order_two);

            } else {
                $orders = Order::where('order_status', $request->get('status'))->get();
            }

        }else{
            $orders = Order::get();
        }

        return view('admin.orders.index', compact('orders', 'active_menu', 'active_submenu'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));
        return view('admin.orders.create', compact('active_menu', 'active_submenu'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        //
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));
        $order = Order::find($id);

        $total_user_payment = Payment::where('order_id', $id)->where('user_confirmed', 1)->count();

        return view('admin.orders.show', compact('order', 'active_menu', 'active_submenu', 'total_user_payment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        //
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));
        $order = Order::find($id);

        return view('admin.orders.edit', compact('order', 'active_menu', 'active_submenu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function doGojek(Request $request, $id)
    {
        $order = Order::find($id);

        # check delivery

        $old_gojek_order = GojekDetail::find($order->deliveries->detail_id);

        if($old_gojek_order){
            $old_gojek_order->delete();
        }

        $delivery_detail_id = DB::table('gojek_details')->insertGetId(
            [
                'status' => 'not made',
                'booking_type' => "gojek",
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]
        );
        $order->deliveries->detail_id = $delivery_detail_id;
        $order->deliveries->delivery_type = "gojek";

        $order->deliveries->save();

        $originLatLong = '-6.102789, 106.803789';
        $defaultAddress = $order->getShippingAddress($order->user_address_id);

        if ($defaultAddress) {

            if(null == $defaultAddress->coordinates) {
                $client = new Client();

                $address = urlencode($defaultAddress->address)
                    .'+'.urlencode($defaultAddress->area->name)
                    . '+' . urlencode($defaultAddress->area->city->city_name)
                    . '+' . urlencode($defaultAddress->area->city->province->province_name_id)
                    .'+'.urlencode($defaultAddress->postal_code);

                $address = str_replace(' ', '+', $address);
                $url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' . $address . '&key=AIzaSyCRDhe-WvUmE8Y96Gb6Glj-CPc9lfjC420';

                $res = $client->request('GET', $url);
                $results = json_decode($res->getBody())->results;
                $destLatLong = $results[0]->geometry->location->lat . ',' . $results[0]->geometry->location->lng;
            } else {
                $destLatLong = $defaultAddress->coordinates;
            }

            $items = $order->groceries;
            $list_item = '';
            foreach ($items as $item) {
                $list_item .= $item->groceries->name.',';
            }

            //$order->getShippingAddress($order->user_address_id)->user->name;

            $recipient_name = null;
            if($defaultAddress->recipient !== '' && $defaultAddress->recipient !== null) {
                $recipient_name = $defaultAddress->recipient;
            } else {
                $recipient_name = $defaultAddress->user->name;
            }

            $recipient_phone = null;
            if($defaultAddress->phone_number !== '' && $defaultAddress->phone_number !== null) {
                $recipient_phone = $defaultAddress->phone_number;
            } else {
                $recipient_phone = $defaultAddress->user->phone;
            }

            unset($client);

            $client = new Client([
                // Base URI is used with relative requests

                'base_uri' => (env('GOJEK_ENV') == 'production') ? env('GOJEK_URI_PRODUCTION') :  env('GOJEK_URI_STAGING'),

                // You can set any number of default request options.
                'timeout'  => 60.0,
            ]);

            $headers = ['Client-ID' => env('GOJEK_CLIENT'), 'Pass-Key' => ENV('GOJEK_PASSKEY'), 'Content-Type' => 'application/json'];
            // $body = '{'.
            //         '"paymentType": 3,'.
            //         '"collection_location": "pickup",'.
            //         '"shipment_method": "SameDay",'.
            //         '"deviceToken": "",'.
            //         '"routes": ['.
            //         '{'.
            //             '"originName": "Seafer Grosir Warehouse",'.
            //             '"originNote": "",'.
            //             '"originContactName": "Seafer Grosir",'.
            //             '"originContactPhone": "02166696285",'.
            //             '"originLatLong": "'.$originLatLong.'",'.
            //             '"originAddress": "Jalan Cumi Raya No. 3, Muara Baru, RT.20/RW.17, Penjaringan, Jakarta Utara, Kota Jkt Utara, Daerah Khusus Ibukota Jakarta 14440",'.
            //             '"destinationName": "'.$defaultAddress->area->name.'",'.
            //             '"destinationNote": "string",'.
            //             '"destinationContactName": "'.$recipient_name.'",'.
            //             '"destinationContactPhone": "'.$recipient_phone.'",'.
            //             '"destinationContactPhone": "081081081081",'.
            //             '"destinationLatLong": "'.$destLatLong.'",'.
            //             '"destinationAddress": "'.$defaultAddress->address.'",'.
            //             '"item": "'.$list_item.'",'.
            //             '"storeOrderId": "'.$order->id.'"'.
            //         '}'.
            //     ']'.
            // '}';
            // print_r($defaultAddress);die;



            $body = preg_replace( "/\r|\n/", "",'
            {
                "paymentType": 3,
                "deviceToken": "",
                "collection_location":"pickup",
                "shipment_method":"Instant",
                "booking_type":"Instant",
                "routes": [{
                   "originNote": "Seafer Grosir Warehouse",
                   "originName": "",
                   "originAddress": "Jln. Cumi Raya No. 3, Muara Baru,RT.20/RW.17,Penjaringan, Jakarta Utara - 14440",
                   "originContactName": "Seafer Grosir",
                   "originContactPhone": "021-66696285",
                   "originLatLong": "'.trim($originLatLong).'",
                   "destinationNote": "",
                   "destinationName": "'.$defaultAddress->area->name.'",
                   "destinationContactName": "'.$recipient_name.'",
                   "destinationContactPhone": "'.$recipient_phone.'",
                   "destinationLatLong": "'.trim($destLatLong).'",
                   "destinationAddress": "'.$defaultAddress->address.'",
                   "serviceType": 10,
                   "storeOrderId":"'.$order->id.'",
                   "item": "'.$list_item.'"
               }]
           }');



            try{
                $res = $client->request('POST', '/gokilat/v10/booking', compact('headers', 'body'));
            } catch (\Exception $e) {
                $response = $e->getResponse();
                if($response != NULL){
                    $statusCode = $response->getStatusCode();
                    switch ($statusCode) {
                        case '400':
                            $request->session()->flash('alert-danger', 'Oops couldn\'t order Gojek for this order. Something went wrong with the request');
                            break;
                        case '401':
                            $request->session()->flash('alert-danger', 'Oops couldn\'t order Gojek for this order. unauthorized');
                            break;
                        case '406':
                            $request->session()->flash('alert-danger', 'Oops couldn\'t order Gojek for this order. The Origin/Destination is not Serviceable');
                            break;
                        case '409':
                            $request->session()->flash('alert-danger', 'Oops couldn\'t order Gojek for this order. Duplicate booking');
                            break;
                        case '422':
                            $request->session()->flash('alert-danger', 'Oops couldn\'t order Gojek for this order. Shipping Method is invalid');
                            break;
                        default:
                            $request->session()->flash('alert-danger', 'Oops couldn\'t order Gojek for this order. Unknown Error');
                            break;
                    }
                }else{
                    $request->session()->flash('alert-danger', 'Oops couldn\'t order to Gojek for this order. Empty Response');
                }

                return back();
            }


            $booking = json_decode($res->getBody()->getContents());
            $gojek = $order->deliveries->detail_id;
            $save_gojek = $this->updateGojekDetail($gojek, $booking);

            if($save_gojek) {

                $request->session()->flash('alert-success', 'Gojek was successfully ordered! The driver will contact you as soon as the system found you one.');

            } else {
                $request->session()->flash('alert-danger', 'Oops something\'s wrong during ordering gojek.');
            }

            return redirect('admin/orders/'.$order->id);

        }
    }

    public function getGojekStatus($order_number) {
        $client = new Client([
            // Base URI is used with relative requests
            'base_uri' => (env('APP_ENV') == 'production') ? env('GOJEK_URI_PRODUCTION') :  env('GOJEK_URI_STAGING'),
            // You can set any number of default request options.
            'timeout'  => 60.0,
        ]);

        $headers = ['Client-ID' => env('GOJEK_CLIENT'), 'Pass-Key' => ENV('GOJEK_PASSKEY'), 'Content-Type' => 'application/json'];
        $res = $client->request('GET', '/gokilat/v10/booking/orderno/'.$order_number, compact('headers'));

        $response = json_decode($res->getBody()->getContents());

        return $response;
    }

    public function insertAwb(Request $request, $id){
        $order = Order::find($id);
        $delivery_id = $order->deliveries->id;

        $delivery = DB::table('deliveries')->where('id', $delivery_id)->update(['booking_unique' => $request->awb_receipt, 'status' => 'on delivery','updated_at' => date('Y-m-d H:i:s')] );

        if($delivery) {
            $request->session()->flash('alert-success', 'The order is now on delivery');
        } else {
            $request->session()->flash('alert-danger', 'Oops something\'s wrong during inserting awb.');
        }

        return redirect('admin/orders/'.$id);
    }

    public function dispatchInternalCourier(Request $request, $id)
    {
        $order = Order::find($id);
        $delivery_id = $order->deliveries->id;

        $delivery = DB::table('deliveries')->where('id', $delivery_id)->update(['delivery_type'=> 'Seafer Grosir Courier','booking_unique' => 'Internal Delivery', 'status' => 'on delivery','updated_at' => date('Y-m-d H:i:s')] );

        if($delivery) {
            $request->session()->flash('alert-success', 'The order is now on delivery');
        } else {
            $request->session()->flash('alert-danger', 'Oops something\'s wrong during dispatching courier.');
        }

        return redirect('admin/orders/'.$id);
    }


    public function finishCourier(Request $request, $id) {
        $order = Order::find($id);
        $delivery_id = $order->deliveries->id;

        $delivery = DB::table('deliveries')->where('id', $delivery_id)->update(['booking_unique' => 'Internal Delivery', 'status' => 'delivered','updated_at' => date('Y-m-d H:i:s')] );

        $order->order_status = 4;
        $order->save();

        $notif = new Notification;
        $notif->user_id = $order->user_id;
        $notif->title = 'Order Delivery Update';
        $notif->description = 'Your order has been delivered';
        $notif->link = url('orders/'.$order->id);
        $notif->is_read = 0;
        $notif->save();

        if($delivery) {
            $request->session()->flash('alert-success', 'The order is now completed');
        } else {
            $request->session()->flash('alert-danger', 'Oops something\'s wrong during completing this order.');
        }

        return redirect('admin/orders/'.$id);
    }

    public function voidOrder(Request $request, $id) {
        $order = Order::find($id);

        foreach ($order->groceries as $gc) {
            $g = Groceries::find($gc->groceries_id);
            $g->stock = $g->stock + $gc->qty;
            $g->sold = $g->sold - $gc->qty;
            $g->save();
        }

        if (null!==$order) {
            $status = DB::table('orders')->where('id', $id)->update(['order_status' => 5] );

            if($status){
                $request->session()->flash('alert-success', 'The order is canceled');
            } else {
                $request->session()->flash('alert-danger', 'Oops something\'s wrong during canceling this order.');
            }
        }

        return redirect('admin/orders/'.$id);
    }

    public function indexShipping(Request $request, $id) {
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));

        $order = Order::find($id);
        $shipping = $order->deliveries;

        return view('admin.orders.show_shipping', compact('order', 'active_menu', 'active_submenu', 'shipping'));

    }

    public function updatePrice(Request $request,$order_id){

        $user = auth()->user();

        if($user == null){
            $response = array('status' => 'ERR', 'status_code' => 400, 'message' => 'please relogin');
            return json_encode($response);
        }

        //Change the order table
        $total = 0;

        $order = Order::find($order_id);

        //compose message for user
        $user = User::find($order->user_id);
        $name = $user->username;
        $msg = "
        <p>
        Beberapa produk di keranjang pesanan Anda untuk Order #".$order->id." yang perlu ditimbang telah ditimbang oleh tim kami dan pesanan Anda Order #".$order->id." telah diproses sesuai berikut:
        </p>
        ";

        //change the order_groceries
        $changes = $request->changes;
        $i = 1;
        foreach($changes as $order_changes){
            $key = key($order_changes);
            $order_groceries = Order_groceries::find($key);
            $order_groceries->change_amount = $order_groceries->price;
            $order_groceries->price = $order_changes[$key]["price"];
            $order_groceries->changed_by_id = auth()->user()->id;
            $order_groceries->change_note = $order_changes[$key]["notes"];
            $order_groceries->save();

            $total += ((Int)$order_changes[$key]["price"] * (Int) $order_groceries->qty);

            //Add the item detail in message
            $msg .= "<p>". $i .". ".Groceries::find($order_groceries->groceries_id)-> name.": ".$order_changes[$key]["price"]." - ".$order_changes[$key]["notes"]."</p>";
            $i++;
        }


        $order->nett_total = $total;
        $order->tax = $total / 10;
        $order->grand_total = $total + ($total / 10) + $order->unique_code;
        $order->isMeasured = 1;
        $order->save();

        $msg .= "
        <p>Mohon segera melakukan pembayaran sesuai method pembayaran yang anda telah dipilih, Terima Kasih atas pesanan anda, Sahabat Seafer Grosir!</p>
        <br>
        <p><a href='".url("orders/".$order_id)."'>Klik untuk melihat detail pesanan</a></p>
        ";

        //Mail User
        try{
            Mail::queue('emails.general', compact('name','msg'), function ($message) use ($user) {

                $message->from('hello@seafermart.co.id', '[NO-REPLY] Order selesai ditimbang');
                $message->to($user->email);

            });
        }catch(Exception $e) {

        }

        $request->session()->flash('alert-success', 'Success, The user has been notified by mail ');

        return json_encode([
            'status' => 'OK',
            'status_code' => 200
        ]);
    }

    public function updateInterest(Request $request,$order_id){
        $this->validate($request, [
            'interest'      => 'required|integer',
        ]);
        $order = Order::find($order_id);
        $user = $order->user;
        $name = $user->company_name;
        $order->interest = $request->interest;
        $order->save();

        //Mail User

        $msg = "
        <p>Pesanan anda dikenakan bunga sebesar ".$request->interest."% dikarekan keterlambatan pembayaran.</p>
        <br>
        <p><a href='".url("orders/".$order_id)."'>Klik disini untuk melihat pesanan anda</a></p>
        ";
        try{
            Mail::queue('emails.general', compact('name','msg'), function ($message) use ($user) {

                $message->from('hello@seafermart.co.id', '[NO-REPLY] Pesanan dikenakan bunga keterlambatan');
                $message->to($user->email);

            });
        }catch(Exception $e) {

        }

        $request->session()->flash('alert-success', 'Success, Adding interest ');
        return redirect('admin/orders/'.$order_id);
    }
}
