<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Groceries\ChildCategory;
use App\Models\Groceries\Category;
use App\Models\Groceries\ChildCategoryImage;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use File;

class ChildCategoryController extends Controller
{
    protected $active_menu = null;
    protected $active_submenu = null;

    public function getActiveMenu($active_menu) {
        $this->active_menu = $active_menu;

        return $this->active_menu;
    }

    public function getActiveSubmenu($active_menu) {
        $this->active_menu = $active_menu;

        return $this->active_menu;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));
        $categories = ChildCategory::orderBy('parent_id', 'asc')->get();
        return view('admin.categories.index_child', compact('categories', 'active_menu', 'active_submenu'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));
        $parent = Category::orderBy('name', 'asc')->lists('name', 'id')->all();
        return view('admin.categories.create_child', compact('parent', 'active_menu', 'active_submenu'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'          => 'required',
            'order'         => 'required|numeric',
            'parent'        => 'required',
        ]);

        $childcategory = new ChildCategory;
        $childcategory->name = $request->name;
        $childcategory->order = $request->order;
        $childcategory->is_active = $request->is_active;
        $childcategory->parent_id = $request->parent;
        $childcategory->child_category_image_id = 0;

        if($file = $request->file('file')) {
            $name = time().$file->getClientOriginalName();

            $file->move('child_category_images', $name);

            $photo = ChildCategoryImage::create(['file' => $name]);

            $childcategory->child_category_image_id = (int) $photo->id;
        }

        $status = $childcategory->save();

        if($status) {
            $request->session()->flash('alert-success', 'Category was successfully added!');
        } else {
            $request->session()->flash('alert-danger', 'Oops, something is wrong during adding category!');

            return back();
        }

        return redirect('admin/child-category');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        //
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));
        return view('admin.category.show_child', compact('active_menu', 'active_submenu'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        //
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));
        $childCategory = ChildCategory::find($id);
        $parent = Category::lists('name', 'id')->all();
        return view('admin.categories.edit_child', compact('childCategory', 'parent', 'active_menu', 'active_submenu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $this->validate($request, [
            'name'          => 'required',
            'slug'          => 'exists:child_categories,slug',
            'order'         => 'required|numeric',
            'parent_id'        => 'required',
            'file'          => 'mimes:jpeg,bmp,png'
        ]);
        $childCategory = ChildCategory::find($id);

        $input = $request->all();

        if($file = $request->file('file')) {
            $name = time().$file->getClientOriginalName();

            $file->move('child_category_images', $name);

            $photo = ChildCategoryImage::create(['file' => $name]);

            $old_image = $childCategory->image;

            if (null != $old_image) {
                if( File::exists(app_path().'/../public'.$old_image->file) ) {
                    File::delete(app_path().'/../public'.$old_image->file);
                }
                
                $old_image->delete();    
            }

            $input['child_category_image_id'] = (int) $photo->id;
            
            unset($input['file']);

        } else {
            unset($input['file']);
            $input['child_category_image_id'] = $childCategory->child_category_image_id;
        }

        $status = $childCategory->update($input);

        if($status) {
            $request->session()->flash('alert-success', 'Category was successfully updated!');
        } else {
            $request->session()->flash('alert-danger', 'Oops, something is wrong during updating category!');

            return back();
        }


        return redirect('admin/child-category');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $childCategory = ChildCategory::find($id);
        
        if(null !== $childCategory->image) {
            $childCategory->image->delete();    
        }
        $status = $childCategory->delete();

        if($status) {
            $request->session()->flash('alert-success', 'Category was successfully deleted!');
        } else {
            $request->session()->flash('alert-danger', 'Oops, something is wrong during deleting category!');

            return back();
        }

        return redirect('admin/child-category');
    }
}
