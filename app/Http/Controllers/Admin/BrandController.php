<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Groceries\Brand;
use \Cviebrock\EloquentSluggable\Services\SlugService;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class BrandController extends Controller
{
    protected $active_menu = null;
    protected $active_submenu = null;

    public function getActiveMenu($active_menu) {
        $this->active_menu = $active_menu;

        return $this->active_menu;
    }

    public function getActiveSubmenu($active_menu) {
        $this->active_menu = $active_menu;

        return $this->active_menu;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));
        $brands = Brand::get();
        return view('admin.products.index_brand', compact('brands', 'active_menu', 'active_submenu'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));
        return view('admin.products.create_brand', compact('active_menu', 'active_submenu'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'name'          => 'required',
            'is_active'     => 'required',
        ]);

        $brand = new Brand;
        $brand->name = $request->name;
        $brand->is_active = $request->is_active;

        $status = $brand->save();

        if($status) {
            $request->session()->flash('alert-success', 'Brand was successfully added!');
        } else {
            $request->session()->flash('alert-danger', 'Oops something\'s wrong during brand creation.');
        }

        return redirect('admin/brands');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        //
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));
        $brand = Brand::findOrFail($id);
        return view('admin.products.show_brand', compact('active_menu', 'active_submenu'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        //
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));
        $brand = Brand::findOrFail($id);
        return view('admin.products.edit_brand',compact('brand', 'active_menu', 'active_submenu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
            'name'          => 'required',
            'slug'          => 'unique:brands,slug,'.$id,
            'is_active'     => 'required',
        ]);

        $brand = Brand::findOrFail($id);

        $input = $request->all();

        $status = $brand->update($input);

        if($status) {
            $request->session()->flash('alert-success', 'Brand was successfully updated!');
        } else {
            $request->session()->flash('alert-danger', 'Oops, something is wrong during updating brand!');

            return back();
        }


        return redirect('admin/brands');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        //
        $status = Brand::find($id)->delete();

        if($status) {
            $request->session()->flash('alert-success', 'Brand was successfully deleted!');
        } else {
            $request->session()->flash('alert-danger', 'Oops, something is wrong during deleting brand!');

            return back();
        }

        return redirect('admin/brands');
    }
}
