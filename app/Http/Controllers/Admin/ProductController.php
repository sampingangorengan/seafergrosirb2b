<?php

namespace App\Http\Controllers\Admin;


use App\Models\Discount;
use App\Models\Groceries\Metric;
use App\Models\Tag;
use App\Models\Groceries\CookingAdvice;
use App\Models\SearchableGroceries;
use Illuminate\Http\Request;
use App\Models\Groceries;
use App\Models\Groceries\Brand;
use App\Models\Promo;
use App\Models\Country;
use App\Models\Groceries\ChildCategory;
use App\Models\Groceries\Image;
use App\Models\Groceries\Video;
use App\Models\Groceries\Groceries_poster as Poster;
use App\Models\Order;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Excel;
use Validator;
use App\Models\userSupplier;
use Illuminate\Http\UploadedFile;
use Pion\Laravel\ChunkUpload\Exceptions\UploadMissingFileException;
use Pion\Laravel\ChunkUpload\Handler\AbstractHandler;
use Pion\Laravel\ChunkUpload\Handler\HandlerFactory;
use Pion\Laravel\ChunkUpload\Receiver\FileReceiver;

class ProductController extends Controller
{
    protected $active_menu = null;
    protected $active_submenu = null;

    private function process_csv_bit($item){

        $grocery = array();

        $grocery['name'] = $item['name'];
        $grocery['price'] = floatval(str_replace(',','',$item['price']));
        $grocery['stock'] = intval($item['stock_akhir']);
        $grocery['created_at'] = date('Y-m-d H:i:s');
        $grocery['updated_at'] = date('Y-m-d H:i:s');

        if(null != $item['description']) {
            $grocery['description'] = $item['description'];
        } else {
            $grocery['description'] = 'No Data';
        }

        if(null != $item['nutrients']) {
            $grocery['nutrients'] = $item['nutrients'];
        } else {
            $grocery['nutrients'] = 'No Data';
        }

        if(null != $item['ingredients']) {
            $grocery['ingredients'] = $item['ingredients'];
        } else {
            $grocery['ingredients'] = 'No Data';
        }

        if(null != $item['sku']) {
            $grocery['sku'] = $item['sku'];
        } else {
            $grocery['sku'] = 'No SKU Data';
        }


        if (null == $item['value_per_package']) {
            $grocery['value_per_package'] = 0;
        } else {
            $grocery['value_per_package'] = intval($item['value_per_package']);
        }

        if (null == $item['metric_id']) {
            $grocery['metric_id'] = 1;
        } else {
            $metric = Metric::select('id')->where('abbreviation', $item['metric_id'])->get();
            if($metric->count() != 0) {
                $grocery['metric_id'] = $metric[0]->id;
            }
        }

        if (null != $item['brand_id']) {
            $brand = Brand::select('id')->where('name', $item['brand_id'])->get();
            if($brand->count() != 0) {
                $grocery['brand_id'] = $brand[0]->id;
            } else {
                $brand = new Brand;
                $brand->name = $item['brand_id'];
                $brand->is_active = 1;

                $brand->save();

                $bid = Brand::select('id')->where('name', $item['brand_id'])->get();
                $grocery['brand_id'] = $bid[0]->id;
            }
        } else {
            $grocery['brand_id'] = 1;
        }

        if(null != $item['expiry_date']) {
            $grocery['expiry_date'] = str_replace('.', '-', $item['expiry_date']).' 00:00:00';
        } else {
            $grocery['expiry_date'] = '1970-01-02 00:00:00';
        }

        if(null != $item['barcode']) {
            $grocery['barcode'] = strval(intval($item['barcode']));
        }

        $gr_id = DB::table('groceries')->insertGetId($grocery);

        $searchable = DB::table('searchable_groceries')->insertGetId(
            [
                'groceries_id' => $gr_id,
                'name' => $request->name,
                'is_active' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]
        );

        $grocer = Groceries::find($gr_id);

        if($item['category_id'] != null) {
            if(false != strpos($item['category_id'], ',')) {
                $cats = explode(', ', $item['category_id']);
                foreach($cats as $cat_name) {
                    $cat = ChildCategory::select('id')->where('name', $cat_name)->get();
                    if($cat->count() != 0) {
                        $grocer->category()->attach($cat[0]->id);
                    }
                }
            } else {
                $cat = ChildCategory::select('id')->where('name', $item['category_id'])->get();
                if($cat->count() != 0) {
                    $grocer->category()->attach($cat[0]->id);
                }
            }
        }

        if(null != $item['tags']) {
            $tags = explode(',', $item['tags']);
            foreach($tags as $tag) {
                // find existing tag
                $tag_exist = Tag::where('name', $tag)->lists('id')->all();

                if (count($tag_exist) > 0) {
                    $grocer->tags()->attach($tag_exist[0]);
                } else {
                    $tag_new = DB::table('tags')->insertGetId(
                        [
                            'name' => $tag
                        ]
                    );
                    $grocer->tags()->attach($tag_new);
                }
            }
        }

        return $item['name'].' done';

    }

    private function calculateDiscount($price, $discount_id) {

        $discount = Discount::find($discount_id);

        $discount_value = 0;

        if($discount->discount_type == 'percent') {

            $discount_value =  ceil( $price - ($price * ( $discount->discount_value / 100)) );

        } else {

            $discount_value = $discount->discount_value;

        }

        if($price > $discount_value) {
            return $price - $discount_value;
        } else {
            return 0;
        }
    }

    public function getActiveMenu($active_menu) {
        $this->active_menu = $active_menu;

        return $this->active_menu;
    }

    public function getActiveSubmenu($active_menu) {
        $this->active_menu = $active_menu;

        return $this->active_menu;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));
        //
        $parameter = $request->fullUrl();
        $query_string = explode('?', $parameter);

        $query_params = [];
        if (count($query_string)>1){
            $query = explode('&', $query_string[1]);
           if(count($query)>0) {
               foreach($query as $key=>$q) {
                    $kq = explode('=',$q);

                    $query_params[$kq[0]] = $kq[1];
               }
           }
        }

        if (!empty($query_params)) {
            if ($query_params['get'] == 'sold') {
                $groceries = Groceries::where('sold', '>', 0)->orderBy('sold')->get();
            } elseif ($query_params['get'] == 'all') {
                $groceries = Groceries::where('deleted', 0)->get();
            }
        } else {
            $groceries = Groceries::where('deleted', 0)->get();
        }


        return view('admin.products.index', compact('groceries', 'active_menu', 'active_submenu'));
    }


    public function minimum(Request $request){
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));

        $groceries = Groceries::where('stock', '<=', 'minimum_stock_alert')->get();

        return view('admin.products.index', compact('groceries', 'active_menu', 'active_submenu'));
    }

    public function initialTransferToSearchable(Request $request) {
        $all_products = Groceries::get();


        foreach($all_products as $prod) {
            $searchable = new SearchableGroceries;
            $searchable->groceries_id = $prod->id;
            $searchable->name = $prod->name;
            $searchable->is_active = 1;
            $searchable->save();
            var_dump($prod->id);
        }
    }

    public function getExistingCategories(Request $request, $id, $result) {

        $groceries = Groceries::find($id);

        $cat = array();
        if($result == 'category') {
            if(null != $groceries->category && $groceries->category->count() > 0) {
                foreach($groceries->category as $k=>$category) {
                    array_push($cat,$category->id);
                }
            }
        } else if($result == 'brand') {
            array_push($cat,$groceries->brand->id);
        }else if ($result == 'supplier'){

            foreach($groceries->suppliers as $supplier){
                array_push($cat,$supplier->id);
            }
        }

        echo json_encode($cat);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));
        $categories = ChildCategory::where('is_active', 1)->orderBy('name', 'asc')->lists('name', 'id')->all();
        $suppliers = UserSupplier::lists('company_name','id')->all();
        $discounts = Discount::get();
        $brands = Brand::where('is_active', 1)->orderBy('name', 'asc')->get();
        $metrics = Metric::where('is_active', 1)->orderBy('name', 'asc')->get();
        $tags = Tag::get();
        /*$cookingAdvice = CookingAdvice::lists('name', 'id')->all();*/
        $country = Country::get();

        $old_input = array(
            'name'          => $request->old('name'),
            'category'      => $request->old('category'),
            'price'         => $request->old('price'),
            'stock'         => $request->old('stock'),
            'cooking_advice'   => $request->old('cooking_advice'),
            'description'   => $request->old('description'),
            'availabity_note' => $request->old('availabity_note'),
            'packing'       => $request->old('packing'),
            'nutrients'     => $request->old('nutrients'),
            'ingredients'   => $request->old('ingredients'),
            'sku'           => $request->old('sku'),
            'range_price' => $request->old('range_price'),
            'supplier_id' => $request->old('supplier_id'),
            'minimum_stock_alert' => $request->old('minimum_stock_alert'),
            'barcode'       => $request->old('barcode'),
            'qpp'           => $request->old('qpp'),
            'coo'           => $request->old('coo'),
            'metric'        => $request->old('metric'),
            'brand'         => $request->old('brand'),
            'nature'       => $request->old('nature'),
            'state'         => $request->old('state'),
            'image_two'     => $request->old('image_two'),
            'image_one'     => $request->old('image_one'),
            'image_three'   => $request->old('image_three'),
            'image_four'    => $request->old('image_four'),
            'image_five'    => $request->old('image_five'),
            'poster_two'     => $request->old('poster_two'),
            'poster_one'     => $request->old('poster_one'),
            'poster_three'   => $request->old('poster_three'),
            'poster_four'    => $request->old('poster_four'),
            'poster_five'    => $request->old('poster_five'),
            'poster_six'    => $request->old('poster_six'),
            'poster_seven'  => $request->old('poster_seven'),
            'expiry_date'   => $request->old('expiry_date'),
            'youtube_link'  => $request->old('ytl')
        );

        #dd($categories);

        return view('admin.products.create', compact('categories','suppliers','discounts','brands', 'country','metrics', 'tags', 'cookingAdvice', 'active_menu', 'active_submenu','old_input'));
    }

    public function csv_import(Request $request) {

        $this->validate($request, [
            'file'          => 'mimes:jpeg,bmp,png',
        ]);

        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));

        /*$ca = ChildCategory::select('id')->where('name', 'Syrup')->get();
        dd($ca[0]->id);
        die();*/

        if($file = $request->file('file')) {
            if($file->getMimeType() != 'text/plain') {
                $request->session()->flash('alert-danger', 'File uploaded must be in csv format!');
                redirect('admin/groceries/import-csv');
            }

            $name = time().'.'.$file->getClientOriginalExtension();

            $file->move('csv', $name);

            Excel::load('public/csv/'.$name, function($reader) {
                $results = $reader->toArray();
                foreach($results as $result) {
                   $this->process_csv_bit($result);
                }

                redirect('admin/groceries');
            });
        }

        return view('admin.products.upload_csv', compact('active_menu', 'active_submenu'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'name'          => 'required',
            'category'      => 'required',
            'price'         => 'required',
            'stock'         => 'required|numeric',
            'minimum_stock_alert' => 'required',
            'barcode'       => 'numeric',
            'qpp'           => 'required',
            'metric'        => 'required',
            'brand'         => 'required',
            'nature'        => 'required',
            'state'         => 'required',
            'supplier'   => 'required',
            'availabity_note' => 'required',
            'packing'       => 'required',
            'image_two'     => 'mimes:jpeg,png,bmp,gif',
            'image_one'     => 'mimes:jpeg,png,bmp,gif',
            'image_three'   => 'mimes:jpeg,png,bmp,gif',
            'image_four'    => 'mimes:jpeg,png,bmp,gif',
            'image_five'    => 'mimes:jpeg,png,bmp,gif',
            'poster_two'     => 'mimes:jpeg,png,bmp,gif',
            'poster_one'     => 'mimes:jpeg,png,bmp,gif',
            'poster_three'   => 'mimes:jpeg,png,bmp,gif',
            'poster_four'    => 'mimes:jpeg,png,bmp,gif',
            'poster_five'    => 'mimes:jpeg,png,bmp,gif'
        ]);

        if ($validator->fails()) {
            return redirect('admin/groceries/create')
                        ->withErrors($validator)
                        ->withInput();
        }

        $exp_date = explode('/',$request->expiry_date);

        if(count($exp_date) == 1) {
            $std = explode(' ', $request->expiry_date);
            $expdate = $std[0];
        } else {
            $expdate = $exp_date[2].'-'.$exp_date[0].'-'.$exp_date[1];
        }

        #configure discount
        $price_after_discount = 0;
        if($request->discount_id != 0) {
            $price_after_discount = $this->calculateDiscount($request->price, $request->discount_id);
        } else {
            $price_after_discount = $request->price;
        }

        $gr_id = DB::table('groceries')->insertGetId(
            [
                'name' => $request->name,
                'fresh_sea_water'   => $request->fresh_sea_water,
                'country_id' => $request->coo,
                'price' => str_replace(',', '', $request->price),
                'supplier_id' => 0,
                'stock' => $request->stock,
                'minimum_stock_alert' => $request->minimum_stock_alert,
                'cooking_advice' => $request->cooking_advice,
                'description' => $request->description,
                'nutrients' => $request->nutrients,
                'ingredients' => $request->ingredients,
                'sku' => $request->sku,
                'range_price' => $request->range_price,
                'availabity_note' => $request->availabity_note,
                'packing'       => $request->packing,
                'value_per_package' => $request->qpp,
                'youtube_link' => urlencode($request->youtube_link),
                'metric_id' => $request->metric,
                'brand_id' => $request->brand,
                'nature' => $request->nature,
                'state' => $request->state,
                'discount_id' => $request->discount,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'expiry_date' => $expdate,
                'price_after_discount' => $price_after_discount
            ]
        );

        $searchable = DB::table('searchable_groceries')->insertGetId(
            [
                'groceries_id' => $gr_id,
                'name' => $request->name,
                'is_active' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]
        );

        $grocery = Groceries::find($gr_id);

        if($request->image_one) {
            $file = $request->image_one;
            $name = uniqid().'1'.$gr_id.'.'.$file->getClientOriginalExtension();

            $file->move('contents', $name);

            $photo = new Image();
            $photo->file_name = $name;
            $photo->groceries_id = $grocery->id;

            $photo->save();
        }

        if($request->image_two) {
            $file = $request->image_two;
            $name = uniqid().'2'.$gr_id.'.'.$file->getClientOriginalExtension();

            $file->move('contents', $name);

            $photo = new Image();
            $photo->file_name = $name;
            $photo->groceries_id = $grocery->id;

            $photo->save();
        }

        if($request->image_three) {
            $file = $request->image_three;
            $name = uniqid().'3'.$gr_id.'.'.$file->getClientOriginalExtension();

            $file->move('contents', $name);

            $photo = new Image();
            $photo->file_name = $name;
            $photo->groceries_id = $grocery->id;

            $photo->save();
        }

        if($request->image_four) {
            $file = $request->image_four;
            $name = uniqid().'4'.$gr_id.'.'.$file->getClientOriginalExtension();

            $file->move('contents', $name);

            $photo = new Image();
            $photo->file_name = $name;
            $photo->groceries_id = $grocery->id;

            $photo->save();
        }

        if($request->image_five) {
            $file = $request->image_five;
            $name = uniqid().'5'.$gr_id.'.'.$file->getClientOriginalExtension();

            $file->move('contents', $name);

            $photo = new Image();
            $photo->file_name = $name;
            $photo->groceries_id = $grocery->id;

            $photo->save();
        }

        if($request->poster_one) {
            $file = $request->poster_one;
            $name = uniqid().'1'.$gr_id.'.'.$file->getClientOriginalExtension();

            $file->move('groceries_poster', $name);

            $photo = new Poster();
            $photo->file = $name;
            $photo->groceries_id = $grocery->id;

            $photo->save();
        }

        if($request->poster_two) {
            $file = $request->poster_two;
            $name = uniqid().'1'.$gr_id.'.'.$file->getClientOriginalExtension();

            $file->move('groceries_poster', $name);

            $photo = new Poster();
            $photo->file = $name;
            $photo->groceries_id = $grocery->id;

            $photo->save();
        }

        if($request->poster_three) {
            $file = $request->poster_three;
            $name = uniqid().'1'.$gr_id.'.'.$file->getClientOriginalExtension();

            $file->move('groceries_poster', $name);

            $photo = new Poster();
            $photo->file = $name;
            $photo->groceries_id = $grocery->id;

            $photo->save();
        }

        if($request->poster_four) {
            $file = $request->poster_four;
            $name = uniqid().'1'.$gr_id.'.'.$file->getClientOriginalExtension();

            $file->move('groceries_poster', $name);

            $photo = new Poster();
            $photo->file = $name;
            $photo->groceries_id = $grocery->id;

            $photo->save();
        }

        if($request->poster_five) {
            $file = $request->poster_five;
            $name = uniqid().'1'.$gr_id.'.'.$file->getClientOriginalExtension();

            $file->move('groceries_poster', $name);

            $photo = new Poster();
            $photo->file = $name;
            $photo->groceries_id = $grocery->id;

            $photo->save();
        }

        if($request->poster_six) {
            $file = $request->poster_six;
            $name = uniqid().'1'.$gr_id.'.'.$file->getClientOriginalExtension();

            $file->move('groceries_poster', $name);

            $photo = new Poster();
            $photo->file = $name;
            $photo->groceries_id = $grocery->id;

            $photo->save();
        }

        if($request->poster_seven) {
            $file = $request->poster_seven;
            $name = uniqid().'1'.$gr_id.'.'.$file->getClientOriginalExtension();

            $file->move('groceries_poster', $name);

            $photo = new Poster();
            $photo->file = $name;
            $photo->groceries_id = $grocery->id;

            $photo->save();
        }


        if($request->tags) {
            foreach($request->tags as $tag) {
                // find existing tag
                $tag_exist = Tag::where('name', $tag)->lists('id')->all();

                if (count($tag_exist) > 0) {
                    $grocery->tags()->attach($tag_exist[0]);
                } else {
                    $tag_new = DB::table('tags')->insertGetId(
                        [
                            'name' => $tag
                        ]
                    );
                    $grocery->tags()->attach($tag_new);
                }
            }
        }

        if($request->category) {
            foreach($request->category as $category) {
                $grocery->category()->attach($category);
            }
        }

        if($request->supplier) {
            foreach($request->supplier as $supplier) {
                $grocery->suppliers()->attach($supplier);
            }
        }

        if($gr_id) {
            $request->session()->flash('alert-success', 'Grocery was successfully added!');
        } else {
            $request->session()->flash('alert-danger', 'Oops, something is wrong during adding grocery!');
        }

        return redirect('admin/groceries');
    }

    public function store_image(Request $request, $id){

        $this->validate($request, [
            'file'          => 'mimes:jpeg,bmp,png',
        ]);

        if($file = $request->file('file')) {

            $name = time().$id.'.'.$file->getClientOriginalExtension();

            $file->move('contents', $name);

            $photo = new Image();
            $photo->file_name = $name;
            $photo->groceries_id = $id;
            //$photo = Image::create(['file_name' => $name, 'groceries_id' => $id]);

            $status = $photo->save();

            if($status) {
                $request->session()->flash('alert-success', 'Grocery image was successfully added!');
            } else {
                $request->session()->flash('alert-danger', 'Oops, something is wrong during adding grocery image!');

                return back();
            }


            return redirect('admin/groceries/'.$id);
        }
    }

    /**
     * Handles the file upload
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @throws UploadMissingFileException
     * @throws \Pion\Laravel\ChunkUpload\Exceptions\UploadFailedException
     */

    public function uploadChunk(Request $request) {
        // create the file receiver
        $receiver = new FileReceiver("file", $request, HandlerFactory::classFromRequest($request));

        // check if the upload is success, throw exception or return response you need
        if ($receiver->isUploaded() === false) {
            throw new UploadMissingFileException();
        }

        // receive the file
        $save = $receiver->receive();

        // check if the upload has finished (in chunk mode it will send smaller files)
        if ($save->isFinished()) {
            // save the file and return any response you need, current example uses `move` function. If you are
            // not using move, you need to manually delete the file by unlink($save->getFile()->getPathname())
            return $this->saveFile($save->getFile());
        }

        // we are in chunk mode, lets send the current progress
        /** @var AbstractHandler $handler */
        $handler = $save->handler();

        return response()->json([
            "done" => $handler->getPercentageDone(),
        ]);
    }

    /**
     * Saves the file
     *
     * @param UploadedFile $file
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function saveFile(UploadedFile $file)
    {
        $fileName = $this->createFilename($file);
        // Group files by mime type
        $mime = str_replace('/', '-', $file->getMimeType());
        // Group files by the date (week
        $dateFolder = date("Y-m-W");
        // Build the file path
        $filePath = "upload/{$mime}/{$dateFolder}/";
        $finalPath = storage_path("app/".$filePath);
        // move the file name
        $file->move($finalPath, $fileName);
        return response()->json([
            'path' => $filePath,
            'name' => $fileName,
            'mime_type' => $mime
        ]);
    }
    /**
     * Create unique filename for uploaded file
     * @param UploadedFile $file
     * @return string
     */
    protected function createFilename(UploadedFile $file)
    {
        $extension = $file->getClientOriginalExtension();
        $filename = str_replace(".".$extension, "", $file->getClientOriginalName()); // Filename without extension
        // Add timestamp hash to name of the file
        $filename .= "_" . md5(time()) . "." . $extension;
        return $filename;
    }

    /**
     * @param Request $request
     * @param $id
     * @param $image_id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete_image(Request $request, $id, $image_id){

        $images = Image::find($image_id);
        if (!$images)
            return redirect('admin/groceries/'.$id);
        $status = $images->delete();

        if($status) {
            /*Storage::delete('public/contents/'.$images->filename);*/
            $request->session()->flash('alert-success', 'Grocery image was successfully deleted!');
        } else {
            $request->session()->flash('alert-danger', 'Oops, something is wrong during deleting grocery image!');

            return back();
        }

        return redirect('admin/groceries/'.$id);
    }

    public function store_poster(Request $request, $id){

        $this->validate($request, [
            'file'          => 'mimes:jpeg,bmp,png',
        ]);

        if($file = $request->file('file')) {

            $name = time().$id.'.'.$file->getClientOriginalExtension();

            $file->move('groceries_poster', $name);

            $photo = new Poster();
            $photo->file = $name;
            $photo->groceries_id = $id;
            //$photo = Image::create(['file_name' => $name, 'groceries_id' => $id]);

            $status = $photo->save();

            if($status) {
                $request->session()->flash('alert-success', 'Grocery poster was successfully added!');
            } else {
                $request->session()->flash('alert-danger', 'Oops, something is wrong during adding grocery poster!');

                return back();
            }


            return redirect('admin/groceries/'.$id);
        }
    }

    /**
     * @param Request $request
     * @param $id
     * @param $image_id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete_poster(Request $request, $id, $poster_id){

        $poster = Poster::find($poster_id);
        if (!$poster)
            return redirect('admin/groceries/'.$id);
        $status = $poster->delete();

        if($status) {
            /*Storage::delete('public/contents/'.$images->filename);*/
            $request->session()->flash('alert-success', 'Grocery poster was successfully deleted!');
        } else {
            $request->session()->flash('alert-danger', 'Oops, something is wrong during deleting grocery poster!');

            return back();
        }

        return redirect('admin/groceries/'.$id);
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        //
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));
        $product = Groceries::find($id);

        if($product->deleted != 0) {
            $request->session()->flash('alert-danger', 'This groceries has been deleted!');
            return back();
        }

        $images = $product->getAllImages($id);
        $posters = $product->getAllPosters($id);
        $youtube_id = $product->getYoutubeId($product->youtube_link);
        $vid = Video::where('groceries_id',$id)->lists('file_name')->all();

        return view('admin.products.show', compact('product', 'images','vid', 'active_menu', 'active_submenu', 'youtube_id', 'posters'));
    }

    public function getSuppliers(Request $request,$id){

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        //

        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));
        $product = Groceries::find($id);
        $categories = ChildCategory::where('is_active', 1)->lists('name', 'id')->all();
        $discounts = Discount::lists('title', 'id')->all();
        $brands = Brand::where('is_active', 1)->lists('name', 'id')->all();
        $metrics = Metric::where('is_active', 1)->lists('name', 'id')->all();
        $tags = Tag::lists('name', 'id')->all();
        $countries = Country::lists('name', 'id')->all();
        /*$cookingAdvice = CookingAdvice::lists('name', 'id')->all();*/

        $images = $product->getAllImages($id);
        $suppliers = UserSupplier::lists('company_name','id')->all();
        $posters = $product->getAllPosters($id);
        $vid = Video::where('groceries_id',$id)->lists('file_name')->all();
        if($product->youtube_link != null) {
            $product->youtube_link = urldecode($product->youtube_link);
        }

        return view('admin.products.edit', compact('product', 'categories', 'suppliers','discounts', 'brands', 'metrics', 'tags', 'active_menu', 'active_submenu', 'countries', 'cookingAdvice', 'images', 'posters','vid'));
    }
        public function uploadvideo($id, Request $request){
        if($file= $request->file('uploadFile')){
            $video_name = time().$file->getClientOriginalName();
            $file->move('videogroceries/', $video_name);

            $video = new Video;
            $video->file_name = $video_name;
            $video->groceries_id = $id;
            $video->created_at = date('Y-m-d H:i:s');
            $video->updated_at = date('Y-m-d H:i:s');
            $video->save();
            echo "Video berhasil diupload!";

        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $product = Groceries::find($id);

        $input = $request->all();

        $input['brand_id'] = $input['brand'];
        $exp_date = explode('/',$request->expiry_date);

        if(count($exp_date) > 1) {
            $input['expiry_date'] = $exp_date[2].'-'.$exp_date[0].'-'.$exp_date[1];
        }

        if($product->discount_id != $input['discount_id']) {
            if($input['discount_id'] == 0 || $input['discount_id'] == null) {
                $input['price_after_discount'] = $product->price_after_discount;
            } else {
                $price_after_discount = $this->calculateDiscount($product->price, $input['discount_id']);
                $input['price_after_discount'] = $price_after_discount;
            }
        }

        $status = $product->update($input);
        //
        // $product->availabity_note = ;
        // $product->save();

        if($input['category']) {
            $product->category()->detach();
            foreach($input['category'] as $category) {
                $product->category()->attach($category);
            }
        }

        if($input['supplier']){
            $product->suppliers()->detach();
            foreach($input['supplier'] as $supplier) {
                $product->suppliers()->attach($supplier);
            }
        }

        /*if(in_array('cooking_advice', $input)) {
            $product->cookingAdvice()->detach();
            foreach($input['cooking_advice'] as $cooking_advice) {

                // find existing tag
                $tag_exist = CookingAdvice::find($cooking_advice);

                if (!empty($tag_exist)) {
                    $product->cookingAdvice()->attach($tag_exist);
                } else {
                    $cooking_advice_new = DB::table('cooking_advices')->insertGetId(
                        [
                            'name' => $cooking_advice,
                            'created_at' => date('Y-m-d H:i:s'),
                            'updated_at' => date('Y-m-d H:i:s')
                        ]
                    );
                    $product->cookingAdvice()->attach($cooking_advice_new);
                }
            }
        }*/

        if($status) {
            $request->session()->flash('alert-success', 'Grocery was successfully updated!');
        } else {
            $request->session()->flash('alert-danger', 'Oops, something is wrong during updating grocery!');

            return back();
        }

        return redirect('admin/groceries');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        /*$status = Groceries::find($id)->delete();*/

        # inactivate grocery
        $target_groceries = Groceries::find($id);
        $target_groceries->deleted = 1;
        $status = $target_groceries->save();

        #inactivate search grocery
        $search = SearchableGroceries::where('groceries_id', $id);
        $search->is_active = 0;
        $search->save();

        if($status) {
            $request->session()->flash('alert-success', 'Grocery was successfully deleted!');
        } else {
            $request->session()->flash('alert-danger', 'Oops, something is wrong during deleting grocery!');

            return back();
        }

        return redirect('admin/groceries');

    }
}
