<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Groceries;
use DB;
use App\Models\userSupplier;

use LocalizedCarbon;

class StatisticController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */



    public function getIndex(Request $request)
    {
        $active_menu = $request->segment(2);
        $active_submenu = null;

        $dateNow = LocalizedCarbon::now();

        $data = Array(
            "omzet" => Array(
                "totalAll" => $this->getTotalOmzet(),
                "monthly" => $this->getOmzetOnMonth($dateNow->month,$dateNow->year)
            ),
            "customers" => Array(
                "totalAll" => $this->getTotalCustomer(),
                "monthly" => $this->getCustomerWithin($dateNow->month,$dateNow->year)
            ),
            "order" => Array(
                "totalAll" => $this->getTotalOrder(),
                "monthly" => $this->getOrderOnMonth($dateNow->month,$dateNow->year)
            ),
            "products" => Array(
                "totalAll" =>  $this->getTotalProduct(),
                "monthly" => $this->getProductOnMonth($dateNow->month,$dateNow->year)
            ),
            "suppliers" => Array(
                "totalAll" =>  $this->getTotalSupplier(),
                "monthly" => $this->getSuppliersOnMonth($dateNow->month,$dateNow->year)
            ),
            "top20Products" => $this->getTop20ProductsByTotalOrder(),
            "top20Customers" => $this->getTop20CustomersByTotalOrder()
        );



        return view('admin.statistics.index',compact('data','active_menu','active_submenu'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */



     private function countGrowth($currentMonth,$previousMonth){
         if($previousMonth == 0){
             return 0;
         }
         return number_format(($currentMonth - $previousMonth) / ($previousMonth) * 100,1);
     }


     //Omset-------------------------------------
     //Will return json with
     // 1. Total getOmzet where status is on delivery,processing order, complete, payment made and confirm
     private function getTotalOmzet(){
         return Order::whereIn('order_status',['4','6','7','3','2'])->sum('nett_total');
     }

     //will return json within
     //1. Total omzet on specific month and $year
     //2. return the number of growth
     private function getOmzetOnMonth($month,$year){
         $currentMonthOmzet = Order::whereIn('order_status',['4','6','7','3','2'])
                                    ->whereYear('created_at','=',$year)
                                    ->whereMonth('created_at','=',$month)
                                    ->sum('nett_total');

        if($month == 1){
            $previousMonth = 12;
            $year = $year - 1;
        }else{
            $previousMonth = $month - 1;
        }

        $previousMonthOmzet = Order::whereYear('created_at','=',$year)
                                    ->whereIn('order_status',['4','6','7','3','2'])
                                   ->whereMonth('created_at','=',$previousMonth)
                                   ->sum('nett_total');



        return Array(
            "current" => $currentMonthOmzet,
            "previous" => $previousMonthOmzet,
            "growth" => $this->countGrowth($currentMonthOmzet,$previousMonthOmzet)
        );

     }



     //Customers-------------------------------------
     //will return value of
     //1. Total customer on table
     private function getTotalCustomer($category=""){

     }

     //will return value of
     //1. Total customer on specific month and $year
     //2. return the number of customer growth
     private function getCustomerWithin($month,$year,$category=""){

     }

     //will return customer that ordered more than x
     private function getRetainCustomer(){

     }


     //return the number of order
     private function getTotalOrder(){
         return Order::all()->count();
     }

     private function getTotalProduct(){
         return Groceries::all()->count();
     }

     private function getProductOnMonth($month,$year){
         $currentMonthProducts = Groceries::whereYear('created_at',"=",$year)
                                    ->whereMonth('created_at',"=",$month)
                                    ->count();
        if($month == 1){
            $previousMonth = 12;
            $year = $year - 1;
        }else{
            $previousMonth = $month - 1;
        }

        $previousMonthProducts = Groceries::whereYear('created_at',"=",$year)
                                   ->whereMonth('created_at',"=",$previousMonth)
                                   ->count();

       return Array(
           "current" => $currentMonthProducts,
           "previous" => $previousMonthProducts,
           "growth" => $this->countGrowth($currentMonthProducts,$previousMonthProducts)
       );

     }

     private function getTotalSupplier(){
         return userSupplier::all()->count();
     }

     private function getSuppliersOnMonth($month,$year){
         $currentMonthSuppliers = userSupplier::whereYear('created_at',"=",$year)
                                    ->whereMonth('created_at',"=",$month)
                                    ->count();
        if($month == 1){
            $previousMonth = 12;
            $year = $year - 1;
        }else{
            $previousMonth = $month - 1;
        }

        $previousMonthSuppliers = userSupplier::whereYear('created_at',"=",$year)
                                   ->whereMonth('created_at',"=",$previousMonth)
                                   ->count();

       return Array(
           "current" => $currentMonthSuppliers,
           "previous" => $previousMonthSuppliers,
           "growth" => $this->countGrowth($currentMonthSuppliers,$previousMonthSuppliers)
       );

     }

     //will return value of
     //1. Total order on specific month and $year
     //2. return the number of order growth
     private function getOrderOnMonth($month,$year){
         $currentMonthOrder = Order::whereYear('created_at',"=",$year)
                                    ->whereMonth('created_at',"=",$month)
                                    ->count();

        if($month == 1){
            $previousMonth = 12;
            $year = $year - 1;
        }else{
            $previousMonth = $month - 1;
        }

        $previousMonthOrder = Order::whereYear('created_at',"=",$year)
                                   ->whereMonth('created_at',"=",$previousMonth)
                                   ->count();

        return Array(
            "current" => $currentMonthOrder,
            "previous" => $previousMonthOrder,
            "growth" => $this->countGrowth($currentMonthOrder,$previousMonthOrder)
        );
     }

     private function getTop20ProductsByTotalOrder(){
        $top20Products = DB::select("SELECT
                                    a.id,a.name,sum(b.qty) as qty
                                    FROM
                                    groceries a INNER JOIN order_groceries b ON a.id = b.groceries_id
                                    group by b.groceries_id
                                    order by qty desc
                                    limit 20
                                    ");
        return $top20Products;
     }


     private function getTop20CustomersByTotalOrder(){
         $top20Users = DB::select("SELECT
                                    a.id,a.company_name,count(b.user_id) as total_order
                                    from
                                    users a INNER JOIN orders b ON a.id = b.user_id
                                    group by b.user_id
                                    order by total_order desc
                                    limit 20");
        return $top20Users;

     }


     private function getTop20SuppliersByTotalSales(){
        //  $top20Suppliers = DB::select("SELECT
        //                             a.id,a.company_name,count(c.supplier_id) as total_order
        //                             from
        //                             users a
        //                             INNER JOIN orders b ON a.id = b.user_id
        //
        //                             group by b.user_id
        //                             order by total_order desc
        //                             limit 20");
        // return $top20Suppliers;
     }












}
