<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\StaticPage\StaticPage;
use App\Models\StaticPage\StaticDescription;
use App\Models\StaticPage\StaticImage;

class StaticDescriptionController extends Controller
{
    protected $active_menu = null;
    protected $active_submenu = null;

    public function getActiveMenu($active_menu) {
        $this->active_menu = $active_menu;

        return $this->active_menu;
    }

    public function getActiveSubmenu($active_menu) {
        $this->active_menu = $active_menu;

        return $this->active_menu;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));
        $pages = StaticPage::lists('page_name', 'id')->all();
        return view('admin.pages.create_description', compact('pages', 'active_menu', 'active_submenu'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $desc = new StaticDescription;
        $desc->page_id = $request->page_id;
        $desc->description_position = $request->description_position;
        $desc->description_content = $request->description_content;
        $status = $desc->save();


        if($status) {
            $request->session()->flash('alert-success', 'Description was successfully added!');
        } else {
            $request->session()->flash('alert-danger', 'Oops, something is wrong during adding description!');
        }

        return redirect('admin/static-description/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $description = StaticDescription::find($id);

        if($description != null) {

            if($request->description_position != '') {
                $description->description_position = $request->description_position;    
            }
            $description->description_content = $request->description_content;
            $status = $description->save();

            if($status) {
                $request->session()->flash('alert-success', 'Description was successfully edited!');
            } else {
                $request->session()->flash('alert-danger', 'Oops, something is wrong during editing description!');
            }

            return redirect('admin/pages/'.$description->page_id.'/edit');
        } else {
            $request->session()->flash('alert-danger', 'Oops, we can\'t find the description you\re looking for!');
            return redirect('admin/pages/');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
