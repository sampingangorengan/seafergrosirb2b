<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\StaticPage\StaticPage;
use App\Models\StaticPage\StaticDescription;
use App\Models\StaticPage\StaticImage;

class StaticImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $photo = StaticImage::find($id);

        /* change static desc to handle image upload */
        if($request->file) {
            $file = $request->file;
            $name = $photo->page_id.'_'.$photo->image_location.'.'.$file->getClientOriginalExtension();

            $path_to_directory = 'static_images/'.$photo->page_id.'/';

            if (!is_dir($path_to_directory)) {
                mkdir($path_to_directory, 0777, true);
            }

            if(file_exists($path_to_directory.$name)) {

                unlink($path_to_directory.$name);
            }

            $file->move($path_to_directory, $name);


            $photo->image_name = $path_to_directory.$name;
            $photo->image_location_detail = $request->image_location_detail;

            $status = $photo->save();

            if($status) {
                $request->session()->flash('alert-success', 'Description was successfully added!');
            } else {
                $request->session()->flash('alert-danger', 'Oops, something is wrong during adding description!');
            }

            return redirect('admin/pages/'.$photo->page_id.'/edit');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
