<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\userSupplier;
use \Cviebrock\EloquentSluggable\Services\SlugService;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\Location\Area;
use App\Models\City;
use App\Models\Province;

class SupplierController extends Controller
{
    protected $active_menu = null;
    protected $active_submenu = null;

    public function getActiveMenu($active_menu) {
        $this->active_menu = $active_menu;

        return $this->active_menu;
    }

    public function getActiveSubmenu($active_menu) {
        $this->active_menu = $active_menu;

        return $this->active_menu;
    }
    public function index(Request $request)
    {
        //
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));
        $suppliers = UserSupplier::all();
        return view('admin.supplier.supplier_list', compact('suppliers', 'active_menu', 'active_submenu'));
    }
    public function show(Request $request, $id)
    {
        //
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));

        if (!auth()->check())
            return redirect('/admin/login');

        $supplier = userSupplier::find($id);

        return view('admin.supplier.show', compact('supplier', 'active_menu', 'active_submenu'));
    }
    public function destroy(Request $request,$id)
    {
        if(auth()->user()->role != 1)
        {
            $request->session()->flash('alert-danger', 'Unauthorized Access!');
            return back();
        }

        $supplier = userSupplier::find($id);

        $status = $supplier->delete();

        if($status) {
            $request->session()->flash('alert-success', 'Supplier was successfully deleted!');
        } else {
            $request->session()->flash('alert-danger', 'Oops, something is wrong during deleting user!');

            return back();
        }

        return redirect('admin/supplier');
    }

    public function edit(Request $request, $id){
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));

        $user = userSupplier::find($id);
        $countries = Country::get();
        $areas = Area::orderBy('name', 'asc')->lists('name', 'id')->all();
        $cities = City::orderBy('city_name', 'asc')->lists('city_name', 'city_id')->all();
        $provinces = Province::orderBy('province_name', 'asc')->lists('province_name', 'province_id')->all();
        return view('admin.supplier.edit', compact('user', 'provinces', 'cities', 'areas','countries', 'active_menu', 'active_submenu'));
    }

    public function update($id,Request $request)
    {


        $this->validate($request, [
            'company_name'    => 'required',
            'business_entity' => 'required',
            'billing_address' => 'required',
            'province'        => 'required',
            'city'            => 'required',
            'area'            => 'required',
            'phone_number'    => 'required|numeric',
            'email'           => 'required|unique:users,email|email',
            'category'        => 'required',
            'Commoditas'        => 'required',
            'selling_method'        => 'required',
            'type_of_goods'     => 'required',
            'country'   => 'required',
        ],[
            'required' => 'The :attribute field is required.'
        ]);


        $user = userSupplier::findOrFail($id);

        $input = $request->all();
        $status = $user->update($input);

        $user->Commoditas = $request->Commoditas;
        $user->save();

        if($status) {
            $request->session()->flash('alert-success', 'Profile berhasil diperbaharui!!');
        } else {
            $request->session()->flash('alert-danger', 'Oops, Terjadi kesalahan saat melakukan pembaharuan profile!');

            return back();
        }

        return redirect('admin/supplier');
    }


}
