<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\User\CashbackReward;
use App\Models\User\CashbackPoint;
use App\Models\Role;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    protected $active_menu = null;
    protected $active_submenu = null;

    public function getActiveMenu($active_menu) {
        $this->active_menu = $active_menu;

        return $this->active_menu;
    }

    public function getActiveSubmenu($active_menu) {
        $this->active_menu = $active_menu;

        return $this->active_menu;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));

        if (!auth()->check())
            return redirect('/admin/login');

        if (auth()->user()->role != 1)
            return redirect('/admin/login');
        $users = User::get();

        return view('admin.users.index', compact('users', 'active_menu', 'active_submenu'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if (!auth()->check())
            return redirect('/admin/login');

        if (auth()->user()->role != 1)
            return redirect('/admin/login');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        //
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));

        if (!auth()->check())
            return redirect('/admin/login');

        $user = User::find($id);

        $cashback_rewards = $user->rewards->sortByDesc('created_at');

        $cashback_point = $user->points;

        return view('admin.users.show', compact('user', 'active_menu', 'active_submenu', 'cashback_point', 'cashback_rewards'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        //
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));

        $user = User::find($id);
        $roles = Role::lists('name', 'id')->all();
        return view('admin.users.edit', compact('user', 'roles', 'active_menu', 'active_submenu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $user = User::findOrFail($id);

        $input = $request->all();
        $user->is_active = $input["is_active"];
        $user->role = $input["role"];
        $user->save();  

        return redirect('admin/users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        if(auth()->user()->role != 1)
        {
            $request->session()->flash('alert-danger', 'Unauthorized Access!');
            return back();
        }

        $user = User::find($id);

        $status = $user->delete();

        if($status) {
            $request->session()->flash('alert-success', 'User was successfully deleted!');
        } else {
            $request->session()->flash('alert-danger', 'Oops, something is wrong during deleting user!');

            return back();
        }

        return redirect('admin/users');
    }

}
