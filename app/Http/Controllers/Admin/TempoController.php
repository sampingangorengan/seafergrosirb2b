<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\User\Tempo;
use App\Http\Controllers\Controller;
use Mail;
use Session;

class TempoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $active_menu = null;
    protected $active_submenu = null;

    public function index(Request $request)
    {
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));
        $status = $request->get('status');

        if($status != null){
            $user_tempo = Tempo::where('status',$status)->get();

        }else{
            $user_tempo = Tempo::all();
        }

        return view('admin.tempo.index', compact('user_tempo','active_menu', 'active_submenu','status'));

    }

    public function show(){

    }

    public function edit(Request $request,$tempo_id){
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));
        $tempo = Tempo::find($tempo_id);


        return view('admin.tempo.edit',compact("tempo",'active_menu', 'active_submenu'));
    }

    public function update(Request $request,$tempo_id){

        $this->validate($request, [
            'status'  => 'in:approved,unsuccessful',
            'aggrement'     => 'required',
            'duration'     => ['required','numeric']
        ]);

        $tempo = Tempo::findOrFail($tempo_id);

        $tempo->aggrement = $request->aggrement;
        $tempo->status = $request->status;
        $tempo->duration = $request->duration;
        $tempo->update();

        $msg = "";
        $name = $tempo->user->name;
        $mail = $tempo->user->email;

        if($request->status == "approved"){
            $msg .= "<p>Selamat atas kesuksesan aplikasi pembayaran tempo Anda! Setelah meninjau permohonan Anda, tim kredit kami telah memutuskan untuk memberikan jangka waktu pembayaran Anda ".$request->duration." hari dari pengiriman pesanan.</p>
            <p>Tim kredit kami akan menghubungi anda untuk membantu lebih lanjut. Terima Kasih, Sahabat Seafer Grosir.</p>";
            $sbj = "Selamat atas kesuksesan aplikasi pembayaran tempo Anda!";
        }else{
            $msg .= "<p>Setelah meninjau permohonan Anda, tim kredit kami sangat menyesal untuk memberitahu Anda bahwa kami tidak dapat memberikan Anda pembayaran Tempo saat ini. Mohon mencoba mengajukan permohonan lagi dalam waktu 1 bulan.<p>
             <p>Jika Anda membutuhkan bantuan lebih lanjut, silakan hubungi layanan pelanggan kami.</p>
            <p>Kami minta maaf atas ketidaknyamanan ini dan terima Kasih, Sahabat Seafer Grosir.</p>";
            $sbj = "Maaf, Applikasi Seafer Grosir Pembayaran Tempo Anda Gagal";
        }

        try{
            Mail::send('emails.general', compact('name','msg'), function ($message) use ($mail,$sbj) {

                $message->from("hello@seafermart.co.id", "Seafer Grosir Payment Notice");
                $message->to($mail)->subject($sbj);

            });
            return redirect('admin/tempo');
        } catch (\Exception $exception) {
            Session::flash('alert-danger', 'Failed to send your success email. Please try again in a few moments!');

            return redirect()->back();
        }


    }

    private function getActiveMenu($active_menu) {
        $this->active_menu = $active_menu;

        return $this->active_menu;
    }

    private function getActiveSubmenu($active_menu) {
        $this->active_menu = $active_menu;

        return $this->active_menu;
    }


}
