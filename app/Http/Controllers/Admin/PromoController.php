<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Promo;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class PromoController extends Controller
{
    protected $active_menu = null;
    protected $active_submenu = null;

    public function getActiveMenu($active_menu) {
        $this->active_menu = $active_menu;

        return $this->active_menu;
    }

    public function getActiveSubmenu($active_menu) {
        $this->active_menu = $active_menu;

        return $this->active_menu;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));
        $promos = Promo::get();
        return view('admin.promos.index', compact('promos', 'active_menu', 'active_submenu'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));
        return view('admin.promos.create', compact('active_menu', 'active_submenu'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request, [
            'title'          => 'required',
            'code'           => 'required',
            'start_date'     => 'required',
            'end_date'      => 'required',
            'quota'         => 'required|numeric',
            'promo_type'    => 'required',
            'discount_value'=> 'required'
        ]);

        $promo = new Promo;
        $promo->title = $request->title;
        $promo->code = $request->code;
        $promo->description = $request->description;
        $promo->promo_type = $request->promo_type;
        $promo->discount_value = $request->discount_value;
        $promo->quota = $request->quota;
        $promo->start_date = $request->start_date;
        $promo->end_date = $request->end_date;


        $status = $promo->save();

        if($status) {
            $request->session()->flash('alert-success', 'Promo was successfully added!');
        } else {
            $request->session()->flash('alert-danger', 'Oops something\'s wrong while creating promo.');
        }

        return redirect('admin/promos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        //
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));
        $promo = Promo::findOrFail($id);
        return view('admin.promos.show', compact('promo', 'active_menu', 'active_submenu'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        //
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));
        $promo = Promo::findOrFail($id);
        return view('admin.promos.edit', compact('promo', 'active_menu', 'active_submenu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
            'title'          => 'required',
            'code'           => 'required|unique:promos,code,'.$id,
            'start_date'     => 'required',
            'end_date'      => 'required',
            'quota'         => 'required|numeric',
            'promo_type'    => 'required',
            'discount_value'=> 'required'
        ]);

        $st_date = explode('/',$request->start_date);
        $en_date = explode('/',$request->end_date);

        $promo = Promo::find($id);

        $input = $request->all();

        if(count($st_date) == 1) {
            $std = explode(' ', $request->start_date);
            $input['start_date'] = $std[0];
        } else {
            $input['start_date'] = $st_date[2].'-'.$st_date[0].'-'.$st_date[1];    
        }

        if(count($en_date) == 1) {
            $input['end_date'] = $en_date[0];
        } else {
            $input['end_date'] = $en_date[2].'-'.$en_date[0].'-'.$en_date[1];
        }

        $status = $promo->update($input);

        if($status) {
            $request->session()->flash('alert-success', 'Promo was successfully updated!');
        } else {
            $request->session()->flash('alert-danger', 'Oops, something is wrong during updating promo!');

            return back();
        }


        return redirect('admin/promos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        //
        $status = Promo::find($id)->delete();

        if($status) {
            $request->session()->flash('alert-success', 'Promo was successfully deleted!');
        } else {
            $request->session()->flash('alert-danger', 'Oops, something is wrong during deleting promo!');

            return back();
        }

        return redirect('admin/promos');
    }
}
