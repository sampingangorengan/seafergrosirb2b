<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Sarav\Multiauth\Foundation\AuthenticatesAndRegistersUsers;
use App\Models\userSupplier;
use App\Models\Groceries;
use App\Models\Country;
use App\Models\Location\Area;
use App\Models\City;
use App\Models\Province;
use Mail;
use App\Models\Order\Groceries as OrderGroceries;
//use App\Models\User;

class SupplierController extends Controller
{
    protected $active_menu = null;
    protected $active_submenu = null;

    public function getActiveMenu($active_menu) {
        $this->active_menu = $active_menu;

        return $this->active_menu;
    }

    public function getActiveSubmenu($active_menu) {
        $this->active_menu = $active_menu;

        return $this->active_menu;
    }
     public function __construct(){
            $this->user = "supplier";
    }

    public function index(request $request)
    {
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));

        $users = userSupplier::get();


        return view('frontend.supplier.index', compact('users', 'active_menu', 'active_submenu'));
    }

    public function sign_in(Request $request){
        $credentials = ['email' => $request->email, 'password' => $request->password];

        $remember = request('remember') ? true : false;

        // Login fails
        //if ( ! auth()->attempt($credentials, $remember)) {
        if ( ! \Auth::attempt('supplier',$credentials,$remember)) {
                $request->session()->flash('error', 'Oops, Email dan Password tidak cocok!');
                return back();
        }

        return redirect('/supplier/home');
    }

    public function profile(Request $request){
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));

        $user = auth()->user('supplier');

        return view('frontend.supplier.supplier_profile', compact('user', 'active_menu', 'active_submenu'));
    }
    public function editprofile(Request $request, $id){
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));

        $user = userSupplier::find($id);
        $countries = Country::get();
        $areas = Area::orderBy('name', 'asc')->lists('name', 'id')->all();
        $cities = City::orderBy('city_name', 'asc')->lists('city_name', 'city_id')->all();
        $provinces = Province::orderBy('province_name', 'asc')->lists('province_name', 'province_id')->all();
        return view('frontend.supplier.edit_profile', compact('user', 'provinces', 'cities', 'areas','countries', 'active_menu', 'active_submenu'));

    }
    public function update(Request $request,$id)
    {

        $this->validate($request, [
            'bussiness_entity' => 'required',
            'billing_address' => 'required',
            'province'        => 'required',
            'city'            => 'required',
            'area'            => 'required',
            'phone_number'    => 'required|numeric',
            'email'           => 'required|unique:users,email|email',
            'category'        => 'required',
            'Commoditas'        => 'required',
            'selling_method'        => 'required',
            'type_of_goods'     => 'required',
            'country'   => 'required',
        ],[
            'required' => 'The :attribute field is required.'
        ]);
        $user = userSupplier::findOrFail($id);

        $input = $request->all();

        $status = $user->update($input);
        $user->Commoditas = $request->Commoditas;
        $user->save();

        if($status) {
            $request->session()->flash('alert-success', 'Profile berhasil diperbaharui!!');
        } else {

            $request->session()->flash('alert-danger', 'Oops, Terjadi kesalahan saat melakukan pembaharuan profile!');

            return back();
        }

        return redirect('supplier/profile');
    }
    public function products(Request $request){
        //print_r(auth()->user('supplier')->user_name);
        //die();
        $active_menu = $this->getActiveMenu($request->segment(2));
        $active_submenu = $this->getActiveSubmenu($request->segment(2).'/'.$request->segment(3));
        $user=auth()->user('supplier')->id;


        $groceries = Groceries::where('supplier_id',$user)->get();

        return view('frontend.supplier.product', compact('groceries', 'active_menu', 'active_submenu'));
    }

    public function showProduct(Request $request,$grocery_id){
        $name= "Admin";
        $msg = "<p> product #".$grocery_id." is on low stocks";
        try{
            Mail::queue('emails.general', compact('name','msg'), function ($message) {

                $message->from('hello@foodis.co.id', '[NO-REPLY] Low Stock');

                $message->to(env('ADMIN_MAIL'),"Seafer Grosirindonesia@gmail.com");
            });
        }catch(Exception $e) {

        }
        $request->session()->flash('alert-success', 'Admin telah diinformasikan');
        return redirect('supplier/products');

    }
    public function getAjaxAreasByCity($id) {
        $area = Area::where('city_id', $id)->orderBy('name', 'asc')->lists('name', 'id')->all();

        if(count($area)>0){
            $result = array('status' => 'success', 'areas' => $area);
        } else {
            $result = array('status' => 'fail', 'message' => 'Area not found for this city');
        }
        return json_encode($result);
    }

    public function getAjaxCitiesByProvince($id){
        $city = City::where('province_id', $id)->orderBy('city_name_full', 'asc')->lists('city_name_full', 'city_id')->all();

        if(count($city)>0){
            $result = array('status' => 'success', 'cities' => $city);
        } else {
            $result = array('status' => 'fail', 'message' => 'City not found for this province');
        }
        return json_encode($result);
    }
}
/*last*/
