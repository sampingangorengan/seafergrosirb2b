<?php

namespace App\Http\Controllers\Order;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\PaymentProof;
use App\Models\Notification;
use App\Models\User\UserBank;
use App\Models\Order\Payment;
use Illuminate\Support\Facades\Mail;
use LocalizedCarbon;

class PaymentController extends Controller
{



    public function postTransferConfirmation(Request $request)
    {
        $this->validate($request, [
            'transfer_amount'=> 'required|numeric',
            'transfer_date'      => 'required',
            'transfer_time_minute'         => 'required|numeric',
            'transfer_time_second'         => 'required|numeric',
            'transfer_sender_bank_name'     => 'required',
            'transfer_sender_account_number'   => 'required',
            'transfer_sender_account_name'     => 'required',
            'file'          => 'mimes:jpeg,bmp,png',
            'order_number' => 'required|numeric'
        ]);



        $order = Order::find($request->order_number);

        if(null === $order && auth()->user()->id != $order->user_id ) {
            $request->session()->flash('alert-danger', 'Invalid order number!');
            return back();
        }

        $amount = str_replace('.','',$request->transfer_amount);

        $payment = Payment::where('order_id',$order->id)->first();
        $payment->transfer_dest_account = request('transfer_dest_account');
        $payment->transfer_sender_bank_name = strtoupper(request('transfer_sender_bank_name'));
        $payment->transfer_sender_account_number = request('transfer_sender_account_number');
        $payment->transfer_sender_account_name = request('transfer_sender_account_name');
        $payment->transfer_date = LocalizedCarbon::parse(request('transfer_date'))->format('Y-m-d');
        $payment->transfer_time = request('transfer_time_minute').':'.request('transfer_time_second');
        $payment->transfer_note = request('transfer_note');
        $payment->transfer_amount = $amount;
        $payment->notify_admin = 0;
        $payment->user_confirmed = 1;

        if($file = $request->file('file')) {

            $name = time() . $order->id . '.' . $file->getClientOriginalExtension();

            $file->move('payment_proof', $name);

            $photo = PaymentProof::create(['file' => $name]);

            $payment->transfer_receipt_file = (int) $photo->id;
        }

        $status = $payment->save();


        if($request->set_bank_preference == "on") {
            // check if preference exists
            $clear_preference = DB::table('user_banks')->where('user_id', auth()->user()->id)->update(['is_preference'=> 0]);
            $existing_preference = UserBank::where('account_name', $request->account_name)->where('account_number', $request->account_number)->get();
            if(null === $existing_preference || $existing_preference->count() == 0) {
                $bank = new UserBank;
                $bank->name = $request->transfer_sender_bank_name;
                $bank->account_name = $request->transfer_sender_account_name;
                $bank->account_number = $request->transfer_sender_account_number;
                $bank->user_id = auth()->user()->id;
                $bank->is_preference = 1;

                $bank->save();
            } else {

                $existing_preference->update(['is_preference' => 1]);
            }
        }

        if($order->count()>1)
        {
            $order->order_status = 6;
            $order->save();
        }

        $notif = new Notification;
        $notif->user_id = $order->user_id;
        $notif->title = 'Payment Proof Submitted';
        $notif->description = 'Bukti pembauaran berhasil dikirim';
        $notif->link = url('orders/'.$order->id);
        $notif->is_read = 0;
        $notif->save();


        $data = array(
            'name' => $payment->order->user->name,
            'email' => $payment->order->user->email,
        );

        try{
            $status = Mail::queue('emails.payment_process.admin_payment_sent_notice', $data, function ($message) use ($data) {

                $message->from("hello@seafermart.co.id", "Seafer Grosir Inquiry");
                $message->to("hello@seafermart.co.id")->subject("Payment Submitted!");

            });
        } catch (\Exception $exception) {
        }


        $request->session()->flash('alert-success', 'Thank you for your payment confirmation');
        return back();
        // return redirect('/orders');
    }
}
