<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\CashbackReward;
use App\Models\Location\Area;
use App\Models\City;
use App\Models\Groceries\Rating;
use App\Models\Order;
use App\Models\Province;
use App\Models\User\EmailPreference;
use App\Models\User\UserBank;
use App\Models\User\ViewHistory;
use App\Models\User\ReferralHistory;
use App\Models\Newsletter as MyNews;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Auth\Passwords\TokenRepositoryInterface;
use GuzzleHttp\Client;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\userSupplier;
use App\Models\CategoryCompany;
use Agent;
use Carte;
use App\Models\User\Address;
use App\Models\User\ProfilePicture;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\URL;
use Storage;
use App\Models\User\Tempo;
use Hash;


class UserController extends Controller
{

    use ResetsPasswords;

    private function sync_cart($userid) {
        $db_cart = Cart::where('user_id', $userid)->get();
        $session_cart = Carte::contents();

        if(null != $db_cart && ! empty($session_cart)) {

            //add quantity if exist
            foreach($db_cart as $cart) {
                foreach($session_cart as $s_cart){
                    if($cart->groceries_id == $s_cart->id && $cart->sku == $s_cart->sku) {

                        $cart->qty = $cart->qty + $s_cart->quantity;
                        $cart->quantity = $cart->quantity + $s_cart->quantity;
                        $status = $cart->save();

                        Carte::item($s_cart->identifier)->remove();
                    }
                }
            }


            foreach(Carte::contents() as $item){

                $cart = new Cart;
                $cart->user_id = $userid;
                $cart->groceries_id = $item->id;
                $cart->qty = $item->quantity;
                $cart->is_read = 0;
                $cart->name = $item->name;
                $cart->sku = $item->sku;
                $cart->price_per_item = $item->price_per_item;
                if(null == $item->unit_metric) {
                    $cart->unit_metric = 'n/a';
                } else {
                    $cart->unit_metric = $item->unit_metric;
                }

                $cart->price = $item->price;
                $cart->image = $item->image;
                $cart->quantity = $item->quantity;
                $status = $cart->save();

                $item->remove();
            }

            $updated_db_cart = Cart::where('user_id', $userid)->get();

            foreach($updated_db_cart as $d_cart){

                $item = Carte::insert([
                    'id'            => $d_cart->groceries_id,
                    'name'          => $d_cart->name,
                    'sku'           => $d_cart->sku,
                    'price_per_item'=> floatval($d_cart->price_per_item),
                    'unit_metric'   => $d_cart->unit_metric,
                    'price'         => floatval($d_cart->price),
                    'image'         => $d_cart->image,
                    'quantity'      => intval($d_cart->qty)
                ]);

            }
        } elseif(null != $db_cart && empty($session_cart)) {
            $updated_db_cart = Cart::where('user_id', $userid)->get();

            foreach($updated_db_cart as $d_cart){

                $item = Carte::insert([
                    'id'            => $d_cart->groceries_id,
                    'name'          => $d_cart->name,
                    'sku'           => $d_cart->sku,
                    'price_per_item'=> floatval($d_cart->price_per_item),
                    'unit_metric'   => $d_cart->unit_metric,
                    'price'         => floatval($d_cart->price),
                    'image'         => $d_cart->image,
                    'quantity'      => intval($d_cart->qty)
                ]);

            }
        }

        return Carte::contents(true);
    }

    private function checkAddressValidity(Address $address) {

        $client = new Client();

        $str_address = urlencode($address->address)
            .'+'.urlencode($address->area->name)
            . '+' . urlencode($address->city->city_name)
            . '+' . urlencode($address->city->province->province_name_id)
            .'+'.urlencode($address->postal_code);

        $string_address = str_replace(' ', '+', $str_address);

        $url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' . $string_address . '&key=AIzaSyCRDhe-WvUmE8Y96Gb6Glj-CPc9lfjC420';

        $res = $client->request('GET', $url);

        $results = json_decode($res->getBody())->results;

        if(null !== $results && !empty($results)) {
            return $results[0]->geometry->location->lat.', '.$results[0]->geometry->location->lng;
        }

        return false;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function getSignIn(Request $request)
    {
        $redirect = null;
        if(null !== $request->input('r')) {
            $redirect = $request->input('r');
        }

        return view('frontend.signin', compact('redirect'));

    }

    public function become_supplier(Request $request){
        if(null !== $request->input('refer_from')) {
            $request->session()->put('refer_from', $request->refer_from);
        }

        if(auth()->check()) {
            $request->session()->flash('alert-danger', 'Oops, you already have an account logged in the house!');
            return redirect('/');
        }
        $categorys = CategoryCompany::all();
        $areas = Area::orderBy('name', 'asc')->lists('name', 'id')->all();
        $cities = City::orderBy('city_name', 'asc')->lists('city_name', 'city_id')->all();
        $provinces = Province::orderBy('province_name', 'asc')->lists('province_name', 'province_id')->all();

        return view('frontend.supplier_benefits',compact('categorys', 'areas', 'cities', 'provinces'));

    }

    public function getSignUp(Request $request)
    {
        if(null !== $request->input('refer_from')) {
            $request->session()->put('refer_from', $request->refer_from);
        }

        if(auth()->check()) {
            $request->session()->flash('alert-danger', 'Oops, you already have an account logged in the house!');
            return redirect('/');
        }
        $categorys = CategoryCompany::all();
        $areas = Area::orderBy('name', 'asc')->lists('name', 'id')->all();
        $cities = City::orderBy('city_name', 'asc')->lists('city_name', 'city_id')->all();
        $provinces = Province::orderBy('province_name', 'asc')->lists('province_name', 'province_id')->all();

        return view('frontend.signup',compact('categorys', 'areas', 'cities', 'provinces'));

    }

    public function getSignOut()
    {
        auth()->logout();
        Carte::destroy();
        Session::flush();
        return redirect('/');
    }

    public function getForgotPassword()
    {
        return view('users.forgot_password');
    }

    public function getMyProfile(){
        if (auth()->check()) {
            $user = User::findOrFail(auth()->user()->id);
            $areas = Area::orderBy('name', 'asc')->lists('name', 'id')->all();
            $cities = City::orderBy('city_name', 'asc')->lists('city_name', 'city_id')->all();
            $provinces = Province::orderBy('province_name', 'asc')->lists('province_name', 'province_id')->all();
            $categorys = CategoryCompany::all();
            return view('frontend.user.my_profile', compact('user','categorys','provinces','cities','areas'));
        }
        return redirect('/');
    }
    public function getMyProfileSupplier(){

        //if (auth()->check()) {
            $user = User::findOrFail(auth()->user()->id);
            return view('frontend.supplier.my_profile', compact('user'));
        //}
        return redirect('/');
    }

    public function getMyAccount() {
        if (auth()->check()) {
            return view('frontend.user.my_account');
        }
        return redirect('user/sign-up');
    }

    public function getMyAddress() {
        if (auth()->check()) {
            $addresses = Address::where('user_id', auth()->user()->id)->where('deleted', 0)->get();

            return view('frontend.user.my_address_book', compact('addresses'));

        }
        return redirect('user/sign-up');
    }

    public function getAddNewAddress() {
        if (auth()->check()) {

            return view('frontend.user.add_new_address', compact('addresses'));

        }
        return redirect('user/sign-up');
    }

    public function getEditAddress(Request $request, $id) {

        if (auth()->check()) {
            $address = Address::find($id);
            $areas = Area::orderBy('name', 'asc')->lists('name', 'id')->all();
            $cities = City::orderBy('city_name', 'asc')->lists('city_name', 'city_id')->all();
            $provinces = Province::orderBy('province_name', 'asc')->lists('province_name', 'province_id')->all();

            return view('frontend.user.edit_address', compact('address', 'areas', 'cities', 'provinces'));
        }
        return redirect('user/sign-up');
    }

    public function getMyBankAccount() {
        if (auth()->check()) {
            $bank_list = auth()->user()->banks()->get();

            if(!auth()->user()->tempo){
                $tempo = new Tempo();
                $tempo->user_id = auth()->user()->id;
                $tempo->aggrement = "";
                $tempo->status = "unapplied";
                $tempo->save();
            }

            return view('frontend.user.my_bank_account', compact('bank_list'));

        }
        return redirect('user/sign-up');
    }

    public function getAddBankAccount() {
        return view('frontend.user.add_bank_account');
    }

    public function postAddBankAccount(Request $request) {
        $this->validate($request, [
            'account_name'      => 'required',
            'account_number'    => 'required|numeric',
            'bank_name'         => 'required',
            'user_password'     => 'required',
        ]);

        if (\Hash::check($request->user_password, auth()->user()->password)){
            $bank = new UserBank;
            $bank->account_name = $request->account_name;
            $bank->account_number = $request->account_number;
            $bank->name = $request->bank_name;
            $bank->user_id = auth()->user()->id;
            if (null != $request->branch) {
                $bank->branch = $request->branch;
            }
            $status = $bank->save();

            if($status) {
                $request->session()->flash('alert-success', 'Bank info added to your account.');
            } else {
                $request->session()->flash('alert-danger', 'Failed adding bank info to your account. Contact our customer service for further assistance.');
            }

            return redirect('user/my-bank-account');
        } else {
            $request->session()->flash('alert-danger', 'Failed adding bank info to your account. Invalid user password.');
            #return redirect('user/add-bank-account');
            return back();
        }
    }

    public function getSetBankAccountPreference(Request $request, $id) {

        if(! auth()->check()) {
            return redirect('/');
        }

        $bank_account = UserBank::find($id);
        if(null == $bank_account) {
            $request->session()->flash('alert-danger', 'Oops, Terjadi kesalahan pada akun bank. dimohon untuk menghubungi customer service kami untuk bantuan!');
        } else {
            if(auth()->user()->id != $bank_account->user_id) {
                $request->session()->flash('alert-danger', 'Oops, Anda tidak memiliki akses ini!');
            } else {
                $clear_existing = DB::table('user_banks')->where('user_id', $bank_account->user_id)->where('is_preference', 1)->update(['is_preference' => 0]);
                $add_existing = DB::table('user_banks')->where('id', (int) $id)->update(['is_preference'=> 1]);

                if($add_existing) {
                    $request->session()->flash('alert-success', 'Akun bank diatur sebagai default.');
                } else {
                    $request->session()->flash('alert-danger', 'Oops, Terjadi kesalahan pada akun bank. dimohon untuk menghubungi customer service kami untuk bantuan!');
                }
            }
        }

        return redirect('user/my-bank-account');
    }

    public function getDeleteBankAccount(Request $request, $id) {
        if(! auth()->check()) {
            return redirect('/');
        }

        $bank_account = UserBank::find($id);
        if(null == $bank_account) {
            $request->session()->flash('alert-danger', 'Oops, Terjadi kesalahan pada akun bank. dimohon untuk menghubungi customer service kami untuk bantuan!');
        } else {
            if(auth()->user()->id != $bank_account->user_id) {
                $request->session()->flash('alert-danger', 'Oops, Anda tidak memiliki akses ini!');
            } else {
                $delete_existing = DB::table('user_banks')->where('id', (int) $id)->delete();

                if($delete_existing) {
                    $request->session()->flash('alert-success', 'Akun bank berhasil dihapus');
                } else {
                    $request->session()->flash('alert-danger', 'Oops, Terjadi kesalahan pada akun bank. dimohon untuk menghubungi customer service kami untuk bantuan!');
                }
            }
        }

        return redirect('user/my-bank-account');
    }

    public function getMyCashbackRewards(Request $request) {

        if(! auth()->check()){
            return redirect('/');
        }

        $cashback_rewards = auth()->user()->rewards()->orderBy('created_at', 'desc')->get();

        return view('frontend.user.cashback_balance', compact('cashback_rewards'));
    }

    public function getMyRatingReview() {
        if (auth()->check()) {

            $orders = Order::ofUser(auth()->user())->orderBy('created_at', 'desc')->get();
            $my_review = array();
            $my_reviews_raw = Rating::where('user_id', auth()->user()->id)->get();

            if(null !== $my_reviews_raw) {
                foreach($my_reviews_raw as $rv) {
                    array_push($my_review, $rv->groceries_id);
                }
            }

            $reviewd = array();

            $grocer_list = array();
            foreach($orders as $order){
                foreach($order->groceries as $grocer) {
                    if(! in_array($grocer->groceries_id, $reviewd)) {
                        if(in_array($grocer->groceries_id, $my_review)) {
                            $grocer->is_reviewed = 1;
                        }
                        array_push($grocer_list,$grocer);
                    }

                    array_push($reviewd, $grocer->groceries_id);
                }
            }

            return view('frontend.user.my_review', compact('grocer_list'));

        }
        return redirect('user/sign-up');
    }

    public function getMyEmailPreference() {
        if (auth()->check()) {
            return view('frontend.user.my_email');

        }
        return redirect('user/sign-up');
    }

    public function postSignIn(Request $request)
    {
        $user = User::where('email',$request->email)->first();

        if(!$user){
            $request->session()->flash('alert-danger', 'Oops, Email doesnt exists');
            return back();
        }

        if($user->is_active == 0){
            $request->session()->flash('alert-danger', 'Oops, You need to active your account first!');
            return back();
        }


        $credentials = ['email' => $request->email, 'password' => $request->password];

        $remember = request('remember') ? true : false;

        // Login fails
        //if ( ! auth()->attempt($credentials, $remember)) {
        if ( ! \Auth::attempt('user',$credentials,$remember)) {
            if(URL::previous() != url('user/sign-in')) {
                $request->session()->flash('alert-danger', 'Oops, email and password does not match!');
                return back();
            } else {
                return redirect('cart');
            }
        }


        $merge_cart = $this->sync_cart(auth()->user()->id);

        if(URL::previous() == url('user/sign-in'))
        {
            /*if(Agent::isDesktop()) {
                return redirect('cart');
            } else {*/
                if(null == $request->input('r')) {
                    return redirect('/');
                } else {
                    return redirect($request->input('r'));
                }
            #}
        } elseif (URL::previous() == url('admin/login')) {

            return redirect('admin');
        } elseif (URL::previous() == url('cart')) {
            return redirect('checkout');
        }
        else {

            if(null == $request->input('r')) {
                return redirect('/');
            } else {
                return redirect($request->input('r'));
            }
        }
    }

    public function postSignUp(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_name'    => 'required',
            'business_entity' => 'required',
            'billing_address' => 'required',
            'province'        => 'required',
            'city'            => 'required',
            'area'            => 'required',
            'postal_code'     => 'required',
            'choose_shipping_address' => 'required',
            'company_phone'   => 'required',
            'company_email'   => 'required',
            'category'        => 'required',
            'title'           => 'required',
            'pic_name'        => 'required',
            'email'           => 'required|unique:users|email|unique:user_supplier,email',
            'phone_number'    => 'required|numeric',
            'password' => 'required|between:8,12|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9]).*$/',
            //'password' => 'required|between:4,20',
            'confirm_password' => 'required|same:password',

            'company_npwp'     => 'required',
            'agree'            => 'required',

        ], [
            'required' => 'The :attribute field is required.',
            'same'    => 'The password and confirm password must match.',
            'unique' => 'The :attribute has been taken by another user. Please head to forgot password section to recover your password',
            'password.between' => 'Password must be at least 8 characters.',
            'password.regex' => 'Password must contain letters and numbers.',
            'mimes'  => 'Format must .JPEG, .JPG or .PNG and max size 2MB',
            'agree.required'    => 'Please check confirm that I have read and agree to SeaferMart Term & Conditions and Privacy Policy',
        ]);
        //add rules when input file not null

        $validator->sometimes('upload_ktp', ['image','max:2000'], function ($input) {
            return $input->upload_ktp != null;
        });
        $validator->sometimes('upload_npwp', ['image','max:2000'], function ($input) {
            return $input->upload_npwp != null;
        });

        $validator->sometimes('shipping_address', 'required', function ($input) {
            return $input->choose_shipping_address == 'another_address';
        });
        $validator->sometimes('shipping_province', 'required', function ($input) {
            return $input->choose_shipping_address == 'another_address';
        });
        $validator->sometimes('shipping_city', 'required', function ($input) {
            return $input->choose_shipping_address == 'another_address';
        });
        $validator->sometimes('shipping_area', 'required', function ($input) {
            return $input->choose_shipping_address == 'another_address';
        });

        if ($validator->fails()) {
            return redirect('user/sign-up')
                        ->withErrors($validator)
                        ->withInput();
        }

        //$fname = explode(' ',request('first_name'));
        $fname = explode(' ',request('pic_name'));
        $referral_code = strtoupper($fname[0]).uniqid();

        $refer_from = null;
        if(null !== $request->session()->get('refer_from') || ! empty($request->session()->get('refer_from'))) {
            $refer = $request->session()->get('refer_from');
            $refer_from_user = User::where('referral_code', $refer)->first();
            if(null !== $refer_from_user || !empty($refer_from_user)) {
                $refer_from = $refer_from_user->id;
            }
        }
        if($ktp_user = $request->file('upload_ktp')) {
            $ktp_name = time().$ktp_user->getClientOriginalName();
            $ktp_user->move('ktp_user/', $ktp_name);
            //Storage::disk('local') -> put('ktp_user/'.$ktp_name, file_get_contents($ktp_user -> getRealPath()));
        }
        if($npwp_user = $request->file('upload_npwp')) {
            $npwp_name = time().$npwp_user->getClientOriginalName();
            $npwp_user->move('npwp_user/', $npwp_name);
            //Storage::disk('local') -> put('npwp_user/'.$npwp_name, file_get_contents($npwp_user -> getRealPath()));
        }



        $confirmation_code = str_random(30);
        $user = new User;
        $user->company_name = request('company_name');
        $user->brand_name = request('company_name');
        $user->business_entity = request('business_entity');
        $user->billing_address = request('billing_address');
        $user->province = request('province');
        $user->city = request('city');
        $user->area = request('area');
        $user->postal_code = request('postal_code');
        $user->choose_shipping_address = request('choose_shipping_address');
        if (request('choose_shipping_address') == 'another_address') {
            $user->shipping_address = request('shipping_address');
            $user->shipping_province = request('shipping_province');
            $user->shipping_city = request('shipping_city');
            $user->shipping_area = request('shipping_area');
            $user->shipping_postal_code = request('shipping_area');
        }
        $user->company_phone = request('company_phone');
        $user->company_email = request('company_email');
        $user->website = request('website');
        $user->category = request('category');

        $user->sex = request('title');
        $user->name = request('pic_name');

        if ($request->file('upload_ktp')) {
            $user->ktp = $ktp_name;
        }
        $user->comp_npwp = request('company_npwp');
        if ($request->file('upload_npwp')) {
            $user->upload_npwp = $npwp_name;
        }
        $user->email = request('email');
        $user->phone_number = '+62'.request('phone_number');
        $user->password = \Hash::make(request('password'));
        $user->role = 2;
        $user->is_active = 0;
        $user->registration_code = $confirmation_code;
        $user->referral_code = $referral_code;
        $user->refer_from = $refer_from;
        $user->save();

        $temp =["cod","transfer"];
        $tempo = new Tempo();
        $tempo->user_id = $user->id;
        $tempo->aggrement = "";
        $tempo->status = "unapplied";

        $address = new Address;
        $address->user_id = $user->id;
        $address->name = request('company_name');
        $address->recipient_title = request('title');
        $address->recipient = request('pic_name');
        $address->address = request('billing_address');
        $address->area_id = request('area');
        $address->city_id = request('city');
        $address->postal_code = request('postal_code');
        $address->phone_number = request('company_phone');
        $address->save();

        if (request('payment_method')) {
            foreach (request('payment_method') as $key) {
                if ($key == "tempo") {
                    $tempo->status = "pending";
                    array_push($temp,"tempo");
                    break;
                }
            }
        }

        $tempo->save();
        $user->payment_method = json_encode($temp);
        $user->save();

        $email_preference = new EmailPreference;
        $email_preference->user_id = $user->id;
        $email_preference->save();

        $newsletter = new MyNews;
        $newsletter->email = request('email');
        $newsletter->save();

        $request->session()->forget('refer_from');


        Mail::queue('emails.signup', compact('confirmation_code', 'user'), function ($message) use ($user) {

            $message->from('hello@Seafer Grosir.id', '[NO-REPLY] Seafer Grosir');

            $message->to($user->email)->subject('Selamat Datang dan Bergabung Dengan Seafer Grosir, Sahabat Seafer Grosir!');

        });
        $request->session()->flash('alert-success', 'Congratulation! You are one step away from completing registration process. Please check your email and verify your account');

        return redirect('/');
    }

    public function getVerifyAccount(Request $request, $confirmation_code) {

        if( ! $confirmation_code)
        {
            $request->session()->flash('alert-danger', 'Oops, confirmation code not found! We are unable to verify your email address');
            redirect('/');
        }

        $user = User::whereRegistrationCode($confirmation_code)->first();
        if ($user == NULL)
        {
            $request->session()->flash('alert-danger', 'Oops, confirmation code not found! We are unable to verify your email address');
            return redirect('/');
        } else {
            $user->is_active = 1;
            $user->registration_code = null;
            $status = $user->save();
            if($status){
                $request->session()->flash('alert-success', 'Congratulation, your account is now active!');
            } else {
                $request->session()->flash('alert-danger', 'Oops, we are unable to verify your email address');
            }
            auth()->login($user);
            return redirect('/');
        }
    }
    public function postSignUpSupplier(Request $request){

        $validator = Validator::make($request->all(), [
            'company_name'    => 'required',
            'business_entity' => 'required',
            'billing_address' => 'required',
            'province'        => 'required',
            'city'            => 'required',
            'area'            => 'required',
            'postal_code'     => 'required',
            'title'           => 'required',
            'pic_name'        => 'required',
            'phone_number'    => 'required|numeric',
            'email'           => 'required|unique:users,email|email',
            'category'        => 'required',
            'commoditas'        => 'required',
            'selling_method'        => 'required',
            'password' => 'required|between:8,12|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9]).*$/',
            'cf_password' => 'required|same:password',
            'types_of_goods'     => 'required',
            'product_origin'   => 'required',
            'country'   => 'required',
            'agree'            => 'required',

        ], [
            'required' => 'The :attribute field is required.',
            'same'    => 'The password and confirm password must match.',
            'unique' => 'The :attribute has been taken by another user. Please head to forgot password section to recover your password',
            'password.between' => 'Password must be at least 8 characters.',
            'password.regex' => 'Password must contain letters and numbers.',
            'agree.required'    => 'Please check confirm that I have read and agree to SeaferMart Term & Conditions and Privacy Policy',
        ]);
        if ($validator->fails()) {
            return redirect('sign-up-supplier')
                        ->withErrors($validator)
                        ->withInput();
        }
        $confirmation_code = str_random(30);
        $user_supplier = new userSupplier;
        $user_supplier->company_name = request('company_name');
        $user_supplier->bussiness_entity = request('business_entity');
        $user_supplier->billing_address = request('billing_address');
        $user_supplier->province = request('province');
        $user_supplier->city = request('city');
        $user_supplier->area = request('area');
        $user_supplier->postcode = request('postal_code');
        $user_supplier->title = request('title');
        $user_supplier->user_name = request('pic_name');
        $user_supplier->phone_number = request('phone_number');
        $user_supplier->email = request('email');
        $user_supplier->website = request('website');
        $user_supplier->category = request('category');
        $user_supplier->commoditas = request('commoditas');
        $user_supplier->selling_method = request('selling_method');
        $user_supplier->password = \Hash::make(request('password'));

        $user_supplier->type_of_goods = request('types_of_goods');
        $temp=array();
        foreach (request('product_origin') as $key) {
            $pay = array('product_origin' => $key);
            array_push($temp, $pay);
        }
        $user_supplier->product_origin = json_encode($temp);
        $user_supplier->country = request('country');
        $user_supplier->email_Verif = $confirmation_code;
        $user_supplier->is_active = 0;
        $user_supplier->save();
        Mail::queue('emails.signup_supplier', compact('confirmation_code', 'user_supplier'), function ($message) use ($user_supplier) {

            $message->from('hello@Seafer Grosir.id', '[NO-REPLY] Seafer Grosir');

            $message->to($user_supplier->email)->subject('Selamat Datang dan Bergabung Dengan Seafer Grosir Sebagai Supplier!');

        });
        $request->session()->flash('alert-success', 'Terima kasih sudah bergabung dengan Seafer Grosir, admin akan memverifikasi data Anda terlebih dahulu.');
        return redirect('/');

    }
    public function getVerifyAccountSupplier(Request $request, $confirmation_code) {

        if( ! $confirmation_code)
        {
            $request->session()->flash('alert-danger', 'Oops, confirmation code not found! We are unable to verify your email address');
            redirect('/');
        }

        $user = userSupplier::whereEmailVerif($confirmation_code)->first();
        if ($user == NULL)
        {
            $request->session()->flash('alert-danger', 'Oops, confirmation code not found! We are unable to verify your email address');
            return redirect('/');
        } else {
            $user->is_active = 1;
            $user->email_verif = null;
            $status = $user->save();
            if($status){
                $request->session()->flash('alert-success', 'Congratulation, your account is now active!');
            } else {
                $request->session()->flash('alert-danger', 'Oops, we are unable to verify your email address');
            }
            return redirect('/');
        }
    }

    public function postMakeHistory(Request $request){

        if ($request->type == 'uid') {
            $history = ViewHistory::where('identifier', $request->identifier)->where('is_uid', 1)->get();
        } elseif ($request->type == 'ss'){
            $history = ViewHistory::where('identifier', $request->identifier)->where('is_session', 1)->get();
        } else {
            return 'Whoaa!Hang on there!';
        }

        if ($history->count() > 6) {
            $status = ViewHistory::where('identifier', $request->identifier)->orderBy('created_at', 'asc')->first()->delete();
        }
        if($history->count() > 0) {
            foreach( $history as $his) {
                if ($his->groceries_id == $request->gid) {
                    return 'already, mate!';
                }
            }
        }

        if ($request->type == 'uid') {
            $hist = new ViewHistory;
            $hist->identifier = $request->identifier;
            $hist->is_uid = 1;
            $hist->groceries_id = intval($request->gid);
            $hist->save();
        } elseif ($request->type == 'ss'){
            $hist = new ViewHistory;
            $hist->identifier = $request->identifier;
            $hist->is_session = 1;
            $hist->groceries_id = intval($request->gid);
            $hist->save();
        }

        return 'alrighty';
    }

    public function postAddNewAddress(Request $request) {

        if (auth()->check()) {

            $validator = Validator::make($request->all(), [
                'name'  => 'required',
                'first_name' => 'required',
                'address_one'   => 'required',
                'area'  => 'required',
                'city'  => 'required',
                'postal_code'   => 'required',
                'recipient_title' => 'required',
                'phone_number'  => 'required|numeric',
            ]);

            if ($validator->fails()) {
                return redirect('user/add-new-address')
                            ->withErrors($validator)
                            ->withInput();
            }



            $address = new Address;
            $address->user_id = auth()->user()->id;
            $address->name = request('name');
            $address->recipient_title = request('recipient_title');
            $address->recipient = request('first_name').' '.request('last_name');
            $address->address = request('address_one').' '.request('address_two').' '.request('address_three');
            $address->area_id = request('area');
            $address->city_id = request('city');
            $address->postal_code = request('postal_code');
            $address->phone_number = request('phone_number');

            $string_address = $this->checkAddressValidity($address);
            if($string_address) {
                $request->session()->flash('alert-success', 'Yay! Alamat anda berhasil ditambahkan');
                $address->coordinates = $string_address;
            } else {
                $request->session()->flash('alert-danger', 'Oops, We can\'t find the coordinates of your address. It will trouble you in the delivery if you proceed with this address.');
                return redirect('user/add-new-address');
            }

            $address->save();

            #return view('users.add_new_address');
        }
        return redirect('user/my-address');
    }

    public function getAjaxAreasByCity($id) {
        $area = Area::where('city_id', $id)->orderBy('name', 'asc')->lists('name', 'id')->all();

        if(count($area)>0){
            $result = array('status' => 'success', 'areas' => $area);
        } else {
            $result = array('status' => 'fail', 'message' => 'Area not found for this city');
        }
        return json_encode($result);
    }

    public function getAjaxCitiesByProvince($id){
        $city = City::where('province_id', $id)->orderBy('city_name_full', 'asc')->lists('city_name_full', 'city_id')->all();

        if(count($city)>0){
            $result = array('status' => 'success', 'cities' => $city);
        } else {
            $result = array('status' => 'fail', 'message' => 'City not found for this province');
        }
        return json_encode($result);
    }

    public function patchUpdateAddress(Request $request, $id) {
        if (auth()->check()) {

            $address = Address::find($id);
            $input = $request->all();

            $input['user_id ']= auth()->user()->id;
            unset($input['_token']);
            unset($input['_method']);

            $status = $address->update($input);

            $request->session()->flash('alert-success', 'Yay! Alamat anda berhasil diupdate');
            #return view('users.add_new_address');
        }
        return redirect('user/my-address');
    }

    public function deleteAddress(Request $request, $id) {
        if (auth()->check()) {

            try {
                /*$status = Address::find($id)->delete();*/
                $status = DB::table('user_addresses')->where('id', $id)->update(['deleted' => 1, 'updated_at' => date('Y-m-d H:i:s')]);
                $request->session()->flash('alert-success', 'Address succesfully deleted');
            } catch (\Exception $e) {
                $request->session()->flash('alert-danger', 'Oops, you can\'t delete this address as it being used for one of your order!');
            }

        }
        return redirect('user/my-address');
    }

    public function postEditGeneralData(Request $request, $id) {
         $validator = Validator::make($request->all(), [
            'file'          => 'mimes:jpeg,bmp,png|max:2000',
            'company_name'    => 'required',
            'brand_name'    => 'required',
            'business_entity'    => 'required',
            'choose_shipping_address'    => 'required',
            'billing_address'    => 'required',
            'province'    => 'required',
            'city'    => 'required',
            'area'    => 'required',
            'postal_code'    => 'required|numeric',
            'company_phone'    => 'required|numeric',
            'website'    => 'required',
            'category'    => 'required',
        ], [
            'required' => 'The :attribute field is required.',
            'same'    => 'The password and confirm password must match.',
            'mimes'  => 'Format must .JPEG, .JPG or .PNG and max size 2MB',

        ]);
        $validator->sometimes('shipping_address', 'required', function ($input) {
            return $input->choose_shipping_address == 'another_address';
        });
        $validator->sometimes('shipping_province', 'required', function ($input) {
            return $input->choose_shipping_address == 'another_address';
        });
        $validator->sometimes('shipping_city', 'required', function ($input) {
            return $input->choose_shipping_address == 'another_address';
        });
        $validator->sometimes('shipping_area', 'required', function ($input) {
            return $input->choose_shipping_address == 'another_address';
        });

        $validator->sometimes('shipping_postal_code', 'required', function ($input) {
            return $input->choose_shipping_address == 'another_address';
        });
        if ($validator->fails()) {
            $request->session()->flash('alert-danger', 'Oops, Terjadi kesalahan saat melakukan update!');
            return redirect('user/my-profile')
                        ->withErrors($validator)
                        ->withInput();
        }
        $user = User::findOrFail($id);

        $input = $request->all();

        /*$input['dob'] = $input['dob_year'].'-'.$input['dob_month'].'-'.$input['dob_date'];
        unset($input['dob_year']);
        unset($input['dob_month']);
        unset($input['dob_date']);*/

        if($file = $request->file('file')) {
            $name = time().$file->getClientOriginalName();

            $file->move('profile_picture', $name);

            $photo = ProfilePicture::create(['file' => $name]);

            $input['profile_picture'] = (int) $photo->id;
        }
        unset($input['file']);
        $status = $user->update($input);

        if($status) {
            $request->session()->flash('alert-success', 'Yay! Profile anda berhasil diupdate');
        }/* else {
            $request->session()->flash('alert-danger', 'Oops, Terjadi kesalahan saat melakukan update!');
        }*/

        return redirect('user/my-profile');
    }
    public function postFileUpload(Request $request, $id) {

        $validator = Validator::make($request->all(), [
            'ktp_upload'          => 'mimes:jpeg,bmp,png|max:2000',
            'npwp_upload'          => 'mimes:jpeg,bmp,png|max:2000',
        ], [

            'mimes'  => 'Format must .JPEG, .JPG or .PNG and max size 2MB',
        ]);
        if ($validator->fails()) {
            $request->session()->flash('alert-danger', 'Oops, Terjadi kesalahan saat melakukan update!');
            return redirect('user/my-profile')
                        ->withErrors($validator)
                        ->withInput();
        }
        $user = User::findOrFail($id);



        if($file_ktp = $request->file('ktp_upload')) {
            $name_ktp = time().$file_ktp->getClientOriginalName();

            $file_ktp->move('ktp_user/', $name_ktp);
            if ($request->file('ktp_upload')) {
                $user->ktp = $name_ktp;
            }
        }
        if($file_npwp = $request->file('npwp_upload')) {
            $name_npwp = time().$file_npwp->getClientOriginalName();

            $file_npwp->move('npwp_user/', $name_npwp);
            if ($request->file('npwp_upload')) {
                $user->upload_npwp = $name_npwp;
            }
        }
        $input = $request->all();
        unset($input['ktp_upload']);
        unset($input['npwp_upload']);
        $status = $user->update($input);

        if($status) {
            $request->session()->flash('alert-success', 'Yay! Profile anda berhasil diupdate');
        }/* else {
            $request->session()->flash('alert-danger', 'Oops, Terjadi kesalahan saat melakukan update!');
        }*/

        return redirect('user/my-profile');
    }

    public function postEditDataPribadi(Request $request, $id) {
        $user = User::findOrFail($id);
        $hashread='';
        Validator::extend('old_password', function ($attribute, $value, $parameters, $validator) {

            return $hashread=\Hash::check($value, current($parameters));

        });
        $validator = Validator::make($request->all(), [
           /* 'old_email'   => 'required|email',
            'email'       => 'required|unique:users|email|confirmed',*/
            'sex'    => 'required',
            'name'    => 'required',
            'email'    => 'required|email',
            'phone_number'    => 'required|numeric',
            /*'old_password' => 'required|old_password:' . auth()->user()->password,*/
            'password' => 'between:8,12|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9]).*$/',
        ], [
            'required' => 'The :attribute field is required.',
            'same'    => 'The password and confirm password must match.',
            'password.between' => 'Password must be at least 8 characters.',
            'password.regex' => 'Password must contain letters and numbers.',
            'mimes'  => 'Format must .JPEG, .JPG or .PNG and max size 2MB',
        ]);
        $validator->sometimes('old_password', 'required|old_password:' . auth()->user()->password, function ($input) {
            return $input->password != null;
        });
        $validator->sometimes('password_confirmation', 'required|same:password', function ($input) {
            return $input->password != null;
        });
        if ($validator->fails()) {
            $request->session()->flash('alert-danger', 'Oops, Terjadi kesalahan saat melakukan update!');
            return redirect('user/my-profile')
                        ->withErrors($validator)
                        ->withInput();
        }

        $input = $request->all();
        if(Hash::check(request('old_password'), $user->password)) {

            if(request('password')) {
                /*print_r(Hash::make(request('password')));
                die();*/
                $user->password = Hash::make(request('password'));
            }
        }
        unset($input['password']);

        $status = $user->update($input);

        if($status) {
            $request->session()->flash('alert-success', 'Yay! Profile anda berhasil diupdate');
        } else {
            $request->session()->flash('alert-danger', 'Oops, Terjadi kesalahan saat melakukan update!');
        }

        return redirect('user/my-profile');
    }

    public function postEditPasswordData(Request $request, $id) {
        $user = User::findOrFail($id);

        $input = $request->all();

        Validator::extend('old_password', function ($attribute, $value, $parameters, $validator) {

            return \Hash::check($value, current($parameters));

        });

        $this->validate($request, [
            'old_password' => 'required|old_password:' . auth()->user()->password,
            'password' => 'required|between:8,12|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9]).*$/',
        ]);

        $input['password'] = \Hash::make($input['password']);
        $status = $user->update($input);

        if($status) {
            $request->session()->flash('alert-success', 'Yay! Profile anda berhasil diupdate');
        } else {
            $request->session()->flash('alert-danger', 'Oops, Terjadi kesalahan saat melakukan update!');
        }

        return redirect('user/my-profile');
    }

    public function getInitReferral() {
        $users = User::where('referral_code', NULL)->get();

        foreach($users as $user) {
            $fname = explode(' ', $user->name);
            $status = DB::table('users')->where('id', $user->id)->update(['referral_code' => strtoupper($fname[0]).uniqid()]);
        }

        return 'ok';
    }

    public function postShareReferral(Request $request) {
        $referral_code = request('referral_code');
        $email = explode(',',request('email'));

        foreach($email as $em) {
            if (ReferralHistory::where('user_id', auth()->user()->id)->where('email_address', $em)->get()->count() !== 0) {
                return array('status' => 'You have already sent invitation to '.$em.'. Please remove this address and try to send it again.');
            }
        }

        try{
            Mail::queue('emails.share_referral', compact('referral_code'), function ($message) use ($email) {
                $message->from('hanskristianto26@gmail.com', 'Referral Code');
                $message->to($email);
            });

            foreach($email as $em) {
                if (ReferralHistory::where('user_id', auth()->user()->id)->where('email_address', $em)->get()->count() == 0) {
                    $referral = new ReferralHistory;
                    $referral->user_id = auth()->user()->id;
                    $referral->email_address = $em;
                    $referral->save();
                } else {
                    return array('status' => 'You have already sent invitation to this email address');
                }
            }


            return array('status' => 'Ok');
        }catch(Exception $e) {
            return array('status' => 'Failed to send email. Please try again in a few moment');
        }
    }

    public function postUpdateEmailPreference(Request $request) {
        if (! auth()->check()) {
            return back();
        }

        $validator = Validator::make($request->all(), [
            'type'  => 'required',
            'new_status' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            return array('status' => 'Failed to update the newsletter. Please provide type and try again in a few moment');
        }

        $id = auth()->user()->id;
        $current_preference = EmailPreference::where('user_id', $id)->first();

        if(null == $current_preference) {
            $ep = new EmailPreference;
            $ep->user_id = $id;
            $ep->save();
            $current_preference = EmailPreference::where('user_id', $id)->first();
        }

        if($request->type == 'promo') {
            $current_preference->promo = $request->new_status;
        } elseif($request->type == 'latest_news') {
            $current_preference->latest_news = $request->new_status;
        } elseif($request->type == 'editorial') {
            $current_preference->editorial = $request->new_status;
        } elseif($request->type == 'low_stock') {
            $current_preference->low_stock = $request->new_status;
        } elseif($request->type == 'new_product') {
            $current_preference->new_product = $request->new_status;
        }

        $current_preference->save();

        return array('status' => 'OK');

    }

    public function my_account_sup(){
        return view('frontend.supplier.my_account');
    }

    public function postUpdatePayment(Request $request){

        $user_payments = json_decode(auth()->user()->payment_method);


        if(!$request->payment_method == null){
            $msg = "Thank you for updating your payment information !";
            foreach( $request->payment_method as $payment){
                if(!in_array($payment,$user_payments)){
                    array_push($user_payments,$payment);
                }else{
                    if($payment == "tempo"){
                        $tempo = Tempo::where('user_id',auth()->user()->id)->first();
                        $tempo->user_id = auth()->user()->id;
                        $tempo->aggrement = "";
                        $tempo->status = "pending";
                        $tempo->save();
                        $msg .= " We will review your tempo request";
                    }
                }
            }
            auth()->user()->payment_method = json_encode($user_payments);
            auth()->user()->save();
            $request->session()->flash('alert-success', $msg);
        }else{
            $msg = "You cannot delete your payment method";
            $request->session()->flash('alert-danger', $msg);
        }





        return redirect('user/my-bank-account');


    }



}
