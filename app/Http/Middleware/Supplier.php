<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class Supplier
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (\Auth::check('supplier')) {
            return $next($request);
        }
        return redirect('/');
    }
}
