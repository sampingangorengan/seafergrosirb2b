<?php

namespace App\Http\Middleware;

use Closure;
use Carte;
class Checkout
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $total = Carte::total();

        if($total < 500000){
            $request->session()->flash('alert-danger', 'You need at least Rp. 500,000 to able to checkout');
            return redirect()->back();
        }

        return $next($request);
    }
}
