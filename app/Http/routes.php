<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
use Jenssegers\Agent\Agent;
use Illuminate\Support\Facades\Mail;
use App\Models\Country;
use App\Models\User\Tempo;
use App\Models\StaticPage\StaticPage;
use App\Models\StaticPage\StaticDescription;
use App\Models\StaticPage\StaticImage;
use App\Models\StaticPage\StaticLink;
use Knp\Snappy\Pdf;
use App\Models\Location\Area;
use App\Models\City;
use App\Models\Province;


Route::get('test',function(){
    $data = array(
        'name' => "okat",
        'email' => "oktagandajaya@gmail.com",
        'order' => 1
    );

    try{
        $status = Mail::queue('emails.payment_process.payment_confirmed', $data, function ($message) use ($data) {

            $message->from("hello@seafermart.co.id", "Foodis Inquiry");
            $message->to($data['email'])->subject("Pembayaran Foodis Order #".$data['order']." telah berhasil dan dikonfirmasi!");

        });
    } catch (\Exception $exception) {
    }
});

Route::get('home', function() {
    return redirect('/');
});

Route::get('user/home', function() {
    return redirect('/');
});



Route::get('test_geo', function(){
    return view('frontend.test_geo');
});

Route::get('country', function() {

    $countries = Countries::getList('en', 'json');
    $decoded_countries = json_decode($countries);

    foreach($decoded_countries as $key=>$c) {
        $country = new Country;
        $country->code = $key;
        $country->name = $c;
        $country->save();
        echo $c.' saved';
        echo '<br/>';
        unset($country);
    }
});
//supplier frontend
Route::get('my_account_sup', 'UserController@my_account_sup');

Route::get('supplier',function(){
    return view('frontend.supplier.sign-in');
});

Route::post('supplier/sign-in','SupplierController@sign_in');
Route::post('supplier/update/{id}','SupplierController@update');

Route::group(['middleware' => 'supplier'], function() {
    Route::get('supplier/home','SupplierController@index');
    Route::get('supplier/profile','SupplierController@profile');
    Route::get('supplier/products','SupplierController@products');
    Route::get('supplier/edit-profile/{id}','SupplierController@editprofile');
    Route::get('supplier/products/{id}','SupplierController@showProduct');

});

// Password reset link request routes...
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

Route::get('search', 'GroceriesController@search');
Route::post('search', 'GroceriesController@search');


Route::controller('order/payment', 'Order\PaymentController');
Route::get('groceries/get-all/{parent}', 'GroceriesController@parentcategory');
// Item page
Route::get('groceries/{id}/{name}', 'GroceriesController@show');
Route::get('groceries/initiate-discount-price', 'GroceriesController@initiateDiscountPrice');
Route::get('groceries/initiate-non-discount-price', 'GroceriesController@initiateNonDiscountItem');


Route::get('groceries/all', 'GroceriesController@showall');
Route::get('groceries/all_data', 'GroceriesController@getAjaxSearchAutocomplete');

Route::get('favourites','GroceriesController@showAllFavourites');
Route::post('groceries/add_to_favourite','GroceriesController@postAddNewFavourite');
Route::post('groceries/remove_from_favourite','GroceriesController@postRemoveFavourite');

// Category page

Route::get('groceries/{category_name}', 'GroceriesController@category');



// Item list
Route::get('groceries', 'GroceriesController@index');

Route::get('groceries-to-love', function() {
    App\Models\Groceries\Love::firstOrCreate([
        'groceries_id' => request('id'),
        'user_id'      => auth()->user()->id
    ]);

    return back();
});

Route::post('kirin/get_questions/{identifier}', 'GroceriesController@postAjaxQuestion');
Route::post('groceries/question/{id}', 'GroceriesController@postQuestion');

Route::post('promo', 'GroceriesController@postPromoCode');
Route::post('donation', 'GroceriesController@postDonation');
Route::post('referral', 'GroceriesController@postReferralCode');

Route::resource('reviews', 'RatingController');

//Removed on b2b Implmentation
//Route::resource('wholesale', 'WholesaleController');

Route::get('orders/{id}', 'OrderController@show');
Route::get('orders/{id}/download', 'OrderController@download');
Route::get('orders/showPrint/{id}', 'OrderController@showPrint');
Route::get('orders', function() {
    if (!auth()->check()) {
        return redirect('user/sign-up');
    }
    $orders = App\Models\Order::ofUser(auth()->user())->orderBy('updated_at', 'desc')->get();

    $agent = new Agent();
    return view('frontend.user.my_order', compact('orders'));

});


/*
|--------------------------------------------------------------------------
| Static Routes
|--------------------------------------------------------------------------
|
|
*/
Route::get('sign-up-supplier',function(){
    $areas = Area::orderBy('name', 'asc')->lists('name', 'id')->all();
    $countries = Countries::getList('en','json');
    $country= json_decode($countries,true);
    /*print_r($country);
    die();*/
    $cities = City::orderBy('city_name', 'asc')->lists('city_name', 'city_id')->all();
    $provinces = Province::orderBy('province_name', 'asc')->lists('province_name', 'province_id')->all();
    return view('frontend.signup_supplier',compact('area','cities','provinces','country'));
});

Route::get('become-member', function() {
    return view('frontend.become_member');
});

Route::get('about-us', function(){
    $descriptions = StaticDescription::where('page_id', 2)->get();
    $description = array();
    foreach($descriptions as $desc){
        $description[$desc->description_position] = $desc;
    }
    $images = StaticImage::where('page_id', 2)->lists('image_name', 'image_location');

    return view('frontend.about', compact('description','images'));

});

Route::get('best-price-guarantee', function(){
    $agent = new Agent();
    if ($agent->isDesktop()) {
        if($agent->isAndroidOS()) {
            return view('responsive.best_price');
        }
        return view('best_price');
    } else {
        return view('responsive.best_price');
    }
});

Route::post('best-price-guarantee', function(){
    $agent = new Agent();
    if ($agent->isDesktop()) {
        if($agent->isAndroidOS()) {
            return view('responsive.best_price');
        }
        return view('best_price');
    } else {
        return view('responsive.best_price');
    }
});

Route::get('return-and-exchange', function(){
    $descriptions = StaticDescription::where('page_id', 6)->get();
    $description = array();
    foreach($descriptions as $desc){
        $description[$desc->description_position] = $desc;
    }

    return view('frontend.return_exchange', compact('description'));
});

Route::get('delivery', function(){
    $descriptions = StaticDescription::where('page_id', 2)->get();
    $description = array();
    foreach($descriptions as $desc){
        $description[$desc->description_position] = $desc;
    }

    return view('frontend.delivery', compact('description'));
});

Route::get('payment', function() {
    $descriptions = StaticDescription::where('page_id', 4)->get();
    $description = array();
    foreach($descriptions as $desc){
        $description[$desc->description_position] = $desc;
    }

    return view('frontend.payment', compact('description'));
});

Route::get('faq', function() {
    $descriptions = StaticDescription::where('page_id', 10)->get();
    $description = array();
    foreach($descriptions as $desc){
        $description[$desc->description_position] = $desc;
    }

    return view('frontend.faq', compact('description'));
});

Route::get('contact-us', function() {
    $descriptions = StaticDescription::where('page_id', 13)->get();
    $description = array();
    foreach($descriptions as $desc){
        $description[$desc->description_position] = $desc;
    }
    $images = StaticImage::where('page_id', 13)->lists('image_name', 'image_location');

    return view('frontend.contact_us', compact('description','images'));
});

Route::post('contact-us', function(Illuminate\Http\Request $request){

    $data = array(
        'name' => $request->name,
        'email' => $request->email,
        'msg' => $request->message,
        'category' => $request->category
    );


    Mail::queue('emails.contact_us', $data, function ($message) use ($data) {

        $message->from("hello@foodis.co.id", "Foodis Inquiry");
        //$message->to('cs@seafermart.co.id')->subject($request->category.' Inquiry');
        $message->to('hello@foodis.co.id')->subject('Contact Us - '.ucfirst($data["category"]));

    });

    $request->session()->flash('alert-success', 'Your inquiry has been successfully sent!');

    return back();

});

Route::get('sendemail', function () {

    $data = array(
        'name' => "Testing Seafer Sendout",
    );

    try{

        Mail::send('test_email', $data, function ($message) {

            $message->from('hello@seafermart.co.id', 'Greetings from Mars');

            $message->to('wardlattelover@gmail.com')->subject('Tes ngirim2 test email pake smtp Google');

        });

        return "nice!";

    } catch (\Exception $exception) {
        dd($exception->getMessage());
    }

    return "Your email has been sent successfully";

});

Route::get('testpdf', function() {
    $snappy = new Pdf('/usr/local/sbin/wkhtmltopdf');
    /*header('Content-Type: application/pdf');
    header('Content-Disposition: attachment; filename="invoice.pdf"');*/
    try {
        $pdf = $snappy->getOutput(url('resp/about'));
        dd($pdf);
        #return $pdf;
        return new Response(
            $pdf,
            200,
            array(
                'Content-Type'          => 'application/pdf',
                'Content-Disposition'   => 'attachment; filename="invoice.pdf"'
            )
        );
    } catch (\Exception $e) {
        echo $e->getMessage();
    }
});

//Removed
// Route::get('loyalty-points', function(){
//     $descriptions = StaticDescription::where('page_id', 3)->get();
//     $description = array();
//     foreach($descriptions as $desc){
//         $description[$desc->description_position] = $desc;
//     }
//
//     return view('frontend.cashback_rewards', compact('description'));
// });

Route::resource('claim', 'ClaimController');
Route::resource('newsletter', 'NewsletterController');
Route::resource('suggestion', 'ProductSuggestionController');
Route::resource('order-claim', 'OrderClaimController');


Route::controller('cart', 'CartController');

Route::get('cart/cart-controller/{id}', 'CartController@getCartController/$id');

//Social Login
Route::get('/login/{provider?}',[
    'uses' => 'Auth\AuthController@getSocialAuth',
    'as'   => 'auth.getSocialAuth'
]);


Route::get('/login/callback/{provider?}',[
    'uses' => 'Auth\AuthController@getSocialAuthCallback',
    'as'   => 'auth.getSocialAuthCallback'
]);

Route::get('admin/login', function(){
    return view('admin.users.login');
});

Route::group(['prefix'=>'admin', 'namespace'=> 'Admin', 'middleware' => 'admin'], function() {
    Route::get('/', 'AdminController@index');

    Route::get('logout', function() {
        auth()->logout();
        return redirect('admin/login');
    });

    Route::get('user/profile', 'UserController@showProfile')->name('profile');
    Route::post('user/add-payment-option', 'OrderController@addPaymentOption');
    Route::get('user/profilesupplier', 'UserController@showProfileSupplier')->name('profilesupplier');
    Route::post('groceries/storeImage/{id}','ProductController@store_image');
    Route::get('groceries/deleteimage/{id}/{im_id}','ProductController@delete_image');
    Route::post('groceries/store-poster/{id}','ProductController@store_poster');
    Route::get('groceries/delete-poster/{id}/{im_id}','ProductController@delete_poster');
    Route::get('groceries/import-csv', 'ProductController@csv_import');
    Route::post('groceries/import-csv', 'ProductController@csv_import');
    Route::get('groceries/minimum', 'ProductController@minimum');
    Route::get('groceries/init', 'ProductController@initialTransferToSearchable');
    Route::get('groceries/existing-groceries-item/{id}/{result}', 'ProductController@getExistingCategories');

    //Route::post('groceries/upload-advanced', 'ProductController@uploadChunk');
    Route::post('groceries/upload-advanced/{id}', 'ProductController@uploadvideo');

    Route::get('transactions/{id}/approve-transaction', 'PaymentController@approveTransaction');
    Route::get('transactions/{id}/threedays-transaction', 'PaymentController@threeDaysApproval');
    Route::get('transactions/step-one/{id}', 'PaymentController@extraStepOne');
    Route::get('transactions/step-two/{id}', 'PaymentController@extraStepTwo');
    Route::get('transactions/add-manual', 'PaymentController@addTransactionManually');
    Route::post('transactions/process-step-one', 'PaymentController@processStepOne');
    Route::post('transactions/reject-transaction', 'PaymentController@rejectTransaction');
    Route::post('transactions/handle-email-transaction', 'PaymentController@handleWithEmail');



    Route::get('orders/order-gojek/{id}', 'OrderController@doGojek');
    Route::post('orders/insert-awb/{id}', 'OrderController@insertAwb');
    Route::post('orders/update-price/{id}', 'OrderController@updatePrice');
    Route::post('orders/update-interest/{id}', 'OrderController@updateInterest');
    Route::get('orders/dispatch-courier/{id}', 'OrderController@dispatchInternalCourier');
    Route::get('orders/finish-courier/{id}', 'OrderController@finishCourier');
    Route::get('orders/get-gojek-status/{order_number}', 'OrderController@getGojekStatus');
    Route::get('orders/void-order/{id}', 'OrderController@voidOrder');
    Route::get('orders/shipping/{id}', 'OrderController@indexShipping');





    Route::get('test/point', 'PaymentController@getDummyCashback');

    Route::resource('roles', 'RoleController');
    Route::resource('users','UserController');
    Route::resource('groceries','ProductController');
    Route::resource('parent-category','ParentCategoryController');
    Route::resource('child-category','ChildCategoryController');
    Route::resource('cooking-advice','CookingAdviceController');
    Route::resource('brands','BrandController');
    Route::resource('orders','OrderController');
    Route::resource('tempo','TempoController');
    Route::resource('transactions','PaymentController');
    Route::resource('promos','PromoController');
    Route::resource('freebies','FreeTaxDeliveryController');
    Route::resource('discounts','DiscountController');
    Route::resource('complaints','ComplainController');
    Route::resource('claims','ClaimController');
    Route::resource('comments','CommentController');
    Route::resource('metrics', 'MetricController');
    Route::resource('tags', 'TagController');
    Route::resource('headlines', 'HomeSlidesController');
    Route::resource('newsletters', 'NewsletterController');
    Route::resource('charity-voucher', 'DonationVoucherController');
    Route::resource('order-claims', 'OrderClaimController');
    Route::resource('info', 'StaticInfoController');
    Route::resource('pages', 'StaticPageController');
    Route::resource('static-images', 'StaticImageController');
    Route::resource('static-description', 'StaticDescriptionController');
    Route::resource('supplier', 'SupplierController');

    Route::controller('statistics', 'StatisticController');

});

Route::group(['prefix'=>'api', 'namespace'=> 'Api'], function() {
    Route::get('/', 'ApiController@index');
    Route::get('/country',function(){
        $countries = \App\Models\Country::all('code','name');
        return (new \Illuminate\Http\Response($countries, 200))
        ->header('Content-Type', 'application/json');
    });


    Route::controller('categories', 'CategoryController');
    Route::controller('homeslide', 'HomeslideController');
    Route::controller('user', 'UserController');
    Route::controller('cart', 'CartController');
    Route::controller('product', 'ProductController');
    Route::controller('message', 'MessageController');
    Route::controller('wishlist', 'WishlistController');
    Route::controller('review', 'ReviewController');
    Route::controller('order', 'OrderController');
    Route::controller('notification', 'NotificationController');
    Route::controller('voucher', 'VoucherController');
    Route::controller('supplier', 'SupplierController');
});

Route::controller('checkout', 'CheckoutController');
Route::controller('user', 'UserController');
Route::controller('/', 'HomeController');
