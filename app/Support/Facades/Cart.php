<?php

namespace App\Support\Facades;

class Cart extends \Illuminate\Support\Facades\Facade
{
    protected static function getFacadeAccessor()
    {
        return 'cart';
    }
}
