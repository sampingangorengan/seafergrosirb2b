<?php

namespace App\Support\Facades;

class Category extends \Illuminate\Support\Facades\Facade
{
    protected static function getFacadeAccessor()
    {
        return 'category';
    }
}
