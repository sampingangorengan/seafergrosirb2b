<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        #Commands\Inspire::class,
        'App\Console\Commands\ApproveDelayedPayment',
        'App\Console\Commands\DummyRunner',
        'App\Console\Commands\JneTimeCompletion',
        'App\Console\Commands\orderExpire',
        'App\Console\Commands\ThreeDaysPaymentNotification',
        'App\Console\Commands\RecallGojek',
        'App\Console\Commands\UpdateGojekStatus',
        'App\Console\Commands\checkOrderExpire'
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        /*$schedule->command('inspire')
                 ->hourly();*/

         $schedule->command('order:check')
                  ->hourly();
        // $schedule->command('order:expire')
        //          ->everyFiveMinutes();
        // $schedule->command('gojek:recall')
        //          ->dailyAt('07:00');
        $schedule->command('gojek:update')
                 ->everyMinute();
        // $schedule->command('order:jnecomplete')
        //          ->everyFiveMinutes();
        $schedule->command('notify:threedays')
                 ->everyFiveMinutes();
    }
}
