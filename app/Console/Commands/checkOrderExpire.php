<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Order;
use App\Models\User;
use App\Models\Order\Payment;
use LocalizedCarbon;
use Illuminate\Support\Facades\Mail;


class checkOrderExpire extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run check to all order that has passed the limit for transfer and tempo';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $orderExceptCOD = Payment::whereIn('method',['transfer','tempo'])
                                    ->where('user_confirmed',0)
                                    ->whereNotIn('payment_status_id',["5","8"])->get();

        $adminMsg = "<p>Hi Admin</p>";

        foreach($orderExceptCOD as $payment){


            //echo "#order".$payment->order->id . "processed";
            $user = User::find($payment->order->user_id);
            $now = LocalizedCarbon::now()->timestamp;


            $orderDate = LocalizedCarbon::parse($payment->order->created_at);

            if($payment->method == "transfer"){
                $orderDate->addDays(1);
            }else
            if($payment->method == "tempo"){

                $orderDate->addDays($user->tempo->duration);
            }

            if($now > $orderDate->timestamp){
                $order = Order::find($payment->order_id);

                //If payment method transfer than cancel
                if($payment->method == "transfer"){
                    $order->order_status = 5;
                    $msg = "
                    <p>Pesanan anda dengan kode#".$payment->order->id." telah dibatalkan karena anda belum melakukan pembayaran</p>
                    ";
                }
                //If payment method tempo than pending
                else{
                    echo "foundddd";
                    $order->order_status = 10;
                    $msg = "
                    <p>Pesanan anda dengan kode#".$payment->order->id." telah melewati masa jatuh tempo. Segera lakukan pembayaran anda.</p>
                    ";
                }

                $order->save();

                //Mail User

                $user = User::find($payment->order->user_id);
                if($user){
                    $name = $user->username;
                    try{
                        Mail::queue('emails.general', compact('name','msg'), function ($message) use ($user) {

                            $message->from('hello@seafermart.co.id', '[NO-REPLY] Order status');

                            $message->to($user->email);

                        });
                    }catch(Exception $e) {

                    }
                }

                $adminMsg .= "
                <p>Pesanan <a href='".url('admin/orders'.$payment->order->id)."'>#".$payment->order->id."</a> untuk ".$user->username." mengalami perubahan status</p>
                ";

            }
        }
        $name = "admin";

        try{
            Mail::queue('emails.general', compact('name','adminMsg'), function ($message){

                $message->from('hello@seafermart.co.id', '[NO-REPLY] Order status changed');

                $message->to('hello@seafermart.co.id');

            });
        }catch(Exception $e) {

        }

    }
}
