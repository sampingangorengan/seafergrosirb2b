<?php

namespace App\Console\Commands;

use App\Models\Delivery;
use App\Models\GojekDetail;
use App\Models\Order;
use App\Models\Notification;
use GuzzleHttp\Client;
use DB;
use Illuminate\Console\Command;

class RecallGojek extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'gojek:recall';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Do the function of re-ordering gojek for those with driver not found status.';

    private function reorderGojek(Delivery $delivery) {
        $gojek = GojekDetail::find($delivery->detail_id);

        if(null !== $gojek) {
            $gojek->delete();

            $order = $delivery->order;

            $originLatLong = '-6.102789, 106.803789';
            $defaultAddress = $order->getShippingAddress($order->user_address_id);

            if ($defaultAddress) {

                $client = new Client();

                $address = urlencode($defaultAddress->address)
                    .'+'.urlencode($defaultAddress->area->name)
                    . '+' . urlencode($defaultAddress->area->city->city_name)
                    . '+' . urlencode($defaultAddress->area->city->province->province_name_id)
                    .'+'.urlencode($defaultAddress->postal_code);

                $address = str_replace(' ', '+', $address);


                $url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' . $address . '&key=AIzaSyCRDhe-WvUmE8Y96Gb6Glj-CPc9lfjC420';

                $res = $client->request('GET', $url);
                $results = json_decode($res->getBody())->results;
                $destLatLong = $results[0]->geometry->location->lat . ',' . $results[0]->geometry->location->lng;

                $items = $order->groceries;
                $list_item = '';
                foreach ($items as $item) {
                    $list_item .= $item->groceries->name.',';
                }

                //$order->getShippingAddress($order->user_address_id)->user->name;

                $recipient_name = null;
                if($defaultAddress->recipient !== '' && $defaultAddress->recipient !== null) {
                    $recipient_name = $defaultAddress->recipient;
                } else {
                    $recipient_name = $defaultAddress->user->name;
                }

                $recipient_phone = null;
                if($defaultAddress->phone_number !== '' && $defaultAddress->phone_number !== null) {
                    $recipient_phone = $defaultAddress->phone_number;
                } else {
                    $recipient_phone = $defaultAddress->user->phone;
                }

                unset($client);

                $client = new Client([
                    // Base URI is used with relative requests
                    'base_uri' => 'https://kilat-api.gojekapi.com',
                    // You can set any number of default request options.
                    'timeout'  => 60.0,
                ]);

                $headers = ['Client-ID' => env('GOJEK_CLIENT'), 'Pass-Key' => ENV('GOJEK_PASSKEY'), 'Content-Type' => 'application/json'];
                $body = preg_replace( "/\r|\n/", "",'
                {
                    "paymentType": 3,
                    "deviceToken": "",
                    "collection_location":"pickup",
                    "shipment_method":"Instant",
                    "booking_type":"Instant",
                    "routes": [{
                       "originNote": "Foodis Warehouse",
                       "originName": "",
                       "originAddress": "Jln. Cumi Raya No. 3, Muara Baru,RT.20/RW.17,Penjaringan, Jakarta Utara - 14440",
                       "originContactName": "Foodis",
                       "originContactPhone": "021-66696285",
                       "originLatLong": "'.trim($originLatLong).'",
                       "destinationNote": "",
                       "destinationName": "'.$defaultAddress->area->name.'",
                       "destinationContactName": "'.$recipient_name.'",
                       "destinationContactPhone": "'.$recipient_phone.'",
                       "destinationLatLong": "'.trim($destLatLong).'",
                       "destinationAddress": "'.$defaultAddress->address.'",
                       "serviceType": 10,
                       "storeOrderId":"'.$order->id.'",
                       "item": "'.$list_item.'"
                   }]
               }');

                try{
                    $res = $client->request('POST', '/gokilat/v10/booking', compact('headers', 'body'));
                } catch (\Exception $e) {
                    if($e->getResponse()->getReasonPhrase() == "Conflict") {
                        $request->session()->flash('alert-danger', 'Oops couldn\'t order Gojek for this order. You can only made request 2 minutes after the first request.');
                    } else {
                        $res = $e->getResponse()->getHeader('Error-Message');
                        $message = $res[0];
                        $request->session()->flash('alert-danger', 'Oops couldn\'t order Gojek for this order. '.$message);
                    }

                    return back();
                }

                $booking = json_decode($res->getBody()->getContents());
                /*var_dump($booking);
                var_dump($booking->orderNo);
                dd($booking);*/


                $gojek = $order->deliveries->detail_id;
                $save_gojek = $this->updateGojekDetail($gojek, $booking);

                if($save_gojek) {

                    return True;

                } else {
                    return False;
                }

            }

        }
    }

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $gojek_deliveries = GojekDetail::where('status', 'Driver Not Found')->get();

        if($gojek_deliveries->count() > 0) {
            foreach($gojek_deliveries as $gojek) {
                $stat = $this->reorderGojek($gojek->delivery);
            }
        }
    }
}
