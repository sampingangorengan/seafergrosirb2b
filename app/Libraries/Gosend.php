<?php

namespace App\Libraries;

use GuzzleHttp\Client;

class Gosend
{
    public static function getFee($originLatLong, $destLatLong, $serviceName)
    {
        $headers = ['Client-ID' => env('GOJEK_CLIENT'), 'Pass-Key' => env('GOJEK_PASSKEY'), 'Content-Type' => 'application/json'];
        $body = '{
            "routeRequests": [
                {
                    "serviceType":"10",
                    "originLatLong":"'.$originLatLong.'",
                    "destinationLatLong":"'.$destLatLong.'"
                }],
                "shipment_method":"'.$serviceName.'"
        }';

        $client = new Client;

        if(env('APP_ENV') == 'production'){
            $res = $client->request('GET', env('GOJEK_URI_PRODUCTION') . '/gokilat/v10/calculate/price', compact('headers', 'body'));
        }else{
            $res = $client->request('GET', env('GOJEK_URI_STAGING')'/gokilat/v10/calculate/price', compact('headers', 'body'));
        }


        return json_decode($res->getBody())->totalPrice;
    }
}
