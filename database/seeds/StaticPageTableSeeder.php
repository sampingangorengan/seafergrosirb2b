<?php

use Illuminate\Database\Seeder;

class StaticPageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('static_pages')->insert([
        	['page_name' => 'home', 'page_link' => '', 'created_at' => '', 'updated_at' => ''],
        	['page_name' => 'about us', 'page_link' => ''],
        	['page_name' => 'cashback rewards', 'page_link' => ''],
        	['page_name' => 'payment', 'page_link' => ''],
        	['page_name' => 'delivery', 'page_link' => ''],
        	['page_name' => 'return & exchange', 'page_link' => ''],
        	['page_name' => 'return & exchange - form', 'page_link' => ''],
        	['page_name' => 'wholesale order', 'page_link' => ''],
        	['page_name' => 'suggest_product', 'page_link' => ''],
        	['page_name' => 'faq', 'page_link' => ''],
        	['page_name' => 'privacy policy', 'page_link' => ''],
        	['page_name' => 'terms & condition', 'page_link' => ''],
        	['page_name' => 'contact us', 'page_link' => ''],
        	['page_name' => 'social media link', 'page_link' => ''],
        	['page_name' => 'apps download link', 'page_link' => ''],
        ]);
    }
}
