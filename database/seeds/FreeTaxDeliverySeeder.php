<?php

use Illuminate\Database\Seeder;

class FreeTaxDeliverySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('free_tax_deliveries')->insert([
            'minimum_spending' => 10000,
            'quota' => 20,
            'usage' => 0,
            'remaining' => 0,
            'start_date' => '2018-07-07',
            'end_date' => '2018-10-25',
            'type' => 'delivery',
            'is_active' => 1
        ]);
    }
}
