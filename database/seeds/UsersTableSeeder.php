<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    private function insertAddress($userId) {
        DB::table('user_addresses')->insert([
            [
                'user_id'      => $userId,
                'name'         => 'Home',
                'address'      => 'Jalan Radio Dalam Raya no. F11, RT.5/RW.4',
                'area_id'      => 1,
                'postal_code'  => '12140',
                'phone_number' => '217396940'
            ],
        ]);
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = App\Models\User::whereEmail('john@doe.com')->first();
        if ( ! $user) {
            $id = DB::table('users')->insertGetId(
                ['name' => 'John Doe', 'email' => 'john@doe.com', 'password' => Hash::make('password')]
            );

            $this->insertAddress($id);
        } elseif (App\Models\User\Address::whereUserId($user->id)->count() == 0) {
            $this->insertAddress($user->id);
        }
    }
}
