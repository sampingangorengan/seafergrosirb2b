<?php

use Illuminate\Database\Seeder;
use App\Models\Groceries;

class GroceriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Groceries::class, 200)->create();
        
        // http://php.net/manual/en/function.copy.php
        // Duplicate-safe
        // png
        copy(public_path('assets/images/box2.png'), public_path('contents/box2.jpg'));

        // jpg
        copy(public_path('assets/images/box1.jpg'), public_path('contents/box1.jpg'));
        copy(public_path('assets/images/box3.jpg'), public_path('contents/box3.jpg'));
        copy(public_path('assets/images/box4.jpg'), public_path('contents/box4.jpg'));
        copy(public_path('assets/images/box5.jpg'), public_path('contents/box5.jpg'));
        copy(public_path('assets/images/box6.jpg'), public_path('contents/box6.jpg'));

        foreach (Groceries::all() as $item) {
            DB::table('groceries_images')->insert([
                ['file_name' => 'box'.rand(1, 2).'.jpg', 'groceries_id' => $item->id],
                ['file_name' => 'box'.rand(3, 4).'.jpg', 'groceries_id' => $item->id],
                ['file_name' => 'box'.rand(5, 6).'.jpg', 'groceries_id' => $item->id],
            ]);
        }
    }
}
