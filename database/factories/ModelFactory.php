<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Models\Groceries::class, function (Faker\Generator $faker) {
    return [
        'name' => ucwords($faker->words(2, true)).', 30 gr',
        'price' => $faker->numberBetween(4500, 155000),
        'stock' => $faker->numberBetween(10, 50),
        'description' => $faker->paragraph(),
        'nutrients' => $faker->paragraph(),
        'ingredients' => $faker->paragraph(),
        'category_id' => $faker->numberBetween(1, 15),
    ];
});
