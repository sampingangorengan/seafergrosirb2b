<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterGroceriesTableAddPackingSupplierAvailablityNote extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('groceries', function (Blueprint $table) {
            $table->string('packing')->nullable();
            $table->integer('supplier_id')->nullable();
            $table->string('availabity_note')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('groceries', function (Blueprint $table) {
            $table->dropColumn('supplier_id');
            $table->dropColumn('packing');
            $table->dropColumn('availabity_note');
        });
    }
}
