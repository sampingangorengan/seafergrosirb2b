<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableDonationVoucher extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('donation_vouchers', function (Blueprint $table) {
            //
            $table->dropColumn('applied_to');
            $table->dropColumn('name');
            $table->string('title')->after('id');
            $table->string('slug');
            $table->string('code')->unique();
            $table->text('description');
            $table->enum('value_type', ['percent', 'nominal'])->after('value');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('donation_vouchers', function (Blueprint $table) {
            //
        });
    }
}
