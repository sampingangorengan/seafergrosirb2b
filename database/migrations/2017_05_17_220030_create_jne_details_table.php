<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJneDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jne_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('unique_number');
            $table->string('date');
            $table->string('pod_receiver');
            $table->string('receiver_name');
            $table->string('city_name');
            $table->string('pod_date');
            $table->string('pod_status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('jne_details');
    }
}
