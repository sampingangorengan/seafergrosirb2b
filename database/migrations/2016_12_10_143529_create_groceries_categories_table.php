<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroceriesCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('groceries_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('uri');
            $table->tinyInteger('order');
            $table->timestamps();
        });

        DB::table('groceries_categories')->insert([
            ['name' => 'Seafood', 'uri' => 'seafood', 'order' => 1],
            ['name' => 'Meat', 'uri' => 'meat', 'order' => 2],
            ['name' => 'Fruits', 'uri' => 'fruits', 'order' => 3],
            ['name' => 'Vegetables', 'uri' => 'vegetables', 'order' => 4],
            ['name' => 'Dairy', 'uri' => 'dairy', 'order' => 5],
            ['name' => 'Deli', 'uri' => 'deli', 'order' => 6],
            ['name' => 'Pantry', 'uri' => 'pantry', 'order' => 7],
            ['name' => 'Snacks', 'uri' => 'snacks', 'order' => 8],
            ['name' => 'Dry & Canned', 'uri' => 'dry-and-canned', 'order' => 9],
            ['name' => 'Frozen', 'uri' => 'frozen', 'order' => 10],
            ['name' => 'Healthy', 'uri' => 'healthy', 'order' => 11],
            ['name' => 'Baby', 'uri' => 'baby', 'order' => 12],
            ['name' => 'Personal Care & Drugs', 'uri' => 'personal-care-and-drugs', 'order' => 13],
            ['name' => 'Household', 'uri' => 'household', 'order' => 14],
            ['name' => 'Cigarette', 'uri' => 'cigarette', 'order' => 15],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('groceries_categories');
    }
}
