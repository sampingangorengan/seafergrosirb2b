<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFreeTaxDeliveriesTableChangeStartDateAndEndDateColumnType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('free_tax_deliveries', function (Blueprint $table) {
            $table->dropColumn('start_date');
            $table->dropColumn('end_date');
        });
        Schema::table('free_tax_deliveries', function (Blueprint $table) {
            $table->dateTime('start_date');
            $table->dateTime('end_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('free_tax_deliveries', function (Blueprint $table) {
            $table->dropColumn('start_date');
            $table->dropColumn('end_date');
        });
        Schema::table('free_tax_deliveries', function (Blueprint $table) {
            $table->date('start_date');
            $table->date('end_date');
        });
    }
}
