<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGojekDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gojek_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('gojek_id');
            $table->string('gojek_order');
            $table->string('status');
            $table->integer('driver_id')->nullable();
            $table->string('driver_name')->nullable();
            $table->string('driver_photo')->nullable();
            $table->float('total_price')->nullable();
            $table->string('receiver_name')->nullable();
            $table->timestamp('order_created_time')->nullable();
            $table->timestamp('order_dispatch_time')->nullable();
            $table->timestamp('order_arrival_time')->nullable();
            $table->text('seller_address_detail')->nullable();
            $table->string('seller_address_name')->nullable();
            $table->text('buyer_address_detail')->nullable();
            $table->string('buyer_address_name')->nullable();
            $table->integer('store_order_id')->nullable();
            $table->text('cancel_description')->nullable();
            $table->string('booking_type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gojek_details');
    }
}
