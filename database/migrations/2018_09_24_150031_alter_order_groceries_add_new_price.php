<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOrderGroceriesAddNewPrice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {



        Schema::table('order_groceries',function(Blueprint $table){
            $table->integer('changed_by_id')->nullable()->unsigned()->index();
            $table->string('change_note')->nullable();
            $table->string('change_amount')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_groceries',function(Blueprint $table){
            $table->dropColumn('change_note');
            $table->dropColumn('change_amount');
            $table->dropColumn('changed_by_id');
        });


    }
}
