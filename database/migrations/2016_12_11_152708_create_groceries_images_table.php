<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroceriesImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('groceries_images', function (Blueprint $table) {
            $table->increments('id');
            $table->string('file_name');
            $table->integer('groceries_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('groceries_images', function ($table) {
            $table->foreign('groceries_id')->references('id')
                ->on('groceries')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('groceries_images');
    }
}
