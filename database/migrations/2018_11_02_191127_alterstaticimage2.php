<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Alterstaticimage2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
         Schema::table('static_images', function (Blueprint $table) {
             $table->string('size_note')->after('image_location_detail')->nullable();
             $table->integer('image_location')->default(0)->change();
         });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::table('static_images', function (Blueprint $table) {
             $table->dropColumn('size_note');
             $table->string('image_location')->change();
         });
     }
}
