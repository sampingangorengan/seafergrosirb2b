<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id')->unsigned();
            $table->enum('method', ['transfer']);
            $table->string('transfer_dest_account');
            $table->string('transfer_sender_account_number');
            $table->string('transfer_sender_account_name');
            $table->date('transfer_date');
            $table->time('transfer_time');
            $table->string('transfer_note');
            $table->string('transfer_receipt_file');
            $table->timestamps();
        });

        Schema::table('order_payments', function ($table) {
            $table->foreign('order_id')->references('id')
                ->on('orders')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('order_payments');
    }
}
