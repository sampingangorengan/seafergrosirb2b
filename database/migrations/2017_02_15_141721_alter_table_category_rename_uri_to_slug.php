<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableCategoryRenameUriToSlug extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('groceries_categories', function (Blueprint $table) {
            //
            $table->dropColumn('is_parent');
            $table->dropColumn('is_child');
            $table->dropColumn('parent');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('groceries_categories', function (Blueprint $table) {
            //
        });
    }
}
