<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterParentCategoriesAddImageColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('groceries_categories', function (Blueprint $table) {
            $table->integer('parent_category_image_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('groceries_categories', function (Blueprint $table) {
            $table->dropColumn('parent_category_image_id');
        });
    }
}
