<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOrderGroceriesTableAddRateColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_groceries', function (Blueprint $table) {
            //
            $table->integer('rating')->unsigned()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_groceries', function (Blueprint $table) {
            //
            $table->dropColumn('rating');
        });
    }
}
