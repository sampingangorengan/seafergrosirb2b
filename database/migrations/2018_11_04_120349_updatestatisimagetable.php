<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Updatestatisimagetable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        DB::table('static_images')->insert([
            "id" => 49,
            "page_id" => 2,
            "image_name" => "/assets/images/new_about/about-slider.jpg",
            "image_location" => 0,
            "image_location_detail" => "About slider",
            "size_note" => ""
        ]);
        //Why choose section
        DB::table('static_images')->insert([
            "id" => 50,
            "page_id" => 2,
            "image_name" => "/assets/images/new_about/choose-1.png",
            "image_location" => 1,
            "image_location_detail" => "Kenapa Foodis 1",
            "size_note" => ""
        ]);
        DB::table('static_images')->insert([
            "id" => 51,
            "page_id" => 2,
            "image_name" => "/assets/images/new_about/choose-2.png",
            "image_location" => 2,
            "image_location_detail" => "Kenapa Foodis 2",
            "size_note" => ""
        ]);
        DB::table('static_images')->insert([
            "id" => 52,
            "page_id" => 2,
            "image_name" => "/assets/images/new_about/choose-3.png",
            "image_location" => 3,
            "image_location_detail" => "Kenapa Foodis 3",
            "size_note" => ""
        ]);

        //fish over meat poultry section
        DB::table('static_images')->insert([
            "id" => 53,
            "page_id" => 2,
            "image_name" => "/assets/images/new_about/img-fish-and-meat.png",
            "image_location" => 4,
            "image_location_detail" => "Kenapa makan ikan",
            "size_note" => ""
        ]);

        //Fish benefit section
        DB::table('static_images')->insert([
            "id" => 54,
            "page_id" => 2,
            "image_name" => "/assets/images/new_about/benefit-eat-fish.png",
            "image_location" => 5,
            "image_location_detail" => "Keuntungan makan ikan",
            "size_note" => ""
        ]);

        //Comparison produksi dan konsumsi ikan
        DB::table('static_images')->insert([
            "id" => 55,
            "page_id" => 2,
            "image_name" => "/assets/images/new_about/fish_compare.png",
            "image_location" => 6,
            "image_location_detail" => "Perbandingan produksi dan konsumsi",
            "size_note" => ""
        ]);

        //Konsumsi makan ikan nasional section
        DB::table('static_images')->insert([
            "id" => 56,
            "page_id" => 2,
            "image_name" => "/assets/images/new_about/chart.png",
            "image_location" => 7,
            "image_location_detail" => "Chart konsumsi makan ikan",
            "size_note" => ""
        ]);

        DB::table('static_images')->insert([
            "id" => 57,
            "page_id" => 2,
            "image_name" => "/assets/images/new_about/eat-fish.png",
            "image_location" => 8,
            "image_location_detail" => "Keluarga makan ikan",
            "size_note" => ""
        ]);

        DB::table('static_images')->insert([
            "id" => 58,
            "page_id" => 2,
            "image_name" => "/assets/images/new_about/gemarikan.jpg",
            "image_location" => 9,
            "image_location_detail" => "gemar makan ikan",
            "size_note" => ""
        ]);


        //Product suggestion section
        DB::table('static_images')->insert([
            "id" => 59,
            "page_id" => 9,
            "image_name" => "/assets/images/suggest-1.png",
            "image_location" => 0,
            "image_location_detail" => "sugeestion left",
            "size_note" => ""
        ]);

        DB::table('static_images')->insert([
            "id" => 60,
            "page_id" => 9,
            "image_name" => "/assets/images/suggest-2.png",
            "image_location" => 1,
            "image_location_detail" => "sugeestion right",
            "size_note" => ""
        ]);

        DB::table('static_images')->insert([
            "id" => 61,
            "page_id" => 1,
            "image_name" => "/assets/images/ICON-01.png",
            "image_location" => 0,
            "image_location_detail" => "kualitas terjamin image",
            "size_note" => ""
        ]);
        DB::table('static_images')->insert([
            "id" => 62,
            "page_id" => 1,
            "image_name" => "/assets/images/ICON-02.png",
            "image_location" => 1,
            "image_location_detail" => "harga terjangkau image",
            "size_note" => ""
        ]);
        DB::table('static_images')->insert([
            "id" => 63,
            "page_id" => 1,
            "image_name" => "/assets/images/ICON-03.png",
            "image_location" => 2,
            "image_location_detail" => "produk lengkap image",
            "size_note" => ""
        ]);
        DB::table('static_images')->insert([
            "id" => 64,
            "page_id" => 1,
            "image_name" => "/assets/images/ICON-09.png",
            "image_location" => 3,
            "image_location_detail" => "pembayaran fleksibel image",
            "size_note" => ""
        ]);
        DB::table('static_images')->insert([
            "id" => 65,
            "page_id" => 1,
            "image_name" => "/assets/images/ICON-04.png",
            "image_location" => 4,
            "image_location_detail" => "gratis pengiriman image",
            "size_note" => ""
        ]);

        DB::table('static_images')->insert([
            "id" => 66,
            "page_id" => 13,
            "image_name" => "/assets/images/contact-us.png",
            "image_location" => 0,
            "image_location_detail" => "header contact us",
            "size_note" => ""
        ]);

        DB::table('static_images')->insert([
            "id" => 67,
            "page_id" => 13,
            "image_name" => "/assets/images/ico-phone.png",
            "image_location" => 1,
            "image_location_detail" => "phone",
            "size_note" => ""
        ]);

        DB::table('static_images')->insert([
            "id" => 68,
            "page_id" => 13,
            "image_name" => "/assets/images/ico-line.png",
            "image_location" => 2,
            "image_location_detail" => "line",
            "size_note" => ""
        ]);

        DB::table('static_images')->insert([
            "id" => 69,
            "page_id" => 13,
            "image_name" => "/assets/images/ico-messag2.png",
            "image_location" => 3,
            "image_location_detail" => "email",
            "size_note" => ""
        ]);

        DB::table('static_images')->insert([
            "id" => 70,
            "page_id" => 13,
            "image_name" => "/assets/images/ico-location.png",
            "image_location" => 4,
            "image_location_detail" => "location",
            "size_note" => ""
        ]);


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        for($i = 49 ; $i <= 70 ; $i++){
            DB::table('static_images')->where('id','=',$i)->delete();
        }
    }
}
