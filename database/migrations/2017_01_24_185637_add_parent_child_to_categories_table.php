<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddParentChildToCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('groceries_categories', function (Blueprint $table) {
            //
            $table->enum('is_parent', [0,1]);
            $table->enum('is_child', [0,1]);
            $table->integer('parent');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('groceries_categories', function (Blueprint $table) {
            //
            $table->dropColumn('is_parent');
            $table->dropColumn('is_child');
            $table->dropColumn('parent');
        });
    }
}
