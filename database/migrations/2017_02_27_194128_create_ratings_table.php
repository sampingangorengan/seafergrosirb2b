<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRatingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfexists('ratings');

        Schema::create('ratings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('rate');
            $table->string('title');
            $table->text('review');
            $table->integer('user_id')->unsigned()->default(0);
            $table->integer('groceries_id')->unsigned()->default(0);
            $table->integer('is_active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ratings');
    }
}
