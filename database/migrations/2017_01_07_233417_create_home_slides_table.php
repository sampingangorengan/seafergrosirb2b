<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomeSlidesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('home_slides', function (Blueprint $table) {
            $table->increments('id');
            $table->string('file_name');
            $table->tinyInteger('order');
            $table->timestamps();
        });

        DB::table('home_slides')->insert([
            ['file_name' => 'default-slide1.png', 'order' => 1],
            ['file_name' => 'default-slide2.jpg', 'order' => 2],
            ['file_name' => 'default-slide3.jpg', 'order' => 3],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('home_slides');
    }
}
