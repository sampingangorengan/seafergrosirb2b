<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loc_cities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('province_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('loc_cities', function ($table) {
            $table->foreign('province_id')->references('id')
                ->on('loc_provinces')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });

        DB::table('loc_cities')->insert([
            ['id' => 1, 'name' => 'South Jakarta', 'province_id' => 1],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('loc_cities');
    }
}
