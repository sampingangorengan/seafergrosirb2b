<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailPreferencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_preferences', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('promo')->default(0);
            $table->integer('new_product')->default(0);
            $table->integer('editorial')->default(0);
            $table->integer('low_stock')->default(0);
            $table->integer('latest_news')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('email_preferences');
    }
}
