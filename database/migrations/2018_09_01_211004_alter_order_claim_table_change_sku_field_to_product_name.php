<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOrderClaimTableChangeSkuFieldToProductName extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_claims', function (Blueprint $table) {
            $table->renameColumn('sku', 'product_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_claims', function (Blueprint $table) {
            $table->renameColumn('product_name', 'sku');
        });
    }
}
