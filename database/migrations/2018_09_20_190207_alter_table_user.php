<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('users', function (Blueprint $table) {
            $table->text('company_name')->after('id');
            $table->text('comp_npwp')->after('company_name');
            $table->string('upload_npwp')->after('comp_npwp');
            $table->text('brand_name')->after('upload_npwp');
            $table->char('business_entity',15)->after('brand_name');
            $table->text('billing_address')->after('business_entity');
            $table->char('province',4)->after('billing_address');
            $table->char('city',4)->after('province');
            $table->char('area',4)->after('city');
            $table->char('postal_code',4)->after('area');
            $table->string('choose_shipping_address')->after('postal_code');
            $table->text('shipping_address')->after('choose_shipping_address');
            $table->char('shipping_province',4)->after('shipping_address');
            $table->char('shipping_city',4)->after('shipping_province');
            $table->char('shipping_area',4)->after('shipping_city');
            $table->char('shipping_postal_code',5)->after('shipping_area');
            $table->char('company_phone',14)->after('shipping_postal_code');
            $table->text('company_email')->after('company_phone');
            $table->text('website')->after('company_email');
            $table->char('category',2)->after('website');
            $table->string('ktp')->after('name');
            $table->string('payment_method');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('company_name');
            $table->dropColumn('brand_name');
            $table->dropColumn('business_entity');
            $table->dropColumn('billing_address');
            $table->dropColumn('province');
            $table->dropColumn('city');
            $table->dropColumn('area');
            $table->dropColumn('postal_code');
            $table->dropColumn('choose_shipping_address');
            $table->dropColumn('shipping_address');
            $table->dropColumn('shipping_province');
            $table->dropColumn('shipping_city');
            $table->dropColumn('shipping_area');
            $table->dropColumn('shipping_postal_code');
            $table->dropColumn('company_phone');
            $table->dropColumn('company_email');
            $table->dropColumn('website');
            $table->dropColumn('category');
            $table->dropColumn('ktp');
            $table->dropColumn('comp_npwp');
            $table->dropColumn('upload_npwp');
            $table->dropColumn('payment_method');
        });
    }
}
