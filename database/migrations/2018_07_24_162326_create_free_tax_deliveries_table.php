<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFreeTaxDeliveriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('free_tax_deliveries', function (Blueprint $table) {
            $table->increments('id');
            $table->float('minimum_spending');
            $table->integer('quota');
            $table->integer('usage');
            $table->integer('remaining');
            $table->date('start_date');
            $table->date('end_date');
            $table->enum('type', ['tax', 'delivery'])->default('delivery');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('free_tax_deliveries');
    }
}
