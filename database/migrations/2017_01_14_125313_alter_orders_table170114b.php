<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOrdersTable170114b extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('shipper_code');
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->enum('shipping_service_code', ['gosend_same_day', 'gosend_instant'])->default('gosend_same_day')->after('user_address_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('shipping_service_code');
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->enum('shipper_code', ['gosend_same_day', 'gosend_instant'])->default('gosend_same_day')->after('user_address_id');
        });
    }
}
