<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterGojekDetailsTableAddDriverPhone extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gojek_details', function (Blueprint $table) {
            //
            $table->string('driver_phone')->nullable()->after('driver_name');
            $table->timestamp('order_closed_time')->nullable()->after('order_arrival_time');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gojek_details', function (Blueprint $table) {
            //
            $table->dropColumn(['driver_phone', 'order_closed_time']);
        });
    }
}
