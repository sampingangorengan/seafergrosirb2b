<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AlterOrderPaymentsTableChangePaymentMethodToEnumTransferCod extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_payments', function (Blueprint $table) {
            #$table->enum('method', ['transfer', 'cod'])->default('transfer')->change();
            DB::statement("ALTER TABLE order_payments MODIFY method ENUM('transfer', 'cod') NOT NULL  DEFAULT 'transfer'");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_payments', function (Blueprint $table) {
            DB::statement("ALTER TABLE order_payments MODIFY method ENUM('transfer') NOT NULL  DEFAULT 'transfer'");
        });
    }
}
