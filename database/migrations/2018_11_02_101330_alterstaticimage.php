<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Alterstaticimage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
         Schema::table('static_images', function (Blueprint $table) {
             $table->string('image_location_detail')->after('image_location')->nullable();
         });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::table('static_images', function (Blueprint $table) {
             $table->dropColumn('image_location_detail');
         });
     }
}
