<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterJneDetailsTableAddBookingTypeColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('jne_details', function (Blueprint $table) {
            //
            $table->string('booking_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('jne_details', function (Blueprint $table) {
            //
            $table->dropColumn('booking_type');
        });
    }
}
