<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOrderPaymentNewTempoMethod extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE `order_payments` CHANGE COLUMN `method` `method` ENUM('transfer','cod','tempo') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'transfer'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE `order_payments` CHANGE COLUMN `method` `method` ENUM('transfer','cod') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'transfer'");
    }
}
