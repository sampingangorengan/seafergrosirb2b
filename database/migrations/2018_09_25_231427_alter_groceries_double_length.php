<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterGroceriesDoubleLength extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        DB::statement("ALTER TABLE `groceries` CHANGE COLUMN `price` `price` DOUBLE(15,2) NOT NULL DEFAULT 0.00");
        DB::statement("ALTER TABLE `groceries` CHANGE COLUMN `price_after_discount` `price_after_discount` DOUBLE(15,2) NOT NULL DEFAULT 0.00");
        DB::statement("ALTER TABLE `order_groceries` CHANGE COLUMN `price` `price` DOUBLE(15,2) NOT NULL DEFAULT 0.00");



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE `groceries` CHANGE COLUMN `price` `price` DOUBLE(8,2) NOT NULL DEFAULT 0.00");
        DB::statement("ALTER TABLE `groceries` CHANGE COLUMN `price_after_discount` `price_after_discount` DOUBLE(8,2) NOT NULL DEFAULT 0.00");
        DB::statement("ALTER TABLE `order_groceries` CHANGE COLUMN `price` `price` DOUBLE(8,2) NOT NULL DEFAULT 0.00");
    }
}
