<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableUserSupplier extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('user_supplier', function (Blueprint $table) {
             $table->renameColumn('google_verif', 'email_verif');
             $table->renameColumn('id_status', 'is_active');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('user_supplier', function (Blueprint $table) {
             $table->renameColumn('email_verif', 'google_verif');
             $table->renameColumn('is_active', 'id_status');
        });
    }
}
