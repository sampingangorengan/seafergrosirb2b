<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBestPriceClaimsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('best_price_claims', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email');
            $table->string('item_name');
            $table->integer('order_id')->index()->unsigned();
            $table->double('seafer_price');
            $table->string('seafer_link');
            $table->double('other_price');
            $table->string('other_link');
            $table->string('store_location');
            $table->integer('claim_screenshot')->index()->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('best_price_claims');
    }
}
