<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterGojekDetailTableAllowNull extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('gojek_details', function ($table) {
            $table->integer('gojek_id')->nullable()->change();
            $table->string('gojek_order')->nullable()->change();
            $table->string('status')->nullable()->change();
            $table->string('booking_type')->nullable()->default('SameDay')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
