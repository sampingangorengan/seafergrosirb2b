<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAreasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loc_areas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('city_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('loc_areas', function ($table) {
            $table->foreign('city_id')->references('id')
                ->on('loc_cities')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });

        DB::table('loc_areas')->insert([
            ['id' => 1, 'name' => 'Kebayoran Baru', 'city_id' => 1],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('loc_areas');
    }
}
