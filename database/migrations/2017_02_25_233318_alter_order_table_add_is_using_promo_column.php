<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOrderTableAddIsUsingPromoColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('orders', function (Blueprint $table) {
            //
            $table->integer('is_using_promo')->unsigned()->default(0)->after('order_status');
            $table->float('nett_total')->default(0);
            $table->float('tax')->default(0);
            $table->float('promo_value')->default(0);
            $table->float('grand_total')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('orders', function (Blueprint $table) {
            //
            $table->dropColumn(['is_using_promo', 'nett_total', 'tax', 'promo_value', 'grand_total']);
        });
    }
}
