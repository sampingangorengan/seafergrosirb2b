<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroceriesFavourites extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('groceries_favourites', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('groceries_id')->unsigned()->index();
            $table->integer('user_id')->unsigned()->index();
            $table->timestamps();
            $table->unique(['groceries_id', 'user_id']);
        });

        Schema::table('groceries_favourites', function ($table) {
            $table->foreign('groceries_id')->references('id')
                ->on('groceries')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });

        Schema::table('groceries_favourites', function ($table) {
            $table->foreign('user_id')->references('id')
                ->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('groceries_favourites');
    }
}
