<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterGroceriesFavouritesAddUsualQty extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('groceries_favourites', function (Blueprint $table) {
            $table->string('usual_qty')->default(0);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('groceries_favourites', function (Blueprint $table) {
            $table->dropColumn('usual_qty');
        });
    }
}
