<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_company', function(Blueprint $table){
            $table->increments('id');
            $table->text('category_name');
            $table->char('status',1);
            $table->timestamps();
        });
        // Insert 
        DB::table('category_company')->insert(
            array(
                array(
                    'category_name' => 'Restaurant',
                    'status' => 1,
                ),
                array(
                    'category_name' => 'Catering',
                    'status' => 1,
                ),
                array(
                    'category_name' => 'Hotel',
                    'status' => 1,
                ),
                array(
                    'category_name' => 'Cafe',
                    'status' => 1,
                ),
                array(
                    'category_name' => 'Warung',
                    'status' => 1,
                ),
                array(
                    'category_name' => 'Street Food',
                    'status' => 1,
                ),
                array(
                    'category_name' => 'Hospital',
                    'status' => 1,
                ),
                array(
                    'category_name' => 'Home Industry',
                    'status' => 1,
                ),
                array(
                    'category_name' => 'Supermarket',
                    'status' => 1,
                ),
                array(
                    'category_name' => 'Toko',
                    'status' => 1,
                ),
                array(
                    'category_name' => 'Pasar',
                    'status' => 1,
                ),
                array(
                    'category_name' => 'Office',
                    'status' => 1,
                ),
            )
        );

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('category_company');
    }
}
