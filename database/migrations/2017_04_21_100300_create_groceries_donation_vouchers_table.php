<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroceriesDonationVouchersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('groceries_donation_vouchers', function (Blueprint $table) {
           $table->increments('id');
           $table->integer('groceries_id');
           $table->integer('donation_vouchers_id');
           $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('groceries_donation_vouchers');
    }
}
