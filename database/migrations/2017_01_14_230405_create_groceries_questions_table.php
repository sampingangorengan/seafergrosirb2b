<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroceriesQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('groceries_questions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('groceries_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->text('question');
            $table->text('answer');
            $table->timestamps();
        });

        Schema::table('groceries_questions', function ($table) {
            $table->foreign('groceries_id')->references('id')
                ->on('groceries')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });

        Schema::table('groceries_questions', function ($table) {
            $table->foreign('user_id')->references('id')
                ->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('groceries_questions');
    }
}
