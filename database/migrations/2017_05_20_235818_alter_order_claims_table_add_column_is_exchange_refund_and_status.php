<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOrderClaimsTableAddColumnIsExchangeRefundAndStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_claims', function (Blueprint $table) {
            //
            $table->string('is_exchange_refund');
            $table->string('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_claims', function (Blueprint $table) {
            //
            $table->dropColumn(['is_exchange_refund','status']);
        });
    }
}
