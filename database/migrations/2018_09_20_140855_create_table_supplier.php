<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSupplier extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('user_supplier', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('company_name');
            $table->char('bussiness_entity',11);
            $table->string('billing_address');
            $table->char('province',4);
            $table->char('city',4);
            $table->char('area',4);
            $table->char('postcode',8);
            $table->char('title',3);
            $table->string('user_name',50);
            $table->char('phone_number',14);
            $table->string('email');
            $table->string('website');
            $table->char('category',2);
            $table->char('commoditas',2);
            $table->char('selling_method',2);
            $table->string('password');
            $table->string('type_of_goods');
            $table->char('product_origin',6);
            $table->char('country',3);
            $table->string('google_verif');
            $table->string('id_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('user_supplier');
    }
}
