<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCartsTable161212a extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('carts', function (Blueprint $table) {
            $table->unique(['user_id', 'groceries_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('carts', function (Blueprint $table) {
            
            // Prevent "SQLSTATE[HY000]: General error: 1553"
            $table->dropForeign('carts_groceries_id_foreign');
            $table->dropForeign('carts_user_id_foreign');

            $table->dropUnique('carts_user_id_groceries_id_unique');
        });

        // Bring them back
        Schema::table('carts', function ($table) {
            $table->foreign('user_id')->references('id')
                ->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('groceries_id')->references('id')
                ->on('groceries')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }
}
