<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterGroceriesTable161211a extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('groceries', function (Blueprint $table) {
            $table->text('description')->after('stock');
            $table->text('nutrients')->after('description');
            $table->text('ingredients')->after('nutrients');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('groceries', function (Blueprint $table) {
            $table->dropColumn(['ingredients', 'nutrients', 'description']);
        });
    }
}
