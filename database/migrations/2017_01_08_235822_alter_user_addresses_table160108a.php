<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\User\Address;

class AlterUserAddressesTable160108a extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_addresses', function (Blueprint $table) {
            $table->integer('area_id')->unsigned()->after('address');
        });

        foreach (Address::all() as $address) {
            $address->area_id = 1;
            $address->save();
        }

        Schema::table('user_addresses', function ($table) {
            $table->foreign('area_id')->references('id')
                ->on('loc_areas')
                ->onUpdate('cascade')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_addresses', function (Blueprint $table) {
            $table->dropForeign(['area_id']);
            $table->dropColumn('area_id');
        });
    }
}
