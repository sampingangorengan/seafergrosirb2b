<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterGroceriesCategoriesTable161210a extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('groceries_categories', function (Blueprint $table) {
            $table->string('name')->unique()->change();
            $table->string('uri')->unique()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('groceries_categories', function (Blueprint $table) {
            $table->dropUnique('groceries_categories_uri_unique')->change();
            $table->dropUnique('groceries_categories_name_unique')->change();
        });
    }
}
