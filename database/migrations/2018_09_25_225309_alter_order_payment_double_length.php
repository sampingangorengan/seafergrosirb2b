<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOrderPaymentDoubleLength extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_payments',function(Blueprint $table){
            DB::statement("ALTER TABLE `order_payments` CHANGE COLUMN `transfer_amount` `transfer_amount` DOUBLE(15,2) NOT NULL DEFAULT 0.00");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_payments',function(Blueprint $table){
            DB::statement("ALTER TABLE `order_payments` CHANGE COLUMN `transfer_amount` `transfer_amount` DOUBLE(8,2) NOT NULL DEFAULT 0.00");
        });
    }
}
