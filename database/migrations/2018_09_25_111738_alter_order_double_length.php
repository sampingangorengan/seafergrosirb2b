<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOrderDoubleLength extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE `orders` CHANGE COLUMN `nett_total` `nett_total` DOUBLE(15,2) NOT NULL DEFAULT 0.00");
        DB::statement("ALTER TABLE `orders` CHANGE COLUMN `grand_total` `grand_total` DOUBLE(15,2) NOT NULL DEFAULT 0.00");
        DB::statement("ALTER TABLE `orders` CHANGE COLUMN `tax` `tax` DOUBLE(15,2) NOT NULL DEFAULT 0.00");
        DB::statement("ALTER TABLE `orders` CHANGE COLUMN `promo_value` `promo_value` DOUBLE(15,2) NOT NULL DEFAULT 0.00");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("ALTER TABLE `orders` CHANGE COLUMN `nett_total` `nett_total` DOUBLE(8,2) NOT NULL DEFAULT 0.00");
        DB::statement("ALTER TABLE `orders` CHANGE COLUMN `grand_total` `grand_total` DOUBLE(8,2) NOT NULL DEFAULT 0.00");
        DB::statement("ALTER TABLE `orders` CHANGE COLUMN `tax` `tax` DOUBLE(8,2) NOT NULL DEFAULT 0.00");
        DB::statement("ALTER TABLE `orders` CHANGE COLUMN `promo_value` `promo_value` DOUBLE(8,2) NOT NULL DEFAULT 0.00");
    }
}
