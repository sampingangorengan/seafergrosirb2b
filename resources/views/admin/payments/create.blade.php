@extends('admin.layouts.master')

@section('title')
    Create Transactions Manually | Control Room
@endsection

@section('mycss')
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="{!! asset('bower_components/AdminLTE') !!}/plugins/datepicker/datepicker3.css">
@endsection

@section('content-header')

    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))

                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
        @endforeach
    </div>

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Create New Transactions Manually
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ url('admin/transactions') }}">Transactions</a></li>
            <li class="active">Create New Transactions</li>
        </ol>
    </section>
@endsection

@section('content')
    <div class="box box-primary">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

    <!-- form start -->
        {!! Form::open(['method'=>'POST', 'action'=>'Admin\\PaymentController@store', 'files' => true]) !!}
        <div class="box-body">
        	<div class="form-group">
                <label for="nameInput">Order Number</label>
                {!! Form::select('order_number', $order_list, $order_id, ['class'=>'form-control', 'placeholder' => 'Select the order number', 'required' => 'required']) !!}
            </div>
            <div class="form-group">
                <label for="nameInput">Transfered To</label>
                {!! Form::select('transfer_dest_account', ['BCA: 2063232327' => 'BCA: 2063232327', 'MANDIRI: 1220007586848' => 'MANDIRI: 1220007586848'], null, ['class'=>'form-control', 'placeholder' => 'Select the bank destination name', 'required' => 'required']) !!}
            </div>
            <div class="form-group">
                <label for="nameInput">Transfered Amount</label>
                {!! Form::text('transfer_amount', null, ['class'=>'form-control', 'placeholder' => 'Enter the amount transfered', 'required' => 'required']) !!}
            </div>
            <div class="form-group">
                <label for="nameInput">Date</label>
                {!! Form::text('transfer_date', null, ['class'=>'form-control datepicker', 'placeholder' => 'Pick the start date', 'required' => 'required']) !!}
            </div>
            <div class="form-group">
                <label for="nameInput">Time</label>
                <div class="small-6 columns">
                    <input type="text" name="transfer_time_minute" class="time" required placeholder="HH">
                </div>
                <div class="small-6 columns no-padding">
                    <input type="text" name="transfer_time_second" class="time" required placeholder="MM">
                </div>
            </div>
            <div class="form-group">
                <label for="nameInput">From Bank</label>
                {!! Form::text('transfer_sender_bank_name', null, ['class'=>'form-control', 'placeholder' => 'Bank Origin Name', 'required' => 'required']) !!}
            </div>
            <div class="form-group">
                <label for="nameInput">Account Number</label>
                {!! Form::text('transfer_sender_account_number', null, ['class'=>'form-control', 'placeholder' => 'Account Number', 'required' => 'required']) !!}
            </div>
            <div class="form-group">
                <label for="nameInput">Account Name</label>
                {!! Form::text('transfer_sender_account_name', null, ['class'=>'form-control', 'placeholder' => 'Account Name', 'required' => 'required']) !!}
            </div>
            <div class="form-group">
                <label for="nameInput">Note</label>
                {!! Form::text('transfer_note', null, ['class'=>'form-control', 'placeholder' => 'Note on this transaction' , 'required' => 'required']) !!}
            </div>
            <div class="form-group">
                <label for="nameInput">Payment Proof</label>
                <input type="file" name="file">
            </div>



        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            {{ csrf_field() }}
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
        {!! Form::close() !!}
    </div>
    <!-- /.box -->
@endsection

@section('myscript')
    <!-- DataTables -->
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <!-- bootstrap datepicker -->
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datepicker/bootstrap-datepicker.js"></script>
    <script>
        $("#example1").DataTable();
        //Date picker
        $('.datepicker').datepicker({
            autoclose: true
        });
    </script>
@endsection