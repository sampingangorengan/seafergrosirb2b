@extends('admin.layouts.master')

@section('title')
    Statistics
@endsection

@section('content-header')
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))

                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
        @endforeach
    </div>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Statistics
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"><a href="{{ url('admin;/statistics') }}">Statistics</a></li>
        </ol>
    </section>
    <br/>
@endsection

@section('content')
    <style >
    .style-content > div{
        background-color: #f5f5f5;
        height: 240px;
    }

    </style>
<div class="box">
    <div class="box-body">
        <div class="row" style="">
            <div class="col-md-12 style-content">
                <h3><b>Omzet</b></h3>
                <div class="col-xs-12 col-md-3">
                    <h4>Total Omzet</h4>
                    <h1><b>{{Money::display($data['omzet']['totalAll'])}}</b></h1>
                    <br>
                    <h4>Monthly Omzet</h4>
                    <h1><b>{{Money::display($data['omzet']['monthly']['current'])}}</b></h1>
                </div>
                <div class="col-xs-12 col-md-3">
                    <h4>Monthly Growth</h4>
                    <h1><b>{{$data['omzet']['monthly']['growth']}} %</b></h1>
                </div>


                <div class="col-xs-12 col-md-6">
                    <div id="bar-omzet" style="height: 200px;margin-top:20px;"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 style-content">
                <h3><b>Customers</b></h3>
                <div class="col-xs-12 col-md-3">
                    <h4>Total Customers</h4>
                    <h1><b></b></h1>
                    <br>
                    <h4>Monthly Customers</h4>
                    <h1><b></b></h1>
                </div>
                <div class="col-xs-12 col-md-3">
                    <h4>Monthly Growth</h4>
                    <h1><b></b></h1>
                </div>


                <div class="col-xs-12 col-md-6">
                    <div id="bar-customer" style="height: 200px;margin-top:20px;"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 style-content">
                <h3><b>Orders</b></h3>
                <div class="col-xs-12 col-md-3">
                    <h4>Total Orders</h4>
                    <h1><b>{{$data['order']['totalAll']}} </b>Orders</h1>
                    <br>
                    <h4>This Month Orders</h4>
                    <h1><b>{{$data['order']['monthly']['current']}} </b>Orders</h1>
                </div>
                <div class="col-xs-12 col-md-3">
                    <h4>Monthly Growth</h4>
                    <h1><b>{{$data['order']['monthly']['growth']}} %</b></h1>
                </div>
                <div class="col-xs-12 col-md-6">
                    <div id="bar-order" style="height: 200px;margin-top:20px;"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 style-content">
                <h3><b>Products</b></h3>
                <div class="col-xs-12 col-md-3">
                    <h4>Total Products</h4>
                    <h1><b>{{$data['products']['totalAll']}} </b>product</h1>
                    <br>
                    <h4>This Month Orders</h4>
                    <h1><b>{{$data['products']['monthly']['current']}} </b>product</h1>
                </div>
                <div class="col-xs-12 col-md-3">
                    <h4>Monthly Growth</h4>
                    <h1><b>{{$data['products']['monthly']['growth']}} %</b></h1>
                </div>
                <div class="col-xs-12 col-md-6">
                    <div id="bar-order" style="height: 200px;margin-top:20px;"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 style-content">
                <h3><b>Suppliers</b></h3>
                <div class="col-xs-12 col-md-3">
                    <h4>Total Products</h4>
                    <h1><b>{{$data['suppliers']['totalAll']}} </b>suppliers</h1>
                    <br>
                    <h4>This Month Orders</h4>
                    <h1><b>{{$data['suppliers']['monthly']['current']}} </b>suppliers</h1>
                </div>
                <div class="col-xs-12 col-md-3">
                    <h4>Monthly Growth</h4>
                    <h1><b>{{$data['suppliers']['monthly']['growth']}} %</b></h1>
                </div>
                <div class="col-xs-12 col-md-6">
                    <div id="bar-order" style="height: 200px;margin-top:20px;"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h3><b>Top 20</b></h3>
                <div class="col-xs-12 col-md-3" style="background-color: #f5f5f5; margin:10px;">
                    <h3><b>Top 20 Products</b></h3>
                    <table class="table table-bordered table-striped">
                        <thead>
                            <td>No</td>
                            <td>Name</td>
                        </thead>
                        @foreach($data['top20Products'] as $key=>$product)
                        <tr>
                            <td>{{++$key}}</td>
                            <td>{{$product->name}}</td>
                        </tr>
                    @endforeach
                    </table>
                </div>
                <div class="col-xs-12 col-md-3" style="background-color: #f5f5f5; margin:10px;">
                    <h3><b>Top 20 Customers</b></h3>
                    <table class="table table-bordered table-striped">
                        <thead>
                            <td>No</td>
                            <td>Name</td>
                        </thead>
                        @foreach($data['top20Customers'] as $key=>$product)
                        <tr>
                            <td>{{++$key}}</td>
                            <td>{{$product->company_name}}</td>
                        </tr>
                    @endforeach
                    </table>
                </div>
                <div class="col-xs-12 col-md-3" style="background-color: #f5f5f5; margin:10px;">
                    <h3><b>Top 20 Suppliers</b></h3>
                    <table class="table table-bordered table-striped">
                        <thead>
                            <td>No</td>
                            <td>Name</td>
                        </thead>
                        @foreach($data['top20Customers'] as $key=>$product)
                        <tr>
                            <td>{{++$key}}</td>
                            <td>{{$product->company_name}}</td>
                        </tr>
                    @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('myscript')
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/chartjs/Chart.min.js"></script>
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/flot/jquery.flot.min.js"></script>
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/flot/jquery.flot.categories.min.js"></script>
    <script type="text/javascript">
    var bar_type = ['omzet','customers','order']

    bar_type.map(function(type){

        if(type == "omzet") { <?php $type = "omzet";?> }
        else
        if(type == "order") { <?php $type = "order";?> }
        else
        if(type == "customers") { <?php $type = "customers";?> }


        var bar_data = {
          data: [["Previous Month", {{$data[$type]['monthly']['previous']}}],["Current Month",{{$data[$type]['monthly']['current']}}]],
          color: "#3c8dbc"
        };

        $.plot("#bar-"+type, [bar_data], {
            responsive:true,
            responsiveAnimationDuration:1,
          grid: {
            borderWidth: 1,
            borderColor: "#f3f3f3",
            tickColor: "#f3f3f3"
          },
          series: {
            bars: {
              show: true,
              barWidth: 0.8,
              align: "center"
            }
          },
          xaxis: {
            mode: "categories",
            tickLength: 0
          }
        });
    });

    </script>
@endsection
