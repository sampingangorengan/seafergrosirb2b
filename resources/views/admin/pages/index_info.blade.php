@extends('admin.layouts.master')

@section('title')
    Static Information | Control Room
@endsection

@section('content-header')
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))

                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
        @endforeach
    </div>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Static Information
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ url('admin/info') }}"> Static Information</a></li>
            <li class="active"> List Static Information</li>
        </ol>
        
        <br/>
    </section>
@endsection

@section('content')

    <div class="box">
        <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Type</th>
                    <th>Content</th>
                    <th>Link</th>
                    <th>Options</th>
                </tr>
                </thead>
                <tbody>
                @foreach($infos as $info)
                    <tr>
                        <td>{{ $info->type }}</td>
                        <td>{{ $info->content }}</td>
                        <td>{{ $info->link }}</td>
                        <td>
                            <a href="{{ url('admin/info/'.$info->id.'/edit') }}" class="pull-left"><button type="button" class="btn btn-primary btn-sm btn-flat">Edit</button>&nbsp;</a>
                        </td>
                    </tr>
                @endforeach

                </tbody>
                <tfoot>
                <tr>
                    <th>Type</th>
                    <th>Content</th>
                    <th>Link</th>
                    <th>Options</th>
                </tr>
                </tfoot>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->

    @include('admin.layouts.delete_modal')
@endsection

@section('myscript')
    <!-- DataTables -->
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script>
        $("#example1").DataTable();
        $(".delete-entity-index").on('click', function(e){
            var $form=$(this).closest('form');
            e.preventDefault();
            $('#confirm').modal({ backdrop: 'static', keyboard: false })
                .on('click', '#delete', function() {
                    $form.trigger('submit'); // submit the form
                });
        })
    </script>
@endsection