@extends('admin.layouts.master')

@section('title')
    Edit {{ ucwords($page->page_name) }} | Control Room
@endsection

@section('content-header')
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))

                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
        @endforeach
    </div>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Edit {{ ucwords($page->page_name) }} Page
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ url('admin/pages') }}">Page Management</a></li>
            <li class="active">Create New Headline</li>
        </ol>
    </section>
@endsection

@section('content')
    <div class="box box-primary">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_1" data-toggle="tab">General Information</a></li>
                <li><a href="#tab_2" data-toggle="tab">Description</a></li>
                <li><a href="#tab_3" data-toggle="tab">Images</a></li>
            </ul>
            <div class="tab-content">
                <!-- s: general information tab -->
                <div class="tab-pane active" id="tab_1">
                    <div class="box-body no-padding">
                        <table class="table table-striped">
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>Property</th>
                                <th>Value</th>
                            </tr>
                            <tr>
                                <td>1.</td>
                                <td>Name</td>
                                <td>
                                    {{ ucwords($page->page_name) }}
                                </td>
                            </tr>
                            <tr>
                                <td>2.</td>
                                <td>Link</td>
                                <td>
                                    {{ $page->page_link }}
                                </td>
                            </tr>
                            <tr>
                                <td>3.</td>
                                <td>Last Update</td>
                                <td>
                                    {{ LocalizedCarbon::parse($page->updated_at)->format('d F Y H:i:s') }}
                                </td>
                            </tr>
                            <tr>
                                <td>4.</td>
                                <td>Created At</td>
                                <td>
                                    {{ LocalizedCarbon::parse($page->created_at)->format('d F Y H:i:s') }}
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <!-- e: general information tab -->

                <!-- s: description tab -->
                <div class="tab-pane" id="tab_2">



                    @if ($page->descriptions->count() > 0)

                        @foreach ($page->descriptions as $description)
                        <div class="box box-solid">

                            <div class="box-body">
                                <div class="box-group" id="accordion">

                                    <div class="panel box box-success">
                                        <div class="box-header with-border">
                                            <h4 class="box-title pull-left" style="padding-top:1%">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse{{ ucwords($description->description_position) }}" class="collapsed" aria-expanded="false">
                                                    Position {{ $description->description_position }}
                                                </a>
                                            </h4>

                                            @if ($description->is_inline_css == 0)
                                            <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#myModal" data-position="{{ $description->description_position }}" data-descid="{{ $description->id }}">
                                                Edit
                                            </button>
                                            @else
                                            <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#myModal2" data-position="{{ $description->description_position }}" data-descid="{{ $description->id }}" data-content="{{ $description->description_content }}">
                                                Edit
                                            </button>
                                            @endif

                                        </div>
                                        <div id="collapse{{ ucwords($description->description_position) }}" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                            <div class="box-body" id="content{{ ucwords($description->description_position) }}">
                                                {!! $description->description_content !!}
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                        @endforeach

                    @else
                    <p>No description for this page</p>
                    @endif
                </div>
                <!-- e: description tab -->

                <!-- s: images tab -->
                <div class="tab-pane" id="tab_3">
                    @if ($page->images->count() > 0)

                        @foreach ($page->images as $images)
                        <div class="box box-solid">

                            <div class="box-body">
                                <div class="box-group" id="accordion">

                                    <div class="panel box box-success">
                                        <div class="box-header with-border">
                                            <h4 class="box-title pull-left" style="padding-top:1%">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseImage{{ ucwords($images->image_location) }}" class="collapsed" aria-expanded="false">
                                                    Position {{ $images->image_location }} | {{ $images->image_location_detail }}
                                                </a>
                                            </h4>

                                            <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#myModalEditImages" data-position="{{ $images->image_location }}" data-descid="{{ $images->id }}" data-posDetail="{{ $images->image_location_detail }}">
                                                Edit
                                            </button>

                                        </div>
                                        <div id="collapseImage{{ ucwords($images->image_location) }}" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                            <div class="box-body" id="content{{ ucwords($images->image_location) }}">
                                                <img src="{!! asset($images->image_name) !!}{!! '?'.date('YmdHis') !!}" style="max-width:100%"/>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <!-- /.box-body -->

                        </div>
                        <!-- /.box -->
                        @endforeach

                    @else
                    <p>No images for this page</p>
                    @endif
                </div>
                <!-- e: images tab -->

                <!-- s: link tab -->
                {{-- <div class="tab-pane" id="tab_4">
                    @if ($page->links->count() > 0)

                    @else
                    <p>No links for this page</p>
                    @endif
                </div> --}}
                <!-- e: link tab -->

            </div>
            <!-- /.tab-content -->
        </div>
    </div>
    <!-- /.box -->

    {{-- s: Edit Popup Modal | CK Editor --}}
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!! Form::open(array('url' => 'admin/static-description/', 'method' => 'patch')) !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                </div>

                <div class="modal-body">

                    <div class="box-body pad">
                            <input type="text" name="description_position" />
                            <textarea id="form_edit" name="description_content" rows="10" cols="80">

                            </textarea>

                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    {{-- e: Edit Popup Modal | CK Editor --}}

    {{-- s: Edit Popup Modal | Biasa --}}
    <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModal2Label">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                {!! Form::open(array('url' => 'admin/static-description/', 'method' => 'patch')) !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                </div>

                <div class="modal-body">

                    <div class="box-body pad">
                            <p>Position:</p>
                            <input type="text" name="description_position" />
                            <p>&nbsp;</p>

                            <p>Content:</p>
                            <textarea id="form_edit" name="description_content" rows="10" cols="80">

                            </textarea>

                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    {{-- e: Edit Popup Modal | Biasa --}}

    {{-- s: Edit Popup Modal | Image Upload --}}
    <div class="modal fade" id="myModalEditImages" tabindex="-1" role="dialog" aria-labelledby="myModalEditImagesLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">

                {!! Form::open(array('url' => 'admin/static-images/', 'method' => 'patch', 'files' => true)) !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                </div>

                <div class="modal-body">

                    <div class="box-body pad">
                        <div class="form-group">
                            <label for="nameInput">Position Description:</label>
                            <br/>
                            <input type="text" name="image_location_detail" style="width:80%"/>
                        </div>
                        <div class="form-group">
                            <label for="nameInput">Image to Replace:</label>
                            <input name="file" type="file" />
                        </div>

                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
                {!! Form::close() !!}

            </div>
        </div>
    </div>
      {{-- e: Edit Popup Modal | Image Upload --}}
@endsection

@section('myscript')
    <!-- DataTables -->
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script>
        $("#example1").DataTable();
    </script>
    <script src="{!! asset(null) !!}assets/plugin/ckeditor/ckeditor.js"></script>
    <script>
        var getDescriptionContent = function(position) {

            var content = $('#content'+position).html();

            $('.modal-body textarea').val(content)

            return content;
        }
        var getFormUri = function(id) {
            var default_uri = "{{ url('admin/static-description') }}"//$('#myModal form').attr('action')

            var uri = default_uri + '/' + id;
            return uri;
        }
        var getFormUriImages = function(id) {
            var default_uri = "{{ url('admin/static-images') }}"//$('#myModal form').attr('action')

            var uri = default_uri + '/' + id;
            return uri;
        }
        $('#myModal').on('show.bs.modal', function (event) {
            var fEd = CKEDITOR.instances['form_edit'];
            if(fEd) {
                for(name in CKEDITOR.instances)
                {
                    CKEDITOR.instances[name].destroy(true);
                }
            }
            var button = $(event.relatedTarget) // Button that triggered the modal
            var description_position = button.data('position') // Extract info from data-* attributes
            var description_id = button.data('descid')
            var content = getDescriptionContent(description_position)
            var action = getFormUri(description_id)

            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
            var modal = $(this)
            modal.find('.modal-title').text('Edit Description for Position ' + description_position)
            modal.find('.modal-body input').val(description_position)
            modal.find('form').attr('action', action)

            $(function () {
                // Replace the <textarea id="editor1"> with a CKEditor
                // instance, using default configuration.
                CKEDITOR.replace('form_edit');
            });
        })
        $('#myModal2').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            var description_position = button.data('position') // Extract info from data-* attributes
            var description_id = button.data('descid')
            var content = button.data('content');
            var action = getFormUri(description_id)

            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
            var modal = $(this)
            modal.find('.modal-title').text('Edit Description for Position ' + description_position)
            modal.find('.modal-body input').val(description_position)
            modal.find('.modal-body textarea').val(content)
            modal.find('form').attr('action', action)
        })

        $('#myModalEditImages').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget); // Button that triggered the modal
            var image_position = button.data('position'); // Extract info from data-* attributes
            var image_position_detail = button.data('posdetail');
            var image_id = button.data('descid')
            var action = getFormUriImages(image_id);

            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
            var modal = $(this)
            modal.find('.modal-title').text('Edit Description for Position ' + image_position)
            modal.find('.modal-body input[name="image_location_detail"]').val(image_position_detail)

            modal.find('form').attr('action', action)
        })
    </script>
@endsection
