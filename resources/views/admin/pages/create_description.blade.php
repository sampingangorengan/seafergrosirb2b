@extends('admin.layouts.master')

@section('title')
    Create New Description | Control Room
@endsection

@section('mycss')
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="{!! asset('bower_components/AdminLTE') !!}/plugins/datepicker/datepicker3.css">
@endsection

@section('content-header')
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))

                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
        @endforeach
    </div>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Create New Description
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ url('admin/pages') }}">List Page</a></li>
            <li class="active">Create New Description</li>
        </ol>
    </section>
@endsection

@section('content')
    <div class="box box-primary">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    <!-- form start -->
        {!! Form::open(['method'=>'POST', 'action'=>'Admin\\StaticDescriptionController@store']) !!}
        <div class="box-body">
            
            <div class="form-group">
                <label for="nameInput">Page</label>
                {!! Form::select('page_id', $pages, null, ['class'=>'form-control']) !!}
            </div>
            <div class="form-group">
                <label for="nameInput">Position</label>
                {!! Form::text('description_position', null, ['class'=>'form-control', 'required' => 'required']) !!}
            </div>
            <div class="form-group">
                <label for="nameInput">Content</label>
                {!! Form::textarea('description_content', null, ['id' => 'desc','class'=>'form-control', 'required' => 'required']) !!}
            </div>
            

        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            {{ csrf_field() }}
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
        {!! Form::close() !!}
    </div>
    <!-- /.box -->
@endsection

@section('myscript')
    <!-- DataTables -->
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <!-- bootstrap datepicker -->
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datepicker/bootstrap-datepicker.js"></script>
    <script src="{!! asset(null) !!}assets/plugin/ckeditor/ckeditor.js"></script>
    <script>
        $("#example1").DataTable();

        //Date picker
        $(function () {
            // Replace the <textarea id="editor1"> with a CKEditor
            // instance, using default configuration.
            CKEDITOR.replace('desc');
        });
    </script>
@endsection