@extends('admin.layouts.master')

@section('title')
    Edit Groceries | Control Room
@endsection

@section('mycss')
    <link rel="stylesheet" href="{{ asset('assets/css/select2.css') }}">
    <style>
        .bar {
            height: 18px;
            background: green;
        }
    </style>
@endsection

@section('content-header')
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))

                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
        @endforeach
    </div>

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Edit Groceries
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ url('admin/groceries') }}"> Groceries</a></li>
            <li class="active"> Edit Groceries</li>
        </ol>
        <br/>
    </section>
@endsection

@section('content')

    <!-- Custom Tabs -->
    <div class="nav-tabs-custom">

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_1" data-toggle="tab">General Information</a></li>
            <li><a href="#tab_6" data-toggle="tab">Grocery Video</a></li>
            <li><a href="#tab_5" data-toggle="tab">Grocery Image</a></li>
            <li><a href="#tab_7" data-toggle="tab">Grocery Poster</a></li>
            <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
        </ul>
        <div class="tab-content">

            <div class="tab-pane active" id="tab_1">

                <div class="box box-solid">
                    <div class="box-header with-border">
                        <h1 class="box-title">{{ $product->name }}</h1>
                    </div>
                    <!-- /.box-header -->

                    <!-- /.box-header -->
                    <div class="box-body no-padding">
                        <!-- form start -->
                        {!! Form::model($product,['method'=>'PATCH', 'action'=>['Admin\\ProductController@update', $product->id]]) !!}
                        <div class="box-body">
                            <div class="form-group">
                                <label for="nameInput">Name*</label>
                                {!! Form::text('name', null, ['class'=>'form-control', 'placeholder' => 'Grocery Name', 'required' => 'required']) !!}
                            </div>
                            <div class="form-group">
                                <label for="inputStock">Brand*</label>
                                {!! Form::select('brand', $brands, $product->brand->id, ['class'=>'form-control brand-box', 'placeholder' => 'Grocery Brand', 'required' => 'required']) !!}
                            </div>
                            <div class="form-group">
                                <label for="inputStock">SKU</label>
                                {!! Form::text('sku', null, ['class'=>'form-control', 'placeholder' => 'SKU']) !!}
                            </div>
                            <div class="form-group">
                                <label for="inputStock">Packing</label>
                                {!! Form::text('packing', null, ['class'=>'form-control', 'placeholder' => 'e.g. 12 pcs/ karton, 500 gr/btl, 5.7ml/pack']) !!}
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Category*</label>
                                <br/>
                                {!! Form::select('category[]', $categories, null, ['class'=>'form-control categorybox', 'required' => 'required', 'multiple' => true]) !!}
                            </div>
                            <div class="form-group">
                                <label for="inputStock">Country of Origin*</label>
                                {!! Form::select('country_id',$countries, $product->country->id, ['class'=>'form-control', 'id' => 'grocery_coo']) !!}
                            </div>
                            <div class="form-group">
                                <label for="inputStock">Sea Caught/Farmed</label>
                                {!! Form::select('fresh_sea_water',['0' => 'Non Sea Caught/Farmed', '1' => 'Sea Caught', '2' => 'Farmed'], $product->fresh_sea_water, ['class'=>'form-control', 'id' => 'grocery_fsw']) !!}
                            </div>
                            <div class="form-group">
                                <label for="inputStock">Supplier</label>
                                <br>
                                {!! Form::select('supplier[]', $suppliers, null, ['class'=>'form-control supplierbox', 'required' => 'required', 'multiple' => true]) !!}

                            </div>
                            <div class="form-group">
                                    <label for="inputStock">Cooking advices</label>
                                    {!! Form::textarea('cooking_advice', null, ['class'=>'form-control', 'placeholder' => 'Description', 'row' => '3']) !!}
                                </div>
                            <div class="form-group">
                                <label>Description*</label>
                                {!! Form::textarea('description', null, ['class'=>'form-control', 'placeholder' => 'Description', 'row' => '3']) !!}
                            </div>
                            <div class="form-group">
                                <label>Availability Notes</label>
                                {!! Form::textarea('availabity_note', null, ['class'=>'form-control', 'placeholder' => 'Availability Notes', 'row' => '3']) !!}
                            </div>
                            <div class="form-group">
                                <label>Nutrition Facts</label>
                                {!! Form::textarea('nutrients', null, ['class'=>'form-control', 'placeholder' => 'Nutrients', 'row' => '3']) !!}
                            </div>
                            <div class="form-group">
                                <label>Ingredients</label>
                                {!! Form::textarea('ingredients', null, ['class'=>'form-control', 'placeholder' => 'Ingredients', 'row' => '3']) !!}
                            </div>
                            <div class="form-group">
                                    <label for="inputStock">Youtube Link</label>
                                    {!! Form::text('youtube_link', $product->youtube_link, ['class'=>'form-control', 'placeholder' => 'Paste your youtube url here', 'id' => 'grocery_ytl', 'onkeyup' => 'saveValue(this);']) !!}
                                </div>
                            <div class="form-group">
                                <label for="inputStock">Quantity per package*</label>
                                {!! Form::text('value_per_package', null, ['class'=>'form-control', 'placeholder' => 'e.g, 1, 100, 1.5, 2.5  (must be number greater than zero)', 'required' => 'required']) !!}
                            </div>
                            <div class="form-group">
                                <label for="inputStock">Package metric*</label>
                                {!! Form::select('metric_id',$metrics, null, ['class'=>'form-control', 'placeholder' => 'Metric', 'required' => 'required']) !!}
                            </div>
                            <div class="form-group">
                                <label for="price">Price*</label>
                                {!! Form::text('price', null, ['class'=>'form-control', 'placeholder' => 'Price', 'required' => 'required']) !!}
                            </div>
                            <div class="form-group">
                                <label for="price">Range Price</label>
                                {!! Form::text('range_price', null, ['class'=>'form-control', 'placeholder' => 'Range price']) !!}
                            </div>
                            <div class="form-group">
                                <label for="nameInput">Expiry Date</label>
                                {!! Form::text('expiry_date', null, ['class'=>'form-control','id' => 'grocery_expiry', 'placeholder' => 'Pick the expiry date']) !!}
                            </div>
                            <div class="form-group">
                                <label for="inputStock">Stock*</label>
                                {!! Form::text('stock', null, ['class'=>'form-control', 'placeholder' => 'Stock', 'required' => 'required']) !!}
                            </div>
                            <div class="form-group">
                                <label for="inputStock">Minimum Stock Alert*</label>
                                {!! Form::text('minimum_stock_alert', null, ['class'=>'form-control', 'placeholder' => 'Stock', 'required' => 'required']) !!}
                            </div>
                            <div class="form-group">
                                <label for="inputStock">Tags</label>
                                {!! Form::select('tags[]',$tags, null, ['class'=>'form-control mytags', 'multiple' => true]) !!}
                            </div>
                            <div class="form-group">
                                <label for="inputStock">Discount</label>
                                {!! Form::select('discount_id',$discounts, null, ['class'=>'form-control', 'placeholder' => 'No Discount']) !!}
                            </div>
                            <div class="form-group">
                                <label for="nature">Product Nature*</label>
                                {!! Form::select('nature',['Ready to Cook' => 'Ready to Cook', 'Ready to Eat' => 'Ready to Eat'], null, ['class'=>'form-control', 'id' => 'grocery_nature', 'required' => 'required']) !!}
                            </div>
                            <div class="form-group">
                                <label for="state">Product State*</label>
                                {!! Form::select('state',['Frozen' => 'Frozen', 'Dry' => 'Dry', 'Fresh' => 'Fresh'], null, ['class'=>'form-control', 'id' => 'grocery_state', 'required' => 'required']) !!}

                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                <!-- /.box -->
            </div>

            <div class="tab-pane" id="tab_6">
                <!-- <input id="fileupload" type="file" name="files[]" data-url="server/php/" multiple accept=".mp4, .avi, .flv">
                <div id="progress">
                    <div class="bar" style="width: 0%;"></div>
                </div> -->
                <form id="uploadImage" action="{{url('admin/groceries/upload-advanced/'.$product->id)}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label>File Upload</label>
                        <input type="file" name="uploadFile" id="uploadFile" accept=".mp4, .avi, .flv" />
                    </div>
                    <div class="form-group">
                        <input type="submit" id="uploadSubmit" value="Upload" class="btn btn-info" />
                    </div>
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                    <div id="targetLayer" style="display:none;"></div>
                </form>
                @foreach($vid as $video => $file_name)
                <div class="info-box">
                    <video width="100%" height="auto" controls>
                      <source src="{!!asset('videogroceries/'.$file_name)!!}" type="video/mp4">
                    </video>
                </div>
                @endforeach
            </div>

            <div class="tab-pane" id="tab_5">
                @if($images->count() < 6)
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="fa fa-file-picture-o"></i></span>

                    <div class="info-box-content">
                        <h4 class="info-box-text">Add New Image</h4>
                        {!! Form::open(['method'=>'POST', 'url'=>'admin/groceries/storeImage/'.$product->id, 'files' => true]) !!}
                        <input type="file" name="file" class="file-upload-native apalah" accept="image/*" />
                        <button type="submit" class="btn btn-success btn-sm btn-flat">Upload</button>
                        {!! Form::close() !!}
                        <p style="color:red;margin-left:2px;">*)The best ratio for this image is 1:1, any size will be presented in a square box 400px x 400px</p>
                    </div>
                    <!-- /.info-box-content -->

                </div>
                <!-- /.info-box -->
                @else
                    <div class="info-box">
                        <span class="info-box-icon bg-green"><i class="fa fa-file-picture-o"></i></span>

                        <div class="info-box-content">
                            <h3 class="info-box-text">Whoops! Storage wise, we limits your images to three for one product.</h3>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                @endif

                <br/>

                @if($images->count() != 0)
                    @foreach($images as $image)
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-green">
                        <div class="inner">
                            <h3><img src="{!! asset('contents/'.$image->file_name) !!}" alt=""  style="height:160px;"></h3>
                        </div>
                        <div class="icon">
                            <br/>
                            <a href="{{ url('admin/groceries/deleteimage/'.$product->id.'/'.$image->id) }}"><i class="fa fa-trash-o"></i></a>
                        </div>
                        <a href="#" class="small-box-footer">
                            {{ $image->file_name }} <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div>
                </div>
                    @endforeach
                <div style="clear:both"></div>
                @endif
            </div>

            <div class="tab-pane" id="tab_7">
                @if($posters->count() < 6)
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="fa fa-file-picture-o"></i></span>

                    <div class="info-box-content">
                        <h4 class="info-box-text">Add New Poster</h4>
                        {!! Form::open(['method'=>'post', 'url'=>'admin/groceries/store-poster/'.$product->id, 'files' => true]) !!}
                        <input type="file" name="file" class="file-upload-native apalah" accept="image/*" />
                        <button type="submit" class="btn btn-success btn-sm btn-flat">Upload</button>
                        {!! Form::close() !!}
                        <p style="color:red;margin-left:2px;">*)The best ratio for poster image is 901px x 263px</p>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
                @else
                    <div class="info-box">
                        <span class="info-box-icon bg-green"><i class="fa fa-file-picture-o"></i></span>

                        <div class="info-box-content">
                            <h3 class="info-box-text">Whoops! Storage wise, we limits your images to three for one product.</h3>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                @endif

                <br/>

                @if($posters->count() != 0)
                    @foreach($posters as $poster)
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-green">
                        <div class="inner">
                            <h3><img src="{!! asset('groceries_poster/'.$poster->file) !!}" alt=""  style="height:160px;"></h3>
                        </div>
                        <div class="icon">
                            <br/>
                            <a href="{{ url('admin/groceries/delete-poster/'.$product->id.'/'.$poster->id) }}"><i class="fa fa-trash-o"></i></a>
                        </div>
                        <a href="#" class="small-box-footer">
                            {{ $poster->file_name }} <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div>
                </div>
                    @endforeach
                <div style="clear:both"></div>
                @endif
            </div>

        </div>
        <!-- /.tab-content -->
    </div>
    <!-- nav-tabs-custom -->


    {{-- <div class="box box-primary">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::model($product,['method'=>'PATCH', 'action'=>['Admin\\ProductController@update', $product->id]]) !!}
        <div class="box-body">
            <div class="form-group">
                <label for="nameInput">Name*</label>
                {!! Form::text('name', null, ['class'=>'form-control', 'placeholder' => 'Grocery Name', 'required' => 'required']) !!}
            </div>
            <div class="form-group">
                <label for="inputStock">Brand*</label>
                {!! Form::select('brand', $brands, $product->brand->id, ['class'=>'form-control brand-box', 'placeholder' => 'Grocery Brand', 'required' => 'required']) !!}
            </div>
            <div class="form-group">
                <label for="inputStock">SKU</label>
                {!! Form::text('sku', null, ['class'=>'form-control', 'placeholder' => 'SKU']) !!}
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Category*</label>
                <br/>
                {!! Form::select('category[]', $categories, null, ['class'=>'form-control categorybox', 'required' => 'required', 'multiple' => true]) !!}
            </div>
            <div class="form-group">
                <label for="inputStock">Country of Origin*</label>
                {!! Form::select('country_id',$countries, $product->country->id, ['class'=>'form-control', 'id' => 'grocery_coo']) !!}
            </div>
            <div class="form-group">
                <label for="inputStock">Sea Caught/Farmed</label>
                {!! Form::select('fresh_sea_water',['0' => 'Non Sea Caught/Farmed', '1' => 'Sea Caught', '2' => 'Farmed'], $product->fresh_sea_water, ['class'=>'form-control', 'id' => 'grocery_fsw']) !!}
            </div>
            <div class="form-group">
                    <label for="inputStock">Cooking advices</label>
                    {!! Form::textarea('cooking_advice', null, ['class'=>'form-control', 'placeholder' => 'Description', 'row' => '3']) !!}
                </div>
            <div class="form-group">
                <label>Description*</label>
                {!! Form::textarea('description', null, ['class'=>'form-control', 'placeholder' => 'Description', 'row' => '3']) !!}
            </div>
            <div class="form-group">
                <label>Nutrition Facts</label>
                {!! Form::textarea('nutrients', null, ['class'=>'form-control', 'placeholder' => 'Nutrients', 'row' => '3']) !!}
            </div>
            <div class="form-group">
                <label>Ingredients</label>
                {!! Form::textarea('ingredients', null, ['class'=>'form-control', 'placeholder' => 'Ingredients', 'row' => '3']) !!}
            </div>
            <div class="form-group">
                    <label for="inputStock">Youtube Link</label>
                    {!! Form::text('youtube_link', $product->youtube_link, ['class'=>'form-control', 'placeholder' => 'Paste your youtube url here', 'id' => 'grocery_ytl', 'onkeyup' => 'saveValue(this);']) !!}
                </div>
            <div class="form-group">
                <label for="inputStock">Quantity per package*</label>
                {!! Form::text('value_per_package', null, ['class'=>'form-control', 'placeholder' => 'e.g, 1, 100, 1.5, 2.5  (must be number greater than zero)', 'required' => 'required']) !!}
            </div>
            <div class="form-group">
                <label for="inputStock">Package metric*</label>
                {!! Form::select('metric_id',$metrics, null, ['class'=>'form-control', 'placeholder' => 'Metric', 'required' => 'required']) !!}
            </div>
            <div class="form-group">
                <label for="price">Price*</label>
                {!! Form::text('price', null, ['class'=>'form-control', 'placeholder' => 'Price', 'required' => 'required']) !!}
            </div>
            <div class="form-group">
                <label for="nameInput">Expiry Date</label>
                {!! Form::text('expiry_date', null, ['class'=>'form-control','id' => 'grocery_expiry', 'placeholder' => 'Pick the expiry date']) !!}
            </div>
            <div class="form-group">
                <label for="inputStock">Stock*</label>
                {!! Form::text('stock', null, ['class'=>'form-control', 'placeholder' => 'Stock', 'required' => 'required']) !!}
            </div>
            <div class="form-group">
                <label for="inputStock">Minimum Stock Alert*</label>
                {!! Form::text('minimum_stock_alert', null, ['class'=>'form-control', 'placeholder' => 'Stock', 'required' => 'required']) !!}
            </div>
            <div class="form-group">
                <label for="inputStock">Tags</label>
                {!! Form::select('tags[]',$tags, null, ['class'=>'form-control mytags', 'multiple' => true]) !!}
            </div>
            <div class="form-group">
                <label for="inputStock">Discount</label>
                {!! Form::select('discount_id',$discounts, null, ['class'=>'form-control', 'placeholder' => 'Discount']) !!}
            </div>
            <div class="form-group">
                <label for="nature">Product Nature*</label>
                {!! Form::select('nature',['Ready to Cook' => 'Ready to Cook', 'Ready to Eat' => 'Ready to Eat'], null, ['class'=>'form-control', 'id' => 'grocery_nature', 'required' => 'required']) !!}
            </div>
            <div class="form-group">
                <label for="state">Product State*</label>
                {!! Form::select('state',['Frozen' => 'Frozen', 'Dry' => 'Dry', 'Fresh' => 'Fresh'], null, ['class'=>'form-control', 'id' => 'grocery_state', 'required' => 'required']) !!}

            </div>
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
        {!! Form::close() !!}
    </div> --}}
    <!-- /.box -->
@endsection

@section('myscript')
    <!-- DataTables -->
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script src="{!! asset('assets/js/select2.full.js') !!}"></script>
    <script src="{!! asset('assets/js/jquery.form.js') !!}"></script>
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datepicker/bootstrap-datepicker.js"></script>

    <script src="{!! asset('assets/plugin/blueimp') !!}/js/vendor/jquery.ui.widget.js"></script>
    <script src="{!! asset('assets/plugin/blueimp') !!}/js/jquery.iframe-transport.js"></script>
    <script src="{!! asset('assets/plugin/blueimp') !!}/js/jquery.fileupload.js"></script>
    <script>
        var initList = function(values, section) {

            if(section == 'category') {
                $(".categorybox").val(values).trigger('change');
            } else if(section == 'brand') {
                $(".brand-box").val(values[0]).trigger('change');
            }else{
                $(".supplierbox").val(values).trigger('change');
            }

        }
        /*var loadCategory = function() {
            var li = $("#select2-category-ld-results");
            li.select2({
                placeholder: "Placeholder"
            });

            var unselected = li.find('option:not(:selected)');
            var selected = [];
            for (var i = 0; i < unselected.length; i++) {
                selected[i] = { id: unselected[i].value, text: unselected[i].text };
            }
            li.select2('data', selected);
        }*/
        $("#example1").DataTable();
        $(".mytags").select2({
            tags: true,
            placeholder: "Select or write your own",
        });
        $(".categorybox").select2();
        $(".supplierbox").select2();
        $('.datepicker').datepicker({
            autoclose: true
        });
        // expire date
        var dateToday = new Date();
        $('#grocery_expiry').datepicker({
            autoclose: true,
            startDate: dateToday
        });

        /*$(".categorybox").select2().select2("val", '1');*/
        $.getJSON('{{ url('admin/groceries/existing-groceries-item/'.$product->id.'/brand') }}', function(opts){
            initList(opts, 'brand');
        })

        $.getJSON('{{ url('admin/groceries/existing-groceries-item/'.$product->id.'/category') }}', function(opts){
            initList(opts, 'category');
        })

        $.getJSON('{{ url('admin/groceries/existing-groceries-item/'.$product->id.'/supplier') }}', function(opts){
            console.log(opts);
            initList(opts, 'supplier');
        })

        $(".delete-entity-index").on('click', function(e){
            var $form=$(this).closest('form');
            e.preventDefault();
            $('#confirm').modal({ backdrop: 'static', keyboard: false })
                .on('click', '#delete', function() {
                    $form.trigger('submit'); // submit the form
                });
        })

        $('#uploadImage').submit(function(event){
            if($('#uploadFile').val()){
                event.preventDefault();
                $('#loader-icon').show();
                $('#targetLayer').hide();
                $(this).ajaxSubmit({
                    target: '#targetLayer',
                    beforeSubmit:function(){
                        $('.progress-bar').width('50%');
                    },
                    uploadProgress: function(event, position, total, percentageComplete){
                        $('.progress-bar').animate({
                            width: percentageComplete + '%'
                        },{
                            duration: 1000
                        });
                    },
                    success:function(){
                        $('#loader-icon').hide();
                        $('#targetLayer').show();
                    },
                    resetForm: true
                });
            }
            return false;
        });
    </script>
@endsection
