@extends('admin.layouts.master')

@section('title')
    {{ $product->name }} | Control Room
@endsection

@section('content-header')
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))

                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
        @endforeach
    </div>
    @if($product->stock <= $product->minimum_stock_alert)
    <div class="flash-message">
        <p class="alert alert-danger"><br/>This grocery's stocks has reach MINIMUM STOCK ALERT!<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
    </div>
    @endif
@endsection

@section('mycss')
    <style>
        .dropdown-menu>li>form>a{
            display: block;
            padding: 3px 20px;
            clear: both;
            font-weight: 400;
            line-height: 1.42857143;
            color: #777;
        }
        .dropdown-menu>li>form>a:hover{
            background-color: red;
            color: #333;
            text-decoration-style: solid;
            font-weight: bold;
        }
    </style>
@endsection

@section('content')
    <!-- Custom Tabs -->
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_1" data-toggle="tab">General Information</a></li>
            <li><a href="#tab_2" data-toggle="tab">Description</a></li>
            <li><a href="#tab_9" data-toggle="tab">Availability Note</a></li>
            <li><a href="#tab_3" data-toggle="tab">Nutritions</a></li>
            <li><a href="#tab_4" data-toggle="tab">Ingredients</a></li>
            <li><a href="#tab_8" data-toggle="tab">Cooking Advice</a></li>
            <li><a href="#tab_6" data-toggle="tab">Grocery Video</a></li>
            <li><a href="#tab_5" data-toggle="tab">Grocery Image</a></li>
            <li><a href="#tab_7" data-toggle="tab">Grocery Poster</a></li>
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    Actions <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="{{ url('admin/groceries/'.$product->id.'/edit') }}">Edit</a></li>
                    <li role="presentation">
                        {!! Form::open(['method' => 'DELETE', 'action' => ['Admin\\ProductController@destroy', $product->id], 'class'=> "pull-left"]) !!}
                        <a role="menuitem" tabindex="-1" href="#" class="delete-entity-index">Delete</a>
                        {!! Form::close() !!}
                    </li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="{{ url('admin/groceries/create') }}">Create New</a></li>
                    <li role="presentation" class="divider"></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" target="_blank" href="{{ url('groceries/'.$product->id.'/'.$product->name) }}">View on frontend</a></li>
                </ul>
            </li>
            <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab_1">

                <div class="box box-solid">
                    <div class="box-header with-border">
                        <h1 class="box-title">{{ $product->name }}</h1>
                    </div>
                    <!-- /.box-header -->

                    <!-- /.box-header -->
                    <div class="box-body no-padding">
                        <table class="table table-striped">
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>Property</th>
                                <th>Value</th>
                            </tr>
                            <?php $i = 0;?>
                            <tr>
                                <td>{{$i += 1}}.</td>
                                <td>Name</td>
                                <td>
                                    {{ $product->name }}
                                </td>
                            </tr>
                            <tr>
                                <td>{{$i += 1}}.</td>
                                <td>Supplier</td>
                                <td>
                                    @if($suppliers = $product->suppliers)
                                        @foreach($suppliers as $supplier)
                                            {{$supplier->company_name}}

                                            @if($product->suppliers->last() != $supplier)
                                                ,
                                            @endif

                                        @endforeach
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td>{{$i += 1}}.</td>
                                <td>Brand</td>
                                <td>
                                    {{ $product->brand ? $product->brand->name : 'Not Available' }}
                                </td>
                            </tr>
                            <tr>
                                <td>{{$i += 1}}.</td>
                                <td>SKU</td>
                                <td>
                                    {{ $product->sku ? $product->sku : 'Not Available' }}
                                </td>
                            </tr>
                            <tr>
                                <td>{{$i += 1}}.</td>
                                <td>Expiry Date</td>
                                <td>
                                    {{ $product->expiry_date ? strtoupper(LocalizedCarbon::parse($product->expiry_date)->format('Y.M.d')) : 'Not Set' }}
                                </td>
                            </tr>
                            <tr>
                                <td>{{$i += 1}}.</td>
                                <td>Category</td>
                                <td>
                                    <ul>
                                        @foreach($product->category as $category)
                                        <li>{{ $category->parent->name.' > '.$category->name }}</li>
                                        @endforeach
                                    </ul>

                                </td>
                            </tr>
                            <tr>
                                <td>{{$i += 1}}.</td>
                                <td>Price</td>
                                <td>
                                    @if($product->range_price)
                                        {{Money::display($product->range_price)}} -
                                    @endif

                                    {{Money::display($product->price) }}
                                </td>
                            </tr>
                            <tr>
                                <td>{{$i += 1}}.</td>
                                <td>Selling package</td>
                                <td>
                                    per {{ $product->value_per_package ? $product->value_per_package : 'Not Available' }} {{ $product->metric ? $product->metric->abbreviation : 'Not Available' }}
                                </td>
                            </tr>
                            <tr>
                                <td>{{$i += 1}}.</td>
                                <td>Stock</td>
                                <td>
                                    {{ $product->stock }}<br/>
                                    Minimum alert: {{ $product->minimum_stock_alert }}
                                </td>
                            </tr>

                            <tr>
                                <td>{{$i += 1}}.</td>
                                <td>Sold</td>
                                <td>
                                    {{ $product->sold ? $product->sold : '0' }}
                                </td>
                            </tr>
                            <tr>
                                <td>{{$i += 1}}.</td>
                                <td>Tags</td>
                                <td>
                                    @foreach($product->tags as $tag)
                                    {{ $tag->name }},
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <td>{{$i += 1}}.</td>
                                <td>Barcode</td>
                                <td>
                                    {{ $product->barcode }}
                                </td>
                            </tr>
                            <tr>
                                <td>{{$i += 1}}.</td>
                                <td>Packing</td>
                                <td>
                                    {{ $product->packing }}
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <!-- /.tab-pane -->
            <div class="tab-pane" id="tab_2">
                {{ $product->description  }}
            </div>
            <!-- /.tab-pane -->
            <div class="tab-pane" id="tab_9">
                {{ $product->availabity_note  }}
            </div>
            <!-- /.tab-pane -->
            <div class="tab-pane" id="tab_6">
                @foreach($vid as $video => $file_name)
                <div class="info-box">
                    <video width="100%" height="auto" controls>
                      <source src="{!!asset('videogroceries/'.$file_name)!!}" type="video/mp4">
                    </video>
                </div>
                @endforeach
            </div>
            <!-- /.tab-pane -->
            <div class="tab-pane" id="tab_3">
                {{ $product->nutrients }}
            </div>
            <!-- /.tab-pane -->
            <div class="tab-pane" id="tab_4">
                {{ $product->ingredients }}
            </div>
            <div class="tab-pane" id="tab_5">
                @if($images->count() < 6)
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="fa fa-file-picture-o"></i></span>

                    <div class="info-box-content">
                        <h4 class="info-box-text">Add New Image</h4>
                        {!! Form::open(['method'=>'POST', 'url'=>'admin/groceries/storeImage/'.$product->id, 'files' => true]) !!}
                        <input type="file" name="file" class="file-upload-native apalah" accept="image/*" />
                        <button type="submit" class="btn btn-success btn-sm btn-flat">Upload</button>
                        {!! Form::close() !!}
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
                @else
                    <div class="info-box">
                        <span class="info-box-icon bg-green"><i class="fa fa-file-picture-o"></i></span>

                        <div class="info-box-content">
                            <h3 class="info-box-text">Whoops! Storage wise, we limits your images to three for one product.</h3>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                @endif

                <br/>

                @if($images->count() != 0)
                    @foreach($images as $image)
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-green">
                        <div class="inner">
                            <h3><img src="{!! asset('contents/'.$image->file_name) !!}" alt=""  style="height:160px;"></h3>
                        </div>
                        <div class="icon">
                            <br/>
                            <a href="{{ url('admin/groceries/deleteimage/'.$product->id.'/'.$image->id) }}"><i class="fa fa-trash-o"></i></a>
                        </div>
                        <a href="#" class="small-box-footer">
                            {{ $image->file_name }} <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div>
                </div>
                    @endforeach
                <div style="clear:both"></div>
                @endif
            </div>

            <div class="tab-pane" id="tab_7">
                @if($posters->count() < 6)
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="fa fa-file-picture-o"></i></span>

                    <div class="info-box-content">
                        <h4 class="info-box-text">Add New Poster</h4>
                        {!! Form::open(['method'=>'post', 'url'=>'admin/groceries/store-poster/'.$product->id, 'files' => true]) !!}
                        <input type="file" name="file" class="file-upload-native apalah" accept="image/*" />
                        <button type="submit" class="btn btn-success btn-sm btn-flat">Upload</button>
                        {!! Form::close() !!}
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
                @else
                    <div class="info-box">
                        <span class="info-box-icon bg-green"><i class="fa fa-file-picture-o"></i></span>

                        <div class="info-box-content">
                            <h3 class="info-box-text">Whoops! Storage wise, we limits your images to three for one product.</h3>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                @endif

                <br/>

                @if($posters->count() != 0)
                    @foreach($posters as $poster)
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-green">
                        <div class="inner">
                            <h3><img src="{!! asset('groceries_poster/'.$poster->file) !!}" alt=""  style="height:160px;"></h3>
                        </div>
                        <div class="icon">
                            <br/>
                            <a href="{{ url('admin/groceries/delete-poster/'.$product->id.'/'.$poster->id) }}"><i class="fa fa-trash-o"></i></a>
                        </div>
                        <a href="#" class="small-box-footer">
                            {{ $poster->file_name }} <i class="fa fa-arrow-circle-right"></i>
                        </a>
                    </div>
                </div>
                    @endforeach
                <div style="clear:both"></div>
                @endif
            </div>

            <!-- /.tab-pane -->
            <div class="tab-pane" id="tab_8">
                {{ $product->cooking_advice }}
            </div>
        </div>
        <!-- /.tab-content -->
    </div>
    <!-- nav-tabs-custom -->
    @include('admin.layouts.delete_modal')
@endsection

@section('myscript')
    <!-- DataTables -->
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script>
        $("#example1").DataTable();
        $(".delete-entity-index").on('click', function(e){
            var $form=$(this).closest('form');
            e.preventDefault();
            $('#confirm').modal({ backdrop: 'static', keyboard: false })
                .on('click', '#delete', function() {
                    $form.trigger('submit'); // submit the form
                });
        })
    </script>
@endsection
