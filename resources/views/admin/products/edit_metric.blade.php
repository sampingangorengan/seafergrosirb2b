@extends('admin.layouts.master')

@section('title')
    Edit Metric | Control Room
@endsection

@section('content-header')
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))

                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
        @endforeach
    </div>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Edit Metric
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ url('admin/groceries') }}">Groceries</a></li>
            <li><a href="{{ url('admin/metrics') }}">Metrics</a></li>
            <li class="active">Edit Metric</li>
        </ol>
    </section>
@endsection

@section('content')
    <div class="box box-primary">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    <!-- form start -->
        {!! Form::model($metric, ['method'=>'PATCH', 'action'=>['Admin\\MetricController@update', $metric->id]]) !!}
        <div class="box-body">
            <div class="form-group">
                <label for="nameInput">Metric Name</label>
                {!! Form::text('name', null, ['class'=>'form-control', 'placeholder' => 'e.g, Kilogram, Gram, Ikat, Bungkus', 'required' => 'required']) !!}
            </div>
            <div class="form-group">
                <label for="nameInput">Metric Abbreviation</label>
                {!! Form::text('abbreviation', null, ['class'=>'form-control', 'placeholder' => 'e.g, kg (for kilograms)/gr (gram)/bks (bungkus)', 'required' => 'required']) !!}
            </div>
            <div class="form-group">
                <label for="nameInput">Metric Status</label>
                {!! Form::select('is_active', array(1 => 'Active', 0 => 'Inactive'), null, ['class'=>'form-control', 'required' => 'required']) !!}
            </div>

        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            {{ csrf_field() }}
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
        {!! Form::close() !!}
    </div>
    <!-- /.box -->
@endsection

@section('myscript')
    <!-- DataTables -->
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script>
        $("#example1").DataTable();
    </script>
@endsection