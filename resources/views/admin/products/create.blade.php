@extends('admin.layouts.master')

@section('title')
    Create Groceries | Control Room
@endsection

@section('mycss')
    <link rel="stylesheet" href="{{ asset('assets/css/select2.css') }}">
@endsection

@section('content-header')
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))
                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
        @endforeach
    </div>

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Add New Groceries
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ url('admin/groceries') }}"> Groceries</a></li>
            <li class="active"> Add New Groceries</li>
        </ol>
        <br/>
    </section>
@endsection

@section('content')
    <div class="box box-primary">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::open(['method'=>'POST', 'action'=>'Admin\\ProductController@store', 'files' => true]) !!}
            <div class="box-body">
                <div class="form-group">
                    <label for="nameInput">Name*</label>
                    <input type="text" name="name" class="form-control" placeholder="Grocery Name" id="grocery_name" onkeyup="saveValue(this);" required>
                </div>
                <div class="form-group">
                    <label for="inputStock">Brand*</label>
                    <select name="brand" class="form-control" placeholder="Grocery Brand" id="grocery_brand" required>
                        <option value="">Grocery Brand</option>
                        @foreach($brands as $b)
                            <option value="{{ $b->id }}">{{ $b->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="inputStock">SKU</label>
                    <input type="text" name="sku" class="form-control" placeholder="Grocery SKU" id="grocery_sku" onkeyup="saveValue(this);">
                </div>
                <div class="form-group">
                    <label for="inputStock">Barcode</label>
                    <input type="text" name="barcode" class="form-control" placeholder="Grocery barcode" id="grocery_barcode" onkeyup="saveValue(this);">
                </div>
                <div class="form-group">
                    <label for="inputStock">Packing</label>
                    <input type="text" name="packing" class="form-control" placeholder="e.g. 12 pcs/ karton, 500 gr/btl, 5.7ml/pack" id="grocery_packing" onkeyup="saveValue(this);">
                </div>
                <div class="form-group">
                    <label>Category*</label>
                    {!! Form::select('category[]', $categories, null, ['class'=>'form-control categorybox', 'required' => 'required', 'multiple' => true]) !!}
                    {{-- @foreach($categories as $c)
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="category[]" id="" value="{{ $c->id }}"> {{ $c->name }}
                        </label>
                    </div>
                    @endforeach --}}
                </div>
                {{-- <div class="form-group" id="sub-category">
                    <label>Sub Category*</label>
                </div> --}}
                <div class="form-group">
                    <label for="inputStock">Country of Origin*</label>
                    <select name="coo" class="form-control" placeholder="Grocery Country of Origin" id="grocery_coo" required>
                        <option value="">Grocery Country of Origin</option>
                        @foreach($country as $ctr)
                            <option value="{{ $ctr->id }}"
                                @if($ctr->id == 107)
                                selected
                                @endif
                            >{{ $ctr->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="inputStock">Sea Caught/Farmed</label>
                    <select name="fresh_sea_water" class="form-control" placeholder="Grocery Sea Caught/Farmed" id="grocery_fsw" required>
                        <option value="0">Non Sea Caught/Farmed</option>
                        <option value="1">Sea Caught</option>
                        <option value="2">Farmed</option>
                    <select>
                </div>
                <div class="form-group">
                    <label for="inputStock">Supplier</label>
                    <br>
                    {!! Form::select('supplier[]', $suppliers, null, ['class'=>'form-control supplierbox', 'required' => 'required', 'multiple' => true]) !!}

                </div>
                <div class="form-group">
                    <label for="inputStock">Cooking advices</label>
                    <textarea name="cooking_advice" class="form-control" placholder="Grocery Cooking Advice" id="grocery_cooking_advice" rows="3" onkeyup="saveValue(this);"></textarea>
                </div>
                <div class="form-group">
                    <label>Description*</label>
                    <textarea name="description" class="form-control" placholder="Description" id="grocery_description" rows="3" onkeyup="saveValue(this);"></textarea>
                </div>
                <div class="form-group">
                    <label>Availability Notes</label>
                    <textarea name="availabity_note" class="form-control" placholder="Description" id="availabity_note" rows="3" onkeyup="saveValue(this);"></textarea>
                </div>
                <div class="form-group">
                    <label>Nutrition Facts</label>
                    <textarea name="nutrients" class="form-control" placholder="Nutrients" id="grocery_nutrients" rows="3" onkeyup="saveValue(this);"></textarea>
                </div>
                <div class="form-group">
                    <label>Ingredients</label>
                    <textarea name="ingredients" class="form-control" placholder="Ingredients" id="grocery_ingredients" rows="3" onkeyup="saveValue(this);"></textarea>
                </div>
                <div class="form-group">
                    <label for="inputStock">Youtube Link</label>
                    <input type="text" name="youtube_link" class="form-control" placeholder="Paste your youtube url here" id="grocery_ytl" onkeyup="saveValue(this);">
                </div>
                <div class="form-group">
                    <label for="inputStock">Quantity per package*</label>
                    <input type="text" name="qpp" class="form-control" placeholder="e.g, 1, 100, 1.5, 2.5  (must be number greater than zero)" id="grocery_qpp" onkeyup="saveValue(this);" required>
                </div>
                <div class="form-group">
                    <label for="inputStock">Package metric*</label>
                    <select name="metric" class="form-control" placeholder="Grocery Package metric" id="grocery_metric" required>
                        <option value="">Grocery Package metric</option>
                        @foreach($metrics as $mtrc)
                            <option value="{{ $mtrc->id }}">{{ $mtrc->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="price">Price*</label>
                    <input type="text" name="price" class="form-control" placeholder="Price" id="grocery_price" onkeyup="saveValue(this);" required>
                </div>
                <div class="form-group">
                    <label for="price">Range Price (Batas bawah)</label>
                    <input type="text" name="range_price" class="form-control" placeholder="Range price" id="grocery_range_price" onkeyup="saveValue(this);" required>
                </div>
                <div class="form-group">
                    <label for="nameInput">Expiry Date</label>
                    <input type="text" name="expiry_date" class="form-control" placeholder="Pick the expiry date" id="grocery_expiry" onkeyup="saveValue(this);">
                </div>
                <div class="form-group">
                    <label for="inputStock">Stock*</label>
                    <input type="text" name="stock" class="form-control" placeholder="Stock" id="grocery_stock" onkeyup="saveValue(this);" required>
                </div>
                <div class="form-group">
                    <label for="inputStock">Minimum Stock Alert*</label>
                    <input type="text" name="minimum_stock_alert" class="form-control" placeholder="Grocery Minimum Stock" id="grocery_minimum_stock" onkeyup="saveValue(this);" required>
                </div>
                <div class="form-group">
                    <label for="inputStock">Tags</label>
                    <select name="tags[]" id="grocery-tags" class="form-control mytags">
                        @foreach($tags as $tgs)
                            <option value="{{ $tgs->id }}">{{ $tgs->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="inputStock">Running Discount</label>
                    <select name="discount" id="grocery-tags" class="form-control" id="grocery_discount">
                        <option value="">Select Discounts</option>
                        @foreach($discounts as $dsc)
                            <option value="{{ $dsc->id }}">{{ $dsc->title }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="nature">Product Nature*</label>
                    <select name="nature" class="form-control" placeholder="Grocery Sea Caught/Farmed" id="grocery_nature" required>
                        <option value="Ready to Cook">Ready to Cook</option>
                        <option value="Ready to Eat">Ready to Eat</option>
                    <select>
                </div>
                <div class="form-group">
                    <label for="state">Product State*</label>
                    <select name="state" class="form-control" placeholder="Grocery State" id="grocery_nature" required>
                        <option value="Frozen">Frozen</option>
                        <option value="Dry">Dry</option>
                        <option value="Fresh">Fresh</option>
                    <select>
                </div>
                <div class="form-group">
                    <label for="inputStock">Product Image*</label>
                    <p style="color:red;margin-left:2px;">*)The best ratio for this image is 1:1, any size will be presented in a square box 400px x 400px</p>
                    {!! Form::file('image_one', null,['class'=>'form-control', 'placeholder' => 'Image', 'id' => 'image_one']) !!}
                    {!! Form::file('image_two', null,['class'=>'form-control', 'placeholder' => 'Image', 'id' => 'image_two']) !!}
                    {!! Form::file('image_three', null,['class'=>'form-control', 'placeholder' => 'Image', 'id' => 'image_three']) !!}
                    {!! Form::file('image_four', null,['class'=>'form-control', 'placeholder' => 'Image', 'id' => 'image_four']) !!}
                    {!! Form::file('image_five', null,['class'=>'form-control', 'placeholder' => 'Image', 'id' => 'image_five']) !!}
                </div>
                <div class="form-group">
                    <label for="inputStock">Product Poster</label>
                    <p style="color:red;margin-left:2px;">*)The best ratio for poster image is 901px x 263px</p>
                    {!! Form::file('poster_one', null,['class'=>'form-control', 'placeholder' => 'Poster', 'id' => 'poster_one']) !!}
                    {!! Form::file('poster_two', null,['class'=>'form-control', 'placeholder' => 'Poster', 'id' => 'poster_two']) !!}
                    {!! Form::file('poster_three', null,['class'=>'form-control', 'placeholder' => 'Poster', 'id' => 'poster_three']) !!}
                    {!! Form::file('poster_four', null,['class'=>'form-control', 'placeholder' => 'Poster', 'id' => 'poster_four']) !!}
                    {!! Form::file('poster_five', null,['class'=>'form-control', 'placeholder' => 'Poster', 'id' => 'poster_five']) !!}
                    {!! Form::file('poster_six', null,['class'=>'form-control', 'placeholder' => 'Poster', 'id' => 'poster_six']) !!}
                    {!! Form::file('poster_seven', null,['class'=>'form-control', 'placeholder' => 'Poster', 'id' => 'poster_seven']) !!}
                </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        {!! Form::close() !!}
    </div>
    <!-- /.box -->
@endsection

@section('myscript')
    <!-- DataTables -->
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script src="{!! asset('assets/js/select2.full.js') !!}"></script>
    <script src="{!! asset('assets/js/accounting.min.js') !!}"></script>
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datepicker/bootstrap-datepicker.js"></script>
    <script>
        var initList = function(values, section) {

            if(section == 'category') {
                $(".categorybox").val(values).trigger('change');
            } else if(section == 'brand') {
                $(".brand-box").val(values[0]).trigger('change');
            }else{
                $(".supplierbox").val(values).trigger('change');
            }

        }
        var getPrice = function() {
            var dirty_price = $('#grocery_price').val();
            dirty_price = dirty_price.replace('.00','');
            dirty_price = dirty_price.replace(',','');
            return dirty_price;
        }
        var drawPrice = function(value) {
            $('#grocery_price').val(value);
            return 'ok';
        }
        $("#example1").DataTable();
        $(".mytags").select2({
            tags: true,
            placeholder: "Select or write your own"
        });
        $(".categorybox").select2();
        $(".supplierbox").select2();
        $('.datepicker').datepicker({
            autoclose: true,
            /*format:"yyyy.M.dd"*/
        });
        // expire date
        var dateToday = new Date();
        $('#grocery_expiry').datepicker({
            autoclose: true,
            startDate: dateToday
        });

        $('#grocery_price').keyup(function(event) {
            result = accounting.formatMoney(getPrice(), "", 0);
            drawPrice(result);
        });

        $('#grocery_price').keypress(function(event){
            if(event.which != 8 && isNaN(String.fromCharCode(event.which))){
                event.preventDefault(); //stop character from entering input
            }
        });

        @if (count($errors) <= 0)
        document.getElementById("grocery_name").value = getSavedValue("grocery_name");    // set the value to this input
        document.getElementById("grocery_sku").value = getSavedValue("grocery_sku");
        document.getElementById("grocery_range_price").value = getSavedValue("grocery_range_price");
        document.getElementById("grocery_barcode").value = getSavedValue("grocery_barcode");
        document.getElementById("grocery_description").value = getSavedValue("grocery_description");
        document.getElementById("grocery_nutrient").value = getSavedValue("grocery_nutrient");
        document.getElementById("grocery_ingredient").value = getSavedValue("grocery_ingredient");
        document.getElementById("grocery_qpp").value = getSavedValue("grocery_qpp");
        document.getElementById("grocery_cooking_advice").value = getSavedValue("grocery_cooking_advice");
        document.getElementById("grocery_ytl").value = getSavedValue("grocery_ytl");
        document.getElementById("grocery_ytl").value = getSavedValue("grocery_fsw");
        document.getElementById("grocery_ytl").value = getSavedValue("grocery_coo");
        document.getElementById("grocery_price").value = getSavedValue("grocery_price");
        document.getElementById("grocery_expiry").value = getSavedValue("grocery_expiry");
        document.getElementById("grocery_stock").value = getSavedValue("grocery_stock");
        document.getElementById("grocery_minimum_stock").value = getSavedValue("grocery_minimum_stock");
        @else
        saveValue(document.getElementById('grocery_name'));
        saveValue(document.getElementById('grocery_sku'));
        saveValue(document.getElementById('grocery_barcode'));
        saveValue(document.getElementById('grocery_description'));
        saveValue(document.getElementById('grocery_nutrient'));
        saveValue(document.getElementById('grocery_ingredient'));
        saveValue(document.getElementById('grocery_qpp'));
        saveValue(document.getElementById('grocery_cooking_advice'));
        saveValue(document.getElementById('grocery_ytl'));
        saveValue(document.getElementById('grocery_fsw'));
        saveValue(document.getElementById('grocery_coo'));
        saveValue(document.getElementById('grocery_price'));
        saveValue(document.getElementById('grocery_expiry'));
        saveValue(document.getElementById('grocery_stock'));
        saveValue(document.getElementById('grocery_minimum_stock'));
        @endif
        /* Here you can add more inputs to set value. if it's saved */

        //Save the value function - save it to localStorage as (ID, VALUE)
        function saveValue(e){
            var id = e.id;  // get the sender's id to save it .
            var val = e.value; // get the value.
            localStorage.setItem(id, val);// Every time user writing something, the localStorage's value will override .
        }

        //get the saved value function - return the value of "v" from localStorage.
        function getSavedValue(v){
            if (localStorage.getItem(v) === null) {
                return "";// You can change this to your default value.
            }
            return localStorage.getItem(v);
        }

        $('form').on('submit', function(e){
            e.preventDefault();

            var price = getPrice();
            $('#grocery_price').val(price);

            localStorage.clear();

            $(this).unbind('submit').submit();
        });
    </script>
@endsection
