@extends('admin.layouts.master')

@section('title')
    Promos | Control Room
@endsection

@section('content-header')
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))

                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
        @endforeach
    </div>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Promo Detail
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ url('admin/promos') }}"> Promos</a></li>
            <li class="active"> Promo Detail</li>
        </ol>
    </section>
@endsection

@section('content')
    <!-- Custom Tabs -->
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_1" data-toggle="tab">General Information</a></li>


            {{--<li><a href="#tab_5" data-toggle="tab">Upload Image</a></li>--}}
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    Actions <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="{{ url('admin/promos/'.$promo->id.'/edit')  }}">Edit</a></li>

                    <li role="presentation">
                        {!! Form::open(['method' => 'DELETE', 'action' => ['Admin\\PromoController@destroy', $promo->id], 'class'=> "pull-left"]) !!}
                        <a role="menuitem" tabindex="-1" href="#" class="delete-entity-index">Delete</a>
                        {!! Form::close() !!}
                    </li>
                    {{--<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Create New</a></li>--}}
                    {{--<li role="presentation" class="divider"></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" target="_blank" href="{{ url('groceries/'.$product->id.'/'.$product->name) }}">View on frontend</a></li>--}}
                </ul>
            </li>
            <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab_1">

                <div class="box box-solid">
                    <div class="box-header with-border">
                        <h1 class="box-title">Promo {{ $promo->title }}</h1>
                    </div>

                <!-- /.box-header -->
                    <div class="box-body no-padding">
                        <table class="table table-striped">
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>Property</th>
                                <th>Value</th>
                            </tr>
                            <tr>
                                <td>1.</td>
                                <td>Title</td>
                                <td>
                                    {{ $promo->title }}
                                </td>
                            </tr>
                            <tr>
                                <td>2.</td>
                                <td>Code</td>
                                <td>
                                    {{ $promo->code }}
                                </td>
                            </tr>
                            <tr>
                                <td>3.</td>
                                <td>Description</td>
                                <td>
                                    {{ $promo->description }}
                                </td>
                            </tr>
                            <tr>
                                <td>4.</td>
                                <td>Promo type</td>
                                <td>
                                    {{ $promo->promo_type }}
                                </td>
                            </tr>
                            <tr>
                                <td>5.</td>
                                <td>Discount value</td>
                                <td>
                                    {{ $promo->discount_value }}
                                </td>
                            </tr>
                            <tr>
                                <td>6.</td>
                                <td>Start Date</td>
                                <td>
                                    {{ LocalizedCarbon::parse($promo->start_date)->format('d F Y') }}
                                </td>
                            </tr>
                            <tr>
                                <td>7.</td>
                                <td>End Date</td>
                                <td>
                                    {{ LocalizedCarbon::parse($promo->end_date)->format('d F Y') }}
                                </td>
                            </tr>
                            <tr>
                                <td>8.</td>
                                <td>Quota</td>
                                <td>
                                    {{ $promo->quota }}
                                </td>
                            </tr>
                            <tr>
                                <td>9.</td>
                                <td>Quota Used</td>
                                <td>
                                    {{ $promo->quota_used }}
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <!-- /.tab-pane -->

        </div>
        <!-- /.tab-content -->
    </div>
    <!-- nav-tabs-custom -->
    @include('admin.layouts.delete_modal')
@endsection

@section('myscript')
    <!-- DataTables -->
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script>
        $("#example1").DataTable();
        $(".delete-entity-index").on('click', function(e){
            var $form=$(this).closest('form');
            e.preventDefault();
            $('#confirm').modal({ backdrop: 'static', keyboard: false })
                .on('click', '#delete', function() {
                    $form.trigger('submit'); // submit the form
                });
        })
    </script>
@endsection