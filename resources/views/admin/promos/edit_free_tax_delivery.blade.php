@extends('admin.layouts.master')

@section('title')
    Edit Free Delivery Settings | Control Room
@endsection

@section('mycss')
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="{!! asset('bower_components/AdminLTE') !!}/plugins/datepicker/datepicker3.css">
@endsection

@section('content-header')
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))

                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
        @endforeach
    </div>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Edit Free Delivery Settings
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ url('admin/freebies') }}">Free Delivery</a></li>
            <li class="active">Edit Free Delivery Settings</li>
        </ol>
    </section>
@endsection

@section('content')
    <div class="box box-primary">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    <!-- form start -->
        {!! Form::model($freebie,['method'=>'PATCH', 'action'=>['Admin\\FreeTaxDeliveryController@update', $freebie->id]]) !!}
        <div class="box-body">
            <div class="form-group">
                <label for="nameInput">On/Off</label>
                {!! Form::select('is_active', array(1 => 'Active/ON', 0 => 'Inactive/OFF'), null, ['class'=>'form-control']) !!}
            </div>
            <div class="form-group">
                <label for="nameInput">Minimum Value</label>
                {!! Form::text('minimum_spending', null, ['class'=>'form-control', 'placeholder' => 'Minimum value must be integer', 'required' => 'required']) !!}
            </div>
            <div class="form-group">
                <label for="nameInput">Quota</label>
                {!! Form::text('quota', null, ['class'=>'form-control', 'placeholder' => 'Quota must be integer', 'required' => 'required']) !!}
            </div>
            
            <div class="form-group">
                <label for="nameInput">Promo Start Date</label>
                {!! Form::text('start_date', null, ['id' => 'startDate', 'class'=>'form-control datepicker', 'placeholder' => 'Pick the start date', 'required' => 'required']) !!}
            </div>
            <div class="form-group">
                <label for="nameInput">Promo End Date</label>
                {!! Form::text('end_date', null, ['id' => 'endDate','class'=>'form-control datepicker', 'placeholder' => 'Pick the end date', 'required' => 'required']) !!}
            </div>


        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            {{ csrf_field() }}
            <button type="submit" class="btn btn-primary">Save Settings</button>
        </div>
        {!! Form::close() !!}
    </div>
    <!-- /.box -->
@endsection

@section('myscript')
    <!-- DataTables -->
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <!-- bootstrap datepicker -->
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datepicker/bootstrap-datepicker.js"></script>
    <script>
        $("#example1").DataTable();
        
        var dateToday = new Date();

        $('#startDate').datepicker({
            format: 'yyyy-mm-dd 00:00:00',
            autoclose: true,
            startDate : dateToday,
        // update "toDate" defaults whenever "fromDate" changes
        }).on('changeDate', function(){
            // set the "toDate" start to not be later than "fromDate" ends:
            $('#endDate').datepicker('setStartDate', new Date($(this).val()));
        }); 

        $('#endDate').datepicker({
            format: 'yyyy-mm-dd 23:59:59',
            startDate : dateToday,
            autoclose: true,
        // update "fromDate" defaults whenever "toDate" changes
        }).on('changeDate', function(){
            // set the "fromDate" end to not be later than "toDate" starts:
            $('#startDate').datepicker('setEndDate', new Date($(this).val()));
        });
    </script>
@endsection