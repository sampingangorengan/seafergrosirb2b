@extends('admin.layouts.master')

@section('title')
    Users | Control Room
@endsection

@section('content-header')
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))

                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
        @endforeach
    </div>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            {{ $user->name }}'s Detail
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ url('admin/complaints') }}"> Claims</a></li>
            <li class="active"> User Detail</li>
        </ol>
    </section>
@endsection

@section('content')

    <!-- Custom Tabs -->
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_1" data-toggle="tab">General Information</a></li>
            <li><a href="#tab_2" data-toggle="tab">Payment Options</a></li>
            <li><a href="#tab_3" data-toggle="tab">Email Preference</a></li>
            {{-- <li><a href="#dropdown" data-toggle="tab">Order List</a></li> --}}

            {{--<li><a href="#tab_5" data-toggle="tab">Upload Image</a></li>--}}
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    Actions <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="{{ url('admin/users/'.$user->id.'/edit') }}"><button type="button" class="btn-sm btn-primary">Edit</button></a></li>


                    <li role="presentation">
                        {!! Form::open(['method' => 'DELETE', 'action' => ['Admin\\UserController@destroy', $user->id], 'class'=> ""]) !!}
                        <button type="submit" class="btn btn-danger btn-sm btn-flat delete-entity-index" style="margin:0 auto;">Delete</button>
                        {!! Form::close() !!}
                    </li>
                    {{--<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Create New</a></li>--}}
                    {{--<li role="presentation" class="divider"></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" target="_blank" href="{{ url('groceries/'.$product->id.'/'.$product->name) }}">View on frontend</a></li>--}}
                </ul>
            </li>
            <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab_1">

                <div class="box box-solid">
                    <div class="box-header with-border">
                        <h1 class="box-title">User Detail:</h1>
                    </div>
                    <!-- /.box-header -->
                    @if($user->photo)
                        <div class="box-body">
                            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                <ol class="carousel-indicators">

                                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>

                                </ol>
                                <div class="carousel-inner">
                                        <div class="item active">
                                            <img src="{{ $user->photo }}" alt="{{ $user->item_name }}">
                                        </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    @endif

                <!-- /.box-header -->
                    <div class="box-body no-padding">
                        <table class="table table-striped">
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>Property</th>
                                <th>Value</th>
                            </tr>
                            <tr>
                                <td>1.</td>
                                <td>Company Name</td>
                                <td>
                                    {{ $user->company_name }}
                                </td>
                            </tr>
                            <tr>
                                <td>2.</td>
                                <td>Brand</td>
                                <td>
                                    {{ $user->brand_name }}
                                </td>
                            </tr>
                            <tr>
                                <td>3.</td>
                                <td>Company type</td>
                                <td>
                                    {{ $user->business_entity }}
                                </td>
                            </tr>
                            <tr>
                                <td>4.</td>
                                <td>Address</td>
                                <td>
                                    Billing Address: {{$user->billing_address}}
                                    <br>
                                    @if($user->shipping_address != "" || $user->shipping_address != null)
                                        Shipping: {{$user->shipping_address}}
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td>5.</td>
                                <td>Company Phone</td>
                                <td>
                                    {{ $user->company_phone }}
                                </td>
                            </tr>
                            <tr>
                                <td>6.</td>
                                <td>Company Email</td>
                                <td>
                                    {{ $user->company_email }}
                                </td>
                            </tr>
                            <tr>
                                <td>7.</td>
                                <td>Company Website</td>
                                <td>
                                    {{ $user->website }}
                                </td>
                            </tr>
                            <tr>
                                <td>7.</td>
                                <td>Company Category</td>
                                <td>
                                    {{ $user->category }}
                                </td>
                            </tr>
                            <tr>
                                <td>8.</td>
                                <td>Pic Name</td>
                                <td>
                                    {{ $user->name }}
                                </td>
                            </tr>
                            <tr>
                                <td>9.</td>
                                <td>Pic Email</td>
                                <td>
                                    {{ $user->email }}
                                </td>
                            </tr>
                            <tr>
                                <td>10.</td>
                                <td>Pic Phone</td>
                                <td>
                                    {{ $user->phone }}
                                </td>
                            </tr>
                            <tr>
                                <td>4.</td>
                                <td>Role</td>
                                <td>
                                    @if($user->role == 1)
                                    Administrator
                                    @else
                                    User
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td>5.</td>
                                <td>Status</td>
                                <td>
                                    @if($user->is_active == 1)
                                    Email confirmed
                                    @else
                                    Email not confirmed
                                    @endif
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <div class="tab-pane" id="tab_2">
                <div class="box box-solid">
                    <div class="box-header with-border">
                        <h1 class="box-title">Payment </h1>
                    </div>
                    <form class="" action="{{url('admin/user/add-payment-option')}}" method="post">
                        <div class="box-body no-padding">
                            <div class="row">
                                <? $user_payment_option = json_decode($user->payment_method) ?? []; ?>
                                @foreach(["cod","transfer","tempo"] as $paymentOPT)
                                    <?
                                    $checked = "";
                                    if(in_array($paymentOPT,$user_payment_option)){
                                        $checked = "checked";
                                    }
                                    ?>
                                    <div class="col-md-4">
                                        <div class="box">
                                            <input class="checkbox" type="checkbox" name="payment_method[]" value="{{$paymentOPT}}" {{$checked}}>
                                            <label for="checkbox" class="agree">{{$paymentOPT}}</label>
                                        </div>
                                    </div>
                                @endforeach
                                <input type="hidden" name="user_id" value="{{$user->id}}"/>

                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary btn-sm" id="">Save</button>
                                </div>

                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.box -->
            </div>
            <div class="tab-pane" id="tab_3">

                <div class="box box-solid">
                    <div class="box-header with-border">
                        <h1 class="box-title">Email Preference: </h1>
                    </div>
                    <!-- /.box-header -->

                <!-- /.box-header -->
                    <div class="box-body no-padding">
                        <table class="table table-striped">
                            <tr>
                                <th width="200"><span style="color:#808184;">Status</span></th>
                                <th width="200"><span style="color:#808184;">News Type</span></th>
                            </tr>
                            <tr>
                                <td><span @if(auth()->user()->email_preference->latest_news == 1) class="fa fa-check" @else class="fa fa-close" @endif></span></td>
                                <td>Latest News</td>
                            </tr>
                            <tr>
                                <td><span @if(auth()->user()->email_preference->promo == 1) class="fa fa-check" @else class="fa fa-close" @endif></span></td>
                                <td>Promo</td>
                            </tr>
                            <tr>
                                <td><span @if(auth()->user()->email_preference->editorial == 1) class="fa fa-check" @else class="fa fa-close" @endif></span></td>
                                <td>Editorial</td>
                            </tr>
                            <tr>
                                <td><span @if(auth()->user()->email_preference->new_product == 1) class="fa fa-check" @else class="fa fa-close" @endif></span></td>
                                <td>New Product</td>
                            </tr>
                            <tr>
                                <td><span @if(auth()->user()->email_preference->low_stock == 1) class="fa fa-check" @else class="fa fa-close" @endif></span></td>
                                <td>Low Stock</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
        <!-- /.tab-content -->
    </div>
    <!-- nav-tabs-custom -->
    @include('admin.layouts.delete_modal')
@endsection

@section('myscript')
    <!-- DataTables -->
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script>
        $("#example1").DataTable();
        $(".delete-entity-index").on('click', function(e){
            var $form=$(this).closest('form');
            e.preventDefault();
            $('#confirm').modal({ backdrop: 'static', keyboard: false })
                .on('click', '#delete', function() {
                    $form.trigger('submit'); // submit the form
                });
        })
    </script>
@endsection
