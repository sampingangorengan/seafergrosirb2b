@extends('admin.layouts.master')

@section('title')
    Edit User | Control Room
@endsection

@section('content-header')
    <div class="box-header with-border">
        <h3 class="box-title">Edit User</h3>
    </div>
@endsection

@section('content')
    <div class="box box-primary">

        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::model($user,['method'=>'PATCH', 'action'=>['Admin\\UserController@update', $user->id]]) !!}
        <div class="box-body">
            <div class="form-group">
                <label for="nameInput">User Name</label>
                {!! Form::text('name', null, ['class'=>'form-control', 'placeholder' => 'User\'s Name', 'disabled' => 'disabled']) !!}
            </div>
            <div class="form-group">
                <label for="nameInput">Email</label>
                {!! Form::text('email', null, ['class'=>'form-control', 'placeholder' => 'Email Address', 'disabled' => 'disabled']) !!}
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Roles</label>
                <br/>
                {!! Form::select('role', $roles, null, ['class'=>'form-control']) !!}
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Status</label>
                <br/>
                {!! Form::select('is_active', array(1 => 'Active', 0 => 'Inactive'), null, ['class'=>'form-control']) !!}
            </div>
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
        {!! Form::close() !!}
    </div>
    <!-- /.box -->
@endsection

@section('myscript')
    <!-- DataTables -->
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script>
        $("#example1").DataTable();
    </script>
@endsection