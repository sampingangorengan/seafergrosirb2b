@extends('admin.layouts.master')

@section('title')
    Users | Control Room
@endsection

@section('content-header')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            User List
            <small>advanced tables</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Users</a></li>
            <li class="active">List User</li>
        </ol>
    </section>
@endsection

@section('content')

    <div class="box">
        <!-- /.box-header -->
        <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Company - Brand - Phone</th>
                    <th>PIC Name</th>
                    <th>PIC Contact</th>
                    <th>Email PIC</th>
                    <th>Website</th>
                    <th>Category</th>
                    <th>Status</th>
                    <th>Options</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                <tr>
                    <td width="5%">#{{$user->id}}</td>
                    <td>{{ $user->company_name }} - {{ $user->brand_name }} - {{ $user->company_phone }}</td>
                    <th>{{ $user->name }}</th>
                    <th>{{ $user->phone_number }}</th>
                    <th>{{ $user->email }}</th>
                    <th>{{ $user->website }}</th>
                    <th>{{ $user->category }}</th>
                    <th {{ $user->status == 1 ? 'class="text-green"' : 'class="text-red"' }}>{{ $user->is_active == 1 ? 'ACTIVE' : 'INACTIVE' }}</th>
                    <th>
                        <a href="{{ url('admin/users/'.$user->id)    }}"><button type="button" class="btn-sm btn-info">View</button></a>
                        <a href="{{ url('admin/users/'.$user->id.'/edit')    }}"><button type="button" class="btn-sm btn-primary">Edit</button></a>
                        {!! Form::open(['method' => 'DELETE', 'action' => ['Admin\\UserController@destroy', $user->id], 'class'=> ""]) !!}
                            <button type="submit" class="btn btn-danger btn-sm btn-flat delete-entity-index">Delete</button>
                        {!! Form::close() !!}
                    </th>
                </tr>
                @endforeach

                </tbody>
                <tfoot>
                    <tr>
                        <th>Company - Brand - Phone</th>
                        <th>PIC Name</th>
                        <th>PIC Contact</th>
                        <th>Email PIC</th>
                        <th>Website</th>
                        <th>Category</th>
                        <th>Status</th>
                        <th>Options</th>
                    </tr>
                </tfoot>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
    @include('admin.layouts.delete_modal')
@endsection

@section('myscript')
    <!-- DataTables -->
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script>
        $(".delete-entity-index").on('click', function(e){
            var $form=$(this).closest('form');
            e.preventDefault();
            $('#confirm').modal({ backdrop: 'static', keyboard: false })
                .on('click', '#delete', function() {
                    $form.trigger('submit'); // submit the form
                });
        })
    </script>
@endsection
