@extends('admin.layouts.master')

@section('title')
    Edit Category Level One | Control Room
@endsection

@section('content-header')
    <div class="box-header with-border">
        <h3 class="box-title">Edit Category Level One</h3>
    </div>
@endsection

@section('content')
    <div class="box box-primary">

        <!-- form start -->
        {!! Form::model($category, ['method'=>'PATCH', 'action'=>['Admin\\ParentCategoryController@update', $category->id], 'files' => True]) !!}
        <div class="box-body">
            <div class="form-group">
                <label for="nameInput">Category Name</label>
                {!! Form::text('name', null, ['class'=>'form-control', 'placeholder' => 'Category Name', 'required' => 'required']) !!}
            </div>
            <div class="form-group">
                <label for="nameInput">Category Slug</label>
                {!! Form::text('slug', null, ['class'=>'form-control', 'placeholder' => 'e.g., delicious-snack (use dash to separate)', 'required' => 'required']) !!}
            </div>
            <div class="form-group">
                <label for="nameInput">Category Order</label>
                {!! Form::text('order', null, ['class'=>'form-control', 'placeholder' => 'e.g., 3 (must be integer)', 'required' => 'required']) !!}
            </div>
            <div class="form-group">
                <label for="nameInput">Category Image</label><br/>
                @if(null !== $category->image)
                <img src="{{ $category->image->file }}" width="100px" height="auto"/>
                @else
                <p style="color:red;margin-left:2px;">This category does not have any image yet.</p>
                @endif
                {!! Form::file('file', null, ['class'=>'form-control', 'required' => 'required']) !!}
                <p style="color:red;margin-left:2px;">*)The best size for this image is 120 x 120px</p>
            </div>
            <div class="form-group">
                <label for="nameInput">Category Status</label>
                {!! Form::select('is_active', array(1 => 'Active', 2 => 'Inactive'), null, ['class'=>'form-control']) !!}
            </div>


        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            {{ csrf_field() }}
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
        {!! Form::close() !!}
    </div>
    <!-- /.box -->
@endsection

@section('myscript')
    <!-- DataTables -->
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script>
        $("#example1").DataTable();
    </script>
@endsection