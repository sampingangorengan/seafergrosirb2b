@extends('admin.layouts.master')

@section('title')
Child Category | Control Room
@endsection

@section('content-header')
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))

                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
        @endforeach
    </div>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            List Category Level Two
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ url('admin/child-category') }}">Categories</a></li>
            <li class="active">List Categories Level Two</li>
        </ol>
        <br/>
        <a href="{{ url('admin/child-category/create') }}"><button class="btn btn-flat btn-sm btn-info">Create New Category Level Two</button></a>
        <br/>
        <br/>
    </section>
@endsection

@section('content')

<br/>
<div class="box">
    <div class="box-body">
        <table id="example1" class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>Name</th>
                <th>Slug</th>
                <th>Order</th>
                <th>Belongs to</th>
                <th>Status</th>
                <th>Options</th>
            </tr>
            </thead>
            <tbody>
            @foreach($categories as $category)
            <tr>
                
                <td>{{ $category->name }}</td>
                <td>{{ $category->slug }}</td>
                <td>{{ $category->order }}</td>
                @if(null != $category->parent)
                <td>{{ $category->parent->name }}</td>
                @else
                <td>Parent not Found</td>
                @endif
                <td><span class="{{ $category->is_active == 1 ? 'text-green' : 'text-red' }}">{{ $category->is_active == 1 ? 'ACTIVE' : 'INACTIVE' }}</span></td>

                <th>
                    <a href="{{ url('admin/child-category/'.$category->id.'/edit') }}" class="pull-left"><button type="button" class="btn btn-sm btn-flat btn-primary">Edit</button>&nbsp;</a>
                    {!! Form::open(['method' => 'DELETE', 'action' => ['Admin\\ChildCategoryController@destroy', $category->id], 'class'=> "pull-left"]) !!}
                    <button type="submit" class="btn btn-danger btn-sm btn-flat delete-entity-index">Delete</button>
                    {!! Form::close() !!}
                </th>
            </tr>
            @endforeach

            </tbody>
            <tfoot>
            <tr>
                <th>Name</th>
                <th>Slug</th>
                <th>Order</th>
                <th>Belongs to</th>
                <th>Status</th>
                <th>Options</th>
            </tr>
            </tfoot>
        </table>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->

    @include('admin.layouts.delete_modal')
@endsection

@section('myscript')
<!-- DataTables -->
<script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
    $("#example1").DataTable();
    $(".delete-entity-index").on('click', function(e){
        var $form=$(this).closest('form');
        e.preventDefault();
        $('#confirm').modal({ backdrop: 'static', keyboard: false })
            .on('click', '#delete', function() {
                $form.trigger('submit'); // submit the form
            });
    })
</script>
@endsection