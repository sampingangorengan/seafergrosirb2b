@extends('admin.layouts.master')

@section('title')
    Edit Category Level Two | Control Room
@endsection

@section('content-header')
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))

                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
        @endforeach
    </div>
    <div class="box-header with-border">
        <h3 class="box-title">Edit Category Level Two</h3>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ url('admin/child-category') }}">Categories</a></li>
            <li class="active">Edit Categories Level Two</li>
        </ol>
    </div>
@endsection

@section('content')
    <div class="box box-primary">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <!-- form start -->
        {!! Form::model($childCategory, ['method'=>'PATCH', 'action'=>['Admin\\ChildCategoryController@update', $childCategory->id], 'files' => True]) !!}
        <div class="box-body">
            <div class="form-group">
                <label for="nameInput">Category Name</label>
                {!! Form::text('name', null, ['class'=>'form-control', 'placeholder' => 'Category Name', 'required' => 'required']) !!}
            </div>
            <div class="form-group">
                <label for="nameInput">Category Slug</label>
                {!! Form::text('slug', null, ['class'=>'form-control', 'placeholder' => 'e.g., delicious-snack (use dash to separate)', 'required' => 'required']) !!}
            </div>
            <div class="form-group">
                <label for="nameInput">Category Order</label>
                {!! Form::text('order', null, ['class'=>'form-control', 'placeholder' => 'e.g., 3 (must be integer)', 'required' => 'required']) !!}
            </div>
            {{-- <div class="form-group">
                <label for="nameInput">Category Image</label><br/>
                @if(null !== $childCategory->image)
                <img src="{{ $childCategory->image->file }}" width="100px" height="auto"/>
                @else
                <p style="color:red;margin-left:2px;">This category does not have any image yet.</p>
                @endif
                {!! Form::file('file', null, ['class'=>'form-control']) !!}
                <p style="color:red;margin-left:2px;">*)The best size for this image is 120 x 120px</p>
            </div> --}}
            <div class="form-group">
                <label for="nameInput">Category Parent</label>
                {!! Form::select('parent_id', $parent, null, ['class'=>'form-control', 'required' => 'required']) !!}
            </div>
            <div class="form-group">
                <label for="nameInput">Category Status</label>
                {!! Form::select('is_active', array(1 => 'Active', 2 => 'Inactive'), null, ['class'=>'form-control']) !!}
            </div>


        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            {{ csrf_field() }}
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
        {!! Form::close() !!}
    </div>
    <!-- /.box -->
@endsection

@section('myscript')
    <!-- DataTables -->
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script>
        $("#example1").DataTable();
    </script>
@endsection