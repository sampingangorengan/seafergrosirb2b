@extends('admin.layouts.master')

@section('title')
    Order | Control Room
@endsection

@section('content-header')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Order List
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ url('admin/tempo') }}">Tempo</a></li>
            <li class="active">List {{$status}} Tempo</li>
        </ol>
    </section>
    <br/>
@endsection

@section('content')

    <div class="box">

        <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Username</th>
                    <th>Tempo Status</th>
                    <th>Apply Date</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($user_tempo as $tempo)
                        <tr>
                            <td>{{$tempo->user->name ?? "-"}}</td>
                            <td style="color:white;
                            background-color:
                            <?
                            switch($tempo->status){
                                case "pending":
                                echo "#dbe642";
                                break;
                                case "approved":
                                echo "#35bd35";
                                break;
                                case "unsuccessful":
                                echo "#e64242";
                                break;
                            }
                            ?>
                            ">{{$tempo->status}}</td>
                            <td>{{LocalizedCarbon::parse($tempo->updated_at)->format('d F Y H:i') }}</td>
                            <td><a href="{{ url('admin/tempo/'.$tempo->id) }}/edit"><button type="button" class="btn btn-info">Review</button></a></td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                <tr>

                </tr>
                </tfoot>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->
    @include('admin.layouts.delete_modal')
@endsection

@section('myscript')
    <!-- DataTables -->
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script>
        $("#example1").DataTable(
        {
            "order": [[ 1, "asc" ]]
        });

    </script>
@endsection
