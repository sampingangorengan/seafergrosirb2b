@extends('admin.layouts.master')

@section('title')
    Edit User | Control Room
@endsection

@section('content-header')
    <div class="box-header with-border">
        <h3 class="box-title">Edit Tempo</h3>
    </div>
@endsection

@section('content')
    <div class="box box-primary">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::model($tempo,['method'=>'PATCH', 'action'=>['Admin\\TempoController@update', $tempo->id]]) !!}
        <div class="box-body">
            <div class="form-group">
                <label for="">KTP</label>
                <img src="{{asset('ktp_user/'.$tempo->user->ktp)}}" alt="">
            </div>
            <div class="form-group">
                <label for="">NPWP</label>
                <img src="{{asset('npwp_user/'.$tempo->user->upload_npwp)}}" alt="">
            </div>
            <div class="form-group">
                <label for="">Status</label>
                <select class="form-control" name="status">
                    <option value="pending" <?php if($tempo->status == "pending") echo "selected";?>>Pending</option>
                    <option value="approved" <?php  if($tempo->status == "approved") echo "selected";?>>Approve</option>
                    <option value="unsuccessful" <?php  if($tempo->status == "unsuccessful") echo "selected";?>>Reject</option>
                </select>
            </div>
            <div class="form-group">
                <label for="">Duration</label>
                {!! Form::text('duration', null, ['class'=>'form-control', 'placeholder' => 'Duration in Days,example input:    2 or 4 or 16',"required"=>"required"]) !!}
            </div>
            <div class="form-group">
                <label for="nameInput">Aggrement</label>
                {!! Form::textarea('aggrement', null, ['class'=>'form-control',
                                                        'placeholder' => 'The aggrement notes',
                                                        "required"=>"required",
                                                        "rows" => "8",
                                                        "cold" => "80"]) !!}

            </div>
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
        {!! Form::close() !!}
    </div>
    <!-- /.box -->
@endsection

@section('myscript')
    <!-- DataTables -->
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script>
        $("#example1").DataTable();
    </script>
@endsection
