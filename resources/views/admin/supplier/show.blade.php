@extends('admin.layouts.master')

@section('title')
    Supplier | Control Room
@endsection

@section('content-header')
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))

                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
        @endforeach
    </div>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            {{ $supplier->company_name }}'s Detail
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ url('admin/supplier') }}"> Supplier</a></li>
            <li class="active"> Supplier Detail</li>
        </ol>
    </section>
@endsection

@section('content')

    <!-- Custom Tabs -->
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_1" data-toggle="tab">General Information</a></li>
              <li><a href="#tab_2" data-toggle="tab">Product Information</a></li>
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    Actions <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <!-- <li role="presentation"><a role="menuitem" tabindex="-1" href="{{ url('admin/supplier/'.$supplier->id.'/edit') }}"><button type="button" class="btn-sm btn-primary">Edit</button></a></li> -->


                    <li role="presentation">
                        {!! Form::open(['method' => 'DELETE', 'action' => ['Admin\\SupplierController@destroy', $supplier->id], 'class'=> ""]) !!}
                        <button type="submit" class="btn btn-danger btn-sm btn-flat delete-entity-index" style="margin:0 auto;">Delete</button>
                        {!! Form::close() !!}
                    </li>
                    {{--<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Create New</a></li>--}}
                    {{--<li role="presentation" class="divider"></li>
                    <li role="presentation"><a role="menuitem" tabindex="-1" target="_blank" href="{{ url('groceries/'.$product->id.'/'.$product->name) }}">View on frontend</a></li>--}}
                </ul>
            </li>
            <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab_1">

                <div class="box box-solid">
                    <div class="box-header with-border">
                        <h1 class="box-title">Supplier Detail:</h1>
                    </div>
                    <!-- /.box-header -->
                    <!-- /.box-body -->


                <!-- /.box-header -->
                    <div class="box-body no-padding">
                        <table class="table table-striped">
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>Property</th>
                                <th>Value</th>
                            </tr>
                            <tr>
                                <td>1.</td>
                                <td>Company Name</td>
                                <td>
                                    {{ $supplier->company_name }}
                                </td>
                            </tr>
                            <tr>
                                <td>2.</td>
                                <td>Bussiness Entity</td>
                                <td>
                                    {{ $supplier->bussiness_entity }}
                                </td>
                            </tr>
                            <tr>
                                <td>3.</td>
                                <td>Address</td>
                                <td>
                                    {{ $supplier->billing_address }}
                                </td>
                            </tr>
                            <tr>
                                <td>4.</td>
                                <td>Title</td>
                                <td>
                                    {{ $supplier->title }}
                                </td>
                            </tr>
                            <tr>
                                <td>5.</td>
                                <td>PIC Name</td>
                                <td>
                                    {{ $supplier->user_name }}
                                </td>
                            </tr>
                            <tr>
                                <td>6.</td>
                                <td>Phone Number</td>
                                <td>
                                    {{ $supplier->phone_number }}
                                </td>
                            </tr>
                            <tr>
                                <td>7.</td>
                                <td>Email</td>
                                <td>
                                    {{ $supplier->email }}
                                </td>
                            </tr>
                            <tr>
                                <td>8.</td>
                                <td>Website</td>
                                <td>
                                    {{ $supplier->website }}
                                </td>
                            </tr>
                            <tr>
                                <td>9.</td>
                                <td>Kategori</td>
                                <td>
                                    {{ $supplier->category }}
                                </td>
                            </tr>
                            <tr>
                                <td>10.</td>
                                <td>Comodity</td>
                                <td>
                                    {{ $supplier->Commoditas }}
                                </td>
                            </tr>
                            <tr>
                                <td>11.</td>
                                <td>Preferred Selling Method</td>
                                <td>
                                    {{ $supplier->selling_method }}
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <div class="tab-pane active" id="tab_2">

                <div class="box box-solid">
                    <div class="box-header with-border">
                        <h1 class="box-title">Supplier Detail:</h1>
                    </div>
                    <!-- /.box-header -->
                    <!-- /.box-body -->


                <!-- /.box-header -->
                    <div class="box-body no-padding">
                        <table class="table table-striped">
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>Property</th>
                                <th>Value</th>
                            </tr>
                            <tr>
                                <td>1.</td>
                                <td>Type of Goods supplied</td>
                                <td>
                                    {{ $supplier->type_of_goods }}
                                </td>
                            </tr>
                            <tr>
                                <td>2.</td>
                                <td>Product Original</td>
                                <td>
                                    {{ $supplier->product_original }}
                                </td>
                            </tr>
                            <tr>
                                <td>3.</td>
                                <td>Country</td>
                                <td>
                                    {{ $supplier->country }}
                                </td>
                            </tr>

                        </table>
                    </div>
                </div>
                <!-- /.box -->
            </div>

        </div>
        <!-- /.tab-content -->
    </div>
    <!-- nav-tabs-custom -->
    @include('admin.layouts.delete_modal')
@endsection

@section('myscript')
    <!-- DataTables -->
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script>
        $("#example1").DataTable();
        $(".delete-entity-index").on('click', function(e){
            var $form=$(this).closest('form');
            e.preventDefault();
            $('#confirm').modal({ backdrop: 'static', keyboard: false })
                .on('click', '#delete', function() {
                    $form.trigger('submit'); // submit the form
                });
        })
    </script>
@endsection
