<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>Hi, {{ $user_supplier->user_name }}</h2>

<div>

    <p>Selamat datang di Seafer Grosir dan selamat telah berhasil bergabung dengan Seafer Grosir! Kami berharap dapat membantu Anda sebanyak mungkin untuk mengembangkan bisnis supply makanan Anda.
Dengan Seafer Grosir, Anda sekarang dapat mengakses ribuan bisnis kuliner tanpa biaya listing, pembayaran yang pasti dan pengiriman dan penyimpanan produk yang tersedia! Kami akan melakukan verifikasi data terlebih dahulu sebelum akun anda <font color="red">aktif</font>.</p>

   <!--  <p><a href="{{ url('user/verify-account-supplier/'. $user_supplier->email_verif) }}"> Verify Now! </a></p> -->
    <br>
    @include('emails.layout.footer')
</div>



</body>
</html>
