<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>Dear Sahabat Seafer Grosir {{ $user->name }}!</h2>

<div>
    <p>Terima Kasih atas pesanan anda! Pesanan anda akan segera kami proses dan kirim. Untuk pembayaran, mohon melakukan pembayaran sesuai dengan methode pembayaran yang telah dipilih anda untuk pesanan Order <a href="{{url('orders/'.$order->id)}}">#{{$order->id}}</a>.</p>
    <br>
    {!! $mes !!}
    <br/>
    <p>Terima Kasih atas pesanan anda, Sahabat Seafer Grosir!</p>
    <br>
    <p>Klik untuk melihat detail pesanan: Link ke order tersebut <a href="{{url('orders/'.$order->id)}}">{{url('orders/'.$order->id)}}</p>
    <br>
    @include('emails.layout.footer')
</div>

</body>
</html>
