<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>Dear Admin,</h2>

<div>
    <p>This user has created COD Order <a href="{{ url('admin/orders/'. $order_number) }}">#{{ $order_number }}</a></p>
    <br/>
    <p>Please check the order and take action of the order made immediately.</p>
    <br/>
    <p>This notification email will be send once. You can check the list of the awaiting-action COD orders <a href="{{ url('admin/orders?get=cod') }}">here</a></p>
    <br>
    @include('emails.layout.footer')
</div>

</body>
</html>
