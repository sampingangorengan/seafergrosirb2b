<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>Hi, {{ $user->name }}!</h2>

<div>
    <p>Selamat datang di Seafer Grosir dan selamat telah berhasil bergabung dengan Seafer Grosir! Kami berharap dapat membantu Anda sebanyak mungkin untuk mengembangkan bisnis kuliner Anda.</p>
    <br/>
    <p> Dengan Seafer Grosir, Anda sekarang dapat memesan kebutuhan bisnis kuliner yang berkualitas Anda dalam satu aplikasi dengan harga terjangkau, pembayaran fleksibel dan pengiriman tepat waktu dan gratis!</p>
    <br>
    <p><strong>Selamat berbelanja dan terima kasih Sahabat Seafer Grosir!</strong></p>
    <br>
    @include('emails.layout.footer')
</div>



</body>
</html>
