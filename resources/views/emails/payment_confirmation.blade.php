{{--
Click here to reset your password: <a href="{{ $link = url('password/reset', $token) }}"> Click me! </a>--}}
        <!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>Hi, {{ $user->name }}!</h2>

<div>

    <p>Thank you for your payment! Here are the details of your payment for your reference:</p>
    <table style="border:none">
        <tr>
            <td>Order ID</td>
            <td>:</td>
            <td>{{ $order->id }}</td>
        </tr>
        <tr>
            <td>Total Transfer</td>
            <td>:</td>
            <td>{{ $order->getPayment($order->id)->transfer_amount }}</td>
        </tr>
        <tr>
            <td>Date and Time of Transfer</td>
            <td>:</td>
            <td>{{ $order->getPayment($order->id)->transfer_date }} {{ $order->getPayment($order->id)->transfer_time }}</td>
        </tr>
        <tr>
            <td>Bank Institution Name</td>
            <td>:</td>
            <td>{{ $order->getPayment($order->id)->transfer_dest_account }}</td>
        </tr>
        <tr>
            <td>Order Status</td>
            <td>:</td>
            <td>{{ ucfirst($order->status->description) }}</td>
        </tr>
        <tr>
            <td>Notes</td>
            <td>:</td>
            <td>{{ $order->getPayment($order->id)->transfer_note }}</td>
        </tr>
    </table>
    <p>Your order is being processed. If there is no problem with the payment, we will deliver your order immediately.</p>
    <p>Once again, thank you for shopping at Seafer Grosir!</p>

    @include('emails.layout.footer')

</div>

</body>
</html>
