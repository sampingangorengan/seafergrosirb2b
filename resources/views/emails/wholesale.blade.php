{{--
Click here to reset your password: <a href="{{ $link = url('password/reset', $token) }}"> Click me! </a>--}}
<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>Hi, {{ $pic }} from {{ $company }}!</h2>

<p>Your request has been sent to our team.</p>
<p>We will get back to you soon to discuss further about our interest regarding your wholesale request.</p>
<p>Here are the recap of what you have submitted:</p>

<div>
	{{-- <p>Order:</p>
	<p>http://foodis.co.id/orders/{{ $order }}</p> --}}
	<br/>
   	<p><u>Company Name:</u></p>
   	<p>{{ $company }}</p>
   	<br/>
   	<p><u>PIC Name:</u></p>
   	<p>{{ $pic }}</p>
    <br/>
   	<p><u>Product List:</u></p>
   	<?php $counter = 0; ?>
   	@foreach($products as $product)
	<p>{{ $counter + 1 }}.<p>
	<p>Product: <a href="{{ $product['url'] }}" >{{ $product['name'] }}</a>
	<p>Volume: {{ $product['volume'] }}</p>
	<br/>
	<?php $counter = $counter + 1; ?>
   	@endforeach
   	<p><u>Type of business: </u></p>
   	<p>{{ $type }}</p>
    <br/>
    <p><u>Message: </u></p>
   	<p>{{ $msg }}</p>
    <br/>
    @include('emails.layout.footer')
</div>

</body>
</html>
