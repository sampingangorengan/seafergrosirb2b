{{--
Click here to reset your password: <a href="{{ $link = url('password/reset', $token) }}"> Click me! </a>--}}
<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>Hi, {{ $name }}</h2>

<div>
	{{-- <p>Order:</p>
	<p>http://foodis.co.id/orders/{{ $order }}</p> --}}
	<br/>
   	{!! $msg !!}
    <br/>
    @include('emails.layout.footer')
</div>

</body>
</html>
