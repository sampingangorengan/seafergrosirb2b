<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>Dear Admin,</h2>

<div>
    <p>It's been three days since {{ $name }} has paid for Order <a href="{{ url('admin/orders/'. $order_number) }}">#{{ $order_number }}</a></p>
    <br/>
    <p>Please check the bank account and take action of the payment made by user <a href="{{ url('admin/transactions/'. $payment_id) }}">here</a>.</p>
    <br/>
    <p>This notification email will be send once. You can check the list of the awaiting approval payment <a href="{{ url('admin/transactions?get=pending') }}">here</a></p>
    <br>
    @include('emails.layout.footer')
</div>

</body>
</html>
