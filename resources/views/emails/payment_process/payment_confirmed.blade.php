<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>Dear Sahabat Seafer Grosir, {{ $name }}!</h2>

<div>
    <p>Pembayaran pesanan Order #{{$order}} anda telah berhasil dan dikonfirmasi oleh team kami.</p>
    <br/>
    <p>Klik untuk melihat detail pesanan:<a href="{{url('orders/'.$order)}}">#{{$order}}</a>
    </p>
    <p>Terima Kasih atas pesanan anda, Sahabat Seafer Grosir!</p>
    @include('emails.layout.footer')
</div>

</body>
</html>
