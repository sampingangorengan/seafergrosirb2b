@extends('frontend.layout.master')

@section('title', App\Page::createTitle('Error 404'))

@section('customcss')
    <link rel="stylesheet" type="text/css" href="assets/css/custom.css">
@endsection

@section('content')
    <!-- Content Begin -->
    <div class="row">
        <div class="small-12 columns">
            <section>
                <div class="center">
                    <img src="assets/images/404.png" style="width:300px;">
                    <p style="margin-top: 20px;" class="center">
                        The page you are looking for was<br>
                        <b>moved, renamed or might never existed.</b>
                    </p>
                    <a href="{{ url('/') }}"><button class="button primary btn-aneh">HOME</button></a>
                </div>
            </section>
        </div>
    </div>
@endsection
