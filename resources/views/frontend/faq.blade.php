@extends('frontend.layout.master')

@section('title', App\Page::createTitle('Frequently Asked Question'))

@section('customcss')
    <link rel='stylesheet' href='assets/plugin/bootstrap/css/bootstrap-responsive-tabs.css'>
    <link rel="stylesheet" type="text/css" href="assets/plugin/fancybox/css/jquery.fancybox.min.css">
@endsection

@section('content')
<div class="main-content">
	<div class="container"  id="becomeAMember">
        <div class="row hide-768">
			<div class="col-md-12">
				<span class="text-grey equal margin-top-10">
				  <a class="text-grey" href="{{ url('/') }}"><b>HOME</b></a> /
				  <a class="text-grey" href="#"><b>FAQ</b></a>
				</span>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				{{-- <h5 class="title title-margin">FAQ</h5> --}}
                {!! $description[1]->description_content !!}
                {!! $description[2]->description_content !!}
				{{-- <p class="after-title hide-768">
					If a product that you love is not available at Seafer Grosir, do let us know.<br>
					We are happy to accommodate new products for you.
				</p> --}}
                <br/>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 col-sm-4 hide-320">
                    <ul class="side-nav side-nav-become-box" id="menu-collapse">
                        <li><a class="about-us active" data-toggle="collapse" data-parent="#accordion" href="#about-us">ABOUT US</a></li>
                        <li><a href="#loyalty-rewards" data-toggle="collapse" data-parent="#accordion" class="loyalty-rewards">CASHBACK REWARDS</a></li>
                        <li><a href="#orders" data-toggle="collapse" data-parent="#accordion" class="orders">ORDERS</a></li>
                        <li><a href="#products" data-toggle="collapse" data-parent="#accordion" class="products">PRODUCTS</a></li>
                        <li><a href="#delivery" data-toggle="collapse" data-parent="#accordion" class="delivery">DELIVERY</a></li>
                        <li><a href="#payments" data-toggle="collapse" data-parent="#accordion" class="payments">PAYMENTS</a></li>
                        <li><a href="#trobuleshooting" data-toggle="collapse" data-parent="#accordion" class="trobuleshooting">TROUBLESHOOTING</a></li>
                        <li><a href="#grocer" data-toggle="collapse" data-parent="#accordion" class="grocer">ABOUT SEAFER GROCER</a></li>
                    </ul>
                </div>
                <div class="col-md-8 col-sm-8">
                    <div class="faq-box">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#about-us" aria-expanded="true">
											ABOUT US
										</a>
									</h4>
								</div>
                                <div id="about-us" class="panel-collapse collapse in">
									<div class="panel-body">
                                        {!! $description[3]->description_content !!}
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
								  <h4 class="panel-title">
									<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#loyalty-rewards" aria-expanded="false">
										CASHBACK REWARDS
									</a>
									
									</h4>
								</div>
								<div id="loyalty-rewards" class="panel-collapse collapse">
									<div class="panel-body">
										{!! $description[4]->description_content !!}
									</div>
                                </div>
                            </div>
							<div class="panel panel-default">
								<div class="panel-heading">
								  <h4 class="panel-title">
									<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#orders" aria-expanded="false">
										ORDERS
									</a>
									</h4>
								</div>
                                <div id="orders" class="panel-collapse collapse">
									<div class="panel-body">
                                        {!! $description[5]->description_content !!}
									</div>
								</div>
                            </div>
							<div class="panel panel-default">
								<div class="panel-heading">
								  <h4 class="panel-title">
									<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#products" aria-expanded="false">
										PRODUCTS
									</a>
								  </h4>
								</div>
                                <div id="products" class="panel-collapse collapse">
									<div class="panel-body">
                                        {!! $description[6]->description_content !!}
									</div>
								</div>
                            </div>
							<div class="panel panel-default">
								<div class="panel-heading">
								  <h4 class="panel-title">
									<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#delivery" aria-expanded="false">
										DELIVERY
									</a>
								   </h4>
								</div>
                                <div id="delivery" class="panel-collapse collapse">
									<div class="panel-body">
                                        {!! $description[7]->description_content !!}
									</div>
                                </div>
                            </div>
							<div class="panel panel-default">
								<div class="panel-heading">
								  <h4 class="panel-title">
									<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#payments" aria-expanded="false">
										PAYMENT
									</a>
								   </h4>
								</div>
                                <div id="payments" class="panel-collapse collapse">
									<div class="panel-body">
                                        {!! $description[8]->description_content !!}
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
								  <h4 class="panel-title">
									<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#trobuleshooting" aria-expanded="false">
										TROUBLESHOOTING
									</a>
								   </h4>
								</div>
                                <div id="trobuleshooting" class="panel-collapse collapse">
									<div class="panel-body">
                                        {!! $description[9]->description_content !!}
									</div>
                                </div>
                            </div>
							<div class="panel panel-default">
								<div class="panel-heading">
								  <h4 class="panel-title">
									<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#grocer" aria-expanded="false">
										ABOUT SEAFER GROCER
									</a>
								   </h4>
								</div>
                                <div id="grocer" class="panel-collapse collapse">
									<div class="panel-body">
                                        {!! $description[10]->description_content !!}
									</div>
                                </div>
                           </div>
                        </div>
                    </div>
                </div>
		</div>
	</div>
	
</div>
@endsection

@section('scripts')
<script src="assets/plugin/bootstrap/js/jquery.bootstrap-responsive-tabs.min.js"></script>
<script src="assets/plugin/fancybox/js/jquery.fancybox.min.js"></script>
@endsection