@extends('frontend.layout.master')

@section('title', App\Page::createTitle('Sign In'))

@section('customcss')
    <link rel='stylesheet' href='assets/plugin/bootstrap/css/bootstrap-responsive-tabs.css'>
    <link rel="stylesheet" type="text/css" href="assets/plugin/fancybox/css/jquery.fancybox.min.css">
@endsection

@section('content')
	<div class="main-content">
		<div class="container" id="SignIn">
			<div class="row">
				<div class="col-md-11 col-sm-11 col-xs-11 col-centered">
					<span class="title"><img class="arrow-L" src="assets/images/arrow-L.png"> SIGN IN</span>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-sm-6 col-xs-11 col-centered">
					<div class="form-box">
						<form method="post">
							<input type="text" placeholder="Your Email" name="email" style="border:2px solid #808184;">
							<input type="password" placeholder="Password" name="password" style="border:2px solid #808184;">
							<div class="box">
								<input id="keepMe2" type="checkbox" name="remember">
								<label for="keepMe2" style="color:#A9A9A9;font-size: 12px;padding-bottom: -1px;">Keep me signed in</label><br>
							</div>
							{{ csrf_field() }}
							<div class="col-md-12 center">
							<button class="btn btn-aneh" type="submit" id="myLogin">SUBMIT</button><br>
							<a href="#" id="forgotPassword" data-toggle="modal" data-target="#modal-forgotpassword">Forgot Password?</a>
							</div>
						</form>
						<div class="col-md-12 center">
							<a href="{{ url('login/facebook') }}"><button class="btn sign-in fb">
								<img src="assets/images/ico-fb-white.png">Sign In with Facebook
							</button></a>
							<br/>
							<a href="{{ url('login/google') }}"><button class="btn sign-in gplus">
								<img src="assets/images/ico-gplus-white.png">Sign In with Google Plus
							</button></a><br/>
						</div>
					</div>
				</div>
	        </div>
		</div>
	</div>
@endsection

@section('scripts')
<script src="assets/plugin/bootstrap/js/jquery.bootstrap-responsive-tabs.min.js"></script>
<script src="assets/plugin/fancybox/js/jquery.fancybox.min.js"></script>
@endsection