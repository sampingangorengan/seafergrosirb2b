@extends('frontend.layout.master')

@section('title', App\Page::createTitle('Sign Up'))

@section('customcss')

    <link rel='stylesheet' href='assets/plugin/bootstrap/css/bootstrap-responsive-tabs.css'>
    <link rel="stylesheet" type="text/css" href="assets/plugin/fancybox/css/jquery.fancybox.min.css">

@endsection

@section('content')
	<div class="main-content">
		<div class="container" id="signUp">
			<div class="row">
				<div class="col-md-12">
					<h6 class="title">FORM PENDAFTARAN</h6>
				</div>
			</div>
			<div class="row">
				@if (count($errors) > 0)

	            <div class="alert alert-danger">
	                <ul>
	                    @foreach ($errors->all() as $error)
	                    	<li>{{ $error }}</li>
	                    @endforeach
	                </ul>
	            </div>
	            @endif
			</div>
			<div class="row">
	            <div class="col-md-3 no-padding-h center hide-768">
	                <img class="pink" src="assets/images/sign-up-1.png">
	            </div>
	        	<form method="post"  id="userForm" enctype="multipart/form-data">
	        		{{ csrf_field() }}
					<div class="col-md-6 col-sm-8 col-xs-10 no-padding-h col-centered-768">
						<div class="form-box">
							<div class="tab">
								<div class="title-form-step">
									<h4>Langkah 1 / 3</h4>
									<h3>Informasi Perusahaan</h3>
								</div>
								<div class="row">
								    <div class="col-md-4">
								        <label for="middle-label" class="text-left middle">Nama Perusahaan<span class="text-red" >*</span></label>
								    </div>
								    <div class="col-md-8">
								        <input class="required" type="text" name="company_name" placeholder="" value="{{ old('company_name') }}"/>
								    </div>
								</div>
								<div class="row">
								    <div class="col-md-4">
								        <label for="middle-label" class="text-left middle">Nama Brand</label>
								    </div>
								    <div class="col-md-8">
								        <input type="text" name="brand_name" placeholder="" value="{{ old('brand_name') }}"/>
								    </div>
								</div>
								<div class="row">
								    <div class="col-md-4">
								        <label for="middle-label" class="text-left middle">Entitas Bisnis<span class="text-red" >*</span></label>
								    </div>
								    <div class="col-md-8">
				                        <select class="short required" name="business_entity">
				                            <option value="pt">PT.</option>
				                            <option value="cv">CV.</option>
				                            <option value="individual">Individual.</option>
				                        </select>
				                    </div>
								</div>
								<div class="row">
								    <div class="col-md-4">
								        <label for="middle-label" class="text-left middle">Alamat Penagihan<span class="text-red" >*</span></label>
								    </div>
								    <div class="col-md-8">
								        <textarea type="text" name="billing_address" placeholder="" class="required" />{{ old('billing_address') }}</textarea>
								    </div>
								</div>
								<div class="row">
								    <div class="col-md-4"></div>
								    <div class="col-md-8">
				                        {!! Form::select('province', $provinces, old('province'), ['class'=>'short', 'id' => 'select_province']) !!}
				                    </div>
								</div>
								<div class="row">
								    <div class="col-md-4"></div>
								    <div class="col-md-8">
								    	<select class="short" name="city" id="select_city">
								    		<option value="">Pilih Kota</option>
											@if(old('city'))
												<option value="{{old('city')}}" selected>{{\App\Models\City::where('city_id',old('city'))->first()->city_name}}</option>
											@endif
								    	</select>
				                    </div>
								</div>
								<div class="row">
								    <div class="col-md-4"></div>
								    <div class="col-md-8">
								    	<select class="short" name="area" id="select_area">
								    		<option value="">Pilih Area</option>
											@if(old('area'))
												<option value="{{old('area')}}" selected>{{\App\Models\Location\Area::find(old('area'))->name}}</option>
											@endif
								    	</select>
				                    </div>
								</div>
								<div class="row">
								    <div class="col-md-4"></div>
								    <div class="col-md-8">
								        <input type="text" class="required" name="postal_code" placeholder="Kode Pos" value="{{ old('postal_code') }}"/>
								    </div>
								</div>
								<div class="row">
								    <div class="col-md-4">
								        <label for="middle-label" class="text-left middle">Alamat Pengiriman<span class="text-red" >*</span></label>
								    </div>
								    <div class="col-md-8">
								    	<div class="row">
								    		<div class="col-md-12">
								    			<input type="radio"  name="choose_shipping_address" value="billing_address" @if(old("choose_shipping_address") == "billing_address") checked @endif>Gunakan alamat pembayaran sebagai alamat pengiriman
								    		</div>
								    		<div class="col-md-12">
								    			<input type="radio" name="choose_shipping_address" value="another_address" @if(old("choose_shipping_address") == "another_address") checked @endif> Gunakan alamat lain
								    		</div>
								    	</div>
								    </div>
								</div>
								<div class="another_address">
									<div class="row">
									    <div class="col-md-4"></div>
									    <div class="col-md-8">
									        <textarea type="text" name="shipping_address" placeholder=""/>{{ old('Shipping Address') }}</textarea>
									    </div>
									</div>
									<div class="row">
									    <div class="col-md-4"></div>
									    <div class="col-md-8">
					                        {!! Form::select('shipping_province', $provinces, null, ['class'=>'short', 'id' => 'shipping_province']) !!}
					                    </div>
									</div>
									<div class="row">
									    <div class="col-md-4"></div>
									    <div class="col-md-8">
									    	<select class="short" name="shipping_city" id="shipping_city">
									    		<option value="">Pilih kota</option>
												@if(old('shipping_city'))
													<option value="{{old('shipping_city')}}" selected>{{\App\Models\City::where('city_id',old('city'))->first()->city_name}}</option>
												@endif
									    	</select>
					                    </div>
									</div>
									<div class="row">
									    <div class="col-md-4"></div>
									    <div class="col-md-8">
					                        <select class="short" name="shipping_area" id="shipping_area">
									    		<option value="">Pilih area</option>
												@if(old('shipping_area'))
													<option value="{{old('shipping_area')}}" selected>{{\App\Models\Location\Area::find(old('shipping_area'))->name}}</option>
												@endif
									    	</select>
					                    </div>
									</div>
									<div class="row">
									    <div class="col-md-4"></div>
									    <div class="col-md-8">
									        <input type="text" name="shipping_postal_code" placeholder="Kode pos" value="{{ old('postal_code') }}"/>
									    </div>
									</div>
								</div>
								<div class="row">
			                        <div class="col-md-4">
			                            <label for="middle-label" class="text-left middle">Telepon Perusahaan<span class="text-red" >*</span></label>
			                        </div>
			                        <div class="col-md-8">
			                            <div class="input-group">
			                                <span class="input-group-label">+62</span>
			                                <input class="input-group-field required" type="text" name="company_phone" value="{{ old('company_phone') }}"/>
			                            </div>
			                        </div>
			                    </div>
								<div class="row">
								    <div class="col-md-4">
								        <label for="middle-label" class="text-left middle">Email Perusahaan<span class="text-red" >*</span></label>
								    </div>
								    <div class="col-md-8">
								        <input type="text" name="company_email" placeholder="" class="required" value="{{ old('company_email') }}"/>
								    </div>
								</div>
								<div class="row">
								    <div class="col-md-4">
								        <label for="middle-label" class="text-left middle">Website</label>
								    </div>
								    <div class="col-md-8">
								        <input type="text" name="website" placeholder="" value="{{ old('website') }}"/>
								    </div>
								</div>
								<div class="row">
								    <div class="col-md-4">
								        <label for="middle-label" class="text-left middle">Kategori<span class="text-red" >*</span></label>
								    </div>
								    <div class="col-md-8">
				                        <select class="short required" name="category">
				                        	@foreach($categorys as $category)
				                            <option value="{{$category->id}}">{{$category->category_name}}</option>
				                            @endforeach
				                        </select>
				                    </div>
								</div>

			                    <div class="row">
			                        <div class="col-md-4">

			                        </div>
			                        <div class="col-md-8">
			                            <label for="middle-label" class="text-left middle"><span class="text-red" >*</span>) all fields with red dot are mandatory</label>
			                        </div>
			                    </div>

							</div>
							<div class="tab">
								<div class="title-form-step">
									<h4>Langkah 2 / 3</h4>
									<h3>Informasi PIC</h3>
								</div>
								<div class="row">
								    <div class="col-md-4">
								        <label for="middle-label" class="text-left middle">Jabatan<span class="text-red" >*</span></label>
								    </div>
								    <div class="col-md-8">
								        <select class="short required" name="title">
				                            <option value="m">Mr</option>
				                            <option value="fs">Mrs</option>
				                            <option value="f">Ms</option>
				                        </select>
								    </div>
								</div>
								<div class="row">
								    <div class="col-md-4">
								        <label for="middle-label" class="text-left middle">Nama<span class="text-red" >*</span></label>
								    </div>

								    <div class="col-md-8">
								        <input type="text" class="required" name="pic_name" placeholder="" value="{{ old('pic_name') }}"/>
								    </div>
								</div>
								<div class="row">
								    <div class="col-md-4">
								        <label for="middle-label" class="text-left middle">Email<span class="text-red" >*</span></label>
								    </div>

								    <div class="col-md-8">
								        <input type="text" name="email" class="required" placeholder="" value="{{ old('pic_email') }}"/>
								    </div>
								</div>
								<div class="row">
								    <div class="col-md-4">
								        <label for="middle-label" class="text-left middle">KTP
								    </div>

								    <div class="col-md-8">
								        <input type="file" name="upload_ktp" id="upload_ktp"/>
								    </div>
								</div>

								<div class="row">
			                        <div class="col-md-4">
			                            <label for="middle-label" class="text-left middle">Nomor Handphone<span class="text-red" >*</span></label>
			                        </div>
			                        <div class="col-md-8">
			                            <div class="input-group">
			                                <span class="input-group-label">+62</span>
			                                <input class="input-group-field required" type="text" name="phone_number" value="{{ old('company_phone') }}"/>
			                            </div>
			                        </div>
			                    </div>


								<div class="row">
								    <div class="col-md-4">
								        <label for="middle-label" class="text-left middle">Password<span class="text-red" >*</span></label>
								    </div>

								    <div class="col-md-8">
								        <input type="password" class="required" name="password" placeholder="" value="{{ old('password') }}"/>
								    </div>
								</div>
								<div class="row">
								    <div class="col-md-4">
								        <label for="middle-label" class="text-left middle">Konfirmasi Password<span class="text-red" >*</span></label>
								    </div>

								    <div class="col-md-8">
								        <input type="password" name="confirm_password" class="required" placeholder="" value="{{ old('confirm_password') }}"/>
								    </div>
								</div>

			                    <div class="row">
			                        <div class="col-md-4">

			                        </div>
			                        <div class="col-md-8">
			                            <label for="middle-label" class="text-left middle"><span class="text-red" >*</span>) all fields with red dot are mandatory</label>
			                        </div>
			                    </div>
							</div>
							<div class="tab">
								<div class="title-form-step">
									<h4>Langkah 3 / 3</h4>
									<h3>Informasi Pembayaran</h3>
								</div>
								<div class="row">
								    <div class="col-md-4">
								        <label for="middle-label" class="text-left middle">NPWP Perusahaan<span class="text-red" >*</span></label>
								    </div>

								    <div class="col-md-8">
								        <input type="text" name="company_npwp" placeholder="" value="{{ old('company_npwp') }}"/>
								    </div>
								</div>
								<div class="row">
								    <div class="col-md-4">
								        <label for="middle-label" class="text-left middle">Upload NPWP
								    </div>

								    <div class="col-md-8">
								        <input type="file" name="upload_npwp" id="upload_npwp" />
								    </div>
								</div>

								<div class="row">
								    <div class="col-md-4">
								        <label for="middle-label" class="text-left middle">Metode Pembayaran<span class="text-red" >*</span></label>
								    </div>
								    <div class="col-md-8">
								    	<div class="box">
											<input class="checkbox" type="checkbox" name="payment_method[]" value="cod" checked disabled>
											<label for="checkbox" class="agree">Cash on Delivery</label>
										</div>
								    </div>
								</div>
								<div class="row">
								    <div class="col-md-4"></div>
								    <div class="col-md-8">
								    	<div class="box">
											<input  class="checkbox" type="checkbox" name="payment_method[]" value="transfer"  checked disabled>
											<label for="checkbox" class="agree">Bank Transfer</label>
										</div>
								    </div>
								</div>
								<div class="row">
								    <div class="col-md-5"></div>
								    <div class="col-md-7">
								    	<div class="logo-payment">
								    		<div><img src="{!! asset(null) !!}assets/images/bank/panin.png" class="payment"></div>
								    		<div><img src="{!! asset(null) !!}assets/images/bank/bca.png" class="payment"></div>
								    	</div>
								    </div>
								</div>
								<div class="row">
								    <div class="col-md-4"></div>
								    <div class="col-md-8">
								    	<div class="box">
											<input  class="checkbox" type="checkbox" id="tempo_check" name="payment_method[]" value="tempo">
											<label for="checkbox" class="agree">TEMPO</label>
										</div>
								    </div>
								</div>
								<div class="row">
								    <div class="col-md-4"></div>
								    <div class="col-md-8">
								    	
								    </div>
								</div>
								<div class="row">
								    <div class="col-md-4"></div>
								    <div class="col-md-8">
			                            <label for="middle-label" class="text-left middle"><span class="text-red">*</span>) all fields with red dot are mandatory</label>
			                        </div>
								</div>
								<div class="row">
			                        <div class="col-md-9 col-md-offset-4">
			                            <div class="box">
			                                <input id="checkbox1" class="checkbox" type="checkbox" required="" name="agree">
			                                <label for="checkbox1" class="agree">By submitting this form, I confirm that I have read and agree to Seafer Grosir's Term &amp; Conditions and Privacy Policy
			                                </label>
			                            </div>
			                        </div>
			                    </div>
							</div>
							<div style="overflow:auto;">
								<div style="float:right;">
									<button class="btn btn-success nextBtn btn-lg pull-left" type="button" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
									<button class="btn btn-primary nextBtn btn-lg pull-right" type="button" id="nextBtn" onclick="nextPrev(1)">Next</button>
									<button class="btn btn-primary nextBtn btn-lg pull-right" type="submit" id="button_submit" >Submit</button>
								</div>
							</div>
							<!-- Circles which indicates the steps of the form: -->
							<div style="text-align:center;margin-top:40px;">
								<span class="step"></span>
								<span class="step"></span>
								<span class="step"></span>
							</div>
						</div>
					</div>
				</form>

	            <div class="col-md-3 no-padding center hide-768">
	                <img class="blue" src="assets/images/sign-up-2.png">
	            </div>
	        </div>
		</div>
	</div>
@endsection

@section('scripts')
<script src="assets/plugin/bootstrap/js/jquery.bootstrap-responsive-tabs.min.js"></script>
<script src="assets/plugin/fancybox/js/jquery.fancybox.min.js"></script>
<script>
    var requiredCheckboxes = $(':checkbox[required]');

    requiredCheckboxes.change(function(){
        if(requiredCheckboxes.is(':checked')) {
            requiredCheckboxes.removeAttr('required');
        }
        else {
            requiredCheckboxes.attr('required', 'required');
        }
    });
</script>

<!-- AJAX province -->
<script>

	// $('#userForm').submit(function(e){
	//
	//
	// 	console.log(npwp);
	//
	// 	if(npwp > 2 && ktp > 2){
	//
	// 		e.preventDefault();
	// 	}
	// });


	$('#select_province').on('change', function(){
        var province_id = $(this).val();
        $.ajax({
            type: 'get',
            url: '{{ url('user/ajax-cities-by-province') }}/' + province_id
        }).done(function(result){
            result = jQuery.parseJSON(result);
            if(result.status == 'success') {
                $('#select_city').html('');
                $.each(result.cities, function(k, v) {
                    $('#select_city').append('<option value="'+k+'">'+v+'</option>');
                });


            } else {
                alert(result.message);
            }
        })
    });

    $('#select_city').on('change', function(){
        var city_id = $(this).val();
        $.ajax({
            type: 'get',
            url: '{{ url('user/ajax-areas-by-city/') }}/' + city_id
        }).done(function(result){
            result = jQuery.parseJSON(result);
            if(result.status == 'success') {
                $('#select_area').html('');
                $.each(result.areas, function(k, v) {
                    $('#select_area').append('<option value="'+k+'">'+v+'</option>');
                });

            } else {
                alert(result.message);
            }
        })

    });


    $('#shipping_province').on('change', function(){
        var province_id = $(this).val();
        $.ajax({
            type: 'get',
            url: '{{ url('user/ajax-cities-by-province') }}/' + province_id
        }).done(function(result){
            result = jQuery.parseJSON(result);
            if(result.status == 'success') {
                $('#shipping_city').html('');
                $.each(result.cities, function(k, v) {
                    $('#shipping_city').append('<option value="'+k+'">'+v+'</option>');
                });

            } else {
                alert(result.message);
            }
        })
    });
    $('#shipping_city').on('change', function(){
        var city_id = $(this).val();
        $.ajax({
            type: 'get',
            url: '{{ url('user/ajax-areas-by-city/') }}/' + city_id
        }).done(function(result){
            result = jQuery.parseJSON(result);
            if(result.status == 'success') {
                $('#shipping_area').html('');
                $.each(result.areas, function(k, v) {
                    $('#shipping_area').append('<option value="'+k+'">'+v+'</option>');
                });

            } else {
                alert(result.message);
            }
        })

    });
</script>

<!-- Form step -->
<script type="text/javascript">
	var currentTab = 0; // Current tab is set to be the first tab (0)
	showTab(currentTab); // Display the crurrent tab

	function showTab(n) {
		// This function will display the specified tab of the form...
		var x = document.getElementsByClassName("tab");
		x[n].style.display = "block";
		//... and fix the Previous/Next buttons:
		if (n == 0) {
			document.getElementById("prevBtn").style.display = "none";
			$("#button_submit").hide();
			$("#nextBtn").show();
		} else {
			document.getElementById("prevBtn").style.display = "inline";
			$("#button_submit").hide();
			$("#nextBtn").show();
		}
		if (n == (x.length - 1)) {

			$("#nextBtn").hide();
			$("#button_submit").show();
			//document.getElementById("nextBtn").innerHTML = "Submit";
			$('#button_submit').on('click', function () {
				checkFileSize();
				//$(this).parents("form:first").submit();
			});
		} else {
			document.getElementById("nextBtn").innerHTML = "Next";

		}
		//... and run a function that will display the correct step indicator:
		fixStepIndicator(n)
	}

	function checkFileSize(){
		let npwp = document.getElementById("upload_npwp").files[0].size / 1000 / 1000;
		let ktp = document.getElementById("upload_ktp").files[0].size / 1000 / 1000;
		if(npwp > 2 || ktp > 2){
			alert('KTP or NPWP File size cannot be over 2 MB');

			return false;
		}else{
			return true;
		}
	}

	function nextPrev(n) {
		// This function will figure out which tab to display
		var x = document.getElementsByClassName("tab");
		// Exit the function if any field in the current tab is invalid:
		if (n == 1 && !validateForm()) return false;
		// Hide the current tab:
		x[currentTab].style.display = "none";
		// Increase or decrease the current tab by 1:
		currentTab = currentTab + n;


		// if you have reached the end of the form...
		if (currentTab >= x.length) {
		// ... the form gets submitted:
			document.getElementById("regForm").submit();
			return false;
		}
		// Otherwise, display the correct tab:
		showTab(currentTab);
	}

	function validateForm() {
		// This function deals with validation of the form fields
		var x, y, i, valid = true;
		x = document.getElementsByClassName("tab");
		y = x[currentTab].getElementsByClassName("required");
		// A loop that checks every input field in the current tab:
		for (i = 0; i < y.length; i++) {
			// If a field is empty...
			if (y[i].value == "") {
				// add an "invalid" class to the field:
				y[i].className += " invalid";
				// and set the current valid status to false
				valid = false;
			}
		}
		// If the valid status is true, mark the step as finished and valid:
		if (valid) {
			document.getElementsByClassName("step")[currentTab].className += " finish";
		}
		return valid; // return the valid status
	}

	function fixStepIndicator(n) {
		// This function removes the "active" class of all steps...
		var i, x = document.getElementsByClassName("step");
		for (i = 0; i < x.length; i++) {
			x[i].className = x[i].className.replace(" active", "");
		}
		//... and adds the "active" class on the current step:
		x[n].className += " active";
	}
</script>



<!-- Radio validator -->
<script type="text/javascript">
	$(document).ready(function () {
	    $('input[type="radio"]').click(function () {
	        if ($(this).attr("value") == "another_address") {
	            $(".another_address").show();
	        }
	        if ($(this).attr("value") == "billing_address") {
	            $(".another_address").hide();
	        }
	    });
	});
</script>

<!--upload checked-->
<script type="text/javascript">
	$('input[type="checkbox"]').click(function () {
		if ($(this).attr("value") == "tempo") {
	      if(document.getElementById("upload_ktp").files.length == 0){
	      	alert("Please upload KTP for tempo payment!");
	      	$( "#tempo_check" ).prop( "checked", false );
	      }
	      else if(document.getElementById("upload_npwp").files.length == 0){
	      	alert("Mohon Upload NPWP for tempo payment!");
	      	$( "#tempo_check" ).prop( "checked", false );
	      }
	      else{
	      	$("#tempocondition").show();
	      }
	    }
	});
</script>
@endsection
