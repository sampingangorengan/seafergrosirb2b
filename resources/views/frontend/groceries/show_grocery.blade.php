@extends('frontend.layout.master')

@section('title', App\Page::createTitle( $item->name ))

@section('customcss')
    <link rel='stylesheet' href='assets/plugin/bootstrap/css/bootstrap-responsive-tabs.css'>
    <link rel="stylesheet" type="text/css" href="assets/plugin/fancybox/css/jquery.fancybox.min.css">
@endsection

@section('content')
	<div class="main-content">
	<div class="container">
		<div class="row hide-320">
			<div class="col-md-12">
				<span class="text-grey equal margin-bottom-35">
				  <a class="text-grey" href="{{ url('/') }}"><b>HOME</b></a> /
				  <a class="text-grey" href="#"><b>SEAFER GROCER </b></a>
				</span>
			</div>
		</div>
		<div class="row">
			<div class="col-md-5 col-sm-5">
				<div class="item-box">
                    <div class="item xlarge">
						<a data-fancybox="gallery" href="{{ Groceries::getFirstImageUrl($item->id) }}" class="zoom">
							<img src="{{ Groceries::getFirstImageUrl($item->id) }}" class="image-large">
						</a>
					</div>
					<div class="thumbnail-box">
						<ul>
							@foreach ($item->images()->get() as $img)
							<li>
								<div class="item">
									<a data-fancybox="gallery" href="{{ asset('contents/'.$img->file_name) }}">
										<img src="{{ asset('contents/'.$img->file_name) }}" class="small-thumbnail">
									</a>
								</div>
                            </li>
							@endforeach
                            @if( $item->video->count() > 0)
							<li>
								<div class="item">
									<a data-fancybox="gallery" href="{{ asset('videogroceries/'.$item->getFirstVideo($item->id)->file_name) }}">
										<img src="{{ asset('assets/images/default_video_thumbnail.png') }}" class="small-thumbnail">
									</a>
								</div>
							</li>
							@endif
						</ul>
					</div>
				</div>
			</div>
			<div class="col-md-7 col-sm-7">
				<div class="detail detail-cart">
					<h6 class="subtitle large">{{ $item->name }}, {{ $item->value_per_package }}{{ $item->metric_id != 0 ? $item->metric->abbreviation : 'metric unknown' }}</h6>
                    <br>
                    {{-- @if($item->discount_id != 0)
                            <span class="price old-price text-right semi-bold">{{ Money::display($item->price) }}</span>
                            @if($item->discount->discount_type == 'percent')
                                @if( $item->price - (($item->price * $item->discount->discount_value)/100) > 0 )
                                <span class="price text-red semi-bold">{{ Money::display( $item->price - (($item->price * $item->discount->discount_value)/100) ) }}</span>
                                @else
                                <span class="price text-red semi-bold">{{ Money::display( 0 ) }}</span>
                                @endif
                            @else
                                @if( $item->price - $item->discount->discount_value > 0 )
                                <span class="price text-red semi-bold">{{ Money::display( $item->price - $item->discount->discount_value ) }}</span>
                                @else
                                <span class="price text-red semi-bold">{{ Money::display( 0 ) }}</span>
                                @endif
                            @endif
                        @else
                            <span class="price text-red semi-bold">{{ Money::display($item->price) }}</span>
                        @endif --}}


                        @if($item->discount_id != 0)
                            <span class="before-discount price semi-bold text-red">
                                @if($item->range_price)
                                {{Money::display($item->range_price)}} -
                                @endif
                                {{Money::display($item->price) }}

                            </span>
                            @if($item->discount->discount_type == 'percent')
                                @if($item->price - (($item->price * $item->discount->discount_value)/100) > 0)
                                <span class="after-discount price semi-bold">
                                    @if($item->range_price)
                                        {{Money::display($item->range_price - (($item->price * $item->discount->discount_value)/100))}} -
                                    @endif

                                    {{Money::display($item->price - (($item->price * $item->discount->discount_value)/100)) }}
                                </span><br>
                                @else
                                <span class="after-discount price semi-bold"> {{ Money::display( 0 )}}</span><br>
                                @endif
                            @else
                                @if( ($item->price - $item->discount->discount_value) > 0)
                                <span class="after-discount price semi-bold">
                                    @if($item->range_price)
                                        {{Money::display($item->range_price - $item->discount->discount_value)}} -
                                    @endif

                                    {{ Money::display($item->price - $item->discount->discount_value ) }}

                                </span><br>
                                @else
                                    <span class="after-discount price semi-bold"> {{ Money::display( 0 ) }}</span><br>
                                @endif
                            @endif
                        @else
                        <span class="after-discount price text-red semi-bold">
                            @if($item->range_price)
                            {{Money::display($item->range_price)}} -
                            @endif
                            {{Money::display($item->price) }}
                        </span><br>
                        @endif



						<div class="counter independent">
                            <!-- <button class="minus">-</button> -->
                            <input id="item_id" type="hidden" value="{{ $item->id }}" />
                            <input id="item_sku" type="hidden" value="{{ $item->sku ? $item->sku : 'No SKU added' }}" />
                            <input id="item_name" type="hidden" value="{{ $item->name }}" />
                            <input id="item_qty" type="hidden" class="totalx" name="" value="1">
                            @if($item->metric)
                                <input class="item_metric" type="hidden" value="{{ $item->value_per_package.' '.$item->metric->abbreviation }}" />
                            @else
                                <input class="item_metric" type="hidden" value="{{ $item->value_per_package.' --metric not added--' }}" />
                            @endif
                            @if($item->discount_id != 0)
                                @if($item->discount->discount_type == 'percent')
                                    @if($item->price - (($item->price * $item->discount->discount_value)/100))
                                        <input class="item_price" type="hidden" value="{{ $item->price - (($item->price * $item->discount->discount_value)/100) }}" />
                                    @else
                                        <input class="item_price" type="hidden" value="0" />
                                    @endif
                                @else
                                    @if($item->price - $item->discount->discount_value)
                                        <input class="item_price" type="hidden" value="{{ $item->price - $item->discount->discount_value }}" />
                                    @else
                                        <input class="item_price" type="hidden" value="0" />
                                    @endif
                                @endif
                            @else
                                <input class="item_price" type="hidden" value="{{ $item->price }}" />
                            @endif
                            <input class="item_image" type="hidden"  name="item_image" value="{{ Groceries::getFirstImageUrl($item->id) }}">
                            <!-- <button class="plus">+</button> -->
                        </div>
					<div class="add-to-cart-box">

						@if($item->stock > 0)

						{{-- <div class="row">
							<div class="col-md-6 col-sm-6 col-xs-6" id="item_add_to_cart_wrap">
								<button class="btn btn-primary" id="item_add_to_cart" style="width:80%;font-size:15px;">ADD TO CART</button>
                            </div>
                        </div> --}}

                        <div class="row">


                        <div class="procat control-cart-{{ $item->id }} col-md-6 col-sm-12 col-xs-12">
                            <?php
                                $cart_items = Carte::contents();
                                $is_there = false;
                                $identifier = '';

                                foreach($cart_items as $keys=>$prod){

                                    if($item->id == $prod->id){
                                        $is_there = true;
                                        $identifier = $keys;
                                        $cart_item = $prod;
                                        break;
                                    }
                                }
                            ?>

                            <div class="atc-initiate">
                                <input class="item_id" type="hidden" value="{{ $item->id }}" />
                                <input class="item_sku" type="hidden" value="{{ $item->sku ? $item->sku : 'No SKU added' }}" />
                                <input class="item_name" type="hidden" value="{{ $item->name }}" />

                                @if($item->discount_id != 0)
                                    @if($item->discount->discount_type == 'percent')
                                        @if($item->price - (($item->price * $item->discount->discount_value)/100))
                                        <input class="item_price" type="hidden" value="{{ $item->price - (($item->price * $item->discount->discount_value)/100) }}" />
                                        @else
                                        <input class="item_price" type="hidden" value="0" />
                                        @endif
                                    @else
                                        @if($item->price - $item->discount->discount_value)
                                        <input class="item_price" type="hidden" value="{{ $item->price - $item->discount->discount_value }}" />
                                        @else
                                        <input class="item_price" type="hidden" value="0" />
                                        @endif
                                    @endif
                                @else
                                    <input class="item_price" type="hidden" value="{{ $item->price }}" />
                                @endif

                                {{--@if($item->metric)
                                    <input class="item_metric" type="hidden" value="{{ $item->value_per_package.' '.$item->metric->abbreviation }}" />
                                @else
                                    <input class="item_metric" type="hidden" value="{{ $item->value_per_package.' --metric not added--' }}" />
                                @endif--}}
                                <input class="item_image" type="hidden"  name="" value="{{ Groceries::getFirstImageUrl($item->id) }}">

                            </div>
                            @if(Carte::find($item->id))
                                <?php $cart = Carte::find($item->id);?>
                                <div class="qty-change" style="display: block;" id="qty-{{$cart->identifier}}">
                                    <div class="counter independent" style="padding:10px;">
                                        <button class="btn minus decrease_cart_item_qty_out_pop">-</button>
                                        <input class="item_identifier" value="{{$cart->identifier}}" type="hidden">
                                        <input class="totalx item_qty" name="" value="{{$cart->quantity}}" type="text">
                                        <button class="btn plus increase_cart_item_qty_out_pop">+</button>
                                    </div>
                                </div>
                            @elseif($item->stock > 0)
                                <button class="button add-to-cart-btn list_add_to_cart" style="margin:0px;">ADD TO CART</button>
                            @else
                                <button class="button add-to-cart-btn" style="color:white;background:#ff0000;" disabled>SOLD OUT</button>
                            @endif


                        </div>
                    </div>




                        <br>
                        <div class="row">
                            <div class="col-md-6 col-sm-12 col-xs-12" >
                                @if($item->isFavourites() != 1)
                                    <button class="btn btn-aneh list_add_to_favourite toggle-favourite" data-g-id="{{ $item->id }}" data-status="0" style="float:left;width:100%;font-size:15px;">ADD TO FAVOURITES</button>
                                @else
                                    <button class="btn btn-aneh remove-from-favourites-btn list_remove_from_favourite toggle-favourite" data-status="1" data-g-id="{{ $item->id }}" style="float:left;width:100%;">REMOVE FROM FAVOURITES</button>
                                @endif


                            </div>
                        </div>
						<span class="price-large"></span>
						@else
						<div class="row">
							<div class="col-md-6 col-sm-6 col-xs-6" id="item_add_to_cart_wrap">
								<button class="button btn-aneh" id="item_add_to_cart" disabled="" type="button" style="opacity:0.25;color:white;background:#ff0000;">SOLD OUT</button>
							</div>
						</div>
						<span class="price-large"></span>
						@endif
						<br>
		                <div class="product-info">
							@if($categories = $item->category)
								<p>Kategori :<strong>
								@foreach ($categories as $category)
									{{Category::find($category->parent_id)->name}}
									@if($categories[count($categories) - 1] != $category)
										,
									@endif
								@endforeach
								</strong></p>
							@endif
							<p>
								Berat : <strong>{{ $item->value_per_package }}{{ $item->metric_id != 0 ? $item->metric->abbreviation : 'metric unknown' }}</strong>
							</p>
		                  <p>Negara Asal : <strong>{{$item->country->name}} </strong></p>
		                  @if($item->country->id == 107)
		                  	<p>Domestic/Import : <strong>Domestic</strong></p>
		                  @else
							<p>Domestic/Import : <strong>Import</strong></p>
		                  @endif

		                  @if($item->cooking_advice == null)
		                  <p>Saran Masak : <strong>-</strong></p>
		                  @else
							{{-- <p>Cooking Instruction : @foreach($item->cookingAdvice as $advice)</strong>{{ $advice->name }}</strong>, &nbsp; @endforeach</p> --}}
							<p>Cooking Instruction : <strong>{{ $item->cooking_advice }}</strong></p>
		                  @endif
						  	<p>Hasil laut/ternak :
							@if($item->fresh_sea_water == 0)
							<strong>Non Sea Caught/ Farmed</strong>
							@elseif($item->fresh_sea_water == 1)
							<strong>Sea Caught</strong>
							@elseif($item->fresh_sea_water == 2)
							<strong>Farmed</strong>
							@endif
							</p>
						  	<p>Product Nature :
								@if($item->nature)
									<strong>{{ $item->nature }}</strong>
								@else
									<strong>-</strong>
								@endif
							</p>
						  	<p>Product State :
								@if($item->state)
									<strong>{{ $item->state }}</strong>
								@else
									<strong>-</strong>
								@endif</p>
		                </div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="panel-group panel-cart" id="accordion">
								<div class="panel panel-default">
									<div class="panel-heading">
									  <h4 class="panel-title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true">
										  Expiry Date
										</a>
									  </h4>
									</div>
									<div id="collapse1" class="panel-collapse collapse ">
									  <div class="panel-body">
										{{ strtoupper(LocalizedCarbon::parse($item->expiry_date)->format("d m Y")) }}
									  </div>
									</div>
								  </div>
								  <div class="panel panel-default">
									<div class="panel-heading">
									  <h4 class="panel-title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="true">
										  Description
										</a>
									  </h4>
									</div>
									<div id="collapse2" class="panel-collapse collapse " >
									  <div class="panel-body">
										{!! nl2br($item->description) !!}
									  </div>
									</div>
								  </div>
								  <div class="panel panel-default">
									<div class="panel-heading">
									  <h4 class="panel-title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="true">
										  Nutrition
										</a>
									  </h4>
									</div>
									<div id="collapse3" class="panel-collapse collapse " >
									  <div class="panel-body">
										@if($item->nutrients == 'No Data' || substr($item->nutrients, 0, 3) == '000')
										Not Available
										@else
										{!! nl2br($item->nutrients) !!}
										@endif
									  </div>
									</div>
								  </div>
								  <div class="panel panel-default">
									<div class="panel-heading">
									  <h4 class="panel-title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="true">
										  Ingredients
										</a>
									  </h4>
									</div>
									<div id="collapse4" class="panel-collapse collapse " >
									  <div class="panel-body">
										{!! nl2br($item->ingredients) !!}
									  </div>
									</div>
								  </div>
                                  <div class="panel panel-default">
                                      <div class="panel-heading">
                                        <h4 class="panel-title">
                                          <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse5" aria-expanded="true">
                                            Availability
                                          </a>
                                        </h4>
                                      </div>
                                      <div id="collapse5" class="panel-collapse collapse ">
                                        <div class="panel-body">
                                          {!! nl2br($item->availabity_note) !!}
                                        </div>
                                      </div>
                                    </div>
							</div>
						</div>
					</div>
                </div>
			</div>

		</div>
		<div class="row">
			<div class="col-md-12">
				@if(count($posters) > 0)
					@foreach($posters as $pst)
					<img src="/groceries_poster/{{ $pst->file }}" alt="" style="width:100%;">
					<br>
					@endforeach
				@endif
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="flash-message">
					@foreach (['danger', 'warning', 'success', 'info'] as $msg)
                        @if(Session::has('alert-' . $msg))

                            <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                        @endif
                    @endforeach
				</div>
				<div class="customer-and-reviews-box ">
					<ul class="nav nav-tabs responsive-tabs">

						<li class="tabs-title"><a href="#panel1" data-toggle="tab">REVIEWS</a></li>
					</ul>
					<div class="tab-content top-line-blue">


						<div class="tab-pane fade in active" id="panel2">
							<div class="row">
								<div class="col-md-6 col-sm-6">
									<ul class="rating list-inline">
										@if($reviews->count() >0)
                                            @for($i = 0; $i < $reviews->avg('rate'); $i++)
                                            <li><img src="{!! asset(null) !!}assets/images/star-fill.png"></li>
                                            @endfor
                                            @else
                                            <li><img src="{!! asset(null) !!}assets/images/star-border.png"></li>
                                            <li><img src="{!! asset(null) !!}assets/images/star-border.png"></li>
                                            <li><img src="{!! asset(null) !!}assets/images/star-border.png"></li>
                                            <li><img src="{!! asset(null) !!}assets/images/star-border.png"></li>
                                            <li><img src="{!! asset(null) !!}assets/images/star-border.png"></li>
                                            @endif
									</ul>
									@if($reviews->count() >0)
                                    <span>{{ $reviews->avg('rate') }} out of 5 stars</span><br><br>
                                    @else
                                        <span>This product has not been rated</span><br><br>
                                    @endif

									<div class="chart-box">
										<table class="chart">
											<tbody>
											<tr>
												<td>
													<p class="percentage">5 Stars</p>
												</td>
												<td class="chart">
													<div class="barchart">
														<span class="meter" style="width: {{ $reviews->count() > 0 ? ($five_star/$reviews->count())*100 : 0 }}%"></span>
													</div>
												</td>
												<td>
													<p class="percentage">{{ $reviews->count() > 0 ? ($five_star/$reviews->count())*100 : 0 }}%</p>
												</td>
											</tr>
											<tr>
												<td>
													<p class="percentage">4 Stars</p>
												</td>
												<td class="chart">
													<div class="barchart">
														<span class="meter" style="width: {{ $reviews->count() > 0 ? ($four_star/$reviews->count())*100 : 0 }}%"></span>
													</div>
												</td>
												<td>
													<p class="percentage">{{ $reviews->count() > 0 ? ($four_star/$reviews->count())*100 : 0 }}%</p>
												</td>
											</tr>
											<tr>
												<td>
													<p class="percentage">3 Stars</p>
												</td>
												<td class="chart">
													<div class="barchart">
														<span class="meter" style="width: {{ $reviews->count() > 0 ? ($three_star/$reviews->count())*100 : 0 }}%"></span>
													</div>
												</td>
												<td>
													<p class="percentage">{{ $reviews->count() > 0 ? ($three_star/$reviews->count())*100 : 0 }}%</p>
												</td>
											</tr>
											<tr>
												<td>
													<p class="percentage">2 Stars</p>
												</td>
												<td class="chart">
													<div class="barchart">
														<span class="meter" style="width: {{ $reviews->count() > 0 ? ($two_star/$reviews->count())*100 : 0 }}%"></span>
													</div>
												</td>
												<td>
													<p class="percentage">{{ $reviews->count() > 0 ? ($two_star/$reviews->count())*100 : 0 }}%</p>
												</td>
											</tr>
											<tr>
												<td>
													<p class="percentage">1 Star</p>
												</td>
												<td class="chart">
													<div class="barchart">
														<span class="meter" style="width: {{ $reviews->count() > 0 ? ($one_star/$reviews->count())*100 : 0 }}%"></span>
													</div>
												</td>
												<td>
													<p class="percentage">{{ $reviews->count() > 0 ? ($one_star/$reviews->count())*100 : 0 }}%</p>
												</td>
											</tr>
											</tbody>
										</table>
									</div>
								</div>
								<div class="col-md-6">
									<div class="customer-qa-box">
										@if($reviews->count() > 0)
										<div id="slider-rating" class="carousel slide">
                                            <ul class="carousel-inner question-container-content">
                                                @foreach($reviews as $key=>$review)
                                                	@if($key == 0 || ($key % 4) == 0)
                                                    <li class="row active item">
                                                        <div class="customer-qa-box">
                                                    @endif
	                                                        <div class="qa-box">
	                                                            <table>
	                                                                <tr>
	                                                                    <td>
	                                                                        <div class="rating-box">
	                                                                            <ul class="rating">
	                                                                                @for($i = 0; $i < $review->rate; $i++)
	                                                                                <li><img src="{!! asset(null) !!}assets/images/star-color.png"></li>
	                                                                                @endfor
	                                                                            </ul>
	                                                                        </div>
	                                                                    </td>
	                                                                    <td><b>{{ $review->title }}</b></td>
	                                                                </tr>
	                                                                <tr>
	                                                                    <td colspan="2"><span class="font-11">By {{ $review->user->name ?? "-" }} on {{ LocalizedCarbon::parse($review->created_at)->format('F, d Y') }}</span></td>
	                                                                </tr>
	                                                                <tr>
	                                                                    <td colspan="2"><span class="font-11">{{ $review->review }}</span></td>
	                                                                </tr>
	                                                            </table>
	                                                        </div>
	                                                @if($key % 3 == 0 || $key == $reviews->count())
	                                                    </div>
	                                                </li>
	                                                @endif
	                                                @endforeach
											</ul>
											<ul class="nav-slide">
                                                <a class="carousel-control left pull-left" href="#slider-rating"  data-slide="prev"><span class="show-for-sr"></span><img src="{!! asset("assets/images/ico-arrows-L.png") !!}"></a>
                                                <a class="carousel-control right pull-right" href="#slider-rating"   data-slide="next"><span class="show-for-sr"></span><img src="{!! asset("assets/images/ico-arrows-R.png") !!}"></a>
                                            </ul>
										</div>
										@endif
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="recomendation-box top-line-blue ">
					<label>YOU MAY ALSO LIKE</label>
					<div class="row">
						@foreach ($recommendation as $item)
						<div class="col-md-2 col-sm-3 col-xs-4 padding-320">
							<a href="{{ url('groceries/'.$item->id.'/'.str_slug($item->name)) }}">
								<div class="item" style="background-image: url({{ Groceries::getFirstImageUrl($item->id) }}); margin:auto"></div>
							</a>
							<a href="{{ url('groceries/'.$item->id.'/'.str_slug($item->name)) }}">
								<span class="text-blue" style="padding-top: 10px;">{{ $item->name }}, {{ $item->value_per_package }}{{ $item->metric_id != 0 ? $item->metric->abbreviation : 'metric unknown' }}</span>
							</a>
							<span>{{ Money::display($item->price) }}</span>
						</div>
						@endforeach

					</div>
				</div>

				<div class="recomendation-box top-line-blue bottom-line-blue">
					<label>BRAND RELATED</label>
					<div class="row">
						@if($brand_related->count() > 0)
                        @foreach ($brand_related as $item)
						<div class="col-md-2 col-sm-3 col-xs-4 padding-320">
							<a href="{{ url('groceries/'.$item->id.'/'.str_slug($item->name)) }}">
								<div class="item" style="background-image: url({{ Groceries::getFirstImageUrl($item->id) }}); margin:auto;"></div>
							</a>
							<a href="{{ url('groceries/'.$item->id.'/'.str_slug($item->name)) }}">
								<span class="text-blue" style="padding-top: 10px;">{{ $item->name }}, {{ $item->value_per_package }}{{ $item->metric_id != 0 ? $item->metric->abbreviation : 'metric unknown' }}</span>
							</a>
							<span>{{ Money::display($item->price) }}</span>
						</div>
						@endforeach
                        @else
                        <div class="col-md-12">
							<p>No more product from this brand to show</p>
						</div>
                        @endif

					</div>
				</div>


				<div class="recomendation-box">
					<label>RECENTLY VIEWED</label>
					<div class="row">

						@if($recent->count() > 0)
                            @foreach ($recent as $item)
                            @if(0 !== $item->groceries_id && null !== $item->grocery)
                            <div class="col-md-2 col-sm-3 col-xs-4 padding-320">



								<a href="{{ url('groceries/'.$item->grocery->id.'/'.str_slug($item->grocery->name)) }}">

									<div class="item" style="background-image: url({{ Groceries::getFirstImageUrl($item->grocery->id) }}); margin:auto"></div>
								</a>

								<a href="{{ url('groceries/'.$item->grocery->id.'/'.str_slug($item->grocery->name)) }}">
									<span class="text-blue" style="padding-top: 10px;">{{ $item->grocery->name }}, {{ $item->grocery->value_per_package }}{{ $item->grocery->metric_id != 0 ? $item->grocery->metric->abbreviation : 'metric unknown' }}</span>
								</a>
								<span>{{ Money::display($item->grocery->price) }}</span>

							</div>
							@endif
							@endforeach
						@else
						<div class="col-md-12">
							<p>This is your first product visit</p>
						</div>
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="modal-question-login" class="modal fade modal-question-login" role="dialog">
    <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-box">
        <div class="dashed-box">
            <div class="img-box">
                <img src="assets/images/img-maskot.png">
            </div>
            <br>
            <b><h4 class="text-blue center">Hey ! <br>Please login first, so we can follow you up better</h4></b>
        </div>
    </div>
    </div>

    </div>
</div>
@endsection

@section('scripts')
<script src="assets/plugin/bootstrap/js/jquery.bootstrap-responsive-tabs.min.js"></script>

<script>

    var closeKirinBox = function(){
        $('#askKirinForm').remove();
        $('#askKirin').show()
    }

    var drawKirin = function(html){

        $('.question-container ul.question-container-content').html(html);
    }

    $(document).ready(function(){
        /*$('.zoom').zoom();*/

        $('.small-thumbnail').on('click', function(){
/*                alert($(this).attr('src'));*/

            $('.item.xlarge img').attr('src', $(this).attr('src'));
        });
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('input[name="search-question"]').keyup(function(){
            var itid = $('#item_id').val();
            $.ajax({
                type:'post',
                url: "{{ url('kirin/get_questions') }}/" + itid,
                data: {
                    search:$('input[name="search-question"]').val(),
                }
            }).done(function(result){
                drawKirin(result);
            });
        });
        @if(auth()->check())

        var idf = "{{ auth()->user()->id }}";
        $.ajax({
            type:'post',
            url: "{{ url('user/make-history') }}",
            data: {
                identifier: idf,
                type:'uid',
                gid:$('#item_id').val()
            }
        });

        $('.submit-kirin').on('click', function(e){
            if($('textarea[name="posttokirin"]').val() == ''){
                e.preventDefault();
                alert('Please type your question on the text area provided.');
                return false;
            }

            console.log($('textarea[name="posttokirin"]').val());

            var itid = $('#item_id').val();
            $.ajax({
                type:'post',
                url: "{{ url('groceries/question') }}/" + itid,
                data: {
                    groceries_id: parseInt(itid),
                    question:$('textarea[name="posttokirin"]').val()
                }
            }).done(function(result){
                drawKirin(result);
                location.reload();
            });
            e.preventDefault();
        });
        @else
        var idf = "{{ Session::getId() }}";
        $.ajax({
            type:'post',
            url: "{{ url('user/make-history') }}",
            data: {
                identifier: idf,
                type:'ss',
                gid:$('#item_id').val()
            }
        });
        @endif

        $('#askKirin').click(function(){
            @if(auth()->check())
                $('#askKirinForm').show();
                $(this).hide();
            @else
            $('#modal-question-login').modal('toggle');
            @endif
		});

    });

	$('.responsive-tabs').responsiveTabs({
	  accordionOn: ['xs'] // xs, sm, md, lg
	});
</script>
@endsection
