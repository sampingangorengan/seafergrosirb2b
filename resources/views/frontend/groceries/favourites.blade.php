@extends('frontend.layout.master')

@section('title', App\Page::createTitle('Groceries'))

@section('customcss')
    <link rel='stylesheet' href='assets/plugin/bootstrap/css/bootstrap-responsive-tabs.css'>
    <link rel="stylesheet" type="text/css" href="assets/plugin/fancybox/css/jquery.fancybox.min.css">
@endsection

@section('content')
	<div class="main-content">
		<div id="productDetail" class="container">
			<div class="row">
	            <div class="col-md-11 col-centered">
	                <div class="top-bar filter-box top-bar-custome">
						@if(count($favourites_item) >= 1)
	                    <div class="pull-right">
	                        <ul class="nav navbar-nav">
	                            <li class=""><a href="{{url('cart/favourites')}}"><button class="btn btn-lg js-btn-check-out btn-aneh" >Check Out Now </button></a></li>
							</ul>
	                    </div>
					@endif
						<div class="pull-left">
							<ul class="nav">
								<li class="hide-320"><span class=""><b><h3>Favourites Item</h3></b></span></li>
							</ul>
						</div>
						<div class="top-bar-right pull-right hide-320" style="width:42%;text-align:right">
							{{-- <div class="col-md-7 col-sm-9 col-xs-12">
								@include('frontend.layout.pagination', ['paginator' => $groceries])
							</div> --}}
						</div>
					</div>
	                <hr class="blue margin-top-0"></hr>
	            </div>

				<div class="col-md-12 col-sm-12 col-xs-12">

					@if(count($favourites_item) >= 1)

					@foreach($favourites_item as $item)
						<div class="col-md-3 col-sm-4 col-xs-6">
							<div class="item-box item-margin-bottom">
								<div class="item large--2">
									@if($item->discount_id != 0)
									<div class="item-sale">
										<img src="assets/images/ico-sale.png">
									</div>
									@endif

                                    <a href="{{ url('groceries/'.$item->id.'/'.str_slug($item->name)) }}">
										<img src="{{ Groceries::getFirstImageUrl($item->id) }}">
									</a>
								</div>
								<img
									@if (!$item->isFavourites())
										src="assets/images/ico-star-border.png"
									@else
										src="assets/images/ico-star-fill-color.png"
									@endif
								class="favourites add-to-favourites-btn toggle-favourite" data-status="{{($item->isFavourites())? 1 : 0}}" alt=""  data-g-id="{{ $item->id }}" data-page="1"/>

								<a href="{{ url('groceries/'.$item->id.'/'.str_slug($item->name)) }}">
	                                <div class="desc">
	                                    <span><b>{{ $item->name }}, {{ $item->value_per_package }}{{ $item->metric_id != 0 ? $item->metric->abbreviation : 'metric unknown' }}</b></span><br>
	                                </div>
								</a>
								<div class="packing" style="height:40px;">
									{{$item->packing}}
								</div>
								<div class="block-price">

								@if($item->discount_id != 0)
								<span class="before-discount">{{ Money::display($item->price) }}</span>
                                    @if($item->discount->discount_type == 'percent')
                                        @if($item->price - (($item->price * $item->discount->discount_value)/100) >0)
                                        <span class="after-discount"> {{ Money::display( $item->price - (($item->price * $item->discount->discount_value)/100) )}}</span><br>
                                        @else
                                        <span class="after-discount"> {{ Money::display( 0 )}}</span><br>
                                        @endif
                                    @else
                                        @if( ($item->price - $item->discount->discount_value) > 0)
                                        <span class="after-discount"> {{ Money::display( $item->price - $item->discount->discount_value ) }}</span><br>
                                        @else
                                            <span class="after-discount"> {{ Money::display( 0 ) }}</span><br>
                                        @endif
                                    @endif
                                @else
                                <span class="after-discount"> {{ Money::display($item->price)}}</span><br>
								@endif
								</div>
	                            <div class="procat control-cart-{{ $item->pivot->id }}">
                                    <?php
                                        $cart_items = Carte::contents();
                                        $is_there = false;
                                        $identifier = '';

                                        foreach($cart_items as $keys=>$prod){

                                            if($item->id == $prod->id){
                                                $is_there = true;
                                                $identifier = $keys;
                                                $cart_item = $prod;
                                                break;
                                            }
                                        }
                                    ?>

                                    <div class="atc-initiate">
										<input class="item_id" type="hidden" value="{{ $item->pivot->id }}" />
                                        <input class="item_sku" type="hidden" value="{{ $item->sku ? $item->sku : 'No SKU added' }}" />
                                        <input class="item_name" type="hidden" value="{{ $item->name }}" />

                                        @if($item->discount_id != 0)
                                            @if($item->discount->discount_type == 'percent')
                                                @if($item->price - (($item->price * $item->discount->discount_value)/100))
                                                <input class="item_price" type="hidden" value="{{ $item->price - (($item->price * $item->discount->discount_value)/100) }}" />
                                                @else
                                                <input class="item_price" type="hidden" value="0" />
                                                @endif
                                            @else
                                                @if($item->price - $item->discount->discount_value)
                                                <input class="item_price" type="hidden" value="{{ $item->price - $item->discount->discount_value }}" />
                                                @else
                                                <input class="item_price" type="hidden" value="0" />
                                                @endif
                                            @endif
                                        @else
                                            <input class="item_price" type="hidden" value="{{ $item->price }}" />
                                        @endif

                                        {{--@if($item->metric)
                                            <input class="item_metric" type="hidden" value="{{ $item->value_per_package.' '.$item->metric->abbreviation }}" />
                                        @else
                                            <input class="item_metric" type="hidden" value="{{ $item->value_per_package.' --metric not added--' }}" />
                                        @endif--}}
                                        <input class="item_image" type="hidden"  name="" value="{{ Groceries::getFirstImageUrl($item->id) }}">

                                    </div>
									@if($item->pivot->usual_qty != 0)
										<div class="qty-change" style="display: block;" id="qty-{{$item->pivot->id}}">
											<div class="counter independent">
												<button class="btn minus decrease_cart_item_qty_out_pop" data-favo="1">-</button>
												<input class="item_identifier" value="{{$item->pivot->id}}" type="hidden">
												<input class="totalx item_qty" name="" value="{{$item->pivot->usual_qty}}" type="text">
												<button class="btn plus increase_cart_item_qty_out_pop" data-favo="1">+</button>
											</div>
										</div>
									@elseif($item->stock > 0)
                                        <button class="button add-to-cart-btn list_add_to_cart" data-favo="1">ADD TO CART</button>
									@else
    									<button class="button add-to-cart-btn" style="color:white;background:#ff0000;" disabled>SOLD OUT</button>
									@endif


                                </div>
							</div>
						</div>
					@endforeach
					@else
						<br>
						&nbsp
					<h3 class="text-blue text-center"><b>Start adding food items now to your favourite supplies so you can automatically add all your favorite supplies for your next order!<b></h3>
						<br>
						&nbsp
					@endif
	            </div>

				{{-- <div class="col-md-7 col-sm-9 col-xs-12 margin-bottom pull-right" style="text-align:right">
	                @include('frontend.layout.pagination', ['paginator' => $groceries])
	            </div> --}}
	        </div>
		</div>
	</div>
@endsection

@section('scripts')
<script src="assets/plugin/bootstrap/js/jquery.bootstrap-responsive-tabs.min.js"></script>
<script src="assets/plugin/fancybox/js/jquery.fancybox.min.js"></script>
<script>
$('ul.nav li.dropdown').hover(function() {
  $(this).find('.dropdown-menu').stop(true, true).delay(50).fadeIn(500);
}, function() {
  $(this).find('.dropdown-menu').stop(true, true).delay(50).fadeOut(500);
});


</script>
@endsection
