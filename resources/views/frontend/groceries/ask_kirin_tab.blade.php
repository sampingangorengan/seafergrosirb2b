@if ($questions->count() > 0)

@foreach($questions as $key=>$question)
@if($key == 0 || $key == 4 || $key == 8 || $key == 12)
<li class="{{ $key == 0 ? 'is-active' : '' }} orbit-slide">
    <div class="customer-qa-box">
@endif
            <div class="qa-box">
                <table>
                    <tr>
                        <td><span class="text-blue">Q :</span></td>
                        <td><span class="text-blue">{{ $question->question }}</span></td>
                    </tr>
                    <tr>
                        <td>A :</td>
                        <td>{{ $question->answer ? $question->answer : 'The answer will be updated soon' }}</td>
                    </tr>
                </table>
            </div>

@if($key == 3 || $key == 7 || $key == 11 || $key == 12 || $key == ($questions->count() - 1))
    </div>
</li>
@endif
@endforeach

@else
    <li class="is-active orbit-slide">
        <div class="qa-box">
            <table>
                <tr>
                    <td><span class="text-blue">search :</span></td>
                    <td><span class="text-blue">{{ $search }}</span></td>
                </tr>
                <tr>
                    <td>result :</td>
                    <td>No match question found!</td>
                </tr>
            </table>
        </div>
    </li>
@endif