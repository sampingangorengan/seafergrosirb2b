@extends('frontend.layout.master')

@section('title', App\Page::createTitle('Thank You'))

@section('customcss')
    <link rel='stylesheet' href='assets/plugin/bootstrap/css/bootstrap-responsive-tabs.css'>
    <link rel="stylesheet" type="text/css" href="assets/plugin/fancybox/css/jquery.fancybox.min.css">
@endsection

@section('content')
	<div class="main-content">
		<div class="container" id="confirm-box">
			<div class="row">
				<div class="col-md-9 col-sm-9 col-xs-10 col-centered center">
					<div class="img-box">
					  <img src="assets/images/thank-you.png">
					</div>
					<div class="subtitle">
					  TERIMA KASIH TELAH BERBELANJA DI <span class="htp">Seafer Grosir</span>
					</div>
					<p>{{ auth()->user()->name }},</p>
					{!! $mes !!}
					<button type="button" class="btn btn-aneh gplus xyz"><a href="{{ url('orders/'.$order->id) }}">LIHAT PESANAN</a></button>
					<span class="or">Or</span>
					<button type="vutton" class="btn btn-aneh xyz"><a href="{{ url('/') }}">HOME</a></button><br><br>
				</div>

			</div>
		</div>

	</div>
@endsection

@section('scripts')
<script src="assets/plugin/bootstrap/js/jquery.bootstrap-responsive-tabs.min.js"></script>
<script src="assets/plugin/fancybox/js/jquery.fancybox.min.js"></script>
@endsection
