@extends('frontend.layout.master')

@section('title', App\Page::createTitle('Cashback Rewards'))

@section('customcss')
    <link rel='stylesheet' href='assets/plugin/bootstrap/css/bootstrap-responsive-tabs.css'>
    <link rel="stylesheet" type="text/css" href="assets/plugin/fancybox/css/jquery.fancybox.min.css">
@endsection

@section('content')
	<div class="main-content">
		<div class="container">
	        <div class="row hide-768">
				<div class="col-md-12">
					<span class="text-grey equal margin-top-10">
					  <a class="text-grey" href="{!! url('/') !!}"><b>HOME</b></a> /
					  <a class="text-grey" href="#"><b>CASHBACK REWARDS</b></a>
					</span>
				</div>
			</div>
			<div class="row">
	            <div class="col-md-12">
	                {{-- <h5 class="title title-margin">CASHBACK REWARDS</h5> --}}
	                {!! $description[1]->description_content!!}
	            </div>
	        </div>
	        <div class="row loyalty-points-box">
	            <div class="col-md-7 col-sm-7 col-xs-10 col-centered">
	                <center>
	                    <div class="img-box">
	                        <img src="assets/images/loyaltypoint.png">
	                    </div>
	                </center>
					{!! $description[2]->description_content!!}
	                {{-- <p>
	                    Our Loyalty Points come in a 0.5% cashback for every Rp 100.000,00 order value. You can choose to redeem your points upon checkout or continue accumulating your points and redeem it later on our website or mobile app. There is no expiry, so you can collect your points during your leisure. Happy shopping! <br><br>
	                    Read <a href="#">Terms & Conditions</a> for more information.

	                </p> --}}
	            </div>
	        </div>
		</div>
	</div>
@endsection

@section('scripts')
<script src="assets/plugin/bootstrap/js/jquery.bootstrap-responsive-tabs.min.js"></script>
<script src="assets/plugin/fancybox/js/jquery.fancybox.min.js"></script>
@endsection