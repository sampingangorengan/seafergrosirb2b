@extends('frontend.layout.master')

@section('title', App\Page::createTitle('Contact Us'))

@section('customcss')
    <link rel='stylesheet' href='assets/plugin/bootstrap/css/bootstrap-responsive-tabs.css'>
    <link rel="stylesheet" type="text/css" href="assets/plugin/fancybox/css/jquery.fancybox.min.css">

@endsection

@section('content')
	<div class="main-content">
		<!-- <div class="container">
	        <div class="row hide-768">
				<div class="col-md-12">
					<span class="text-grey equal margin-top-10">
					  <a class="text-grey" href="{{ url('/') }}"><b>HOME</b></a> /
					  <a class="text-grey" href="loyalty-points.html"><b>CONTACT US</b></a>
					</span>
				</div>
			</div>
		</div> -->
		<div class="contact-us-box">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					{{-- <h5 class="title title-margin">CONTACT US</h5> --}}
					{!! $description[1]->description_content !!}
				</div>
			</div>
			<div class="row">
				<div class="col-md-7 col-sm-7 col-xs-10 col-centered">
					<center>
						<div class="img-box">
							<img src="{{$images[0]}}" style="max-height:400px">
						</div>
						<br>
						{!! $description[2]->description_content !!}
						{{-- <h6>HOW CAN WE HELP YOU?</h6> --}}
					</center>
					<b >{!! $description[3]->description_content !!}</b>
					{{-- <p>
						In case your email has changed, please update it by signing in. If you have anti-spam software installed,
						<br>
						please make sure it does not block or filter emails sent from our domain (foodis.co.id).
						<br><br>
						If you suspect you're still unable to receive our emails after changing the settings of those programs,
						<br>
						please contact your ISP (Internet Service Provider) to enable you to receive e-mails sent from our
						<br>domain (foodis.co.id).
					</p> --}}
				</div>
			</div>
		</div>
		</div>
		<div class="container" id="contact-us">
			<div class="row">
				<div class="col-md-7 col-sm-7 col-xs-10 col-centered">
					<div class="col-md-6">
	                    <h5 class="subtitle">REACH US HERE</h5><br>
	                    <ul class="reach-us-here">
	                                                <li>
	                            <img src="{{$images[1]}}">
	                            <span>+62-21-6669-6285</span>
	                        </li>
	                        <li>
	                            <img src="{{$images[2]}}">
	                            <span>@Seafer Grosir</span>
	                        </li>
	                        <li>
	                            <img src="{{$images[3]}}">
	                            <span>hello@foodis.co.id</span>
	                        </li>
	                        <li>
	                            <img src="{{$images[4]}}">
	                            <span>PT. Seafer Grosir Jaya Raya<br>
								  Jl. Cumi Raya No.3, Muara baru<br>
								  Jakarta Utara 144440, Indonesia<span>
	                        </li>
	                        <li>
	                            <div class="gmap-box">
	                                <div id="gmap-display" style="">
	                                    <iframe frameborder="0" src="https://www.google.com/maps?q=Tangerang&output=embed"></iframe>
	                                </div>
	                            </div>
	                        </li>
	                    </ul>
	                </div>
	                <div class="col-md-6">
	                    <div class="flash-message">
	                     </div>
	                    <form method="post">
	                    <h5 class="subtitle">INQUIRY FORM</h5><br>
	                    <div class="form-box label-placeholder  top-d">
	                        <div class="row">
	                            <div class="col-md-4">
	                                <label for="middle-label" class="text-left middle no-padding-2">NAME<span class="text-red">*</span></label>
	                            </div>
	                            <div class="col-md-8">
	                                <input type="text"  placeholder="State your name" name="name" required="required">
	                            </div>
	                        </div>
	                        <div class="row">
	                            <div class="col-md-4">
	                                <label for="middle-label" class="text-left middle no-padding-2">EMAIL<span class="text-red">*</span></label>
	                            </div>
	                            <div class="col-md-8">
	                                <input type="text"  placeholder="Type your email here" name="email" required="required">
	                            </div>
	                        </div>
	                        <div class="row">
	                            <div class="col-md-4">
	                                <label for="middle-label" class="text-left middle no-padding-2 no-placeholder">CATEGORY<span class="text-red">*</span></label>
	                            </div>
	                            <div class="col-md-8">
	                                <select name="category">
	                                    <option value="orders">Orders</option>
	                                    <option value="products">Products</option>
	                                    <option value="cashback rewards">Cashback Rewards</option>
	                                    <option value="delivery">Delivery</option>
	                                    <option value="payment">Payment</option>
	                                    <option value="troubleshooting">Troubleshooting</option>
	                                    <option value="suggest product or seafer eats">Suggest a Product/Seafer Eats Seller</option>
	                                    <option value="general inquiry">General Inquiry</option>
	                                </select>
	                            </div>
	                        </div>
	                        <div class="row">
	                            <div class="col-md-4">
	                                <label for="middle-label" class="text-left middle no-padding-2">MESSAGE</label>
	                            </div>
	                            <div class="col-md-8">
	                                <textarea cols="3" rows="5" name="message" required="required"></textarea>
	                            </div>
	                        </div>
	                        <div class="row">
	                            <div class="col-md-4">
	                                &nbsp;
	                            </div>
	                            <div class="col-md-8">
	                                <input type="hidden" name="_token" value="YSGpIKGpqR53M3SBI730RXYMtoAiDnNNwv0DIC3u">
	                                <button class="btn pull-right btn-aneh">SUBMIT</button>
	                            </div>
	                        </div>
	                        </form>
	                    </div>
	                </div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
<script src="assets/plugin/bootstrap/js/jquery.bootstrap-responsive-tabs.min.js"></script>
<script src="assets/plugin/fancybox/js/jquery.fancybox.min.js"></script>
@endsection
