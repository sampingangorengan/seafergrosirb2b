@extends('frontend.layout.master')

@section('title', App\Page::createTitle('About Us'))

@section('customcss')
    <link rel='stylesheet' href='assets/plugin/bootstrap/css/bootstrap-responsive-tabs.css'>
    <link rel="stylesheet" type="text/css" href="assets/plugin/fancybox/css/jquery.fancybox.min.css">
@endsection

@section('content')
<div class="boxUtamaAbout">
	<div class="box-content">
		<!-- SLIDER -->
		<section class="bannerAbout">
			<img src="{{ $images[0] }}">
			<div class="captAbout">
				{!! $description[1]->description_content !!}
			</div>
		</section>
		<!-- SLIDER -->

		<!-- WHY CHOOSE Seafer Grosir -->
		<section class="boxWhyChoose">
			<div class="headingTitle after_clear">
	          <h1>Kenapa Seafer Grosir</h1>
	        </div>

	        <div class="content-grid">
	        	<div class="row">
	        		<div class="col-md-4 col-sm-6 col-xs-6">
	        			{!! $description[2]->description_content !!}

	        		</div>
	        		<div class="col-md-4 col-sm-6 col-xs-6 no-color">
						<img src="{{ $images[1] }}">
	        			
	        		</div>
	        		<div class="col-md-4 col-sm-6 col-xs-6">
	        			{!! $description[3]->description_content !!}

	        		</div>
	        		<div class="col-md-4 col-sm-6 col-xs-6 no-color">
	        			<img src="{{ $images[2] }}">
	        		</div>
	        		<div class="col-md-4 col-sm-6 col-xs-6">
	        			{!! $description[4]->description_content !!}
	        		</div>
	        		<div class="col-md-4 col-sm-6 col-xs-6 no-color">
	        			<img src="{{ $images[3] }}">
	        		</div>
	        	</div>
	        </div>


		</section>
		<!-- WHY CHOOSE Seafer Grosir -->

		<!-- FISH AND MEAT -->
		<section class="boxFishandMeat">
			<div class="headingTitle after_clear">
	          {!! $description[5]->description_content !!}
	        </div>
	        <div class="content-fish">
	        	<div class="row">
	        		<div class="col-md-6 col-sm-7 col-img">
	        			<img src="{{ $images[4] }}">
	        		</div>
	        		<div class="col-md-6 col-sm-5">
	        			{!! $description[6]->description_content !!}
	        		</div>
	        	</div>

	        	<div class="boxEatBenefit after_clear">
	        		<div class="img">
	        			<img src="{{ $images[5] }}">
	        		</div>
	        		<div class="boxTextList">
	        			<div class="text-1">{!! $description[7]->description_content !!}</div>
	        			<div class="text-2">{!! $description[8]->description_content !!}</div>
	        			<div class="text-3">{!! $description[9]->description_content !!}</div>
	        			<div class="text-4">{!! $description[10]->description_content !!}</div>
	        		</div>

	        	</div>

	        </div>
	    </section>
		<!-- FISH AND MEAT -->


		<!-- OTHER BENEFIT -->
		<section class="boxWhyChoose">
			<div class="headingTitle after_clear">
	          {!! $description[11]->description_content !!}
	        </div>

	        <div class="content-benefit after_clear">
	        	<div class="col-md-4 col-sm-4 col-xs-6">
	        		<div class="img">
	        			<img src="/assets/images/new_about/benefit-1.png">
	        		</div>
	        		<div class="boxTitDesc">
	        			{!! $description[12]->description_content !!}
	        			{{-- <div class="title">Kalium <br> Bantuan protein</div>
	        			<div class="desc">synthesis &amp; Dukungan <br> untuk pembentukan otot.</div> --}}
	        		</div>
	        	</div>
	        	<div class="col-md-4 col-sm-4 col-xs-6">
	        		<div class="img">
	        			<img src="/assets/images/new_about/benefit-2.png">
	        		</div>
	        		<div class="boxTitDesc">
	        			{!! $description[13]->description_content !!}
	        			{{-- <div class="title">Kalsium &amp; <br> Phosporus</div>
	        			<div class="desc">Bantuan Kesehatan<br>
                        bones and teeth.</div> --}}
	        		</div>
	        	</div>
	        	<div class="col-md-4 col-sm-4 col-xs-6">
	        		<div class="img">
	        			<img src="/assets/images/new_about/benefit-3.png">
	        		</div>
	        		<div class="boxTitDesc">
	        			{!! $description[14]->description_content !!}
	        			{{-- <div class="title">Zinc</div>
	        			<div class="desc">Aids in boosting<br>
                        immune system.</div> --}}
	        		</div>
	        	</div>
	        	<div class="col-md-4 col-sm-4 col-xs-6">
	        		<div class="img">
	        			<img src="/assets/images/new_about/benefit-4.png">
	        		</div>
	        		<div class="boxTitDesc">
	        			{!! $description[15]->description_content !!}
	        			{{-- <div class="title">Vitamin D</div>
	        			<div class="desc">Helps the body<br>
                        absorb calcium.</div> --}}
	        		</div>
	        	</div>
	        	<div class="col-md-4 col-sm-4 col-xs-6">
	        		<div class="img">
	        			<img src="/assets/images/new_about/benefit-5.png">
	        		</div>
	        		<div class="boxTitDesc">
	        			{!! $description[16]->description_content !!}
	        			{{-- <div class="title">Magnesium</div>
	        			<div class="desc">Menjaga otot<br>
                        normal dan kinerja<br>
                        saraf.</div> --}}
	        		</div>
	        	</div>

	        	<div class="col-md-4 col-sm-4 col-xs-6">
	        		<div class="img">
	        			<img src="/assets/images/new_about/benefit-6.png">
	        		</div>
	        		<div class="boxTitDesc">
	        			{!! $description[17]->description_content !!}
	        			{{-- <div class="title">Iodine</div>
	        			<div class="desc">Necessary for proper<br>
                        thyroid function.</div> --}}
	        		</div>
	        	</div>
	        	<div class="col-md-4 col-sm-4 col-xs-6">
	        		<div class="img">
	        			<img src="/assets/images/new_about/benefit-7.png">
	        		</div>
	        		<div class="boxTitDesc">
	        			{!! $description[18]->description_content !!}
	        			{{-- <div class="title">Vitamin B2</div>
	        			<div class="desc">Melindungi kulit, rambut dan mata,<br>
                        menjaga stamina dan<br>
                        mencegah migraine.</div> --}}
	        		</div>
	        	</div>
	        </div>
	    </section>
		<!-- OTHER BENEFIT -->

		<!-- FISH COMPARE -->
		<section class="boxFishandMeat">
			<div class="headingTitle after_clear">
	          {!! $description[19]->description_content !!}
	        </div>
	        <div class="content-compare">
	        	<img src="{{ $images[6] }}" class="img-compare">
	        	{!! $description[20]->description_content !!}
	        </div>
	    </section>
		<!-- FISH COMPARE -->

		<!-- NATIONAL FISH CONSUMPTION -->
		<section class="boxWhyChoose">
			<div class="headingTitle after_clear">
				{!! $description[21]->description_content !!}
	        </div>
	        <div class="content-consumption">
	        	{!! $description[22]->description_content !!}

	        	<div class="row box-chart">
	        		<div class="col-md-6 col-sm-6 col-xs-7">
	        			<img src="{{ $images[7] }}" class="img-chart">
	        		</div>
	        		<div class="col-md-6 col-sm-6 col-xs-5">
	        			<div class="img-eat-fish">
	        				<img src="{{ $images[8] }}">
	        			</div>
	        			<div class="img-gemar">
	        				<img src="{{ $images[9] }}">
	        			</div>
	        		</div>
	        	</div>
	        </div>
	    </section>
		<!-- NATIONAL FISH CONSUMPTION -->
	</div>
</div>
@endsection

@section('scripts')
<script src="assets/plugin/bootstrap/js/jquery.bootstrap-responsive-tabs.min.js"></script>
<script src="assets/plugin/fancybox/js/jquery.fancybox.min.js"></script>
@endsection
