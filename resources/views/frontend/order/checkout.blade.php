@extends('frontend.layout.master')

@section('title', App\Page::createTitle('Checkout'))

@section('customcss')
    <link rel='stylesheet' href='assets/plugin/bootstrap/css/bootstrap-responsive-tabs.css'>
    <link rel="stylesheet" type="text/css" href="assets/plugin/fancybox/css/jquery.fancybox.min.css">
	<link rel="stylesheet" type="text/css" href="{!! asset('bower_components/AdminLTE') !!}/plugins/datepicker/datepicker3.css">
    <style>
		#container-map {
  			border: 5px solid #025EFF;
  			display: inline block;
  			width:100%;
  		}

		#map {
			height: 300px;
			width:100%;
		}

		span#map-help {
		    width: 100%;
		    background: #025EFF !important;
		    font-weight: bold;
		    display: block;
		}

		#map #infowindow-content {
  			display: inline;
  		}
  		.pac-container {
		  	z-index: 99999 !important;
		}
		.sub-subtitle {
			margin-bottom:10px;
		}
		.panin_img {
			height:80px;
			width:auto !important;
		}
		.payment_method, .payment_option {
			margin-left:15px;
		}
	</style>
@endsection

@section('content')

	<div class="main-content">
		<div class="container" id="checkout">
            <div class="row">
                <div class="col-md-4 col-sm-10 col-xs-12 col-centered-768">
					<div class="col-md-12 col-sm-10 col-xs-12 col-centered-768">
                        <h5 class="title-aneh">INFORMASI PEMBAYARAN</h5>
                        <table>
                            <tbody><!--
	                            <tr>
	                                <td>
	                                    <span class="sub-subtitle">ALAMAT</span>
	                                </td>
	                            </tr> -->
	                            <tr>
	                            	<td>
	                            		NAMA
	                            	</td>
	                            </tr>
		                        <tr>
		                        	<td>
		                        		{{ auth()->user()->name }}
		                        	</td>
		                        </tr>
		                        <tr>
		                        	<td>
		                        		&nbsp;
		                        	</td>
		                        </tr>
		                        <tr>
		                        	<td>
		                        		ALAMAT EMAIL
		                        	</td>
		                        </tr>
		                        <tr>
		                        	<td>
		                        		{{ auth()->user()->email }}
		                        	</td>
		                        </tr>
		                        <tr>
		                        	<td>
		                        		&nbsp;
		                        	</td>
		                        </tr>
		                        <tr>
		                        	<td>
		                        		TELEPON
		                        	</td>
		                        </tr>
		                        <tr>
		                        	<td>
		                        		{{ auth()->user()->phone_number }}
		                        	</td>
		                        </tr>
							</tbody>
							</table>
						</div>
						<table>
							<tr>
								<td>
									<hr class="blue">
								</td>
							</tr>
							<tr>
								<td>
									<h4 class="sub-subtitle LEFT payment_option">METODE PEMBAYARAN</h4>
								</td>
							</tr>
						</table>

						<table class="payment_method">
							<tr>
								<td>
									<div class="box">
										<input value="cod" name="pay_option" class="square pay_option" type="radio">
										<label for="regular">CASH ON DELIVERY</label>
									</div>
									<div class="box">
										<input value="transfer" name="pay_option" class="square pay_option" type="radio">
										<label for="yes">BANK TRANSFER</label>
									</div>

									<div class="box" style="{{(auth()->user()->checkTempo())?"":"opacity:0.2"}}">
										<input value="tempo" name="pay_option" class="square pay_option tempo_radio" type="radio" {{(auth()->user()->checkTempo())?"":"disabled"}}>
										<label for="">TEMPO</label>
									</div>
									@if(auth()->user()->hasTempo() )
										@if(auth()->user()->tempo->status == "approved")
										<div class="box js_tempo_day_text" hidden>
											<b>
												<h5>Aggrement</h5>
												<h5>Durasi Pembayaran: {{auth()->user()->tempo->duration}} Hari</h5>
												<p>{{auth()->user()->tempo->aggrement}}</p>
											</b>
										</div>
										@elseif(auth()->user()->tempo->status == "pending")
											<div class="box">
												<h5>Tempo anda sedang dalam proses review</h5>
											</div>

										@elseif(auth()->user()->tempo->status == "unsuccessful")
											<div class="box">
												<h5>Aplikasi tempo anda gagal</h5>
											</div>
										@else
											<div class="box">
												<h5>Anda harus mengajukan aplikasi untuk dapat melakukan Tempo</h5>
											</div>
										@endif
									@endif

								</td>
							</tr>
						</table>


						<div class="col-md-12 col-sm-10 col-xs-11 col-centered-768">
							<div class="row">
								<div class="payment-box">
									<label for="payment2">
										<img src="assets/images/bank/panin.png" class="payment panin_img"><br>
										{{-- <span>153 500 7529</span>
										<br>
										<span>(a/n) PT. Seafer Grosir Jaya Raya</span> --}}
									</label>
								</div>
							</div>
							<div class="row">
								<div class="payment-box">
									<label for="payment1">
										<img src="assets/images/bank/bca.png" class="payment"><br>
										{{-- <span>206 3232 327</span>
										<br>
										<span>(a/n) PT. Seafer Grosir Jaya Raya </span> --}}
									</label>
								</div>
							</div>

						</div>
						<br>
                	</div>
                    <div class="col-md-4 col-sm-10 col-xs-12 col-centered-768">

                        <form method="post" id="checkout-form">
							<div class="col-md-12 col-sm-10 col-xs-12 col-centered-768">
								<h5 class="title-aneh">INFORMASI SHIPPING</h5>
								<table class="payment">
									<tbody>
										<tr>
											<td>
												<span class="sub-subtitle">
													ALAMAT SHIPPING
												</span>
											</td>
										</tr>
										<tr>
											<td>
												<select name="address_id" id="checkout_address_id">
			                                        <option value="0">
			                                            Pilih Alamat
			                                        </option>
			                                        @foreach (App\Models\User\Address::whereUserId(auth()->user()->id)->where('deleted', 0)->orderBy('name')->get() as $address)
			                                            <option value="{{ $address->id }}">
			                                                    {{ $address->name }}
			                                            </option>
			                                        @endforeach
			                                    </select>
											</td>
										</tr>
										<tr>
											<td>
												<a class="pull-right" data-toggle="modal" data-target="#addAddressModal" href="#" aria-controls="addAddressModal" aria-haspopup="true" tabindex="0">add new address</a>
											</td>
										</tr>
										<tr>
											<td>
												<span class="sub-subtitle">
													Metode Shipping
												</span>
											</td>
										</tr>
										<tr>
											<td>
												<p>Seafer Grosir Courier</p>
											</td>
										</tr>
										<tr>
											<td>
												<span class="sub-subtitle">
													Tanggal Pengiriman
												</span>

											</td>
										</tr>
										{{--  Delivery date Implmentation--}}
										<tr>
											<td>
												<input type="text" class="js_delivery_date" name="delivery_date" value="" autocomplete="off">
											</td>
										</tr>
										{{--  Delivery date Implmentation--}}
										<tr>
											<td>
												<span class="sub-subtitle">
													Notes
												</span>
											</td>
										</tr>
										<tr>
											<td>
												<textarea type="text" class="" name="delivery_notes" value="" rows="5" placeholder="ex: Time, Special instructions, Contact"></textarea>

											</td>
										</tr>
									</tbody>
								</table>
							</div>
                    </div>
                    <div class="col-md-4 col-sm-10 col-xs-12 col-centered-768">
						<div class="col-md-12 col-sm-10 col-xs-12 col-centered-768">
                        	<h5 class="title-aneh">ORDER REVIEW</h5>
                    		<table class="summary">
                        		<tbody>
	                        		<tr>
	                        			<td colspan="3">
	                        				<b class="text-blue size-18">FOO</b>
	                        				<b class="text-red size-18">DIS</b>
	                        			</td>
									</tr>
									<?php $state = ''; ?>
									@foreach (Carte::contents() as $cartItem)
									<?php

										$product = DB::table('groceries')->where('id',$cartItem->id)->first();
										$state = $state.' , '.$product->state;
										$range_price = \App\Models\Groceries::where('name',$cartItem->name)->first()->range_price;

									?>
		                            <tr class="cart-items" data-state="{{ $product->state }}">
		                                <td width="50%">{{ $cartItem->name }}</td>
		                                <td width="10%">{{ $cartItem->quantity }}pcs</td>
                                        @if($range_price)
                                            <td width="10%">
												<?php $is_range_order = 0; ?>
                                                @if($range_price)
                                                <?php $is_range_order = 1; ?>
                                                {{Money::display($range_price * $cartItem->quantity)}}
                                                @endif
											</td>
											<td width="10%">
												{{ Money::display($cartItem->price * $cartItem->quantity) }}
											</td>
										@else
											<td></td>
                                            <td width="">
                                                {{ Money::display($cartItem->price * $cartItem->quantity) }}
                                            </td>
                                        @endif

		                            </tr>
			                        @endforeach
									<input type="hidden" id="product-states" value="{{ $state }}">
	                                <tr>
	                            		<td>&nbsp;</td>
	                            		<td>&nbsp;</td>
	                            		<td>&nbsp;</td>
	                            		<td>&nbsp;</td>
	                        		</tr>
			                        <tr>
			                            <td colspan="3">
			                            	<span style="float:left">SUB TOTAL</span>
			                            </td>
			                            <td>
			                            	<span style="float:right" align="right">{{Money::display( Carte::total() ) }}</span>
			                            </td>
			                        </tr>
			                        <tr>
			                            <td colspan="3">
			                            	<span style="float:left">PROMO</span>
			                            </td>
			                            <td id="checkout_promo_fee" style="color:#ff0000;" align="right">
			                            	<span style="float:right;margin-right:-5px;">({{ $promo_applied == false ? 'Rp 0' : Money::display($promo_applied) }})</span>
			                            </td>
			                        </tr>
			                        <tr>
			                            <td colspan="3">
			                            	<span style="float:left">TOTAL</span>
			                            </td>
			                            <td id="checkout_total" align="right">
			                            	<span style="float:right">{{ Money::display($total) }}</span>
			                            </td>
			                        </tr>
			                        <tr>
			                            <td colspan="4">
			                                <input name="shipping_fee" value="0" type="hidden">
			                                <input name="paymt_opt" type="hidden" />
			                                <button id="form-submit-button" data-open="modal-before" class="btn btn-primary maxwidth space-3">SUBMIT</button>
			                            </td>
			                        </tr>
			                        <tr>
			                            <td>&nbsp;</td>
			                            <td>&nbsp;</td>
			                            <td>&nbsp;</td>
			                        </tr>
		                        </tbody>
		                    </table>

                        	<br><br><br>
                        	{{ csrf_field() }}
                    		</form>
						</div>
                    </div>
                </div>
            </div>
		</div>
	</div>
	<div id="addAddressModal" class="modal fade addAddressModal" role="dialog">

        <div class="modal-dialog">
            <div class="modal-content">
      			<div class="modal-box">
	                <div class="form-box">
	                    <form id="new-address-add" class="border-dashed" method="post" action="{{ url('checkout/new-address') }}" style="font-size:16px">
	                        <div class="row">
	                            <div class="large-4 columns"><span class="required" style="font-weight: bold;">ADDRESS NAME</span></div>
	                            <div class="large-8 columns"><input type="text" name="name" required="required" placeholder="e.g., Alamat Kantor"></div>
	                        </div>
	                        <div class="row">
	                            <div class="large-4 columns"><span class="required" style="font-weight: bold;">RECIPIENT'S TITLE</span></div>
	                            <div class="large-8 columns">
	                                <select name="recipient_title" id="select_salutation" required="required">
	                                    <option value="0">Please Select Recipient's Salutation</option>
	                                    <option value="m">Mr.</option>
	                                    <option value="fs">Ms.</option>
	                                    <option value="f">Mrs.</option>
	                                </select>
	                            </div>
	                        </div>
	                        <div class="row">
	                            <div class="large-4 columns"><span class="required" style="font-weight: bold;">RECIPIENT'S NAME</span></div>
	                            <div class="large-8 columns"><input type="text" name="recipient" required="required" placeholder="type recipient name here"></div>
	                        </div>
	                        <div class="row">
	                            <div class="large-4 columns"><span class="required" style="font-weight: bold;">MOBILE NUMBER</span></div>
	                            <div class="large-8 columns"><div class="input-group">
	                                    <span class="input-group-label">+62</span>
	                                    <input required name="phone_number" class="input-group-field" type="text" placeholder="e.g., 8123xxx" style="height:26px;" required="required">
	                                </div></div>
	                        </div>
	                        <div class="row">
	                            <div class="large-4 columns"><span class="required" style="font-weight: bold;">ADDRESS</span></div>
	                            <div class="large-8 columns"><input id="address-line" type="text" name="address" required="required" placeholder="type your address here"></div>
	                        </div>

	                        <div class="row">
	                            <div class="large-4 columns"><span class="required" style="font-weight: bold;">STATE/PROVINCE</span></div>
	                            <div class="large-8 columns">
	                                <select name="province_id" id="select_province">
	                                    <option value="0">Please Select Province</option>
	                                    @foreach(App\Models\Province::get() as $province)
	                                        <option value="{{ $province->province_id }}">{{ $province->province_name }}</option>
	                                    @endforeach
	                                </select>
	                            </div>
	                        </div>
	                        <div class="row">
	                            <div class="large-4 columns"><span class="required" style="font-weight: bold;">CITY</span></div>
	                            <div class="large-8 columns">
	                                <select name="city_id" id="select_city">
	                                    <option value="0">Please Select City</option>
	                                    @foreach(App\Models\City::where('is_active', 1)->orderBy('city_name_full', 'asc')->get() as $city)
	                                        <option value="{{ $city->city_id }}" data-prov="{{ $city->province_id }}">{{ $city->city_name_full }}</option>
	                                    	}
	                                    @endforeach
	                                </select>
	                            </div>
	                        </div>
	                        <div class="row">
	                            <div class="large-4 columns">  <span class="required" style="font-weight: bold;">AREA</span></div>
	                            <div class="large-8 columns">
	                                <select name="area_id" id="select_area" placeholder="Please Select Area" required="required">
	                                    <option value="0">Please Select Area</option>
	                                    @foreach(App\Models\Location\Area::orderBy('name', 'asc')->get() as $area)
	                                        <option value="{{ $area->id }}" data-city="{{ $area->city_id }}">{{ $area->name }}</option>
	                                    @endforeach
	                                </select>
	                            </div>
	                        </div>

	                        {{-- <div class="row">
	                           <div class="large-4 columns">  <span class="required" style="font-weight: bold;">SUB DISTRICT</span></div>
	                           <div class="large-8 columns"><select>
	                               <option>Please select sub district</option>
	                             </select></div>
	                         </div>--}}
	                        <div class="row">
	                            <div class="large-4 columns"> <span class="required" style="font-weight: bold;">ZIP/POSTAL CODE</span></div>
	                            <div class="large-8 columns"><input type="text" name="postal_code" required="required"  placeholder="type your postal code here"></div>
	                        </div>
	                        <input id="input-coordinate" type="hidden" name="coordinates" value="0" />

	                        <div class="row">
                            <div class="large-4 columns">&nbsp;</div>
                            <div class="large-8 columns">

                            </div>

                            <div class="small-12 columns">

                                <div id="container-map">
                                    <span class="[round radius] label" id="map-help">Please input your address below and use the pin to locate your address</span>
                                    <input id="pac-input" class="controls" type="text" placeholder="Find your address">
                                    <div id="map"></div>
                                </div>
                            </div>
                            {{-- <div class="small-12 columns">
                                {{ csrf_field() }}
                                <button class="button btn-aneh" style="font-size: 1.2rem" type="submit">SAVE NEW ADDRESS</button>
                            </div> --}}
                        </div>
	                        <div class="row">
	                            <div class="large-4 columns">&nbsp;</div>
	                            <div class="large-8 columns">
	                                {{ csrf_field() }}
	                                <button class="button btn-aneh" style="font-size: 1.2rem;color:white;" type="submit">SAVE NEW ADDRESS</button>
	                            </div>
	                        </div>
	                    </form>
	                </div>
	            </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCRDhe-WvUmE8Y96Gb6Glj-CPc9lfjC420&libraries=places&callback=initMap"></script>
<script src="assets/plugin/bootstrap/js/jquery.bootstrap-responsive-tabs.min.js"></script>
<script src="assets/plugin/fancybox/js/jquery.fancybox.min.js"></script>
<script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datepicker/bootstrap-datepicker.js"></script>
<script>
	function checkForDisabled(classname) {
		if($(classname).attr('disabled') == undefined) {
			return false;
		}

		return true;
	}

	function checkForChecked(classname) {
		if($(classname).attr('checked') == undefined) {
			return false;
		}

		return true;
	}

	$('.pay_option').change(function() {
		$('input[name="paymt_opt"]').val($('.pay_option:checked').val());
	});

    $('#select_province').on('change', function(){
        var province_id = $(this).val();
        $.ajax({
            type: 'get',
            url: '{{ url('user/ajax-cities-by-province') }}/' + province_id
        }).done(function(result){
            result = jQuery.parseJSON(result);
            if(result.status == 'success') {
                $('#select_city').html('');
                $.each(result.cities, function(k, v) {
                    $('#select_city').append('<option value="'+k+'">'+v+'</option>');
                });

            } else {
                alert(result.message);
            }
        })
    });
    $('#select_city').on('change', function(){
        var city_id = $(this).val();
        $.ajax({
            type: 'get',
            url: '{{ url('user/ajax-areas-by-city/') }}/' + city_id
        }).done(function(result){
            result = jQuery.parseJSON(result);
            if(result.status == 'success') {
                $('#select_area').html('');
                $.each(result.areas, function(k, v) {
                    $('#select_area').append('<option value="'+k+'">'+v+'</option>');
                });

            } else {
                alert(result.message);
            }
        })

    });

    function validate(){
    	var delivery_date = $('input[name="delivery_date"]').val();
		var address = $('select[name="address_id"]').val();

		if(!address || address == 0){
            return false;
        }
		if(!delivery_date || delivery_date == ""){
			return false;
		}


        /*if(shipping_fee.trim() == 'Rp 0'){
            return false;
        }*/

        return true;
    }
    $('#form-submit-button').click(function(){
        var validation_status = validate();

        if(validation_status == true){
            return true;
        } else {
            alert('Please ensure that you have chosen payment option, address, and delivery date');
            return false;
        }
    });
    $('#new-address-add').submit(function(e){
        if($('#select_salutation').val() == 0 || $('#select_salutation').val() == '0'){
            e.preventDefault();
            alert('Please select recipient\'s salutation');
            return false;
        }
        if($('#select_area').val() == 0 || $('#select_area').val() == '0'){
            e.preventDefault();
            alert('Please select area');
            return false;
        }
        if($('#select_city').val() == 0 || $('#select_city').val() == '0'){
            e.preventDefault();
            alert('Please select city');
            return false;
        }
        if($('#select_province').val() == 0 || $('#select_province').val() == '0'){
            e.preventDefault();
            alert('Please select area');
            return false;
        }
    });

	$('.checkout_shipping_service_code').change(function(){
		var check = check_state();
		if($('#checkout_address_id').val() == 0) {
			alert('Please select address');
			$('.checkout_shipping_service_code').removeAttr('checked');
			return false;
		}
		var code = $(this).val();
		if(!check && code == 'jne_reg' || !check && code == 'jne_yes'){
			var code = false;
		}
		if(code){
			$.ajax({
				type: 'post',
				/*url: url('checkout/ajax-change-shipping-service-code'),*/
				url: "{{ url('checkout/calculate-shipping-fare') }}",
				data: {
					address_id: $('#checkout_address_id').val(),
					shipping_type: $(this).val()
				},
				beforeSend: function() {

					$('#checkout_shipping_fee').html("<img src='http://foodis.co.id/assets/images/ajax-loader.gif' />");
				}
			}).done(function(result){
				if(result.status == false){
					$('#modal-flash-new .text-red').text('Please pick your address first.');
					$('#modal-flash-new').foundation('open', 'open');
					$('input[name="shipping_service_code"]').removeAttr('checked');
					$('input[name="shipping_service_code"]').prop('checked',false);
					$('input[name="shipping_service_code"]').attr('checked',false);
				}
				$('td#checkout_shipping_fee').text(result.shippingFee);
				$('input[name="shipping_fee"]').val(result.shippingFee);
				$('td#checkout_total').text(result.total);
			});
		}
	});
    </script>
    <script>

        function getLatLongFromInput() {

            var area = document.getElementById('select_area');
            var city = document.getElementById('select_city');
            var province = document.getElementById('select_province');

            var complete_address = address.value + ", " + area.options[area.selectedIndex].text + ", " + province.options[province.selectedIndex].text;

            console.log(complete_address);
        }

        function setCoordinateInput(lat,long) {
            var coordinate = lat+","+ long;
            $('#input-coordinate').val(coordinate);
        }

        function setLatLongFromMaps(full_address) {

            var split_address = full_address.split(", ");
            split_address = split_address.reverse();

            console.log(split_address);


            //get province and postcode
            var province_postcode = split_address[1].split(' ');
            var province = '';
            var postcode = '';
            if(province_postcode.length > 1) {
                for(var i = 0;i<(province_postcode.length - 1);i++) {
                    if(i == 0){
                        province = province_postcode[0];
                    } else{
                        province = province + ' ' + province_postcode[i];
                    }

                }
                postcode = province_postcode[province_postcode.length -1];
            } else {
                province = split_address[1];
            }

            $('#select_province').find(":selected").removeAttr('selected');
            // set province
            if(province == 'Daerah Khusus Ibukota Jakarta' || province == 'Jakarta'){
                $('#select_province option:contains("DKI Jakarta")').attr('selected', true);
            } else if(province == 'Daerah Daerah Istimewa Yogyakarta' || province == 'Yogyakarta'){
                $('#select_province option:contains("DI Yogyakarta")').attr('selected', true);
            } else if(province == 'Aceh' || province == 'Daerah Istimewa Aceh'){
                $('#select_province option:contains("Nanggroe Aceh Darussalam")').attr('selected', true);
            } else {
                if ($('#select_province option:contains("'+province+'")').val() == null){
                    $('#select_province option:contains("Please Select Province")').attr('selected', true);
                } else {
                    $('#select_province option:contains("'+province+'")').attr('selected', true);
                }
            }


            // get & set postcode
            $('input[name="postal_code"]').val(province_postcode[province_postcode.length-1]);

            // get & set city
            $('#select_city').find(":selected").removeAttr('selected');
            if(split_address[2] == 'Kota SBY'){
                $('#select_city option:contains("Kota Surabaya")').attr('selected', true);
            } else {
                if ($('#select_city option:contains("'+split_address[2]+'")').val() == null){
                    $('#select_city option:contains("Please Select City")').attr('selected', true);
                } else {
                    $('#select_city option:contains("'+split_address[2]+'")').attr('selected', true);
                }
            }


            // get & set area
            $('#select_area').find(":selected").removeAttr('selected');
            if ($('#select_area option:contains('+split_address[3]+')').val() == null){
                $('#select_area option:contains("Please Select Area")').attr('selected', true);
            } else {
                $('#select_area option:contains('+split_address[3]+')').attr('selected', true);
            }


            // get address line
            var address = '';
            for(var i = split_address.length -1 ;i>3;i--) {
                if(i == split_address.length -1 ){
                    address = split_address[split_address.length -1];
                } else{
                    address = address + ', ' + split_address[i];
                }

            }

            // set address line
            $('#address-line').val(address);

        }

        var geocoder;
        var markers = [];

        // Sets the map on all markers in the array.
        function setMapOnAll(map) {
            for (var i = 0; i < markers.length; i++) {
                markers[i].setMap(map);
            }
        }

        function addMarker(map, place) {
            /*var marker = new google.maps.Marker({
              position: location,
              map: map
            });*/
            var image = 'http://foodis.co.id/assets/images/marker.png';

            // Create a marker for each place.
            marker = new google.maps.Marker({
                map: map,
                icon: image,
                draggable: true,
                animation: google.maps.Animation.DROP,
                title: 'Posisi Anda',
                position: place.geometry.location
            });

            marker.addListener('dragend', function() {
                geocodePosition(marker.getPosition());
            });

            marker.addListener('click', function() {
                map.setCenter(marker.getPosition());
                geocodePosition(marker.getPosition());
            });
            markers.push(marker);
        }

        function initMap() {
            geocoder = new google.maps.Geocoder();
            var myLatLng = {lat: -6.175392, lng: 106.827153};
            var map = new google.maps.Map(document.getElementById('map'), {
                center: myLatLng,
                zoom: 15,
                mapTypeId: 'roadmap'
            });

            // Create the search box and link it to the UI element.
            var input = document.getElementById('pac-input');
            var searchBox = new google.maps.places.SearchBox(input);
            map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

            // Bias the SearchBox results towards current map's viewport.
            map.addListener('bounds_changed', function() {
                searchBox.setBounds(map.getBounds());
            });

            /*google.maps.event.addListener(markers, 'dragend', function() {
                geocodePosition(markers.getPosition());
            });

            google.maps.event.addListener(markers, 'click', function() {
                geocodePosition(markers.getPosition());
            });*/

            // Listen for the event fired when the user selects a prediction and retrieve
            // more details for that place.
            searchBox.addListener('places_changed', function() {
                var places = searchBox.getPlaces();

                if (places.length == 0) {
                    return;
                }

                setMapOnAll(null);
                markers = [];

                // For each place, get the icon, name and location.
                var bounds = new google.maps.LatLngBounds();
                places.forEach(function(place) {
                    if (!place.geometry) {
                        console.log("Returned place contains no geometry");

                        return;
                    }

                    addMarker(map, place);

                    google.maps.event.trigger(markers, 'click');

                    if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }
                });
                map.fitBounds(bounds);

            });
        }

        function geocodePosition(pos) {
            geocoder.geocode({
                latLng: pos
            }, function(responses) {

                if (responses && responses.length > 0) {
                    markers.formatted_address = responses[0].formatted_address;
                    setCoordinateInput(responses[0].geometry.location.lat(), responses[0].geometry.location.lng());
                } else {
                    markers.formatted_address = 'Cannot determine address at this location.';
                }

                setLatLongFromMaps(markers.formatted_address);
                $('#input-coordinate').val(pos);

            });
        }

		function check_state(){
			var states = $('#product-states').val();
			if(states.includes('Fresh') || states.includes('Frozen')){
				if (!$(".frozen-notice")[0]) {
					$('.shipping-serv').append('<p class="frozen-notice" style="color:red;font-size:10px;margin-left:15px">*) Frozen item can only be delivered using Gojek or COD</p>');
				}

				$('.jne_reg_button').attr('disabled', 'disabled');
				$('.jne_reg_button').removeAttr('checked');
				$('.jne_yes_button').attr('disabled', 'disabled');
				$('.jne_yes_button').removeAttr('checked');
				return false;
			}
			 return true;
		}

		$( "#pin-location" ).click(function() {
            $( "#container-map" ).toggle();
            google.maps.event.trigger(map, 'resize');
        });

		var dateToday = new Date();

		function setupDatePicker(){
			var dateToday = new Date();

			dateToday.setDate(dateToday.getDate() + 1 );

			$('.js_delivery_date').datepicker({
				autoClose: true,
				startDate: dateToday,
				format: 'yyyy-mm-dd'
			});
		}

		$('document').ready(function(){
			check_state();
			setupDatePicker();
		})
    </script>

	<script type="text/javascript">
		$('.pay_option').on('click',function(val){
			if($(this).val() == "tempo"){
				$('.js_tempo_day_text').show(200);
			}else{
				$('.js_tempo_day_text').hide(200);
			}
		});
	</script>
@endsection
