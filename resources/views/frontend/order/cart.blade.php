@extends('frontend.layout.master')

@section('title', App\Page::createTitle('Cart'))

@section('customcss')
    <link rel='stylesheet' href='assets/plugin/bootstrap/css/bootstrap-responsive-tabs.css'>
    <link rel="stylesheet" type="text/css" href="assets/plugin/fancybox/css/jquery.fancybox.min.css">
@endsection

@section('content')

	<div class="main-content">
		<div class="container" id="cart">
			<div class="row hide-320">
				<div class="col-md-12">
					<span class="text-grey equal margin-bottom-35">
					  <a class="text-grey" href="#"><b>CART </b></a> /
					  <a class="text-grey" href="#"><b>ORDER SUMMARY </b></a>
					</span>
				</div>

			</div>
			<div class="row">
	                <div class="col-md-12 col-sm-10 col-xs-10 col-centered-768">
	                    <h5 class="title title-margin">ORDER SUMMARY</h5>
	                </div>
	            </div>
	            <div class="row">
	                <div class="col-md-10 col-sm-11 col-xs-11 col-centered">
	                    <b><span class="text-blue size-18">FOO</span><span class="text-red size-18">DIS GROCER</span></b>
	                    <br>
	                    <div class="seafer-grocer">
	                        <table class="bg-transparent">
	                            <tbody>
	                            <tr>
	                                <th>ITEMS</th>
	                                <th>SKU</th>
	                                <th>DESCRIPTION</th>
	                                <th width="15%">QUANTITY</th>
	                                <th>UNIT</th>
	                                <th>UNIT PRICE</th>
	                                <th>SUB TOTAL</th>
	                                <th>&nbsp;</th>
	                            </tr>
                                <?php
                                    $is_range_order = 0;
                                ?>
	                            @if( count($carts) > 0 )

	                            @foreach( $carts as $key=>$cartItem)
									<?php
										$range_price = \App\Models\Groceries::where('name',$cartItem->name)->first()->range_price;
									?>

								<tr id="{{ $key }}">
									<td>
										<div class="item-box">
											<div class="item small" style="background-image: url('{{ $cartItem->image }}');background-size: cover;background-position: center; ">
											</div>
										</div>
									</td>
									<td data-content="SKU">
										<span class="block">
											{{ $cartItem->sku }}
										</span>
									</td>
									<td>
										<span class="block">{{ $cartItem->name }}</span>
									</td>
									<td width="15%">
										<div class="counter">
											<button class="btn minus decrease_cart_item_qty">-</button>
											<input class="total" value="{{ $cartItem->quantity }}" type="text">
											<input class="cart_id" value="{{ $cartItem->id }}" type="hidden">
											<button class="btn plus increase_cart_item_qty">+</button>
										</div>
									</td>
									<td data-content="UNIT">
										@if(null !== $cartItem->unit_metric)
										{{ $cartItem->unit_metric }}
										@else
										Not Available
										@endif
									</td>
									<td data-content="UNIT PRICE">
										<?php $is_range_order = 0; ?>
										@if($range_price)
                                        <?php $is_range_order = 1; ?>
                                        {{Money::display($range_price - ($cartItem->price_per_item - $range_price))}} -
                                        @endif

										{{ Money::display($cartItem->price_per_item) }}
									</td>
									<td data-content="SUB TOTAL" class="item_total">
										{{ Money::display($cartItem->total()) }}
									</td>
									<td class="cpage">
										<button class="btn list_del_cart_item" style="color:red;font-size:20px"><strong>&times;</strong></button>
									</td>

								</tr>
								@endforeach
								@else
								<tr>
									<br/>
									<h4>No item in the cart</h4>
									<br/>
								</tr>
								@endif
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="progress" style="height:10px;">
                          <div class="progress-bar" role="progressbar"
                            aria-valuenow="95" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <h5>Minimum cart size is Rp. 500,000</h5>
                    </div>

                    <div class="row">
                        <div class="col-md-7 col-sm-6">
                            <div class="form-box promotional">
                                <span>Promotional Code / Coupon</span>

								@if($promo_applied == false)
                                {!! Form::open(['method'=>'POST', 'action'=>'GroceriesController@postPromoCode']) !!}
	                                <input class="code" type="text" name="promo">
	                                <button class="button btn-aneh use-promo-button" style="color:white">APPLY CODE</button>
                                {!! Form::close() !!}

                                @else
                                    {{-- <h4><strong>Promo {{ Session::get('is_promo') }} is applied</strong></h4> --}}
                                    @if($promo_detail->promo_type == 'percent')
                                    <h4><strong>Your {{ $promo_detail->code }} promo code has been claimed! A deduction of {{ number_format($promo_detail->discount_value, 0).' %' }} will be applied to your purchase!</strong></h4>
                                    @else
                                    <h4><strong>Your {{ Session::get('is_promo') }} promo code has been claimed! A deduction of {{ Money::display($promo_applied) }} will be applied to your purchase!</strong></h4>
                                    @endif
                                @endif
							</div>
						</div>
                        <div class="col-md-5 col-sm-6">
                            <table class="table-total bg-transparent">
                                <tbody>
	                                <tr>
	                                    <input name="base_total" value="{{ Carte::total() }}" type="hidden">
	                                    <td>SUB TOTAL</td>
	                                    <td id="cart_total" align="right"><span>{{ Money::display(Carte::total()) }}</span></td>
	                                </tr>

	                                <tr>
	                                    <td><span>PROMO CODE</span></td>
	                                    <td id="cart_promo" style="color:#ff0000;" align="right">(Rp <span class="promo-holder">{{ $promo_applied == false ? '0' : Money::onlyNominal($promo_applied) }}</span>)</td>
	                                </tr>
	                                <tr>
	                                    <td>&nbsp;</td>
	                                    <td>&nbsp;</td>
	                                </tr>
	                                <tr>
	                                    <td>TOTAL</td>
	                                    @if($promo_applied == false)
	                                        @if( ceil((Carte::total())/10) > 0 )

	                                            @if( (Carte::total()) + ceil((Carte::total()) /10) > 0)
	                                            <td id="cart_grand_total" align="right"><span>@if($is_range_order) <span style="font-size:9px"> estimated </span> @endif{{ Money::display( (Carte::total()) ) }}</span></td>
	                                            @else
	                                            <td id="cart_grand_total" align="right"><span>@if($is_range_order) <span style="font-size:9px"> estimated </span> @endif{{ Money::display(0) }}</span></td>
	                                            @endif

	                                        @else

	                                            @if( (Carte::total()) > 0)
	                                            <td id="cart_grand_total" align="right"><span>@if($is_range_order) <span style="font-size:9px"> estimated </span> @endif{{ Money::display( (Carte::total()) ) }}</span></td>
	                                            @else
	                                            <td id="cart_grand_total" align="right"><span>@if($is_range_order) <span style="font-size:9px"> estimated </span> @endif{{ Money::display(0) }}</span></td>
	                                            @endif

	                                        @endif
	                                    @else
	                                        @if( ceil((Carte::total() - $promo_applied)/10) > 0 )

	                                            @if( ((Carte::total() - $promo_applied))  > 0)
	                                            <td id="cart_grand_total" align="right"><span>@if($is_range_order) <span style="font-size:9px"> estimated </span> @endif{{ Money::display( (Carte::total() - $promo_applied)) }}</span></td>
	                                            @else
	                                            <td id="cart_grand_total" align="right"><span>@if($is_range_order) <span style="font-size:9px"> estimated </span> @endif{{ Money::display(0) }}</span></td>
	                                            @endif

	                                        @else

	                                            @if( (Carte::total() - $promo_applied)  > 0)
	                                            <td id="cart_grand_total" align="right"><span>@if($is_range_order) <span style="font-size:9px"> estimated </span> @endif{{ Money::display( (Carte::total() - $promo_applied) ) }}</span></td>
	                                            @else
	                                            <td id="cart_grand_total" align="right"><span>@if($is_range_order) <span style="font-size:9px"> estimated </span> @endif{{ Money::display(0) }}</span></td>
	                                            @endif

	                                        @endif
	                                    @endif
	                                 </tr>

									@if($is_range_order)
									<tr>
										<td>
											<p class="small">
												*Pesanan anda terdapat item yang harus kami timbang terlebih dahulu untuk mendapatkan harga pasti.
											</p>
										</td>
									</tr>
									@endif
	                                <tr>
	                                    <td colspan="2">
	                                        <a href="{{ url('checkout') }}">
											<button class="btn btn-payment btn-aneh js_button_payment" disabled>
	                                            PROCEED TO PAYMENT
	                                        </button></a>
	                                    </td>
	                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
		</div>
	</div>
	<div id="dialog" title="" style="display:none">

    </div>
@endsection

@section('scripts')
<script src="assets/plugin/bootstrap/js/jquery.bootstrap-responsive-tabs.min.js"></script>
<script src="assets/plugin/fancybox/js/jquery.fancybox.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>

	var checkSubtotal = function() {
            var subtotal = $('#cart_total').text().split('Rp ');
            subtotal = subtotal[1].replace('.','');
            return parseFloat(subtotal);
        }

        var checkPromo = function() {
            var promo = $('#cart_promo').text().split('Rp ');
            promo = promo[1].replace('.','');
            return parseFloat(promo);
        }

        var checkCurrentCashbackBalance = function() {
            var balance = $('#current-cashback-balance').text().replace('Rp ','');
            balance = $('#current-cashback-balance').text().replace('.','');
            return parseFloat(balance);
        }

        var calculateTax = function(a, b, c) {
            return Math.ceil(( (a - b - c) * 10)/100);
        }

        var calculateGrandTotal = function(total, tax, promo, cashback) {
            var grand = (total - cashback - promo) ;
            if(grand < 0){
                grand = 0;
            }
            return grand;
        }

        $(document).ready(function(){
			$('[data-toggle="tooltip"]').tooltip();

            var current_balance = checkCurrentCashbackBalance();

            $('.use-promo-button').on('click', function(e){
                e.preventDefault();
                var form = $(this).closest("form");

                $.ajax({
                    type:'post',
                    url: form.attr('action'),
                    data: {
                        promo:form.children('input[name="promo"]').val()
                    }
                }).done(function(result){
                    var json = $.parseJSON(result);
                    if (json.status == 'fail') {
                        alert(json.message);
                    } else {
                        var detail = json.item;

                        //var prom = Object.values(json.item);

                        rprc(detail.promo_type, detail.discount_value);

                        alert("Promo code applied");

                        if(detail.promo_type == 'percent') {
                            $('.promotional').html('<span>Promotional Code/Coupon</span><h4><strong>Your '+form.children('input[name="promo"]').val()+' promo code has been claimed! A deduction of '+Math.round(detail.discount_value)+' % will be applied to your purchase!</strong></h4>');
                        } else {
                            $('.promotional').html('<span>Promotional Code/Coupon</span><h4><strong>Your '+form.children('input[name="promo"]').val()+' promo code has been claimed! A deduction of Rp '+moneyFormat(detail.discount_value)+' will be applied to your purchase!</strong></h4>');
                        }

                        return false;

                    }

                })
            });

            $('.use-donation-button').on('click', function(e){
                e.preventDefault();
                var form = $(this).closest("form");

                $.ajax({
                    type:'post',
                    url: form.attr('action'),
                    data: {
                        donation:form.children('input[name="donation"]').val()
                    }
                }).done(function(result){
                    var json = $.parseJSON(result);
                    if (json.status == 'fail') {
                        /*showDialog('Warning', json.message);*/
                        alert(json.message);
                    } else {
                        var prom = Object.values(json.item);
                    }

                })
            });

            $('.use-referral-button').on('click', function(e){
                e.preventDefault();
                var form = $(this).closest("form");

                $.ajax({
                    type:'post',
                    url: form.attr('action'),
                    data: {
                        referral:form.children('input[name="referral"]').val()
                    }
                }).done(function(result){
                    var json = $.parseJSON(result);
                    if (json.status == 'fail') {
                        /*showDialog('Warning', json.message);*/
                        alert(json.message);
                    } else {
                        /*showDialog('Success', json.message);*/
                        alert(json.message);
                    }

                })
            });
        });


        var showDialog = function(title,message){
            $('#dialog').attr('title', title);
            $('#dialog').html('<p>'+message+'</p>');
            $('#dialog').dialog();
        }

        var rprc = function(type, value){
            var total = $('input[name="base_total"]').val();
            var promo_value = 0;
            if (type == 'percent'){
                promo_value = (total * value) / 100;
                $('.promo-holder').html(moneyFormat(promo_value));
            } else if(type == 'nominal') {
                promo_value = value;
                $('.promo-holder').html(moneyFormat(promo_value));
            }

            var prm = checkPromo();
            var subtot = checkSubtotal();

            $('#cart_total').text('Rp '+moneyFormat(subtot));

            if( (subtot - prm) > 0 ) {
                $('#cart_tax_of_total').text('Rp ' + moneyFormat(calculateTax( subtot, prm) ));
            } else {
                $('#cart_tax_of_total').text('Rp '+moneyFormat(0));
            }

            var ppn = Math.ceil((10*(subtot  - prm))/100);

            if(ppn < 0)
            {
                var gt = (total - promo_value );
            } else {
                var gt = (total - promo_value ) + ppn;
            }

            if (gt < 0) {
                gt = 0;
            }
            $('#cart_grand_total').html('Rp '+moneyFormat(gt));
        }

        var moneyFormat = function numberWithCommas(x) {
            var parts = x.toString().split(".");
            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            return parts.join(".");
        }

    </script>
@endsection
