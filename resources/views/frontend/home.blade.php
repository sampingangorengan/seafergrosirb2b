@extends('frontend.layout.master')

@section('customcss')
    <link rel='stylesheet' href='assets/plugin/bootstrap/css/bootstrap-responsive-tabs.css'>
    <link rel="stylesheet" type="text/css" href="assets/plugin/fancybox/css/jquery.fancybox.min.css">

@endsection

@section('home')
    {{-- <div class="loading-container">
      <div class="loading-box">
        <p>
          <img src="assets/images/logo.png" class="img-logo-loading">
          &nbsp
          <br>
          <img src="assets/images/load.gif" alt="">
        </p>
      </div>
    </div> --}}
@endsection

@section('content')

	<div id="godOfContent">
        <div class="newboxMenu after_clear">
            <ul id="grocerMenu" class="newleftMenu ul_nostyle">
                @foreach( App\Models\Groceries\Category::orderBy('order', 'asc')->get() as $category )

                <li>
                    <a href="{!! url('groceries/get-all/' . $category->slug) !!}">
                        <!-- <img src="{!! url() !!}{{ $category->image->file }}"> -->
                        <img src="{!! url() !!}{{ $category->image->file }}">
                        {{ $category->name }}
                    </a>

                    @if(count($category->child) > 0)
                    <div class="newSubmenu">
                        <ul class="ul_nostyle">
                            @foreach($category->childByOrder($category->id) as $child_category)
                             <li>
                                <a href="{!! url('groceries/' . $child_category->slug) !!}">
                                    {{ ucfirst($child_category->name) }}
                                </a>
                            </li>
                            @endforeach
                        </ul>
                    @endif
                </li>
                @endforeach
            </ul>
            <div class="slick-slider">
                <?php $i = 0 ?>
                @foreach (App\Models\Home\Slide::where('is_active', 1)->orderBy('order')->get() as $slide)
                    <?php $class = '' ?>
                    @if ($i == 0)
                        <?php $class = 'is-active' ?>
                    @endif
                <div>
                    <img src="{!! asset(null) !!}headlines/{{ $slide->file_name }}" alt="Responsive image">
                </div>
                    <?php $i++ ?>
                @endforeach
            </div>
        </div>
        <div class="feature-box">
            <div class="box-content" style="display:table">

                <div class="col-lg-offset-1 col-lg-2 col-md-4 col-sm-6 col-xs-6" style="height: 300px;">
                  <img src="{{$images[0]}}" class="feature-img img-responsive">
                    <h2 class="feature-title">KUALITAS TERJAMIN
                    </h2>
                    <h6 class="feature-desc">
                        Kualitas produk kita jaga
                        mulai dari Sumber penyimpanan
                        hingga pengiriman
                    </h6>
                </div>
                <div class="col-lg-2 col-md-4 col-sm-6 col-xs-6" style="height: 300px;">
                  <img src="{{$images[1]}}" class="feature-img img-responsive">
                    <h2 class="feature-title">HARGA TERJANGKAU
                    </h2>
                    <h6 class="feature-desc">
                        Seafer Grosir bekerja sama dengan
                        supplier untuk menawarkan
                    harga terjangkau
                    </h6>
                </div>
                <div class="col-lg-2 col-md-4 col-sm-6 col-xs-6" style="height: 300px;">
                  <img src="{{$images[2]}}" class="feature-img img-responsive">
                    <h2 class="feature-title">PRODUK LENGKAP
                    </h2>
                    <h6 class="feature-desc">
                        Ribuan produk kuliner tersedia untuk bisnis anda
                    </h6>
                </div>
                <div class=" col-lg-2 col-md-6 col-sm-6 col-xs-6" style="height: 300px;">
                  <img src="{{$images[3]}}" class="feature-img img-responsive">
                    <h2 class="feature-title">PEMBAYARAN FLEKSIBEL
                    </h2>
                    <h6 class="feature-desc">
                        Beli sekarang, bayar nanti sesuai ketentuan Seafer Grosir
                    </h6>
                </div>
                <div class="col-lg-2 col-md-6 col-sm-6 col-xs-6" style="height: 300px;">
                  <img src="{{$images[4]}}" class="feature-img img-responsive">
                    <h2 class="feature-title">GRATIS PENGIRIMAN DAN TEPAT WAKTU
                    </h2>
                    <h6 class="feature-desc">
                        Dapatkan ongkos kirim gratis dari pengiriman yang tepat dari logistik
                    </h6>
                </div>

            </div>


        </div>

		<!-- Main Content -->
		<div class="box-content">

			<!-- explore product -->
			<div class="exploreProd">
                <div class="row">
                    <div class="col-md-4"><hr></div>
                    <div class="col-md-4"><h3>OUR PRODUCTS</h3></div>
                    <div class="col-md-4"><hr></div>

                </div>

				@foreach($new_groceries_body as $grocery)
				@if($grocery->grocery_list->count() > 0)
                <div class="exploreProdList">
					<div class="headingTitle after_clear">
						<h1>{{ $grocery->name }}</h1>
						<a href="{!! url('groceries/' . $grocery->slug) !!}" class="linkMore">See all products</a>
					</div>
        			<div class="prodlistInner after_clear">

        				@foreach($grocery->grocery_list as $g)
          				<a class="prodItem" alt="{{ ucwords($g->name)}}" title="{{ ucwords($g->name) }}" href="{{ url('groceries/'.$g->id.'/'.str_slug($g->name)) }}">
							@if($g->discount_id != 0)
    							<div class="item-sale">
    								<img src="assets/images/ico-sale.png">
    							</div>
							@endif
            				<img src="{{ $g->getFirstImageUrl($g->id) }}" style="max-height:65%;width:auto">
							{{-- <img src="assets/images/products/crabs.jpg"> --}}
            				<div class="prodCaption">

                                 <span><b>{{$g->brand->name}}</b> - {{ str_limit($g->name, $limit = 10, $end = '...') }} / {{ $g->value_per_package }}{{ $g->metric_id != 0 ? $g->metric->abbreviation : 'metric unknown' }}</span><br>
                                <div class="packing">
									{{$g->packing}}

								</div>
								@if($g->discount_id != 0)
                                <span class="oth_desc oth_before">{{ Money::display($g->price) }}</span>
                                    @if($g->discount && $g->discount->discount_type == 'percent')
                                        @if($g->price - (($g->price * $g->discount->discount_value)/100) >0)
                                        <span class="oth_desc text-red">
                                            @if($g->range_price)
												{{Money::display($g->range_price - (($g->price * $g->discount->discount_value)/100))}} -
											@endif

                                            {{ Money::display( $g->price - (($g->price * $g->discount->discount_value)/100) )}}
                                        </span>
                                        @else
                                        <span class="oth_desc text-red"> {{ Money::display( 0 )}}</span>
                                        @endif
                                    @else
                                        @if($g->discount)
                                            @if( ($g->price - $g->discount->discount_value) > 0)
                                            <span class="oth_desc text-red">
                                                @if($g->range_price)
    												{{Money::display($g->range_price - $g->discount->discount_value)}} -
    											@endif
                                                {{ Money::display( $g->price - $g->discount->discount_value ) }}
                                            </span>
                                            @else
                                                <span class="oth_desc text-red"> {{ Money::display( 0 ) }}</span>
                                            @endif
                                        @endif
                                    @endif
                                @else
                                <span class="oth_desc text-red">
                                    @if($g->range_price)
									{{Money::display($g->range_price)}} -
									@endif
                                    {{ Money::display($g->price)}}
                                </span>
                                @endif
            				</div>
          				</a>
						@endforeach
        			</div>
      			</div>
    			<!-- Squid & Octopus -->
    			@endif
				@endforeach

		    </div>
		    <!-- explore product -->


  		</div>

        <div class="box-supplier-content" style="">
            <div class="content">
                <div class="supplier-signup">
                    <div class="cover-footer">
                        <div class="img-cover">
                            <h1>Bergabung Dengan Seafer Grosir Sebagai Supplier</h1>
                            <p>Perluas pasar anda sekarang
                            dan dapatkan akses ke ribuan bisnis kuliner
                            dengan cara yang <b>Mudah, Aman, dan Efisien !</b></p>
                             <a href="{{url('sign-up-supplier')}}" class="btn btn-supplier ">Daftar Menjadi Supplier</a>
                        </div>
                    </div>
                </img>

                </div>
            </div>
        </div>
    </div>



@endsection

@section('scripts')
    {{-- <script>
    $('.loading-container').show();
    $(window).load(function(){
        $('.loading-container').animate({
                opacity:0,
        },function(){
            $(this).remove();
        });
    });
    </script> --}}
@endsection
