@extends('frontend.layout.master')

@section('title', App\Page::createTitle('Sign Up'))

@section('customcss')

    <link rel='stylesheet' href='assets/plugin/bootstrap/css/bootstrap-responsive-tabs.css'>
    <link rel="stylesheet" type="text/css" href="assets/plugin/fancybox/css/jquery.fancybox.min.css">

@endsection

@section('content')
	<div class="main-content" >
        <div class="box-content"  >
            <div class="">
                <div class="content" style="margin-bottom:20px">
                    <div class="supplier-signup">
                        <div class="cover-footer">
                            <div class="img-cover-supplier"></div>
                            <button  class="btn btn-register ">Daftar Menjadi Supplier</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="">
                <div class="container">
                    <div class="supplier-content">
                        <div class="content">
                            <div class="col-md-6 col-sm-6">
                                <div class="supplier-benefits">
                                    <div class="title-benefits-supplier">
                                        <h2><u>SUPPLIER BENEFITS</u></h2>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                             <img src="/assets/images/supplier/ICON-11.png">
                                        </div>
                                        <div class="col-md-8 col-sm-8 col-xs-8">
                                            <h4>Akses ke Pasar</h4>
                                            <p>Perluas pasar anda dan akses ribuan bisnis kuliner di Seafer Grosir</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                             <img src="/assets/images/supplier/ICON-12.png">
                                        </div>
                                        <div class="col-md-8 col-sm-8 col-xs-8">
                                            <h4>Pengiriman dan penyimpanan produk tersedia</h4>
                                            <p>Tersedia armada pengiriman dan gudang penyimpanan dengan suhu yang tepat</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                             <img src="/assets/images/supplier/ICON-13.png">
                                        </div>
                                        <div class="col-md-8 col-sm-8 col-xs-8">
                                            <h4>Pembayaran Terjamin</h4>
                                            <p>Pembayaran terjamin dilunaskan dan supplier bebas dari penagihan</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                             <img src="/assets/images/supplier/ICON-14.png">
                                        </div>
                                        <div class="col-md-8 col-sm-8 col-xs-8">
                                            <h4>Gratis Biaya Listing</h4>
                                            <p>Jual dan promosi produk anda tanpa biaya listing</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                             <img src="/assets/images/supplier/ICON-15.png">
                                        </div>
                                        <div class="col-md-8 col-sm-8 col-xs-8">
                                            <h4>Digitalisasi Bisnis Anda</h4>
                                            <p>Tambahkan effesiensi bisnis anda lewat teknologi Seafer Grosir</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="img-supplier-phone">
                                    <img src="/assets/images/supplier/SP_BANNER_BENEFIT_SUPPLIER.png">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


		<div class="container" id="signUp">
			<div class="row">
				<div class="col-md-12">
					<h6 class="title">SIGN UP SUPPLIER</h6>
				</div>
			</div>
			<div class="row">
				@if (count($errors) > 0)

	            <div class="alert alert-danger">
	                <ul>
	                    @foreach ($errors->all() as $error)
	                    	<li>{{ $error }}</li>
	                    @endforeach
	                </ul>
	            </div>
	            @endif
			</div>
			<div class="row">
	            <div class="col-md-3 no-padding-h center hide-768">
	                <img class="pink" src="assets/images/sign-up-1.png">
	            </div>
	        	<form method="post" action="{{action('UserController@postSignUpSupplier')}}">
	        		{{ csrf_field() }}
					<div class="col-md-6 col-sm-8 col-xs-10 no-padding-h col-centered-768">
						<div class="form-box">
							<div class="tab">
								<div class="title-form-step">
									<h4>Step 1 of 2</h4>
									<h3>Company Information</h3>
								</div>
								<div class="row">
								    <div class="col-md-4">
								        <label for="middle-label" class="text-left middle">Company Name<span class="text-red" >*</span></label>
								    </div>
								    <div class="col-md-8">
								        <input type="text" class="required" name="company_name" placeholder="" value="{{ old('company_name') }}"/>
								    </div>
								</div>
								<div class="row">
								    <div class="col-md-4">
								        <label for="middle-label" class="text-left middle">Business entity<span class="text-red" >*</span></label>
								    </div>
								    <div class="col-md-8">
				                        <select class="short" name="business_entity" class="required">
				                            <option value="pt">PT.</option>
				                            <option value="cv">CV.</option>
				                            <option value="individual">Individual</option>
				                        </select>
				                    </div>
								</div>
								<div class="row">
								    <div class="col-md-4">
								        <label for="middle-label" class="text-left middle">Alamat<span class="text-red" >*</span></label>
								    </div>
								    <div class="col-md-8">
								        <textarea type="text" class="required" name="billing_address" placeholder=""/>{{ old('billing_address') }}</textarea>
								    </div>
								</div>
								<div class="row">
								    <div class="col-md-4"></div>
								    <div class="col-md-8">
				                        {!! Form::select('province', $provinces, null, ['class'=>'short', 'id' => 'select_province']) !!}
				                    </div>
								</div>
								<div class="row">
								    <div class="col-md-4"></div>
								    <div class="col-md-8">
								    	<select class="short" name="city" id="select_city">
								    		<option value="">Select city</option>
								    	</select>
				                    </div>
								</div>
								<div class="row">
								    <div class="col-md-4"></div>
								    <div class="col-md-8">
								    	<select class="short" name="area" id="select_area">
								    		<option value="">Select area</option>
								    	</select>
				                    </div>
								</div>
								<div class="row">
								    <div class="col-md-4"></div>
								    <div class="col-md-8">
								        <input type="text" class="required" name="postal_code" placeholder="Postal Code" value="{{ old('postal_code') }}"/>
								    </div>
								</div>
								<div class="row">
								    <div class="col-md-4">
								        <label for="middle-label" class="text-left middle">Title<span class="text-red" >*</span></label>
								    </div>
								    <div class="col-md-8">
								        <select class="short" name="title" class="required">
				                            <option value="mr">Mr</option>
				                            <option value="mrs">Mrs</option>
				                            <option value="ms">Ms</option>
				                        </select>
								    </div>
								</div>
								<div class="row">
								    <div class="col-md-4">
								        <label for="middle-label" class="text-left middle">Name<span class="text-red" >*</span></label>
								    </div>

								    <div class="col-md-8">
								        <input type="text" class="required" name="pic_name" placeholder="" value="{{ old('pic_name') }}"/>
								    </div>
								</div>
								<div class="row">
			                        <div class="col-md-4">
			                            <label for="middle-label" class="text-left middle">Phone Number<span class="text-red" >*</span></label>
			                        </div>
			                        <div class="col-md-8">
			                            <div class="input-group">
			                                <span class="input-group-label">+62</span>
			                                <input class="input-group-field required" type="text" name="phone_number" value="{{ old('phone_number') }}"/>
			                            </div>
			                        </div>
			                    </div>
								<div class="row">
								    <div class="col-md-4">
								        <label for="middle-label" class="text-left middle">Email<span class="text-red" >*</span></label>
								    </div>
								    <div class="col-md-8">
								        <input type="text" class="required" name="email" placeholder="" value="{{ old('email') }}"/>
								    </div>
								</div>
								<div class="row">
								    <div class="col-md-4">
								        <label for="middle-label" class="text-left middle">Website</label>
								    </div>
								    <div class="col-md-8">
								        <input type="text" name="website"  placeholder="" value="{{ old('website') }}"/>
								    </div>
								</div>
								<div class="row">
								    <div class="col-md-4">
								        <label for="middle-label" class="text-left middle">Category<span class="text-red" >*</span></label>
								    </div>
								    <div class="col-md-8">
				                        <select class="short required" name="category">
				                            <option value="principal">Principal</option>
				                            <option value="distributor">Distributor</option>
				                            <option value="trader">Trader</option>
				                        </select>
				                    </div>
								</div>
								<div class="row">
								    <div class="col-md-4">
								        <label for="middle-label" class="text-left middle">Commoditas<span class="text-red" >*</span></label>
								    </div>
								    <div class="col-md-8">
				                        <select class="short required" name="commoditas">
				                            <option value="seafood">Seafood</option>
				                            <option value="meat and poultry">Meat and Poultry</option>
				                            <option value="vegetables">Vegetables</option>
				                        </select>
				                    </div>
								</div>
								<div class="row">
								    <div class="col-md-4">
								        <label for="middle-label" class="text-left middle">Preferred Selling Method<span class="text-red" >*</span></label>
								    </div>
								    <div class="col-md-8">
				                        <select class="short required" name="selling_method">
				                            <option value="consignment">Consignment</option>
				                            <option value="direct_Purchase">Direct Purchase</option>
				                        </select>
				                    </div>
								</div>
			                    <div class="row">
								    <div class="col-md-4">
								        <label for="middle-label" class="text-left middle">Password<span class="text-red" >*</span></label>
								    </div>

								    <div class="col-md-8">
								        <input type="password" class="required" name="password" placeholder="" value="{{ old('password') }}"/>
								    </div>
								</div>
								<div class="row">
								    <div class="col-md-4">
								        <label for="middle-label" class="text-left middle">Confirm Password<span class="text-red" >*</span></label>
								    </div>

								    <div class="col-md-8">
								        <input type="password" class="required" name="cf_password" placeholder="" value="{{ old('cf_password') }}"/>
								    </div>
								</div>
			                    <div class="row">
			                        <div class="col-md-4"></div>
			                        <div class="col-md-8">
			                            <label for="middle-label" class="text-left middle"><span class="text-red" >*</span>) all fields with red dot are mandatory</label>
			                        </div>
			                    </div>
							</div>
							<div class="tab">
								<div class="title-form-step">
									<h4>Step 2 of 2</h4>
									<h3>Products</h3>
								</div>
								<div class="row">
								    <div class="col-md-4">
								        <label for="middle-label" class="text-left middle">Types of goods supplied<span class="text-red" >*</span></label>
								    </div>
								    <div class="col-md-8">
				                        <textarea type="text" class="required" name="types_of_goods" placeholder="" rows="4" cols="100"/>{{old('types_of_goods') }}</textarea>
				                    </div>
								</div>

								<div class="row">
								    <div class="col-md-4">
								        <label for="middle-label" class="text-left middle">Products Origin<span class="text-red" >*</span></label>
								    </div>
								    <div class="col-md-8">
								        <div class="box">
											<input class="checkbox" type="checkbox" name="product_origin[]" value="Local">
											<label for="checkbox" class="agree">Local</label>
										</div>
								    </div>
								</div>
								<div class="row">
								    <div class="col-md-4"></div>
								    <div class="col-md-8">
								        <div class="box">
											<input class="checkbox" type="checkbox" name="product_origin[]" value="Import">
											<label for="checkbox" class="agree">Import</label>
										</div>
								    </div>
								</div>
								<div class="row">
								    <div class="col-md-4">
								        <label for="middle-label" class="text-left middle">Country<span class="text-red" >*</span></label>
								    </div>
								    <div class="col-md-8">
				                        <select class="short requiredCheckboxes" name="country">
				                        	@foreach($country as $value =>$key)
				                            <option value="{{$value}}">{{$key}}</option>
				                            @endforeach
				                        </select>
				                    </div>
								</div>
			                    <div class="row">
			                        <div class="col-md-4">

			                        </div>
			                        <div class="col-md-8">
			                            <label for="middle-label" class="text-left middle"><span class="text-red" >*</span>) all fields with red dot are mandatory</label>
			                        </div>
			                    </div>
			                    <div class="row">
			                        <div class="col-md-9 col-md-offset-4">
			                            <div class="box">
			                                <input id="checkbox1" class="checkbox" type="checkbox" required="" name="agree">
			                                <label for="checkbox1" class="agree">By submitting this form, I confirm that I have read and agree to Seafer Grosir's Term &amp; Conditions and Privacy Policy
			                                </label>
			                            </div>
			                        </div>
			                    </div>
							</div>

							<div style="overflow:auto;">
								<div style="float:right;">
									<button class="btn btn-success nextBtn btn-lg pull-left" type="button" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
									<button class="btn btn-primary nextBtn btn-lg pull-right" type="button" id="nextBtn" onclick="nextPrev(1)">Next</button>
									<button class="btn btn-primary nextBtn btn-lg pull-right" type="submit" id="button_submit">Submit</button>
								</div>
							</div>
							<!-- Circles which indicates the steps of the form: -->
							<div style="text-align:center;margin-top:40px;">
								<span class="step"></span>
								<span class="step"></span>
							</div>
						</div>
					</div>
				</form>

	           	<div class="col-md-3 no-padding center hide-768">
	                <img class="blue" src="assets/images/sign-up-2.png">
	            </div>
	        </div>
		</div>
	</div>
@endsection

@section('scripts')
<script src="assets/plugin/bootstrap/js/jquery.bootstrap-responsive-tabs.min.js"></script>
<script src="assets/plugin/fancybox/js/jquery.fancybox.min.js"></script>
<!-- AJAX province -->
<script>

    $('.btn-register').click(function(){
        $('html,body').animate({
        scrollTop: $("#signUp").offset().top},
        'slow');
    });

    @if (count($errors) > 0)
        $('html,body').animate({
            scrollTop: $("#signUp").offset().top},
        'slow');
    @endif

	$('#select_province').on('change', function(){
        var province_id = $(this).val();
        $.ajax({
            type: 'get',
            url: '{{ url('user/ajax-cities-by-province') }}/' + province_id
        }).done(function(result){
            result = jQuery.parseJSON(result);
            if(result.status == 'success') {
                $('#select_city').html('');
                $.each(result.cities, function(k, v) {
                    $('#select_city').append('<option value="'+k+'">'+v+'</option>');
                });


            } else {
                alert(result.message);
            }
        })
    });

    $('#select_city').on('change', function(){
        var city_id = $(this).val();
        $.ajax({
            type: 'get',
            url: '{{ url('user/ajax-areas-by-city/') }}/' + city_id
        }).done(function(result){
            result = jQuery.parseJSON(result);
            if(result.status == 'success') {
                $('#select_area').html('');
                $.each(result.areas, function(k, v) {
                    $('#select_area').append('<option value="'+k+'">'+v+'</option>');
                });

            } else {
                alert(result.message);
            }
        })

    });
</script>
<script>
    var requiredCheckboxes = $(':checkbox[required]');

    requiredCheckboxes.change(function(){
        if(requiredCheckboxes.is(':checked')) {
            requiredCheckboxes.removeAttr('required');
        }
        else {
            requiredCheckboxes.attr('required', 'required');
        }
    });
</script>
<!-- Form Step -->
<script type="text/javascript">
	var currentTab = 0; // Current tab is set to be the first tab (0)
	showTab(currentTab); // Display the crurrent tab

	function showTab(n) {
		// This function will display the specified tab of the form...
		var x = document.getElementsByClassName("tab");
		x[n].style.display = "block";
		//... and fix the Previous/Next buttons:
		if (n == 0) {
			document.getElementById("prevBtn").style.display = "none";
			$("#button_submit").hide();
			$("#nextBtn").show();
		} else {
			document.getElementById("prevBtn").style.display = "inline";
			$("#button_submit").hide();
			$("#nextBtn").show();
		}
		if (n == (x.length - 1)) {

			$("#nextBtn").hide();
			$("#button_submit").show();
			//document.getElementById("nextBtn").innerHTML = "Submit";
			$('#button_submit').on('click', function () {
			    $(this).parents("form:first").submit();
			});
		} else {
			document.getElementById("nextBtn").innerHTML = "Next";

		}
		//... and run a function that will display the correct step indicator:
		fixStepIndicator(n)
	}

	function nextPrev(n) {
		// This function will figure out which tab to display
		var x = document.getElementsByClassName("tab");
		// Exit the function if any field in the current tab is invalid:
		if (n == 1 && !validateForm()) return false;
		// Hide the current tab:
		x[currentTab].style.display = "none";
		// Increase or decrease the current tab by 1:
		currentTab = currentTab + n;


		// if you have reached the end of the form...
		if (currentTab >= x.length) {
		// ... the form gets submitted:
			document.getElementById("regForm").submit();
			return false;
		}
		// Otherwise, display the correct tab:
		showTab(currentTab);
	}

	function validateForm() {
		// This function deals with validation of the form fields
		var x, y, i, valid = true;
		x = document.getElementsByClassName("tab");
		y = x[currentTab].getElementsByClassName("required");
		// A loop that checks every input field in the current tab:
		for (i = 0; i < y.length; i++) {
			// If a field is empty...
			if (y[i].value == "") {
				// add an "invalid" class to the field:
				y[i].className += " invalid";
				// and set the current valid status to false
				valid = false;
			}
		}
		// If the valid status is true, mark the step as finished and valid:
		if (valid) {
			document.getElementsByClassName("step")[currentTab].className += " finish";
		}
		return valid; // return the valid status
	}

	function fixStepIndicator(n) {
		// This function removes the "active" class of all steps...
		var i, x = document.getElementsByClassName("step");
		for (i = 0; i < x.length; i++) {
			x[i].className = x[i].className.replace(" active", "");
		}
		//... and adds the "active" class on the current step:
		x[n].className += " active";
	}
</script>

@endsection
