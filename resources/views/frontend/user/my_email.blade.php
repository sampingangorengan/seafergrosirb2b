@extends('frontend.layout.master')

@section('title', App\Page::createTitle('My Email'))

@section('customcss')
    <link rel='stylesheet' href='assets/plugin/bootstrap/css/bootstrap-responsive-tabs.css'>
    <link rel="stylesheet" type="text/css" href="assets/plugin/fancybox/css/jquery.fancybox.min.css">
@endsection

@section('content')
	<div class="main-content">
		<div class="container">
			<div class="row hide-320">
				<div class="col-md-12">
					<span class="text-grey equal margin-bottom-35">
					  <a class="text-grey" href="{{ url('/') }}"><b>HOME</b></a> /
					  <a class="text-grey" href="{{ url('user/my_account') }}"><b>MY ACCOUNT </b></a> /
					  <a class="text-grey" href="#"><b>MY EMAIL PREFERENCE </b></a>
					</span>
				</div>
			
			</div>
			 <div class="row">
	            <div class="col-md-12 col-sm-10 col-xs-10 col-centered-768">
	                <h5 class="title title-margin">MY EMAIL PREFERENCE</h5>
	            </div>
	        </div>
			<div class="row">
	            <div class="col-md-3 center hide-768">
	                <img class="left" src="assets/images/box-fly.png">
	            </div>
	            <div class="col-md-6 col-sm-7 col-xs-10 col-centered-768">
	                <p class="semi" style="text-align:center">
	                    Here you can manage which kind of mail you want to be subscribed to.<br>
	                    To update your email please visit my account.
	                </p>
	                <div class="box text-left">
	                    <input id="latest_news" type="checkbox" name="latest_news" @if(auth()->user()->email_preference->latest_news == 1)checked="checked"@endif>
	                    <label for="latest_news">Send me the latest news from Seafer Grosir</label>
	                </div>
	                <hr>
	                <p class="semi" style="margin-bottom: 10px;">
	                    Choose the type of e-mail you would like to receive:
	                </p>
	                <div class="box text-left">
	                    <input id="promo" type="checkbox" name="promo" @if(auth()->user()->email_preference->promo == 1)checked="checked"@endif>
	                    <label for="promo">Promo</label>
	                </div>
	                <div class="box text-left">
	                    <input id="new_product" type="checkbox" name="new_product" @if(auth()->user()->email_preference->new_product == 1)checked="checked"@endif>
	                    <label for="new_product">New Products</label>
	                </div>
	                <div class="box text-left">
	                    <input id="editorial" type="checkbox" name="editorial" @if(auth()->user()->email_preference->editorial == 1)checked="checked"@endif>
	                    <label for="editorial">Editorials</label>
	                </div>
	                <div class="box text-left">
	                    <input id="low_stock" type="checkbox" name="low_stock" @if(auth()->user()->email_preference->low_stock == 1)checked="checked"@endif>
	                    <label for="low_stock">Low stock alert</label>
	                </div>
	            </div>
				<div class="col-md-3 col-sm-10 col-xs-10 col-centered-768">
				<div class="col-md-12 col-sm-6 col-xs-6 hide-dekstop right">
					 <img src="assets/images/box-fly.png">
				</div>
				<div class="col-md-12 col-sm-6 col-xs-6">
					<img src="assets/images/bin-box.png">
				</div>
	          </div>

			</div>
		</div>
	</div>
@endsection

@section('scripts')
<script src="assets/plugin/bootstrap/js/jquery.bootstrap-responsive-tabs.min.js"></script>
<script src="assets/plugin/fancybox/js/jquery.fancybox.min.js"></script>
<script>
	var doAjax = function(type, new_status) {
		$.ajax({
            type: 'post',
            url: '{{ url('user/update-email-preference') }}',
            data: {
                    type:type,
                    new_status:new_status
                },
        }).done(function(result){
            if(result.status == 'OK') {
                alert('Preferences Updated');
                location.reload();
            } else {
                alert(result.message);
            }
        })
	}

	var getPromoValue = function() {
		var value = $('#promo').prop('checked');

		if(value == true) {
			return 1;	
		} else {
			return 0;
		}
	}

	var getLatestNewsValue = function() {
		var value =  $('#latest_news').prop('checked');
		if(value == true) {
			return 1;	
		} else {
			return 0;
		}
	}

	var getNewProductValue = function() {
		var value =  $('#new_product').prop('checked');
		if(value == true) {
			return 1;	
		} else {
			return 0;
		}
	}

	var getEditorialValue = function() {
		var value =  $('#editorial').prop('checked');
		if(value == true) {
			return 1;	
		} else {
			return 0;
		}
	}

	var getLowStockValue = function() {
		var value =  $('#low_stock').prop('checked');
		if(value == true) {
			return 1;	
		} else {
			return 0;
		}
	}

	$(document).ready(function() {
		$('input[name="promo"]').on('change', function() {
			var value = getPromoValue();
			doAjax('promo', value);
		});

		$('input[name="latest_news"]').on('change', function() {
			var value = getLatestNewsValue();
			doAjax('latest_news', value);
		});

		$('input[name="editorial"]').on('change', function() {
			var value = getEditorialValue();
			doAjax('editorial', value);
		});

		$('input[name="low_stock"]').on('change', function() {
			var value = getLowStockValue();
			doAjax('low_stock', value);
		});

		$('input[name="new_product"]').on('change', function() {
			var value = getNewProductValue();
			doAjax('new_product', value);
		});
	});
</script>
@endsection