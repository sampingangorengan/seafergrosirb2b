@extends('frontend.layout.master')

@section('title', App\Page::createTitle('My Account'))

@section('customcss')
    <link rel='stylesheet' href='assets/plugin/bootstrap/css/bootstrap-responsive-tabs.css'>
    <link rel="stylesheet" type="text/css" href="assets/plugin/fancybox/css/jquery.fancybox.min.css">
@endsection

@section('content')
<div class="main-content">
	<div class="container" id="checkout">
		<div class="row">
          <div class="col-md-5 col-xs-10 col-centered"><br><br>
            <h5 class="title title-margin">Reset Password</h5>
            <p class="b2">
              </p><center>
                Hi  You can reset your Password Here
              </center>
            <p></p>
          </div>
        </div>
        <div class="row">
            @if (count($errors) > 0)

            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
        </div>
        <div class="row">
          <div class="col-md-3 hide-768">
            <img class="pink" src="assets/images/sign-up-1.png">
          </div>
          <div class="col-md-6 col-sm-8 col-xs-8 col-centered-768">
            <div class="menu-account-box">
              <form method="POST" action="{{ url('password/reset') }}">
                  {!! csrf_field() !!}
                  <input type="hidden" name="token" value="{{ $token }}">
                  <div class="row">
                        <div class="col-md-4">
                            <label for="middle-label" class="text-left middle">Email<span class="text-red" >*</span></label>
                        </div>
                        <div class="col-md-8">
                            <input class="required" type="text" name="email" placeholder="" value="{{ old('email') }}"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label for="middle-label" class="text-left middle">New Password<span class="text-red" >*</span></label>
                        </div>
                        <div class="col-md-8">
                            <input class="required" type="password" name="password" placeholder="" value="{{ old('password') }}"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label for="middle-label" class="text-left middle">Confirm Password<span class="text-red" >*</span></label>
                        </div>
                        <div class="col-md-8">
                            <input class="required" type="password" name="password_confirmation" placeholder="" value=""/>
                        </div>
                    </div>
                    <div class="row center">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
          </div>
          <div class="col-md-3 col-sm-10 col-xs-10 col-centered-768">
			<div class="col-md-12 col-sm-6 col-xs-6 no-padding-h hide-dekstop right">
				 <img class="img-market" src="assets/images/img-market.png">
			</div>
			<div class="col-md-12 col-sm-6 col-xs-6 no-padding-h">
				<img class="blue" src="assets/images/sign-up-2.png">
			</div>
          </div>
        </div>
	</div>
</div>
@endsection

@section('scripts')
<script src="assets/plugin/bootstrap/js/jquery.bootstrap-responsive-tabs.min.js"></script>
<script src="assets/plugin/fancybox/js/jquery.fancybox.min.js"></script>
@endsection
