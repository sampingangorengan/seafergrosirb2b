@extends('frontend.layout.master')

@section('title', App\Page::createTitle('My Profile'))

@section('customcss')
    <link rel='stylesheet' href='assets/plugin/bootstrap/css/bootstrap-responsive-tabs.css'>
    <link rel="stylesheet" type="text/css" href="assets/plugin/fancybox/css/jquery.fancybox.min.css">
@endsection

@section('content')
	<div class="main-content">
		<div class="container" id="signUp">
			<div class="row">
				<div class="col-md-12">

					<h5 class="title title-margin">MY PROFILE </h5>
				</div>
			</div>
			<div class="row">
				@if (count($errors) > 0)

	            <div class="alert alert-danger">
	                <ul>
	                    @foreach ($errors->all() as $error)
	                    	<li>{{ $error }}</li>
	                    @endforeach
	                </ul>
	            </div>
	            @endif
			</div>
			<div class="row">
				<div class="col-md-8 col-sm-8 col-xs-10 col-centered">
					<div class="form-box">
						<div class="row">
							{!! Form::model($user,['method'=>'POST','action'=>['UserController@postEditGeneralData', $user->id], 'files' => true]) !!}
							<div class="row ">
								<div class="background-profile">
									<div class="col-md-offset-4 col-md-8">

										@if($user->profile_picture)
		                                <div class="preview img-wrapper col-centered-768 js-user" style="background-image:url('profile_picture/{{ auth()->user()->photo->file }}')">
											<img src="" alt="">
		                                @else
		                                    <div class="preview img-wrapper col-centered-768">
		                                @endif

										<div class="file-upload-wrapper">
											<input name="file" class="file-upload-profile-native" accept="image/*" type="file">
											<input disabled class="file-upload-profile-text btn-upload" type="text">
										</div>

										</div>
									</div>
									<div align="center">
										<div>
											<h3><b>{{$user->company_name}}</b></h3>
										</div>
									</div>
								</div>
							</div>
							<div class="informasi_table">
								<div class="title_info" align="center"><h4><b>INFORMASI PERUSAHAAN</b></h4></div>
								<div class="row">
									<div class="col-md-4">
										<label for="middle-label" class="text-left middle hide-768"><span class="required">Nama Perusahaan</span></label>
									</div>
									<div class="col-md-8">
										{!! Form::text('company_name', null, ['class'=>'form-control']) !!}
									</div>
								</div>
								<div class="row">
									<div class="col-md-4">
										<label for="middle-label" class="text-left middle"><span class="required">Nama Brand</span></label>
									</div>
									<div class="col-md-8">
										{!! Form::text('brand_name', null, ['class'=>'form-control']) !!}
									</div>
								</div>
								<div class="row">
									<div class="col-md-4">
										<label for="middle-label" class="text-left middle"><span class="required">Nama Entitas</span></label>
									</div>
									<div class="col-md-8">
										<select name="business_entity" required="">
											<option value="pt" {{ $user->business_entity == 'pt' ? 'selected' : ''}}>PT</option>
											<option value="cv" {{ $user->business_entity == 'cv' ? 'selected' : ''}}>CV</option>
											<option value="individual" {{ $user->business_entity == 'individual' ? 'selected' : ''}}>Individual</option>
										</select>
									</div>
								</div>
								<div class="row">
									<div class="col-md-4">
										<label for="middle-label" class="text-left middle"><span class="required">Company Address</span></label>
									</div>
									<div class="col-md-8">
										{!! Form::text('billing_address', $user->billing_address, ['class'=>'form-control']) !!}
									</div>
								</div>
								<div class="row">
									<div class="col-md-4">
										<label for="middle-label" class="text-left middle"><span class="required">Province</span></label>
									</div>
									<div class="col-md-8">
										<select name="province" id="select_province">
						                    @foreach($provinces as $province => $nameprovince)
						                    <option value="{{$province}}" <?php if($user->province == $province){echo 'selected';}?>>{{$nameprovince}}</option>
						                    @endforeach
						                </select>
									</div>
								</div>
								<div class="row">
									<div class="col-md-4">
										<label for="middle-label" class="text-left middle"><span class="required">City</span></label>
									</div>
									<div class="col-md-8">
										<select name="city" id="select_city">
						                    @foreach($cities as $city => $namecity)
						                    <option value="{{$city}}" <?php if($user->city == $city){echo 'selected';}?>>{{$namecity}}</option>
						                    @endforeach
						                </select>
									</div>
								</div>
								<div class="row">
									<div class="col-md-4">
										<label for="middle-label" class="text-left middle"><span class="required">Area</span></label>
									</div>
									<div class="col-md-8">
										<select name="area" id="select_area">
						                    @foreach($areas as $area => $namearea)
						                    <option value="{{$area}}" <?php if($user->area == $area){echo 'selected';}?>>{{$namearea}}</option>
						                    @endforeach
						                </select>
									</div>
								</div>
								<div class="row">
									<div class="col-md-4">
										<label for="middle-label" class="text-left middle phone"><span class="required">Kode Pos</span></label>
									</div>
									<div class="col-md-8">
										<div class="input-group">
											{!! Form::text('postal_code', null, ['class'=>'input-group-field check-input postal_code']) !!}
										</div>
									</div>
								</div>
								<div class="row">
								    <div class="col-md-4">
								        <label for="middle-label" class="text-left middle">Shipping Address<span class="text-red" >*</span></label>
								    </div>
								    <div class="col-md-8">
								    	<div class="row">
								    		<div class="col-md-12">
								    			<input type="radio"  name="choose_shipping_address" value="billing_address" @if($user->choose_shipping_address == "billing_address")checked @endif> Use Billing Address as Shipping Address
								    		</div>
								    		<div class="col-md-12">
								    			<input type="radio" name="choose_shipping_address" value="another_address" id="another_address"@if($user->choose_shipping_address == "another_address")checked @endif> Provide Another Address
								    		</div>
								    	</div>
								    </div>
								</div>


								<div class="another_address">
									<div class="row">
									    <div class="col-md-4">
									    	<label for="middle-label" class="text-left middle">Alamat<span class="text-red" >*</span></label>
									    </div>
									    <div class="col-md-8">
									        <textarea type="text" name="shipping_address" placeholder=""/></textarea>
									    </div>
									</div>
									<div class="row">
									    <div class="col-md-4"></div>
									    <div class="col-md-8">
					                        {!! Form::select('shipping_province', $provinces, null, ['class'=>'short', 'id' => 'shipping_province']) !!}
					                    </div>
									</div>
									<div class="row">
									    <div class="col-md-4"></div>
									    <div class="col-md-8">
									    	{!! Form::select('shipping_city', $cities, null, ['class'=>'short', 'id' => 'shipping_city']) !!}

					                    </div>
									</div>
									<div class="row">
									    <div class="col-md-4"></div>
									    <div class="col-md-8">
									    	{!! Form::select('shipping_area', $areas, null, ['class'=>'short', 'id' => 'shipping_area']) !!}

					                    </div>
									</div>
									<div class="row">
									    <div class="col-md-4"></div>
									    <div class="col-md-8">
									        <input type="text" name="shipping_postal_code" placeholder="Postal Code" value=""/>
									    </div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-4">
										<label for="middle-label" class="text-left middle phone"><span class="required">Telepon Kantor</span></label>
									</div>
									<div class="col-md-8">
										<div class="input-group">
											<span class="input-group-label">+62</span>
											{!! Form::text('company_phone', null, ['class'=>'input-group-field check-input phone_number']) !!}
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-4">
										<label for="middle-label" class="text-left middle phone"><span class="required">Email Kantor</span></label>
									</div>
									<div class="col-md-8">
										<div class="input-group">
											{!! Form::text('company_email', null, ['class'=>'input-group-field check-input company_email']) !!}
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-4">
										<label for="middle-label" class="text-left middle phone">Website</label>
									</div>
									<div class="col-md-8">
										<div class="input-group">
											{!! Form::text('website', null, ['class'=>'input-group-field check-input website']) !!}
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-md-4">
										<label for="middle-label" class="text-left middle"><span class="required">Kategori</span></label>
									</div>
									<div class="col-md-8">
										<select name="category" required="">

											@foreach($categorys as $category)
				                            <option value="{{$category->id}}" {{ $user->category == $category->id ? 'selected' : ''}}>{{$category->category_name}}</option>
				                            @endforeach
				                        </select>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12 right">
										<button class="btn btn-save btn-long">SAVE</button>
									</div>
								</div>
								{!! Form::close() !!}
							</div>


							<hr class="blue">

							<div class="informasi_table">
								<div class="title_info" align="center"><h4><b>INFORMASI PRIBADI</b></h4></div>
								{!! Form::model($user,['method'=>'POST', 'action'=>['UserController@postEditDataPribadi', $user->id]]) !!}
								<div class="row">
									<div class="col-md-4">
										<label for="middle-label" class="text-left middle"><span class="required">Title</span></label>
									</div>
									<div class="col-md-8">
										<select name="sex" required="">
											<option value="m" {{ $user->sex == 'm' ? 'selected' : ''}}>Mr</option>
											<option value="fs" {{ $user->sex == 'fs' ? 'selected' : ''}}>Mrs</option>
											<option value="f" {{ $user->sex == 'f' ? 'selected' : ''}}>Ms</option>
										</select>
									</div>
								</div>

								<div class="row">
									<div class="col-md-4">
										<label for="middle-label" class="text-left middle phone"><span class="required">Nama</span></label>
									</div>
									<div class="col-md-8">
										<div class="input-group">
											{!! Form::text('name', null, ['class'=>'input-group-field check-input name']) !!}
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-md-4">
										<label for="middle-label" class="text-left middle phone"><span class="required">Email</span></label>
									</div>
									<div class="col-md-8">
										<div class="input-group">
											{!! Form::text('email', null, ['class'=>'input-group-field check-input email']) !!}
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-4">
										<label for="middle-label" class="text-left middle phone"><span class="required">Telepon</span></label>
									</div>
									<div class="col-md-8">
										<div class="input-group">
											<span class="input-group-label">+62</span>
											{!! Form::text('phone_number', null, ['class'=>'input-group-field check-input telepon']) !!}
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-4">
										<label for="middle-label" class="text-left middle">CURRENT PASSWORD</label>
									</div>
									<div class="col-md-8">
										<input placeholder="" class="check-input"  name="old_password" type="password">
									</div>
								</div>
								<div class="row">
									<div class="col-md-4">
										<label for="middle-label" class="text-left middle"><span class="required">Password</span></label>
									</div>
									<div class="col-md-8">
										<input placeholder="" class="check-input" name="password" type="password">
									</div>
								</div>
								<div class="row">
									<div class="col-md-4">
										<label for="middle-label" class="text-left middle"><span class="required">Confirm Password</span></label>
									</div>
									<div class="col-md-8">
										<input placeholder="" class="check-input" name="password_confirmation" type="password">
									</div>
								</div>
								<div class="row">
									<div class="col-md-12 right">
										<button class="btn btn-save btn-long">SAVE</button>
									</div>
								</div>
								{!! Form::close() !!}
							</div>
							<hr class="blue">

							<div class="informasi_table">
								{!! Form::model($user,['method'=>'POST','action'=>['UserController@postFileUpload', $user->id], 'files' => true]) !!}
								<div class="row">
									<div class="col-md-6">
										<p><h3 align="center">KTP</h3></p>
										@if($user->ktp)
		                                <div class="preview_adding_files img-wrapper col-centered-768"  style="background-image:url('ktp_user/{{$user->ktp}}')">
		                                @else

		                                    <div class="preview_adding_files img-wrapper col-centered-768 js-ktp"  style="background-image:url('assets/images/KTP.png')">
		                                @endif

											<div class="file-upload-wrapper">
												<input name="ktp_upload" class="file-upload-native" accept="image/*" type="file">
												<input disabled class="file-upload-text-adding-files btn-upload" type="text" >
											</div>

										</div>
									</div>

									<div class="col-md-6">
										<p><h3 align="center">NPWP</h3></p>
										@if($user->upload_npwp)
										<div class="preview_adding_files2 img-wrapper col-centered-768" style="background-image:url('npwp_user/{{$user->upload_npwp}}')">
		                                @else

		                                <div class="preview_adding_files2 img-wrapper col-centered-768 js-npwp" style="background-image:url('assets/images/NPWP.png')">
		                                @endif

											<div class="file-upload-wrapper">
												<input name="npwp_upload" class="file-upload-native" accept="image/*" type="file">
												<input disabled class="file-upload-text-adding-files btn-upload" type="text">
											</div>

										</div>
									</div>

									<div class="row">
										<div class="col-md-12 right">
											<button class="btn btn-save btn-long">SAVE KTP & NPWP</button>
										</div>
									</div>
								{!! Form::close() !!}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
@endsection

@section('scripts')
<script src="assets/plugin/bootstrap/js/jquery.bootstrap-responsive-tabs.min.js"></script>
<script src="assets/plugin/fancybox/js/jquery.fancybox.min.js"></script>
<script>
	var isEdited = false;

	function readURL(input,targetClass) {

	  if (input.files && input.files[0]) {
	    var reader = new FileReader();

		reader.onload = function(e) {
			$('.'+targetClass).css('background-image', "url("+e.target.result+")");
	    }

	    reader.readAsDataURL(input.files[0]);
	  }
	}

	$('input[name="ktp_upload"]').change(function(){
		readURL(this,"js-ktp");
	});
	$('input[name="npwp_upload"]').change(function(){
		readURL(this,"js-npwp");
	});

	$('input[name="file"]').change(function(){
		readURL(this,"js-user");
	});
	$('input').change(function(){
		isEdited = true;
	})
	$('#select_province').on('change', function(){
        var province_id = $(this).val();
        $.ajax({
            type: 'get',
            url: '{{ url('user/ajax-cities-by-province') }}/' + province_id
        }).done(function(result){
            result = jQuery.parseJSON(result);
            if(result.status == 'success') {
                $('#select_city').html('');
                $.each(result.cities, function(k, v) {
                    $('#select_city').append('<option value="'+k+'">'+v+'</option>');
                });


            } else {
                alert(result.message);
            }
        })
    });

    $('#select_city').on('change', function(){
        var city_id = $(this).val();
        $.ajax({
            type: 'get',
            url: '{{ url('user/ajax-areas-by-city/') }}/' + city_id
        }).done(function(result){
            result = jQuery.parseJSON(result);
            if(result.status == 'success') {
                $('#select_area').html('');
                $.each(result.areas, function(k, v) {
                    $('#select_area').append('<option value="'+k+'">'+v+'</option>');
                });

            } else {
                alert(result.message);
            }
        })

    });


    $('#shipping_province').on('change', function(){
        var province_id = $(this).val();
        $.ajax({
            type: 'get',
            url: '{{ url('user/ajax-cities-by-province') }}/' + province_id
        }).done(function(result){
            result = jQuery.parseJSON(result);
            if(result.status == 'success') {
                $('#shipping_city').html('');
                $.each(result.cities, function(k, v) {
                    $('#shipping_city').append('<option value="'+k+'">'+v+'</option>');
                });

            } else {
                alert(result.message);
            }
        })
    });
    $('#shipping_city').on('change', function(){
        var city_id = $(this).val();
        $.ajax({
            type: 'get',
            url: '{{ url('user/ajax-areas-by-city/') }}/' + city_id
        }).done(function(result){
            result = jQuery.parseJSON(result);
            if(result.status == 'success') {
                $('#shipping_area').html('');
                $.each(result.areas, function(k, v) {
                    $('#shipping_area').append('<option value="'+k+'">'+v+'</option>');
                });

            } else {
                alert(result.message);
            }
        })

    });
</script>
<!-- Radio validator -->
<script type="text/javascript">
	$(document).ready(function () {
	    $('input[type="radio"]').click(function () {
	        if ($(this).attr("value") == "another_address") {
	            $(".another_address").show();
	        }
	        if ($(this).attr("value") == "billing_address") {
	            $(".another_address").hide();
	        }
	    });
	    if (document.getElementById("another_address").checked == true) {
	        $(".another_address").show();
	    }
	});
</script>

@endsection
