@extends('frontend.layout.master')

@section('title', App\Page::createTitle('Order Detail'))

@section('customcss')
    <link rel='stylesheet' href='assets/plugin/bootstrap/css/bootstrap-responsive-tabs.css'>
    <link rel='stylesheet' href='assets/plugin/bootstrap/css/datepicker.css'>
    <link rel="stylesheet" type="text/css" href="assets/plugin/fancybox/css/jquery.fancybox.min.css">
    <link rel="stylesheet" type="text/css" href="assets/plugin/jquery/css/jquery-ui.min.css">
@endsection

@section('content')
	<div class="main-content">
		<div class="container" id="checkout">
			<div class="row hide-320">
				<div class="col-md-12">
					<span class="text-grey equal margin-bottom-35">
					  <a class="text-grey" href="{{ url('user/my-account') }}"><b>MY ACCOUNT</b></a> /
					  <a class="text-grey" href="{{ url('orders') }}"><b>MY ORDERS </b></a> /
					  <a class="text-grey" href="#"><b>MY ORDER DETAIL </b></a>
					</span>
				</div>

			</div>
			<div class="row">
				<div class="col-md-12">
					<h5 class="title title-margin" style="margin-bottom:0;">MY ORDERS </h5>
	                <h5 class="center" style="margin-bottom:50px;"><span class="subtitle">DETAILS</span></h5>
				</div>
			</div>
			<div class="row">
				<div class="col-md-9 col-sm-11 col-xs-11 col-centered">
					<div class="row">
						<div class="col-md-6 col-sm-6">
							<div class="status">
								<table width="100%">
									<tbody>
									<tr>
										<td width="50%">Order Date</td>
										<td class="hide-320">:</td>
										<td>{{ $order->created_at->format('d M Y, H:i') }}</td>
									</tr>
									<tr>
										<td>Order No</td>
										<td class="hide-320">:</td>
										<td>#{{ $order->id }}</td>
									</tr>
									<tr>
										<td>Order Status</td>
										<td class="hide-320">:</td>
										<td>{{ $order->status->description }}</td>
									</tr>
									@if($order->payment[0]->method == "tempo")
										<tr>
											<td>Jatuh Tempo</td>
											<td class="hide-320">:</td>
											<td>{{ LocalizedCarbon::parse($order->created_at)->addDays(auth()->user()->tempo->duration)->format("d F Y H:m:s") }}</td>
										</tr>
									@endif
									<tr>
										<td>Metode Pembayaran</td>
										<td class="hide-320">:</td>
										<td>{{ucfirst($order->payment[0]->method)}}</td>
									</tr>
									<tr>
										<td>Shipping Service</td>
										<td class="hide-320">:</td>
										<td>Seafer Grosir Courier</td>
									</tr>
									<tr>
										<td>Shipping Address</td>
										<td class="hide-320">:</td>
										<td>{{ $order->address_detail->address }}</td>
									</tr>
									</tbody>
								</table>
							</div>
						</div>

						@if ($order->payment()->where('user_confirmed', 0)->count() > 0 && $order->payment[0]->method != "cod" && !in_array($order->order_status,[4,5,7,8,9,3]) && $order->isMeasured != 0)
						<div class="col-md-6 col-sm-6 center">
							<button type="button" class="btn btn-aneh confirm gplus" onclick="showModal()">CONFIRM PAYMENT</button>
						</div>
						@endif
						@if(!$order->isMeasured)
							<h5 class="">Please wait for your order to be Measured</h5>
						@endif
					</div>
					<div class="table-detail-order">
						<div class="title-catogory">
							<b class="text-blue">FOO</b><b class="text-red">DIS GROCER</b>
						</div>

						<table>
							<tbody>
							<tr class="bg-transparent">
								<th>Product</th>
								<th>&nbsp;</th>
								<th>SKU</th>
								<th>Quantity</th>
								<th>&nbsp;</th>
								<th width="20%">Unit Price</th>
								<th width="20%">Total Price</th>
							</tr>

							@foreach ($order->groceries as $orderGroceries)
							<tr>

								@if($orderGroceries->groceries->images->count() != 0)
								<td><img src="{{ asset('contents/'.$orderGroceries->groceries->images->first()->file_name) }}" height="85" width="128"></td>
								@else
                                <td><img src="{{ asset('assets/images/img-empty.png') }}" width="128" height="85"></td>
                                @endif

								<td>{{ $orderGroceries->groceries->name }}</td>
								<td data-content="SKU">{{ $orderGroceries->groceries->sku ? $orderGroceries->groceries->sku : 'No SKU added' }}</td>
								<td data-content="Quantity">{{ $orderGroceries->qty }}</td>
								<td data-content="&nbsp;" style="border:none">&nbsp;</td>
								<td data-content="Unit Price" >{{ Money::display($orderGroceries->price) }}</td>
								<td data-content="Total Price">{{ Money::display($orderGroceries->price * $orderGroceries->qty) }}</td>
							</tr>
							@endforeach

							</tbody>
						</table>
					</div>

					<div class="col-md-4 col-md-offset-8 col-sm-4 col-sm-offset-8 no-padding-h">
						<div class="table-total">
							<table>
								<tbody>
								<tr>
									<td>Subtotal: </td>
									<td align="right"><span>{{ Money::display($order->nett_total) }}</span></td>
								</tr>
								<tr>
									<td>Promo:</td>
									<td align="right"><span style="color:#ff0000;margin-right:-5px">({{ Money::display($order->promo_value) }})</span></td>
								</tr>
								<tr>
									<td>Total:</td>
									@if( ($order->nett_total - $order->redeem - $order->promo_value) < 0)
                                    <td align="right"><span>{{ Money::display($order->shipping_fee) }}</span></td>
                                    @else
                                    <td align="right"><span>{{ Money::display($order->nett_total  - $order->promo_value) }}</span></td>
                                    @endif
								</tr>
								</tbody>
							</table>
						</div>
					</div>

					<div class="col-md-4 col-md-offset-8 col-sm-4 col-sm-offset-8 no-padding-h">
						<div class="table-total">
							<table>
								<tbody>
								<tr>
									<td>Unique Code:</td>
									<td align="right"><span>{{ Money::display($smallAdditionalFee) }}</span></td>
								</tr>
								@if($order->order_status == 10)
								<tr>
									<td>Interest </td>
									<td align="right"><span>{{ $order->interest }}%</span></td>
								</tr>
								@endif
								@if($order->interest != 0 )
									<tr>
										<td>Interest:</td>
										<td>{{$order->interest}}%</td>
									</tr>
								@endif
								<tr>
									<td>GRAND TOTAL:</td>
									<td align="right"><span>{{ Money::display($order->grand_total - $order->tax * (1+($order->interest/100))) }}</span></td>
								</tr>
								{{-- <tr>
									<td colspan="2"><br>
										<a href="{{ url('orders/'.$order->id.'/download') }}" class="text-grey"><img src="{{ asset(null) }}assets/images/ico-pdf.png"> Download your invoice as PDF file</a>
									</td>
								</tr> --}}
								</tbody>
							</table>

						</div>
					</div>
				</div>
			</div>

	</div>

	<!-- Modal -->
	<div id="modalPayment" class="modal fade modalPayment" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-box">
					<h3>Payment Confirmation for Order #{{$order->id}}</h3>
						<form class="border-dashed" method="post" action="{{ url('order/payment/transfer-confirmation') }}" enctype="multipart/form-data">
	                        <div class="row">
	                            <div class="col-md-4">
	                            	<label for="middle-label" class="text-left middle "><span class="required">TRANSFERED TO</span></label>

	                            </div>
	                            <div class="col-md-8">
	                                <select name="transfer_dest_account">
	                                    <option value="BCA: 2063232327">BCA: 2063232327</option>
	                                    <option value="MANDIRI: 1220007586848">MANDIRI: 1220007586848</option>
	                                </select>
	                            </div>
	                        </div>
	                        <div class="row">
	                            <div class="col-md-4">
	                            	<label for="middle-label" class="text-left middle">TRANSFERED AMOUNT</label>
	                            </div>
	                            <div class="col-md-8">
	                            	<input class="check-input" type="text" name="transfer_amount" placeholder="{{ Money::display($order->grand_total - $order->tax * (1+($order->interest/100)))}}"  required></div>
	                        </div>
	                        <div class="row">
	                            <div class="col-md-4">
	                            	<label for="middle-label" class="text-left middle">DATE</label>
	                            </div>
	                            <div class="col-md-8"><input class="datepick" readonly="readonly"  type="text" name="transfer_date" required></div>
	                        </div>
	                        <div class="row">
	                            <div class="col-md-4">
	                            	<label for="middle-label" class="text-left middle">TIME</label>
	                            </div>
	                            <div class="col-md-8">
	                                <div class="row">
	                                    <div class="col-md-6">
	                                        <div class="row">
	                                            <div class="col-md-6">
	                                                <input type="text" name="transfer_time_minute" class="check-input time" required placeholder="HH">
	                                            </div>
	                                            <div class="col-md-6">
	                                                <input type="text" name="transfer_time_second" class="check-input time" required placeholder="MM">
	                                            </div>
	                                        </div>
	                                    </div>
	                                    <div class="col-md-6">

	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <div class="row">
	                            <div class="col-md-4">
	                            	<label for="middle-label" class="text-left middle">FROM BANK</label>
	                            </div>
	                            <div class="col-md-8"><input class="check-input" type="text" name="transfer_sender_bank_name" @if(null !== $bank_preference)value="{{ $bank_preference->name }}"@endif required></div>
	                        </div>
	                        <div class="row">
	                            <div class="col-md-4">
	                            	<label for="middle-label" class="text-left middle">ACCOUNT NUMBER</label>
	                            </div>
	                            <div class="col-md-8"><input class="check-input" type="text" name="transfer_sender_account_number" @if(null !== $bank_preference)value="{{ $bank_preference->account_number }}"@endif required></div>
	                        </div>
	                        <div class="row">
	                            <div class="col-md-4">
	                            	<label for="middle-label" class="text-left middle">ACCOUNT NAME</label>
	                            </div>
	                            <div class="col-md-8"><input class="check-input" type="text" name="transfer_sender_account_name" @if(null !== $bank_preference)value="{{ $bank_preference->account_name }}"@endif required></div>
	                        </div>
	                        <div class="row">
	                            <div class="col-md-4">
	                            	<label for="middle-label" class="text-left middle">NOTE</label>
	                            </div>
	                            <div class="col-md-8"><input class="check-input" type="text" name="transfer_note"></div>
	                        </div>
	                        <div class="row">
	                            <div class="col-md-4">
	                            	<label for="middle-label" class="text-left middle">PROOF PAYMENT</label>
	                            </div>
	                            <div class="col-md-8"><input type="file" name="file" style="height: 37px;padding: 0 0.5rem;border: 1.5px solid #808184"></div>
	                        </div>
	                        <div class="row">
	                            <div class="col-md-12">
	                                <div class="save-payment-box">
	                                    <input type="checkbox" class="square" id="saveBank" name="set_bank_preference">
	                                    <label for="saveBank">Save this bank account information for next payment</label>
	                                </div>
	                            </div>
	                        </div>
	                        <div class="row">
	                            <div class="col-md-12"><br>
	                                <center> <button type="submit" class="btn btn-aneh align-items-center">CONFIRM PAYMENT</button></center>

	                            </div>
	                        </div>

	                        <input type="hidden" name="order_number" value="{{ $order->id }}"/>
                        	{{ csrf_field() }}
	                    </form>
	    			</div>
	    		</div>

	  		</div>
		</div>


		<div id="modalInfo" class="modal fade modalInfo" role="dialog">
			<div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-box">
						<h3>Order #{{$order->id}}</h3>
							<div class="row">
		                            <div class="col-md-12">
		                            	<label for="middle-label" class="text-left middle "><span class="required">Please wait for your order to be Measured</span></label>

		                            </div>

		                        </div>


		    			</div>
		    		</div>

		  		</div>
			</div>
@endsection

@section('scripts')

<script src="assets/plugin/bootstrap/js/jquery.bootstrap-responsive-tabs.min.js"></script>
<script src="assets/plugin/fancybox/js/jquery.fancybox.min.js"></script>
<script src="assets/plugin/bootstrap/js/bootstrap-datepicker.js"></script>
<script src="assets/js/js_lib.js"></script>
<script>
        $(document).ready(function(){
        	function datepick(){
				$(".datepick").datepicker({
					format: 'dd-mm-yyyy',
	                startDate: '0',
				    showOtherMonths: true,
				    selectOtherMonths: true
				});
				$(".withdatepick").click(function(){
					$(".datepick").datepicker({
		                format: 'dd-mm-yyyy',
		                startDate: '0',
					    showOtherMonths: true,
					    selectOtherMonths: true
					});
				})
			}

			$(document).ready(function(){
				datepick();
			})
            /*$('input[name="transfer_amount"]').mask("#.##0", {reverse: true});
            $('input[name="transfer_time_minute"]').mask('00');
            $('input[name="transfer_time_second"]').mask('00');*/
            $("input[name='transfer_sender_account_number']").keydown(function (e) {
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                     // Allow: Ctrl+A, Command+A
                    (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                     // Allow: home, end, left, right, down, up
                    (e.keyCode >= 35 && e.keyCode <= 40)) {
                         // let it happen, don't do anything
                         return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });
            $('input[name="transfer_sender_bank_name"],input[name="transfer_sender_account_name"]').keydown(function (e) {
                e = e || window.event;
                var charCode = (typeof e.which == "undefined") ? e.keyCode : e.which;
                var charStr = String.fromCharCode(charCode);
                if (/\d/.test(charStr)) {
                    return false;
                }
            });
            $('input[name="transfer_time_minute"]').on('keydown keyup', function(e){
                if ($(this).val() > 23
                    && e.keyCode != 46 // delete
                    && e.keyCode != 8 // backspace
                   ) {
                   e.preventDefault();
                   $(this).val(23);
                }
            });
            $('input[name="transfer_time_second"]').on('keydown keyup', function(e){
                var minute = $('input[name="transfer_time_minute"]').val();
                var limit = 59;
                if(minute > 23){
                    limit = 0;
                }
                if ($(this).val() > limit
                    && e.keyCode != 46 // delete
                    && e.keyCode != 8 // backspace
                   ) {
                   var formattedNumber = ("0" + limit).slice(-2);
                   e.preventDefault();
                   $(this).val(formattedNumber);
                }
            });
        })

		function showModal(){
			@if($order->isMeasured == 0)
				$('#modalInfo').modal('toggle');
			@else
				$('#modalPayment').modal('toggle');
			@endif


			// data-toggle="modal" data-target="#modalPayment"
		}
    </script>
@endsection
