@extends('frontend.layout.master')

@section('title', App\Page::createTitle('My Address'))

@section('customcss')
    <link rel='stylesheet' href='assets/plugin/bootstrap/css/bootstrap-responsive-tabs.css'>
    <link rel="stylesheet" type="text/css" href="assets/plugin/fancybox/css/jquery.fancybox.min.css">
@endsection

@section('content')
	<div class="main-content">
		<div class="container" id="checkout">
			<div class="row hide-320">
				<div class="col-md-12">
					<span class="text-grey equal margin-bottom-35">
					  <a class="text-grey" href="{{ url('/') }}"><b>HOME</b></a> /
					  <a class="text-grey" href="{{ url('user/my-account') }}"><b>MY ACCOUNT</b></a> /
					  <a class="text-grey" href="#"><b>MY ADDRESS BOOK </b></a>
					</span>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h5 class="title title-margin">MY ADDRESS BOOK</h5>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3 hide-768">
					<br><br><br>
					<div class="rumah-box">
						<img src="assets/images/img-house-yellow.png">
						<img class="left" src="assets/images/img-house-bluesky.png">
					</div>
				</div>
				<div class="col-md-6 col-sm-7 col-xs-10 col-centered-768">
					<div class="row">
						<div class="col-md-12">
							<div class="center">
								<a href ="{{ url('user/add-new-address') }}"><button class="btn btn-aneh">ADD NEW ADDRESS</button></a>
							</div>
						</div>
					</div>
					<div class="row">
						@if($addresses && count($addresses) > 0)
                            @foreach($addresses as $address)
						<div class="col-md-6 col-sm-6 no-padding-h address-list">
							<div class="address-card">
								<h6>{{ $address->name }}</h6>
								<p>
									{{ $address->address }},<br>
                                    {{ $address->area->name }},
									{{ $address->area->city['name'] }}, <br>
                                    {{ $address->city->province->province_name_id }},&nbsp;Indonesia<br/>
                                    {{ $address->postal_code }}<br>
								</p>
								<a class="no-padding-h" href="{{ url('user/edit-address/'.$address->id) }}"><button type="button"><b>edit</b></button></a>
								{!! Form::open(['method' => 'DELETE', 'action' => ['UserController@deleteAddress', $address->id], 'class'=> "delete-address", 'id'=>'form-address-'.$address->id]) !!}
								<button type="submit" onclick="return confirm('Apakah Anda yakin ingin menghapus alamat ini?')"><b>delete</b></button>
                                {!! Form::close() !!}
							</div>
						</div>
							@endforeach
                        @endif

					</div>
				</div>
				<div class="col-md-3 hide-768">
					<br><br><br>
					<div class="rumah-box">
						<img src="assets/images/img-house-blue.png">
						<img class="right" src="assets/images/img-house-red.png">
					</div>
				</div>
			</div>

	</div>
@endsection

@section('scripts')
<script src="assets/plugin/bootstrap/js/jquery.bootstrap-responsive-tabs.min.js"></script>
<script src="assets/plugin/fancybox/js/jquery.fancybox.min.js"></script>
@endsection
