@extends('frontend.layout.master')

@section('title', App\Page::createTitle('My Bank Account'))

@section('customcss')
    <link rel='stylesheet' href='assets/plugin/bootstrap/css/bootstrap-responsive-tabs.css'>
    <link rel="stylesheet" type="text/css" href="assets/plugin/fancybox/css/jquery.fancybox.min.css">
@endsection

@section('content')
	<div class="main-content">
		<div class="container">
			<div class="row hide-320">
				<div class="col-md-12">
					<span class="text-grey equal margin-bottom-35">
					  <a class="text-grey" href="{{ url('/') }}"><b>HOME</b></a> /
					  <a class="text-grey" href="{{ url('user/my-account') }}"><b>MY ACCOUNT </b></a> /
					  <a class="text-grey" href="#"><b>MY PAYMENT </b></a>
					</span>
				</div>

			</div>
			 <div class="row">
	            <div class="col-md-12">
	                <h5 class="title title-margin">My Payment</h5>
				</div>
	        </div>
            <div class="row">

			</div>
			<div class="row">
	            <div class="col-md-3 center hide-768">
	                <img class="left" src="assets/images/bank-account-left.png">
	            </div>
	            <div class="col-md-6 col-sm-7 col-xs-10 col-centered-768 center">
					<form  action="{{url('user/update-payment')}}" method="post">
						<?php
							if(json_decode(auth()->user()->payment_method) != null){
								$payment = json_decode(auth()->user()->payment_method);
							}else{
								$payment = [];
							}

						?>
					<div class="col-md-12">
						<div class="box">
							<input class="checkbox" type="checkbox" name="payment_method[]" value="cod" {{(in_array('cod',$payment))?"checked disabled":"disabled"}}>
							<label for="checkbox" class="agree pull-left">Cash on Delivery</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="box">
							<input class="checkbox" type="checkbox" name="payment_method[]" value="transfer" {{(in_array('transfer',$payment))?"checked disabled":"disabled"}}>
							<label for="checkbox" class="agree pull-left">Bank Transfer</label>
						</div>
					</div>
					<div class="col-md-12">
						<div class="box">
							<input class="checkbox" type="checkbox" name="payment_method[]" value="tempo"
														{{(in_array('tempo',$payment) && auth()->user()->tempo->status == "approved")?"checked disabled":""}}
														{{(auth()->user()->tempo->status == "unsuccessful" || auth()->user()->tempo->status == "pending")?" checked disabled":""}}>
							<label for="checkbox" class="agree pull-left">Tempo
								@if(auth()->user()->tempo->status == "pending")
									- Reviewing
								@elseif(auth()->user()->tempo->status == "approved")
									-  Approved - {{auth()->user()->tempo->duration }} Days
								@elseif(auth()->user()->tempo->status == "unsuccessful")
									-  Rejected
								@endif
							</label>
						</div>
					</div>
					@if(auth()->user()->tempo->status == "approved")
					<div class="col-md-12">
						<p><b><u>Tempo Aggrement</u></b></p>
						<p>{{auth()->user()->tempo->aggrement}}</p>
					</div>
					@endif

					@if(auth()->user()->tempo->status == "unsuccessful")
					<div class="col-md-12">
						<p>Sorry you have to wait for a month to be able to apply for another tempo</p>
					</div>
					@endif
					&nbsp
					@if( !in_array('tempo',$payment) || auth()->user()->tempo->status == "unapplied")
						<div class="col-md-12">
							<button class="btn btn-aneh pull-left" id="" type="submit">Apply Tempo</button>
						</div>
					@endif



				</form>
					<div class="col-md-12 border-line"></div>
					<div class="row">
					   <div class="col-md-12">
						   <h5 class="title title-margin">My Bank Account</h5>
					   </div>
				   </div>
	                <div class="col-md-6 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-2 left">
	                    @if($bank_list->count() > 0)
	                    @foreach($bank_list as $bank)
	                    <p class="semi">
	                        {{ $bank->name }} {{ $bank->branch }}
	                        <br>a/n {{ $bank->account_name }}
	                        <br>{{ $bank->account_number }}
	                        <br>

	                        @if($bank->is_preference == 0)
	                        <a  class="grey-underline" href="{{ url('user/set-bank-account-preference/'.$bank->id) }}">Set as default</a>
	                        @else
	                        <a  class="grey-underline" href="#">Default account</a>
	                        @endif
	                        <a class="grey-underline"  href="{{ url('user/delete-bank-account/'.$bank->id) }}">delete</a>
	                    </p>
	                    @endforeach
	                    @else
	                    <p class="semi">
	                        You have not add any bank to your preference
	                    </p>
	                    @endif
	                </div>
					<div class="col-md-12 center">
						<a href="{{ url('user/add-bank-account') }}" class="btn btn-aneh">ADD BANK ACCOUNT</a>
					</div>
	            </div>
	            <div class="col-md-3 center hide-768">
	                <img class="left" src="assets/images/bank-account-right.png">
	            </div>
			</div>
		</div>

	</div>
@endsection

@section('scripts')
<script src="assets/plugin/bootstrap/js/jquery.bootstrap-responsive-tabs.min.js"></script>
<script src="assets/plugin/fancybox/js/jquery.fancybox.min.js"></script>
@endsection
