@extends('frontend.layout.master')

@section('title', App\Page::createTitle('Add New Address'))

@section('customcss')
    <link rel='stylesheet' href='assets/plugin/bootstrap/css/bootstrap-responsive-tabs.css'>
    <link rel="stylesheet" type="text/css" href="assets/plugin/fancybox/css/jquery.fancybox.min.css">
@endsection

@section('content')
	<div class="main-content">
		<div class="container" id="checkout">
			<div class="row hide-320">
				<div class="col-md-12">
					<span class="text-grey equal margin-bottom-35">
					  <a class="text-grey" href="{{ url('user/my-account') }}"><b>MY ACCOUNT</b></a> /
					  <a class="text-grey" href="{{ url('user/my-address') }}"><b>MY ADDRESS BOOK </b></a>/
					  <a class="text-grey " href="#"><b>ADD NEW ADDRESS</b></a>
					</span>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h5 class="title title-margin">ADD NEW ADDRESS</h5>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-sm-7 col-xs-10 col-centered">
					<form method="post">
						<div class="form-box top-d">
							<div class="row">
								<div class="col-md-4">
									<label for="middle-label" class="text-left middle "><span class="required">ADDRESS NAME</span></label>
								</div>
								<div class="col-md-8">
									<input class="check-input" placeholder="e.g., Alamat Kantor" name="name" type="text">
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<label for="middle-label" class="text-left middle">TITLE</label>
								</div>
								<div class="col-md-8">
									<select name="recipient_title">
										<option value="m">Mr.</option>
										<option value="fs">Ms.</option>
										<option value="f">Mrs.</option>
									</select>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<label for="middle-label" class="text-left middle"><span class="required">FIRST NAME</span></label>
								</div>
								<div class="col-md-8">
									<input class="check-input" placeholder="First Name" name="first_name" required="required" type="text">
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<label for="middle-label" class="text-left middle"><span class="required">LAST NAME</span></label>
								</div>
								<div class="col-md-8">
									<input class="check-input" placeholder="Last Name" name="last_name" required="required" type="text">
								</div>
							</div>
							<div class="row">
								<hr class="blue">
							</div>
							<div class="row">&nbsp;</div>
							<div class="row">
								<div class="col-md-4">
									<label for="middle-label" class="text-left middle"><span class="required">ADDRESS LINE1</span></label>
								</div>
								<div class="col-md-8">
									<input class="check-input" placeholder="Address" name="address_one" required="required" type="text">
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<label for="middle-label" class="text-left middle">ADDRESS LINE 2</label>
								</div>
								<div class="col-md-8">
									<input class="check-input" placeholder="Address" name="address_two" type="text">
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<label for="middle-label" class="text-left middle">ADDRESS LINE 3</label>
								</div>
								<div class="col-md-8">
									<input class="check-input" placeholder="Address" name="addres_three" type="text">
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<label for="middle-label" class="text-left middle"><span class="required">PROVINCE</span></label>
								</div>
								<div class="col-md-8">
									<select name="province" id="select_province">
                                        <option value="0">Please Select Province</option>
                                        @foreach(App\Models\Province::where('is_active', 1)->get() as $province)
                                        <option value="{{ $province->province_id }}">{{ $province->province_name }}</option>
                                        @endforeach
                                    </select>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<label for="middle-label" class="text-left middle"><span class="required">CITY</span></label>
								</div>
								<div class="col-md-8">
									<select name="city" id="select_city">
                                        <option value="0">Please Select City</option>
                                        @foreach(App\Models\City::where('is_active', 1)->orderBy('city_name', 'asc')->get() as $city)
                                            <option value="{{ $city->city_id }}" data-prov="{{ $city->province_id }}">{{ $city->city_name }}</option>
                                        @endforeach
                                    </select>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<label for="middle-label" class="text-left middle"><span class="required">AREA</span></label>
								</div>
								<div class="col-md-8">
									<select name="area" id="select_area">
                                        <option value="0">Please Select Area</option>
                                        @foreach(App\Models\Location\Area::orderBy('name', 'asc')->get() as $area)
                                            <option value="{{ $area->id }}" data-city="{{ $area->city_id }}">{{ $area->name }}</option>
                                        @endforeach
                                    </select>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<label for="middle-label" class="text-left middle"><span class="required">POSTAL CODE</span></label>
								</div>
								<div class="col-md-8">
									<input class="check-input" placeholder="Postal Code" name="postal_code" required="required" type="text">
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<label for="middle-label" class="text-left middle phone"><span class="required">MOBILE NUMBER</span></label>
								</div>
								<div class="col-md-8">
									<div class="input-group">
										<span class="input-group-label">+62</span>
										{{ csrf_field() }}
										<input class="input-group-field check-input phone_number" name="phone_number" placeholder="8129xxx" type="text">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<label for="middle-label" class="text-left middle hide-768" style="color:white">MAKE THIS MY</label>
								</div>
								<div class="col-md-8">
									<div class="col-md-6 col-sm-6 col-xs-6">
										<button data-open="modal-submit" class="btn btn-action blue space-3 savebutton" type="submit" aria-controls="modal-submit" aria-haspopup="true" tabindex="0">SAVE</button>
									</div>
									<div class="col-md-6 col-sm-6 col-xs-6">
										<a href="{{ url('user/my-address') }}" class="btn btn-action red space-3"><span style="color:white">CANCEL</span></a>
									</div>
								</div>
							</div>

							<div class="reveal" id="modal-submit" data-reveal>
                                <div class="modal-box">
                                    <div class="dashed-box">
                                        <div class="img-box">
                                            <img src="{!! asset(null) !!}assets/images/img-popup.png">
                                        </div>
                                        <h5>Thank you for submitting!<br />we will process your application</h5>
                                        <a href="{{ url('user/my-account') }}">Click here</a> <span>to continue</span>
                                    </div>
                                </div>
                            </div>

							<div class="row">
								<div class="col-md-6"></div>
								<div class="col-md-2"></div>
								<div class="col-md-4">
									<div class="row">

									</div>
								</div>
							</div>
					</form>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
	<script src="assets/plugin/bootstrap/js/jquery.bootstrap-responsive-tabs.min.js"></script>
	<script src="assets/plugin/fancybox/js/jquery.fancybox.min.js"></script>
	<script>
        $('.reveal').hide();
        var address_name = $(':input[name="name"]').val();
        var fname = $(':input[name="first_name"]').val();
        var lname = $(':input[name="last_name"]').val();
        var addr = $(':input[name="address_one"]').val();
        var mob = $(':input[name="phone"]').val();

        $('.submitbutton').click(function(e){
            if(address_name == '' || fname=='' || lname=='' || addr=='' || mob=='') {
                console.log('all ampty');
                this.removeAttribute('data-open');
                e.preventDefault();
            }
            e.preventDefault();
        });

        $('#select_province').on('change', function(){
            var province_id = $(this).val();
            $.ajax({
                type: 'get',
                url: '{{ url('user/ajax-cities-by-province') }}/' + province_id
            }).done(function(result){
                result = jQuery.parseJSON(result);
                if(result.status == 'success') {
                    $('#select_city').html('');
                    $.each(result.cities, function(k, v) {
                        $('#select_city').append('<option value="'+k+'">'+v+'</option>');
                    });

                } else {
                    alert(result.message);
                }
            })
        });

        /*$('#select_area').on('change', function(){
            var area_id = $(this).val();
            var chosen = $(this).find('option[value="'+area_id+'"]');
            var city_id =$('#select_city').val(chosen.attr('data-city'));
            var city_chosen = city_id.find('option[value="'+city_id.val()+'"]')
            var prov_id = $('#select_province').val(city_chosen.attr('data-prov'));

        });*/

        $('#select_city').on('change', function(){
            var city_id = $(this).val();
            $.ajax({
                type: 'get',
                url: '{{ url('user/ajax-areas-by-city/') }}/' + city_id
            }).done(function(result){
                result = jQuery.parseJSON(result);
                if(result.status == 'success') {
                    $('#select_area').html('');
                    $.each(result.areas, function(k, v) {
                        $('#select_area').append('<option value="'+k+'">'+v+'</option>');
                    });

                } else {
                    alert(result.message);
                }
            })

        });

        $('form').submit(function(){
            $('#modal-submit').foundation('reveal', 'open', {
                url: 'http://local.Seafer Grosir/user/add-new-address',
                success: function(data) {
                    alert('modal data loaded');
                },
                error: function() {
                    alert('failed loading modal');
                }
            });
        });

    </script>
@endsection
