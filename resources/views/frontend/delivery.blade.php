@extends('frontend.layout.master')

@section('title', App\Page::createTitle('Delivery'))

@section('customcss')
    <link rel='stylesheet' href='assets/plugin/bootstrap/css/bootstrap-responsive-tabs.css'>
    <link rel="stylesheet" type="text/css" href="assets/plugin/fancybox/css/jquery.fancybox.min.css">
	
@endsection

@section('content')
<div class="main-content">
	<div class="container">
        <div class="row hide-768">
			<div class="col-md-12">
				<span class="text-grey equal margin-top-10">
				  <a class="text-grey" href="{!! url('/') !!}"><b>HOME</b></a> /
				  <a class="text-grey" href="#"><b>DELIVERY</b></a>
				</span>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<h5 class="title title-margin">DELIVERY</h5>
			</div>
		</div>
		<div class="row delivery-box">
			<div class="col-md-7 col-sm-7 col-xs-10 col-centered">
                	<p>
                Armada pengiriman Seafer Grosir siap mengantarkan pesanan anda dengan waktu yang tepat dan secara aman. Saat ini kami melayani wilayah Jabodetabek (Jakarta, Bogor, Depok, Tangerang, dan Bekasi). Sebelum pengiriman, kami memeriksa dan menerapkan praktik terbaik untuk mengemas produk segar, beku, dan kering untuk memastikan bahwa produk yang dikirim kepada Anda tibah dengan kondisi sebaik mungkin.
</p>
	<p>
Untuk waktu pengiriman, kami akan mengirimkan pesanan Anda pada hari yang sama jika Anda memesan sebelum jam 11 pagi. Untuk pesanan yang dilakukan setelah pukul 11 ​​pagi, kami akan mengirimkan pesanan Anda keesokan harinya. Jika tidak tersedia stok untuk produk yang Anda pesan, kami akan mencoba yang terbaik untuk mengirim pesanan Anda di dalam 2-3 hari.
</p>
	<p>
Selain armada Seafer Grosir, kami juga bekerja sama dengan Go-Jek, JNE dan mitra logistik lainnya.
			</p>

                {{-- <div class="sub-content">
                    For Seafer Grocer products
                    <ul>
                        <li>If you order before 10 AM, we will deliver between 12-6 PM.</li>
                        <li>If you order before 2 PM, we will deliver between 4-10 PM.</li>
                        <li>If you order after 2 PM, we will deliver on the next day between 9 AM-5 PM.</li>
                    </ul>
                    <p>
                        For Seafer Eats products<br>
                        Your products will be delivered separately from Seafer Grocer items as the sellers will directly send them to  you. Your delivery schedule for Seafer Eats items will be shown when you check out.
                    </p>
                </div>
                <div class="delivery-img">
                    <img src="{{ asset(null) }}responsive/images/img-gojek.png">
                    <img src="{{ asset(null) }}responsive/images/img-jne.png">
					<img src="{{ asset(null) }}responsive/images/seafer-delivery.png">
                </div>
                <p>
                    Currently we are working with Gojek and JNE to help deliver the products to you.
                </p>--}}
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script src="assets/plugin/bootstrap/js/jquery.bootstrap-responsive-tabs.min.js"></script>
<script src="assets/plugin/fancybox/js/jquery.fancybox.min.js"></script>
@endsection
