@extends('frontend.layout.master')

@section('title', App\Page::createTitle('Return and Exchange'))

@section('customcss')
    <link rel='stylesheet' href='assets/plugin/bootstrap/css/bootstrap-responsive-tabs.css'>
    <link rel="stylesheet" type="text/css" href="assets/plugin/fancybox/css/jquery.fancybox.min.css">
@endsection

@section('content')
	<div class="main-content">
		<div class="container">
	        <div class="row hide-768">
				<div class="col-md-12">
					<span class="text-grey equal margin-top-10">
					  <a class="text-grey" href="{{ url('/') }}"><b>HOME</b></a> /
					  <a class="text-grey" href="loyalty-points.html"><b>RETURN &amp; EXCHANGE POLICY</b></a>
					</span>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					{{-- <h5 class="title title-margin">RETURN &amp; EXCHANGE POLICY</h5> --}}
					{!! $description[1]->description_content !!}
				</div>
			</div>
			<div class="row delivery-box">
				<div class="col-md-7 col-sm-7 col-xs-10 col-centered">
					{!! $description[2]->description_content !!}

					
					<div class="sub-content" id="return-exchange">
						{!! $description[3]->description_content !!}
						<ul>
							{{-- <li>
								<h6>INCORRECT</h6>
								<ul style="margin-left: 0;padding-left: 0;">
									<p>If you receive an incorrect item(s), you may request for a <span class="text-red">return or refund <span style="text-decoration: underline;">within 24 hours</span></span> after you receive the item(s) by submitting a claim form that you can fill out <a href="{{ url('order-claim') }}"> here</a>. Your request will be processed <span class="text-red" style="text-decoration: underline;">within 24 hours</span> <span style="font-style: italic;">(excluding Saturday, Sunday and public holidays)</span> from the time you submit your complain. </p><p>*This policy does not extend to any items which have been opened or whose seal has been broken and perishable items (such as chilled, bakery, or vegetable items).</p>
								</ul>
							</li>
							<li>
								<h6>MISSING</h6>
								<ul style="margin-left: 0;">
									<p>
										If one of the items that you ordered did not arrive, please submit a claim form that you can fill out <a href="http://foodis.co.id/order-claim"> here</a>. We will process your request and send you the missing item(s) <span class="text-red" style="text-decoration: underline;">within 24 hours</span><span style="font-style: italic;"> (excluding Saturday, Sunday and public holidays)</span> from the time you submit your complain.
									</p>
								</ul>
							</li>
							<li>
								<h6>DAMAGED</h6>
								<ul style="margin-left: 0;padding-left: 0;">
									<p>If you receive an incorrect item(s), you may request for a <span class="text-red">return or refund <span style="text-decoration: underline;">within 24 hours</span></span> after you receive the item(s) by submitting a claim form that you can fill out <a href="{{ url('order-claim') }}"> here</a>. Your request will be processed <span class="text-red" style="text-decoration: underline;">within 24 hours</span> <span style="font-style: italic;">(excluding Saturday, Sunday and public holidays)</span> from the time you submit your complain. </p><p>*This policy does not extend to any items which have been opened or whose seal has been broken and perishable items (such as chilled, bakery, or vegetable items).</p>
								</ul>

							</li> --}}
						</ul>
					</div>
					<br/>
					<p>
						<a href="{{ url('order-claim') }}" ><button class="btn btn-primary btn-aneh">ORDER CLAIM FORM</button></a>
					</p>
				</div>
			</div>
			<br/>
			<br/>
			
		</div>
	</div>
@endsection

@section('scripts')
<script src="assets/plugin/bootstrap/js/jquery.bootstrap-responsive-tabs.min.js"></script>
<script src="assets/plugin/fancybox/js/jquery.fancybox.min.js"></script>
@endsection