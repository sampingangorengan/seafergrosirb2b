@extends('frontend.layout.master')

@section('title', App\Page::createTitle('Best Price Guarantee'))

@section('customcss')
    <link rel='stylesheet' href='assets/plugin/bootstrap/css/bootstrap-responsive-tabs.css'>
    <link rel="stylesheet" type="text/css" href="assets/plugin/fancybox/css/jquery.fancybox.min.css">
@endsection

@section('content')
	<div class="main-content">
		<div class="container hide-768">
	        <div class="row">
				<div class="col-md-12">
					<span class="text-grey equal margin-top-10">
					  <a class="text-grey" href="{!! url('/') !!}"><b>HOME</b></a> /
					  <a class="text-grey" href="#"><b>GARANSI HARGA TERBAIK</b></a>
					</span>
				</div>
			</div>
		
		</div>
		<section>
		<div class="container">
	        <div class="row">
	            <div class="col-md-12">
	                <h5 class="title title-margin">GARANSI HARGA TERBAIK</h5>
	                <div class="col-md-3 col-sm-3 hide-320">
	                    <ul class="side-nav side-nav-become-box sidebar">
	                        <li><a href="#" id="toHowitworkstwo" class="line-active">PANDUAN</a></li>
	                        <li><a href="#" id="toClaimForm">DAPATKAN FORMULIR</a></li>
	                    </ul>
	                </div>
	                <div class="col-md-6 col-sm-6">
	                    <h5 id="howitworks" class="no-border subtitle">HOW IT WORKS</h5>
	                    <div class="knowmore">
	                        <p>
	                            At Seafer Grosir, we want to provide the lowest prices whenever you shop with us. So if you find a lower selling price on an <span class="text-red">identical, in-stock, non-promotional and non-discounted product</span> at other online or physical stores in <span class="text-red">Jabodetabek</span> area within <span class="text-red">3 days</span> of purchase from Seafer Grosir, let us know and we will refund you the difference directly to your loyalty points.</p>
	                        <p>
	                            Please bear in mind that the Best Price Guarantee Refund does not apply in the event of a pricing error, typographical error, or other similar error if it is an error on the part of the store. We reserve the right to investigate the legitimacy of claims and deny the ones that are being submitted to intentionally abuse Seafer Grosir’s Best Price Guarantee scheme.
	                        </p>
	                    </div>
	                </div>
	            </div>
	        </div>
		</div>
	    </section>
	    <section id="blue-bg">
		<div class="container">
	        <div class="row">
	            <div class="col-md-12">
	                <div class="col-md-6 col-md-offset-4 col-sm-6 col-sm-offset-3">
	                    <h5 class="grey-bold margin-bottom-30">What qualifies for best price refund?</h5>
	                    <div class="col-md-4">
	                        <img src="{{ asset(null) }}responsive/images/identical-stuff.png">
	                        <p class="blue" style="margin-bottom: 0;">IDENTICAL STUFF</p>
	                        <p>The item must be exactly the same – same brand, distributor, make, model, size, color, packaging, and net-weight.</p>
	                    </div>
	                    <div class="col-md-4">
	                        <img src="{{ asset(null) }}responsive/images/in-stock.png">
	                        <p class="blue" style="margin-bottom: 0;">IN-STOCK</p>
	                        <p>The item must be in-stock and available for immediate purchase.</p>
	                    </div>
	                    <div class="col-md-4">
	                        <img src="{{ asset(null) }}responsive/images/regular-selling-price.png">
	                        <p class="blue" style="margin-bottom: 0;">REGULAR SELLING PRICE</p>
	                        <p>The item must be sold at a regular selling price – no discount, no promo, or special offer.</p>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	    </section>
	    <section>
		<div class="container">
	        <div class="row">
	            <div class="col-md-12">
	                <div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3">
	                    <h5 class="grey-bold margin-bottom-30">How to claim for refund?</h5>
	                    <ul class="claim-refund">
	                        <li>
	                            1.  Take a picture that shows <span class="text-red">both</span> the product <span class="text-red">and</span> the price tag with a lower selling price than Seafer Grosir
	                            <br>
	                            <img style="margin: 15px;" src="{{ asset(null) }}responsive/images/check-uncheck-boxdua.png">
	                        </li>
	                        <li>
	                            2.  Submit your claim online simply by filling out the form <a href="#"> here.</a>
	                        </li>
	                        <li>
	                            3.  Kindly wait for the refund. You will be notified via email.
	                        </li>
	                    </ul>
	                    <hr id="claim-refund">
	                </div>
	            </div>
	        </div>
	    </div>
	    </section>
	    <section id="claim-form">
		<div class="container">
	        <div class="row">
	            <div class="col-md-12">
	                <div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3">
	                    <div class="flash-message"></div> <!-- end .flash-message -->
	                    
	                    {!! Form::open(['method'=>'POST', 'action'=>'ClaimController@store', 'files' => true]) !!}
	                    <h5 id="claimform" class="no-border subtitle" style="margin-bottom: 40px;">CLAIM FORM</h5>
	                    <div class="form-box">
	                        <div class="row">
	                            <div class="col-md-5">
	                                <label for="middle-label" class="text-left">TYPE<span class="text-red">*</span></label>
	                            </div>
	                            <div class="col-md-7">
	                                {!! Form::select('TYPE', ['Return&Exchange' => 'Return&Exchange', 'Refund' => 'Refund']); !!}
								</div>
	                        </div>
	                        <div class="row">
	                            <div class="col-md-5">
	                                <label for="middle-label" class="text-left">FIRST NAME<span class="text-red">*</span></label>
	                            </div>
	                            <div class="col-md-7">
	                                {!! Form::text('first_name', null, ['class'=>'form-control']) !!}
	                            </div>
	                        </div>
	                        <div class="row">
	                            <div class="col-md-5">
	                                <label for="middle-label" class="text-left">LAST NAME<span class="text-red">*</span></label>
	                            </div>
	                            <div class="col-md-7">
	                                {!! Form::text('last_name', null, ['class'=>'form-control']) !!}
	                            </div>
	                        </div>
	                        <div class="row">
	                            <div class="col-md-5">
	                                <label for="middle-label" class="text-left">EMAIL<span class="text-red">*</span></label>
	                            </div>
	                            <div class="col-md-7">
	                                {!! Form::text('email', null, ['class'=>'form-control']) !!}
	                            </div>
	                        </div>
	                        <div class="row">
	                            <div class="col-md-5">
	                                <label for="middle-label" class="text-left">ITEM NAME<span class="text-red">*</span></label>
	                            </div>
	                            <div class="col-md-7">
	                                {!! Form::text('item_name', null, ['class'=>'form-control']) !!}
	                            </div>
	                        </div>
	                        <div class="row">
	                            <div class="col-md-5">
	                                <label for="middle-label" class="text-left">ORDER NUMBER<span class="text-red">*</span></label>
	                            </div>
	                            <div class="col-md-7">
	                                {!! Form::text('order_id', null, ['class'=>'form-control']) !!}
	                            </div>
	                        </div>
	                        <div class="row">
	                            <div class="col-md-5">
	                                <label for="middle-label" class="text-left">URL link of foodis.co.id item<span class="text-red">*</span></label>
	                            </div>
	                            <div class="col-md-7">
	                                {!! Form::text('seafer_link', null, ['class'=>'form-control']) !!}
	                            </div>
	                        </div>
	                        <div class="row">
	                            <div class="col-md-5">
	                                <label for="middle-label" class="text-left">Regular Selling Price you paid at Seafer Grosir (Rp.)<span class="text-red">*</span></label>
	                            </div>
	                            <div class="col-md-7">
	                                {!! Form::text('seafer_price', null, ['class'=>'form-control']) !!}
	                            </div>
	                        </div>
	                        <div class="row">
	                            <div class="col-md-5">
	                                <label for="middle-label" class="text-left">Regular Selling Price you found in Other Store (Rp.)<span class="text-red">*</span></label>
	                            </div>
	                            <div class="col-md-7">
	                                {!! Form::text('other_price', null, ['class'=>'form-control']) !!}
	                            </div>
	                        </div>
	                        <div class="row">
	                            <div class="col-md-5">
	                                <label for="middle-label" class="text-left">URL link of product page (if item is on online store)<span class="text-red">*</span></label>
	                            </div>
	                            <div class="col-md-7">
	                                {!! Form::text('other_link', null, ['class'=>'form-control']) !!}
	                            </div>
	                        </div>
	                        <div class="row">
	                            <div class="col-md-5">
	                                <label for="middle-label" class="text-left">STORE LOCATION (if item is on online store)<span class="text-red">*</span></label>
	                            </div>
	                            <div class="col-md-7">
	                                {!! Form::select('store_location', ['Hero' => 'Hero Supermarket', 'Indomaret' => 'Indomaret', 'Alfamart' => 'Alfamart']); !!}
								</div>
	                        </div>
	                        <div class="row">
	                            <div class="col-md-5">
	                                <label for="middle-label" class="text-left middle ">SCREENSHOT PROOF</label>
	                            </div>
	                            <div class="col-md-7">
	                                <div class="preview img-wrapper simple"></div>
	                                <div class="file-upload-wrapper custom" style="position: absolute;width:10px;height:10px">
	                                    <input type="file" name="file" class="file-upload-native apalah" accept="image/*" />
	                                </div>
	                            </div>
	                        </div>
	                        <div class="row">
	                            <div class="col-md-7 col-md-offset-5 col-sm-7 col-sm-offset-4 col-xs-7 col-xs-offset-3" style="margin-top: 20px;">
	                                <button class="btn btn-aneh" type="submit">SUBMIT</button>
	                            </div>
	                        </div>
	                    </div>
	                    {!! Form::close() !!}
	                </div>
	            </div>
	        </div>
	    </div>
	    </section>
	</div>
@endsection

@section('scripts')
<script src="assets/plugin/bootstrap/js/jquery.bootstrap-responsive-tabs.min.js"></script>
<script src="assets/plugin/fancybox/js/jquery.fancybox.min.js"></script>
@endsection