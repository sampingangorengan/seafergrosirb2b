<footer>
	<div class="container">
		<div class="box-content">
			<div class="row">
				<div class="menu-footer col-md-2 col-sm-2 no-padding-h">
					<h6>Alasan Bergabung Dengan Kami</h6>
					<ul class="list-inline">
						<li><a href="{!! url('about-us') !!}">Tentang Kami</a></li>
						<li><a href="{!! url('become-member') !!}">Keuntungan Member</a></li>
						{{-- <li><a href="{!! url('best-price-guarantee') !!}">Garansi Harga Terbaik</a></li> --}}
					</ul>
				</div>
				<div class="menu-footer col-md-2 col-sm-2 no-padding-h">
					<h6>Pemesanan</h6>
					<ul class="list-inline">
						<li><a href="{!! url('payment') !!}">Pembayaran</a></li>
						<li><a href="{!! url('delivery') !!}">Pengiriman</a></li>
						<li><a href="{!! url('return-and-exchange') !!}">Pengembalian & Penukaran</a></li>
					</ul>
				</div>
				<div class="menu-footer col-md-2 col-sm-2 no-padding-h">
					<h6>Layanan</h6>
					<ul class="list-inline">
						<li><a href="{{ url('user/my-account') }}">Akun Saya</a></li>
						<li><a href="{{ url('supplier') }}">Supplier Login</a></li>
						<li><a href="{{ url('sign-up-supplier') }}">
							Menjadi Supplier
						</a></li>
						<li><a href="{{ url('suggestion') }}">Penawaran Produk</a></li>
						<li><a href="{{ url('faq') }}">Seputar Pertanyaan</a></li>
						<li><a href="{{ url('contact-us') }}">Hubungi Kami</a></li>
										</ul>
				</div>
				<div class="col-md-6 col-sm-6 no-padding-h">
					<div class="form-box">
						<ul class="socmed-list">
							<li><a href="https://www.facebook.com/Seafer Grosir.id/"><img src="assets/images/ico-fb.png" data-rjs="3"></a></li>
							<li><a href="https://www.instagram.com/Seafer Grosirid/"><img src="assets/images/ico-ig.png" data-rjs="3"></a></li>
						</ul>
					</div>
				</div>
			<div class="box_down_app">
					<a href=""><img src="assets/images/materials/img_playstore.png"></a>
				</div>
				<div class="box_call_bubble">
			      <ul>
			        <li>
			        	<?php
			        		$info_whatsapp = App\Models\StaticInfo::where('type', 'whatsapp')->first();
			        		$info_phone = App\Models\StaticInfo::where('type', 'phone')->first();
			        	 ?>
						{{-- @if(Jenssegers\Agent\Agent::isDesktop()) --}}
			          	{{-- <a href="https://web.whatsapp.com/send?phone={{ $info_whatsapp->link }}">{{ $info_whatsapp->content }}</a> --}}
			          	<a href="{{ $info_whatsapp->link }}">{{ $info_whatsapp->content }}</a>
			          	{{-- @else
						<a href="https://api.whatsapp.com/send?phone={{ $info_whatsapp->link }}">{{ $info_whatsapp->content }}</a>
			          	@endif --}}
			        </li>
			        <li>
			          	<a href="{{ $info_phone->link }}">{{ $info_phone->content }}</a>
			        </li>
			      </ul>
			    </div>
			</div>
		</div>
	</div>

</footer>
