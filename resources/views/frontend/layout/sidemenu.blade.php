<!-- side menu -->
<div id="side-nav">
	@if(auth()->check())
	<div class="col-md-12 no-padding-h">
		<a class="menu-account ac-profile">{{ auth()->user()->name }}</a>
	</div>
	<div class="col-md-12 no-padding-h">
		<a class="menu-myaccount" href="{{ url('/orders') }}">Order Saya</a>
	</div>
	<div class="col-md-12 no-padding-h">
		<a class="menu-myaccount" href="{{ url('/favourites') }}">Favorit</a>
	</div>
	<div class="col-md-12 no-padding-h">
		<a class="menu-myaccount" href="{{ url('user/my-rating-review') }}">Rating dan review</a>
	</div>
	<div class="col-md-12 no-padding-h">
		<a class="menu-myaccount" href="{{ url('user/my-account') }}">Akun Saya</a>
	</div>
	@else
	<div class="col-md-12 no-padding-h">
		<a class="menu-signin" href="{{ url('user/sign-in').'?r='.$_SERVER['REQUEST_URI'] }}">Masuk</a>
	</div>
	<div class="col-md-12 no-padding-h">
		<a class="menu-signup" href="{{ url('user/sign-up') }}">Daftar</a>
	</div>
	@endif
  	<div id="dl-menu" class="dl-menuwrapper">
		<ul class="dl-menu dl-menuopen">
			<li>
				<a href="#">SEMUA KATEGORI</a>
				<ul class="dl-submenu">
					@foreach( App\Models\Groceries\Category::orderBy('order')->get() as $category )
					<li>
						<a href="#{{-- {!! url('groceries/' . $category->slug) !!} --}}">{{ strtoupper($category->name) }}</a>
						<ul class="dl-submenu">
							@foreach($category->child as $child_category)
							<li><a href="{!! url('groceries/' . $child_category->slug) !!}">{{ strtoupper($child_category->name) }}</a></li>
							@endforeach
						</ul>
					</li>
					@endforeach
				</ul>
			</li>
			<li>
				<a href="#">Alasan Bergabung Dengan Kami</a>
				<ul class="dl-submenu">
					<li>
						<a href="{!! url('about-us') !!}">Tentang Kami</a>
						<a href="{!! url('become-member') !!}">Keuntungan Member</a>

						{{-- <a href="{!! url('best-price-guarantee') !!}">Garansi Harga Terbaik</a> --}}
					</li>
				</ul>
			</li>
			<li>
				<a href="#">Panduan</a>
				<ul class="dl-submenu">
					<li>
						<a href="{!! url('loyalty-points') !!}">Pemberian Cashback</a>
						<a href="{!! url('payment') !!}">Pembayaran</a>
						<a href="{!! url('delivery') !!}">Pengiriman</a>

					</li>
				</ul>
			</li>
			<li>
				<a href="#">Bantuan</a>
				<ul class="dl-submenu">
					<li>
						<a href="{!! url('return-and-exchange') !!}">Pengembalian & Penukaran Barang</a>
						<a href="{!! url('suggestion') !!}">Penawaran Produk</a>
						<a href="{!! url('faq') !!}">Seputar Pertanyaan</a>
						<a href="{!! url('contact-us') !!}">Hubungi Kami</a>
					</li>
				</ul>
			</li>
			<li>
				<a href="{{ url('sign-up-supplier') }}">Menjadi Supplier</a>
			</li>
			<li>
				<a href="{{ url('supplier') }}">Supplier Login</a>
			</li>
			@if(auth()->check())
				<li>
					<a  href="{{ url('user/sign-out') }}">Keluar</a>
				</li>

			@endif
			{{-- <li>
				<a href="#">Seafer Grosir COOK</a>
				<ul class="dl-submenu">
					<li>
						<a href="#">Living Room</a>
						<ul class="dl-submenu">
							<li><a href="#">Sofas &amp; Loveseats</a></li>
							<li><a href="#">Coffee &amp; Accent Tables</a></li>
							<li><a href="#">Chairs &amp; Recliners</a></li>
							<li><a href="#">Bookshelves</a></li>
						</ul>
					</li>
					<li>
						<a href="#">Bedroom</a>
						<ul class="dl-submenu">
							<li>
								<a href="#">Beds</a>
								<ul class="dl-submenu">
									<li><a href="#">Upholstered Beds</a></li>
									<li><a href="#">Divans</a></li>
									<li><a href="#">Metal Beds</a></li>
									<li><a href="#">Storage Beds</a></li>
									<li><a href="#">Wooden Beds</a></li>
									<li><a href="#">Children's Beds</a></li>
								</ul>
							</li>
							<li><a href="#">Bedroom Sets</a></li>
							<li><a href="#">Chests &amp; Dressers</a></li>
						</ul>
					</li>
					<li><a href="#">Home Office</a></li>
					<li><a href="#">Dining &amp; Bar</a></li>
					<li><a href="#">Patio</a></li>
				</ul>
			</li>
			<li>
				<a href="#">Seafer Grosir EATS</a>
				<ul class="dl-submenu">
					<li><a href="#">Fine Jewelry</a></li>
					<li><a href="#">Fashion Jewelry</a></li>
					<li><a href="#">Watches</a></li>
					<li>
						<a href="#">Wedding Jewelry</a>
						<ul class="dl-submenu">
							<li><a href="#">Engagement Rings</a></li>
							<li><a href="#">Bridal Sets</a></li>
							<li><a href="#">Women's Wedding Bands</a></li>
							<li><a href="#">Men's Wedding Bands</a></li>
						</ul>
					</li>
				</ul>
			</li> --}}

		</ul>
	</div>

	@if( auth()->check() )
	{{-- <div class="clearfix"></div> --}}
	<div class="col-md-12 no-padding-h logout">
		<a class="menu-signup" href="{{ url('user/sign-out') }}">KELUAR</a>
	</div>
	@endif
</div>

{{-- </div> --}}
<!-- side menu end -->
