	<title>
		{{ app()->environment() == 'production' || app()->environment() == 'live' ? '' : '('.app()->environment().') ' }}
    	@yield('title', 'Seafer Grosir')
	</title>
  	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate"/>
    <meta http-equiv="Pragma" content="no-cache"/>
    <meta http-equiv="Expires" content="0"/>
	<base href="{{ url('/') }}">
	<link rel="shortcut icon" href="assets/images/fav.png">
	<link rel='stylesheet' href='assets/plugin/bootstrap/css/bootstrap.min.css'>
	<link rel="stylesheet" type="text/css" href="assets/css/main.css">
	<link rel="stylesheet" type="text/css" href="assets/css/menu.css">
	<link rel="stylesheet" type="text/css" href="assets/plugin/typeahead/css/jquery.typeahead.css">
	<link rel="stylesheet" type="text/css" href="assets/plugin/slick/slick.css">
	<link rel="stylesheet" type="text/css" href="assets/plugin/slick/slick-theme.css">
	<link rel="stylesheet" type="text/css" href="assets/css/custom.css">
	@yield('customcss')
