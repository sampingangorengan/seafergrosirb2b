	<header>
		<div class="container">
			<div class="row">

				<div class="header-text">
					<div class="col-lg-12 col-md-12 col-xs-12 no-padding-h">
						<ul class="pull-left" style="position:relative;left:25px;">
		                    <li><b>Sahabat Bisnis Kuliner Anda</b></li>
						</ul>
						<ul class="pull-right" style="
    left: 20px;
    position: relative;">
							<li>
								<a href="{{ url('about-us') }}">
									Tentang Kami
								</a>
							</li>
							<li>
								<!-- <a href="{{ url('groceries') }}">
									Produk
								</a> -->
								<?php
					        		$info_whatsapp = App\Models\StaticInfo::where('type', 'whatsapp')->first();
					        	?>
								<a href="{{ $info_whatsapp->link }}"><img src="assets/images/materials/whatsapp-copy.png"> {{ $info_whatsapp->content }}</a>
							</li>

						</ul>
					</div>
				</div>
			</div>
		</div>
	</header>
