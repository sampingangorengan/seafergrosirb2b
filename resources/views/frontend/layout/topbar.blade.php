<div class="top-bar fixed-top " id="top-menu">
		<div class="container">
			<div class="row">
				<div class="menu-mobile col-md-1 col-sm-1 col-xs-2">
					<nav>
						<button class="hamburger-button" role="tab">
							<i title="Show navigation">menu</i>
						  </button>
					</nav>
				</div>
				<div class="col-md-4 col-sm-9 col-xs-6">
					<div class="logo">
						<a href="{{ url('/') }}">
							<img src="assets/images/logo.png" class="logo-desktop">
							<img src="assets/images/logo.png" class="logo-mobile">
							<span class="text-red">Seafer Grosir</span>&nbsp;
						</a>
					</div>
				</div>
				<div class="col-md-8 col-sm-2 col-xs-4 padding-v no-padding-h">
					<div class="top-bar-right pull-right">
						@if (auth()->check())
						<div class="sub-bar sign no-padding-h" id="barAccount">
							<a href="#" id="openAccount" class="text-red pull-right">
			                    <b><h4><span class="fontsize-username ">Hi, {{ explode(" ",auth()->user()->name)[0] }} <br></h4></b>
			                   </span>


			                    {{--<img src="assets/images/ico-header-acc.png">--}}
			                    @if(auth()->user()->profile_picture)
			                        @if(strpos(auth()->user()->photo->file, 'facebook') !== false || strpos(auth()->user()->photo->file, 'google') !== false)
			                            <img src="{{ auth()->user()->photo->file }}" style="width:40px;height:40px;border-radius: 20px;border:1px solid blue;">
			                        @else
			                            <img src="profile_picture/{{ auth()->user()->photo->file }}" style="width:40px;height:40px;border-radius: 20px;border:1px solid blue;">
			                        @endif
			                    @else
			                    <img src="assets/images/ico-header-acc.png" >
			                    @endif
								<div class="fav">
									<a href="{{url('favourites')}}">
										<img src="assets/images/love1.png" alt="favourites" class="cart" style="height:20px;">
										<span class="fontsize-username" style="font-size:11px"> My Favourites</span>

									</a>
								</div>
			                    {{--<img src="assets/images/dot-merah-besar.png" style="position: absolute;left:115px !important;">--}}
							</a>

							<div id="popupAccount">
								<span class="close close-button-cart"><img src="assets/images/ico-close.png"></span>
			                    <ul>
			                        <li>
			                            <a href="{{ url('user/my-account') }}" style="font-size:12px;">My Accounts</a>
			                        </li>
			                        <li>
			                            <div><a href="{{ url('orders') }}"  style="font-size:12px;">Payment Confirmation</a></div>
			                            @if(\App\Models\Order::ofUser(auth()->user())->where('order_status',1)->orderBy('updated_at', 'desc')->get()->count() > 0)
			                            <div class="payment-notif">{{ \App\Models\Order::ofUser(auth()->user())->where('order_status',1)->orderBy('updated_at', 'desc')->get()->count() }}</div>
			                            @endif
			                            {{--<img src="{!! asset(null) !!}assets/images/dot-merah-kecil.png" style="position: absolute;">--}}
			                        </li>
			                        <li>
			                            <a id="log-me-out" href="{{ url('user/sign-out') }}" style="font-size:12px;">Keluar</a>
			                        </li>
			                    </ul>
			                </div>
			            </div>
			            @else
						<div class="sub-bar sign col-md-1 col-xs-1 no-padding-h">
							<a href="{{ url('user/sign-up') }}">
								Daftar
							</a>
						</div>
						<div class="sub-bar sign col-md-1 col-xs-1 no-padding-h">
							<div class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">Masuk</a>
								<div id="popupSignin" class="popup-box-signin right2 dropdown-menu">
									<span class="text-blue" style="padding-left: 12px;">Masuk</span>
									<span class="close close-button"><img src="assets/images/ico-close.png"></span>
									<div class="form-box" style="padding-left: 12px;">

										<form method="post" action="{{ url('user/sign-in') }}">
											<input type="text" placeholder="Your Email" name="email" style="border:2px solid #808184;">
											<input type="password" placeholder="Password" name="password" style="border:2px solid #808184;">
											<div class="box">
												<input id="keepMe2" type="checkbox" name="remember">
												<label for="keepMe2" style="color:#A9A9A9;font-size: 12px;padding-bottom: -1px;">Ingat saya</label><br>
											</div>
											{{ csrf_field() }}
											<button class="btn pull-left" id="myLogin">SUBMIT</button>
											<a href="#" id="forgotPassword" data-toggle="modal" data-target="#modal-forgotpassword" class="pull-left withdatepick">Lupa Sandi?</a><br>
										</form>
									</div>
								</div>
							</div>
						</div>
					@endif
						<div class="sub-bar cart-toggle col-md-1 col-xs-6 no-padding-h">

							<div class="dropdown">
								<!-- cart 320 -->
								<a href="http:#" class="cart-320" data-toggle="dropdown">
									<img src="assets/images/ico-header-cart.png" class="cart">
									<div class="cart-notif" style="display: none;" ></div>
								</a>
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<img src="assets/images/ico-header-cart.png" class="cart">
									<div class="cart-notif" style="display: none;" ></div>
								</a>
								<!-- ini kalau untuk empty cart style leftnya di ganti ya jadi left:-30px atau left:-20px -->
								<div id="popupChart" class="popup-box right1 dropdown-menu" onClick="event.stopPropagation();">
									<span class="text-blue">CART</span>
									<span class="close close-button-cart">
										<img src="assets/images/ico-close.png">
									</span>

									<div class="cart_item_container">
										<div class="if-empty">
											<img src="assets/images/img-empty.png?59d219a0b81d3">
											<span>Your cart is still empty</span>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="sub-bar search-mobile col-md-1 col-xs-6 no-padding-h">
								<a href="javascript:void(0)" id="show-search">
									<img src="assets/images/ico-search.png">
								</a>
						</div>
						<div class="sub-bar search-box col-md-7 col-xs-12 no-padding-h">
							<form method="POST" action="{{ url('search') }}" accept-charset="UTF-8"><input name="_token" type="hidden" value="UUDOwhSqsFcVKaPEaMRgbb8fTvDyjGRIGYIGwImx">
								<div class="col-md-10 col-sm-11 col-xs-10 no-padding-h">
									<div class="typeahead__container">
										<div class="typeahead__field">
											<span class="typeahead__query">
												<input class="search js-typeahead" type="search" placeholder="I'm looking for" name="search" autocomplete="off">
											</span>
										</div>
									</div>
								</div>
								<div class="col-md-2 col-sm-1 col-xs-2 no-padding-h">
									<button type="submit" class="button search-btn"><img src="assets/images/ico-search.png"></button>
								</div>
							</form>

						</div>
					</div>
				</div>

			</div>

		</div>

	</div>
