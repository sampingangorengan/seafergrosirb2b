<!DOCTYPE html>
<html>
<head>

    @include('frontend.layout.html_header')
    <div class="faded-screen">

    </div>
</head>
<body>
    @yield('home')
    <div id="fb-root"></div>
    <script>
  window.fbAsyncInit = function() {
    FB.init({
        appId      : '318284038949381',
      cookie     : true,
      xfbml      : true,
      version    : 'v3.1'
    });
    FB.AppEvents.logPageView();
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>


    <!-- DOWNLOAD    -->
    <div class="boxDownloadApp after_clear">
        <div class="img_ico">
            <img src="assets/images/img-maskot.png">
        </div>
        <div class="text_app">
            <span>Pengguna baru ?</span>
            <span>Segera download Seafer Grosir App!</span>
        </div>
        <a class="download_app">
            Unduh
        </a>
    </div>
    <!-- DOWNLOAD    -->

	@include('frontend.layout.header')

	@include('frontend.layout.topbar')

	@include('frontend.layout.sidemenu')

	@yield('content')

@include('frontend.layout.footer')

<div class="copyright">
  <div class="box-content">
	<div class="col-lg-12">
		<p style="text-align:center">&copy; 2018 Seafer Grosir All Rights Reserved</p>
	</div>
  </div>
</div>

	@include('frontend.layout.modals')

<script src="assets/plugin/modernizr/js/modernizr.custom.js"></script>
<script src="assets/plugin/jquery/js/jquery.min.js"></script>
<script src="assets/plugin/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/plugin/bootstrap/js/jquery.bootstrap-responsive-tabs.min.js"></script>
<script src="assets/plugin/typeahead/js/jquery.typeahead.js"></script>
<script src="assets/plugin/fancybox/js/jquery.fancybox.min.js"></script>
<script src="assets/plugin/dlmenu/js/jquery.dlmenu.js"></script>
<script src="assets/plugin/slick/js/slick.min.js"></script>
<script src="assets/js/main.js"></script>
<script src="assets/js/slider.js"></script>

@yield('scripts')

<script>

$(function() {

    window.onscroll = function() {stickyScroll()};


        var header = document.getElementById("top-menu");
        var sticky = header.offsetTop;
        function stickyScroll() {
          let width = window.innerWidth;
          if (width > 768 && window.pageYOffset > sticky) {
            header.classList.add("sticky");
          } else {
            header.classList.remove("sticky");
          }
        }

    $('.cart-320').click(function(e){
      let width = window.innerWidth;
      if(width < 768){
        e.stopPropagation();
        e.preventDefault();
        window.location.href = "{{url('/cart')}}";
      }

    });

	$( '#dl-menu' ).dlmenu({
		animationClasses : { classin : 'dl-animate-in-2', classout : 'dl-animate-out-2' }
	});

  $('.search-btn-container, .faded-screen').click(function(){
    $('.search-box, .faded-screen').toggle(300);
  });


});
$('#log-me-out').on('click', function(){
   	phpLiveChat.endChat();
});
// Handling forgot password
$('.do-not-forget').click(function(e){
    e.preventDefault();
    $.ajax({
        type: 'post',
        url: "{{ url('password/email') }}",
        data: {
            email: $('.forget-me-not input').val()
        }
    }).done(function(result){
        console.log(result);
    });
});
$('form.forget-me-not').submit(function(e){
    $.ajax({
        type: 'post',
        url: "{{ url('password/email') }}",
        data: {
            email: $('.forget-me-not input').val()
        }
    }).done(function(result){
        console.log('ahahahaha');
        $('#modal-forgotpassword').removeAttr('style');
        $('#modal-forgotpassword').parent().removeAttr('style');
        $('#modal-afterForgotPassword').attr('style','top: 70px; display: block;');
        $('#modal-afterForgotPassword').parent().attr('style','display:block;')
    });
    e.preventDefault();
})
$.getJSON( "{{ url('groceries/all_data') }}", callbackFuncWithData);
function callbackFuncWithData(data)
{
    console.log(data);
    $.typeahead({
        input: ".search",
        minLength: 2,
        maxItem: 6,
        order: "asc",
        hint: true,
        backdrop: {
            "opacity":0.92,
            "background-color": "#ffffff"
        },
        emptyTemplate: 'No result',
        dynamic:true,
        href:"{{url()}}/groceries/@{{id}}/@{{slug}}",
        source: {
            name:{
                display: "name",
                data: data.data
            }
        },
        callback: {

        },
        debug: false
    });
}
@foreach (['danger', 'warning', 'success', 'info'] as $msg)
    @if(Session::has('alert-' . $msg))

$(document).ready(function(){
    $("#modal-session").modal();
    $('.loading-container').hide();
});
    @endif
@endforeach

function checkLoginState() {
  FB.getLoginStatus(function(response) {
    statusChangeCallback(response);
  });
}
</script>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5bb8b0e8b033e9743d02972a/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
</body>
</html>
