@extends('frontend.layout.master')

@section('title', App\Page::createTitle('Suggest Product'))

@section('customcss')
    <link rel='stylesheet' href='assets/plugin/bootstrap/css/bootstrap-responsive-tabs.css'>
    <link rel="stylesheet" type="text/css" href="assets/plugin/fancybox/css/jquery.fancybox.min.css">
@endsection

@section('content')
	<div class="main-content">
	<div class="container">
        <div class="row hide-768">
			<div class="col-md-12">
				<span class="text-grey equal margin-top-10">
				  <a class="text-grey" href="{{ url('/') }}"><b>HOME</b></a> /
				  <a class="text-grey" href="#"><b>SUGGEST A PRODUCT</b></a>
				</span>
			</div>
		</div>
	</div>
	<div class="suggest-product-box">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				{{-- <h5 class="title title-margin">SUGGEST A PRODUCT</h5> --}}
				{!! $description[1]->description_content !!}
				{!! $description[2]->description_content !!}
				{{-- <p class="after-title">
					If a product that you love is not available at Seafer Grosir, do let us know.<br>
					We are happy to accommodate new products for you.
				</p> --}}
			</div>
		</div>
		<div class="row suggest-product-box">
			<div class="col-md-3 hide-768">
				<div class="buah-box">
					<img class="jeruk" src="{{ $images[0]}}">
				</div>
			</div>
			<div class="col-md-6 col-sm-7 col-xs-10 col-centered-768">

				{!! Form::open(['method'=>'POST', 'action'=>['ProductSuggestionController@store'], 'files' => true]) !!}
				<div class="form-box">
					<div class="row">
						<div class="col-md-4">
							<label for="middle-label" class="text-left middle "><span class="required">PRODUCT NAME</span></label>
						</div>
						<div class="col-md-8">
							{!! Form::text('product_name', null, ['class'=>'form-control', 'required' => 'required']) !!}
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<label for="middle-label" class="text-left middle "><span class="required">BRAND NAME</span></label>
						</div>
						<div class="col-md-8">
							{!! Form::text('brand_name', null, ['class'=>'form-control', 'required' => 'required']) !!}
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<label for="middle-label" class="text-left middle"><span class="required">CATEGORY</span></label>
						</div>
						<div class="col-md-8">
							<select class="short" name="category" required="required">
                                        @foreach(App\Models\Groceries\Category::orderBy('id', 'asc')->where('is_active', 1)->get() as $category)
                                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                                        @endforeach
                                        {{-- <option value="0">Instafood</option>
                                        <option value="1">Instadrink</option>
                                        <option value="2">Instagram</option> --}}
                                    </select>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<label for="middle-label" class="text-left middle ">COMMENTS</label>
						</div>
						<div class="col-md-8">
							<textarea rows="3" name="comments"></textarea>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<label for="middle-label" class="text-left middle "><span class="required">EMAIL ADDRESS</span></label>
						</div>
						<div class="col-md-8">
							{!! Form::text('email', null, ['class'=>'form-control', 'required' => 'required']) !!}
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<label for="middle-label" class="text-left middle ">UPLOAD PHOTO</label>
						</div>
						<div class="col-md-8">
							<div class="preview img-wrapper simple"></div>
							<div class="file-upload-wrapper custom">
								<input type="file" name="file" class="file-upload-native apalah" accept="image/*"/>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							&nbsp;
						</div>
						<div class="col-md-8 col-sm-offset-4 col-xs-offset-3">
							<button class="btn btn-primary btn-aneh">SUBMIT</button>
						</div>
					</div>
				</div>
				{!! Form::close() !!}
			</div>
			<div class="col-md-3 hide-768">
				<div class="buah-box">
					<img class="apel" src="{{ $images[1]}}">

				</div>
			</div>
		</div>
	</div>
	</div>
</div>
@endsection

@section('scripts')
<script src="assets/plugin/bootstrap/js/jquery.bootstrap-responsive-tabs.min.js"></script>
<script src="assets/plugin/fancybox/js/jquery.fancybox.min.js"></script>
@endsection
