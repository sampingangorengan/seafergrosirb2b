@extends('frontend.supplier.master')

@section('title')
    Groceries | Control Room
@endsection

@section('content-header')
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))

                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
        @endforeach
    </div>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            List Order Groceries
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('supplier') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ url('supplier/products') }}">List Products</a></li>
            <li class="active">Product<li>

        </ol>
    </section>
    <br/>
@endsection

@section('content')

    <div class="box">
    {{--<div class="box-header">
        <h3 class="box-title">Data Table With Full Features</h3>
    </div>--}}
    <!-- /.box-header -->
        <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Order</th>
                    <th>Status</th>
                    <th>Quantity</th>
                    <th>Buyer</th>
                </thead>
                <tbody>
                    @foreach($order_groceries as $order_grocery)
                        <tr>
                            <td>#{{$order_grocery->order_id}}</td>
                            <td>{{$order_grocery->order->status->description}}</td>
                            <td>{{$order_grocery->qty}}</td>
                            <td>{{$order_grocery->order->user->company_name}}</td>
                        </tr>

                    @endforeach


                </tbody>
                <tfoot>
                <tr>

                </tr>

                </tfoot>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->

    @include('admin.layouts.delete_modal')
@endsection

@section('myscript')
    <!-- DataTables -->
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script>
        $("#example1").DataTable();
        $(".delete-entity-index").on('click', function(e){
            var $form=$(this).closest('form');
            e.preventDefault();
            $('#confirm').modal({ backdrop: 'static', keyboard: false })
                .on('click', '#delete', function() {
                    $form.trigger('submit'); // submit the form
                });
        })
    </script>
@endsection
