<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                @if(auth()->user()->profile_picture)
                    <img src="{!! asset(null) !!}profile_picture/{{ auth()->user()->photo->file }}" class="img-circle">
                @else
                    <img src="{!! asset(null) !!}assets/images/ico-header-acc.png" >
                @endif
            </div>
            <div class="pull-left info">
                <p>{{ auth()->user('supplier')->user_name }}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>

            {{-- s: Account Management--}}
            <li class="treeview {{ $active_menu == 'users' ? 'active' : '' }}">
                <a href="#">
                    <i class="fa fa-user"></i> <span>Supplier Management</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li{{ $active_submenu == 'users/' ? ' class=active' : '' }}><a href="{{ url('supplier/profile') }}"><i class="fa fa-user"></i>Supplier Profile</a></li>
                    <li{{ $active_submenu == 'roles/' ? ' class=active' : '' }}><a href="{{ url('supplier/products') }}"><i class="fa fa-black-tie"></i>Products</a></li>
                </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
