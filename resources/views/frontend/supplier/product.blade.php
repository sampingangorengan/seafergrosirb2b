@extends('frontend.supplier.master')

@section('title')
    Groceries | Control Room
@endsection

@section('content-header')
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))

                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
        @endforeach
    </div>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            List Groceries
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ url('supplier/products') }}">List Products</a></li>
            <li class="active">Product<li>
        </ol>
    </section>
    <br/>
@endsection

@section('content')

    <div class="box">
    {{--<div class="box-header">
        <h3 class="box-title">Data Table With Full Features</h3>
    </div>--}}
    <!-- /.box-header -->
        <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Product Rate</th>
                    <th>Sold</th>
                    <th>Stock</th>
                    <th></th>

                </thead>
                <tbody>
                @foreach($groceries as $grocery)
                    <tr>
                        <td>{{ $grocery->name }}</td>

                        <td>
                            <?
                                //Count the rating of grocery
                                $ratings = $grocery->ratings;
                                $total_rate = (float) $ratings->count() * 5.0 ;
                                $current_rate = 0.0;
                                foreach($ratings as $rating){
                                    $current_rate += (float) $rating->rate;
                                }
                                $rate = $current_rate / ($total_rate + 0.01) * 5.0;
                                echo
                                        '<b>'.
                                        number_format((float)$rate,1, '.', '') .
                                        ' / '.
                                        number_format((float) $total_rate,1, '.', '') .
                                        '</b>'.
                                        ' From '.$ratings->count() . ' Ratings';

                            ?>
                        </td>
                        <td>
                            <?
                                //Find the number of products SOLD
                                $order_grocery = App\Models\Order\Groceries::
                                                    where('groceries_id',$grocery->id)
                                                    // ->whereHas('order',function($q){
                                                    //     $q->where('order_status',4);
                                                    // })
                                                    ->sum('qty');
                                echo $order_grocery ?? 0 .' Items';
                            ?>
                        </td>
                        <td>
                            {{$grocery->stock}}
                        </td>
                        <td>
                            <a href="{{url('supplier/products/'.$grocery->id)}}" class="btn-sm btn-primary">Info Admin Stock Menipis</a>
                        </td>
                    </tr>
                @endforeach

                </tbody>
                <tfoot>
                <tr>
                    <th>Name</th>
                    <th>Product Rate</th>
                    <th>Sold</th>
                    <th>Detail</th>
                </tr>

                </tfoot>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
    <!-- /.box -->

    @include('admin.layouts.delete_modal')
@endsection

@section('myscript')
    <!-- DataTables -->
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script>
        $("#example1").DataTable();
        $(".delete-entity-index").on('click', function(e){
            var $form=$(this).closest('form');
            e.preventDefault();
            $('#confirm').modal({ backdrop: 'static', keyboard: false })
                .on('click', '#delete', function() {
                    $form.trigger('submit'); // submit the form
                });
        })
    </script>
@endsection
