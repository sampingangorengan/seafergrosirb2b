@extends('frontend.supplier.master')

@section('title')
    Edit User | Control Room
@endsection

@section('content-header')
    <div class="box-header with-border">
        <h3 class="box-title">Edit User</h3>
    </div>
@endsection

@section('content')
    <div class="box box-primary">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <!-- /.box-header -->
        <!-- form start -->
        {!! Form::model($user,['method'=>'post', 'action'=>['SupplierController@update', $user->id]]) !!}

        <div class="box-body">
            <div class="form-group">
                <label for="nameInput">Company Name/Supplier<span class="text-red" >*</span></label>
                {!! Form::text('company_name', $user->company_name, ['class'=>'form-control','disabled' => 'disabled']) !!}
            </div>
            <div class="form-group">
                <label for="nameInput">Bussiness Entity<span class="text-red" >*</span></label>
                <select class="form-control" name="bussiness_entity">
                    <option value="pt" <?php if($user->bussiness_entity == "pt"){echo 'selected';}?>>PT</option>
                    <option value="cv" <?php if($user->bussiness_entity == "cv"){echo 'selected';}?>>CV</option>
                    <option value="individual" <?php if($user->bussiness_entity == "individual"){echo 'selected';}?>>Individual</option>
                </select>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Company Address<span class="text-red" >*</span></label>
                {!! Form::text('billing_address', $user->billing_address, ['class'=>'form-control']) !!}
            </div>

            <div class="form-group">
                <label for="exampleInputPassword1">Province<span class="text-red" >*</span></label>
                <select class="form-control" name="province" id="select_province">
                    @foreach($provinces as $province => $nameprovince)
                    <option value="{{$province}}" <?php if($user->province == $province){echo 'selected';}?>>{{$nameprovince}}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="exampleInputPassword1">City<span class="text-red" >*</span></label>
                <select class="form-control" name="city" id="select_city">
                    @foreach($cities as $city => $namecity)
                    <option value="{{$city}}" <?php if($user->city == $city){echo 'selected';}?>>{{$namecity}}</option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="exampleInputPassword1">Area<span class="text-red" >*</span></label>
                <select class="form-control" name="area" id="select_area">
                    @foreach($areas as $area => $namearea)
                    <option value="{{$area}}" <?php if($user->area == $area){echo 'selected';}?>>{{$namearea}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Phone Number<span class="text-red" >*</span></label>
                <span class="input-group-label">+62</span>
                {!! Form::text('phone_number', $user->phone_number, ['class'=>'form-control']) !!}

            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Email<span class="text-red" >*</span></label>
                {!! Form::text('email', $user->email, ['class'=>'form-control']) !!}

            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Website</label>
                {!! Form::text('website', $user->website, ['class'=>'form-control']) !!}

            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Category<span class="text-red" >*</span></label>
                <select class="form-control" name="category">
                    <option value="distributor" <?php if($user->category == 'distributor'){echo 'selected';}?>>Distributor</option>
                    <option value="principal" <?php if($user->category == 'principal'){echo 'selected';}?>>Principal</option>
                </select>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Comodity<span class="text-red" >*</span></label>
                <select class="form-control" name="Commoditas">
                    <option value="seafood" <?php if($user->Commoditas == 'seafood'){echo 'selected';}?>>Seafood</option>
                    <option value="meat and poultry" <?php if($user->Commoditas == 'meat and poultry'){echo 'selected';}?>>Meat and Poultry</option>
                    <option value="vegetables" <?php if($user->Commoditas == 'vegetables'){echo 'selected';}?>>Vegetables</option>
                </select>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Prefer Selling Method<span class="text-red" >*</span></label>
                <select class="form-control" name="selling_method">
                    <option value="consignment" <?php if($user->selling_method == 'consignment'){echo 'selected';}?>>Consignment</option>
                    <option value="direct_Purchase" <?php if($user->selling_method == 'direct_Purchase'){echo 'selected';}?>>Direct Purchase</option>
                </select>

            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Types of goods supplied<span class="text-red" >*</span></label>

                {!! Form::text('type_of_goods', $user->type_of_goods, ['class'=>'form-control']) !!}

            </div>


            <div class="form-group">
                <label for="exampleInputPassword1">Country Supplier <span class="text-red" >*</span></label>
                <select class="form-control" name="country">
                    @foreach($countries as $country)
                    <option value="{{$country->code}}" <?php if($country->code == $user->country){echo 'selected';}?>>{{$country->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <!-- /.box-body -->

        <div class="box-footer">
            {{ csrf_field() }}
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
        {!! Form::close() !!}
    </div>
    <!-- /.box -->
@endsection

@section('myscript')
    <!-- DataTables -->
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script>
        $("#example1").DataTable();
    </script>
    <script>
        $('#select_province').on('change', function(){
            var province_id = $(this).val();
            $.ajax({
                type: 'get',
                url: '{{ url('user/ajax-cities-by-province') }}/' + province_id
            }).done(function(result){
                result = jQuery.parseJSON(result);
                if(result.status == 'success') {
                    $('#select_city').html('');
                    $.each(result.cities, function(k, v) {
                        $('#select_city').append('<option value="'+k+'">'+v+'</option>');
                    });


                } else {
                    alert(result.message);
                }
            })
        });

        $('#select_city').on('change', function(){
            var city_id = $(this).val();
            $.ajax({
                type: 'get',
                url: '{{ url('user/ajax-areas-by-city/') }}/' + city_id
            }).done(function(result){
                result = jQuery.parseJSON(result);
                if(result.status == 'success') {
                    $('#select_area').html('');
                    $.each(result.areas, function(k, v) {
                        $('#select_area').append('<option value="'+k+'">'+v+'</option>');
                    });

                } else {
                    alert(result.message);
                }
            })

        });
    </script>
@endsection
<!-- last -->
