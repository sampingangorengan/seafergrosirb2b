@extends('frontend.supplier.master')

@section('title')
    Users | Control Room
@endsection

@section('content-header')
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))

                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
        @endforeach
    </div>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
          {{ $user->user_name }}'s Detail
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ url('admin/complaints') }}"> Claims</a></li>
            <li class="active"> User Detail</li>
        </ol>
    </section>
@endsection

@section('content')

    <!-- Custom Tabs -->
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_1" data-toggle="tab">General Information</a></li>

            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    Actions <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    <li role="presentation"><a role="menuitem" tabindex="-1" href="{{ url('supplier/edit-profile/'.$user->id) }}"><button type="button" class="btn-sm btn-primary">Edit</button></a></li>
                </ul>
            </li>
            <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab_1">

                <div class="box box-solid">
                    <div class="box-header with-border">
                        <h1 class="box-title">User Detail:</h1>
                    </div>
                    <!-- /.box-header -->
                <!-- /.box-header -->
                    <div class="box-body no-padding">
                        <table class="table table-striped">
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>Property</th>
                                <th>Value</th>
                            </tr>
                            <tr>
                                <td>1.</td>
                                <td>Company Name/Supplier</td>
                                <td>
                                   {{ $user->company_name }}
                                </td>
                            </tr>
                            <tr>
                                <td>2.</td>
                                <td>Bussiness Entity</td>
                                <td>
                                   {{ $user->bussiness_entity }}
                                </td>
                            </tr>
                            <tr>
                                <td>3.</td>
                                <td>Company Address</td>
                                <td>
                                   {{ $user->billing_address }}
                                </td>
                            </tr>
                            <tr>
                                <td>4.</td>
                                <td>Phone Number</td>
                                <td>
                                   {{ $user->phone_number }}
                                </td>
                            </tr>
                            <tr>
                                <td>5.</td>
                                <td>Email</td>
                                <td>
                                   {{ $user->email }}
                                </td>
                            </tr>
                            <tr>
                                <td>6.</td>
                                <td>Website</td>
                                <td>
                                   {{ $user->website }}
                                </td>
                            </tr>
                            <tr>
                                <td>7.</td>
                                <td>Category</td>
                                <td>
                                   {{ $user->category }}
                                </td>
                            </tr>
                            <tr>
                                <td>7.</td>
                                <td>Comodity</td>
                                <td>
                                   {{ $user->Commoditas }}
                                </td>
                            </tr>
                            <tr>
                                <td>8.</td>
                                <td>Prefer Selling Method</td>
                                <td>
                                   {{ $user->selling_method }}
                                </td>
                            </tr>
                            <tr>
                                <td>9.</td>
                                <td>Types of goods supplied</td>
                                <td>
                                   {{ $user->type_of_goods }}
                                </td>
                            </tr>
                            <tr>
                                <td>10.</td>
                                <td>Products Origin</td>
                                <td>
                                    @if($user->product_origin != null)
                                        @foreach(json_decode($user->product_origin, true) as $origin)
                                        {{$origin['product_origin']}}
                                       @endforeach
                                    @endif
                                </td>
                            </tr>

                        </table>
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <div class="tab-pane" id="tab_2">

                <div class="box box-solid">
                    <div class="box-header with-border">
                        <h1 class="box-title">Cashback History: </h1>
                    </div>
                    <!-- /.box-header -->

                <!-- /.box-header -->
                    <div class="box-body no-padding">
                        <table class="table table-striped">
                            <tr>
                                <th width="200"><span style="color:#808184;">Date</span></th>
                                <th width="200"><span style="color:#808184;">Rewards Nominal</span></th>
                                <th width="200"><span style="color:#808184;">Activities</span></th>
                            </tr>
                        </table>
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <div class="tab-pane" id="tab_3">

                <div class="box box-solid">
                    <div class="box-header with-border">
                        <h1 class="box-title">Email Preference: </h1>
                    </div>
                    <!-- /.box-header -->


            </div>
        </div>
        <!-- /.tab-content -->
    </div>
    <!-- nav-tabs-custom -->
@endsection

@section('myscript')
    <!-- DataTables -->
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{!! asset('bower_components/AdminLTE') !!}/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script>
        $("#example1").DataTable();
        $(".delete-entity-index").on('click', function(e){
            var $form=$(this).closest('form');
            e.preventDefault();
            $('#confirm').modal({ backdrop: 'static', keyboard: false })
                .on('click', '#delete', function() {
                    $form.trigger('submit'); // submit the form
                });
        })
    </script>
@endsection
