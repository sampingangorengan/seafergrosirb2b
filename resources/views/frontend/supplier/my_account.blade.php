@extends('frontend.layout.master')

@section('title', App\Page::createTitle('My Account'))

@section('customcss')
    <link rel='stylesheet' href='assets/plugin/bootstrap/css/bootstrap-responsive-tabs.css'>
    <link rel="stylesheet" type="text/css" href="assets/plugin/fancybox/css/jquery.fancybox.min.css">
@endsection

@section('content')
<div class="main-content">
	<div class="container" id="checkout">
		<div class="row">
          <div class="col-md-5 col-xs-10 col-centered"><br><br>
            <h5 class="title title-margin">MY ACCOUNT</h5>
            <p class="b2">
              </p><center>
              </center>
            <p></p>
          </div>
        </div>
        <div class="row">
          <div class="col-md-3 hide-768">
            <img src="assets/images/img-market.png">
          </div>
          <div class="col-md-6 col-sm-8 col-xs-8 col-centered-768">
            <div class="menu-account-box">
              <div class="row">
                <div class="col-md-6 col-sm-6">
                  <table>
                    <tbody>
                      <tr>
                        <td><img src="assets/images/ico-maskot-dua.png"></td>
                        <td><a href="{{ url('user/my-profile-supplier') }}">Supplier Profile</a></td>
                      </tr>
                     <tr>
                    </tbody>
                  </table>
                </div>
                <div class="col-md-6 col-sm-6">
                  <table>
                    <tbody>
                      <tr>
                        <td><img src="assets/images/ico-shop-dua.png"></td>
                        <td><a href="{{ url('user/my-products-account') }}">Products</a></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <br>
            </div>
          </div>
          <div class="col-md-3 col-sm-10 col-xs-10 col-centered-768">
			<div class="col-md-12 col-sm-6 col-xs-6 no-padding-h hide-dekstop right">
				 <img class="img-market" src="assets/images/img-market.png">
			</div>
			<div class="col-md-12 col-sm-6 col-xs-6 no-padding-h">
				<img src="assets/images/img-seafer.png">
			</div>
          </div>
        </div>
	</div>
</div>
@endsection

@section('scripts')
<script src="assets/plugin/bootstrap/js/jquery.bootstrap-responsive-tabs.min.js"></script>
<script src="assets/plugin/fancybox/js/jquery.fancybox.min.js"></script>
@endsection
