@extends('frontend.layout.master')

@section('title', App\Page::createTitle('Become A Member'))

@section('customcss')
    <link rel='stylesheet' href='assets/plugin/bootstrap/css/bootstrap-responsive-tabs.css'>
    <link rel="stylesheet" type="text/css" href="assets/plugin/fancybox/css/jquery.fancybox.min.css">
    
@endsection

@section('content')
	<div class="main-content">
		<div class="container">
			<div class="row">
	            <div class="col-md-12">
	                <h6 class="title">Keuntungan Sahabat Seafer Grosir</h6>
	            </div>
	            <div class="col-md-3 col-sm-3 hide-320">
	                <ul class="side-nav side-nav-become-box">
	                    <li><a href="#" id="toBenefits" class="line-active">Keuntungan</a></li>
	                    <!-- <li><a href="#" id="toHowitworks">HOW IT WORKS</a></li> -->
	                    <li><a href="{{ url('user/sign-up') }}" id="toSignup">Masuk</a></li>
	                </ul>
	            </div>
	            <div class="col-md-9 col-sm-9">
	                <div class="become-box">
	                    <div class="row">
	                        <div class="col-md-9">
	                            <p class="detail-title">

	                            </p>
	                        </div>
	                    </div>
                        <div class="row">
                            <div class="col-md-12">
                            <p><b>Sebagai Sahabat Seafer Grosir, bisnis kuliner anda bisa nikmati keuntungan berikut: </b></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <img src="assets/images/ICON-01.png">
                            </div>
                            <div class="col-md-7">
                                <h6 class="sub-subtitle">Kualitas Terjamin</h6>
                                <p>
                                    Seafer Grosir berkomitmen terhadap qualitas produk yang ditawarkan ke customer. Standar Qualitas yang tinggi dipertahankan secara konsisten dan dijaga mulai dari sourcing, penyimpanan dan pengiriman.
                                    Buat sourcing, kita bermitra dengan produsen dan supplier yang bisa menawarkan qualitas produk yang baik.
                                    Buat Penyimpanan, maupun itu produk segar, beku ato kering, produk disimpan di gudang kita dengan suhu yang tepat secara higienis.
                                    Buat pengiriman, kita telah menyediakan armada pengiriman (cold chain) yang aman dengan penanganan yang baik dan suhu yang tepat.
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <img src="assets/images/ICON-02.png">
                            </div>
                            <div class="col-md-7">
                                <h6 class="sub-subtitle">Harga Terjangkau</h6>
                                <p>Seafer Grosir berkerja sama dengan produsen dan supplier untuk menawarkan harga yang terjangkau dan transparan buat sahabat pelanggan Seafer Grosir. Team pembelian Seafer Grosir selalu pantau kondisi pasar dan menawarkan sahabat Seafer Grosir harga pasar yang terendah!</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <img src="assets/images/ICON-03.png">
                            </div>
                            <div class="col-md-7">
                                <h6 class="sub-subtitle">Produk Lengkap</h6>
                                <p>Ribuan produk kuliner tersedia buat bisnis anda di Seafer Grosir.  Sahabat Seafer Grosir sekarang tidak perlu lagi kuatir atau repot ketika stok bahan masak sudah menipis karena berbagi produk telah tersedia buat anda di satu applikasi. Tinggal klik, order dan kirim di Seafer Grosir!</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <img src="assets/images/ICON-09.png">
                            </div>
                            <div class="col-md-7">
                                <h6 class="sub-subtitle">Pembayaran Flexibel</h6>
                                <p>Beli sekarang, bayar nanti. Karena bergerak di bidang kuliner, Seafer Grosir mengerti kebutuhan bisnis kuliner di hal perputaran keuangan. Oleh karena itu, Seafer Grosir bisa memberikan fasilitas pembayaran tempo buat sahabat Seafer Grosir (sesuai kentetuan dan syarat Seafer Grosir).</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <img src="assets/images/ICON-04.png">
                            </div>
                            <div class="col-md-7">
                                <h6 class="sub-subtitle">Pengiriman GRATIS dan Cepat</h6>
                                <p>Member Seafer Grosir tidak usah lagi kuatir dengan masalah pengiriman karena Seafer Grosir telah menyediakan armada pengiriman yang tepat waktu dan bebas biaya!</p>
                            </div>
                        </div>
                    </div>
	            </div>
	        </div>
	        <div class="row">
	            <div class="col-md-10 col-centered">
	                <div class="signup-member">

	                   <a class="btn btn-primary sign-up-btn" id="signup" href="{!! url('user/sign-up') !!}">Daftar Sekarang</a>
	                </div>
	            </div>
	        </div>

		</div>
	</div>
@endsection

@section('scripts')
<script src="assets/plugin/bootstrap/js/jquery.bootstrap-responsive-tabs.min.js"></script>
<script src="assets/plugin/fancybox/js/jquery.fancybox.min.js"></script>
@endsection
