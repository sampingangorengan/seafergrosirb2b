@extends('frontend.layout.master')

@section('title', App\Page::createTitle('About Us'))

@section('customcss')
    <link rel='stylesheet' href='assets/plugin/bootstrap/css/bootstrap-responsive-tabs.css'>
    <link rel="stylesheet" type="text/css" href="assets/plugin/fancybox/css/jquery.fancybox.min.css">
@endsection

@section('content')
	<div class="main-content">
		<div class="container">
			<div class="row align-center">
				<section>
					<h5 class="title title-margin">TENTANG KAMI</h5>
					<div class="col-md-12" style="padding: 10px 25%;">
						<img src="assets/images/about-us/about-1.png"></a>
					</div>
					<div class="col-md-12" style="padding: 10px 25%;">
						<img src="assets/images/about-us/about-2.png"></a>
					</div>
					<div class="col-md-12">
						<img src="assets/images/about-us/about-3.png"></a>
					</div>
					<div class="col-md-12">
						<img src="assets/images/about-us/about-4.png"></a>
					</div>
					<div class="col-md-12">
						<img src="assets/images/about-us/about-5.png"></a>
					</div>
					<div class="col-md-12">
						<img src="assets/images/about-us/about-6.png"></a>
					</div>
				</section>
	        </div>
		</div>
	</div>
@endsection

@section('scripts')
<script src="assets/plugin/bootstrap/js/jquery.bootstrap-responsive-tabs.min.js"></script>
<script src="assets/plugin/fancybox/js/jquery.fancybox.min.js"></script>
@endsection