@extends('frontend.layout.master')

@section('title', App\Page::createTitle('Payment'))

@section('customcss')
    <link rel='stylesheet' href='assets/plugin/bootstrap/css/bootstrap-responsive-tabs.css'>
    <link rel="stylesheet" type="text/css" href="assets/plugin/fancybox/css/jquery.fancybox.min.css">
    
@endsection

@section('content')
	<div class="main-content">
		<div class="container">
	        <div class="row hide-768">
				<div class="col-md-12">
					<span class="text-grey equal margin-top-10">
					  <a class="text-grey" href="index.html"><b>HOME</b></a> /
					  <a class="text-grey" href="loyalty-points.html"><b>PAYMENT</b></a>
					</span>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					{{-- <h5 class="title title-margin">PAYMENT</h5> --}}
                    <div class="row">
                        <div class="col-md-12">
                            <h5 class="title title-margin">Payment</h5>
                        </div>
                    </div>
					{!! $description[1]->description_content !!}
				</div>
			</div>
			<div class="row delivery-box">
				<div class="col-md-7 col-sm-7 col-xs-10 col-centered">
					{!! $description[2]->description_content !!}
					{{-- <p>
                        We are committed to delivering the products within the shortest time possible after payment is made using the following steps:
                    </p>
                    <div class="sub-content">

                        <ol>
                            <li>Please make your payment to the following account: PT. Seafer Grosir Jaya Raya – BCA 206 3232 327 / Bank Panin 1535007529</li>
                            <li>Take a picture of your proof of payment and upload it to Confirm Payment form.</li>
                            <li>Seafer Grosir will notify you after the payment is made.</li>
                            <li>Seafer Grosir will confirm your payment immediately within 24 hours and process your order.</li>

                        </ol>
                    </div> --}}
                    {{-- <div class="payment-img">
                        <img src="{!! asset(null) !!}assets/images/img-bca.png" class="payment">
                         <img src="{!! asset(null) !!}assets/images/img-mandiri.png" class="payment">
                    </div> --}}
                    {{-- <p>
                        We thank you for your timely and responsible payment.
                    </p> --}}
				</div>
			</div>

		</div>

	</div>
@endsection

@section('scripts')
<script src="assets/plugin/bootstrap/js/jquery.bootstrap-responsive-tabs.min.js"></script>
<script src="assets/plugin/fancybox/js/jquery.fancybox.min.js"></script>
@endsection
